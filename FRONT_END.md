
1. DateTimePicker: Pick cả ngày và giờ
- Template demo: https://keenthemes.com/metronic/preview/demo11/crud/forms/widgets/bootstrap-datetimepicker.html
- Docs: https://www.npmjs.com/package/bootstrap-datetime-picker
// Có thể chỉ dùng ngày hoặc giờ nhưng UX không tốt lắm. Pick riêng có thể dùng 2 cái dưới.

2. DatePicker: Chỉ pick ngày
- Template demo: https://keenthemes.com/metronic/preview/demo11/crud/forms/widgets/bootstrap-datepicker.html
- Docs: https://bootstrap-datepicker.readthedocs.io
- Live demo: https://uxsolutions.github.io/bootstrap-datepicker

3. TimePicker: Chỉ pick giờ
- Template demo: https://keenthemes.com/metronic/preview/demo11/crud/forms/widgets/bootstrap-timepicker.html
- Docs: http://jdewit.github.io/bootstrap-timepicker/

4. Bootstrap Select: Dùng cho select ít dữ liệu, đơn giản. Box search
- Template demo: https://keenthemes.com/metronic/preview/demo11/crud/forms/widgets/bootstrap-select.html
- Docs: https://developer.snapappointments.com/bootstrap-select/options/

5. Select2: Dùng cho select nhiều dữ liệu, custom item, nhiều dạng hiển thị, ...
- Template demo: https://keenthemes.com/metronic/preview/demo11/crud/forms/widgets/select2.html
- Docs: https://select2.org/

6. TreeView: Dùng hiển thị tree
- Template demo: https://keenthemes.com/metronic/preview/demo11/components/extended/treeview.html
- Docs: https://www.jstree.com/api/

7. Bootstrap Notify: Hiển thị thông báo khi thao tác thành công hoặc thất bại
- Template demo: https://keenthemes.com/metronic/preview/demo11/components/extended/bootstrap-notify.html
- Ví dụ: $.notify('Xóa khách hàng thành công!', { type: 'success' });


8. SweetAlert
- Demo: https://sweetalert.js.org/
- Ví dụ: KTApp.swal
- Tham khảo: DMMS/assets/js/pages/customer.js

9. 1 số plugin của App: DMMS/assets/js/core
- KTApp.blockPage()
- KTApp.unblockPage()
- KTDialog
- ...