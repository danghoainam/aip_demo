﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace DMMS.Controllers
{
    public class TeamController : BaseController<TeamController>
    {
        private readonly ITeamService _teamService;
        private readonly IStaffService _staffService;
        public TeamController(ITeamService teamService, IStaffService staffService)
        {
            _teamService = teamService;
            _staffService = staffService;
        }

        [DisplayName("Danh sách đội-nhóm")]
        [HttpGet("/teams")]
        public IActionResult gets()
        {
            var header = Header;
            header.Action = CustomConfigApi.Team.Gets;

            var request = new TeamRequest
            {
                limit = 100
            };

            var obj = _teamService.Gets(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        [DisplayName("Danh sách đội nhóm")]
        public IActionResult Index()
        {
            // Start tranfer data Subheader
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Team,
                Title = MenuTitle.Team
            };
            ViewData[SubHeaderData] = subHeader;

            //Lấy danh sách nhân viên
            ViewBag.Staff = GetStaffs();

            return View();
        }

        #region For Ajax

        /// <summary>
        /// Get staffs not in team
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpGet("/team/staff-not-in")]
        public IActionResult teamStaffNotIn()
        {
            var objReturn = new List<StaffModel>();
            var header = Header;
            header.Action = CustomConfigApi.Staff.Gets;

            var obj = _staffService.Get(header, null);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null && obj.data.items != null)
            {
                objReturn = obj.data.items.Select(i => new StaffModel
                {
                    uuid = i.uuid,
                    name = i.name,
                }).ToList();
            }
            return Ok(objReturn);
        }

        /// <summary>
        /// Add or update Staff in to Team
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("/team/upsert/{id}")]
        public IActionResult teamUpsert(string id, TeamUpsertRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Team.Upsert;
            if (!string.IsNullOrEmpty(request.module))
            {
                if (string.IsNullOrEmpty(request.uuids))
                {
                    return NotFound(new
                    {
                        error = EnumError.Error,
                        msg = "Bạn chưa chọn bản ghi!"
                    });
                }

                if (request.module.ToLower().Equals("staff"))
                {
                    request.staff = request.uuids.Split(",".ToCharArray()).ToList();
                }
            }
            request.uuid = id;
            request.uuids = string.Empty;

            var obj = _teamService.Upsert(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }

        /// <summary>
        /// Get customers of group
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpGet("/team/staff/{uuid}")]
        public IActionResult teamStaff(string uuid)
        {
            var objReturn = new List<TeamModel>();
            var header = Header;
            header.Action = CustomConfigApi.Team.Detail;
            var obj = _teamService.Detail(header, new TeamDetailRequest
            {
                uuid = uuid
            });

            if(obj!=null && (EnumError)obj.error == EnumError.Success && obj.data!=null)
            {
                objReturn = obj.data.staff.Select(i => new TeamModel
                {
                    uuid = i.uuid,
                    name = i.name,
                }).ToList();
            }

            return Ok(objReturn);
        }

        public IActionResult getStaff(StaffListRequest request)
        {
            var objReturn = new List<StaffModel>();

            var header = Header;
            header.Action = CustomConfigApi.Staff.Gets;

            var obj = _staffService.Get(header, null);
            if (obj!=null && (EnumError)obj.error == EnumError.Success && obj.data!=null)
            {
                objReturn = obj.data.items.Select(i => new StaffModel
                {
                    uuid = i.uuid,
                    name = i.name,
                }).ToList();
            }

            return Ok(objReturn);
        }
        #endregion

        [DisplayName("Action: Xóa đội nhóm")]
        [HttpPut]
        public IActionResult Delete(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Team.Delete;

            var request = new TeamDetailRequest
            {
                uuid = id,
            };

            var obj = _teamService.Delete(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        [DisplayName("Thêm mới đội-nhóm")]
        public IActionResult Create()
        {
            var model = new TeamUpsertRequest();

            return View(model);
        }

        [DisplayName("Action: Thêm mới đội-nhóm")]
        [HttpPost("/team/create")]
        public IActionResult Create(TeamUpsertRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Team.Upsert;

            var obj = _teamService.Upsert(header, model);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        [DisplayName("Cập nhật đội-nhóm")]
        public IActionResult Edit(string id)
        {
            var model = new TeamUpsertRequest();
            var header = Header;
            header.Action = CustomConfigApi.Team.Detail;

            var request = new TeamDetailRequest
            {
                uuid = id,
            };

            var obj = _teamService.Detail(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                model = new TeamUpsertRequest
                {
                    uuid = obj.data.uuid,
                    name = obj.data.name,
                    //description = obj.data.description,
                    //leader_uuid = obj.data.leader.uuid,
                };

                if (obj.data.staff != null && obj.data.staff.Count > 0)
                {
                    model.staff_uuid_str = JsonConvert.SerializeObject(obj.data.staff.Select(x => x.uuid));
                }
            }

            return View(model);
        }

        [DisplayName("Action: Cập nhật đội-nhóm")]
        [HttpPut("/team/edit/{uuid}")]
        public IActionResult Edit(TeamUpsertRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Team.Upsert;

            var request = new TeamUpsertRequest
            {
                uuid = model.uuid,
                name = model.name,
                description = model.description,
                leader_uuid = model.leader_uuid,
                staff = model.staff
            };

            var obj = _teamService.Upsert(header, request);
            if(obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        [DisplayName("Chi tiết đội-nhóm")]
        public IActionResult Detail(string id)
        {
            TeamDataResponse model = new TeamDataResponse();

            var header = Header;
            header.Action = CustomConfigApi.Team.Detail;


            var request = new TeamDetailRequest
            {
                uuid = id
            };

            var obj = _teamService.Detail(header, request);
            if(obj.error == 0)
            {
                model = obj.data;

                return View(model);
            }
            else
            {
                return NotFound();
            }
        }

        [DisplayName("Lấy thông tin nhân viên")]
        public IActionResult detailAjax(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Team.Detail;
            var request = new TeamDetailRequest
            {
                uuid = id,
            };

            var obj = _teamService.Detail(header, request);

            return Ok(obj);
        }


        //Lấy ra danh sách filter theo team(id, name)
        public void Filter(ref List<TeamModel> entries)
        {
            if (ModelState.IsValid)
            {
                var header = Header;
                header.Action = CustomConfigApi.Team.Filter;

                var request = new TeamRequest
                {

                };

                var obj = _teamService.Filter(header, request);
                entries = obj.data.items;
                ViewBag.Filter = entries;
            }
        }

        //Lấy danh sách nhân viên
        private List<StaffModel> GetStaffs()
        {
            var header = Header;
            header.Action = CustomConfigApi.Staff.GetsList;

            var request = new StaffListRequest()
            {
                limit = 200
            };

            var obj = _staffService.GetList(header, request);

            if (obj.data != null && obj.error == 0)
            {
                return obj.data.items;
            }

            return null;
        }

        [DisplayName("Action: Thêm staff vao team")]
        [HttpPost("/team/add-staff")]
        public IActionResult AddStaff(string team_uuid, string staff_uuid)
        {
            var header = Header;
            header.Action = CustomConfigApi.Team.Add;

            var request = new
            {
                team_uuid,
                staff_uuid
            };

            var obj = _teamService.Add(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }

        [DisplayName("Action: Xoa staff khoi team")]
        [HttpPost("/team/remove-staff")]
        public IActionResult RemoveStaff(string staff_uuid)
        {
            var header = Header;
            header.Action = CustomConfigApi.Team.Remove;

            var request = new  { staff_uuid };

            var obj = _teamService.Remove(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }

        [DisplayName("Action: Xoa staff khoi team")]
        [HttpPost("/team/change-leader")]
        public IActionResult ChangeLeader(string team_uuid, string staff_uuid)
        {
            var header = Header;
            header.Action = CustomConfigApi.Team.ChangeLeader;
            var request = new
            {
                team_uuid,
                staff_uuid
            };
            var obj = _teamService.ChangeLeader(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
    }
}