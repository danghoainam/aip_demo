﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class NotificationController : BaseController<NotificationController>
    {
        private readonly INotificationService _notificationSerivce;
        public NotificationController(INotificationService notification)
        {
            _notificationSerivce = notification;
        }
        [Route("/Notification/index")]
        
        public IActionResult Index(NotificationRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Notification.Gets;
            if (request.limit == 0)
            {
                request.limit = 20;
            }
            var obj = _notificationSerivce.Get(header, request);
          
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj.data.items);
            }
            return NotFound();
        }
        [Route("/Notification/index/{page}")]
        public IActionResult scroll(NotificationRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Notification.Gets;
            if (request.limit == 0)
            {
                request.limit = 20;
            }
            var obj = _notificationSerivce.Get(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj.data);
            }
            return NotFound();
        }
        public IActionResult ReadAll()
        {
            var header = Header;
            header.Action = CustomConfigApi.Notification.Read;
            var obj = _notificationSerivce.ReadNoti(header, null);
            if (obj != null )
            {
                return Ok(obj.data);
            }
            return NotFound();
        }
        public IActionResult Count(NotificationRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Notification.Counts;
            var obj = _notificationSerivce.Count(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        public IActionResult Upsert (string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Notification.Upserts;
            var request = new NotificationRequest
            {
                uuid = id,
            };
            var obj = _notificationSerivce.Upsert(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }
    }
}