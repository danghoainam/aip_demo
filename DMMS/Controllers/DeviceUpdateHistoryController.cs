﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class DeviceUpdateHistoryController : BaseController<DeviceUpdateHistoryController>
    {
        private readonly IDeviceService _deviceService;
        public DeviceUpdateHistoryController(IDeviceService deviceService)
        {
            _deviceService = deviceService;
        }

        [DisplayName("Lấy danh sách lịch sử cập nhật thiết bị")]
        [HttpGet("deviceupdatehistory/gets/{uuid}")]
        public IActionResult gets(string uuid)
        {
            var header = Header;
            header.Action = CustomConfigApi.Devices.History;

            var request = new DeviceHistoryRequest
            {
                limit = 20,
                uuid = uuid,
            };

            var obj = _deviceService.History(header, request);

            foreach (var item in obj.data.items)
            {
                if(item.images != null)
                {
                    foreach (var img in item.images)
                    {
                        img.path = CustomConfigApi.ImageDomain + "/" + img.path;
                    }
                }
            }

            if(obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}