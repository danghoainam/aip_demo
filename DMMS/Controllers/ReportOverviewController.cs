﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;

namespace DMMS.Controllers
{
    public class ReportOverviewController : BaseController<ReportOverviewController>
    {
        private readonly IReportOverviewService _reportOverviewService;
        private readonly IReportStatisticDetailCallService _reportStatisticDetailCallService;
        private readonly IReportStatisticDetailStatusDeviceService _reportStatisticDetailStatusDeviceService;
        private readonly IReportStatisticDetailService _reportStatisticDetailService;
        private readonly IReportStatisticDetailTaskService _reportStatisticDetailTaskService;
        private readonly IReportStatisticTroubleshootService _reportStatisticTroubleshootService;
        private readonly IReportStatisticDetailSuppliesTaskService _reportStatisticDetailSuppliesTaskService;
        private readonly IReportStatisticDetailStatusContractService _reportStatisticDetailStatusContractService;
        private readonly ILocationService _locationService;
        private readonly IManageFiledService _manageFieldService;
        private readonly ICustomerService _customerService;
        private readonly ITeamService _teamService;
        private readonly IProjectService _projectService;
        private readonly IStaffService _staffService;

        public ReportOverviewController(IReportOverviewService reportOverviewService, IReportStatisticDetailCallService reportStatisticDetailCallService, IReportStatisticDetailService reportStatisticDetailService, IReportStatisticDetailStatusDeviceService reportStatisticDetailStatusDeviceService, IReportStatisticDetailTaskService reportStatisticDetailTaskService, IReportStatisticTroubleshootService reportStatisticTroubleshootService, IReportStatisticDetailSuppliesTaskService reportStatisticDetailSuppliesTaskService, IReportStatisticDetailStatusContractService reportStatisticDetailStatusContractService, ICustomerService customerService, ILocationService locationService, IManageFiledService manageFieldService, ITeamService teamService, IProjectService project, IStaffService staffService)
        {
            _reportOverviewService = reportOverviewService;
            _reportStatisticDetailCallService = reportStatisticDetailCallService;
            _reportStatisticDetailService = reportStatisticDetailService;
            _reportStatisticDetailStatusDeviceService = reportStatisticDetailStatusDeviceService;
            _reportStatisticDetailTaskService = reportStatisticDetailTaskService;
            _reportStatisticTroubleshootService = reportStatisticTroubleshootService;
            _reportStatisticDetailSuppliesTaskService = reportStatisticDetailSuppliesTaskService;
            _reportStatisticDetailStatusContractService = reportStatisticDetailStatusContractService;
            _customerService = customerService;
            _locationService = locationService;
            _manageFieldService = manageFieldService;
            _teamService = teamService;
            _projectService = project;
            _staffService = staffService;
        }

        public IActionResult Index()
        {
            // Start tranfer data Subheader
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Dashboard,
                Title = MenuTitle.Dashboard,
                SubTitle = SubMenuTitle.ReportOverview,
                Toolbar = 1
            };
            ViewData[SubHeaderData] = subHeader;
            return View();
        }

        [HttpPost("/projectoverview/all")]
        public IActionResult getReportCharts(RequestReportOverview request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Dashboard.GetsProject;
            var obj = _reportOverviewService.GetProject(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }


        [HttpPost("/ticketoverview/all")]
        public IActionResult getTicketCharts(RequestReportOverview request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Dashboard.GetsTicket;
            var obj = _reportOverviewService.GetTicket(header, request);

            var listDate = new List<string>();

            for (var dt = Convert.ToDateTime(request.timeFrom); dt <= Convert.ToDateTime(request.timeTo); dt = dt.AddDays(1))
            {
                listDate.Add(dt.ToString("dd/MM"));
            }

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                var dic = new Dictionary<string, List<object>>();
                obj.data.items.ToList().ForEach(t =>
                {
                    string date = t.date.Substring(0, t.date.Length - 5);
                    var obj1 = new TicketOverviewModel
                    {
                        type = t.type,
                        count = t.count
                    };
                    if (!dic.ContainsKey(date))
                        dic.Add(date, new List<object>());
                    dic[date].Add(obj1);

                });
                foreach (string dates in listDate)
                {
                    string date1 = dates;
                    var obj2 = new TicketOverviewModel
                    {
                        type = "100",
                        count = "0"
                    };
                    if (!dic.ContainsKey(date1))
                        dic.Add(date1, new List<object>());
                    dic[date1].Add(obj2);

                }
                var dic1 = dic.OrderBy(o => DateTime.ParseExact(o.Key, "dd/MM", null)).ToDictionary(t => t.Key, t => t.Value);
                Console.WriteLine(dic1);
                if (dic1.Count < 32)
                {
                    return Ok(dic1);
                }
                else if (dic1.Count >= 32 && dic1.Count < 60)
                {
                    int a = 2;
                    var dic2 = new Dictionary<string, List<object>>();

                    for (int i = 0; i < dic1.Count; i++)
                    {
                        int b = i * a;
                        if ((a + b - 1) < dic1.Count)
                        {
                            int count1 = 0;
                            int count2 = 0;

                            TicketOverviewModel itemTotal1 = new TicketOverviewModel();
                            TicketOverviewModel itemTotal2 = new TicketOverviewModel();

                            for (int j = b; j < a + b; j++)
                            {
                                List<object> item = dic1[dic1.Keys.ToList()[j]];

                                foreach (TicketOverviewModel item2 in item)
                                {
                                    if (item2.type == "1")
                                    {
                                        count1 += Int32.Parse(item2.count);
                                    }
                                    if (item2.type == "2")
                                    {
                                        count2 += Int32.Parse(item2.count);
                                    }
                                }
                            }
                            itemTotal1.type = "1";
                            itemTotal1.count = count1.ToString();
                            itemTotal2.type = "2";
                            itemTotal2.count = count2.ToString();

                            string dateItem = dic1.Keys.ToList()[b] + '-' + dic1.Keys.ToList()[a + b - 1];
                            if (!dic2.ContainsKey(dateItem))
                                dic2.Add(dateItem, new List<object>());
                            dic2[dateItem].Add(itemTotal1);
                            dic2[dateItem].Add(itemTotal2);
                        }
                    }
                        
                    if (dic1.Count % a == 1)
                    {
                        int count1 = 0;
                        int count2 = 0;

                        TicketOverviewModel itemTotal1 = new TicketOverviewModel();
                        TicketOverviewModel itemTotal2 = new TicketOverviewModel();

                        List<object> item = dic1[dic1.Keys.ToList()[dic1.Count - 1]];

                        foreach (TicketOverviewModel item2 in item)
                        {
                            if (item2.type == "1")
                            {
                                count1 += Int32.Parse(item2.count);
                            }
                            if (item2.type == "2")
                            {
                                count2 += Int32.Parse(item2.count);
                            }
                        }
                        itemTotal1.type = "1";
                        itemTotal1.count = count1.ToString();
                        itemTotal2.type = "2";
                        itemTotal2.count = count2.ToString();

                        string dateItem = dic1.Keys.ToList()[dic1.Count - 1];
                        if (!dic2.ContainsKey(dateItem))
                            dic2.Add(dateItem, new List<object>());
                        dic2[dateItem].Add(itemTotal1);
                        dic2[dateItem].Add(itemTotal2);
                    }
                    else if (dic1.Count % a > 1)
                    {
                        int count1 = 0;
                        int count2 = 0;

                        TicketOverviewModel itemTotal1 = new TicketOverviewModel();
                        TicketOverviewModel itemTotal2 = new TicketOverviewModel();

                        for (int j = dic1.Count - (dic1.Count % a); j < dic1.Count; j++)
                        {
                            List<object> item = dic1[dic1.Keys.ToList()[j]];

                            foreach (TicketOverviewModel item2 in item)
                            {
                                if (item2.type == "1")
                                {
                                    count1 += Int32.Parse(item2.count);
                                }
                                if (item2.type == "2")
                                {
                                    count2 += Int32.Parse(item2.count);
                                }
                            }
                        }
                        itemTotal1.type = "1";
                        itemTotal1.count = count1.ToString();
                        itemTotal2.type = "2";
                        itemTotal2.count = count2.ToString();

                        string dateItem = dic1.Keys.ToList()[dic1.Count - (dic1.Count % a)] + '-' + dic1.Keys.ToList()[dic1.Count - 1];
                        if (!dic2.ContainsKey(dateItem))
                            dic2.Add(dateItem, new List<object>());
                        dic2[dateItem].Add(itemTotal1);
                        dic2[dateItem].Add(itemTotal2);
                    }
                    return Ok(dic2);
                }
                else
                {
                    int a = dic1.Count / 30;
                    var dic2 = new Dictionary<string, List<object>>();

                    for (int i = 0; i < dic1.Count; i++)
                    {
                        int b = i * a;
                        if ((a + b - 1) < dic1.Count)
                        {
                            int count1 = 0;
                            int count2 = 0;

                            TicketOverviewModel itemTotal1 = new TicketOverviewModel();
                            TicketOverviewModel itemTotal2 = new TicketOverviewModel();

                            for (int j = b; j < a + b; j++)
                            {
                                List<object> item = dic1[dic1.Keys.ToList()[j]];

                                foreach (TicketOverviewModel item2 in item)
                                {
                                    if (item2.type == "1")
                                    {
                                        count1 += Int32.Parse(item2.count);
                                    }
                                    if (item2.type == "2")
                                    {
                                        count2 += Int32.Parse(item2.count);
                                    }
                                }
                            }
                            itemTotal1.type = "1";
                            itemTotal1.count = count1.ToString();
                            itemTotal2.type = "2";
                            itemTotal2.count = count2.ToString();

                            string dateItem = dic1.Keys.ToList()[b] + '-' + dic1.Keys.ToList()[a + b - 1];
                            if (!dic2.ContainsKey(dateItem))
                                dic2.Add(dateItem, new List<object>());
                            dic2[dateItem].Add(itemTotal1);
                            dic2[dateItem].Add(itemTotal2);
                        }
                    }

                    if (dic1.Count % a == 1)
                    {
                        int count1 = 0;
                        int count2 = 0;

                        TicketOverviewModel itemTotal1 = new TicketOverviewModel();
                        TicketOverviewModel itemTotal2 = new TicketOverviewModel();

                        List<object> item = dic1[dic1.Keys.ToList()[dic1.Count - 1]];

                        foreach (TicketOverviewModel item2 in item)
                        {
                            if (item2.type == "1")
                            {
                                count1 += Int32.Parse(item2.count);
                            }
                            if (item2.type == "2")
                            {
                                count2 += Int32.Parse(item2.count);
                            }
                        }
                        itemTotal1.type = "1";
                        itemTotal1.count = count1.ToString();
                        itemTotal2.type = "2";
                        itemTotal2.count = count2.ToString();

                        string dateItem = dic1.Keys.ToList()[dic1.Count - 1];
                        if (!dic2.ContainsKey(dateItem))
                            dic2.Add(dateItem, new List<object>());
                        dic2[dateItem].Add(itemTotal1);
                        dic2[dateItem].Add(itemTotal2);
                    }
                    else if (dic1.Count % a > 1)
                    {
                        int count1 = 0;
                        int count2 = 0;

                        TicketOverviewModel itemTotal1 = new TicketOverviewModel();
                        TicketOverviewModel itemTotal2 = new TicketOverviewModel();

                        for (int j = dic1.Count - (dic1.Count % a); j < dic1.Count; j++)
                        {
                            List<object> item = dic1[dic1.Keys.ToList()[j]];

                            foreach (TicketOverviewModel item2 in item)
                            {
                                if (item2.type == "1")
                                {
                                    count1 += Int32.Parse(item2.count);
                                }
                                if (item2.type == "2")
                                {
                                    count2 += Int32.Parse(item2.count);
                                }
                            }
                        }
                        itemTotal1.type = "1";
                        itemTotal1.count = count1.ToString();
                        itemTotal2.type = "2";
                        itemTotal2.count = count2.ToString();

                        string dateItem = dic1.Keys.ToList()[dic1.Count - (dic1.Count % a)] + '-' + dic1.Keys.ToList()[dic1.Count - 1];
                        if (!dic2.ContainsKey(dateItem))
                            dic2.Add(dateItem, new List<object>());
                        dic2[dateItem].Add(itemTotal1);
                        dic2[dateItem].Add(itemTotal2);
                    }
                    return Ok(dic2);
                }
            }
            return NotFound();
        }

        [HttpPost("/taskoverview/all")]
        public IActionResult getTaskCharts(RequestReportOverview request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Dashboard.GetsTask;
            var obj = _reportOverviewService.GetTask(header, request);
            var listDate = new List<string>();
            for (var dt = Convert.ToDateTime(request.timeFrom); dt <= Convert.ToDateTime(request.timeTo); dt = dt.AddDays(1))
            {
                listDate.Add(dt.ToString("dd/MM"));
            }
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                var dic = new Dictionary<string, List<object>>();
                obj.data.items.ToList().ForEach(t =>
                {
                    string date = t.date.Substring(0, t.date.Length - 5);
                    var obj1 = new TicketOverviewModel
                    {
                        type = t.type,
                        count = t.count,
                        state = t.state
                    };
                    if (!dic.ContainsKey(date))
                        dic.Add(date, new List<object>());
                    dic[date].Add(obj1);
                });
                foreach (string dates in listDate)
                {
                    string date1 = dates;
                    var obj2 = new TicketOverviewModel
                    {
                        state = "100",
                        count = "0",
                        type = "100"
                    };
                    if (!dic.ContainsKey(date1))
                        dic.Add(date1, new List<object>());
                    dic[date1].Add(obj2);
                }
                var dic1 = dic.OrderBy(o => DateTime.ParseExact(o.Key, "dd/MM", null)).ToDictionary(t => t.Key, t => t.Value);
                Console.WriteLine(dic1);
                if (dic1.Count < 32)
                {
                    return Ok(dic1);
                }
                else if (dic1.Count >= 32 && dic1.Count < 60)
                {
                    int a = 2;
                    var dic2 = new Dictionary<string, List<object>>();

                    for (int i = 0; i < dic1.Count; i++)
                    {
                        int b = i * a;
                        if ((a + b - 1) < dic1.Count)
                        {
                            int count1 = 0;
                            int count2 = 0;
                            int count3 = 0;

                            TicketOverviewModel itemTotal1 = new TicketOverviewModel();
                            TicketOverviewModel itemTotal2 = new TicketOverviewModel();
                            TicketOverviewModel itemTotal3 = new TicketOverviewModel();

                            for (int j = b; j < a + b; j++)
                            {
                                List<object> item = dic1[dic1.Keys.ToList()[j]];

                                foreach (TicketOverviewModel item2 in item)
                                {
                                    if(item2.type == "1")
                                    {
                                        if (item2.state == "1" || item2.state == "2")
                                        {
                                            count1 += Int32.Parse(item2.count);
                                        }
                                        else if (item2.state == "8")
                                        {
                                            count2 += Int32.Parse(item2.count);
                                        }
                                        else
                                        {
                                            count3 += Int32.Parse(item2.count);
                                        }
                                    }
                                }
                            }
                            itemTotal1.type = "1";
                            itemTotal1.state = "1";
                            itemTotal1.count = count1.ToString();
                            itemTotal2.type = "1";
                            itemTotal2.state = "8";
                            itemTotal2.count = count2.ToString();
                            itemTotal3.type = "1";
                            itemTotal3.state = "3";
                            itemTotal3.count = count3.ToString();

                            string dateItem = dic1.Keys.ToList()[b] + '-' + dic1.Keys.ToList()[a + b - 1];
                            if (!dic2.ContainsKey(dateItem))
                                dic2.Add(dateItem, new List<object>());
                            dic2[dateItem].Add(itemTotal1);
                            dic2[dateItem].Add(itemTotal2);
                            dic2[dateItem].Add(itemTotal3);
                        }
                    }

                    if (dic1.Count % a == 1)
                    {
                        int count1 = 0;
                        int count2 = 0;
                        int count3 = 0;

                        TicketOverviewModel itemTotal1 = new TicketOverviewModel();
                        TicketOverviewModel itemTotal2 = new TicketOverviewModel();
                        TicketOverviewModel itemTotal3 = new TicketOverviewModel();

                        List<object> item = dic1[dic1.Keys.ToList()[dic1.Count - 1]];

                        foreach (TicketOverviewModel item2 in item)
                        {
                            if (item2.type == "1")
                            {
                                if (item2.state == "1" || item2.state == "2")
                                {
                                    count1 += Int32.Parse(item2.count);
                                }
                                else if (item2.state == "8")
                                {
                                    count2 += Int32.Parse(item2.count);
                                }
                                else
                                {
                                    count3 += Int32.Parse(item2.count);
                                }
                            }
                        }
                        itemTotal1.type = "1";
                        itemTotal1.state = "1";
                        itemTotal1.count = count1.ToString();
                        itemTotal2.type = "1";
                        itemTotal2.state = "8";
                        itemTotal2.count = count2.ToString();
                        itemTotal3.type = "1";
                        itemTotal3.state = "3";
                        itemTotal3.count = count3.ToString();

                        string dateItem = dic1.Keys.ToList()[dic1.Count - 1];
                        if (!dic2.ContainsKey(dateItem))
                            dic2.Add(dateItem, new List<object>());
                        dic2[dateItem].Add(itemTotal1);
                        dic2[dateItem].Add(itemTotal2);
                        dic2[dateItem].Add(itemTotal3);
                    }
                    else if (dic1.Count % a > 1)
                    {
                        int count1 = 0;
                        int count2 = 0;
                        int count3 = 0;

                        TicketOverviewModel itemTotal1 = new TicketOverviewModel();
                        TicketOverviewModel itemTotal2 = new TicketOverviewModel();
                        TicketOverviewModel itemTotal3 = new TicketOverviewModel();

                        for (int j = dic1.Count - (dic1.Count % a); j < dic1.Count; j++)
                        {
                            List<object> item = dic1[dic1.Keys.ToList()[j]];

                            foreach (TicketOverviewModel item2 in item)
                            {
                                if (item2.type == "1")
                                {
                                    if (item2.state == "1" || item2.state == "2")
                                    {
                                        count1 += Int32.Parse(item2.count);
                                    }
                                    else if (item2.state == "8")
                                    {
                                        count2 += Int32.Parse(item2.count);
                                    }
                                    else
                                    {
                                        count3 += Int32.Parse(item2.count);
                                    }
                                }
                            }
                        }
                        itemTotal1.type = "1";
                        itemTotal1.state = "1";
                        itemTotal1.count = count1.ToString();
                        itemTotal2.type = "1";
                        itemTotal2.state = "8";
                        itemTotal2.count = count2.ToString();
                        itemTotal3.type = "1";
                        itemTotal3.state = "3";
                        itemTotal3.count = count3.ToString();

                        string dateItem = dic1.Keys.ToList()[dic1.Count - (dic1.Count % a)] + '-' + dic1.Keys.ToList()[dic1.Count - 1];
                        if (!dic2.ContainsKey(dateItem))
                            dic2.Add(dateItem, new List<object>());
                        dic2[dateItem].Add(itemTotal1);
                        dic2[dateItem].Add(itemTotal2);
                        dic2[dateItem].Add(itemTotal3);
                    }
                    return Ok(dic2);
                }
                else
                {
                    int a = dic1.Count / 30;
                    var dic2 = new Dictionary<string, List<object>>();

                    for (int i = 0; i < dic1.Count; i++)
                    {
                        int b = i * a;
                        if ((a + b - 1) < dic1.Count)
                        {
                            int count1 = 0;
                            int count2 = 0;
                            int count3 = 0;

                            TicketOverviewModel itemTotal1 = new TicketOverviewModel();
                            TicketOverviewModel itemTotal2 = new TicketOverviewModel();
                            TicketOverviewModel itemTotal3 = new TicketOverviewModel();

                            for (int j = b; j < a + b; j++)
                            {
                                List<object> item = dic1[dic1.Keys.ToList()[j]];

                                foreach (TicketOverviewModel item2 in item)
                                {
                                    if (item2.type == "1")
                                    {
                                        if (item2.state == "1" || item2.state == "2")
                                        {
                                            count1 += Int32.Parse(item2.count);
                                        }
                                        else if (item2.state == "8")
                                        {
                                            count2 += Int32.Parse(item2.count);
                                        }
                                        else
                                        {
                                            count3 += Int32.Parse(item2.count);
                                        }
                                    }
                                }
                            }
                            itemTotal1.type = "1";
                            itemTotal1.state = "1";
                            itemTotal1.count = count1.ToString();
                            itemTotal2.type = "1";
                            itemTotal2.state = "8";
                            itemTotal2.count = count2.ToString();
                            itemTotal3.type = "1";
                            itemTotal3.state = "3";
                            itemTotal3.count = count3.ToString();

                            string dateItem = dic1.Keys.ToList()[b] + '-' + dic1.Keys.ToList()[a + b - 1];
                            if (!dic2.ContainsKey(dateItem))
                                dic2.Add(dateItem, new List<object>());
                            dic2[dateItem].Add(itemTotal1);
                            dic2[dateItem].Add(itemTotal2);
                            dic2[dateItem].Add(itemTotal3);
                        }
                    }

                    if (dic1.Count % a == 1)
                    {
                        int count1 = 0;
                        int count2 = 0;
                        int count3 = 0;

                        TicketOverviewModel itemTotal1 = new TicketOverviewModel();
                        TicketOverviewModel itemTotal2 = new TicketOverviewModel();
                        TicketOverviewModel itemTotal3 = new TicketOverviewModel();

                        List<object> item = dic1[dic1.Keys.ToList()[dic1.Count - 1]];

                        foreach (TicketOverviewModel item2 in item)
                        {
                            if (item2.type == "1")
                            {
                                if (item2.state == "1" || item2.state == "2")
                                {
                                    count1 += Int32.Parse(item2.count);
                                }
                                else if (item2.state == "8")
                                {
                                    count2 += Int32.Parse(item2.count);
                                }
                                else
                                {
                                    count3 += Int32.Parse(item2.count);
                                }
                            }
                        }
                        itemTotal1.type = "1";
                        itemTotal1.state = "1";
                        itemTotal1.count = count1.ToString();
                        itemTotal2.type = "1";
                        itemTotal2.state = "8";
                        itemTotal2.count = count2.ToString();
                        itemTotal3.type = "1";
                        itemTotal3.state = "3";
                        itemTotal3.count = count3.ToString();

                        string dateItem = dic1.Keys.ToList()[dic1.Count - 1];
                        if (!dic2.ContainsKey(dateItem))
                            dic2.Add(dateItem, new List<object>());
                        dic2[dateItem].Add(itemTotal1);
                        dic2[dateItem].Add(itemTotal2);
                        dic2[dateItem].Add(itemTotal3);
                    }
                    else if (dic1.Count % a > 1)
                    {
                        int count1 = 0;
                        int count2 = 0;
                        int count3 = 0;

                        TicketOverviewModel itemTotal1 = new TicketOverviewModel();
                        TicketOverviewModel itemTotal2 = new TicketOverviewModel();
                        TicketOverviewModel itemTotal3 = new TicketOverviewModel();

                        for (int j = dic1.Count - (dic1.Count % a); j < dic1.Count; j++)
                        {
                            List<object> item = dic1[dic1.Keys.ToList()[j]];

                            foreach (TicketOverviewModel item2 in item)
                            {
                                if (item2.type == "1")
                                {
                                    if (item2.state == "1" || item2.state == "2")
                                    {
                                        count1 += Int32.Parse(item2.count);
                                    }
                                    else if (item2.state == "8")
                                    {
                                        count2 += Int32.Parse(item2.count);
                                    }
                                    else
                                    {
                                        count3 += Int32.Parse(item2.count);
                                    }
                                }
                            }
                        }
                        itemTotal1.type = "1";
                        itemTotal1.state = "1";
                        itemTotal1.count = count1.ToString();
                        itemTotal2.type = "1";
                        itemTotal2.state = "8";
                        itemTotal2.count = count2.ToString();
                        itemTotal3.type = "1";
                        itemTotal3.state = "3";
                        itemTotal3.count = count3.ToString();

                        string dateItem = dic1.Keys.ToList()[dic1.Count - (dic1.Count % a)] + '-' + dic1.Keys.ToList()[dic1.Count - 1];
                        if (!dic2.ContainsKey(dateItem))
                            dic2.Add(dateItem, new List<object>());
                        dic2[dateItem].Add(itemTotal1);
                        dic2[dateItem].Add(itemTotal2);
                        dic2[dateItem].Add(itemTotal3);
                    }
                    return Ok(dic2);
                }
            }
            return NotFound();
        }


        public IActionResult ReportStatisticDetailCall()
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.ReportDaily,
                Title = SubMenuTitle.ReportOverview,
                SubTitle = SubMenuTitle.ReportStatisticOverviewCall,
                SubTitleSmal = SubMenuTitle.ReportStatisticDetailCall,
                Toolbar = 1,
            };

            ViewData[SubHeaderData] = subHeader;
            ViewBag.Customers = GetListCustomers();
            ViewBag.TaskState = Utilities.ExtensionMethods.ToSelectList<EnumTaskState>();
            return View();
        }

        public IActionResult GetReportStatisticDetailCall(ReportStatisticDetailCallRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.ReportStatisticDetailCall.Gets;

            if (request.limit == 0)
            {
                request.limit = 20;
            }

            var obj = _reportStatisticDetailCallService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }

        public IActionResult GetReportStatisticDetailCallAway(ReportStatisticDetailCallRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.ReportStatisticDetailCall.GetCallAway;

            if (request.limit == 0)
            {
                request.limit = 20;
            }

            var obj = _reportStatisticDetailCallService.GetCallAway(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }

        public IActionResult ReportStatisticDetail()
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.ReportDaily,
                Title = SubMenuTitle.ReportOverview,
                SubTitle = SubMenuTitle.ReportStatisticDetail,
                Toolbar = 1
            };

            ViewData[SubHeaderData] = subHeader;
            ViewBag.province = GetLocation();
            ViewBag.Customers = GetListCustomers();
            ViewBag.Fields = GetListFields();
            ViewBag.FilterTeam = GetDepartmentUuid();
            return View();
        }

        public IActionResult GetReportStatisticDetailProject(ReportStatisticDetailRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.ReportStatisticDetails.Gets;

            if (request.limit == 0)
            {
                request.limit = 20;
            }

            request.viewDetail = 0;

            var obj = _reportStatisticDetailService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }

        public IActionResult GetReportStatisticDetailProjectTab2(ReportStatisticDetailTab2Request request)
        {
            var header = Header;
            header.Action = CustomConfigApi.ReportStatisticDetails.Gets;

            if (request.limit == 0)
            {
                request.limit = 20;
            }

            request.viewDetail = 1;

            var obj = _reportStatisticDetailService.GetTab2(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }

        public IActionResult GetReportStatisticDetailArea(ReportStatisticDetailAreaRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.ReportStatisticDetails.GetList;

            if (request.limit == 0)
            {
                request.limit = 20;
            }


            var obj = _reportStatisticDetailService.GetList(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }

        public IActionResult ReportStatisticDetailStatusDevice()
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.ReportDaily,
                Title = SubMenuTitle.ReportOverview,
                SubTitle = SubMenuTitle.ReportStatisticOverviewDeviceAndSupplies,
                SubTitleSmal = SubMenuTitle.ReportStatisticDetailStatusDevice,
                Toolbar = 1,
            };

            ViewData[SubHeaderData] = subHeader;
            ViewBag.Customers = GetListCustomers();
            ViewBag.TaskState = Utilities.ExtensionMethods.ToSelectList<EnumTaskState>();
            return View();
        }

        public IActionResult GetReportStatisticDetailStatusDevice(ReportStatisticDetailStatusDeviceRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.ReportStatisticDetailStatusDevice.Gets;

            if (request.limit == 0)
            {
                request.limit = 20;
            }

            var obj = _reportStatisticDetailStatusDeviceService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }

        public IActionResult ReportStatisticDetailTask()
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.ReportDaily,
                Title = SubMenuTitle.ReportOverview,
                SubTitle = SubMenuTitle.ReportStatisticOverviewTask,
                SubTitleSmal = SubMenuTitle.ReportStatisticDetailTask,
                Toolbar = 1,
            };

            ViewData[SubHeaderData] = subHeader;
            ViewBag.Customers = GetListCustomers();
            ViewBag.FilterTeam = GetTeamUuid();
            ViewBag.Staff = GetStaff();
            ViewBag.TaskState = Utilities.ExtensionMethods.ToSelectList<EnumReportTaskState>();
            return View();
        }

        public IActionResult GetReportStatisticDetailTaskTab1(ReportStatisticDetailTaskRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.ReportStatisticDetailTask.Gets;

            if (request.limit == 0)
            {
                request.limit = 20;
            }

            request.type = 1;

            var obj = _reportStatisticDetailTaskService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }

        public IActionResult GetReportStatisticDetailTaskTab2(ReportStatisticDetailTaskTab2Request request)
        {
            var header = Header;
            header.Action = CustomConfigApi.ReportStatisticDetailTask.Gets;

            if (request.limit == 0)
            {
                request.limit = 20;
            }

            request.type = 2;

            var obj = _reportStatisticDetailTaskService.GetTab2(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }

        public IActionResult GetReportStatisticDetailTaskTab3(ReportStatisticDetailTaskTab3Request request)
        {
            var header = Header;
            header.Action = CustomConfigApi.ReportStatisticDetailTaskTab3.Gets;

            if (request.limit == 0)
            {
                request.limit = 20;
            }

            //request.type = 0;

            var obj = _reportStatisticDetailTaskService.GetTab3(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }

        public IActionResult ReportStatisticTroubleshoot()
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.ReportDaily,
                Title = SubMenuTitle.ReportOverview,
                SubTitle = SubMenuTitle.ReportStatisticTroubleshoot,
                Toolbar = 1,
            };

            ViewData[SubHeaderData] = subHeader;
            ViewBag.Customers = GetListCustomers();
            ViewBag.TaskState = Utilities.ExtensionMethods.ToSelectList<EnumReportTicketState>();
            return View();
        }

        public IActionResult GetReportStatisticTroubleshoot(ReportStatisticTroubleshootRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.ReportStatisticTroubleshoot.Gets;

            if (request.limit == 0)
            {
                request.limit = 20;
            }

            var obj = _reportStatisticTroubleshootService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }

        public IActionResult ReportStatisticDetailSuppliesTask()
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.ReportDaily,
                Title = SubMenuTitle.ReportOverview,
                SubTitle = SubMenuTitle.ReportStatisticOverviewDeviceAndSupplies,
                SubTitleSmal = SubMenuTitle.ReportStatisticDetailSuppliesTask,
                Toolbar = 1,
            };

            ViewData[SubHeaderData] = subHeader;
            return View();
        }

        public IActionResult GetReportStatisticDetailSuppliesTaskTab1(ReportStatisticDetailSuppliesTaskRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.ReportStatisticDetailSuppliesTask.Gets;

            if (request.limit == 0)
            {
                request.limit = 20;
            }

            var obj = _reportStatisticDetailSuppliesTaskService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }

        public IActionResult GetReportStatisticDetailSuppliesTaskTab2(ReportStatisticDetailSuppliesTaskRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.ReportStatisticDetailSuppliesTask.GetTab2;

            if (request.limit == 0)
            {
                request.limit = 20;
            }

            var obj = _reportStatisticDetailSuppliesTaskService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }

        public IActionResult ReportStatisticDetailStatusContract()
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.ReportDaily,
                Title = SubMenuTitle.ReportOverview,
                SubTitle = SubMenuTitle.ReportStatisticDetailStatusContract,
                Toolbar = 1,
            };

            ViewData[SubHeaderData] = subHeader;
            ViewBag.province = GetLocation();
            ViewBag.Fields = GetListFields();
            ViewBag.TaskState = Utilities.ExtensionMethods.ToSelectList<EnumTaskState>();
            return View();
        }

        public IActionResult GetReportStatisticDetailStatusContract(ReportStatisticDetailStatusContractRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.ReportStatisticDetailStatusContract.Gets;

            if (request.limit == 0)
            {
                request.limit = 20;
            }

            var obj = _reportStatisticDetailStatusContractService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }

        private List<CustomerModel> GetListCustomers()
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.GetList;

            var request = new CustomerRequest
            {
                limit = 1000,
            };

            var obj = _customerService.GetList(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return obj.data.items;
            }

            return null;
        }

        private List<LocationModel> GetLocation()
        {
            var header = Header;
            header.Action = CustomConfigApi.Location.Provinces;
            var request = new LocationRequest
            {
                limit = -1,
            };
            var obj = _locationService.GetsTree(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }

        private List<ManageFieldModel> GetListFields()
        {
            var header = Header;
            header.Action = CustomConfigApi.Field.Get;

            var request = new ManageFieldRequest
            {
                limit = 1000,
            };

            var obj = _manageFieldService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return obj.data.items;
            }

            return null;
        }

        public List<ProjectModel> GetTeamUuid()
        {
            var request = new TeamProjectRequest
            {
                type = 2
            };
            var header = Header;
            header.Action = CustomConfigApi.Project.GetTeam;
            var obj = _projectService.GetTeam(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }

        public List<ProjectModel> GetDepartmentUuid()
        {
            var request = new TeamProjectRequest
            {
                type = 1
            };
            var header = Header;
            header.Action = CustomConfigApi.Project.GetTeam;
            var obj = _projectService.GetTeam(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }

        private List<StaffModel> GetStaff()
        {
            var header = Header;
            header.Action = CustomConfigApi.Staff.GetsList;

            var request = new StaffListRequest
            {
                limit = 1000,
            };

            var obj = _staffService.GetList(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return obj.data.items;
            }

            return null;
        }

        /* public void test()
         {
             var lst = new List<dynamic>();
             var dic = new Dictionary<string, List<object>>();

             lst.ForEach(t =>
             {
                 var date = t["date"];
                 var obj = new
                 {
                     type = t["type"],
                     count = t["count"]
                 };
                 if (!dic.ContainsKey(date))
                     dic.Add(date, new List<object>());
                 dic[date].Add(obj); 
             });
         }*/
    }
}
