﻿using DMMS.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace DMMS.Controllers
{
    public class ErrorController : Controller
    {
        [Route("/error")]
        public IActionResult Index()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}