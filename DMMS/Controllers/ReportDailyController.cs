﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class ReportDailyController : BaseController<ReportDailyController>
    {
        private readonly IReportDailyService _reportDailyService;
        private readonly ITaskService _taskService;
        private readonly IStaffService _staffService;
        private readonly ICustomerService _customerService;
        public ReportDailyController(IReportDailyService reportdaily, ITaskService task, IStaffService staff, ICustomerService customer)
        {
            _taskService = task;
            _reportDailyService = reportdaily;
            _staffService = staff;
            _customerService = customer;

        }
        public IActionResult gets(ReportDailyRequest request)
        {
            if (request.date_report1 != null)
            {
                DateTime dateTime = DateTime.ParseExact(request.date_report1, "dd/MM/yyyy", null);
                TimeSpan epoch = (dateTime - new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc));
                request.date_report = epoch.TotalSeconds;
            }

            if(request.check == 0)
            {
                request.is_manager = true;
            }
            else
            {
                request.is_manager = false;

            }
            var header = Header;
            header.Action = CustomConfigApi.ReportDailys.Gets;
            if (request.limit == 0)
            {
                request.limit = 10;
            }
            var obj = _reportDailyService.Get(header, request);

            return Ok(obj);
        }

        [Route("/get-calendar")]
        public IActionResult getCalendar(ReportDailyRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.ReportDailys.Gets;
            if (request.limit == 0)
            {
                request.limit = 100;
            }
            var result = new List<CalendarModel>();
            var obj = _reportDailyService.Get(header, request);
            if(obj!=null && (EnumError)obj.error == EnumError.Success && obj.data!=null && obj.data.items != null)
            {
                result = obj.data.items.Select(x => new CalendarModel
                {
                    id = x.task.uuid,
                    title = x.task.name,
                    description = x.task.description,
                    startDate = x.date_report,
                    endDate = x.task.deadline,
                    className = x.state == (int)EnumReportState.Denined ? "fc-event-solid-danger fc-event-light " : (x.state== (int)EnumReportState.Approved ? "fc-event-light fc-event-solid-primary" : "fc-event-danger fc-event-solid-warning")
                }).ToList();

                return Ok(result);
            }

            return NotFound();

        }

        public IActionResult Index(ReportDailyModel model)
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.ReportDaily,
                Title = MenuTitle.Report,
                SubTitle = SubMenuTitle.DailyReport,
            };
            ViewData[SubHeaderData] = subHeader;
            //ViewBag.task = GetTasks();
            ViewBag.staff = GetStaffs();
            ViewBag.state = Utilities.ExtensionMethods.ToSelectList<EnumReportState>();
            return View(model);
        }
        public IActionResult Create()
        {
            var task = GetTasks();
            ViewBag.task = task;
            var customer = GetCustomers();
            ViewBag.customer = customer;
            return View();
        }
        [HttpPost]
        public IActionResult Create(ReportDailyAdd model)
        {

            var task = GetTasks();
            ViewBag.task = task;
            var customer = GetCustomers();
            ViewBag.customer = customer;
            var objReturn = new ResponseData<ReportDailyModel>();
            var header = Header;
            header.Action = CustomConfigApi.ReportDailys.Upsert;
            var objAdd = _reportDailyService.Upsert(header, model);
            if (objAdd.error == 1)
            {
                objReturn.msg = string.IsNullOrEmpty(objAdd.msg) ? Constants.MESSAGE_ERROR : objAdd.msg;
            }
            else
            {
                return Ok(objReturn);
            }
            return View(model);
        }
        public IActionResult Edit(string id)
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.ReportDaily,
                Title = MenuTitle.Report,
                SubTitle = SubMenuTitle.DailyReport,
            };
            ViewData[SubHeaderData] = subHeader;
            var model = new ReportDailyAdd();
         
                var header = Header;
                header.Action = CustomConfigApi.ReportDailys.Detail;
                var request = new ReportDailyDetailRequest
                {
                    uuid = id,
                };
                var obj = _reportDailyService.Detail(header, request);
                if (obj.error == 0 && obj.data != null)
                {
                    var objDetail = obj.data;
                model = new ReportDailyAdd
                {
                    reviewer_name = objDetail.reviewer != null ? objDetail.reviewer.name : "Chưa rõ",
                    staff_name = objDetail.staff.name != null ? objDetail.staff.name : "Chưa rõ",
                    content = objDetail.content,
                    time_end = objDetail.time_end,
                    totaltime = objDetail.end != "" ? objDetail.start + " - " + objDetail.end : objDetail.start + "Chưa có thời gian kết thúc",
                    date = objDetail.datereport,
                    image = objDetail.image,
                    taskmodel = objDetail.task != null ? objDetail.task.name : "Không có công việc",
                    customermodel = objDetail.customer != null ? objDetail.customer.name : "Không có khách hàng",

                };
                subHeader.SubTitleSmal = objDetail.staff_name;
                return View(model);
            }
            return NotFound();
            
        }
        [HttpPut]
        public IActionResult Edit(string uuid, ReportDailyAdd model)
        {
            var objReturn = new ResponseData<ReportDailyDataResponse>();
            var header = Header;
            header.Action = CustomConfigApi.ReportDailys.Upsert;
            if (string.IsNullOrEmpty(model.uuid))
            {
                objReturn.error = 1;
                objReturn.msg = "Lỗi không có id";
                return Ok(objReturn);
            }
            var request = new ReportDailyAdd
            {
                uuid = uuid,
                task_uuid = model.task_uuid,
                customer_uuid = model.customer_uuid,
                content = model.content,
                time_end = model.time_end,
                time_start = model.time_start,
                date = model.datereport,
            };
            var obj = _reportDailyService.Upsert(header, request);
            if (obj.error == 0)
            {
                return Ok(obj);
            }
            else
            {
                objReturn.msg = string.IsNullOrEmpty(obj.msg) ? Constants.MESSAGE_ERROR : obj.msg;
                return Ok(objReturn);
            }
        }

        public IActionResult Details(string id)
        {
            ReportDailyDataResponse detail = new ReportDailyDataResponse();
            var header = Header;
            header.Action = CustomConfigApi.ReportDailys.Detail;
            var request = new ReportDailyDetailRequest
            {
                uuid = id,
            };
            var obj = _reportDailyService.Detail(header, request);
            {
                detail = obj.data;
            };
            return View(detail);
        }
        private List<StaffModel> GetStaffs()
        {
            var header = Header;
            header.Action = CustomConfigApi.Staff.GetsListByTeamLead;
            var request = new StaffListRequest
            {
                limit = 1000,
            };
            var obj = _staffService.GetList(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        private List<TaskModel> GetTasks()
        {
            var header = Header;
            header.Action = CustomConfigApi.Task.Gets;
            var request = new TaskRequest
            {
                limit = 1000,
            };
            var obj = _taskService.Get(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        private List<CustomerModel> GetCustomers()
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.GetList;
            var request = new CustomerRequest
            {
                limit = 1000,
            };
            var obj = _customerService.Get(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        public IActionResult FullCalendar()
        {
            return View();
        }
    }
}