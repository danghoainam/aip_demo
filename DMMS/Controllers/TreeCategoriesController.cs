﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using OfficeOpenXml.FormulaParsing.Utilities;
using OfficeOpenXml.Utils;

namespace DMMS.Controllers
{
    public class TreeCategoriesController : BaseController<TreeCategoriesController>
    {
        private readonly ITreeCategoriesService _treeCategoriesService;
        private readonly IManageFiledService _manageFiledService;

        private readonly ITreeCategoryService _category;

        public TreeCategoriesController(ITreeCategoriesService treeCategoriesService, IManageFiledService manageFiledService, ITreeCategoryService category)
        {
            _treeCategoriesService = treeCategoriesService;
            _manageFiledService = manageFiledService;
            _category = category;
        }


        public IActionResult gets(TreeCategoriesRequest request)
        {

            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.Get;
            var obj = _treeCategoriesService.Get(header, request);
            List<InitTreeCategories> createtree = new List<InitTreeCategories>();
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {

                for (var i = 0; i < obj.data.items.Count; i++)
                {
                    var inittree = new InitTreeCategories()
                    {
                        text = obj.data.items[i].name,
                        type = obj.data.items[i].id,
                    };
                    if (obj.data.items[i].id == 1)
                    {

                        inittree.id = 100;
                    }
                    else if (obj.data.items[i].id == 2)
                    {
                        inittree.id = 101;
                    }
                    else if (obj.data.items[i].id == 3)
                    {
                        inittree.id = 102;
                    }
                    else if (obj.data.items[i].id == 4)
                    {
                        inittree.id = 103;
                    }
                    if (obj.data.items[i].id == 3)
                    {
                        inittree.icon = "/statics/images/dungcu.png";

                    }
                    else if (obj.data.items[i].id == 2)
                    {
                        inittree.icon = "/statics/images/vattu.png";

                    }
                    else if (obj.data.items[i].id == 1)
                    {
                        inittree.icon = "/statics/images/thietbi.png";
                    }
                    else if (obj.data.items[i].id == 4)
                    {
                        inittree.icon = "/statics/images/phanmem.png";
                    }
                    createtree.Add(inittree);
                }
                return Ok(createtree);
            }

            return NotFound(obj);
        }

        [Route("treecategories/getlist/{categoryOrderUuid}")]
        public IActionResult getList(TreeCategoriesSearchRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.GetList;

            var obj = _treeCategoriesService.GetList(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        public async Task<IActionResult> SearchTree(BaseRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.Search;
            var obj = _treeCategoriesService.Search(header, request);
            List<InitTreeCategoriesSearch> inttreesearch = new List<InitTreeCategoriesSearch>();
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                for (var i = 0; i < obj.data.items.Count; i++)
                {
                    var intree = new InitTreeCategoriesSearch
                    {
                        id = obj.data.items[i].id,
                        text = obj.data.items[i].name,
                        parent = obj.data.items[i].parent_id,
                        type = obj.data.items[i].type,
                        is_leaf = obj.data.items[i].is_leaf,
                        parent_id = obj.data.items[i].parent_id,

                    };
                    intree.is_search = 1;
                    if (obj.data.items[i].type == 1)
                    {
                        intree.icon = "/statics/images/branddevice.png";
                    }
                    else if (obj.data.items[i].type == 2)
                    {
                        intree.icon = "/statics/images/vattu.png";
                    }
                    else if (obj.data.items[i].type == 3)
                    {
                        intree.icon = "/statics/images/dungcu.png";
                    }
                    foreach (var cat in obj?.data?.items)
                    {
                        _category[cat.id] = cat;


                    }


                    inttreesearch.Add(intree);


                }
                List<TreeCategoriesModel> test = new List<TreeCategoriesModel>();
                if (obj.data.pagination.pages > 1)
                {
                    for (var k = 1; k < obj.data.pagination.pages; k++)
                    {
                        var test1 = GetLstSearch(request.limit, ++request.page, request.keyword);
                        test.AddRange(test1);
                    }
                }
                for (var i = 0; i < test.Count; i++)
                {
                    var init = new InitTreeCategoriesSearch
                    {
                        id = test[i].id,
                        text = test[i].name,
                        parent = test[i].parent_id,
                        type = test[i].type,
                        is_leaf = test[i].is_leaf,
                        parent_id = test[i].parent_id,

                    };
                    if (test[i].type == 1)
                    {
                        init.icon = "/statics/images/branddevice.png";
                    }
                    else if (test[i].type == 2)
                    {
                        init.icon = "/statics/images/vattu.png";
                    }
                    else if (test[i].type == 3)
                    {
                        init.icon = "/statics/images/dungcu.png";
                    }
                    if (test[i].is_leaf == 1 && test[i].type == 1)
                    {
                        init.icon = "/statics/images/leafdevice.png";
                    }
                    else if (test[i].is_leaf == 1 && test[i].type == 2)
                    {
                        init.icon = "/statics/images/leafsuplies.png";
                    }
                    else if (test[i].is_leaf == 1 && test[i].type == 3)
                    {
                        init.icon = "/statics/images/leaftools.png";
                    }
                    inttreesearch.Add(init);
                }
                return Ok(inttreesearch);
            }
            return NotFound();
        }
        public IActionResult SubCategories(TreeCategoriesRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.Subtree;
            var obj = _treeCategoriesService.Get(header, request);
            List<InitTreeCategories> subcategories = new List<InitTreeCategories>();
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                for (var i = 0; i < obj.data.items.Count; i++)
                {
                    var initbrank = new InitTreeCategories
                    {
                        id = obj.data.items[i].id,
                        text = obj.data.items[i].name,
                        type = obj.data.items[i].type,
                        prefix_name = obj.data.items[i].prefix_name,
                        uuid = obj.data.items[i].uuid,
                        is_leaf = obj.data.items[i].is_leaf,
                        page = obj.data.pagination.page,
                        pages = obj.data.pagination.pages
                    };
                    if (obj.data.items[i].type == 1)
                    {
                        initbrank.icon = "/statics/images/branddevice.png";
                    }
                    if (obj.data.items[i].type == 2)
                    {
                        initbrank.icon = "/statics/images/vattu.png";
                    }
                    if (obj.data.items[i].type == 3)
                    {
                        initbrank.icon = "/statics/images/dungcu.png";
                    }
                    if (obj.data.items[i].parent_id != null)
                    {
                        initbrank.parent_id = obj.data.items[i].parent_id;
                    }
                    if (obj.data.items[i].is_leaf == 1 && obj.data.items[i].type == 1)
                    {
                        initbrank.icon = "/statics/images/leafdevice.png";
                    }
                    else if (obj.data.items[i].is_leaf == 1 && obj.data.items[i].type == 2)
                    {
                        initbrank.icon = "/statics/images/leafsuplies.png";
                    }
                    else if (obj.data.items[i].is_leaf == 1 && obj.data.items[i].type == 3)
                    {
                        initbrank.icon = "/statics/images/leaftools.png";
                    }
                    subcategories.Add(initbrank);

                }

                foreach (var cat in obj?.data?.items)
                {
                    _category[cat.id] = cat;


                }
                return Ok(subcategories);

            }
            return NotFound(obj);
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult SearchLeaf(CategoreSearchLeaf request)
        {
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.SearchLeaf;
            var obj = _treeCategoriesService.SearchLeaf(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        public IActionResult CreateDevice(ManageFieldRequest request)
        {
            request.limit = -1;
            var header = Header;
            header.Action = CustomConfigApi.Field.Get;
            var obj = _manageFiledService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                ViewBag.Field = obj.data.items;
                return View();
            }
            return NotFound();
        }
        [HttpPost]
        public IActionResult CreateDevice(TreeCategoriesAddRequest request)
        {
            if (request.detail != null)
            {
                if (request.detail.exp_price_ref_string != null)
                {
                    request.detail.exp_price_ref = long.Parse(request.detail.exp_price_ref_string.Replace(",", ""));
                }
                if (request.detail.imp_price_ref_string != null)
                {
                    request.detail.imp_price_ref = long.Parse(request.detail.imp_price_ref_string.Replace(",", ""));
                }
            }
            if (request.type == null)
            {
                request.type = 1;
            }
            if (request.is_leaf == 1)
            {
                request.detail.model = request.name;
            }
            request.is_enable = 1;
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.Create;
            var obj = _treeCategoriesService.Add(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            else
            {
                return NotFound();
            }


        }
        public IActionResult CreateSupplies(ManageFieldRequest request)
        {
            request.limit = -1;
            var header = Header;
            header.Action = CustomConfigApi.Field.Get;
            var obj = _manageFiledService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                ViewBag.Field = obj.data.items;
                return View();
            }
            return NotFound();
        }
        [HttpPost]
        public IActionResult CreateSupplies(TreeCategoriesAddRequest request)
        {
            if (request.detail != null)
            {
                if (request.detail.exp_price_ref_string != null)
                {
                    request.detail.exp_price_ref = long.Parse(request.detail.exp_price_ref_string.Replace(",", ""));
                }
                if (request.detail.imp_price_ref_string != null)
                {
                    request.detail.imp_price_ref = long.Parse(request.detail.imp_price_ref_string.Replace(",", ""));
                }
            }
            request.is_enable = 1;
            if (request.type == null)
            {
                request.type = 2;
            }
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.Create;
            var obj = _treeCategoriesService.Add(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        public IActionResult CreateTools(ManageFieldRequest request)
        {
            request.limit = -1;
            var header = Header;
            header.Action = CustomConfigApi.Field.Get;
            var obj = _manageFiledService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                ViewBag.Field = obj.data.items;
                return View();
            }
            return NotFound();
        }
        [HttpPost]
        public IActionResult CreateTools(TreeCategoriesAddRequest request)
        {
            if (request.detail != null)
            {
                if (request.detail.exp_price_ref_string != null)
                {
                    request.detail.exp_price_ref = long.Parse(request.detail.exp_price_ref_string.Replace(",", ""));
                }
                if (request.detail.imp_price_ref_string != null)
                {
                    request.detail.imp_price_ref = long.Parse(request.detail.imp_price_ref_string.Replace(",", ""));
                }
            }
            request.is_enable = 1;
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.Create;
            var obj = _treeCategoriesService.Add(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        public IActionResult EditDevice(TreeCategoriesDetail request)
        {
            TreeCategoriesDetailRespose model = new TreeCategoriesDetailRespose();

            List<ManageFieldModel> test = GetField();
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.Detail;
            var obj = _treeCategoriesService.Detail(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                model.name = obj.data.name;
                model.parent_id = obj.data.parent_id;
                model.is_leaf = obj.data.is_leaf;
                model.type = obj.data.type;
                //model.detail = obj.data.detail;
                model.fields = obj.data.fields;
                model.parent_id = obj.data.parent_id;
                model.id = obj.data.id;
                model.description = obj.data.description;
                if (obj.data.is_leaf == 1)
                {
                    model.detail = obj.data.detail;
                    model.detail.exp_price_ref_string = obj.data.exp_price_ref_dis;
                    model.detail.imp_price_ref_string = obj.data.imp_price_ref_dis;
                }
                foreach (var lst in test)
                {
                    foreach (var check in obj.data.fields)
                    {
                        if (lst.id == check)
                        {
                            lst.IsCheck = "Checked";
                        }
                    }
                }
            }
            ViewBag.Field = test;
            return View(model);
        }
        [HttpPut]
        public IActionResult EditDevice(TreeCategoriesDetailRespose model)
        {
            if (model.detail.exp_price_ref_string != null)
            {
                model.detail.exp_price_ref = Convert.ToInt64(model.detail.exp_price_ref_string.Replace(".", "").Replace("₫", "").Replace(",", "").Trim());
            }
            if (model.detail.imp_price_ref_string != null)
            {
                model.detail.imp_price_ref = Convert.ToInt64(model.detail.imp_price_ref_string.Replace(".", "").Replace("₫", "").Replace(",", "").Trim());
            }
            var request = new TreeCategoriesEditRequest
            {
                id = model.id,
                name = model.name,
                fields = model.fields,
                description = model.description
            };
            if (model.is_leaf == 0)
            {
                request.detail = null;
            }
            else
            {
                request.detail = model.detail;
            }
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.Edit;
            var obj = _treeCategoriesService.Update(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        public IActionResult EditSupplies(TreeCategoriesDetail request)
        {
            TreeCategoriesDetailRespose model = new TreeCategoriesDetailRespose();
            List<ManageFieldModel> test = GetField();
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.Detail;
            var obj = _treeCategoriesService.Detail(header, request);
            model.name = obj.data.name;
            model.parent_id = obj.data.parent_id;
            model.is_leaf = obj.data.is_leaf;
            model.type = obj.data.type;
            //model.detail = obj.data.detail;
            model.fields = obj.data.fields;
            model.parent_id = obj.data.parent_id;
            model.id = obj.data.id;
            if (obj.data.is_leaf == 1)
            {
                model.detail = obj.data.detail;
                model.detail.exp_price_ref_string = obj.data.exp_price_ref_dis;
                model.detail.imp_price_ref_string = obj.data.imp_price_ref_dis;
            }
            foreach (var lst in test)
            {
                foreach (var check in obj.data.fields)
                {
                    if (lst.id == check)
                    {
                        lst.IsCheck = "Checked";
                    }
                }
            }
            ViewBag.Field = test;
            return View(model);
        }
        [HttpPut]
        public IActionResult EditSupplies(TreeCategoriesDetailRespose model)
        {
            TreeCategoriesEditRequest request = new TreeCategoriesEditRequest
            {
                id = model.id,
                name = model.name,
                fields = model.fields,
            };
            if (model.is_leaf == 1)
            {
                request.detail = model.detail;
            }
            else if (model.is_leaf == 0)
            {
                request.detail = null;
            }


            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.Edit;
            var obj = _treeCategoriesService.Update(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();

        }
        public IActionResult EditTools(TreeCategoriesDetail request)
        {
            List<ManageFieldModel> test = GetField();
            TreeCategoriesDetailRespose model = new TreeCategoriesDetailRespose();
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.Detail;
            var obj = _treeCategoriesService.Detail(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                model.name = obj.data.name;
                model.parent_id = obj.data.parent_id;
                model.is_leaf = obj.data.is_leaf;
                model.type = obj.data.type;
                //model.detail = obj.data.detail;
                model.fields = obj.data.fields;
                model.parent_id = obj.data.parent_id;
                model.id = obj.data.id;
            }
            if (obj.data.is_leaf == 1)
            {
                model.detail = obj.data.detail;
                model.detail.exp_price_ref_string = obj.data.exp_price_ref_dis;
                model.detail.imp_price_ref_string = obj.data.imp_price_ref_dis;


            }
            foreach (var lst in test)
            {
                foreach (var check in obj.data.fields)
                {
                    if (lst.id == check)
                    {
                        lst.IsCheck = "Checked";
                    }
                }
            }
            ViewBag.Field = test;
            return View(model);
        }
        [HttpPut]
        public IActionResult EditTools(TreeCategoriesDetailRespose model)
        {
            TreeCategoriesEditRequest request = new TreeCategoriesEditRequest()
            {
                id = model.id,
                name = model.name,
                fields = model.fields,
            };
            if (model.is_leaf == 1)
            {
                request.detail = model.detail;
            }
            else if (model.is_leaf == 0)
            {
                request.detail = null;
            }
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.Edit;
            var obj = _treeCategoriesService.Update(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        [HttpDelete]
        public IActionResult Delete(TreeCategoriesDelete request)
        {
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.Delete;
            var obj = _treeCategoriesService.Delete(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {

                return Ok(obj);
            }
            return NotFound();
        }

        [Route("/treecategories/getidsubtree/{id}")]
        public IActionResult getidsubtree(int id)
        {
            var list = new List<Tuple<int?, string>>();
            GetRelationship(list, id);
            return Ok(list);
        }
        public IActionResult CheckName(CategoriesCheckName request)
        {
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.CheckName;
            var obj = _treeCategoriesService.CheckName(header, request);
            return Ok(obj);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lst"></param>
        /// <param name="id"></param>
        public void GetRelationship(List<Tuple<int?, string>> lst, int? id)
        {
            var parentId = _category[id].parent_id;
            if (_category[id] != null)
            {
                lst.Add(Tuple.Create(_category[id].id, _category[id].name));
            }
            if (parentId.HasValue)
            {
                GetRelationship(lst, parentId);
            }
        }
        public List<TreeCategoriesModel> GetLstSearch(int limit, int page, string keyword)
        {
            BaseRequest request = new BaseRequest()
            {
                limit = limit,
                page = page,
                keyword = keyword,
            };
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.Search;
            var obj = _treeCategoriesService.Search(header, request);
            return obj.data.items;
        }
        private List<ManageFieldModel> GetField()
        {
            var request = new ManageFieldRequest();
            request.limit = -1;
            var header = Header;
            header.Action = CustomConfigApi.Field.Get;
            var obj = _manageFiledService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {

                return obj.data.items;
            }
            return null;
        }

    }
}