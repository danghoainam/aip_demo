﻿
using System.Diagnostics;

using Microsoft.AspNetCore.Mvc;

using DMMS.Models;
using Microsoft.AspNetCore.Authorization;
using DMMS.Utilities;
using Microsoft.AspNetCore.Http;
using System;
using Microsoft.Extensions.Logging;

namespace DMMS.Controllers
{
    
    public class HomeController : BaseController<HomeController>
    {
        private readonly ILogger _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        [Route("/set-theme")]
        public IActionResult SetThems(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                name = "white";
            }
            CookieOptions cookieOptions = new CookieOptions();
            cookieOptions.Expires = DateTime.Now.AddMonths(1);
            Response.Cookies.Append("theme", name, cookieOptions);
            return Ok();
        }
        public IActionResult IndexAsync()
        {
            return View();
        }

        [Route("/")]
        public IActionResult Dashboard()
        {
            _logger.LogInformation("Start : Getting item details for {ID}");
            // Start tranfer data Subheader
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Dashboard,
                Title = MenuTitle.Dashboard,
                Toolbar = 1
            };

            ViewData[SubHeaderData] = subHeader;
            // End

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [AllowAnonymous]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
