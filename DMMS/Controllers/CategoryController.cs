﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DMMS.Extensions;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;

using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class CategoryController : BaseController<CategoryController>
    {
        private readonly ICategoryService _categoryService;
        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }


        #region: Page
        [CustomController(parentName: "", parentController: "Category", controllerName: "Category", name: MenuTitle.Category,
                           actionName: "Index", method: "", route: "/category/index", isPage: true, enumAction: EnumAction.View, isShow: true, icon: Icon.Category)]
        public IActionResult Index()
        {
            // Start tranfer data Subheader
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Device,
                Title = MenuTitle.Device,
                SubTitle = SubMenuTitle.Category
            };

            ViewData[SubHeaderData] = subHeader;

            return View();
        }

        [CustomController(parentName: MenuTitle.Category, parentController: "Category", controllerName: "Category", name: "Thêm mới loại thiết bị",
                            actionName: "Create", method: "", route: "/category/create", isPage: false, enumAction: EnumAction.View, isShow: false)]
        public IActionResult Create()
        {
            var model = new CategoryUpsertRequest();
            ViewBag.Category = GetsTree();

            return View(model);
        }

        [CustomController(parentName: MenuTitle.Category, parentController: "Category", controllerName: "Category", name: "Cập nhật loại thiết bị", actionName: "Edit",
                            method: "", route: "/category/edit", isPage: true, enumAction: EnumAction.View, isShow: false)]
        public IActionResult Edit(int? id)
        {
            var model = new CategoryUpsertRequest();
            ViewBag.Category = GetsTree();

            if (id == null)
            {
                return View(model);
            }
            else
            {
                var header = Header;
                header.Action = CustomConfigApi.Category.Detail;

                var request = new CategoryDetailRequest
                {
                    id = id
                };

                var obj = _categoryService.Detail(header, request);

                if (obj.data != null && obj.error == 0)
                {
                    model = new CategoryUpsertRequest
                    {
                        id = obj.data.id,
                        name = obj.data.name,
                        parent_id = obj.data.parent_id,
                        description = obj.data.description
                    };

                    return View(model);
                }

                return RedirectToAction("index", "category");
            }
        }

        #endregion

        #region: Ajax
        [CustomController(parentName: MenuTitle.Category, parentController: "Category", controllerName: "Category", name: "Get danh sách thiết bị",
                   actionName: "gets", method: "GET", route: "/category/gets", isPage: false, enumAction: EnumAction.View, isShow: false, icon: "")]
        [HttpGet("/category/gets")]
        public IActionResult gets(CategoryRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Category.Gets;
            var obj = _categoryService.Gets(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(new
                {
                    data = obj.data.items
                });
            }
            return NotFound();
        }
        public IActionResult getstreepackage(CategoryRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Category.GetsTree;
            var obj = _categoryService.Gets(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(new
                {
                    data = obj.data.items
                });
            }
            return NotFound();
        }
        [CustomController(parentName: MenuTitle.Category, parentController: "Category", controllerName: "Category", name: "Thêm mới loại thiết bị", actionName: "Create",
                            method: "POST", route: "/category/create", isPage: false, enumAction: EnumAction.Add, isShow: false)]
        [HttpPost("/category/create")]
        public IActionResult Create(CategoryUpsertRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Category.Upsert;

            var obj = _categoryService.Upsert(header, model);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.code == 0)
            {
                return Ok(obj);
            }
            else
            {
                return NotFound(obj);
            }

        }

        [CustomController(parentName: MenuTitle.Category, parentController: "Category", controllerName: "Category", name: "Cập nhật loại thiết bị", actionName: "Edit",
                            method: "PUT", route: "/category/edit", isPage: false, enumAction: EnumAction.Edit, isShow: false)]
        [HttpPut("/category/edit")]
        public IActionResult Edit(CategoryUpsertRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Category.Upsert;

            var request = new CategoryUpsertRequest
            {
                id = model.id,
                parent_id = model.parent_id,
                name = model.name,
                description = model.description,
            };

            var obj = _categoryService.Upsert(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.code == 0)
            {
                return Ok(obj);
            }
            else
            {
                return NotFound(obj);
            }
        }

        [CustomController(parentName: MenuTitle.Category, parentController: "Category", controllerName: "Category", name: "Chi tiết loại thiết bị", actionName: "Details",
                                    method: "GET", route: "/category/detail", isPage: false, enumAction: EnumAction.View, isShow: false)]
        [HttpGet]
        public IActionResult Detail(int? id)
        {
            var model = new CategoryDataResponse();

            var header = Header;
            header.Action = CustomConfigApi.Category.Detail;

            var request = new CategoryDetailRequest
            {
                id = id
            };

            var obj = _categoryService.Detail(header, request);
            if (obj.data != null && obj.error == 0)
            {
                model = obj.data;

                return View(model);
            }
            else
            {
                return NotFound();
            }

        }

        [CustomController(parentName: MenuTitle.Category, parentController: "Category", controllerName: "Category", name: "Xóa loại thiết bị", actionName: "Delete",
                            method: "DELETE", route: "/category/delete", isPage: false, enumAction: EnumAction.Delete, isShow: false)]
        [DisplayName("Xóa loại thiết bị")]
        [HttpDelete]
        public IActionResult Delete(int id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Ticket.Delete;

            var request = new CategoryDetailRequest
            {
                id = id
            };

            var obj = _categoryService.Delete(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }

        private List<CategoryModel> GetsTree()
        {
            var header = Header;
            header.Action = CustomConfigApi.Category.GetsTree;

            var obj = _categoryService.Gets(header, null);
            if (obj.data != null && obj.error == 0)
            {
                return obj.data.items;
            }

            return null;
        }
        #endregion

    }
}