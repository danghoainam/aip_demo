﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Extensions;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class TaskController : BaseController<TaskController>
    {
        private readonly ITaskService _taskService;
        private readonly ITicketService _ticketService;
        private readonly IStaffService _staffService;
        private readonly ICustomerService _customerService;

        public TaskController(ITaskService taskService, ITicketService ticketService, IStaffService staffService,
            ICustomerService customerService)
        {
            _taskService = taskService;
            _ticketService = ticketService;
            _staffService = staffService;
            _customerService = customerService;
        }


        [CustomController(parentName: MenuTitle.Task, parentController: "Task", controllerName: "Task", name: "Get danh sách công việc",
                   actionName: "gets", method: "GET", route: "/task/gets", isPage: false,
                   enumAction: EnumAction.View, isShow: false, icon: "")]
        public IActionResult gets(TaskRequest request, string state)
        {
            if (!string.IsNullOrEmpty(state))
            {
                request.stateIds = state;
            }
            var header = Header;
            
            header.Action = CustomConfigApi.Task.Gets;
            if (request.limit == 0)
            {
                request.limit = 10;
            }
            var obj = _taskService.Get(header, request);
            if(obj!=null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }

        [CustomController(parentName: MenuTitle.Task, parentController: "Task", controllerName: "Task", name: "Get danh sách công việc của nhân viên",
                    actionName: "gettask", method: "GET", route: "/task/gettask/{staff_uuid}", isPage: false,
                    enumAction: EnumAction.View, isShow: false, icon: "")]
        [Route("/task/gettask/{staff_uuid}")]
        public IActionResult gettask(TaskRequest request)
        {
            var header = Header;

            header.Action = CustomConfigApi.Task.Gets;
            if (request.limit == 0)
            {
                request.limit = 10;
            }
            var obj = _taskService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }

        [CustomController(parentName: "", parentController: "Task", controllerName: "Task", name: MenuTitle.Task,
                    actionName: "Index", method: "", route: "/task/index", isPage: true,
                    enumAction: EnumAction.View, isShow: true, icon: Icon.Task)]
        public IActionResult Index(TaskRequest model, string state)
        {            
                // Start tranfer data Subheader
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Ticket,
                Title = MenuTitle.Task
            };
            ViewData[SubHeaderData] = subHeader;
            if (!string.IsNullOrEmpty(state))
            {
                model.stateIds = state;
            }
            // End
            ViewBag.Ticket = GetTicket();
            ViewBag.Staff = GetStaff();
            ViewBag.TaskState = Utilities.ExtensionMethods.ToSelectList<EnumTaskState>();
            ViewBag.TaskPriority = Utilities.ExtensionMethods.ToSelectList<EnumTicketPriority>();

            return View(model);
        }

        [CustomController(parentName: MenuTitle.Task, parentController: "Task", controllerName: "Task", name: "Thêm công việc mới",
                    actionName: "Upsert", method: "", route: "/task/upsert", isPage: true,
                    enumAction: EnumAction.View, isShow: false, icon: "")]
        [Route("/task/upsert")]
        [Route("/task/upsert/{taskUuid}")]
        [Route("/task/upsert/{ticketUuid}/{taskUuid}")]
        public IActionResult Upsert(string taskUuid, string ticketUuid)
        {
            var model = new TaskModel();
            ViewBag.Customer = GetCustomer();
            ViewBag.TaskState = Utilities.ExtensionMethods.ToSelectList<EnumTaskState>();
            ViewBag.TaskPriority = Utilities.ExtensionMethods.ToSelectList<EnumTicketPriority>();
            ViewBag.Staff = GetStaff();
            if (string.IsNullOrEmpty(ticketUuid) && string.IsNullOrEmpty(taskUuid))
            {
                // Tao moi
            }
            else
            {
                // Update task
                if (string.IsNullOrEmpty(ticketUuid))
                {
                    var header = Header;
                    header.Action = CustomConfigApi.Task.Detail;
                    var request = new TaskRequest
                    {
                        uuid = taskUuid
                    };

                    var obj = _taskService.Get(header, request);
                    if(obj != null && (EnumError)obj.error == EnumError.Success)
                    {
                        model = new TaskModel
                        {
                            uuid = taskUuid,
                            deadline = obj.data.deadline,
                            created_at = obj.data.created_at,
                            description = obj.data.description,
                            complete_at = obj.data.complete_at,
                            name = obj.data.name,
                            staff = obj.data.staff,
                            state = obj.data.state,
                            code = string.IsNullOrEmpty(obj.data.code) ? obj.data.uuid : obj.data.code,
                            state_update_at = obj.data.state_update_at,
                            closed_at = obj.data.closed_at,
                            id = obj.data.id,
                            type = obj.data.type,
                            priority = obj.data.priority,
                        };
                    }
                }
                //update task & ticket
                else
                {
                    var header = Header;
                    header.Action = CustomConfigApi.Task.Detail;
                    var request = new TaskRequest
                    {
                        uuid = taskUuid
                    };

                    var obj = _taskService.Get(header, request);
                    if (obj != null && (EnumError)obj.error == EnumError.Success)
                    {
                        model = new TaskModel
                        {
                            uuid = taskUuid,
                            deadline = obj.data.deadline,
                            created_at = obj.data.created_at,
                            description = obj.data.description,
                            complete_at = obj.data.complete_at,
                            name = obj.data.name,
                            staff = obj.data.staff,
                            state = obj.data.state,
                            code = string.IsNullOrEmpty(obj.data.code) ? obj.data.uuid : obj.data.code,
                            state_update_at = obj.data.state_update_at,
                            closed_at = obj.data.closed_at,
                            ticket = obj.data.ticket,
                            id = obj.data.id,
                            type = obj.data.type,
                        };

                    }
                }
            }
            return View(model);
        }

        [CustomController(parentName: MenuTitle.Task, parentController: "Task", controllerName: "Task", name: "Thêm công việc mới",
                    actionName: "Upsert", method: "POST", route: "/task/create", isPage: false,
                    enumAction: EnumAction.Add, isShow: false, icon: "")]
        [HttpPost("/task/create")]
        public IActionResult Upsert(string uuid, TaskAddRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Task.Upsert;

            var obj = _taskService.Upsert(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }

        [CustomController(parentName: MenuTitle.Task, parentController: "Task", controllerName: "Task", name: "Cập nhật công việc",
                    actionName: "Update", method: "", route: "/task/update", isPage: true,
                    enumAction: EnumAction.View, isShow: true, icon: Icon.Task)]
        public IActionResult Update (string id)
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Ticket,
                Title = MenuTitle.Task,
                
            };
            ViewData[SubHeaderData] = subHeader;
            var model = new TaskModel();
            ViewBag.TaskState = Utilities.ExtensionMethods.ToSelectList<EnumTaskState>();
            ViewBag.TaskPriority = Utilities.ExtensionMethods.ToSelectList<EnumTicketPriority>();
            //ViewBag.Staff = GetStaff();
                // Update task
                
                    var header = Header;
                    header.Action = CustomConfigApi.Task.Detail;
                    var request = new TaskRequest
                    {
                        uuid = id
                    };
                    
                    var obj = _taskService.Get(header, request);
                     subHeader.SubTitleSmal = "Tiêu đề: "+ obj.data.name;
                    if (obj != null && (EnumError)obj.error == EnumError.Success)
                    {
                model = new TaskModel
                {
                    uuid = id,
                    deadline = obj.data.deadline,
                    created_at = obj.data.created_at,
                    description = obj.data.description,
                    complete_at = obj.data.complete_at,
                    name = obj.data.name,
                    //staff_uuid = obj.data.staff.uuid,
                    state = obj.data.state,
                    code = obj.data.code,
                    state_update_at = obj.data.state_update_at,
                    closed_at = obj.data.closed_at,
                    id = obj.data.id,
                    type = obj.data.type,
                    employees = obj.data.employees,
                    starttime = obj.data.start_estimate_format,
                    finishtime = obj.data.finish_estimate_format,
                    start_estimate = obj.data.start_estimate,
                    finish_estimate = obj.data.finish_estimate,
                    priority = obj.data.priority,
                    staffs = obj.data.staffs,
                    time_created_display = obj.data.time_created_format,
                    cost = obj.data.cost,
                    note_plan = obj.data.note_plan,
                        };
                    }
                
                //update task & ticket
               
            
            return View(model);
        }

        [CustomController(parentName: MenuTitle.Task, parentController: "Task", controllerName: "Task", name: "Cập nhật công việc",
                    actionName: "Update", method: "PUT", route: "/task/update", isPage: false,
                    enumAction: EnumAction.Edit, isShow: false, icon: "")]
        [HttpPut("/task/update")]
        public IActionResult Update(string uuid, TaskAddRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Task.Upsert;

            //request.start_estimate = DateTime.Parse(request.starttime);
            //request.finish_estimate = DateTime.Parse(request.finishtime);

            var obj = _taskService.Upsert(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        [CustomController(parentName: MenuTitle.Task, parentController: "Task", controllerName: "Task", name: "Chi tiết công việc",
                    actionName: "detail", method: "", route: "/task/detail", isPage: true,
                    enumAction: EnumAction.View, isShow: false, icon: "")]
        public IActionResult detail(string id)
        {
            ViewBag.TaskState = Utilities.ExtensionMethods.ToSelectList<EnumTaskState>();
            ViewBag.TaskPriority = Utilities.ExtensionMethods.ToSelectList<EnumTicketPriority>().ToList();
            var model = new TaskModel();
            var request = new TaskDetailRequest
            {
                uuid = id,
            };
            var header = Header;
            header.Action = CustomConfigApi.Task.Detail;
            var obj = _taskService.Detail(header, request);
            if((EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                model = obj.data;
                return View(model);
            }
            return View(model);
        }
        //[Route("/task/upsert/{ticketUuid}")]
        //[Route("/task/upsert/{ticketUuid}/{taskUuid}")]
        //public IActionResult Upsert(string taskUuid, string ticketUuid)
        //{
        //    var model = new TaskModel();
        //    ViewBag.TaskState = ExtensionMethods.ToSelectList<EnumTaskState>();
        //    ViewBag.Staff = GetStaff();
        //    if (!string.IsNullOrEmpty(ticketUuid) && string.IsNullOrEmpty(taskUuid))
        //    {
        //        // Tao moi
        //    }
        //    else
        //    {
        //        // Update
        //        if (string.IsNullOrEmpty(taskUuid))
        //        {
        //            return View(model);
        //        }
        //        else
        //        {
        //            var header = Header;
        //            header.Action = CustomConfigApi.Task.Detail;
        //            var request = new TaskRequest
        //            {
        //                uuid = taskUuid
        //            };

        //            var obj = _taskService.Get(header, request);
        //            if (obj != null && (EnumError)obj.error == EnumError.Success)
        //            {
        //                model = new TaskModel
        //                {
        //                    uuid = taskUuid,
        //                    deadline = obj.data.deadline,
        //                    created_at = obj.data.created_at,
        //                    description = obj.data.description,
        //                    complete_at = obj.data.complete_at,
        //                    name = obj.data.name,
        //                    staff = obj.data.staff,
        //                    state = obj.data.state,
        //                    code = string.IsNullOrEmpty(obj.data.code) ? obj.data.uuid : obj.data.code,
        //                    state_update_at = obj.data.state_update_at,
        //                    closed_at = obj.data.closed_at,
        //                    ticket = obj.data.ticket,
        //                    id = obj.data.id,
        //                    type = obj.data.type
        //                };

        //            }
        //        }
        //    }
        //    return View(model);
        //}

        //[DisplayName("Xóa yêu công việc")]
        [CustomController(parentName: MenuTitle.Task, parentController: "Task", controllerName: "Task", name: "Xóa công việc",
                    actionName: "Delete", method: "DELETE", route: "/task/delete", isPage: false,
                    enumAction: EnumAction.Delete, isShow: false, icon: "")]
        [HttpDelete]
        public IActionResult Delete(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Task.Delete;

            var request = new TaskDetailRequest
            {
                uuid = id
            };

            var obj = _taskService.Delete(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }

        public IActionResult ChangeState(string id)
        {
            var model = new ChangeStateRequest() 
            { 
                uuid = id,
            };

            ViewBag.States = Utilities.ExtensionMethods.ToSelectList<EnumTaskState>();

            return View(model);
        }

        [HttpPost]
        public IActionResult ChangeState(ChangeStateRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Task.ChangeState;

            //if (!string.IsNullOrEmpty(model.deadline_str))
            //{
            //    long epochTicks = new DateTime(1970, 1, 1).Ticks;
            //    model.deadline = ((DateTime.Parse(model.deadline_str).Ticks - epochTicks) / TimeSpan.TicksPerSecond);
            //}

            long epochTicks = new DateTime(1970, 1, 1).Ticks;

            var request = new ChangeStateRequest()
            {
                uuid = model.uuid,
                note = model.note,
                offer = model.offer,
                state = model.state,
                deadline = !string.IsNullOrEmpty(model.deadline_str) ? 
                                        model.deadline = ((DateTime.Parse(model.deadline_str).Ticks - epochTicks) / TimeSpan.TicksPerSecond) : null,
                images = !string.IsNullOrEmpty(model.image_uuids) ?
                                        model.image_uuids.Split(",".ToCharArray()).ToList().Select(s => int.Parse(s)).ToList() : null
            };

            var obj = _taskService.ChangeState(header, request);
            if((EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        //public IActionResult Details(string id)
        //{
        //    TaskAddDataResponse abc = new TaskAddDataResponse();


        //    var header = Header;
        //    header.Action = CustomConfigApi.Customers.Details;

        //    var request = new CustomerDetailRequest
        //    {

        //        uuid = id,

        //    };
        //    var obj = _taskService.Detail(header, request);
        //    {
        //        abc = obj.data;

        //    };
        //    return View(abc);
        //}

        private List<TicketModel> GetTicket()
        {
            var header = Header;
            header.Action = CustomConfigApi.Ticket.Gets;

            var request = new TicketRequest
            {
                limit = 1000,
            };

            var obj = _ticketService.Gets(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return obj.data.items;
            }

            return null;
        }

        private List<StaffModel> GetStaff()
        {
            var header = Header;
            header.Action = CustomConfigApi.Staff.GetsList;

            var request = new StaffListRequest
            {
                limit = 1000,
            };

            var obj = _staffService.GetList(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return obj.data.items;
            }

            return null;
        }

        private List<CustomerModel> GetCustomer()
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.GetList;

            var obj = _customerService.GetList(header, null);

            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return obj.data.items;
            }

            return null;
        }


    }
}