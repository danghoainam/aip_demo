﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Extensions;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;

namespace DMMS.Controllers
{

    public class AccountController : BaseController<AccountController>
    {

        private readonly IAccountService _accountService;
        private readonly IStaffService _staffService;
        public AccountController(IAccountService accountService, ISipAccountService sipAccountService,IStaffService staffService)
        {
            _accountService = accountService;
            _staffService = staffService;
        }
        //[ActionName("Thông tin tài khoản")]
        public IActionResult Index()
        {
            var model = new ProfilerModel();
            var subHeader = new SubHeaderModel
            {
                Icon = "flaticon2-user",
                Title = "Tài khoản",
                SubTitle = "Thông tin tài khoản"
            };

            ViewData["SubHeader"] = subHeader;

            var header = new RequestHeader
            {
                Authorization = (ClaimsIdentity != null && ClaimsIdentity.Claims != null && ClaimsIdentity.Claims.Count() > 0) ? ClaimsIdentity.FindFirst(Constants.TOKEN).Value : string.Empty,
                ContentType = "application/json",
                Method = "POST",
                Action = "accounts/profile"
            };
            var obj = _accountService.Profile(header);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null && obj.data.user_info != null)
            {
                var accountModel = new AccountModel
                {
                    address = obj.data.user_info.address,
                    email = obj.data.user_info.email,
                    name = obj.data.user_info.name,
                    phone_number = obj.data.user_info.phone_number,
                    status = obj.data.user_info.status,
                    username = obj.data.username,
                    id = obj.data.id,
                    uuid = obj.data.user_info.uuid,
                };

                model.Account = accountModel;
            }

            return View(model);
        }


        [HttpPut]
        public IActionResult UpdateInfo(ProfilerModel model)
        {
            //var header = Header;
            //header.Action = CustomConfigApi.Staff.Edit;
            //var request = new StaffAddRequest()
            //{

            //}
            //var obj = _staffService.Edit(header, model);
            return NotFound();
        }

        public IActionResult Profile()
        {
            var model = new ProfilerModel();
            var header = Header;
            header.Action = CustomConfigApi.Account.Profile;
            var obj = _accountService.Profile(header);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null && obj.data.user_info != null)
            {
                model.Account = new AccountModel
                {
                    address = obj.data.user_info.address,
                    email = obj.data.user_info.email,
                    name = obj.data.user_info.name,
                    phone_number = obj.data.user_info.phone_number,
                    status = obj.data.user_info.status,
                    username = obj.data.username,
                    id = obj.data.id,
                    uuid = obj.data.user_info.uuid
                };


            }
            return View(model);
        }

        public IActionResult ChangePassword()
        {
            var subHeader = new SubHeaderModel
            {
                Icon = "flaticon2-user",
                Title = "Tài khoản",
                SubTitle = "Thay đổi mật khẩu"
            };

            ViewData["SubHeader"] = subHeader;
            var model = new AccountChangePasswordModel();
            return View(model);
        }

        //[DisplayName("Thay đổi mật khẩu")]
        [HttpPut("/account/change-pass")]
        [CustomController(parentName: MenuTitle.Account, parentController: "Account", controllerName: "Account", name: "Thay đổi mật khẩu",
            actionName: "ChangePassword", method: "PUT", route: "account/change-pass",
            isPage: false, enumAction: EnumAction.View, isShow: false)]
        public IActionResult ChangePassword(ProfilerModel model)
        {
            if (model.ChangePassword.verify_new_password.Equals(model.ChangePassword.new_password))
            {
                var header = Header;
                header.Action = CustomConfigApi.Account.ChangePassword;

                var request = new AccountChangePasswordModel
                {
                    new_password = model.ChangePassword.new_password,
                    old_password = model.ChangePassword.old_password,
                    verify_new_password = model.ChangePassword.verify_new_password
                };
                var obj = _accountService.ChangePassword(header, request);
                if (obj != null && (EnumError)obj.error == EnumError.Success)
                {
                    return Ok(obj);
                }
                else
                {
                    return NotFound(obj);
                }
            }
            else
            {
                return NotFound(new
                {
                    error = 1,
                    msg = "Mật khẩu nhập lại không đúng"
                });
            }
        }

        [DisplayName("Đặt lại mật khẩu")]
        [CustomController(parentName: MenuTitle.Account, parentController: "Account", controllerName: "Account", name: "Đặt lại mật khẩu",
            actionName: "ResetPassword", method: "POST", route: "/account/resetpassword/{id}",
            isPage: false, enumAction: EnumAction.Edit, isShow: false)]
        [HttpPost("/account/resetpassword/{id}")]
        public IActionResult ResetPassword(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Account.Reset;

            var request = new ResetPasswordRequest
            {
                uuid = id
            };

            var obj = _accountService.ResetPassword(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }

        public IActionResult Info()
        {
            var model = new AccountModel();
            var header = Header;
            header.Action = CustomConfigApi.Account.Profile;
            var obj = _accountService.Profile(header);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null && obj.data.user_info != null)
            {
                model = new AccountModel
                {
                    address = obj.data.user_info.address,
                    email = obj.data.user_info.email,
                    name = obj.data.user_info.name,
                    phone_number = obj.data.user_info.phone_number,
                    status = obj.data.user_info.status,
                    username = obj.data.username,
                    id = obj.data.id,
                    uuid = obj.data.user_info.uuid
                };
            }
            return View(model);
        }

        //[DisplayName("Thông tin tài khoản")]
        //[CustomController("", "Account", MenuTitle.Account, controllerName: "Account", "Personal", "", "account/personal", true, EnumAction.View, true, icon: Icon.Account)]
        [Extensions.CustomController(parentName: "", parentController: "Account", controllerName: "Account", name: MenuTitle.Account,
                            actionName: "Index", method: "", route: "/account/personal", isPage: true,
                            enumAction: EnumAction.View, isShow: true, icon: "kt-menu__link-icon flaticon-responsive")]
        public IActionResult Personal()
        {
            // Start tranfer data Subheader
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Account,
                Title = MenuTitle.Account,
                SubTitle = SubMenuTitle.AccountInfo
            };

            ViewData[SubHeaderData] = subHeader;
            // End

            var model = new ProfilerModel();
            var header = Header;
            header.Action = CustomConfigApi.Account.Profile;
            var obj = _accountService.Profile(header);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null && obj.data.user_info != null)
            {
                model.Account = new AccountModel
                {
                    address = obj.data.user_info.address,
                    email = obj.data.user_info.email,
                    name = obj.data.user_info.name,
                    phone_number = obj.data.user_info.phone_number,
                    status = obj.data.user_info.status,
                    username = obj.data.username,
                    id = obj.data.id,
                    uuid = obj.data.user_info.uuid,
                    image = obj.data.user_info.image
                };
                if (obj.data.user_info.image != null && !string.IsNullOrEmpty(obj.data.user_info.image.path))
                {
                    obj.data.user_info.image.path = CustomConfigApi.ImageDomain + obj.data.user_info.image.path;
                }
            }
            return View(model);
        }

    }
}