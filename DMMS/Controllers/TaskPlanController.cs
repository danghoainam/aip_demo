﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Extensions;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class TaskPlanController : BaseController<TaskPlanController>
    {
        private readonly IProjectService _projectService;
        private readonly ITaskPlanService _taskPlanService;
        private readonly ICustomerService _customerService;
        private readonly IStaffService _staffService;
        private readonly IAccountService _accountService;
        private readonly IProjectCustomerService _projectCustomerService;
        public TaskPlanController(IProjectService projectService, ITaskPlanService taskPlanService, ICustomerService customerService,
            IStaffService staffService, IAccountService accountService, IProjectCustomerService projectCustomerService)
        {
            _projectService = projectService;
            _taskPlanService = taskPlanService;
            _customerService = customerService;
            _staffService = staffService;
            _accountService = accountService;
            _projectCustomerService = projectCustomerService;
        }

        //[DisplayName("Danh sách kế hoạch công việc")]
        [CustomController(parentName: MenuTitle.TaskPlan, parentController: "TaskPlan", controllerName: "TaskPlan", name: "Get danh sách kế hoạch công việc",
                    actionName: "gets", method: "GET", route: "/taskplan/gets", isPage: false,
                    enumAction: EnumAction.View, isShow: false, icon: "")]
        public IActionResult gets(TaskPlanRequest model, string filterStatus)
        {
            if (!string.IsNullOrEmpty(filterStatus))
            {
                model.stateIds = filterStatus;
            }

            var header = Header;
            header.Action = CustomConfigApi.TaskPlan.Get;

            if (model.limit == 0)
            {
                model.limit = 20;
            }

            var obj = _taskPlanService.Get(header, model);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        [CustomController(parentName: MenuTitle.TaskPlan, parentController: "TaskPlan", controllerName: "TaskPlan", name: "Get danh sách khách hàng kế hoạch công việc",
            actionName: "getCustomers", method: "GET", route: "/taskplan/getCustomers/{plan_uuid}", isPage: false,
            enumAction: EnumAction.View, isShow: false, icon: "")]
        [Route("taskplan/getCustomers/{plan_uuid}")]
        public IActionResult getCustomers(TaskPlanDetailCustomerRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.TaskPlan.DetailCustomer;

            if (model.limit == 0)
            {
                model.limit = 20;
            }

            var obj = _taskPlanService.DetailCustomer(header, model);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        [CustomController(parentName: MenuTitle.TaskPlan, parentController: "TaskPlan", controllerName: "TaskPlan", name: "Get danh sách công việc của kế hoạch công việc",
            actionName: "gets", method: "GET", route: "/taskplan/getTasks/{plan_uuid}", isPage: false,
            enumAction: EnumAction.View, isShow: false, icon: "")]
        [Route("taskplan/getTasks/{plan_uuid}")]
        public IActionResult getTasks(TaskPlanDetailTaskRequest model, string filterStatusTask)
        {
            if (!string.IsNullOrEmpty(filterStatusTask))
            {
                model.taskStateIds = filterStatusTask;
            }

            var header = Header;
            header.Action = CustomConfigApi.TaskPlan.DetailTask;

            if (model.limit == 0)
            {
                model.limit = 20;
            }

            var obj = _taskPlanService.DetailTask(header, model);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        //[CustomController(parentName: MenuTitle.TaskPlan, parentController: "TaskPlan", controllerName: "TaskPlan", name: "Get nhân viên theo công việc",
        //            actionName: "getstaff", method: "GET", route: "/taskplan/getstaff/{task_uuid}", isPage: false,
        //            enumAction: EnumAction.View, isShow: false, icon: "")]
        //[Route("/taskplan/getstaff/{task_uuid}")]
        //public IActionResult getstaff(string task_uuid)
        //{
        //    var header = Header;
        //    header.Action = CustomConfigApi.Task.Detail;

        //    var request = new TaskDetailRequest
        //    {
        //        uuid = task_uuid,
        //    };

        //    var obj = _taskService.Detail(header, request);
        //    if(obj != null && (EnumError)obj.error == EnumError.Success)
        //    {
        //        return Ok(obj.data.employees);
        //    }
        //    return NotFound(obj.data.employees);
        //}

        //[DisplayName("Danh sách kế hoạch công việc")]
        [CustomController(parentName: "", parentController: "TaskPlan", controllerName: "TaskPlan", name: MenuTitle.TaskPlan,
                    actionName: "Index", method: "", route: "/taskplan/index", isPage: true,
                    enumAction: EnumAction.View, isShow: true, icon: Icon.TaskPlan)]
        public IActionResult Index(TaskPlanRequest model, string filterStatus)
        {
            if (!string.IsNullOrEmpty(filterStatus))
            {
                model.stateIds = filterStatus;
            }

            var subHeader = new SubHeaderModel
            {
                Icon = Icon.TaskPlan,
                Title = MenuTitle.TaskPlan
            };
            ViewData[SubHeaderData] = subHeader;

            ViewBag.TaskPlanStates = Utilities.ExtensionMethods.ToSelectList<EnumTaskPlanState>();

            return View(model);
        }

        //[DisplayName("Thêm mới kế hoạch công việc")]
        [CustomController(parentName: MenuTitle.TaskPlan, parentController: "TaskPlan", controllerName: "TaskPlan", name: "Thêm kế hoạch mới",
                    actionName: "Create", method: "", route: "/taskplan/create", isPage: true,
                    enumAction: EnumAction.View, isShow: false, icon: "")]
        public IActionResult Create()
        {
            var model = new TaskPlanUpsertRequest();

            ViewBag.StaffRole = Utilities.ExtensionMethods.ToSelectList<EnumStaffRole>();
            //ViewBag.Customers = GetCustomer();
            ViewBag.Projects = GetProjects();
            //ViewBag.Staffs = GetStaff();
            var acc = GetInfo();
            model.creatorUuid = acc.uuid;

            return View(model);
        }

        //[DisplayName("Action: Thêm mới kế hoạch công việc")]
        [CustomController(parentName: MenuTitle.TaskPlan, parentController: "TaskPlan", controllerName: "TaskPlan", name: "Thêm kế hoạch mới",
                    actionName: "Create", method: "POST", route: "/taskplan/create", isPage: false,
                    enumAction: EnumAction.Add, isShow: false, icon: "")]
        [HttpPost]
        public IActionResult Create(TaskPlanUpsertRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.TaskPlan.Add;

            if (model.customerUuids.Count > 0 && model.customerUuids[0] == null)
            {
                model.customerUuids.RemoveAt(0);
            }

            if (!string.IsNullOrEmpty(model.enddate) && !string.IsNullOrEmpty(model.startdate))
            {
                model.startTime = DateTime.ParseExact(model.startdate, "dd/MM/yyyy", null);
                model.finishTime = DateTime.ParseExact(model.enddate, "dd/MM/yyyy", null);
            }

            //Debug.WriteLine("Display cost_string: {0}", long.Parse(model.costEstimate_string.Replace(",", "")));
            //Debug.WriteLine("Display starttime: {0}", model.startTime);
            //Debug.WriteLine("Display finshtime: {0}", model.finishTime);

            var request = new TaskPlanUpsertRequest()
            {
                creatorUuid = model.creatorUuid,
                costEstimate = long.Parse(model.costEstimate_string.Replace(",", "")),
                customerUuids = model.customerUuids,
                description = model.description,
                name = model.name,
                //staffUuids = model.staffUuids,
                projectUuid = model.projectUuid,
                finishTime = model.finishTime,
                startTime = model.startTime,
            };

            var obj = _taskPlanService.Upsert(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            else
            {
                return NotFound(obj);
            }

        }

        //[DisplayName("Chi tiết hoạch công việc")]
        [CustomController(parentName: MenuTitle.TaskPlan, parentController: "TaskPlan", controllerName: "TaskPlan", name: "Chi tiết kế hoạch",
                    actionName: "Detail", method: "", route: "/taskplan/detail", isPage: true,
                    enumAction: EnumAction.View, isShow: true, icon: Icon.TaskPlan)]
        public IActionResult Detail(string id)
        {
            ViewBag.TaskStates = Utilities.ExtensionMethods.ToSelectList<EnumTaskState>();

            var model = new TaskPlanDetailModel();

            var header = Header;
            header.Action = CustomConfigApi.TaskPlan.Detail;

            var request = new TaskPlanDetailRequest()
            {
                plan_uuid = id,
            };

            var obj = _taskPlanService.Detail(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                model = new TaskPlanDetailModel
                {
                    uuid = obj.data.item.uuid,
                    name = obj.data.item.name,
                    projectName = obj.data.item.projectName,
                    costEstimate = obj.data.item.costEstimate,
                    finishTime = obj.data.item.finishTime,
                    startTime = obj.data.item.startTime,
                    description = obj.data.item.description,
                    startdate = obj.data.item.startTime_format,
                    enddate = obj.data.item.finishTime_format,
                    reviewAt_display = obj.data.item.reviewAt_fomat,
                    reviewId = obj.data.item.reviewId,
                    reviewName = obj.data.item.reviewName,
                    reviewNote = obj.data.item.reviewNote,
                    state = obj.data.item.state,
                    cost_update = obj.data.item.costEstimate_format,
                    projectUuid = obj.data.item.projectUuid,
                    teamUuid = obj.data.item.teamUuid,

                };
                if (obj.data.item.customers != null)
                {
                    model.customers_format = new List<string>();
                    foreach (var i in obj.data.item.customers)
                    {
                        model.customers_format.Add(i.uuid);
                    }

                }

                ViewBag.GetTeamLeaders = model.teamUuid != null ? GetTeamLeader(model.teamUuid) : null;
                ViewBag.Customer = TaskGetCustomer(model.projectUuid);
            }

            var subHeader = new SubHeaderModel
            {
                Icon = Icon.TaskPlan,
                Title = MenuTitle.TaskPlan,
                SubTitle = model.name,
            };
            ViewData[SubHeaderData] = subHeader;

            return View(model);
        }
        [HttpPut]
        [Route("TaskPlan/Detail/{uuid}")]
        public IActionResult Detail(string uuid, TaskPlanDetailModel model)
        {
            var acc = GetInfo();
            if (model.customers_format != null && model.customers_format.Count > 0)
            {
                model.customers_format.RemoveAll(t => { return t == null; });
            }
            var request = new TaskPlanUpsertRequest();
            if (model.cost_update != null)
            {
                model.cost_update = model.cost_update.Replace(",", "").Replace("₫", "").Replace(".", "").Trim();
                request.costEstimate = long.Parse(model.cost_update);
            }
            if (!string.IsNullOrEmpty(model.enddate) && !string.IsNullOrEmpty(model.startdate))
            {
                request.startTime = DateTime.ParseExact(model.startdate, "dd/MM/yyyy", null);
                request.finishTime = DateTime.ParseExact(model.enddate, "dd/MM/yyyy", null);
            }
            request.description = model.description;
            request.name = model.name;
            request.state = model.state;
            request.planUuid = uuid;
            request.customers = model.customers_format;
            request.reviewNote = model.reviewNote;
            request.reviewUuid = acc.uuid;
            request.responserUuid = model.responserUuid;
            var header = Header;
            header.Action = CustomConfigApi.TaskPlan.Upsert;
            var obj = _taskPlanService.Upsert(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        //[DisplayName("Action: Sửa kế hoạch công việc")]
        //[CustomController(parentName: MenuTitle.TaskPlan, parentController: "TaskPlan", controllerName: "TaskPlan", name: "Cập nhật kế hoạch",
        //            actionName: "Update", method: "PUT", route: "/taskplan/update", isPage: false,
        //            enumAction: EnumAction.Edit, isShow: false, icon: "")]
        //[HttpPut]
        //public IActionResult Update(TaskPlanUpsertRequest model)
        //{
        //    var header = Header;
        //    header.Action = CustomConfigApi.TaskPlan.Upsert;

        //    var request = new TaskPlanUpsertRequest
        //    {
        //        uuid = model.uuid,
        //        costEstimate = model.costEstimate,
        //        creatorUuid = model.creatorUuid,
        //        customerUuids = model.customerUuids,
        //        description = model.description,
        //        name = model.name,
        //        staffUuids = model.staffUuids,
        //        finishTime = model.finishTime,
        //        startTime = model.startTime
        //    };
        //    var obj = _taskPlanService.Upsert(header, request);
        //    if(obj != null && (EnumError)obj.error == EnumError.Success)
        //    {
        //        return Ok(obj);
        //    }
        //    return NotFound(obj);
        //}

        [CustomController(parentName: MenuTitle.TaskPlan, parentController: "TaskPlan", controllerName: "TaskPlan", name: "Xóa kế hoạch công việc",
                    actionName: "Delete", method: "DELETE", route: "/taskplan/delete", isPage: false,
                    enumAction: EnumAction.Delete, isShow: false, icon: "")]
        [HttpDelete]
        public IActionResult Delete(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.TaskPlan.Delete;

            var request = new TaskPlanDetailRequest
            {
                plan_uuid = id
            };

            var obj = _taskPlanService.Delete(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [CustomController(parentName: MenuTitle.TaskPlan, parentController: "TaskPlan", controllerName: "TaskPlan", name: "Duyệt kế hoạch công việc",
            actionName: "Decide", method: "POST", route: "/taskplan/decide", isPage: false,
            enumAction: EnumAction.Edit, isShow: false, icon: "")]
        [HttpPost]
        public IActionResult Decide(string id, string reviewNote, bool accept)
        {
            var acc = GetInfo();
            var header = Header;
            header.Action = CustomConfigApi.TaskPlan.Decide;

            var request = new TaskPlanDecideRequest
            {
                reviewUuid = acc.uuid,
                plan_uuid = id,
                reviewNote = reviewNote,
                accept = accept
            };

            var obj = _taskPlanService.Decide(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        [HttpPost]
        [Route("TaskPlan/Deploy/{uuid}")]
        public IActionResult Deploy(string uuid, TaskPlanDetailModel model)
        {
            var acc = GetInfo();
            var header = Header;
            header.Action = CustomConfigApi.TaskPlan.Reported;

            var request = new TaskPlanDeployRequest
            {
                reviewUuid = acc.uuid,
                planUuid = uuid,
                reviewNote = model.reviewNote,
            };

            var obj = _taskPlanService.Deploy(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        private List<CustomerModel> GetCustomer()
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.GetList;

            var obj = _customerService.GetList(header, null);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return obj.data.items;
            }

            return new List<CustomerModel>();
        }


        //public IActionResult getStaffAjax(CustomerRequest request)
        //{
        //    var objReturn = new List<CustomerModel>();

        //    var header = Header;
        //    header.Action = CustomConfigApi.Customers.Gets;

        //    request.limit = 100;

        //    var obj = _customerService.Get(header, request);
        //    if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
        //    {
        //        objReturn = obj.data.items.Select(i => new CustomerModel
        //        {
        //            uuid = i.uuid,
        //            name = i.name_display,
        //        }).ToList();
        //    }

        //    return Ok(objReturn);
        //}

        private List<StaffModel> GetStaff()
        {
            var header = Header;
            header.Action = CustomConfigApi.Staff.GetsList;

            var request = new StaffListRequest
            {
                limit = 1000
            };

            var obj = _staffService.GetList(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return obj.data.items;
            }

            return new List<StaffModel>();
        }

        private List<ProjectModel> GetProjects()
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.Gets;

            var request = new ProjectSearchRequest
            {
                limit = 1000
            };

            var obj = _projectService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return obj.data.items;
            }

            return new List<ProjectModel>();
        }

        //Lấy thông tin tài khoản đăng nhập
        private AccountModel GetInfo()
        {
            var model = new AccountModel();
            var header = Header;
            header.Action = CustomConfigApi.Account.Profile;

            var obj = _accountService.Profile(header);
            if ((EnumError)obj.error == EnumError.Success)
            {
                model = obj.data.user_info;
                return model;
            }
            return model;
        }
        private List<ProjectCustomerModel> TaskGetCustomer(string uuid)
        {
            ProjectGetCustomer request = new ProjectGetCustomer();
            request.limit = -1;
            request.projectuuid = uuid;
            var header = Header;
            header.Action = CustomConfigApi.Project.ProjectCustomer;
            var obj = _projectCustomerService.GetCustomer(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                //obj.data.items[0].cu   
                return obj.data.items;
            }
            return null;
        }
        [CustomController(parentName: MenuTitle.TaskPlan, parentController: "TaskPlan", controllerName: "TaskPlan", name: "Get danh sách nhân viên",
                    actionName: "getStaffAjax", method: "GET", route: "/taskplan/getStaffAjax", isPage: false,
                    enumAction: EnumAction.View, isShow: false, icon: "")]
        public IActionResult getStaffAjax(StaffListRequest request)
        {
            var objReturn = new List<StaffModel>();

            var header = Header;
            header.Action = CustomConfigApi.Staff.GetsList;
            request.limit = 1000;

            var obj = _staffService.GetList(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                objReturn = obj.data.items.Select(i => new StaffModel
                {
                    uuid = i.uuid,
                    name = i.name,
                }).ToList();
            }

            return Ok(objReturn);
        }

        private List<Staff_Team> GetTeamLeader(string teamUuid)
        {
            var entries = new List<Staff_Team>();
            var header = Header;
            header.Action = CustomConfigApi.Staff.GetTeamLeaders;

            var request = new TeamLeaderRequest()
            {
                limit = -1,
                teamUuid = teamUuid,
            };

            var obj = _staffService.GetTeamLeaders(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                entries = obj.data.items;
                return entries;
            }
            return entries;
        }
    }
}