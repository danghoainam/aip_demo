﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Extensions;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DMMS.Controllers
{
    public class TimeWarningController : BaseController<TimeWarningController>
    {
        private readonly ITimeWarningService _timeWarningService;
        public TimeWarningController(ITimeWarningService timeWarningService)
        {
            _timeWarningService = timeWarningService;
        }

        //[CustomController(MenuTitle.TimeWarning, "Timewarning", "Get danh sách thời gian cảnh báo", "gets", "GET", "timewarning/gets", false, EnumAction.View, false)]
        //[DisplayName("Action: Danh sách thời gian cảnh báo")]
        [CustomController(parentName: MenuTitle.TimeWarning, parentController: "TimeWarning", controllerName: "TimeWarning", name: "Get danh sách cảnh báo",
                           actionName: "gets", method: "GET", route: "/timewarning/gets", isPage: false,
                           enumAction: EnumAction.View, isShow: false, icon: "")]
        [HttpGet("/timewarning/gets")]
        public IActionResult gets(TimeWarningRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.TimeWarning.Gets;

            var obj = _timeWarningService.Gets(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [CustomController(parentName: "", parentController: "TimeWarning", controllerName: "TimeWarning", name: MenuTitle.TimeWarning,
                           actionName: "Index", method: "", route: "/timewarning/index", isPage: true,
                           enumAction: EnumAction.View, isShow: true, icon: Icon.TimeWarning)]
        public IActionResult Index()
        {
            // Start tranfer data Subheader
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.TimeWarning,
                Title = MenuTitle.TimeWarning,
            };
            ViewData[SubHeaderData] = subHeader;

            ViewBag.TicketStates = Utilities.ExtensionMethods.ToSelectList<EnumTicketState>().ToList();
            ViewBag.DeviceStates = Utilities.ExtensionMethods.ToSelectList<EnumDeviceState>().ToList();
            ViewBag.TaskStates = Utilities.ExtensionMethods.ToSelectList<EnumTaskState>().ToList();
            ViewBag.ReportStates = Utilities.ExtensionMethods.ToSelectList<EnumReportState>().ToList();
            ViewBag.ContractStates = Utilities.ExtensionMethods.ToSelectList<EnumContractState>().ToList();

            /// <summary>
            /// Cài đặt cảnh báo theo Type
            ///0 - Ticket
            ///1 - Device
            ///2 - Task
            ///3 - Dailyreport
            ///4 - Contract
            /// </summary>
            ViewBag.WarningTypes = new Dictionary<int, List<SelectListItem>>();
            ViewBag.WarningTypes.Add(0, ViewBag.TicketStates);
            ViewBag.WarningTypes.Add(1, ViewBag.DeviceStates);
            ViewBag.WarningTypes.Add(2, ViewBag.TaskStates);
            ViewBag.WarningTypes.Add(3, ViewBag.ReportStates);
            ViewBag.WarningTypes.Add(4, ViewBag.ContractStates);

            ViewBag.Warnings = GetWarnings(ViewBag.WarningTypes);

            ViewBag.NameTypes = Utilities.ExtensionMethods.ToSelectList<EnumTypeWarning>().ToList();
            ViewBag.UnitStates = Utilities.ExtensionMethods.ToSelectList<EnumUnitState>().ToList();


            var model = new TimeWarningUpsertRequest();

            return View(model);
        }

        [CustomController(parentName: MenuTitle.TimeWarning, parentController: "TimeWarning", controllerName: "TimeWarning", name: "Lưu cấu hình",
                       actionName: "Index", method: "POST", route: "/timewarning/index", isPage: false,
                       enumAction: EnumAction.Add, isShow: false, icon: "")]
        //[DisplayName("Action: Thêm mới thời gian cảnh báo")]
        [HttpPost]
        public IActionResult Index(TimeWarningUpsertRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.TimeWarning.Upsert;

            var obj = _timeWarningService.Upsert(header, model.warnings);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.code == 0)
            {
                return Ok(obj);
            }
            else
            {
                return NotFound(obj);
            }
        }

        private enum EnumUnitState
        {
            [Description("Phút")]
            Minute = 1,
            [Description("Giờ")]
            Hour = 2,
            [Description("Ngày")]
            Day = 3,
            [Description("Tuần")]
            Week = 4,
            [Description("Tháng")]
            Month = 5,
        }

        private Dictionary<int, Dictionary<int, List<TimeWarningModel>>> GetWarnings(Dictionary<int, List<SelectListItem>> warningTypes)
        {
            var header = Header;
            header.Action = CustomConfigApi.TimeWarning.Gets;
            var warnings = new Dictionary<int, Dictionary<int, List<TimeWarningModel>>>();
            var obj = _timeWarningService.Gets(header, null);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                //Group & Order warnings by type => convert to dictionary, Group value of warnings by state => convert to dictionary
                warnings = obj.data.items.OrderBy(o => o.type).GroupBy(g => g.type)
                    .ToDictionary(t => t.Key, t => t.ToList().OrderBy(o => o.state).GroupBy(g => g.state)
                    .ToDictionary(t => t.Key.Value, t => t.ToList()));
            }

            //foreach dữ liệu trong warningTypes
            foreach (var item in warningTypes)
            {
                //Variable dictionary
                var dictState = new Dictionary<int, List<TimeWarningModel>>();


                //Kiểm trả xem dictionary warnings có chứa type từ warningTypes nếu:
                //false - warnings không có type vừa kiểm tra
                //true - warnings có type vừa kiểm tra
                if (warnings.ContainsKey(item.Key) == false)
                {
                    //foreach dữ liệu Value của từng phần tử trong timeTypes
                    foreach (var state in item.Value.Select((value, index) => new { index, value }))
                    {
                        //Tạo một list TimeWarningModel
                        var entries = new List<TimeWarningModel>();
                        //Vòng lặp tương ứng với 3 warningLevel trong mỗi state
                        for (var i = 1; i <= 3; i++)
                        {
                            //Tạo một biến chứa TimeWarningModel
                            var model = new TimeWarningModel()
                            {
                                uuid = "",
                                isEnable = true,
                                state = state.index + 1,
                                unit = 1,
                                type = item.Key,
                                warningLevel = i,
                                warningValue = 0,
                            };
                            //Add model vào entries
                            entries.Add(model);
                        }
                        //Add Key là state.index + 1 & Value là entries vào Dictionary dictState
                        dictState.Add((state.index + 1), entries);
                    }
                    //Add Key là item.Key & Value là dictState vào Dictionary warnings
                    warnings.Add(item.Key, dictState);
                    //Order warnings by type || Key
                    warnings = warnings.OrderBy(o => o.Key).ToDictionary(t => t.Key, t => t.Value);
                }
                else
                {
                    //So sánh số lượng state trong warning với số lượng state trong item của waringTypes tương ứng
                    if (warnings[item.Key].Count != item.Value.Count)
                    {
                        foreach (var state in item.Value.Select((value, index) => new { index, value }))
                        {
                            //Compare type of warnings vs type of warningTypes
                            if (warnings[item.Key].Select(s => s.Key).Contains(state.index + 1) == false)
                            {
                                var entries = new List<TimeWarningModel>();
                                for (var i = 1; i <= 3; i++)
                                {
                                    var model = new TimeWarningModel()
                                    {
                                        uuid = "",
                                        isEnable = true,
                                        state = state.index + 1,
                                        unit = 1,
                                        type = item.Key,
                                        warningLevel = i,
                                        warningValue = 0,
                                    };
                                    //Add model to entries
                                    entries.Add(model);
                                }
                                //Add Key & Value to dictionary
                                warnings[item.Key].Add(state.index + 1, entries);
                                //Order warnings type by state
                                warnings[item.Key] = warnings[item.Key].OrderBy(o => o.Key).ToDictionary(o => o.Key, o => o.Value);
                            }
                        }
                    }
                }
            }
            return warnings;
        }
    }
}
