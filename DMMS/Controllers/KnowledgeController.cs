﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class KnowledgeController : BaseController<KnowledgeController>
    {
        private readonly IKnowledgeService _knowledgeService;
        public KnowledgeController(IKnowledgeService knowledgeService)
        {
            _knowledgeService = knowledgeService;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult gets(KnowledgeRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Knowledge.Topic;
            var obj = _knowledgeService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        public IActionResult getsqa(KnowledgeRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Knowledge.Qa;
            var obj = _knowledgeService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        public IActionResult GetQa()
        {
            return View();
        }
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(KnowledgeRequest request)
        {

            var header = Header;
            header.Action = CustomConfigApi.Knowledge.Upsert;
            var obj = _knowledgeService.Add(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        public IActionResult Editqa(string id)
        {
            var request = new KnowledgeRequest
            {
                uuid = id,

            };
            var header = Header;
            header.Action = CustomConfigApi.Knowledge.Qaupsertdetail;
            var obj = _knowledgeService.Get(header, request);
            var model = new KnowledgeModel
            {
                question = obj.data.question,
                uuid = obj.data.uuid,
                answer = obj.data.answer,
                topic_uuid = obj.data.knowledge_base_topic.uuid,
            };
            return View(model);
        }
        [HttpPut]
        public IActionResult Editqa(KnowledgeModel model)
        {
            var request = new QaUpsert
            {
                topic_uuid = model.topic_uuid,
                answer = model.answer,
                question = model.question,
                uuid = model.uuid
            };
            var header = Header;
            header.Action = CustomConfigApi.Knowledge.Qaupsert;
            var obj = _knowledgeService.QaUpsert(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        public IActionResult Edit(string id)
        {
            var request = new KnowledgeRequest
            {
                uuid = id,

            };
            var header = Header;
            header.Action = CustomConfigApi.Knowledge.Detail;
            var obj = _knowledgeService.Get(header, request);
            var model = new KnowledgeModel
            {

                uuid = obj.data.uuid,
                name = obj.data.name,

            };
            return View(model);
        }
        [HttpPut]
        public IActionResult Edit(KnowledgeModel model)
        {
            var request = new KnowledgeRequest
            {
                uuid = model.uuid,
                name = model.name,
            };
            var header = Header;
            header.Action = CustomConfigApi.Knowledge.Upsert;
            var obj = _knowledgeService.Get(header, request);
            return View(model);
        }
        public IActionResult testinput()
        {
            return View();
        }
        public IActionResult Qa(string id)
        {
            var model = new KnowledgeModel
            {
                topic_uuid = id
            };
            return View(model);
        }
        [HttpPost]
        public IActionResult Qa(KnowledgeModel model)
        {
            var request = new QaUpsert
            {
                topic_uuid = model.topic_uuid,
                answer = model.answer,
                question = model.question,
            };
            var header = Header;
            header.Action = CustomConfigApi.Knowledge.Qaupsert;
            var obj = _knowledgeService.QaUpsert(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        public IActionResult CreateQa()
        {
            ViewBag.Getqa = Gettopic();
            return View();
        }
        [HttpPost]
        public IActionResult CreateQa(KnowledgeModel model)
        {
            var request = new QaUpsert
            {
                topic_uuid = model.topic_uuid,
                answer = model.answer,
                question = model.question,
            };
            var header = Header;
            header.Action = CustomConfigApi.Knowledge.Qaupsert;
            var obj = _knowledgeService.QaUpsert(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        private List<KnowledgeModel> Gettopic()
        {
            var header = Header;
            header.Action = CustomConfigApi.Knowledge.Gets;
            var request = new KnowledgeRequest
            {

            };
            var obj = _knowledgeService.Get(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        [HttpDelete]
        public IActionResult Delete(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Knowledge.Delete;

            var request = new KnowledgeRequest
            {
                uuid = id
            };

            var obj = _knowledgeService.Delete(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        [HttpDelete]
        public IActionResult Deleteqa(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Knowledge.DeleteQA;

            var request = new KnowledgeRequest
            {
                uuid = id
            };

            var obj = _knowledgeService.DeleteQA(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }

    }
}