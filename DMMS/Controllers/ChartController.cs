﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class ChartController : BaseController<ChartController>
    {
        private readonly IChartService _chartService;
        public ChartController(IChartService chartService)
        {
            _chartService = chartService;
        }


        [HttpGet("/chart/all")]
        public IActionResult getCharts(RequestChart request)
        {
            Random random = new Random();
            var header = Header;
            header.Action = CustomConfigApi.Chart.All;
            var obj = _chartService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                int totalTasks = 0, totalDevices = 0, totalContracts = 0, totalOffers = 0;
                List<ChartModel> tasks = null;
                List<ChartModel> tickets = null;
                List<ChartModel> dailyreport = null;
                PieChartModel[] test = null;
                PieChartModel[] devices = null;
                PieChartModel[] offers = null;
                PieChartModel[] contracts = null;
                int[] datatask3 = null;
                int[] datatask1 = null;
                int[] datatask2 = null;
                int[] dataticket = null;
                if (obj.data!=null){
                    if(obj.data.items.tasks != null){
                        tasks = obj.data.items.tasks;



                    }
                    if (obj.data.items.tasks != null)
                    {
                       
                        test = new PieChartModel[obj.data.items.tasks.Count];
                        for (var i = 0; i< obj.data.items.tasks.Count;i++)
                        {
                            if (obj.data.items.tasks[i].level == 3)
                            {
                                var x = (from cus in obj.data.items.tasks[i].info where cus.state == 1 select cus);
                                var x1 = (from cus in obj.data.items.tasks[i].info where cus.state == 3 || cus.state == 4 select cus);
                                var x2 = (from cus in obj.data.items.tasks[i].info where cus.state == 5 || cus.state == 6 || cus.state ==7 select cus);
                                var x3 = (from cus in obj.data.items.tasks[i].info where cus.state == 2  select cus);
                                datatask3 = new int[]{ x.Sum(d => d.count), x1.Sum(d => d.count), x2.Sum(d => d.count), x3.Sum(d => d.count)};
                            }
                            if (obj.data.items.tasks[i].level == 1)
                            {
                                var x = (from cus in obj.data.items.tasks[i].info where cus.state == 1 select cus);
                                var x1 = (from cus in obj.data.items.tasks[i].info where cus.state == 3 || cus.state == 4 select cus);
                                var x2 = (from cus in obj.data.items.tasks[i].info where cus.state == 5 || cus.state == 6 || cus.state == 7 select cus);
                                var x3 = (from cus in obj.data.items.tasks[i].info where cus.state == 2 select cus);
                                datatask1 = new int[] { x.Sum(d => d.count), x1.Sum(d => d.count), x2.Sum(d => d.count), x3.Sum(d => d.count) };
                            }
                            if (obj.data.items.tasks[i].level == 2)
                            {
                                var x = (from cus in obj.data.items.tasks[i].info where cus.state == 1 select cus);
                                var x1 = (from cus in obj.data.items.tasks[i].info where cus.state == 3 || cus.state == 4 select cus);
                                var x2 = (from cus in obj.data.items.tasks[i].info where cus.state == 5 || cus.state == 6 || cus.state == 7 select cus);
                                var x3 = (from cus in obj.data.items.tasks[i].info where cus.state == 2 select cus);
                                 datatask2 = new int[]{ x.Sum(d => d.count), x1.Sum(d => d.count), x2.Sum(d => d.count), x3.Sum(d => d.count) };
                            }


                        }
                        
                        
                    }
                    if (obj.data.items.tickets != null)
                    {
                        var a = 0; var b = 0; var c = 0;
                        for(var i = 0; i < obj.data.items.tickets.Count; i++)
                        {
                            if(obj.data.items.tickets[i].level == 1)
                            {
                                 var x = (from ticket in obj.data.items.tickets[i].info where ticket.state == 1 select ticket);
                                a = x.Sum(d => d.count);
                            }
                            if (obj.data.items.tickets[i].level == 2)
                            {
                                 var x = (from ticket in obj.data.items.tickets[i].info where ticket.state == 1 select ticket);
                                b = x.Sum(d => d.count);
                            }
                            if (obj.data.items.tickets[i].level == 3)
                            {
                                var x = (from ticket in obj.data.items.tickets[i].info where ticket.state == 1 select ticket);
                                c = x.Sum(d => d.count);
                            }
                            dataticket = new int[] { a, b, c };
                        } 
                        //var a = from ticket in obj.data.tickets where ticket.level == 1 select ticket into newselect where newselect.info[0].state == 1 select newselect;

                    }
                    if(obj.data.items.daily_reports != null)
                    {
                        dailyreport = obj.data.items.daily_reports;
                    }
                    if (obj.data.items.offers!=null){
                        offers = new PieChartModel[obj.data.items.offers.Count];
                        offers = obj.data.items.offers.Select(x=> new PieChartModel{
                            name = x.name,
                            y = random.Next(10,100),
                            id = x.id,
                            key = x.ids != null ? String.Join(",", x.ids) : ""
                        }).ToArray();
                        totalOffers = obj.data.items.offers.Sum(x => x.count);
                    }
                    if(obj.data.items.contracts!=null){
                        contracts = new PieChartModel[obj.data.items.contracts.Count];
                        contracts = obj.data.items.contracts.Select(x=> new PieChartModel{
                            name = x.name,
                            y = random.Next(10, 100),
                            id = x.id,
                            key = x.ids != null ? String.Join(",", x.ids) : ""
                        }).ToArray();
                        totalContracts = contracts.Sum(x => x.y);
                    }
                    if(obj.data.items.devices!=null){
                        devices = new PieChartModel[obj.data.items.devices.Count];
                        devices = obj.data.items.devices.Select(x=> new PieChartModel
                        {
                            name = x.name,
                            id = x.id,
                            //data = new int[]{ random.Next(20, 200) },
                            y = random.Next(50, 250),
                            key = x.ids !=null ? String.Join(",", x.ids) : ""
                        }).ToArray();
                        totalDevices = obj.data.items.devices.Sum(x => x.count);
                    }
                }

                return Ok(new {
                    tasks,
                    tickets,
                    offers,
                    dailyreport,
                    devices,
                    contracts,
                    totalDevices,
                    totalContracts,
                    totalTasks,
                    totalOffers,
                    datatask3,
                    datatask2,
                    datatask1,
                    dataticket,

                });
            }

            return NotFound();
        }


        public IActionResult Index()
        {
            return View();
        }
    }
}