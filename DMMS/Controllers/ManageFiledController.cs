﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class ManageFiledController : BaseController<ManageFiledController>
    {
        private readonly IManageFiledService _manageFiledService;
        public ManageFiledController (IManageFiledService manageFiledService)
        {
            _manageFiledService = manageFiledService;
        }
        public IActionResult gets(ManageFieldRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Field.Get;

            if (request.limit == 0)
            {
                request.limit = 10;
            }

            var obj = _manageFiledService.Get(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        public IActionResult Index()
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Team,
                Title = MenuTitle.Field,
            };

            ViewData[SubHeaderData] = subHeader;
            return View();
        }
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(MangeFiledAddRequest request)
        {
            if (request.categoryId == null)
            {
                request.categoryId = new List<int>();
            }
            var header = Header;
            header.Action = CustomConfigApi.Field.Add;
            var obj = _manageFiledService.Add(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }
        
        [HttpDelete]
        public IActionResult Delete(int id)
        {
            var request = new MangeFiledDelete
            {
                field_id = id,
            };
            var header = Header;
            header.Action = CustomConfigApi.Field.Delete;
            var obj = _manageFiledService.Delete(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }
        public IActionResult Edit(int id)
        {
            var request = new MangeFiledDelete
            {
                field_id = id,
            };
            var header = Header;
            header.Action = CustomConfigApi.Field.Detail;
            var obj = _manageFiledService.Detail(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                var model = new ManageFiledDataResponse
                {
                    id = obj.data.id,
                    name = obj.data.name,
                    description = obj.data.description,
                    categories = obj.data.categories,
                };
                return View(model);
            }
            return NotFound();
        }
        [HttpPut]
        public IActionResult Edit(ManageFiledDataResponse model)
        {
            var request = new MangeFiledUpdateRequest()
            {
                id = model.id,
                name = model.name,
                description = model.description,
                is_enable = 1,
            };
            var header = Header;
            header.Action = CustomConfigApi.Field.Edit;
            var obj = _manageFiledService.Update(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        [Route("ManageFiled/inittable/{id}")]
        public IActionResult inittable(FiledGetCategories request)
        {
            if (request.limit == 0)
            {
                request.limit = 10;
            }
            var header = Header;
            header.Action = CustomConfigApi.Field.GetCategories;
            var obj = _manageFiledService.GetCategories(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }
    }
}