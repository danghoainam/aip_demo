﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class RolesController : BaseController<RolesController>
    {
        private readonly IRolesService _rolesService;
        public RolesController(IRolesService rolesService)
        {
            _rolesService = rolesService;
        }
        public IActionResult Index()
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Tree,
                Title = MenuTitle.Roles,
                SubTitle = SubMenuTitle.Roles
            };

            ViewData[SubHeaderData] = subHeader;
            return View();
        }
        public IActionResult gets(RolesRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Roles.Gets;
            var obj = _rolesService.Gets(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        public IActionResult Details(RolesDetailRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Roles.Detail;
            var obj = _rolesService.Detail(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        public IActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Add(RolesAddRequest model)
        {
            
            var header = Header;
            header.Action = CustomConfigApi.Roles.Add;
            var obj = _rolesService.Add(header, model);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        [Route("roles/edit/{uuid}")]
        public IActionResult Edit(string uuid)
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Tree,
                Title = MenuTitle.Roles,
                
            };

            ViewData[SubHeaderData] = subHeader;
            var request = new RolesDetailRequest()
            {
                role_uuid = uuid
            };
            var header = Header;
            header.Action = CustomConfigApi.Roles.Detail;
            var obj = _rolesService.Detail(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                var model =new  RolesDataResponse();
                model = obj.data;
                return View(model);
            }
            return NotFound(obj);
        }
        [HttpPut]
        public IActionResult Edit(RolesDataResponse model,List<string> names)
        {

            var request = new RolesUpdateRequest();
            request.role_uuid = model.role.uuid;
            request.name = model.role.name;
            request.is_default = model.role.is_default;
            request.description = model.role.description;
            request.permissions = names;

            var header = Header;
            header.Action = CustomConfigApi.Roles.Edit;
            var obj = _rolesService.Edit(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        [HttpDelete("roles/delete/{uuid}")]
        public IActionResult Delete(string uuid)
        {
            var request = new RolesDeleteRequest()
            {
                role_uuid = uuid
            };
            var header = Header;
            header.Action = CustomConfigApi.Roles.Delete;
            var obj = _rolesService.Delete(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
    }
}