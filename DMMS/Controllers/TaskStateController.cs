﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class TaskStateController : BaseController<TaskStateController>
    {
        private readonly ITaskStateService _taskStateService;
        private readonly ITaskService _taskService;
        public TaskStateController(ITaskStateService taskstate, ITaskService task)
        {
            _taskStateService = taskstate;
            _taskService = task;

        }
        public IActionResult gets(TaskStateRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.TaskState.Gets;
            if (request.limit == 0)
            {
                request.limit = 10;
            }
            var obj = _taskStateService.Gets(header, request);
            return Ok(obj);

        }
        public IActionResult Index()
        {
            ViewBag.tasksate = ExtensionMethods.ToSelectList<EnumTaskState>();
            var task = gettask();
            ViewBag.task = task;

            return View();
        }

        public IActionResult Create()
        {
            ViewBag.devicestate = ExtensionMethods.ToSelectList<EnumTaskState>();

            var task = gettask();
            ViewBag.task = task;

            var model = new TaskStateAddRequest();
            return View(model);
        }
        [HttpPost]
        public IActionResult Create(TaskStateAddRequest request)
        {
            ViewBag.devicestate = ExtensionMethods.ToSelectList<EnumTaskState>();
            var objreturn = new ResponseData<TaskStateDataResponse>();
            var header = Header;
            header.Action = CustomConfigApi.TaskState.Upsert;            

            var obj = _taskStateService.Upsert(header, request);
            if (obj!=null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
            
        }       
        public IActionResult Edit(string id)
        {
            ViewBag.devicestate = ExtensionMethods.ToSelectList<EnumTaskState>();
            var task = gettask();
            ViewBag.task = task;
            var model = new TaskStateAddRequest();
            if (string.IsNullOrEmpty(id))
            {
                return View(model);
            }
            else
            {
                var header = Header;
                header.Action = CustomConfigApi.TaskState.Gets;
                var request = new TaskStateRequest
                {
                    limit =1,
                    uuid = id
                };
                var obj = _taskStateService.Gets(header, request);
                if (obj.error == 0 && obj.data != null)
                {
                    var objget = obj.data.items;
                    model = new TaskStateAddRequest
                    {
                        uuid = id,
                        state = objget[0].state,
                        note = objget[0].note,
                        task_uuid = objget[0].task_uuid,
                    };
                }
                return View(model);
            }
        }
        [HttpPut]
        public IActionResult Edit(string uuid, TaskStateModel model)
        {
            ViewBag.devicestate = ExtensionMethods.ToSelectList<EnumTaskState>();
            var objreturn = new ResponseData<TaskStateDataResponse>();
            var header = Header;
            header.Action = CustomConfigApi.TaskState.Upsert;
            //check
            if (string.IsNullOrEmpty(model.uuid))
            {
                objreturn.error = 1;
                objreturn.msg = "khong co id";
                return Ok(objreturn);
            }
            var request = new TaskStateAddRequest
            {
                uuid = uuid,
                state = model.state,
                note = model.note,
            
            };
            var obj = _taskStateService.Upsert(header, request);
            if (obj.error == 0)
            {
                return Ok(obj);
            }
            else
            {
                objreturn.msg = string.IsNullOrEmpty(obj.msg) ? Constants.MESSAGE_ERROR : obj.msg;
                return Ok(objreturn);
            }
        }
        private List<TaskModel> gettask()
        {
            var headertask = Header;
            headertask.Action = CustomConfigApi.Task.Gets;
            var request = new TaskRequest
            {
                limit = 1000,
            };
            var objtask = _taskService.Get(headertask, request);
            if (objtask.error == 0 && objtask.data != null)
            {
                return objtask.data.items;
            }
            return null;
        }
    }
}