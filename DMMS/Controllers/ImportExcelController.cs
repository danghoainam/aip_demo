﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using RestSharp.Extensions;

namespace DMMS.Controllers
{
    public class ImportExcelController : BaseController<ImportExcelController>
    {

        private readonly IImportExcelService _importExcelService;
        private readonly IImportExcelPackageCategoriesService _importExcelPackageCategoriesService;
        public ImportExcelController(IImportExcelService importExcelService, IImportExcelPackageCategoriesService importExcelPackageCategoriesService)
        {
            _importExcelService = importExcelService;
            _importExcelPackageCategoriesService = importExcelPackageCategoriesService;
        }
        RequestProjectsChecking requestProject = new RequestProjectsChecking();
        List<ImportExcelModel> listRowExcel = new List<ImportExcelModel>();
        private static List<RequestProjectsAddData> model = new List<RequestProjectsAddData>();

        List<ImportExcelPackageCategoriesModel> listRowExcel2 = new List<ImportExcelPackageCategoriesModel>();
        private static List<RequestPackageCategoriesAddData> model2 = new List<RequestPackageCategoriesAddData>();

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult ImportProject()
        {
            return View();
        }

        public ActionResult Download()
        {
            string Files = "wwwroot/statics/ImportExcel/ImportTemplate.xlsx";
            byte[] fileBytes = System.IO.File.ReadAllBytes(Files);
            System.IO.File.WriteAllBytes(Files, fileBytes);
            MemoryStream ms = new MemoryStream(fileBytes);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, "ImportTemplate.xlsx");
        }

        public ActionResult ImportExcelProject()
        {
            model = null;
            model = new List<RequestProjectsAddData>();
            ImportExcelModel result = new ImportExcelModel();
            IFormFile file = Request.Form.Files[0];
            string filename = SaveFile(file);
            StringBuilder sb = new StringBuilder();
            StringBuilder sb1 = new StringBuilder();
            var Data = ReadFile(filename, "Dự án");
            ProjectsCheckingResponse listCheck = new ProjectsCheckingResponse();

            listCheck = GetsAllCheck(requestProject);
            if (Data.Count <= 0 || Data[0].Count > 33 || Data[0].Count != 33)
            {
                this.Content(sb.ToString());
            }
            else
            {
                sb.Append("<div style = 'width: 100%;overflow: auto;'>");
                sb.Append("<table class='tg' style='overflow: auto; width: 400% '><tr>");
                sb.Append("<th width=1.5%>" + "Mã DA" + "</th>");
                sb.Append("<th width=1.5% >" + "Tỉnh/ Thành" + "</th>");
                sb.Append("<th width=3%>" + "Tên dự án" + "</th>");
                sb.Append("<th width=5%>" + "Mô tả dự án" + "</th>");
                sb.Append("<th width=2%>" + "Phòng ban quản lý" + "</th>");
                sb.Append("<th width=2.5%>" + "Lĩnh vực" + "</th>");
                sb.Append("<th width=1.5%>" + "Giá trị HĐ" + "</th>");
                sb.Append("<th width=1.5%>" + "Đơn vị ký HĐ" + "</th>");
                sb.Append("<th width=1.5%>" + "Số hiệu HĐ" + "</th>");
                sb.Append("<th width=1.5%>" + "Ngày ký HĐ" + "</th>");
                sb.Append("<th width=3%>" + "Ngày bàn giao (trên HĐ)" + "</th>");
                sb.Append("<th width=3%>" + "Thời gian bảo trì (tháng)" + "</th>");
                sb.Append("<th width=3%>" + "Tự động đóng bảo hành" + "</th>");
                sb.Append("<th width=3%>" + "Thời gian bảo trì định kỳ (tháng)" + "</th>");
                sb.Append("<th width=2%>" + "Số lần phải bảo trì định kỳ" + "</th>");
                sb.Append("<th width=2%>" + "Số lần phải bảo trì định kỳ (Auto)" + "</th>");
                sb.Append("<th width=2%>" + "Số lần đã bảo trì(đối với dự án cũ)" + "</th>");
                sb.Append("<th width=3%>" + "Chủ đầu tư" + "</th>");
                sb.Append("<th width=3%>" + "Trạng thái thực tế" + "</th>");
                sb.Append("<th width=3%>" + "Tên đơn vị" + "</th>");
                sb.Append("<th width=4%>" + "Chi tiết" + "</th>");
                sb.Append("<th width=4%>" + "Phường/ Xã/ Thị trấn" + "</th>");
                sb.Append("<th width=4%>" + "Quận/ Huyện/ Thị Xã" + "</th>");
                sb.Append("<th width=2%>" + "Tỉnh/ Thành" + "</th>");
                sb.Append("<th width=2%>" + "Email" + "</th>");
                sb.Append("<th width=3%>" + "Tên người liên hệ" + "</th>");
                sb.Append("<th width=2%>" + "Số điện thoại" + "</th>");
                sb.Append("<th width=2%>" + "Chức vụ" + "</th>");
                sb.Append("<th width=2%>" + "Ngày bàn giao (thực tế)" + "</th>");
                sb.Append("<th width=3%>" + "Sản phẩm" + "</th>");
                sb.Append("<th width=3%>" + "Loại Sản phẩm" + "</th>");
                sb.Append("<th width=2%>" + "Số lượng phòng" + "</th>");
                sb.Append("<th width=2%>" + "Trạng thái bảo hành" + "</th>");
                sb.Append("</tr>");

                List<string> maDuAnMoi = new List<string>();
                List<string> maDaKiemTra = new List<string>();
                List<CustomerAndLocation> donViThuHuongMoi = new List<CustomerAndLocation>();
                if (listCheck != null)
                {
                    maDuAnMoi = listCheck.project_code.not_exits;
                    donViThuHuongMoi = listCheck.customer_and_location.not_exits;
                }
                List<CustomerAndLocation> donViThuHuongDaKiemTra = new List<CustomerAndLocation>();
                for (int i = 0; i < Data.Count; i++)
                {
                    ImportExcelModel rowLine = new ImportExcelModel();
                    string MaDuAn = Data[i][0];
                    if (MaDuAn == "") break;
                    rowLine.projectCode = Data[i][0];
                    rowLine.projectProvinceName = Data[i][1];
                    rowLine.projectName = Data[i][2];
                    rowLine.projectDescription = Data[i][3];
                    rowLine.projectTeamName = Data[i][4];
                    rowLine.projectFieldName = Data[i][5];
                    
                    rowLine.contractCost = long.Parse(Data[i][6] != "" ? Data[i][6].ToString() : "0");
                    rowLine.contractUnitSignedName = Data[i][7];
                    rowLine.contractCode = Data[i][8];
                    rowLine.contractTimeSigned = Data[i][9] != "" ? Data[i][9].ToString() : DateTime.Now.ToString();
                    rowLine.contractTimeValidStart = Data[i][10] != "" ? Data[i][10].ToString() : DateTime.Now.ToString();

                    var timeMaiVailid = Data[i][11];
                    if (Data[i][11] != "")
                    {
                        timeMaiVailid = Data[i][11].ToString().Trim();
                        if (timeMaiVailid.Length > 1)
                        {
                            timeMaiVailid = timeMaiVailid + " ";
                            timeMaiVailid = timeMaiVailid.Substring(0, timeMaiVailid.IndexOf(" "));
                        }
                    }
                    else timeMaiVailid = "0";

                    rowLine.contractTimeValidValue = Int32.Parse(timeMaiVailid);
                    rowLine.contractIsAutoClose = Data[i][12] == "Không" ? false : true;
                    rowLine.contractTimeMaintainLoop = Int32.Parse(Data[i][15] != "" ? Data[i][15].ToString() : "0");
                    rowLine.contractTimeMaintainValue = Int32.Parse(Data[i][16] != "" ? Data[i][15].ToString() : "0");
                    rowLine.contractInvestorName = Data[i][17];
                    rowLine.contractStateName = Data[i][18];
                    rowLine.customerName = Data[i][19];
                    rowLine.customerAddress = Data[i][20];
                    rowLine.customerVillageName = Data[i][21];
                    rowLine.customerTownName = Data[i][22];
                    rowLine.customerProvinceName = Data[i][23];
                    rowLine.customerEmail = Data[i][24];
                    rowLine.customerContactName = Data[i][25];
                    rowLine.customerContactPhone = Data[i][26];
                    rowLine.customerContactDuty = Data[i][27];
                    rowLine.packageName = Data[i][29];
                    rowLine.packageCodeName = Data[i][30];
                    rowLine.packageCount = Int32.Parse(Data[i][31] != "" ? Data[i][31].ToString() : "0");
                    rowLine.customerStateName = Data[i][32];
                    listRowExcel.Add(rowLine);
                    var checkNewProject = maDuAnMoi.Where(p => p.Equals(MaDuAn));
                    if (checkNewProject.Count() != 0)
                    {
                        rowLine.projectCode = MaDuAn;
                        sb.AppendLine("<tr>");
                        sb.Append("<td style='font-weight: bold;color:#52c41a;'>" + "<span><i class='flaticon2-check-mark text-success'></i></span> &nbsp" + MaDuAn + "</td>");
                        for (int j = 1; j <= 32; j++)
                        {
                            if (j == 19)
                            {
                                string DonViThuHuong = Data[i][19];
                                string TinhThanh = Data[i][23];
                                var checkNewCustome = donViThuHuongMoi.Where(p => p.customerName.Equals(DonViThuHuong) && p.provinceName.Equals(TinhThanh));
                                if (checkNewCustome.Count() != 0)
                                {
                                    sb.Append("<td style='font-weight: bold;color:#52c41a;'>" + "<span><i class='flaticon2-check-mark text-success'></i></span> &nbsp" + DonViThuHuong + "</td>");
                                }
                                if (checkNewCustome.Count() == 0)
                                {
                                    sb.Append("<td style='font-weight: bold;color:red;'>" + DonViThuHuong + "</td>");
                                }
                            }
                            else
                            {
                                sb.Append("<td >" + Data[i][j] + "</td>");
                            }
                        }
                        sb.Append("</tr>");
                    }
                    else
                    {
                        sb.AppendLine("<tr>");
                        sb.Append("<td style='font-weight: bold;color:red;'>" + MaDuAn + "</td>");
                        for (int k = 1; k <= 32; k++)
                        {
                            if (k == 19)
                            {
                                string DonViThuHuong = Data[i][19];
                                string TinhThanh = Data[i][23];
                                var checkNewCustome = donViThuHuongMoi.Where(p => p.customerName.Equals(DonViThuHuong) && p.provinceName.Equals(TinhThanh));
                                if (checkNewCustome.Count() != 0)
                                {
                                    sb.Append("<td style='font-weight: bold;color:#52c41a;'>" + "<span><i class='flaticon2-check-mark text-success'></i></span> &nbsp" + DonViThuHuong + "</td>");
                                }
                                if (checkNewCustome.Count() == 0)
                                {
                                    sb.Append("<td style='font-weight: bold;color:red;'>" + DonViThuHuong + "</td>");
                                }
                            }
                            else
                            {
                                sb.Append("<td >" + Data[i][k] + "</td>");
                            }
                        }
                        sb.Append("</tr>");
                    }
                }

                var dic = new Dictionary<string, List<ImportExcelModel>>();
                listRowExcel.ToList().ForEach(t =>
                {
                    var timeSigned = "";
                    var timeValidStar = "";
                    try
                    {
                        timeSigned = Convert.ToDateTime(t.contractTimeSigned).ToString("yyyy-MM-dd");
                        timeValidStar = Convert.ToDateTime(t.contractTimeValidStart).ToString("yyyy-MM-dd");
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                  
                    string projectCode = t.projectCode;
                    var obj1 = new ImportExcelModel()
                    {
                        projectCode = t.projectCode,
                        projectProvinceName = t.projectProvinceName,
                        projectName = t.projectName,
                        projectDescription = t.projectDescription,
                        projectTeamName = t.projectTeamName,
                        projectFieldName = t.projectFieldName,
                        contractCode = t.contractCode,
                        contractCost = t.contractCost,
                        contractTimeSigned = timeSigned,
                        contractTimeValidValue = t.contractTimeValidValue,
                        contractTimeValidStart = timeValidStar,
                        contractTimeMaintainLoop = t.contractTimeMaintainLoop,
                        contractTimeMaintainValue = t.contractTimeMaintainValue,
                        contractUnitSignedName = t.contractUnitSignedName,
                        contractInvestorName = t.contractInvestorName,
                        contractIsAutoClose = t.contractIsAutoClose,
                        contractStateName = t.contractStateName,
                        customerName = t.customerName,
                        customerAddress = t.customerAddress,
                        customerProvinceName = t.customerProvinceName,
                        customerTownName = t.customerTownName,
                        customerVillageName = t.customerVillageName,
                        customerEmail = t.customerEmail,
                        customerContactName = t.customerContactName,
                        customerContactPhone = t.customerContactPhone,
                        customerContactDuty = t.customerContactDuty,
                        customerStateName = t.customerStateName,
                        packageName = t.packageName,
                        packageCodeName = t.packageCodeName,
                        packageCount = t.packageCount,
                    };
                    if (!dic.ContainsKey(projectCode))
                        dic.Add(projectCode, new List<ImportExcelModel>());
                    dic[projectCode].Add(obj1);
                });

                dic.ToList().ForEach(t =>
                {
                    int dem = 0;
                    RequestProjectsAddData rowData = new RequestProjectsAddData();
                    var dicsub = new Dictionary<string, List<CustomerAndPackageModel>>();
                    foreach (var items in t.Value.ToArray())
                    {
                        if (dem == 0)
                        {
                            rowData.project.code = items.projectCode;
                            rowData.project.provinceName = items.projectProvinceName;
                            rowData.project.name = items.projectName;
                            rowData.project.description = items.projectDescription;
                            rowData.project.teamName = items.projectTeamName;
                            rowData.project.fieldName = items.projectFieldName;
                            rowData.contract.code = items.contractCode;
                            rowData.contract.cost = items.contractCost;
                            rowData.contract.timeSigned = items.contractTimeSigned;
                            rowData.contract.timeValidValue = items.contractTimeValidValue;
                            rowData.contract.timeValidStart = items.contractTimeValidStart;
                            rowData.contract.timeMaintainLoop = items.contractTimeMaintainLoop;
                            rowData.contract.timeMaintainValue = items.contractTimeMaintainValue;
                            rowData.contract.unitSignedName = items.contractUnitSignedName;
                            rowData.contract.investorName = items.contractInvestorName;
                            rowData.contract.isAutoClose = items.contractIsAutoClose;
                            rowData.contract.stateName = items.contractStateName;
                        }
                        dem++;

                        string customerName = items.customerName;
                        var obj2 = new CustomerAndPackageModel()
                        {
                            customerName = items.customerName,
                            customerAddress = items.customerAddress,
                            customerProvinceName = items.customerProvinceName,
                            customerTownName = items.customerTownName,
                            customerVillageName = items.customerVillageName,
                            customerEmail = items.customerEmail,
                            customerContactName = items.customerContactName,
                            customerContactPhone = items.customerContactPhone,
                            customerContactDuty = items.customerContactDuty,
                            customerStateName = items.customerStateName,
                            packageName = items.packageName,
                            packageCodeName = items.packageCodeName,
                            packageCount = items.packageCount,
                        };
                        if (!dicsub.ContainsKey(customerName))
                            dicsub.Add(customerName, new List<CustomerAndPackageModel>());
                        dicsub[customerName].Add(obj2);
                    }


                    dicsub.ToList().ForEach(l =>
                    {
                        ImportCustomerAndPackage customeAndPackage = new ImportCustomerAndPackage();
                        int dem1 = 0;
                        foreach (var itemsCustome in l.Value.ToArray())
                        {
                            if (dem1 == 0)
                            {
                                customeAndPackage.customer.name = itemsCustome.customerName;
                                customeAndPackage.customer.address = itemsCustome.customerAddress;
                                customeAndPackage.customer.provinceName = itemsCustome.customerProvinceName;
                                customeAndPackage.customer.townName = itemsCustome.customerTownName;
                                customeAndPackage.customer.villageName = itemsCustome.customerVillageName;
                                customeAndPackage.customer.email = itemsCustome.customerEmail;
                                customeAndPackage.customer.contactName = itemsCustome.customerContactName;
                                customeAndPackage.customer.contactPhone = itemsCustome.customerContactPhone;
                                customeAndPackage.customer.contactDuty = itemsCustome.customerContactDuty;
                                customeAndPackage.customer.stateName = itemsCustome.customerStateName;
                            }
                            dem1++;
                            ImportPackage packageSub = new ImportPackage();
                            packageSub.name = itemsCustome.packageName;
                            packageSub.packageCodeName = itemsCustome.packageCodeName;
                            packageSub.count = itemsCustome.packageCount;
                            customeAndPackage.packages.Add(packageSub);
                        }
                        rowData.customerAndPackage.Add(customeAndPackage);
                    });
                    model.Add(rowData);
                });
                sb.Append("</table>");
                sb.Append("</div>");
            }
            return this.Content(sb.ToString());     
        }

        private string SaveFile(IFormFile ImportFile)
        {
            string FolderPath = "wwwroot/statics/ImportExcel/ImportExcelProject";
            byte[] fileBytes;
            using (var stream = ImportFile.OpenReadStream())
            {
                fileBytes = stream.ReadAsBytes();
            }
            string uploadFileName = string.Format("{0:yyyyMMdd_hhmmss}", DateTime.Now) + ImportFile.FileName.Substring(ImportFile.FileName.LastIndexOf('.'));
            // Set full path to upload file
            string uploadFilePath = Path.Combine(FolderPath, uploadFileName);
            if (!Directory.Exists(FolderPath))
            {
                Directory.CreateDirectory(FolderPath);
            }
            // Save new file
            System.IO.File.WriteAllBytes(uploadFilePath, fileBytes);
            return uploadFilePath;
        }

        private List<List<string>> ReadFile(string FilePath, string SheetName)
        {
            int startRowIndex = 3;
            List<List<string>> Result = new List<List<string>>();

            FileInfo fileInfo = new FileInfo(FilePath);

            using (var excelPackage = new ExcelPackage(fileInfo))
            {
                var sheet = excelPackage.Workbook.Worksheets[SheetName];
                if (sheet == null)
                    sheet = excelPackage.Workbook.Worksheets[1];

                if (sheet != null)
                {
                    for (var rowIndex = 0; rowIndex < sheet.Dimension.Rows - 1; rowIndex++)
                    {
                        List<string> Line = new List<string>();

                        for (var colIndex = 0; colIndex < sheet.Dimension.Columns; colIndex++)
                        {                           
                            var Value = sheet.Cells[rowIndex + startRowIndex, colIndex + 1].Value;
                            Line.Add(Value != null ? Value.ToString() : "");
                        }
                        var MaDuAn = sheet.Cells[rowIndex + startRowIndex, 1].Value;
                        var DonViThuHuong = sheet.Cells[rowIndex + startRowIndex, 19].Value;
                        var TinhThanh = sheet.Cells[rowIndex + startRowIndex, 23].Value;
                        if (MaDuAn != null)
                        {
                            requestProject.projectCodes.Add(MaDuAn.ToString().Trim());
                        }
                        if (DonViThuHuong != null)
                        {
                            CustomerAndLocation checkCustome = new CustomerAndLocation();
                            checkCustome.customerName = DonViThuHuong.ToString().Trim();
                            checkCustome.provinceName = TinhThanh != null ? TinhThanh.ToString() : "";
                            requestProject.customerAndLocations.Add(checkCustome);
                        }
                        Result.Add(Line);
                    }
                }
            }
            return Result;
        }

        public ProjectsCheckingResponse GetsAllCheck(RequestProjectsChecking request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.ProjectChecking;
            var obj = _importExcelService.GetAllList(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data;
            }
            return null;
        }

        [HttpPost]
        public IActionResult UpsetExcelProject()
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.UploadExcelData;
            List<string> maDaDuAnDaDuyet = new List<string>();
            var obj = _importExcelService.UpSert(header, model);            
            if (obj.error == 0 && obj.msg == "success")
            {
                model = null;
                return Ok(obj);
            }
            return NotFound(obj);

        }

        public ActionResult ImportExcelPackage()
        {
            model2 = null;
            model2 = new List<RequestPackageCategoriesAddData>();
            ImportExcelPackageCategoriesModel result = new ImportExcelPackageCategoriesModel();
            IFormFile file = Request.Form.Files[0];
            string filename = SaveFilePackage(file);
            StringBuilder sb = new StringBuilder();
            StringBuilder sb1 = new StringBuilder();
            var Data = ReadFilePackageCategory(filename, "Chi tiết sản phẩm");
            //ProjectsCheckingResponse listCheck = new ProjectsCheckingResponse();

            //listCheck = GetsAllCheck(requestProject);
            if (Data.Count <= 0 || Data[0].Count > 13 || Data[0].Count != 13)
            {
                this.Content(sb.ToString());
            }
            else
            {
                sb.Append("<div style = 'width: 100%;overflow: auto;'>");
                sb.Append("<table class='tg' style='overflow: auto; width: 130% '><tr>");
                sb.Append("<th width=5%>" + "ID dự án" + "</th>");
                sb.Append("<th width=8% >" + "Tên sản phẩm" + "</th>");
                sb.Append("<th width=8%>" + "Loại sản phẩm" + "</th>");
                sb.Append("<th width=12%>" + "Danh mục" + "</th>");
                sb.Append("<th width=7%>" + "Model" + "</th>");
                sb.Append("<th width=7%>" + "Hãng" + "</th>");
                sb.Append("<th width=7%>" + "Xuất xứ" + "</th>");
                sb.Append("<th width=11%>" + "Giá trị (mua vào)" + "</th>");
                sb.Append("<th width=11%>" + "Giá trị (bán ra)" + "</th>");
                sb.Append("<th width=5%>" + "Đơn vị" + "</th>");
                sb.Append("<th width=10%>" + "Mô tả" + "</th>");
                sb.Append("<th width=4%>" + "SL" + "</th>");
                sb.Append("<th width=5%>" + "Thời gian BH (tháng)" + "</th>");
                sb.Append("</tr>");

                //List<string> maDuAnMoi = new List<string>();
                //List<string> maDaKiemTra = new List<string>();
                //List<CustomerAndLocation> donViThuHuongMoi = new List<CustomerAndLocation>();
                //maDuAnMoi = listCheck.project_code.not_exits;
                //donViThuHuongMoi = listCheck.customer_and_location.not_exits;
                //List<CustomerAndLocation> donViThuHuongDaKiemTra = new List<CustomerAndLocation>();
                for (int i = 0; i < Data.Count; i++)
                {
                    ImportExcelPackageCategoriesModel rowLine = new ImportExcelPackageCategoriesModel();
                    rowLine.projectCode = Data[i][0];
                    rowLine.packageName = Data[i][1];
                    rowLine.packageCodeName = Data[i][2];
                    rowLine.categoriesName = Data[i][3];
                    rowLine.categoriesModel = Data[i][4];
                    rowLine.categoriesManufacturer = Data[i][5];
                    rowLine.categoriesOriginName = Data[i][6];
                    rowLine.categoriesExpPriceRef = long.Parse(Data[i][7] != "" ? Data[i][7].ToString() : "0");
                    rowLine.categoriesImpPriceRef = long.Parse(Data[i][8] != "" ? Data[i][8].ToString() : "0");
                    rowLine.categoriesUnitName = Data[i][9];
                    rowLine.categoriesDescription = Data[i][10];
                    //rowLine.categoriesType = int.Parse(Data[i][11] != "" ? Data[i][11].ToString() : "0");
                    rowLine.categoriesCount = int.Parse(Data[i][11] != "" ? Data[i][11].ToString() : "0");
                    rowLine.categoriesTimeMaintainValue = int.Parse(Data[i][12] != "" ? Data[i][12].ToString() : "0");

                    listRowExcel2.Add(rowLine);

                    sb.AppendLine("<tr>");
                    for (int j = 0; j < 13; j++)
                    {
                        sb.Append("<td >" + Data[i][j] + "</td>");
                    }
                    sb.Append("</tr>");

                }

                var projectCode = "";
                var projectCode2 = "";
                var dic = new Dictionary<string, List<ImportExcelPackageCategoriesModel>>();
                listRowExcel2.ToList().ForEach(t =>
                {
                    projectCode = t.projectCode;
                    if (projectCode == "")
                    {
                        projectCode = projectCode2;
                    }
                    else projectCode2 = t.projectCode;

                    var obj1 = new ImportExcelPackageCategoriesModel()
                    {
                        projectCode = t.projectCode,
                        packageName = t.packageName,
                        packageCodeName = t.packageCodeName,
                        categoriesName = t.categoriesName,
                        categoriesModel = t.categoriesModel,
                        categoriesManufacturer = t.categoriesManufacturer,
                        categoriesOriginName = t.categoriesOriginName,
                        categoriesExpPriceRef = t.categoriesExpPriceRef,
                        categoriesImpPriceRef = t.categoriesImpPriceRef,
                        categoriesUnitName = t.categoriesUnitName,
                        categoriesDescription = t.categoriesDescription,
                        categoriesType = t.categoriesType,
                        categoriesCount = t.categoriesCount,
                        categoriesTimeMaintainValue = t.categoriesTimeMaintainValue,
                    };
                    if (!dic.ContainsKey(projectCode))
                        dic.Add(projectCode, new List<ImportExcelPackageCategoriesModel>());
                    dic[projectCode].Add(obj1);
                });

                dic.ToList().ForEach(t =>
                {
                    int dem = 0;
                    string packageName = "";
                    string packageName2 = "";
                    RequestPackageCategoriesAddData rowData = new RequestPackageCategoriesAddData();
                    var dicsub = new Dictionary<string, List<PackageCategoriesModel>>();
                    foreach (var items in t.Value.ToArray())
                    {
                        if (dem == 0)
                        {
                            rowData.projectCode = items.projectCode;
                        }

                        dem++;
                        packageName = items.packageName;
                        if (packageName == "")
                        {
                            packageName = packageName2;
                        }
                        else
                        {
                            packageName2 = items.packageName;
                        }

                        var obj2 = new PackageCategoriesModel()
                        {
                            packageName = items.packageName,
                            packageCodeName = items.packageCodeName,
                            categoriesName = items.categoriesName,
                            categoriesModel = items.categoriesModel,
                            categoriesManufacturer = items.categoriesManufacturer,
                            categoriesOriginName = items.categoriesOriginName,
                            categoriesExpPriceRef = items.categoriesExpPriceRef,
                            categoriesImpPriceRef = items.categoriesImpPriceRef,
                            categoriesUnitName = items.categoriesUnitName,
                            categoriesDescription = items.categoriesDescription,
                            categoriesType = items.categoriesType,
                            categoriesCount = items.categoriesCount,
                            categoriesTimeMaintainValue = items.categoriesTimeMaintainValue,
                        };
                        if (!dicsub.ContainsKey(packageName))
                            dicsub.Add(packageName, new List<PackageCategoriesModel>());
                        dicsub[packageName].Add(obj2);
                    }


                    dicsub.ToList().ForEach(l =>
                    {
                        ImportPackageCategories packageCategories = new ImportPackageCategories();
                        int dem1 = 0;
                        foreach (var itemsCustome in l.Value.ToArray())
                        {
                            if (dem1 == 0)
                            {
                                packageCategories.package.packageName = itemsCustome.packageName;
                                packageCategories.package.packageCodeName = itemsCustome.packageCodeName;
                            }
                            dem1++;
                            ImportCategories categories = new ImportCategories();
                            categories.categoryName = itemsCustome.categoriesName;
                            categories.model = itemsCustome.categoriesModel;
                            categories.manufacturer = itemsCustome.categoriesManufacturer;
                            categories.originName = itemsCustome.categoriesOriginName;
                            categories.expPriceRef = itemsCustome.categoriesExpPriceRef;
                            categories.impPriceRef = itemsCustome.categoriesImpPriceRef;
                            categories.unitName = itemsCustome.categoriesUnitName;
                            categories.description = itemsCustome.categoriesDescription;
                            categories.type = itemsCustome.categoriesType;
                            categories.count = itemsCustome.categoriesCount;
                            categories.timeMaintainValue = itemsCustome.categoriesTimeMaintainValue;
                            packageCategories.categories.Add(categories);
                        }
                        rowData.packageCategories.Add(packageCategories);
                    });
                    model2.Add(rowData);
                });
                sb.Append("</table>");
                sb.Append("</div>");
            }
            return this.Content(sb.ToString());
        }

        private string SaveFilePackage(IFormFile ImportFile)
        {
            string FolderPath = "wwwroot/statics/ImportExcel/ImportExcelPackageCategory";
            byte[] fileBytes;
            using (var stream = ImportFile.OpenReadStream())
            {
                fileBytes = stream.ReadAsBytes();
            }
            string uploadFileName = string.Format("{0:yyyyMMdd_hhmmss}", DateTime.Now) + ImportFile.FileName.Substring(ImportFile.FileName.LastIndexOf('.'));
            // Set full path to upload file
            string uploadFilePath = Path.Combine(FolderPath, uploadFileName);
            if (!Directory.Exists(FolderPath))
            {
                Directory.CreateDirectory(FolderPath);
            }
            // Save new file
            System.IO.File.WriteAllBytes(uploadFilePath, fileBytes);
            return uploadFilePath;
        }

        private List<List<string>> ReadFilePackageCategory(string FilePath, string SheetName)
        {
            int startRowIndex = 2;
            List<List<string>> Result = new List<List<string>>();

            FileInfo fileInfo = new FileInfo(FilePath);

            using (var excelPackage = new ExcelPackage(fileInfo))
            {
                var sheet = excelPackage.Workbook.Worksheets[SheetName];
                if (sheet == null)
                    sheet = excelPackage.Workbook.Worksheets[1];

                if (sheet != null)
                {
                    for (var rowIndex = 0; rowIndex < sheet.Dimension.Rows - 1; rowIndex++)
                    {
                        List<string> Line = new List<string>();

                        for (var colIndex = 0; colIndex < sheet.Dimension.Columns; colIndex++)
                        {
                            var Value = sheet.Cells[rowIndex + startRowIndex, colIndex + 1].Value;
                            Line.Add(Value != null ? Value.ToString() : "");
                        }
                        //var MaDuAn = sheet.Cells[rowIndex + startRowIndex, 1].Value;
                        //var DonViThuHuong = sheet.Cells[rowIndex + startRowIndex, 19].Value;
                        //var TinhThanh = sheet.Cells[rowIndex + startRowIndex, 23].Value;
                        //if (MaDuAn != null)
                        //{
                        //    requestProject.projectCodes.Add(MaDuAn.ToString().Trim());
                        //}
                        //if (DonViThuHuong != null)
                        //{
                        //    CustomerAndLocation checkCustome = new CustomerAndLocation();
                        //    checkCustome.customerName = DonViThuHuong.ToString().Trim();
                        //    checkCustome.provinceName = TinhThanh != null ? TinhThanh.ToString() : "";
                        //    requestProject.customerAndLocations.Add(checkCustome);
                        //}
                        Result.Add(Line);
                    }
                }
            }
            return Result;
        }

        [HttpPost]
        public IActionResult UpsetExcelPackage()
        {
            var header = Header;
            header.Action = CustomConfigApi.Package.UploadExcelData;
            var obj = _importExcelPackageCategoriesService.UpSert(header, model2);
            if (obj.error == 0 && obj.msg == "success")
            {
                model = null;
                return Ok(obj);
            }
            return NotFound(obj);

        }
    }
}
