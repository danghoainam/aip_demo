﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Extensions;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;

namespace DMMS.Controllers
{

    public class TicketController : BaseController<TicketController>
    {
        private readonly ITicketService _ticketService;
        private readonly IStaffService _staffService;
        private readonly ICustomerService _customerService;
        private readonly IDeviceService _deviceService;
        private readonly ITaskService _taskService;
        private readonly IImageService _imageService;

        public TicketController(ITicketService ticket, IStaffService staffService, ICustomerService customerService, 
            IDeviceService deviceService, IImageService imageService, ITaskService taskService)
        {
            _ticketService = ticket;
            _staffService = staffService;
            _customerService = customerService;
            _deviceService = deviceService;
            _imageService = imageService;
            _taskService = taskService;
        }

        //[DisplayName("Action: Danh sách ticket")]
        [CustomController(parentName: MenuTitle.Ticket, parentController: "Ticket", controllerName: "Ticket", name: "Get danh sách yêu cầu",
                           actionName: "gets", method: "GET", route: "/ticket/gets", isPage: false,
                           enumAction: EnumAction.View, isShow: false, icon: "")]
        public IActionResult gets(TicketRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Ticket.Gets;

            if(model.limit == 0)
            {
                model.limit = 10;
            }

            var obj = _ticketService.Gets(header, model);

            if(obj != null && (EnumError)obj.error==EnumError.Success && obj.code != 404)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        [CustomController(parentName: MenuTitle.Ticket, parentController: "Ticket", controllerName: "Ticket", name: "Get danh sách yêu cầu theo đơn vị",
                   actionName: "getsTicket", method: "GET", route: "/ticket/getsticket/{customer_uuid}", isPage: false,
                   enumAction: EnumAction.View, isShow: false, icon: "")]
        [Route("/ticket/getsticket/{customer_uuid}")]
        public IActionResult getsTicket(TicketRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Ticket.Gets;

            if (request.limit == 0)
            {
                request.limit = 10;
            }

            var obj = _ticketService.Gets(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [CustomController(parentName: MenuTitle.Ticket, parentController: "Ticket", controllerName: "Ticket", name: "Lấy chi tiết của ticket",
           actionName: "getDetails", method: "GET", route: "/ticket/getdetails", isPage: false,
           enumAction: EnumAction.View, isShow: false, icon: "")]
        [Route("/ticket/getdetails")]
        public IActionResult getDetails(TicketDetailRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Ticket.Detail;

            var obj = _ticketService.Detail(header, request);

            if (obj.data.images != null && obj.data.images.Count > 0)
            {
                foreach (var item in obj.data.images)
                {
                    item.path = CustomConfigApi.ImageDomain + item.path;
                }
            }

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        //[DisplayName("Xóa yêu cầu")]
        [CustomController(parentName: MenuTitle.Ticket, parentController: "Ticket", controllerName: "Ticket", name: "Xóa yêu cầu",
                   actionName: "Delete", method: "DELETE", route: "/ticket/delete", isPage: false,
                   enumAction: EnumAction.Delete, isShow: false, icon: "")]
        [HttpDelete]
        public IActionResult Delete(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Ticket.Delete;

            var request = new TicketDetailRequest
            {
                uuid = id
            };

            var obj = _ticketService.Delete(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        [Route("ticket/cancel/{uuid}")]
        public IActionResult Cancel(string uuid, TicketCancelRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Ticket.Cancel;
            request.ticket_uuid = uuid;
            var obj = _ticketService.Cancel(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        //[DisplayName("Danh sách ticket")]
        [CustomController(parentName: "", parentController: "Ticket", controllerName: "Ticket", name: MenuTitle.Ticket,
           actionName: "Index", method: "", route: "/ticket/index", isPage: true,
           enumAction: EnumAction.View, isShow: true, icon: Icon.Ticket)]
        public IActionResult Index()
        {
            // Start tranfer data Subheader
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Ticket,
                Title = MenuTitle.Ticket
            };

            ViewData[SubHeaderData] = subHeader;
            // End

            //Lấy danh sách nhân viên
            ViewBag.Staff = GetStaff();

            //Lấy danh sách nhân viên
            ViewBag.Customer = GetCustomer();

            ViewBag.State = Utilities.ExtensionMethods.ToSelectList<EnumTicketState>();
            ViewBag.Priority = Utilities.ExtensionMethods.ToSelectList<EnumTicketPriority>();

            return View();
        }

        //[DisplayName("Thêm mới ticket")]
        [CustomController(parentName: MenuTitle.Ticket, parentController: "Ticket", controllerName: "Ticket", name: "Thêm yêu cầu mới",
                   actionName: "Create", method: "", route: "/ticket/create", isPage: true,
                   enumAction: EnumAction.View, isShow: false, icon: "")]
        public IActionResult Create()
        {
            var model = new TicketUpsertRequest();
            ViewBag.Devices = GetDevices();
            ViewBag.Customer = GetCustomer();
            ViewBag.TicketState = Utilities.ExtensionMethods.ToSelectList<EnumTicketState>();
            ViewBag.TicketPriority = Utilities.ExtensionMethods.ToSelectList<EnumTicketPriority>();
            return View(model);
        }

        //[DisplayName("Action: Thêm mới ticket")]
        [CustomController(parentName: MenuTitle.Ticket, parentController: "Ticket", controllerName: "Ticket", name: "Thêm yêu cầu mới",
                   actionName: "Create", method: "POST", route: "/ticket/create", isPage: false,
                   enumAction: EnumAction.Add, isShow: false, icon: "")]
        [HttpPost("ticket/create")]
        public IActionResult Create(TicketUpsertRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Ticket.Create;
            if (!model.state.HasValue)
            {
                model.state = (int)EnumTicketState.New;
            }

            if (!model.priority.HasValue)
            {
                model.priority = (int)EnumTicketPriority.Normal;
            }

            var request = new TicketUpsertRequest
            {
                uuid = model.uuid,
                customer_uuid = model.customer_uuid,
                contact_name = model.contact_name,
                contact_phone_number = model.contact_phone_number,
                content = model.content,
                state = model.state,
                title = model.title,
                priority = model.priority,
                devices = model.devices,
            };

            var obj = _ticketService.Upsert(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {                
                return Ok(obj);
            }

            return NotFound(obj);
        }

        #region: Edit
        //[DisplayName("Cập nhật ticket")]
        [CustomController(parentName: MenuTitle.Ticket, parentController: "Ticket", controllerName: "Ticket", name: "Cập nhật yêu cầu",
                   actionName: "Edit", method: "", route: "/ticket/edit", isPage: true,
                   enumAction: EnumAction.View, isShow: true, icon: Icon.Ticket)]
        public IActionResult Edit(string id)
        {
            var model = new TicketUpsertRequest();
            //ViewBag.Devices = GetDevices();
            //ViewBag.Task = GetTask(id);
            //ViewBag.CustomerMySelft = GetCustomerMySelft();
            //ViewBag.Customer = GetCustomer();
            ViewBag.TicketState = Utilities.ExtensionMethods.ToSelectList<EnumTicketState>();
            ViewBag.TicketPriority = Utilities.ExtensionMethods.ToSelectList<EnumTicketPriority>();

            if (string.IsNullOrEmpty(id))
            {
                return View(model);
            }
            else
            {
                var header = Header;
                header.Action = CustomConfigApi.Ticket.Detail;

                var request = new TicketDetailRequest
                {
                    uuid = id
                };

                var obj = _ticketService.Detail(header, request);
                ViewBag.Contacts = GetContacts(obj.data.customer.uuid);

                var subHeader = new SubHeaderModel
                {
                    Icon = Icon.Ticket,
                    Title = MenuTitle.Ticket,
                    SubTitle = obj.data.title,
                };

                ViewData[SubHeaderData] = subHeader;
                if (obj.data != null && obj.error == 0)
                {

                    model = new TicketUpsertRequest
                    {
                        uuid = obj.data.uuid,
                        contact_name = obj.data.contact_name,
                        customer_uuid = obj.data.customer.uuid,
                        content = obj.data.content,
                        title = obj.data.title,
                        contact_phone_number = obj.data.contact_phone_number,
                        state = obj.data.state,
                        state_id = obj.data.state,
                        priority = obj.data.priority,
                        rating = obj.data.rating,
                        feedback = obj.data.feedback,
                        devices = obj.data.devices,
                        customer_name = obj.data.customer.name,
                        ticket_devices = obj.data.ticket_devices,
                        contact_uuid = obj.data.contact_uuid,
                        task = obj.data.task,
                        
                        //images = imageList,
                    };
                    ViewBag.reason = obj.data.reason;
                    if (obj.data.devices != null && obj.data.devices.Count > 0)
                    {
                        model.device_uuid_str = JsonConvert.SerializeObject(obj.data.devices.Select(x => x.device_id));
                        //ViewBag.abc = JsonConvert.SerializeObject(obj.data.devices.Select(x => x.uuid));
                    }
                }

                return View(model);
            }
        }

        //[DisplayName("Action: Cập nhật ticket")]
        [CustomController(parentName: MenuTitle.Ticket, parentController: "Ticket", controllerName: "Ticket", name: "Cập nhật yêu cầu",
                   actionName: "Edit", method: "PUT", route: "ticket/edit/{uuid}", isPage: false,
                   enumAction: EnumAction.Edit, isShow: false, icon: "")]
        [HttpPut("ticket/edit/{uuid}")]
        public IActionResult Edit(TicketUpsertRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Ticket.Upsert;

            var request = new TicketUpsertRequest
            {
                uuid = model.uuid,
                customer_uuid = model.customer_uuid,
                contact_name = model.contact_name,
                contact_phone_number = model.contact_phone_number,
                content = model.content,
                state = model.state,
                title = model.title,
                devices = model.devices,
                priority = model.priority,
                contact_uuid = model.contact_uuid
                //images = !string.IsNullOrEmpty(model.image_uuids) ? model.image_uuids.Split(",".ToCharArray()).ToList() : null
            };

            var obj = _ticketService.Upsert(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }
        #endregion

        [DisplayName("Chi tiết ticket")]
        [HttpGet]
        public IActionResult Details(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Ticket.Detail;
            var request = new TicketDetailRequest
            {
                uuid = id,
            };

            var obj = _ticketService.Detail(header, request);

            if(obj.data.images != null && obj.data.images.Count > 0)
            {
                foreach (var item in obj.data.images)
                {
                    item.path = CustomConfigApi.ImageDomain + item.path;
                }
            }

            if(obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        //[DisplayName("Đánh giá ticket")]
        [CustomController(parentName: MenuTitle.Ticket, parentController: "Ticket", controllerName: "Ticket", name: "Đánh giá yêu cầu",
                   actionName: "RateTicket", method: "", route: "ticket/rateticket/{uuid}", isPage: true,
                   enumAction: EnumAction.View, isShow: false, icon: "")]
        [HttpGet("ticket/rateticket/{uuid}")]
        public IActionResult RateTicket(string uuid)
        {
            var model = new TicketRateRequest();
            model.uuid = uuid;

            return View(model);
        }

        //[DisplayName("Action: Đánh giá ticket")]
        [CustomController(parentName: MenuTitle.Ticket, parentController: "Ticket", controllerName: "Ticket", name: "Đánh giá yêu cầu",
                   actionName: "RateTicket", method: "POST", route: "ticket/rateticket/{uuid}", isPage: false,
                   enumAction: EnumAction.Add, isShow: false, icon: "")]
        [HttpPost("ticket/rateticket/{uuid}")]
        public IActionResult ReteTicket(TicketRateRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Ticket.Rate;

            var request = new TicketRateRequest()
            {
                uuid = model.uuid,
                feedback = model.feedback,
                rating = model.rating,
            };

            var obj = _ticketService.Rate(header, request);


            if(obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [HttpGet("ticket/getstaffajax/{keyword}")]
        public IActionResult GetStaffAjax(string keyword)
        {
            var header = Header;
            header.Action = CustomConfigApi.Staff.Creator;

            var request = new StaffListRequest
            {
                keyword = keyword,
            };

            var obj = _staffService.GetList(header, request);
            if (obj.data != null && obj.error == 0)
            {
                for (var i = 0; i < obj.data.items.Count; i++)
                {
                    if (obj.data.items[i].type == 1)
                    {
                        obj.data.items[i].name = "Khách hàng - " + obj.data.items[i].name;
                    }
                    if (obj.data.items[i].type == 2)
                    {
                        obj.data.items[i].name = "Nhân viên - " + obj.data.items[i].name;
                    }
                }
                return Ok(obj);
            }

            return NotFound(obj);
        }

        //lấy danh sách nhân viên
        private List<StaffModel> GetStaff()
        {
            var header = Header;
            header.Action = CustomConfigApi.Staff.Creator;

            var request = new StaffListRequest
            {
               
            };

            var obj = _staffService.GetList(header, request);
            if(obj.data != null && obj.error == 0)
            {
                for(var i = 0;i< obj.data.items.Count; i++)
                {
                    if(obj.data.items[i].type == 1)
                    {
                        obj.data.items[i].name = "Khách hàng - " + obj.data.items[i].name;
                    }
                    if (obj.data.items[i].type == 2)
                    {
                        obj.data.items[i].name = "Nhân viên - " + obj.data.items[i].name;
                    }
                }
                return obj.data.items;
            }

            return null;
        }

        //Lấy danh sách thiết bị
        private List<DeviceModel> GetDevices()
        {
            var header = Header;
            header.Action = CustomConfigApi.Devices.Gets;

            var request = new DeviceSearchRequest
            {
                limit = 1000
            };

            var obj = _deviceService.Get(header, request);
            if(obj != null && (EnumError)obj.error == EnumError.Success && obj.data!=null)
            {
                return obj.data.items;
            }

            return null;
        }

        private List<CustomerModel> GetCustomer()
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.GetList;

            var obj = _customerService.GetList(header, null);
            
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return obj.data.items;
            }

            return null;
        }

        private List<ContactModel> GetContacts(string customer_uuid)
        {
            var contacts = new List<ContactModel>();

            var header = Header;
            header.Action = CustomConfigApi.Customers.GetsContact;

            var request = new ContactRequest()
            {
                customer_uuid = customer_uuid,
                limit = 1000,
            };

            var obj = _customerService.GetContact(header, request);

            if(obj != null && (EnumError)obj.error == EnumError.Success)
            {
                contacts = obj.data.items;
            }

            return contacts;
        }

        private List<CustomerModel> GetCustomerMySelft()
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.CustomerMySelft;

            var obj = _customerService.ListMySelft(header, null);

            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {

                return obj.data.items;
            }

            return null;
        }

        private List<TaskModel> GetTask(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Task.Gets;

            var request = new TaskRequest
            {
                limit = 1000,
                owner_uuid = id,
            };

            var obj = _taskService.Get(header, request);
            if(obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return obj.data.items;
            }

            return null;
        }        
    }
}