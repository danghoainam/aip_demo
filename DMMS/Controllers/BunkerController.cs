﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DMMS.Extensions;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class BunkerController : BaseController<BunkerController>
    {
        private readonly IBunkerService _bunkerService;
        private readonly ILocationNewService _locationNewService;
        private readonly ITreeCategoriesService _treeCategoriesService;
        private readonly IStaffService _staffService;
        private readonly IAccountService _accountService;
        private readonly ISupplierService _supplierService;
        private readonly IProjectService _projectService;
        public BunkerController(IBunkerService bunkerService, ILocationNewService locationNewService,
            ITreeCategoriesService treeCategoriesService, IStaffService staffService, IAccountService accountService,
            ISupplierService supplierService, IProjectService projectService)
        {
            _bunkerService = bunkerService;
            _locationNewService = locationNewService;
            _treeCategoriesService = treeCategoriesService;
            _staffService = staffService;
            _accountService = accountService;
            _supplierService = supplierService;
            _projectService = projectService;
        }

        [CustomController(parentName: MenuTitle.Bunker, parentController: "Bunker", controllerName: "Bunker", name: "Get danh sách kho",
           actionName: "gets", method: "GET", route: "/bunker/gets", isPage: false,
           enumAction: EnumAction.View, isShow: false, icon: "")]
        public IActionResult gets(BunkerRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.Get;
            if (request.limit == 0)
            {
                request.limit = 20;
            }

            var obj = _bunkerService.Gets(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [CustomController(parentName: MenuTitle.Bunker, parentController: "Bunker", controllerName: "Bunker", name: "Get danh sách thiết bị vật tư kho",
           actionName: "getSuppliesDeviceInBunker", method: "GET", route: "/bunker/getSuppliesDeviceInBunker/{storage_uuid}", isPage: false,
           enumAction: EnumAction.View, isShow: false, icon: "")]
        [Route("bunker/getSuppliesDeviceInBunker/{storage_uuid}")]
        public IActionResult getSuppliesDeviceInBunker(SuppliesDeviceInBunkerRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.GetSuppliesDeviceInBunker;

            if (request.limit == 0)
            {
                request.limit = 20;
            }

            var obj = _bunkerService.GetSuppliesDeviceInBunker(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                obj.data.items = obj.data.items.Where(w => w.quantity_exist > 0).ToList();
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [CustomController(parentName: MenuTitle.Bunker, parentController: "Bunker", controllerName: "Bunker", name: "Get danh sách nhân viên kho",
           actionName: "getStaffInBunker", method: "GET", route: "/bunker/getstaffinBunker", isPage: false,
           enumAction: EnumAction.View, isShow: false, icon: "")]
        [Route("/bunker/getstaffinbunker/{storage_uuid}")]
        public IActionResult getStaffInBunker(StaffInBunkerRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.GetStaffInBunker;

            if (request.limit == 0)
            {
                request.limit = 20;
            }
            var obj = _bunkerService.GetStaffInBunker(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [CustomController(parentName: MenuTitle.Bunker, parentController: "Bunker", controllerName: "Bunker", name: "Get danh sách đề xuất kho",
               actionName: "getOfferInBunker", method: "GET", route: "/bunker/getofferinBunker", isPage: false,
               enumAction: EnumAction.View, isShow: false, icon: "")]
        [Route("bunker/getofferinbunker/{storageUuid}")]
        public IActionResult getOfferInBunker(OfferInBunkerRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.GetOfferInBunker;

            if (request.limit == 0)
            {
                request.limit = 20;
            }
            var obj = _bunkerService.GetOfferInBunker(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [CustomController(parentName: MenuTitle.Bunker, parentController: "Bunker", controllerName: "Bunker", name: "Get danh sách lịch sử kho",
               actionName: "getHistoryOfBunker", method: "GET", route: "/bunker/gethistoryofbunker", isPage: false,
               enumAction: EnumAction.View, isShow: false, icon: "")]
        public IActionResult getHistoryOfBunker(HistoryOfBunkerRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.GetHistoryOfBunker;

            if (request.limit == 0)
            {
                request.limit = 20;
            }
            var obj = _bunkerService.GetHistoryOfBunker(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [CustomController(parentName: MenuTitle.Bunker, parentController: "Bunker", controllerName: "Bunker", name: "Get danh phiếu kiểm kê của kho",
               actionName: "getHistoryOfBunker", method: "GET", route: "/bunker/gethistoryofbunker", isPage: false,
               enumAction: EnumAction.View, isShow: false, icon: "")]
        [Route("/bunker/getinventoryinbunker/{storage_uuid}")]
        public IActionResult getInventoryInBunker(InventoryRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.GetInventoryInBunker;

            if (request.limit == 0)
            {
                request.limit = 20;
            }
            var obj = _bunkerService.GetInventoryInBunker(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [CustomController(parentName: "", parentController: "Bunker", controllerName: "Bunker", name: MenuTitle.Bunker,
                     actionName: "Index", method: "", route: "/bunker/index", isPage: true,
                     enumAction: EnumAction.View, isShow: true, icon: Icon.Bunker)]
        public IActionResult Index(BunkerModel model)
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Bunker,
                Title = MenuTitle.Bunker,
            };
            ViewData[SubHeaderData] = subHeader;

            ViewBag.Provinces = GetProvinces();
            ViewBag.States = Utilities.ExtensionMethods.ToSelectList<EnumBunkerState>();

            return View(model);
        }

        public IActionResult Create()
        {
            ViewBag.Provinces = GetProvinces();
            ViewBag.StaffNotInBunker = GetStaffNotInBunker();
            ViewBag.Unitsigneds = GetUnitsigned();

            var model = new BunkerUpsertRequest();

            return View(model);
        }

        [HttpPost]
        public IActionResult Create(BunkerUpsertRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.Upsert;

            if (model.storage_keeper.Count > 0 && model.storage_keeper[0] == null)
            {
                model.storage_keeper.RemoveAt(0);
            }

            var obj = _bunkerService.Upsert(header, model);

            if ((EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        public IActionResult Edit(string id)
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Bunker,
                Title = MenuTitle.Bunker,
            };
            ViewData[SubHeaderData] = subHeader;

            ViewBag.Unitsigneds = GetUnitsigned();

            ViewBag.States = Utilities.ExtensionMethods.ToSelectList<EnumBunkerState>();
            ViewBag.StatusExportTo = Utilities.ExtensionMethods.ToSelectList<EnumStateExportTo>();
            ViewBag.StatusOffers = Utilities.ExtensionMethods.ToSelectList<EnumOfferInBunker>();
            ViewBag.Provinces = GetProvinces();
            ViewBag.Supplies = GetSupplies();
            ViewBag.Categories = GetFirstCategory();

            var model = new BunkerUpsertRequest();

            var header = Header;
            header.Action = CustomConfigApi.Bunker.Details;

            var request = new BunkerDetailRequest
            {
                storage_uuid = id,
            };

            var obj = _bunkerService.Details(header, request);

            if ((EnumError)obj.error == EnumError.Success)
            {
                subHeader.SubTitle = obj.data.name;

                if (obj.data.province.id != null) ViewBag.Towns = GetTowns(obj.data.province.id);
                if (obj.data.town.id != null) ViewBag.Villages = GetVillages(obj.data.town.id);

                model = new BunkerUpsertRequest()
                {
                    uuid = obj.data.uuid,
                    code = obj.data.code,
                    name = obj.data.name,
                    address = obj.data.address,
                    state = obj.data.state,
                    province_id = obj.data.province.id,
                    town_id = obj.data.town.id,
                    village_id = obj.data.village.id,
                    unitSignedUuid = obj.data.unit_signed.unit_signed_uuid,
                    unitSignedName = obj.data.unit_signed.unit_signed_name,
                };

                return View(model);
            }
            return View(model);
        }

        [HttpPut]
        public IActionResult Edit(BunkerUpsertRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.Update;

            var obj = _bunkerService.Update(header, request);

            if ((EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [HttpDelete]
        public IActionResult Delete(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.Delete;

            var request = new BunkerDetailRequest()
            {
                storage_uuid = id
            };

            var obj = _bunkerService.Delete(header, request);

            if ((EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [HttpGet("bunker/importsupplies/{type}/{storage_uuid}/{name}")]
        public IActionResult ImportSupplies(int type, string storage_uuid, string name)
        {
            ViewBag.Supplies = GetSupplies();
            ViewBag.Staffs = GetStaffs(1);
            ViewBag.Suppliers = GetSuppliers();
            ViewBag.Bunkers = GetBunker();
            ViewBag.ToName = name;
            if (type == 3)
            {
                ViewBag.ListExports = GetListExport(storage_uuid);
            }
            var profile = GetInfo();
            var model = new ImportBunkerRequest()
            {
                type = type,
                to_staff_uuid = profile.uuid,
                to_staff_name = profile.name,
                to_uuid = storage_uuid,

            };

            return View(model);
        }

        [HttpPost("bunker/importsupplies")]
        public IActionResult ImportSupplies(ImportBunkerRequest model)
        {
            //if (model.type == 1 || model.type == 3)
            //{
            //    foreach (var removeimei in model.detail)
            //    {
            //        if (removeimei.device != null)
            //        {
            //            foreach (var setnull in removeimei.device)
            //            {
            //                //setnull.imei = null;
            //                setnull.serial = null;
            //                setnull.expire_device = null;
            //            }
            //        }
            //    }
            //}
            var header = Header;
            header.Action = CustomConfigApi.Bunker.ImportSupplies;

            foreach (var item in model.detail)
            {
                if (!string.IsNullOrEmpty(item.unit_price_string))
                {
                    item.unit_price = Convert.ToInt64(item.unit_price_string.Replace(",", ""));
                }
            }

            foreach (var item in model.detail)
            {
                item.device?.RemoveAll(t =>
                {
                    return t.serial == null;
                });
            }

            model.detail?.RemoveAll(r => r.category_id == null);
            if (model.type == 2) model.from_uuid = model.supplier_uuid;

            var obj = _bunkerService.ImportSupplies(header, model);
            Debug.WriteLine("Msg: {0}", obj.msg);
            if ((EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        [HttpGet("bunker/exportsupplies/{type}/{storage_uuid}/{name}")]
        public IActionResult ExportSupplies(int type, string storage_uuid, string name)
        {
            ViewBag.Suppliers = GetSuppliers();
            ViewBag.Supplies = GetSupplies();
            ViewBag.Bunkers = GetBunker();
            ViewBag.Staff = GetStaffInBunker(storage_uuid);
            ViewBag.StaffTech = GetStaffs(1);
            ViewBag.SuppliesInBunker = GetListSuppliesInBunker(storage_uuid);
            var info = GetInfo();
            var model = new ExportBunkerRequest()
            {
                //from_staff_uuid = info.uuid,
                //from_staff_name = info.name,
                type = type,
                from_uuid = storage_uuid,
                from_name = name
            };

            return View(model);
        }

        [HttpGet("bunker/ExportToOrder/{storage_uuid}/{oders_uuid}")]
        public IActionResult ExportToOrder(string storage_uuid, string oders_uuid)
        {
            var model = new ExportBunkerRequest();
            var header = Header;
            header.Action = CustomConfigApi.Bunker.DetailOfferInBunker;
            var request = new DetailOfferInBunkerRequest()
            {
                order_uuid = oders_uuid,
            };
            var getname = GetBunker();
            foreach (var item in getname)
            {
                if (item.uuid == storage_uuid)
                {
                    ViewBag.Getname = item.name;
                }
            }
            var obj = _bunkerService.DetailOfferInBunker(header, request);
            ViewBag.StateOfOrder = obj.data.state;
            model.categories = obj.data.categories;
            //ViewBag.Suppliers = GetSuppliers();
            //ViewBag.Supplies = GetSupplies();
            //ViewBag.Bunkers = GetBunker();
            ViewBag.Staff = GetStaffInBunker(storage_uuid);
            //ViewBag.StaffTech = GetStaffs(1);
            //ViewBag.SuppliesInBunker = GetListSuppliesInBunker(storage_uuid);
            return View(model);
        }

        [HttpPost]
        public IActionResult ExportToOrder(ExportBunkerRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.ExportSupplies;

            foreach (var item in model.detail)
            {
                if (!string.IsNullOrEmpty(item.unit_price_string))
                {
                    item.unit_price = Convert.ToInt64(item.unit_price_string.Replace(",", ""));
                }

                if (item.real_quantity == null)
                    item.real_quantity = 0;
            }

            model.type = 1;

            var obj = _bunkerService.ExportSupplies(header, model);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);

        }

        [HttpGet("bunker/ExportToOrderTo/{storage_uuid}/{oders_uuid}")]
        public IActionResult ExportToOrderTo(string storage_uuid, string oders_uuid)
        {
            var model = new ImportBunkerRequest();
            var header = Header;
            header.Action = CustomConfigApi.Bunker.DetailExport;
            var request = new ExportDetailRequest()
            {
                export_uuid = oders_uuid,
            };
            var getname = GetBunker();
            foreach (var item in getname)
            {
                if (item.uuid == storage_uuid)
                {
                    ViewBag.Getname = item.name;
                }
            }
            var obj = _bunkerService.DetailExport(header, request);
            //model.document_origin = obj.data.description;
            model.to_uuid = obj.data.to.uuid;
            ViewBag.ToName = obj.data.to.name;
            ViewBag.fromName = obj.data.from.name;
            model.from_uuid = obj.data.from.uuid;
            model.description = obj.data.description;
            model.document_origin = obj.data.code;
            model.type = obj.data.type;
            var request1 = new ExDetailCategoriesRequest()
            {
                exportUuid = oders_uuid,
                limit = -1,
            };
            var header1 = Header;
            header1.Action = CustomConfigApi.Bunker.ExDetailCategories;
            var obj1 = _bunkerService.ExDetailCategories(header1, request1);
            ViewBag.categories = obj1.data.items;
            ViewBag.Suppliers = GetSuppliers();
            ViewBag.Supplies = GetSupplies();
            ViewBag.Bunkers = GetBunker();
            ViewBag.Staff = GetStaffInBunker(storage_uuid);
            ViewBag.StaffTech = GetStaffs(1);
            ViewBag.SuppliesInBunker = GetListSuppliesInBunker(storage_uuid);
            return View(model);
        }

        [HttpPost("bunker/ExportToOrderTo")]
        public IActionResult ExportToOrderTo(ImportBunkerRequest model)
        {

            //if (model.type == 1 || model.type == 3)
            //{
            //    foreach (var removeimei in model.detail)
            //    {
            //        if (removeimei.device != null)
            //        {
            //            foreach (var setnull in removeimei.device)
            //            {


            //                //setnull.imei = null;
            //                setnull.serial = null;
            //                setnull.expire_device = null;


            //            }
            //        }
            //    }

            //}
            var header = Header;
            header.Action = CustomConfigApi.Bunker.ImportSupplies;

            foreach (var item in model.detail)
            {
                if (!string.IsNullOrEmpty(item.unit_price_string))
                {
                    item.unit_price = Convert.ToInt64(item.unit_price_string.Replace(",", ""));
                }
            }
            var remove = new DeviceInDetail();
            foreach (var item in model.detail)
            {
                if (item.device != null)
                {
                    foreach (var item1 in item.device)
                    {
                        if (item1.serial == null)
                        {
                            remove = item1;
                        }
                    }
                    item.device.Remove(remove);
                }
            }

            var obj = _bunkerService.ImportSupplies(header, model);
            Debug.WriteLine("Msg: {0}", obj.msg);
            if ((EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        [HttpPost("bunker/exportsupplies")]
        [HttpPost("bunker/exportsupplies/{invetory_uuid}")]
        public IActionResult ExportSupplies(ExportBunkerRequest model, string invetory_uuid)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.ExportSupplies;

            foreach (var item in model.detail)
            {
                if (!string.IsNullOrEmpty(item.unit_price_string))
                {
                    item.unit_price = Convert.ToInt64(item.unit_price_string.Replace(",", ""));
                }
            }

            model.detail?.RemoveAll(p => p.category_id == 0);

            //Nếu là xuất trả nhà cung cấp thì kho đích = nhà cung cấp
            if (model.type == 2) model.to_uuid = model.supplier_uuid;
            //Nếu xuất tiêu hao từ kiểm kê thì kho đích = phiếu kiểm kê
            else if (model.type == 5 && !string.IsNullOrEmpty(invetory_uuid)) model.to_uuid = invetory_uuid;

            var obj = _bunkerService.ExportSupplies(header, model);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        [HttpGet]
        public IActionResult AddStaff(string id)
        {
            var model = new AddStaffInBunkerRequest();
            model.storage_uuid = id;

            ViewBag.StaffNotIn = GetStaffNotInBunker();

            return View(model);
        }

        [HttpPost]
        public IActionResult AddStaff(AddStaffInBunkerRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.AddStaffInBunker;

            if (model.storage_keeper[0] == null || model.storage_keeper[0] == "")
            {
                model.storage_keeper.RemoveAt(0);
            }

            var obj = _bunkerService.AddStaffInBunker(header, model);

            if ((EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [HttpGet("bunker/addoffer/{storage_uuid}/{storage_name}")]
        public IActionResult AddOffer(string storage_uuid, string storage_name)
        {
            ViewBag.Supplies = GetSupplies();
            var acc = GetInfo();

            var model = new OfferInBunkerAddRequest()
            {
                storage_uuid = storage_uuid,
                storage_name = storage_name,
                creator_name = acc.name,
                creator_uuid = acc.uuid,
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult AddOffer(OfferInBunkerAddRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.AddOfferInBunker;

            var obj = _bunkerService.AddOfferInBunker(header, model);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        [HttpGet("bunker/DetailOffer/{uuid}/{storge_uuid}/{state_bunker}")]
        public IActionResult DetailOffer(string uuid, string storge_uuid, int? state_bunker)
        {
            ViewBag.OfferSates = Utilities.ExtensionMethods.ToSelectList<EnumOfferInBunker>().ToList();
            ViewBag.StateBunker = state_bunker;
            var model = new OfferInBunkerModel();

            var header = Header;
            header.Action = CustomConfigApi.Bunker.DetailOfferInBunker;
            var subHeader = new SubHeaderModel()
            {
                Icon = Icon.Bunker,
                Title = MenuTitle.Bunker,
                SubTitle = SubMenuTitle.Bunker,
            };
            ViewData[SubHeaderData] = subHeader;
            var request = new DetailOfferInBunkerRequest()
            {
                order_uuid = uuid,
            };

            var obj = _bunkerService.DetailOfferInBunker(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                model = obj.data;
                subHeader.SubTitleSmal = obj.data.code;
                model.content = obj.data.content;
                model.reviewer = obj.data.reviewer;

            }
            model.store_uuid = storge_uuid;
            return View(model);
        }
        [HttpGet("bunker/ExportToDetail/{uuid}/{storge_uuid}")]
        public IActionResult ExportToDetail(string uuid, string storge_uuid)
        {

            var model = new ExportDetailResponese();

            var header = Header;
            header.Action = CustomConfigApi.Bunker.DetailExport;
            var subHeader = new SubHeaderModel()
            {
                Icon = Icon.Bunker,
                Title = MenuTitle.Bunker,
                SubTitle = SubMenuTitle.Bunker,
            };
            ViewData[SubHeaderData] = subHeader;
            var request = new ExportDetailRequest()
            {
                export_uuid = uuid,
            };

            var obj = _bunkerService.DetailExport(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                model = obj.data;
                //subHeader.SubTitleSmal = obj.data.code;

            }
            model.store_uuid = storge_uuid;
            return View(model);
        }

        [HttpDelete("bunker/deletestaff")]
        public IActionResult DeleteStaff(DeleteStaffRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.DeleteStaffInBunker;

            var obj = _bunkerService.DeleteStaffInBunker(header, request);

            if ((EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [Route("bunker/addinventory/{storage_uuid}")]
        public IActionResult AddInventory(string storage_uuid)
        {
            ViewBag.Supplies = GetListSuppliesInBunker(storage_uuid);
            ViewBag.Staffs = GetStaffs(null);
            var profile = GetInfo();
            var model = new AddInventoryRequest()
            {
                creator_name = profile.name,
                creator_uuid = profile.uuid
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult AddInventory(AddInventoryRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.AddInventoryInBunker;
            request.time_stock_take = DateTime.Now;

            var obj = _bunkerService.AddInventoryInBunker(header, request);

            if ((EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        //Chi tiết kiểm kê
        [HttpGet]
        public IActionResult DetailInventory(string id)
        {
            var model = new InventoryModel();

            var header = Header;
            header.Action = CustomConfigApi.Bunker.DetailInventoryInBunker;

            var request = new InventoryDetailRequest()
            {
                inventory_uuid = id,
            };

            var obj = _bunkerService.DetailInventoryInBunker(header, request);

            if ((EnumError)obj.error == EnumError.Success)
            {
                ViewBag.SuppliesInInventory = GetSuppliesDeviceInInventory(obj.data.uuid);

                model = obj.data;

                return View(model);
            }

            return View(model);
        }

        //Xuất tiêu hao từ phiếu kiểm kê
        [HttpGet("bunker/exporttoinventory/{id}/{storage_uuid}/{storage_name}")]
        public IActionResult ExportToInventory(string id, string storage_uuid, string storage_name)
        {
            var model = new ExportBunkerRequest()
            {
                from_uuid = storage_uuid,
                from_name = storage_name
            };
            var header = Header;
            header.Action = CustomConfigApi.Bunker.DetailInventoryInBunker;

            var request = new InventoryDetailRequest()
            {
                inventory_uuid = id
            };

            var obj = _bunkerService.DetailInventoryInBunker(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                var devices = GetSuppliesDeviceInInventory(obj.data.uuid);
                ViewBag.CountSupplies = devices.Where(w => w.type == 2 && w.deviations_quantity > 0).Count();
                ViewBag.LabortTools = devices.Where(w => w.type == 3 && w.deviations_quantity > 0).Count();
                ViewBag.SuppliesInInventory = devices;
                ViewBag.Inventory = obj.data;


                return View(model);
            }
            return View(model);
        }

        public IActionResult DetailImportTicket(ImportDetailRequest request)
        {
            var model = new ImportDetailResponese();
            var header = Header;
            header.Action = CustomConfigApi.Bunker.DetailImport;
            var obj = _bunkerService.DetailImport(header, request);
            model = obj.data;
            return View(model);
        }
        public IActionResult DetailExportDetail(ExportDetailRequest request)
        {
            var model = new ExportDetailResponese();

            var header = Header;
            header.Action = CustomConfigApi.Bunker.DetailExport;
            var obj = _bunkerService.DetailExport(header, request);
            model = obj.data;
            return View(model);
        }

        public IActionResult DetailMessengerImportDetail(ExportDetailRequest request)
        {
            var model = new ExportDetailResponese();
            var header = Header;
            header.Action = CustomConfigApi.Bunker.DetailExport;
            var obj = _bunkerService.DetailExport(header, request);
            model = obj.data;
            return View(model);
        }

        [HttpGet("bunker/MessengerImport/{id}")]
        public IActionResult MessengerImport(string id)
        {
            var acc = GetInfo();
            var model = new ImportBunkerRequest();
            ViewBag.TypeCategory = Utilities.ExtensionMethods.ToSelectList<Utilities.ListSupplies>().ToList();
            //Lấy thông tin chung
            var header = Header;
            header.Action = CustomConfigApi.Bunker.DetailExport;
            var request = new ExportDetailRequest()
            {
                export_uuid = id
            };
            var obj = _bunkerService.DetailExport(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                model = new ImportBunkerRequest()
                {
                    exportUuid = id,
                    document_origin = obj.data.code,
                    type = obj.data.type == 10 ? 1 : obj.data.type,
                    description = obj.data.description,
                    from_uuid = obj.data.from.uuid,
                    to_uuid = obj.data.to.uuid,
                    to_staff_uuid = obj.data.staff_create.uuid,
                };
                ViewBag.Staff_create = obj.data.staff_create.name;
                ViewBag.ToName = obj.data.to.name;
                ViewBag.FromName = obj.data.from.name;
                ViewBag.UuidOfExportDetail = obj.data.uuid;
            }

            //Lấy thông tin thiết bị-vật tư trong phiếu
            var headerCategories = Header;
            headerCategories.Action = CustomConfigApi.Bunker.ExDetailCategories;
            var requestCategories = new ExDetailCategoriesRequest()
            {
                exportUuid = id,
                limit = -1
            };
            var objCategories = _bunkerService.ExDetailCategories(headerCategories, requestCategories);
            if (objCategories != null && (EnumError)objCategories.error == EnumError.Success)
            {
                ViewBag.Categories = objCategories.data.items;
            }

            return View(model);
        }

        [HttpPost]
        public IActionResult MessengerImport(ImportBunkerRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.ImportSupplies;

            var obj = _bunkerService.ImportSupplies(header, model);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [Route("bunker/DetailCategoriesImport/{importUuid}")]
        public IActionResult DetailCategoriesImport(ImDetailCategoriesRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.ImDetailCategories;
            var obj = _bunkerService.ImDetailCategories(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        [Route("bunker/DetailCategoriesExport/{exportUuid}")]
        public IActionResult DetailCategoriesExport(ExDetailCategoriesRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.ExDetailCategories;
            var obj = _bunkerService.ExDetailCategories(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        public IActionResult DetailSupplies()
        {
            return View();
        }
        public IActionResult DetailDeviceIm(ImDetailDeviceRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.ImDetailCategoriesDevices;
            var obj = _bunkerService.ImDetailDevice(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound(obj);

        }
        public IActionResult DetailDeviceEx(ExDetailDeviceRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.ExDetailCategoriesDevices;
            var obj = _bunkerService.ExDetailDevice(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        //Từ chối nhập từ thông báo phiếu nhập
        [HttpGet("bunker/rejectexport/{export_uuid}")]
        public IActionResult RejectExport(RejectExportRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.RejectExport;
            var obj = _bunkerService.RejectExport(header, request);
            if (obj.error == 0)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        //Từ chối nhập từ thông báo phiếu nhập
        [HttpGet("bunker/rollbackdevice")]
        public IActionResult RollbackDevice(RejectExportRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.RollbackDevice;
            var obj = _bunkerService.RejectExport(header, request);
            if (obj.error == 0)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        //-------------------------------------------
        [HttpGet("bunker/getdetailsuppliesinbunker/{storage_uuid}/{category_id}")]
        public IActionResult GetDetailSuppliesInBunker(SuppliesDeviceInBunkerRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.DetailSuppliesInBunker;

            if (request.limit == 0)
            {
                request.limit = 10;
            }

            var obj = _bunkerService.DetailSuppliesInBunker(header, request);

            if ((EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [Route("bunker/gettowns/{id}")]
        public IActionResult GetTowns(int? id)
        {
            var entries = new List<LocationNewModel>();
            var header = Header;
            header.Action = CustomConfigApi.LocationNew.GetTowns;

            var request = new TownRequest()
            {
                limit = -1,
                province_id = id,
            };

            var obj = _locationNewService.GetTowns(header, request);

            if ((EnumError)obj.error == EnumError.Success)
            {
                entries = obj.data.items;
                return Ok(entries);
            }
            return NotFound(entries);
        }

        [Route("bunker/getvillages/{id}")]
        public IActionResult GetVillages(int? id)
        {
            var entries = new List<LocationNewModel>();
            var header = Header;
            header.Action = CustomConfigApi.LocationNew.GetVillages;

            var request = new VillageRequest()
            {
                limit = -1,
                town_id = id,
            };

            var obj = _locationNewService.GetVillages(header, request);

            if ((EnumError)obj.error == EnumError.Success)
            {
                entries = obj.data.items;
                return Ok(entries);
            }
            return NotFound(entries);
        }

        [Route("bunker/DetailSuppliesAjax/{category_id}/{storage_uuid}")]
        [Route("bunker/DetailSuppliesAjax/{category_id}")]
        public IActionResult DetailSuppliesAjax(TreeCategoriesDetail request)
        {
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.Detail;

            var obj = _treeCategoriesService.Detail(header, request);

            if ((EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [Route("bunker/getcategory/{id}")]
        public IActionResult GetCategory(int? id)
        {
            var entries = new List<TreeCategoriesModel>();

            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.Subtree;

            var request = new TreeCategoriesRequest()
            {
                level = 0,
                category_type = 0,
                limit = -1,
                parent_id = id,
            };

            var obj = _treeCategoriesService.Get(header, request);
            if ((EnumError)obj.error == EnumError.Success)
            {
                entries = obj.data.items;
                return Ok(entries);
            }
            return NotFound(entries);
        }
        [Route("bunker/ListImport/{storageUuid}")]
        public IActionResult ListImport(ListImportRequest request)
        {

            var header = Header;
            header.Action = CustomConfigApi.Bunker.ListImport;
            var obj = _bunkerService.ListImport(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                foreach (var check in obj?.data?.items)
                {
                    if (check.StaffFrom != null)
                    {
                        check.namefrom = check.StaffFrom.name;
                    }
                    else if (check.StorageFrom != null)
                    {
                        check.namefrom = check.StorageFrom.name;
                    }
                    else if (check.SupplierFrom != null)
                    {
                        check.namefrom = check.SupplierFrom.name;
                    }
                }
                return Ok(obj);
            }

            return NotFound(obj);
        }
        [Route("bunker/ListExport/{storageUuid}")]
        public IActionResult ListExport(ListImportRequest request)
        {

            var header = Header;
            header.Action = CustomConfigApi.Bunker.ListExport;
            var obj = _bunkerService.ListImport(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                foreach (var check in obj?.data?.items)
                {

                    if (check.storageTo != null)
                    {
                        check.nameto = check.storageTo.name;
                    }
                    else if (check.staffTo != null)
                    {
                        check.nameto = check.staffTo.name;
                    }
                    else if (check.taskTo != null)
                    {
                        check.nameto = check.taskTo.name;
                    }
                    else if (check.supplierTo != null)
                    {
                        check.nameto = check.supplierTo.name;
                    }
                }
                return Ok(obj);
            }

            return NotFound(obj);
        }
        [Route("bunker/ListExportTo/{storageUuid}")]
        public IActionResult ListExportTo(ListImportRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.ListExportTo;
            var obj = _bunkerService.ListExport(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                foreach (var check in obj?.data?.items)
                {
                    if (check.StaffFrom != null)
                    {
                        check.namefrom = check.StaffFrom.name;
                    }
                    else if (check.StorageFrom != null)
                    {
                        check.namefrom = check.StorageFrom.name;
                    }
                    else if (check.SupplierFrom != null)
                    {
                        check.namefrom = check.SupplierFrom.name;
                    }
                }
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [HttpGet("bunker/DetailDeviceInInventoryAjax/{id}/{category_id}")]
        public IActionResult DetailDeviceInInventoryAjax(string id, int category_id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.DetailDeviceInInventory;

            var request = new SuppliesInInventoryRequest()
            {
                category_id = category_id,
                inventory_uuid = id,
            };

            var obj = _bunkerService.DetailDeviceInInventory(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        //------------------------------------------------------
        private List<BunkerModel> GetBunker()
        {
            var entries = new List<BunkerModel>();

            var header = Header;
            header.Action = CustomConfigApi.Bunker.Get;

            var request = new BunkerRequest()
            {
                limit = -1,
            };

            var obj = _bunkerService.Gets(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                entries = obj.data.items;
                return entries;
            }
            return entries;
        }

        private List<LocationNewModel> GetProvinces()
        {
            var entries = new List<LocationNewModel>();

            var header = Header;

            header.Action = CustomConfigApi.LocationNew.GetProvinces;

            var request = new ProvinceRequest
            {
                limit = -1,
            };

            var obj = _locationNewService.GetProvinces(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                if (obj.data != null && obj.data.items != null)
                {
                    entries = obj.data.items;
                }
            }

            return entries;
        }

        private List<SuppliesDeviceInBunkerModel> GetListSuppliesInBunker(string storage_uuid)
        {
            var entries = new List<SuppliesDeviceInBunkerModel>();

            var header = Header;
            header.Action = CustomConfigApi.Bunker.GetSuppliesDeviceInBunker;

            var request = new SuppliesDeviceInBunkerRequest()
            {
                storage_uuid = storage_uuid,
                limit = -1,
            };

            var obj = _bunkerService.GetSuppliesDeviceInBunker(header, request);

            if ((EnumError)obj.error == EnumError.Success)
            {
                entries = obj.data.items.Where(w => w.quantity_exist > 0).ToList();
                return entries;
            }
            return entries;
        }

        private List<StaffInBunkerModel> GetStaffInBunker(string id)
        {
            var entries = new List<StaffInBunkerModel>();

            var header = Header;
            header.Action = CustomConfigApi.Bunker.GetStaffInBunker;

            var request = new StaffInBunkerRequest
            {
                storage_uuid = id,
                limit = -1,
            };

            var obj = _bunkerService.GetStaffInBunker(header, request);

            if ((EnumError)obj.error == EnumError.Success)
            {
                entries = obj.data.items;
                return entries;
            }

            return entries;
        }

        private List<StaffInBunkerModel> GetStaffNotInBunker()
        {
            var entries = new List<StaffInBunkerModel>();

            var header = Header;
            header.Action = CustomConfigApi.Bunker.GetStaffNotInBunker;

            var request = new StaffInBunkerRequest
            {
                limit = -1,
            };

            var obj = _bunkerService.GetStaffNotInBunker(header, request);

            if ((EnumError)obj.error == EnumError.Success)
            {
                entries = obj.data.items;
                return entries;
            }

            return entries;
        }

        private List<TreeCategoriesModel> GetSupplies()
        {
            var entries = new List<TreeCategoriesModel>();

            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.SearchLeaf;

            var request = new CategoreSearchLeaf()
            {
                category_type = 0,
                limit = -1
            };

            var obj = _treeCategoriesService.SearchLeaf(header, request);
            if ((EnumError)obj.error == EnumError.Success)
            {
                entries = obj.data.items;
                return entries;
            }
            return entries;
        }

        public List<TreeCategoriesModel> GetFirstCategory()
        {
            var entries = new List<TreeCategoriesModel>();

            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.Subtree;

            var request = new TreeCategoriesRequest()
            {
                level = 0,
                category_type = 0,
                limit = -1,
            };

            var obj = _treeCategoriesService.Get(header, request);
            if ((EnumError)obj.error == EnumError.Success)
            {
                entries = obj.data.items;
                return entries;
            }
            return entries;
        }

        private List<StaffModel> GetStaffs(int? type)
        {
            var entries = new List<StaffModel>();

            var header = Header;
            header.Action = CustomConfigApi.Staff.GetsList;

            var request = new StaffListRequest()
            {
                type = type,
                limit = 1000
            };

            var obj = _staffService.GetList(header, request);

            if ((EnumError)obj.error == EnumError.Success)
            {
                entries = obj.data.items;
                return entries;
            }
            return entries;
        }

        private List<SuppliesDeviceInBunkerModel> GetSuppliesDeviceInInventory(string id)
        {
            var entries = new List<SuppliesDeviceInBunkerModel>();

            var header = Header;
            header.Action = CustomConfigApi.Bunker.GetSuppliesDeviceInInventory;

            var request = new SuppliesInInventoryRequest()
            {
                inventory_uuid = id,
                limit = -1,
                category_id = null
            };

            var obj = _bunkerService.GetSuppliesDeviceInInventory(header, request);

            if ((EnumError)obj.error == EnumError.Success)
            {
                entries = obj.data.items;
                foreach (var item in entries)
                {
                    if (item.type == 1)
                    {
                        item.detail_temp = DetailDeviceInInventory(id, item.category_id);
                    }
                }
                return entries;
            }
            return entries;
        }

        private List<DetailDeviceInInventoryModel> DetailDeviceInInventory(string id, int category_id)
        {
            var entries = new List<DetailDeviceInInventoryModel>();
            var header = Header;
            header.Action = CustomConfigApi.Bunker.DetailDeviceInInventory;

            var request = new SuppliesInInventoryRequest()
            {
                category_id = category_id,
                inventory_uuid = id,
            };

            var obj = _bunkerService.DetailDeviceInInventory(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                entries = obj.data.items.Select(s => new DetailDeviceInInventoryModel
                {
                    uuid = s.uuid,
                    prefix_name = s.prefix_name,
                    category_id = s.category_id,
                    code = s.code,
                    expire_device = s.expire_device,
                    name = s.name,
                    serial = s.serial,
                    state_in_inventory = s.state_in_inventory,
                    type = s.type
                }).ToList();

                return entries;
            }
            return entries;
        }

        //private List<>

        //Lấy thông tin tài khoản đăng nhập
        private AccountModel GetInfo()
        {
            var model = new AccountModel();
            var header = Header;
            header.Action = CustomConfigApi.Account.Profile;

            var obj = _accountService.Profile(header);
            if ((EnumError)obj.error == EnumError.Success)
            {
                model = obj.data.user_info;
                return model;
            }
            return model;
        }

        //Lấy danh sách nhà cung cấp
        private List<SupplierModel> GetSuppliers()
        {
            var entries = new List<SupplierModel>();
            var header = Header;
            header.Action = CustomConfigApi.Suppliers.Gets;

            var request = new SupplierSearchRequest()
            {
                limit = 1000
            };

            var obj = _supplierService.Get(header, request);
            if ((EnumError)obj.error == EnumError.Success)
            {
                entries = obj.data.items;
                return entries;
            }
            return entries;
        }

        public List<ListImportModel> GetListExport(string storage_uuid)
        {
            var entries = new List<ListImportModel>();

            var header = Header;
            header.Action = CustomConfigApi.Bunker.ListExportTo;

            var request = new ListImportRequest()
            {
                storageUuid = storage_uuid,
                limit = -1,
            };

            var obj = _bunkerService.ListImport(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                //foreach (var check in obj?.data?.items)
                //{
                //    if (check.StorageFrom != null)    
                //    {
                //        check.uuidfrom = check.StorageFrom.uuid;
                //        check.namefrom = check.StorageFrom.name;
                //    }
                //    else if (check.StaffFrom != null)
                //    {
                //        check.uuidfrom = check.StaffFrom.uuid;
                //        check.namefrom = check.StaffFrom.name;
                //    }
                //}
                entries = obj?.data?.items.Where(s => s.StorageFrom != null && (s.status == 1 || s.status == 4)).ToList();
                return entries;
            }

            return entries;
        }

        public List<InvestorModel> GetUnitsigned()
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.GetUnitsigned;
            var obj = _projectService.GetInvestor(header);
            if (obj.error == 0)
            {
                return obj.data.items;
            }
            return null;
        }
    }
}