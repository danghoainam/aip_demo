﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Extensions;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using DMMS.ViewComponents;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Newtonsoft.Json;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;

namespace DMMS.Controllers
{
    public class CustomerController : BaseController<CustomerController>
    {
        private readonly ICustomerService _customerService;
        private readonly ILocationService _locationService;
        private readonly IGroupService _groupService;
        private readonly IImageService _imageService;
        private readonly ITicketService _ticketService;

        public CustomerController(ICustomerService customer, ILocationService location, 
            IGroupService groupService, IImageService image, ITicketService ticketService)
        {
            _groupService = groupService;
            _customerService = customer;
            _locationService = location;
            _imageService = image;
            _ticketService = ticketService;
        }

        //[DisplayName("Danh sách đơn vị")]
        [CustomController(parentName: MenuTitle.Customer, parentController: "Customer", controllerName: "Customer", name: "Get danh sách đơn vị",
                   actionName: "gets", method: "GET", route: "/customer/gets", isPage: false,
                   enumAction: EnumAction.View, isShow: false, icon: "")]
        public IActionResult gets(CustomerRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.Gets;
            if (request.limit == 0)
            {
                request.limit = 20;
            }
           
            var obj = _customerService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [CustomController(parentName: MenuTitle.Customer, parentController: "Customer", controllerName: "Customer", name: "Get danh sách liên hệ",
                   actionName: "getContact", method: "GET", route: "/customer/getcontact/{customer_uuid}", isPage: false,
                   enumAction: EnumAction.View, isShow: false, icon: "")]
        [Route("/customer/getcontact/{customer_uuid}")]
        public IActionResult getContact(ContactRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.GetContact;

            if (request.limit == 0)
            {
                request.limit = 20;
            }

            var obj = _customerService.GetContact(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        [Route("/customer/GetProject/{customerUuid}")]
        public IActionResult GetProject(CustomerProjectRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.GetsProject;
            if (request.limit == 0)
            {
                request.limit = 20;
            }
            var obj = _customerService.GetProject(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        /// <summary>
        /// Danh sách group của customer
        /// </summary>
        /// <returns></returns>
        [HttpGet("/customer/get-list-group")]
        [HttpPost("/customer/get-list-group")]
        public IActionResult getListGroup(CustomerRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.ListGroup;

            var obj = _customerService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data!=null && obj.data.items!=null)
            {
                return Ok(obj);
            }
            return NotFound();
        }

        //[DisplayName("Danh sách đơn vị")]
        [CustomController(parentName: "", parentController: "Customer", controllerName: "Customer", name: MenuTitle.Customer,
                   actionName: "Index", method: "", route: "/customer/index", isPage: true, enumAction: EnumAction.View, isShow: true, icon: Icon.Customer)]
        public IActionResult Index()
        {
            // Start tranfer data Subheader
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Customer,
                Title = MenuTitle.Customer
            };

            ViewData[SubHeaderData] = subHeader;
            // Get Location List
            ViewBag.province = GetLocation();
            //ViewBag.Groups = GetGroups();
            return View();
        }

        //[DisplayName("Thêm mới đơn vị")]
        [CustomController(parentName: MenuTitle.Customer, parentController: "Customer", controllerName: "Customer", name: "Thêm mới đơn vị",
                           actionName: "Create", method: "", route: "/customer/create", isPage: true, enumAction: EnumAction.View, isShow: true, icon: Icon.Customer)]
        public IActionResult Create()
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Customer,
                Title = MenuTitle.Customer,
                SubTitle = "Thêm mới",
            };
            ViewData[SubHeaderData] = subHeader;
            
            var model = new CustomerAddRequest();
            ViewBag.province = GetLocation();

            return View(model);
        }
        
        //[DisplayName("Thêm mới đơn vị")]
        [CustomController(parentName: MenuTitle.Customer, parentController: "Customer", controllerName: "Customer", name: "Thêm mới đơn vị",
                            actionName: "Create", method: "POST", route: "/customer/create", isPage: false, enumAction: EnumAction.Add, isShow: false, icon: "")]
        [HttpPost]    
        public IActionResult Create(CustomerAddRequest model)
        {
            //ViewBag.Locations = GetLocations();
            var objReturn = new ResponseData<CustomerDataResponse>();
            var header = Header;
            header.Action = CustomConfigApi.Customers.Create;

            if (model.contacts[0].name == null && model.contacts[0].phone_number == null && model.contacts[0].duty == null)
            {
                model.contacts.RemoveAt(0);
            }

            if(model.contacts != null && model.contacts.Count > 0)
            {
                foreach (var item in model.contacts)
                {
                    item.phone_number = item.phone_number.Replace(" ", "").Replace(".", "").Replace(",", "").Replace("/", "").Replace(";", "").Trim();
                }
            }
            model.account.status = 1;
            if (model.account.username == null && model.account.password == null)
            {
                model.account = null;
            }
            var objAdd = _customerService.Upsert(header, model);
            if (objAdd.error == 0)
            {
                return Ok(objAdd);

            }
            else
            {
                ModelState.AddModelError("", objAdd.msg);
                objReturn.msg = string.IsNullOrEmpty(objAdd.msg) ? Constants.MESSAGE_ERROR : objAdd.msg;
                return NotFound(objAdd);
            }
        }

        //[DisplayName("Chỉnh sửa đơn vị")]
        [CustomController(parentName: MenuTitle.Customer, parentController: "Customer", controllerName: "Customer", name: "Chỉnh sửa đơn vị",
                           actionName: "Update", method: "", route: "/customer/update", isPage: true, enumAction: EnumAction.View, isShow: true, icon: Icon.Customer)]
        public IActionResult Update(string id)
        {
            var model = new CustomerAddRequest();
            ViewBag.province = GetLocation();
            ViewBag.ListTicket = GetTicket(id);
            ViewBag.LockOrUnlock = Utilities.ExtensionMethods.ToSelectList<EnumLockOrUnlock>();
            ViewBag.TicketState = Utilities.ExtensionMethods.ToSelectList<EnumTicketState>();
            if (string.IsNullOrEmpty(id))
            {
                return View(model);
            }
            else
            {
                var header = Header;
                header.Action = CustomConfigApi.Customers.Details;
                var request = new CustomerDetailRequest
                {
                    uuid = id
                };
                var obj = _customerService.Detail(header, request);
                var subHeader = new SubHeaderModel
                {
                    Icon = Icon.Customer,
                    Title = MenuTitle.Customer,
                    SubTitle = obj.data.name,
                };
                ViewData[SubHeaderData] = subHeader;
                if (obj.error == 0 && obj.data != null)
                {
                    var objDetail = obj.data;
                    model = new CustomerAddRequest
                    {
                        uuid = id,
                        name = objDetail.name,
                        address = objDetail.address,
                        coordinate = objDetail.coordinate,
                        //contacts = objDetail.contacts,
                        //contact_phone_number = objDetail.contact_phone_number,
                        //contact_name = objDetail.contact_name,
                        email = objDetail.email,                      
                        avatar = obj.data.image != null ? CustomConfigApi.ImageDomain + obj.data.image.path : null,
                        province_id = objDetail.location_assembly.province_id,
                        town_id = objDetail.location_assembly.town_id,
                        village_id = objDetail.location_assembly.village_id,
                       
                    };
                    ViewBag.Town = GetLocationTowns(model.province_id);
                    ViewBag.Vilages = GetLocationVillage(model.town_id);

                    if(objDetail.account != null)
                    {
                        model.account = new Models.Account
                        {
                            username = objDetail.account.username,
                            uuid = objDetail.account.uuid,
                        };
                    }

                    return View(model);
                }
                return RedirectToAction("index", "customer");
            }
        }
        public IActionResult DetailDevice()
        {
            return View();
        }
        //[DisplayName("Chỉnh sửa đơn vị")]
        [CustomController(parentName: MenuTitle.Customer, parentController: "Customer", controllerName: "Customer", name: "Chỉnh sửa đơn vị",
                   actionName: "Update", method: "PUT", route: "/customer/update", isPage: false, enumAction: EnumAction.Edit, isShow: false, icon: "")]
        [HttpPut]
        public IActionResult Update(string uuid, CustomerAddRequest model)
        {
           
                var header = Header;
                header.Action = CustomConfigApi.Customers.Edit;
                var request = new CustomerAddRequest
                {
                    uuid = model.uuid,
                    address = model.address,
                    email = model.email,
                    //contacts = model.contacts,
                    //contact_phone_number = model.contact_phone_number,
                    //contact_name = model.contact_name,
                    name = model.name,
                    coordinate = model.coordinate,
                    //description = model.description,
                    image = model.image,
                    province_id = model.province_id,
                    town_id = model.town_id,
                    village_id = model.village_id
                };
                var obj = _customerService.Upsert(header, request);
                if (obj.error == 0)
                {
                    return Ok(obj);
                }

                return NotFound();
          
        }


        [DisplayName("Action: Chi tiết đơn vị")]
        public IActionResult Details(string id)
        {
            //ViewBag.Location = GetLocations();
            ViewBag.ListTicket = GetTicket(id);
            var model = new CustomerDataResponse();
            var header = Header;
            header.Action = CustomConfigApi.Customers.Details;
            var request = new CustomerDetailRequest
            {
                uuid = id,
            };

            var obj = _customerService.Detail(header, request);
            // Start tranfer data Subheader
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Customer,
                Title = MenuTitle.Customer,
                SubTitle = obj.data.name,
            };
            ViewData[SubHeaderData] = subHeader;
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                model = new CustomerDataResponse
                {
                    uuid = obj.data.uuid,
                    account = obj.data.account,
                    name = obj.data.name,
                    address = obj.data.address,
                    coordinate = obj.data.coordinate,
                    avatar = obj.data.image != null ? CustomConfigApi.ImageDomain + obj.data.image.path : null,
                    contact_name = obj.data.contact_name,
                    contact_phone_number = obj.data.contact_phone_number,
                    location_id = obj.data.location.id,
                    username = obj.data.account != null ? obj.data.account.username : "Chưa có tài khoản",
                    email = obj.data.email,
                };
                return View(model);
            }
            return NotFound(obj);
        }

        //[DisplayName("Xóa đơn vị")]
        [CustomController(parentName: MenuTitle.Customer, parentController: "Customer", controllerName: "Customer", name: "Xóa đơn vị",
                   actionName: "Delete", method: "DELETE", route: "/customer/delete", isPage: false, enumAction: EnumAction.Delete, isShow: false, icon: "")]
        [HttpDelete]
        public IActionResult Delete(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.Delete;

            var request = new CustomerDetailRequest
            {
                uuid = id
            };

            var obj = _customerService.Delete(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        //[DisplayName("Thêm liên hệ mới")]
        [CustomController(parentName: MenuTitle.Customer, parentController: "Customer", controllerName: "Customer", name: "Thêm liên hệ mới",
                  actionName: "CreateContact", method: "", route: "/customer/createcontact", isPage: true, enumAction: EnumAction.View, isShow: false, icon: "")]
        public IActionResult CreateContact(string id)
        {
            var model = new ContactUpsertRequest();
            model.customer_uuid = id;

            return View(model);
        }

        //Thêm mới liên hệ
        [CustomController(parentName: MenuTitle.Customer, parentController: "Customer", controllerName: "Customer", name: "Thêm liên hệ mới",
                  actionName: "CreateContact", method: "POST", route: "/customer/createcontact", isPage: false, enumAction: EnumAction.Add, isShow: false, icon: "")]
        [HttpPost]
        public IActionResult CreateContact(ContactUpsertRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.AddContact;

            var request = new ContactUpsertRequest
            {
                customer_uuid = model.customer_uuid,
                name = model.name,
                duty = model.duty,
                phone_number = model.phone_number.Replace(" ", "").Replace(".", "").Replace(",", "").Replace("/", "").Replace(";", "").Trim(),
            };

            var obj = _customerService.UpsertContact(header, request);

            if(obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        //[DisplayName("Chỉnh sửa đơn vị")]
        [CustomController(parentName: MenuTitle.Customer, parentController: "Customer", controllerName: "Customer", name: "Chỉnh sửa liên hệ",
                  actionName: "UpdateContact", method: "", route: "/customer/updatecontact", isPage: true, enumAction: EnumAction.View, isShow: false, icon: "")]
        public IActionResult UpdateContact(string id)
        {
            var model = new ContactUpsertRequest();
            if (string.IsNullOrEmpty(id))
            {
                return View(model);
            }
            else
            {
                var header = Header;
                header.Action = CustomConfigApi.Customers.DetailContact;
                var request = new ContactDetailRequest
                {
                    uuid = id
                };
                var obj = _customerService.DetailContact(header, request);

                if (obj != null && (EnumError)obj.error == EnumError.Success)
                {
                    model = new ContactUpsertRequest
                    {
                        uuid = obj.data.uuid,
                        customer_uuid = obj.data.customer.uuid,
                        name = obj.data.name,
                        phone_number = obj.data.phone_number,
                        duty = obj.data.duty,
                    };
                }
                return View(model);
            }
        }
        //[DisplayName("Action: Chỉnh sửa đơn vị")]
        [CustomController(parentName: MenuTitle.Customer, parentController: "Customer", controllerName: "Customer", name: "Chỉnh sửa liên hệ",
                  actionName: "UpdateContact", method: "PUT", route: "/customer/updatecontact", isPage: false, enumAction: EnumAction.Edit, isShow: false, icon: "")]
        [HttpPut]
        public IActionResult UpdateContact(ContactUpsertRequest model)
        {
            if (ModelState.IsValid)
            {
                var header = Header;
                header.Action = CustomConfigApi.Customers.EditContact;
                var request = new ContactUpsertRequest
                {
                    uuid = model.uuid,
                    customer_uuid = model.customer_uuid,
                    name = model.name,
                    duty = model.duty,
                    phone_number = model.phone_number.Replace(" ", "").Replace(".", "").Replace(",", "").Replace("/", "").Replace(";", "").Trim(),
                };
                var obj = _customerService.UpsertContact(header, request);
                if (obj.error == 0)
                {
                    return Ok(obj);
                }
                else
                {
                    ModelState.AddModelError("", obj.msg);
                }
            }
            else
            {
                ModelState.AddModelError("", Constants.MESSAGE_ERROR);
            }
            return View(model);
        }

        //Xóa liên hệ khỏi danh sách người liên hệ
        //[DisplayName("Xóa liên hệ")]
        [CustomController(parentName: MenuTitle.Customer, parentController: "Customer", controllerName: "Customer", name: "Xóa liên hệ",
                  actionName: "DeleteContact", method: "DELETE", route: "/customer/deletecontact", isPage: false, enumAction: EnumAction.Delete, isShow: false, icon: "")]
        [HttpDelete]
        public IActionResult DeleteContact(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.DeleteContact;

            var request = new ContactDetailRequest
            {
                uuid = id
            };

            var obj = _customerService.DeleteContact(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        //[DisplayName("Lấy thông tin đơn vị")]
        [CustomController(parentName: MenuTitle.Customer, parentController: "Customer", controllerName: "Customer", name: "Lấy thông tin đơn vị",
                  actionName: "detailCustomerAjax", method: "GET", route: "/customer/detailCustomerAjax", isPage: false, enumAction: EnumAction.View, isShow: false, icon: "")]
        public IActionResult detailCustomerAjax(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.Details;
            var request = new CustomerDetailRequest
            {
                uuid = id,
            };

            var obj = _customerService.Detail(header, request);

            return Ok(obj);
        }

        [CustomController(parentName: MenuTitle.Customer, parentController: "Customer", controllerName: "Customer", name: "Lấy thông tin người liên hệ theo uuid",
                actionName: "detailContactAjax", method: "GET", route: "customer/detailcontactajax/{contact_uuid}", isPage: false, enumAction: EnumAction.View, isShow: false, icon: "")]
        [HttpGet("customer/detailcontactajax/{contact_uuid}")]
        public IActionResult detailContactAjax(string contact_uuid)
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.DetailContact;
            var request = new ContactDetailRequest
            {
                uuid = contact_uuid,
            };

            var obj = _customerService.DetailContact(header, request);

            return Ok(obj);
        }

        [CustomController(parentName: MenuTitle.Customer, parentController: "Customer", controllerName: "Customer", name: "Lấy danh sách người liên hệ theo đơn vị",
                actionName: "getContactAjax", method: "GET", route: "customer/getContactAjax/{id}", isPage: false, enumAction: EnumAction.View, isShow: false, icon: "")]
        [HttpGet("customer/getContactAjax/{id}")]
        public IActionResult getContactAjax(string id)
        {
            var objReturn = new List<ContactModel>();

            var header = Header;
            header.Action = CustomConfigApi.Customers.GetsContact;

            var request = new ContactRequest
            {
                customer_uuid = id,
                limit = 1000,
            };

            var obj = _customerService.GetContact(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                objReturn = obj.data.items.Select(i => new ContactModel
                {
                    uuid = i.uuid,
                    name = i.name_display,
                }).ToList();
            }

            return Ok(objReturn);
        }

        private List<LocationModel> GetLocations()
        {
            var headerlocation = Header;
            headerlocation.Action = CustomConfigApi.Location.Gets;

            var objLocation = _locationService.Get(headerlocation, null);
            if (objLocation.error == 0 && objLocation.data != null)
            {
                return objLocation.data.items;
            }
            return new List<LocationModel>();
        }

        //[Route("/customer/getsticket/id")]
        //public IActionResult getsTicket(TicketRequest request)
        //{
        //    var header = Header;
        //    header.Action = CustomConfigApi.Ticket.Gets;

        //    if (request.limit == 0)
        //    {
        //        request.limit = 10;
        //    }

        //    var obj = _ticketService.Gets(header, request);
        //    if (obj != null && (EnumError)obj.error == EnumError.Success)
        //    {
        //        return Ok(obj);
        //    }
        //    return NotFound();
        //}

        private List<TicketModel> GetTicket(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Ticket.Gets;

            var request = new TicketRequest
            {
                customer_uuid = id,
                limit = 1000,
            };

            var obj = _ticketService.Gets(header, request);
            if(obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            };
            return new List<TicketModel>();
        }

        //private List<GroupModel> GetGroups()
        //{
        //    var header = Header;
        //    header.Action = CustomConfigApi.Groups.GetsOfList;

        //    var obj = _groupService.Get(header, null);
        //    if (obj.error == 0 && obj.data != null)
        //    {
        //        return obj.data.items;
        //    }
        //    return new List<GroupModel>();
        //}

        /// <summary>
        /// Them moi khach hang vao team hoac staff
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [CustomController(parentName: MenuTitle.Customer, parentController: "Customer", controllerName: "Customer", name: "Them moi khach hang vao team hoac staff",
                  actionName: "AddCutomerToTeam", method: "POST", route: "/customer/AddCutomerToTeam", isPage: false, enumAction: EnumAction.Add, isShow: false, icon: "")]
        [HttpPost("/customers/add-customer")]
        public IActionResult AddCutomerToTeam(string customer_uuid, string team_uuid, string staff_uuid)
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.Add;

            object request = null;
            if (string.IsNullOrEmpty(staff_uuid))
            {
                request = new
                {
                    customer_uuid,
                    team_uuid
                };
            }
            else
            {
                request = new
                {
                    customer_uuid,
                    staff_uuid
                };
            }
            var obj = _customerService.Add(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }


        /// <summary>
        /// Xoa khach hang khoi team hoac staff
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [CustomController(parentName: MenuTitle.Customer, parentController: "Customer", controllerName: "Customer", name: "Xoa khach hang khoi team hoac staff",
                  actionName: "RemoveCutomerFromTeam", method: "POST", route: "/customer/RemoveCutomerFromTeam", isPage: false, enumAction: EnumAction.Edit, isShow: false, icon: "")]
        [HttpPost("/customers/remove-customer")]
        public IActionResult RemoveCutomerFromTeam(string customer_uuid,string target_uuid)
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.Remove;

            var request = new { customer_uuid, target_uuid };

            var obj = _customerService.Remove(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        private List<LocationModel> GetLocation()
        {
            var header = Header;
            header.Action = CustomConfigApi.Location.Provinces;
            var request = new LocationRequest
            {
                limit = -1,
            };
            var obj = _locationService.GetsTree(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        private List<LocationModel> GetLocationTowns(int? provinceid)
        {
            var header = Header;
            header.Action = CustomConfigApi.Location.Town;
            var request = new LocationTownRequest
            {
                province_id = provinceid,
                limit = -1,
            };
            var obj = _locationService.GetTown(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        private List<LocationModel> GetLocationVillage(int? town)
        {
            var header = Header;
            header.Action = CustomConfigApi.Location.Village;
            var request = new LocationTownRequest
            {
                town_id = town,
                limit = -1,
            };
            var obj = _locationService.GetTown(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        [Route("/customer/getlocationtown/{provinceid}")]
        public IActionResult GetLocationTown(int provinceid)
        {

            var header = Header;
            header.Action = CustomConfigApi.Location.Town;
            var request = new LocationTownRequest
            {
                province_id = provinceid,
                limit = -1,
            };
            var obj = _locationService.GetTown(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        [Route("/customer/getlocationvillages/{town}")]
        public IActionResult GetLocationVillages(int town)
        {

            var header = Header;
            header.Action = CustomConfigApi.Location.Village;
            var request = new LocationTownRequest
            {
                town_id = town,
                limit = -1,
            };
            var obj = _locationService.GetTown(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
    }
}