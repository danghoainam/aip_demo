﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Utilities;
using DMMS.ViewComponents;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class ListSuppliesController : BaseController<ListSuppliesController>
    {
        private readonly IListSuppliesService _listSupplies;
        private readonly ITreeCategoriesService _treeCategoriesService;
        public ListSuppliesController(IListSuppliesService listSupplies, ITreeCategoriesService treeCategoriesService)
        {
            _listSupplies = listSupplies;
            _treeCategoriesService = treeCategoriesService;
        }
        public IActionResult Index()
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Team,
                Title = MenuTitle.listsupplies,
            };

            ViewData[SubHeaderData] = subHeader;
            ViewBag.Supplies = Utilities.ExtensionMethods.ToSelectList<ListSupplies>();
            return View();
        }
        public IActionResult gets(CategoreSearchLeaf request)
        {
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.SearchLeaf;
            var obj = _treeCategoriesService.SearchLeaf(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }
        [Route("/listsupplies/edit/{category_id}")]
        public IActionResult Edit(TreeCategoriesDetail request)
        {
            var model = new ListSuppliesAddRequest();
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.Detail;
            var obj = _treeCategoriesService.Detail(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                model.name = obj.data.name;
                
                model.prefix_name = obj.data.prefix_name;
                model.type = obj.data.type;
                model.unit = obj.data.detail.unit;
                if(obj.data.exp_price_ref_format != null)
                {
                    model.exp_price_ref_format = obj.data.exp_price_ref_format.Replace(".", "").Replace("₫", "").Trim();
                }
                if(obj.data.imp_price_ref_format != null)
                {
                    model.imp_price_ref_format = obj.data.imp_price_ref_format.Replace(".", "").Replace("₫", "").Trim();
                }
                model.manufacturer = obj.data.detail.manufacturer;
                model.origin = obj.data.detail.origin;
                model.status = obj.data.detail.status;
                model.id = obj.data.id;
                return View(model);
            }

            return NotFound(obj);
            
        }
        [HttpPost]
        public IActionResult Edit(ListSuppliesAddRequest model) 
        {
            var request = new TreeCategoriesEditRequest()
            {
                detail = new Detail(),
            };
            if (model.exp_price_ref_format != null)
            {
                model.exp_price_ref_format = model.exp_price_ref_format.Replace(",", "");
                request.detail.exp_price_ref = long.Parse(model.exp_price_ref_format);
            }
            if (model.imp_price_ref_format != null)
            {
                model.imp_price_ref_format = model.imp_price_ref_format.Replace(",", "");
                request.detail.imp_price_ref = long.Parse(model.imp_price_ref_format);
            }
            request.detail.status = model.status;
            request.detail.unit = model.unit;
            request.id = model.id;
            request.name = model.name;
            request.detail.origin = model.origin;
            request.detail.manufacturer = model.manufacturer;
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.Edit;
            var obj = _treeCategoriesService.Update(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
    }
}