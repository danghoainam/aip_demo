﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class PermissionsController : BaseController<PermissionsController>
    {
        private readonly IPermissionsSerivce _permissionsSerivce;
        public PermissionsController(IPermissionsSerivce permissionsSerivce)
        {
            _permissionsSerivce = permissionsSerivce;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult gets(PermissionsRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Permissions.Gets;
            var obj = _permissionsSerivce.Gets(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        public IActionResult Edit()
        {
            return View();
        }
        
    }
    
}