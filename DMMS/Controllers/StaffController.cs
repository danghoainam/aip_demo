﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;
using static DMMS.Models.StaffAddRequest;
using static DMMS.Models.StaffModel;

namespace DMMS.Controllers
{
    public class StaffController : BaseController<StaffController>
    {
        private readonly IStaffService _staffservice;
        private readonly ITeamService _teamservice;
        private readonly IGroupService _groupService;
        private readonly ILocationService _locationService;
        private readonly ISupplierService _supplierService;
        private readonly IProjectService _projectService;


        public StaffController(IStaffService staff, ITeamService team, IGroupService groupService, ILocationService locationService, ISupplierService supplierService,IProjectService projectService)
        {
            _groupService = groupService;
            _staffservice = staff;
            _teamservice = team;
            _locationService = locationService;
            _supplierService = supplierService;
            _projectService = projectService;
        }

        [DisplayName("Lấy chi tiết nhân viên")]
        public IActionResult getDetail(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Staff.Detail;

            var request = new StaffDetailRequest()
            {
                uuid = id
            };

            var obj = _staffservice.StaffDetail(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [DisplayName("Lấy chi tiết nhân viên theo id")]
        [Route("staff/getDetailByID/{id}")]
        public IActionResult getDetailByID(StaffDetailRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Staff.Detail;

            var obj = _staffservice.StaffDetail(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [DisplayName("Danh sách nhân viên")]
        public IActionResult gets(StaffListRequest request)
        {

            var header = Header;
            header.Action = CustomConfigApi.Staff.GetsList;
            if (request.limit == 0)
            {
                request.limit = 10;
            }
            var obj = _staffservice.GetList(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        [DisplayName("Danh sách theo teamlead")]
        public IActionResult getsListByTeamLead(StaffListByTeamLeadRequest request)
        {

            var header = Header;
            header.Action = CustomConfigApi.Staff.GetsListByTeamLead;

            var obj = _staffservice.StaffListByTeamLead(header, request);

            return Ok(obj);
        }
        [DisplayName("Danh sách theo GM")]
        public IActionResult getsListByGM(StaffListByGMRequest request)
        {

            var header = Header;
            header.Action = CustomConfigApi.Staff.GetsListByGM;

            var obj = _staffservice.StaffListByGM(header, request);

            return Ok(obj);
        }
        [DisplayName("Danh sách nhân viên no team")]
        public IActionResult Staff_NoTeam()
        {
            List<StaffModel> abc = new List<StaffModel>();
            if (ModelState.IsValid)
            {
                var header = Header;
                header.Action = CustomConfigApi.Staff.Gets;

                var request = new StaffGetListRequest
                {

                };
                var obj = _staffservice.Get(header, request);
                {
                    abc = obj.data.items;
                };
            }
            return View(abc);
        }
        [DisplayName("Danh sách nhân viên")]
        public IActionResult Index()
        {
            // Start tranfer data Subheader
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Staff,
                Title = MenuTitle.Staff
            };
            ViewBag.Types = ExtensionMethods.ToSelectList<EnumTypeStaff>();
            ViewData[SubHeaderData] = subHeader;
            ViewBag.province = GetLocation();
            // Get Team List
            var teams = GetTeam();
            ViewBag.teams = teams;
            return View();
        }
        [DisplayName("Danh sách nhân viên")]
        [HttpPost]
        public IActionResult Index(StaffListRequest request)
        {

            var teams = GetTeam();
            ViewBag.teams = teams;

            var model = new List<StaffModel>();
            if (ModelState.IsValid)
            {
                var header = Header;
                header.Action = CustomConfigApi.Staff.GetsList;
                var obj = _staffservice.GetList(header, request);
                if (obj != null && obj.error == 0)
                {
                    model = obj.data.items;
                }
                else
                {
                    ModelState.AddModelError("", obj.msg == null ? "Co loi xay ra" : obj.msg);

                };
            }
            return View(model);
        }

        [DisplayName("Thêm mới nhân viên")]
        public IActionResult Create()
        {
            ViewBag.Unitsigned = GetUnitsigned();
            ViewBag.province = GetLocation();
            var model = new StaffAddRequest();
            ViewBag.Types = ExtensionMethods.ToSelectList<EnumTypeStaff>();
            ViewBag.teams = GetTeam();
            return View(model);
        }
        [DisplayName("Thêm mới nhân viên")]
        [HttpPost]
        //        [ValidateAntiForgeryToken]
        public IActionResult Create(StaffAddRequest model)
        {
            ViewBag.teams = GetTeam();
            model.account.status = 1;
            if(model.account.username == null && model.account.password == null)
            {
                model.account = null;
            }
            var header = Header;
            header.Action = CustomConfigApi.Staff.Add;

            var obj = _staffservice.Add(header, model);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            else
            {
                return NotFound(obj);
            }
        }  
        [DisplayName("Cập nhật nhân viên")]
        public IActionResult Update(string id)
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Staff,
                Title = MenuTitle.Staff,
            };
            var model = new StaffAddRequest();
            ViewBag.State = Utilities.ExtensionMethods.ToSelectList<EnumTaskState>();
            ViewBag.Types = ExtensionMethods.ToSelectList<EnumTypeStaff>().ToList();
            ViewBag.teams = GetTeam();
            ViewBag.province = GetLocation();
            ViewBag.Unitsigned = GetUnitsigned();
            ViewBag.supplier = GetSupplier();
            ViewBag.Vote = Utilities.ExtensionMethods.ToSelectList<EnumVote>();
            var header = Header;
            header.Action = CustomConfigApi.Staff.Detail;
            var request = new StaffDetailRequest
            {
                uuid = id,
            };
            var obj = _staffservice.StaffDetail(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                var objDetail = obj.data;
                subHeader.SubTitle = objDetail.name;
                model = new StaffAddRequest
                {
                    uuid = id,
                    type = objDetail.type,
                    name = objDetail.name,
                    address = objDetail.address,
                    email = objDetail.email,
                    phone_number = objDetail.phone_number,
                    team_uuid = objDetail.team != null ? objDetail.team.uuid : objDetail.team_uuid,
                    avatar = objDetail.image != null ? CustomConfigApi.ImageDomain + objDetail.image.path : null,
                    //acc = objDetail.account.username != null ? objDetail.account.username : null,
                    //account = objDetail.account != null ? objDetail.account : null,
                    province_id = objDetail.location_assembly.province_id != null ? objDetail.location_assembly.province_id : null,
                    town_id = objDetail.location_assembly.town_id != null ? objDetail.location_assembly.town_id : null,
                    village_id = objDetail.location_assembly.village_id != null ? objDetail.location_assembly.village_id : null,
                    unit_signed_uuid = objDetail.unit_signed_uuid,
                    team = objDetail.team,
                    department = objDetail.department
                };
                if(objDetail.account == null)
                {
                    model.account = new Models.Account();
                }
                else
                {
                    model.account = objDetail.account;
                }
            }
            ViewBag.Town = GetLocationTowns(model.province_id);
            ViewBag.Vilages = GetLocationVillage(model.town_id);
            ViewData[SubHeaderData] = subHeader;
            ViewBag.Error = obj.msg;
            return View(model);
        }

        [DisplayName("Cập nhật nhân viên")]
        [HttpPut]
        public IActionResult Update(StaffAddRequest model)
        {
          
            ViewBag.teams = GetTeam();
            var header = Header;
            header.Action = CustomConfigApi.Staff.Edit;
            var obj = _staffservice.Edit(header, model);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            else
            {
                return NotFound(obj);
            }
        }

        [DisplayName("Xóa nhân viên")]
        [HttpDelete]
        public IActionResult Delete(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Staff.Delete;

            var request = new StaffDetailRequest
            {
                uuid = id
            };

            var obj = _staffservice.Delete(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [DisplayName("Chi tiết nhân viên")]
        public IActionResult StaffDetail(string id)
        {
            StaffAddDataResponse abc = new StaffAddDataResponse();
            var header = Header;
            header.Action = CustomConfigApi.Staff.Detail;

            var request = new StaffDetailRequest
            {
                uuid = id,
            };
            var obj = _staffservice.StaffDetail(header, request);
            if (obj.data.image != null)
            {
                obj.data.image.path = CustomConfigApi.ImageDomain + obj.data.image.path;
            }
            abc = obj.data;
            return View(abc);
        }
        // lấy danh sách team
        private List<TeamModel> GetTeam()
        {
            var headerteam = Header;
            headerteam.Action = CustomConfigApi.Team.Gets;
            var request = new TeamRequest
            {
                limit = 20,
            };
            var objTeam = _teamservice.Gets(headerteam, request);
            if (objTeam.error == 0 && objTeam.data != null)
            {
                return objTeam.data.items;
            }
            return null;
        }
        [HttpGet("/employees/get-list-group")]
        [HttpPost("/employees/get-list-group")]
        public IActionResult groupStaff(GroupEmployeesRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Groups.GetGroupEmployees;
            var obj = _groupService.GetGroupEmployees(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        [DisplayName("Danh sách nhân viên theo teamlead")]
        public IActionResult StaffListByTeamLead()
        {
            // Get Team List
            var teams = GetTeam();
            ViewBag.teams = teams;
            return View();
        }
        [DisplayName("Danh sách nhân viên theo teamlead")]
        [HttpPost]
        public IActionResult StaffListByTeamLead(StaffListByTeamLeadRequest request)
        {

            var teams = GetTeam();
            ViewBag.teams = teams;

            var model = new List<StaffModel>();
            if (ModelState.IsValid)
            {
                var header = Header;
                header.Action = CustomConfigApi.Staff.GetsListByTeamLead;
                var obj = _staffservice.StaffListByTeamLead(header, request);
                if (obj != null && obj.error == 0)
                {
                    model = obj.data.items;
                }
                else
                {
                    ModelState.AddModelError("", obj.msg == null ? "Co loi xay ra" : obj.msg);

                };
            }
            return View(model);
        }
        [DisplayName("Danh sách nhân viên theo GM")]
        public IActionResult StaffListByGM()
        {
            var teams = GetTeam();
            ViewBag.teams = teams;
            return View();
        }

        [DisplayName("Danh sách nhân viên theo GM")]
        [HttpPost]
        public IActionResult StaffListByGM(StaffListByGMRequest request)
        {

            var teams = GetTeam();
            ViewBag.teams = teams;

            var model = new List<StaffModel>();
            if (ModelState.IsValid)
            {
                var header = Header;
                header.Action = CustomConfigApi.Staff.GetsListByGM;
                var obj = _staffservice.StaffListByGM(header, request);
                if (obj != null && obj.error == 0)
                {
                    model = obj.data.items;
                }
                else
                {
                    ModelState.AddModelError("", obj.msg == null ? "Co loi xay ra" : obj.msg);

                };
            }
            return View(model);
        }
        public IActionResult LockAccount(LockAccount model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Staff.Locks;
            var request = new StaffModel()
            {  
                uuid = model.id
            };
            var obj = _staffservice.Lockacc(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            else
            {
                return NotFound();
            }
        }
        public IActionResult UnlockAccount(LockAccount model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Staff.Unlocks;
            var request = new StaffModel()
            {
                uuid = model.id
            };
            var obj = _staffservice.UnLockacc(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            else
            {
                return NotFound();
            }
        }
        [Route("staff/gettask/{staffUuid}")]
        public IActionResult GetTask(StaffTaskRequest request)
        {
            
            var header = Header;
            header.Action = CustomConfigApi.Staff.GetTask;
            var obj = _staffservice.GetTask(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            else
            {
                return NotFound();
            }
        }
        [Route("staff/getdevice/{staffUuid}")]
        public IActionResult GetDevice(SupplierDeviceRequest request)
        {
            if(request.states == null)
            {
                request.states = new List<int>();
            }
            var header = Header;
            header.Action = CustomConfigApi.Project.ProjectDevice;
            if (request.limit == 0)
            {
                request.limit = 20;
            }
            var obj = _supplierService.GetDevice(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        [Route("staff/getbalo/{staffUuid}")]
        public IActionResult GetBalo(StaffTaskRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Staff.GetBalo;
            var obj = _staffservice.GetBalo(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        [Route("staff/getimport/{staffUuid}")]
        public IActionResult GetImport(StaffTaskRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Staff.GetImport;
            var obj = _staffservice.GetImport(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                foreach(var check in obj?.data?.items)
                {
                    if (check.StaffFrom != null)
                    {
                        check.namefrom = check.StaffFrom.name;
                    }
                    else if (check.StorageFrom != null)
                    {
                        check.namefrom = check.StorageFrom.name;
                    }
                    else if (check.SupplierFrom != null)
                    {
                        check.namefrom = check.SupplierFrom.name;
                    }
                    else if(check.TaskFrom != null)
                    {
                        check.namefrom = check.TaskFrom.name;
                    }
                }
                return Ok(obj);
            }
            return NotFound();
        }
        [Route("staff/getexport/{staffUuid}")]
        public IActionResult GetExport(StaffTaskRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Staff.GetExport;
            var obj = _staffservice.GetImport(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                foreach (var check in obj?.data?.items)
                {
                    if (check.StaffFrom != null)
                    {
                        check.namefrom = check.StaffFrom.name;
                    }
                    else if (check.StorageFrom != null)
                    {
                        check.namefrom = check.StorageFrom.name;
                    }
                    else if (check.SupplierFrom != null)
                    {
                        check.namefrom = check.SupplierFrom.name;
                    }
                    else if (check.TaskFrom != null)
                    {
                        check.namefrom = check.TaskFrom.name;
                    }
                }
                return Ok(obj);
            }
            return NotFound();
        }
        private List<LocationModel> GetLocation()
        {
            var header = Header;
            header.Action = CustomConfigApi.Location.Provinces;
            var request = new LocationRequest
            {
                limit = -1,
            };
            var obj = _locationService.GetsTree(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        private List<LocationModel> GetLocationTowns(int? provinceid)
        {
            var header = Header;
            header.Action = CustomConfigApi.Location.Town;
            var request = new LocationTownRequest
            {
                province_id = provinceid,
                limit = -1,
            };
            var obj = _locationService.GetTown(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        private List<LocationModel> GetLocationVillage(int? town)
        {
            var header = Header;
            header.Action = CustomConfigApi.Location.Village;
            var request = new LocationTownRequest
            {
                town_id = town,
                limit = -1,
            };
            var obj = _locationService.GetTown(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        private List<SupplierModel> GetSupplier()
        {
            var header = Header;
            header.Action = CustomConfigApi.Suppliers.Gets;
            var request = new SupplierSearchRequest
            {
                limit = 1000,
            };
            var obj = _supplierService.Get(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        public List<InvestorModel> GetUnitsigned()
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.GetUnitsigned;
            var obj = _projectService.GetInvestor(header);
            if (obj.error == 0)
            {
                return obj.data.items;
            }
            return null;
        }
    }
}