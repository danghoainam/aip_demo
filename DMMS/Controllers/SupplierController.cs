﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class SupplierController : BaseController<SupplierController>
    {
        private readonly ISupplierService _supplierService;
        private readonly IImageService _imageService;
        public SupplierController(ISupplierService supplier, IImageService image)
        {
            _supplierService = supplier;
            _imageService = image;
        }
        [DisplayName("Danh sách nhà cung cấp")]
        public IActionResult gets(SupplierSearchRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Suppliers.Gets;
            if (request.limit == 0)
            {
                request.limit = 10;
            }
            var obj = _supplierService.Get(header, request);
            if(obj!=null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }
        [DisplayName("Danh sách nhà cung câp")]
        public IActionResult Index()
        {
            // Start tranfer data Subheader
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Device,
                Title = MenuTitle.Device,
                SubTitle = SubMenuTitle.Supplier
            };

            ViewData[SubHeaderData] = subHeader;

            return View();
        }
        [DisplayName("CHi tiết nhà cung cấp")]
        public IActionResult Details(string id)
        {
            SupplierDataResponse detail = new SupplierDataResponse();
            var header = Header;
            header.Action = CustomConfigApi.Suppliers.Details;
            var request = new SupplierDetailRequest
            {
                uuid = id,
            };
            var obj = _supplierService.Detail(header, request);
            {
                detail = obj.data;
            };
            return View(detail);
        }
        [DisplayName("Thêm mới nhà cung câp")]
        public IActionResult Create()
            {

            var model = new SupplierAddRequest();
           
            return View(model);
        }
        [DisplayName("Action: Thêm mới nhà cung cấp")]
        [HttpPost("/supplier/create")]
        public IActionResult Create(SupplierAddRequest model)
        {
            if (ModelState.IsValid)
            {
                var header = Header;
                header.Action = CustomConfigApi.Suppliers.Upsert;
                var request = new SupplierAddRequest
                {
                    name = model.name,
                    description = model.description,
                    image = model.image


                };
                var obj = _supplierService.UpSert(header, request);
                return RedirectToAction(nameof(Index));
            }
            return View();
        }
        [DisplayName("Cập nhật nhà cung cấp")]
        public IActionResult Edit(string id)
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Supplier,
                Title = MenuTitle.Supplier,
                
            };
            ViewData[SubHeaderData] = subHeader;
            var model = new SupplierModel();
            if (string.IsNullOrEmpty(id))
            {
                return View(model);
            }
            else
            {
                var header = Header;
                header.Action = CustomConfigApi.Suppliers.Details;
                var request = new SupplierDetailRequest
                {
                    uuid = id,
                };
                var obj = _supplierService.Detail(header, request);
                if (obj.error ==0 && obj.data != null)
                {
                    var objDetail = obj.data;
                    subHeader.SubTitle = objDetail.name;
                    model = new SupplierModel
                    {
                        uuid = id,
                        name = objDetail.name,
                        description = objDetail.description,
                        test = objDetail.image != null ? CustomConfigApi.ImageDomain + objDetail.image.path : null,
                        id = objDetail.id,
                        //images = objdetail.images
                    };                    
                }
                return View(model);
            }
        }
        [DisplayName("Action: Cập nhật nhà cung cấp")]
        [HttpPut("/supplier/edit")]
        public IActionResult Edit(SupplierAddRequest model)
        {
            var objReturn = new ResponseData<SupplierDataResponse>();
            var header = Header;
            header.Action = CustomConfigApi.Suppliers.Upsert;

            // check
            if (string.IsNullOrEmpty(model.uuid))
            {
                objReturn.error = 1;
                objReturn.msg = "Không có id";

                return Ok(objReturn);
            }

            var request = new SupplierAddRequest
            {
                uuid = model.uuid,
                description = model.description,
                name = model.name,
                image = model.image
            };
            var obj = _supplierService.UpSert(header, request);
            if (obj.error == 0)
            {
                return Ok(obj);
            }
            else
            {
                objReturn.msg = string.IsNullOrEmpty(obj.msg) ? Constants.MESSAGE_ERROR : obj.msg;
                return Ok(objReturn);
            }
        }

        [DisplayName("Xóa nhà cung cấp")]
        [HttpDelete]
        public IActionResult Delete(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Suppliers.Delete;

            var request = new SupplierDetailRequest
            {
                uuid = id
            };

            var obj = _supplierService.Delete(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        [Route("/supplier/GetDevice/{supplierUuid}")]
        public IActionResult GetDevice(SupplierDeviceRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.ProjectDevice;
            if (request.limit == 0)
            {
                request.limit = 20;
            }
            var obj = _supplierService.GetDevice(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }
    }
}