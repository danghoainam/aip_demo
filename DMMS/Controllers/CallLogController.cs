﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Extensions;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class CallLogController : BaseController<CallLogController>
    {
        private readonly ICallLogService _callLogService;
        public CallLogController(ICallLogService callLog)
        {
            _callLogService = callLog;
        }        
        public IActionResult gets(CallLogRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.CallLog.Gets;
            var obj = _callLogService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                for (var i = 0; i < obj.data.items.Count; i++)
                {
                    obj.data.items[i].Authorization = header.Authorization;
                    
                }

                return Ok(obj);
            }
            return NotFound();

        }
        public IActionResult Index()
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Calllog,
                Title = MenuTitle.Calllog,
               


            };
            ViewData[SubHeaderData] = subHeader;
            return View();
        }
   
    }
}