﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DMMS.CustomAttributes;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DMMS.Controllers
{
    [AllowAnonymous]
    public class AuthenController : BaseController<AuthenController>
    {

        private readonly IAuthenService _authenService;
        private readonly IAccountService _accountService;
        public AuthenController(IAuthenService authenService, IAccountService accountService)
        {
            _authenService = authenService;
            _accountService = accountService;
        }
        
        //[Route("/login")]
        public IActionResult Login()
        {
            var model = new LoginModel();
            return View(model);
        }

        public IActionResult Gets()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var header = Header;
                header.Action = CustomConfigApi.Account.Login;
                var request = new LoginRequest
                {
                    password = model.Password,
                    username = model.Username
                };
                var obj = _authenService.Login(header, request);
                if (obj != null && obj.data != null && obj.error == 0)
                {
                    #region Start get user info

                    var accountModel = new AccountModel();
                    var headerAccount = new RequestHeader
                    {
                        Authorization = obj.data.token,
                        ContentType = "application/json",
                        Method = "POST",
                        Action = "accounts/profile"                        
                    };
                    var objAccount = _accountService.Profile(headerAccount);
                    if (objAccount != null && (EnumError)obj.error == EnumError.Success && objAccount.data != null && objAccount.data.user_info != null)
                    {
                        accountModel = new AccountModel
                        {
                            address = objAccount.data.user_info.address,
                            email = objAccount.data.user_info.email,
                            name = objAccount.data.user_info.name,
                            phone_number = objAccount.data.user_info.phone_number,
                            status = objAccount.data.user_info.status,
                            username = objAccount.data.username,
                            id = objAccount.data.id,
                            uuid = objAccount.data.user_info.uuid,
                            image = objAccount.data.user_info.image
                        };
                    }

                    #endregion

                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, request.username),
                        new Claim(Constants.TOKEN, obj.data.token),
                        new Claim(Constants.REFRESH_TOKEN, obj.data.refresh_token),
                        new Claim(Constants.TTL, obj.data.ttl.ToString()),
                        new Claim(Constants.USER_AVATAR, accountModel.image !=null ? CustomConfigApi.ImageDomain + accountModel.image.path : "/dist/images/icon-avatar.png"),
                        new Claim(Constants.USER_INFO, JsonConvert.SerializeObject(accountModel))
                    };

                    var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                    await HttpContext.SignInAsync(
                        scheme: CookieAuthenticationDefaults.AuthenticationScheme,
                        principal: new ClaimsPrincipal(claimsIdentity),
                        properties: new AuthenticationProperties
                        {
                            IsPersistent = true,
                        }).ConfigureAwait(false);
                    if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                    {
                        return Redirect(model.ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", obj==null || obj.msg == null ? Constants.MESSAGE_ERROR : obj.msg);
                }
            }
            else
            {
                ModelState.AddModelError("", "Thông tin đăng nhập không đúng!");
            }
            return View(model);

        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            var logout = await LogoutAsync();
            if (logout)
            {
                return RedirectToAction("login","authen");
            }
            else
            {
                return RedirectToAction("error","authen");
            }
            /*
            var header = Header;

            header.Action = CustomConfigApi.Account.Logout;            
            
            var obj = _authenService.Logout(header);
            if (obj != null && obj.error == 0)
            {
                await HttpContext.SignOutAsync().ConfigureAwait(false);
                return RedirectToAction("login", "authen");
            }
            return RedirectToAction("error","home");
            */
        }

        private async Task<bool> LogoutAsync()
        {
            var header = Header;
            header.Action = CustomConfigApi.Account.Logout;

            var obj = _authenService.Logout(header);
            if (obj != null && obj.error == 0)
            {
                await HttpContext.SignOutAsync().ConfigureAwait(false);
                return true;
            }
            return false;
        }

        private async Task<bool> RefreshTokenAsync()
        {
            var header = Header;
            header.Action = CustomConfigApi.Account.RefreshToken;

            var request = new RefreshTokenRequest
            {
                refresh_token = DMMSRefreshToken
            };

            var obj = _authenService.RefreshToken(header, request);

            if (obj != null && obj.error == 0)
            {
                var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, Username),
                        new Claim(Constants.TOKEN, obj.data.token),
                        new Claim(Constants.REFRESH_TOKEN, DMMSRefreshToken),
                    };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                await HttpContext.SignInAsync(
                    scheme: CookieAuthenticationDefaults.AuthenticationScheme,
                    principal: new ClaimsPrincipal(claimsIdentity),
                    properties: new AuthenticationProperties
                    {
                        IsPersistent = true,
                    }).ConfigureAwait(true);
                return true;
            }
            else
            {
                var logout = LogoutAsync();

                return false;
            }

        }

        [Route("authen/refresh")]
        public IActionResult RefreshToken(string currentUrl)
        {
            var refreshToken = RefreshTokenAsync();
            if (refreshToken.Result)
            {
                if (string.IsNullOrEmpty(currentUrl))
                {
                    return RedirectToPage("/home");
                }
                else
                {
                    return RedirectToPage(currentUrl);
                }
            }
            else
            {
                return RedirectToPage("~/authen/login");
            }           
        }

        [HttpPost]
        public async Task<IActionResult> Refresh_Token()
        {            
            var header = Header;
            header.Action = CustomConfigApi.Account.RefreshToken;            

            var request = new RefreshTokenRequest
            {
                refresh_token = DMMSRefreshToken
            };

            var obj = _authenService.RefreshToken(header, request);

            if(obj!=null && obj.error == 0)
            {
                var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, Username),
                        new Claim(Constants.TOKEN, obj.data.token),
                        new Claim(Constants.REFRESH_TOKEN, DMMSRefreshToken),
                    };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                await HttpContext.SignInAsync(
                    scheme: CookieAuthenticationDefaults.AuthenticationScheme,
                    principal: new ClaimsPrincipal(claimsIdentity),
                    properties: new AuthenticationProperties
                    {
                        IsPersistent = true,
                    }).ConfigureAwait(true);
            }
            return Ok(obj);

        }
    }
}