﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using DMMS.Extensions;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Utils;
using RestSharp.Extensions;

namespace DMMS.Controllers
{
    public class ProjectController : BaseController<ProjectController>
    {
        private readonly IProjectService _projectService;
        private readonly ICustomerService _customerService;
        private readonly ISupplierService _supplierService;
        private readonly ICategoryService _categoryService;
        private readonly IProjectCustomerService _projectCustomerService;
        private readonly IProjectPackageService _projectPackageService;
        private readonly IDeviceService _deviceService;
        private readonly ILocationService _locationService;
        private readonly IContractService _contractService;
        private readonly IProjectContractservice _projectContractservice;
        private readonly ITreeCategoriesService _treeCategoriesService;
        private readonly IManageFiledService _manageFiledService;
        private readonly ILogger _logger;
        private readonly IAccountService _accountService;
        public ProjectController(IProjectService project, ICustomerService customer, ISupplierService supplier, ICategoryService category, IProjectCustomerService projectCustomer, IProjectPackageService projectPackage, IDeviceService device, ILocationService location, IContractService contract, IProjectContractservice projectContractservice, ITreeCategoriesService treeCategoriesService, IManageFiledService manageFiledService, ILogger<ProjectController> logger, IAccountService accountService)
        {
            _projectService = project;
            _customerService = customer;
            _supplierService = supplier;
            _categoryService = category;
            _projectCustomerService = projectCustomer;
            _projectPackageService = projectPackage;
            _deviceService = device;
            _locationService = location;
            _contractService = contract;
            _projectContractservice = projectContractservice;
            _treeCategoriesService = treeCategoriesService;
            _manageFiledService = manageFiledService;
            _logger = logger;
            _accountService = accountService;
        }
        [CustomController(parentName: "", parentController: "project", controllerName: "project", name: "Get danh sách dự án",
                          actionName: "gets", method: "GET", route: "/project/gets", isPage: false, enumAction: EnumAction.View, isShow: false, icon: "")]
        public IActionResult gets(ProjectSearchRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.Gets;
            var obj = _projectService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }

        public IActionResult getCustomer(string id)
        {

            var header = Header;
            header.Action = CustomConfigApi.Project.Details;
            var request = new ProjecDetailRequest
            {
                project_uuid = id,
            };
            var obj = _projectService.Detail(header, request);

            return Json(obj);
        }

        //[Route("package/getPackageAutoComplete")]
        //public IActionResult getPackageAutoComplete(ProjectGetPackage request)
        //{
        //    var header = Header;
        //    header.Action = CustomConfigApi.Project.GetPacCus;
        //    var obj = _projectPackageService.GetPackage(header, request);
        //    if (obj != null && (EnumError)obj.error == EnumError.Success && obj.code == 0)
        //    {
        //        return Ok(obj);
        //    }
        //    else
        //    {
        //        return NotFound(obj);
        //    }

        //}

        [CustomController(parentName: "", parentController: "project", controllerName: "project", name: MenuTitle.Project,
                         actionName: "Index", method: "", route: "/project", isPage: true, enumAction: EnumAction.View, isShow: true, icon: Icon.Project)]

        public IActionResult Index()

        {
            ViewBag.FilterField = GetField();
            ViewBag.FilterProvince = GetLocation();
            ViewBag.FilterTeam = GetTeamUuid();
            // Start tranfer data Subheader
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Project,
                Title = MenuTitle.Project
            };

            ViewData[SubHeaderData] = subHeader;

            return View();
        }
        //[CustomController(parentName: MenuTitle.Project, parentController: "project", controllerName: "project", name: "Chi tiết dự án",
        //                 actionName: "Detail", method: "", route: "/project/detail", isPage: true, enumAction: EnumAction.View, isShow: false, icon: "")]
        //public IActionResult Detail(string id, ProjecDetailRequest request)
        //{
        //    ProjectDataResponse model = new ProjectDataResponse();
        //    var header = Header;
        //    header.Action = CustomConfigApi.Project.Details;
        //    request = new ProjecDetailRequest
        //    {
        //        uuid = id,
        //    };
        //    var obj = _projectService.Detail(header, request);
        //    if (obj.data != null && obj.data.images != null && obj.data.images.Count > 0)
        //    {
        //        foreach (var item in obj.data.images)
        //        {
        //            item.path = CustomConfigApi.ImageDomain + item.path;
        //        }
        //    }
        //    if (obj != null && (EnumError)obj.error == EnumError.Success)
        //    {
        //        return Ok(obj.data);
        //        model = obj.data;
        //    }
        //    return View(model);
        //}
        [CustomController(parentName: MenuTitle.Project, parentController: "project", controllerName: "project", name: "Thêm mới dự án",
                        actionName: "Create", method: "", route: "/project/create", isPage: true, enumAction: EnumAction.View, isShow: false, icon: "")]
        public IActionResult Create()
        {
            ViewBag.FilterTeam = GetTeamUuid();
            ViewBag.Investor = GetInvestor();
            ViewBag.Unitsigned = GetUnitsigned();
            var customer = GetCustomer();
            ViewBag.Customers = customer;
            List<ManageFieldModel> test = GetField();
            ViewBag.Field = test;
            ViewBag.province = GetLocation();
            return View();
        }
        [CustomController(parentName: MenuTitle.Project, parentController: "project", controllerName: "project", name: "Thêm mới dự án",
                    actionName: "Create", method: "POST", route: "/project/create", isPage: false, enumAction: EnumAction.Add, isShow: false, icon: "")]
        [HttpPost("/project/create")]
        public IActionResult Create(ProjectAddRequest model)
        {
            if (model.contract.timeMaintainValue == null)
            {
                model.contract.timeMaintainValue = 0;
            }
            if (model.contract.timeSignedstring != null)
            {
                DateTime dateTime = DateTime.ParseExact(model.contract.timeSignedstring, "dd/MM/yyyy", null);

                model.contract.timeSigned = dateTime;
            }
            //if (model.contract.timeValidEndstring != null && model.contract.timeValidEndstring != "Invalid date")
            //{
            //    DateTime dateTime = DateTime.ParseExact(model.contract.timeValidEndstring, "dd/MM/yyyy", null);

            //    model.contract.timeValidEnd = dateTime;
            //}
            if (model.contract.timeValidStartstring != null)
            {
                DateTime dateTime = DateTime.ParseExact(model.contract.timeValidStartstring, "dd/MM/yyyy", null);

                model.contract.timeValidStart = dateTime;
            }
            if (model.cost_string != null)
            {
                model.contract.cost = long.Parse(model.cost_string.Replace(",", ""));
            }

            var header = Header;
            header.Action = CustomConfigApi.Project.Add;
            var obj = _projectService.UpSert(header, model);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        private List<ManageFieldModel> GetField()
        {
            var request = new ManageFieldRequest();
            request.limit = -1;
            var header = Header;
            header.Action = CustomConfigApi.Field.Get;
            var obj = _manageFiledService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {

                return obj.data.items;
            }
            return null;
        }

        [CustomController(parentName: MenuTitle.Project, parentController: "project", controllerName: "project", name: "Cập nhật dự án",
                       actionName: "Edit", method: "", route: "/project/edit", isPage: true, enumAction: EnumAction.View, isShow: false, icon: "")]
        public IActionResult Edit(string id)
        {
            List<ManageFieldModel> test = GetField();
            ViewBag.Field = test;
            var customer = GetCustomer();
            ViewBag.Customers = customer;
            ViewBag.devicestate = Utilities.ExtensionMethods.ToSelectList<EnumDeviceState>();
            ViewBag.prodevicestate = Utilities.ExtensionMethods.ToSelectList<EnumStateprocus>();
            ViewBag.Location = GetLocation();
            ViewBag.Investor = GetInvestor();
            ViewBag.Unitsigned = GetUnitsigned();
            ViewBag.FilterTeam = GetTeamUuid();
            //ViewBag.LocationVillage = GetLocationVillage();
            ViewBag.supplier = GetSupplier();
            var package = GetPackageSearch();
            ViewBag.getpackage = package;
            //ViewBag.province = Getprovince();

            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Project,
                Title = MenuTitle.Project
            };

            ViewData[SubHeaderData] = subHeader;
            var model = new ProjectAddRequest();

            if (string.IsNullOrEmpty(id))
            {
                return View(model);
            }
            else
            {
                var header = Header;
                header.Action = CustomConfigApi.Project.Details;
                var request = new ProjecDetailRequest
                {
                    project_uuid = id
                };
                var obj = _projectService.Detail(header, request);
                if (obj.error == 0 && obj.data != null)
                {
                    var objDetail = obj.data;
                    subHeader.SubTitle = objDetail.name;
                    model = new ProjectAddRequest
                    {
                        //customers = objDetail.customers,
                        province_display = objDetail.province.province_id,

                    };
                    model.project = new Projecta();
                    model.project.uuid = objDetail.uuid;
                    model.project.name = objDetail.name;
                    model.project.description = objDetail.description;
                    model.project.code = objDetail.code;
                    model.contract = new ConTractAddRequest();
                    model.contract.investorUuid = objDetail.investor != null ? objDetail.investor.investor_uuid : null;
                    model.contract.coststring = objDetail.cost_diplay;
                    model.contract.timeSigned = objDetail.time_signed;
                    model.contract.timeValidStart = objDetail.time_valid_start;
                    ViewBag.timeend = objDetail.time_valid_end;
                    model.contract.timeMaintainLoop = objDetail.time_maintain_loop;
                    model.contract.timeValidValue = objDetail.time_valid_value;
                    model.contract.code = objDetail.contract_code;
                    model.project.provinceId = objDetail.province.province_id;
                    model.project.fieldId = objDetail.field.field_id;
                    model.contract.uuid = objDetail.contract_uuid;
                    model.project.teamUuid = objDetail.team.team_uuid;
                    model.contract.timeMaintainValue = objDetail.time_maintain_value;
                    ViewBag.CheckState = objDetail.state.state_id;
                    model.contract.unitSignedUuid = objDetail.unit_signed != null ? objDetail.unit_signed.unit_signed_uuid : null;
                    if (model.contract.timeSigned != null)
                    {
                        model.contract.timeSignedstring = model.contract.timeSigned?.ToString("dd/MM/yyyy");
                    }
                    if (model.contract.timeValidStart != null)
                    {
                        model.contract.timeValidStartstring = model.contract.timeValidStart?.ToString("dd/MM/yyyy");
                    }
                    if (ViewBag.timeend != null)
                    {
                        model.contract.timeValidEndstring = ViewBag.timeend.ToString("dd/MM/yyyy");
                    }
                    if (objDetail.contract_isautoclose == 0)
                    {
                        model.contract.isAutoClose = false;
                    }
                    else if (objDetail.contract_isautoclose == 1)
                    {
                        model.contract.isAutoClose = true;
                    }

                    string path = CustomConfigApi.DownloadExcel + "/temp-excel-tbda.xlsx";
                    model.path = path;
                }


                return View(model);
            }
        }
        public IActionResult DetailDevice()
        {
            return View();
        }
        [CustomController(parentName: MenuTitle.Project, parentController: "project", controllerName: "project", name: "Cập nhật dự án",
                        actionName: "Edit", method: "PUT", route: "/project/edit", isPage: false, enumAction: EnumAction.Edit, isShow: false, icon: "")]
        [HttpPut("project/edit")]
        public IActionResult Edit(ProjectAddRequest model)
        {
            if (model.contract.timeMaintainValue == null)
            {
                model.contract.timeMaintainValue = 0;
            }
            var header = Header;
            header.Action = CustomConfigApi.Project.Upsert;
            //check
            var request = new ProjectEditRequest();
            request.project = new Projecttest();
            request.project.uuid = model.project.uuid;
            request.project.name = model.project.name;
            request.project.description = model.project.description;
            request.project.isEnable = true;
            request.project.code = model.project.code;
            request.project.provinceId = model.project.provinceId;
            request.project.fieldId = model.project.fieldId;
            request.project.teamUuid = model.project.teamUuid;
            //if (request.project.provinceId == null)
            //{
            //    request.project.provinceId = 0;
            //}
            //if (request.project.fieldId == null)
            //{
            //    request.project.fieldId = 0;
            //}
            request.contract = new ConTractAddRequest();
            request.contract.uuid = model.contract.uuid;
            if (model.contract.timeSignedstring != null)
            {
                request.contract.timeSigned = DateTime.ParseExact(model.contract.timeSignedstring, "dd/MM/yyyy", null);
            }
            if (model.contract.timeValidStartstring != null)
            {
                request.contract.timeValidStart = DateTime.ParseExact(model.contract.timeValidStartstring, "dd/MM/yyyy", null);
            }
            //if (model.contract.timeValidEndstring != null)
            //{
            //    request.contract.timeValidEnd = DateTime.ParseExact(model.contract.timeValidEndstring, "dd/MM/yyyy", null);
            //}

            request.contract.timeMaintainValue = model.contract.timeMaintainValue;
            request.contract.timeValidValue = model.contract.timeValidValue;
            request.contract.timeMaintainLoop = model.contract.timeMaintainLoop;
            request.contract.investorUuid = model.contract.investorUuid;
            request.contract.code = model.contract.code;
            request.contract.isAutoClose = model.contract.isAutoClose;
            if (model.contract.coststring != null)
            {
                request.contract.cost = long.Parse(model.contract.coststring.Replace(",", "").Replace("₫", "").Replace(".", ""));
            }

            request.contract.unitSignedUuid = model.contract.unitSignedUuid;
            request.contract.InvestorName = model.contract.InvestorName;
            request.contract.UnitSignedName = model.contract.UnitSignedName;

            var obj = _projectService.Edit(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return Ok(obj);
            }
            else
            {
                return NotFound(obj);
            }


        }


        /// <summary>
        /// Them khach hang vao du an
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [CustomController(parentName: MenuTitle.Project, parentController: "project", controllerName: "project", name: "Thêm mới khách hàng dự án",
                               actionName: "Create", method: "", route: "/project/create", isPage: true, enumAction: EnumAction.View, isShow: false, icon: "")]

        public IActionResult AddCustomer(string id, int cusid)
        {
            if (cusid != 0)
            {
                var customer = GetCustomerid(cusid);
                ViewBag.Customers = customer;
                foreach (var i in ViewBag.Customers)
                {
                    //if(i.Villages.name == null && i.Provinces.name != null && i.Towns.name != null)
                    //  {
                    //      i.address = i.Towns.name + "-" + i.Provinces.name;
                    //  }
                    //else if(i.Towns.name == null)
                    //  {
                    //      i.address = i.Villages.name + "-" + i.Provinces.name;
                    //  }
                    //else if (i.Villages.name == null)
                    //  {
                    //      i.address = i.Towns.name + "-" + i.Provinces.name;
                    //  }
                    //  else if (i.Villages.name == null && i.Towns.name == null)
                    //  {
                    //      i.address = i.Provinces.name;
                    //  }
                    //  else
                    //  {
                    //      i.address = i.Villages.name + "-" + i.Towns.name +"-" + i.Provinces.name;
                    //  }
                    i.address = i.Villages.name + "-" + i.Towns.name + "-" + i.Provinces.name;
                    i.address.Replace("null-", "");
                }
            }
            else
            {
                var customer = GetCustomer();
                ViewBag.Customers = customer;
            }


            var package = GetPackage(id);
            ViewBag.getpackage = package;
            var contract = GetlstContract(id);
            ViewBag.contract = contract;
            var model = new ProjectAddCustomerRequest
            {
                uuid = id,
            };
            var header = Header;
            header.Action = CustomConfigApi.Project.Details;
            var request = new ProjecDetailRequest
            {
                project_uuid = id,
            };
            var obj = _projectService.Detail(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                ViewBag.ProjectName = obj.data.name;
                ViewBag.CustomerUuids = (obj.data.customers != null && obj.data.customers.Count > 0) ? JsonConvert.SerializeObject(obj.data.customers.Select(x => x.uuid)) : null;
                model.timeHandOverstring = obj.data.time_valid_start?.ToString("dd/MM/yyyy");
            }

            return View(model);
        }

        public IActionResult PackageDetail(ProjectGetPackage request)
        {
            ViewBag.category = GetCategory();
            ViewBag.packagelst = GetPackagelst();
            ProjectAddPackageRequest model = new ProjectAddPackageRequest();
            var header = Header;
            header.Action = CustomConfigApi.Project.ProjectPackage;
            var obj = _projectPackageService.GetPackage(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                model.name = obj.data.items[0].name;
                model.packageCodeID = obj.data.items[0].packageCodeID;
                model.package_name = obj.data.items[0].packageCode;
                model.projectUuid = request.projectUuid;
                model.uuid = request.package_uuid;
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        public IActionResult PackageDetail(ProjectAddPackageRequest request)
        {

            var header = Header;
            header.Action = CustomConfigApi.Project.AddPackage;
            var obj = _projectPackageService.AddPackage(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }

        public IActionResult AddPackage(string id)
        {
            var model = new ProjectAddPackageRequest();
            ViewBag.category = GetCategory();
            ViewBag.packagelst = GetPackagelst();
            ViewBag.Categories = GetCategory();
            model.projectUuid = id;
            return View(model);
        }

        [HttpPost]
        public IActionResult AddPackage(ProjectAddPackageRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.AddPackage;
            //foreach (var item in request.packageCategories)
            //{
            //    if (item.categoryId == 0 && item.count == 0 && item.timeMaintain == 0)
            //    {
            //        request.packageCategories = new List<total>();
            //    }
            //}
            request.packageCategories.RemoveAll(t => { return t.categoryId == 0 && t.count == 0 && t.timeMaintain == 0; });
            var obj = _projectPackageService.AddPackage(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        public IActionResult EditPackage(ProjectGetPackage request, string name)
        {
            ViewBag.category = GetCategory();
            ViewBag.packagelst = GetPackagelst();
            ProjectAddPackageRequest model = new ProjectAddPackageRequest();
            request.limit = -1;
            var header = Header;
            header.Action = CustomConfigApi.Project.PackageDevice;
            var obj = _projectPackageService.GetPackage(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null && obj.data.items.Count > 0)
            {
                model.name = name;
                model.packageCodeID = obj.data.items[0].packageCodeID;
                model.package_name = obj.data.items[0].packageCode;
                model.projectUuid = request.project_uuid;
                model.uuid = request.package_uuid;
                return View(model);
            }
            else
            {
                model.packageCodeID = request.packageCodeId;
                model.projectUuid = request.project_uuid;
                model.uuid = request.package_uuid;
                return View(model);
            }
        }
        [Route("/project/EditPackage")]
        [HttpPost]
        public IActionResult EditPackage(ProjectAddPackageRequest request)
        {
            var abc = new total();
            request.package_name = request.name;
            request.packageUuid = request.uuid;
            foreach (var item in request.packageCategories)
            {
                if (item.categoryId == 0 && item.count == 0 && item.timeMaintain == 0)
                {
                    abc = item;
                }

            }
            request.packageCategories.Remove(abc);
            var header = Header;
            header.Action = CustomConfigApi.Project.UpdatePackage;
            var obj = _projectService.EditPackage(header, request);
            if (obj.error == 0)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        [DisplayName("Xóa dự án")]
        [HttpDelete]
        public IActionResult Delete(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.Delete;

            var request = new Delete
            {
                project_uuid = id
            };

            var obj = _projectService.Delete(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        public IActionResult Done(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.DoneProject;

            var request = new Delete
            {
                project_uuid = id
            };

            var obj = _projectService.Delete(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        [HttpDelete]
        public IActionResult DeletePackage(DeletePackage request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.DeletePackage;
            var obj = _projectService.DeletePackage(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        public IActionResult EditCustomer(string id, int cusid, string id_cus, string contractids, int state, string date)
        {
            ProjectEditCustomerRequest model = new ProjectEditCustomerRequest();


            var customer = GetCustomer();
            ViewBag.Customers = customer;
            model.Date_display = date;
            model.projectUuid = id;
            model.customerUuid = id_cus;
            model.contractUuid = contractids;
            var package = GetPackage(id);
            ViewBag.getpackage = package;
            var contract = GetlstContract(id);
            ViewBag.contract = contract;
            foreach (var i in ViewBag.contract)
            {
                if (i.uuid == contractids)
                {
                    model.Contract_display = i.contractCode;
                }
            }
            foreach (var i in ViewBag.Customers)
            {
                if (i.uuid == model.customerUuid)
                {
                    model.Customer_display = i.name;
                }
            }
            return View(model);
        }
        public IActionResult GetLstPackage(string id, int cusid, string id_cus, string contractids, int state, string date)
        {
            var request = new ProjectGetPackage();
            request.limit = -1;
            request.contractUuid = contractids;
            request.customerUuid = id_cus;
            request.projectUuid = id;
            var header = Header;
            header.Action = CustomConfigApi.Project.GetPacCus;
            var obj = _projectPackageService.GetPackage(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.code == 0)
            {
                return Ok(obj);
            }
            else
            {
                return NotFound(obj);
            }

        }
        [HttpPost]
        public IActionResult EditCustomer(ProjectEditCustomerRequest request)
        {
            var abc = new orderPackages();

            foreach (var item in request.orderPackages)
            {
                if (item.packageUuid == null && item.quantity == 0)
                {
                    abc = item;
                }

            }

            request.orderPackages.Remove(abc);
            if (request.Date_display != null)
            {
                request.timeHandover = DateTime.ParseExact(request.Date_display, "dd/MM/yyyy", null);
            }

            var header = Header;
            header.Action = CustomConfigApi.Project.EditCustomer;
            var obj = _projectService.EditCustomer(header, request);
            if (obj.error == 0)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [DisplayName("Action: Thêm khách hàng vào dự án")]
        [Route("/project/addcustomer")]
        [HttpPost]
        public IActionResult AddCustomer(string uuid, ProjectAddCustomerRequest request)
        {
            request.projectUuid = uuid;
            if (request.timeHandOverstring != null)
            {
                request.timeHandOver = DateTime.ParseExact(request.timeHandOverstring, "dd/MM/yyyy", null);
            }

            var objReturn = new ResponseData<ProjectDataResponse>();

            var header = Header;
            header.Action = CustomConfigApi.Project.AddCustomer;
            //check
            if (string.IsNullOrEmpty(request.uuid))
            {
                objReturn.error = 1;
                objReturn.msg = "Không có id";
                return Ok(objReturn);
            }
            if (request.orderPackages.Count == 1 && request.orderPackages[0].packageUuid == null)
            {
                var checkfail = 1;
                return NotFound(checkfail);
            }
            //if(request.orderPackages[0].packageUuid == null && request.orderPackages[0].quantity == null)
            //{
            //    request.orderPackages.RemoveAt(0);
            //};
            request.orderPackages.RemoveAll(t => { return t.quantity == 0 && t.packageUuid == null; });
            var obj = _projectService.AddCustomer(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.code == 0)
            {
                return Ok(obj);
            }
            else
            {
                return NotFound(obj);
            }
        }
        [Route("/project/projectgetcustomer/{projectuuid}")]
        public IActionResult ProjectGetCustomer(ProjectGetCustomer request)
        {
            if (request.limit == 0)
            {
                request.limit = 10;
            }
            var header = Header;
            header.Action = CustomConfigApi.Project.ProjectCustomer;
            var obj = _projectCustomerService.GetCustomer(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                //obj.data.items[0].cu   
                return Ok(obj);
            }
            return NotFound();
        }
        [Route("/project/projectgetdevice/{projectUuid}")]
        public IActionResult ProjectGetDevice(ProjectGetDeivce request)
        {
            if (request.states == null)
            {
                request.states = new List<int>();
            }
            if (request.limit == 0)
            {
                request.limit = 10;
            }
            var header = Header;
            header.Action = CustomConfigApi.Project.ProjectDevice;
            var obj = _deviceService.ProjectGet(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        [Route("/project/projectgetcontract/{projectUuid}/{contractUuid}")]
        public IActionResult ProjectGetContract(ContractSearchRequest request)
        {
            if (request.limit == 0)
            {
                request.limit = 10;
            }
            var header = Header;
            header.Action = CustomConfigApi.Project.ProjectContract;
            var obj = _contractService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        public IActionResult ProjectGetCustomerPac(ProjectGetPackage request)
        {
            request.limit = -1;
            var header = Header;
            header.Action = CustomConfigApi.Project.GetPacCus;
            var obj = _projectPackageService.GetPackage(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        [Route("/project/projectgetpac/{project_uuid}")]
        public IActionResult ProjectGetPac(ProjectGetPackage request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.ProjectPackage;
            var obj = _projectPackageService.GetPackage(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        public IActionResult Detaildevicecustomer()
        {
            return View();
        }
        [Route("/project/ProjecDetailDevice/{category_id}")]
        public IActionResult ProjecDetailDevice(ProjectGetDeivce request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.DeviceDetail;
            var obj = _projectPackageService.GetDetailDevice(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        //[Route("project/ProjectDetailDeviceCustomer/{projectUuid}/{packageUuid}/{customerUuid}/{categoryUuid}")]
        public IActionResult ProjectDetailDeviceCustomer(ProjectDetailDeviceRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.DetailDeviceCustomer;
            var obj = _projectCustomerService.GetDetalDevice(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        public IActionResult ProjectDetailStatistics(ProjectDetailDeviceRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.ProjectDetailStatistics;
            var obj = _projectCustomerService.DetailStatistics(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        [Route("/project/projectgetpacSub")]
        public IActionResult ProjectGetPacSub(ProjectGetPackage request)
        {
            request.limit = -1;
            var header = Header;
            header.Action = CustomConfigApi.Project.PackageDevice;
            var obj = _projectPackageService.GetPackage(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        [Route("project/CreateContract/{project}/{custommerUuid}")]
        public IActionResult CreateContract(string project, string custommerUuid)
        {
            var header = Header;
            var request = new ProjecDetailRequest();
            request.project_uuid = project;
            header.Action = CustomConfigApi.Project.Details;
            var obj = _projectService.Detail(header, request);
            var model = new ConTractAddRequest();
            model.code = obj.data.contract_code;
            model.timeValidStartstring = obj.data.time_valid_start.Value.ToString("dd-MM-yyyy");
            model.timeValidEndstring = obj.data.time_valid_end.Value.ToString("dd-MM-yyyy");
            model.timeValidValue = obj.data.time_valid_value;
            model.ProjectUuid = project;
            model.contractUuid = obj.data.contract_uuid;
            model.customerUuid = custommerUuid;
            ViewBag.getcustomer = GetCustomersproject(project);
            ViewBag.guarantee = Utilities.ExtensionMethods.ToSelectList<EnumGuarantee>();
            return View(model);
        }
        [Route("project/UpdateContract/{subcontract}/{project}")]
        public IActionResult UpdateContract(string subcontract, string project)
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.DetailSubContract;
            var request = new SubContractDetailRequest();
            request.subcontract_uuid = subcontract;
            var obj = _contractService.DetailSubContract(header, request);
            ViewBag.getcustomer = GetCustomersproject(project);
            ViewBag.guarantee = Utilities.ExtensionMethods.ToSelectList<EnumGuarantee>();
            if (obj.error == 0)
            {

                var model = new ConTractAddRequest();
                model.timeSignedstring = obj.data.timeSigned.Value.ToString("dd/MM/yyyy");
                model.timeValidStartstring = obj.data.timeValidStart.Value.ToString("dd/MM/yyyy");
                model.timeValidEndstring = obj.data.timeValidEnd.Value.ToString("dd/MM/yyyy");
                model.timeValidValue = obj.data.timeValidValue;
                model.timeMaintainLoop = obj.data.timeMaintainLoop;
                model.timeMaintainValue = obj.data.timeMaintainValue;
                ViewBag.CustomerName = obj.data.customerName;
                model.code = obj.data.code;
                model.subcontract_uuid = subcontract;
                return View(model);
            }
            return NotFound(obj);


        }
        [HttpPost]
        public IActionResult UpdateContract(ConTractAddRequest model)
        {
            if (model.timeSignedstring != null)
            {
                DateTime dateTime = DateTime.ParseExact(model.timeSignedstring, "dd/MM/yyyy", null);

                model.timeSigned = dateTime;
            }
            //if (model.timeValidEndstring != null)
            //{
            //    DateTime dateTime = DateTime.ParseExact(model.timeValidEndstring, "dd/MM/yyyy", null);

            //    model.timeValidEnd = dateTime;
            //}
            if (model.timeValidStartstring != null)
            {
                DateTime dateTime = DateTime.ParseExact(model.timeValidStartstring, "dd/MM/yyyy", null);

                model.timeValidStart = dateTime;
            }
            var header = Header;
            header.Action = CustomConfigApi.Project.UpdateSubContract;
            var obj = _contractService.EditSubContract(header, model);
            if (obj.error == 0)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        [HttpPost]
        public IActionResult CreateContract(ConTractAddRequest model)
        {
            if (model.coststring != null)
            {
                model.coststring = model.coststring.Replace(",", "");
                model.cost = long.Parse(model.coststring);
            }
            if (model.timeSignedstring != null)
            {
                DateTime dateTime = DateTime.ParseExact(model.timeSignedstring, "dd/MM/yyyy", null);

                model.timeSigned = dateTime;
            }
            //if (model.timeValidEndstring != null)
            //{
            //    DateTime dateTime = DateTime.ParseExact(model.timeValidEndstring, "dd/MM/yyyy", null);

            //    model.timeValidEnd = dateTime;
            //}
            if (model.timeValidStartstring != null)
            {
                DateTime dateTime = DateTime.ParseExact(model.timeValidStartstring, "dd/MM/yyyy", null);

                model.timeValidStart = dateTime;
            }
            var header = Header;
            header.Action = CustomConfigApi.Project.AddContract;
            var obj = _projectContractservice.Adds(header, model);
            if (obj.error == 0)
            {
                return Ok(obj);
            }
            return NotFound(obj);

        }
        private List<CustomerModel> GetCustomer()
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.GetList;
            var request = new CustomerRequest
            {
                limit = 1000,
            };
            var obj = _customerService.GetList(header, request);
            if (obj.error == 0)
            {
                return obj.data.items;
            }
            return null;
        }
        private List<InvestorModel> GetInvestor()
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.GetInvestor;
            var obj = _projectService.GetInvestor(header);
            if (obj.error == 0)
            {
                return obj.data.items;
            }
            return null;
        }
        public List<InvestorModel> GetUnitsigned()
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.GetUnitsigned;
            var obj = _projectService.GetInvestor(header);
            if (obj.error == 0)
            {
                return obj.data.items;
            }
            return null;
        }
        private List<ProjectCustomerModel> GetCustomersproject(string id)
        {
            var request = new ProjectGetCustomer();
            request.limit = -1;
            request.projectuuid = id;
            var header = Header;
            header.Action = CustomConfigApi.Project.ProjectCustomer;
            var obj = _projectCustomerService.GetCustomer(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                //obj.data.items[0].cu   
                return obj.data.items;
            }
            return null;
        }
        private List<ContractModel> GetContractsproject(string id)
        {
            var request = new ContractSearchRequest();


            request.limit = -1;
            request.project_uuid = id;

            var header = Header;
            header.Action = CustomConfigApi.Project.ProjectrealContract;
            var obj = _contractService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        private List<CustomerModel> GetCustomerid(int id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.Gets;
            var request = new CustomerFindRequest
            {
                province_id = id,
                limit = 1000,
            };
            var obj = _customerService.FindCustomer(header, request);
            if (obj.error == 0)
            {
                return obj.data.items;
            }
            return null;
        }

        public IActionResult GetlstCustomer()
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.GetList;
            var request = new CustomerRequest
            {
                limit = 1000,
            };
            var obj = _customerService.GetList(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return Ok(obj);
            }
            return null;
        }
        private List<ProjectModel> GetProject()
        {
            var headerproject = Header;
            headerproject.Action = CustomConfigApi.Project.Gets;
            var request = new ProjectSearchRequest
            {
                limit = 1000,
            };
            var objproject = _projectService.Get(headerproject, request);
            if (objproject.error == 0 && objproject.data != null)
            {
                return objproject.data.items;
            }
            return null;
        }
        private List<ProjectPackageModel> GetPackage(string id)
        {
            var request = new ProjectGetPackage
            {
                limit = -1,
                project_uuid = id,
            };

            var header = Header;
            header.Action = CustomConfigApi.Project.ProjectPackage;
            var obj = _projectPackageService.GetPackage(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        private List<ProjectPackageModel> GetPackageSearch()
        {
            var request = new ProjectGetPackage
            {
                limit = -1,

            };

            var header = Header;
            header.Action = CustomConfigApi.Package.Get;
            var obj = _projectPackageService.GetPackage(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        public IActionResult GetlstPac(string id)
        {
            var request = new ProjectGetPackage
            {
                limit = -1,
                project_uuid = id,
            };

            var header = Header;
            header.Action = CustomConfigApi.Project.ProjectPackage;
            var obj = _projectPackageService.GetPackage(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return null;
        }
        public List<ContractModel> GetlstContract(string id)
        {
            var request = new ContractSearchRequest
            {
                limit = -1,
                projectUuid = id,
            };
            var header = Header;
            header.Action = CustomConfigApi.Contract.Gets;
            var obj = _contractService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        private List<SupplierModel> GetSupplier()
        {
            var headersupplier = Header;
            headersupplier.Action = CustomConfigApi.Suppliers.Gets;
            var request = new SupplierSearchRequest
            {
                limit = 1000,
            };
            var objsupplier = _supplierService.Get(headersupplier, request);
            if (objsupplier.error == 0 && objsupplier.data != null)
            {
                return objsupplier.data.items;
            }
            return null;
        }
        public Dictionary<string, int?> Getsupplier()
        {
            var header = Header;
            header.Action = CustomConfigApi.Suppliers.Gets;
            var request = new SupplierSearchRequest
            {
                limit = 2000,
            };
            var obj = _supplierService.Get(header, request);
            if (obj.data != null && obj.error == 0)
            {
                return obj.data.items.ToList().ToDictionary(t => t.name.ToLower().Trim(), t => t.id);
            }
            return null;
        }
        public Dictionary<string, int> GetCategoryss()
        {
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.SearchLeaf;
            var request = new CategoreSearchLeaf
            {
                limit = -1,
                category_type = 1
            };
            var obj = _treeCategoriesService.SearchLeaf(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                var dic = new Dictionary<string, int>();
                var res = obj.data.items.ToList();
                foreach (var r in res)
                {
                    try
                    {
                        dic.Add(r.name.ToLower().Trim(), r.id.Value);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
                return dic;

                //return obj.data.items.ToList().ToDictionary(t => t.name.ToLower(), t => t.id);
            }
            return null;
        }

        private List<TreeCategoriesModel> GetCategory()
        {
            CategoreSearchLeaf request = new CategoreSearchLeaf()
            {
                limit = -1,
                category_type = 1
            };
            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.SearchLeaf;
            var obj = _treeCategoriesService.SearchLeaf(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                foreach (var name in obj.data.items)
                {
                    name.name_format = name.prefix_name + "-" + name.name;
                }
                return obj.data.items;
            }
            return null;
        }
        private List<LocationModel> GetLocation()
        {
            var header = Header;
            header.Action = CustomConfigApi.Location.Provinces;
            var request = new LocationRequest
            {
                limit = -1,
            };
            var obj = _locationService.GetsTree(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        [Route("/project/getlocationtown/{provinceid}")]
        public IActionResult GetLocationTown(int provinceid)
        {

            var header = Header;
            header.Action = CustomConfigApi.Location.Town;
            var request = new LocationTownRequest
            {
                province_id = provinceid,
                limit = -1,
            };
            var obj = _locationService.GetTown(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        [Route("/project/getlocationvillages/{town}")]
        public IActionResult GetLocationVillages(int town)
        {

            var header = Header;
            header.Action = CustomConfigApi.Location.Village;
            var request = new LocationTownRequest
            {
                town_id = town,
                limit = -1,
            };
            var obj = _locationService.GetTown(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        //private List<LocationModel> GetLocationVillage()
        //{
        //    var header = Header;
        //    header.Action = CustomConfigApi.Location.Village;
        //    var request = new LocationRequest
        //    {
        //        limit = -1,
        //    };
        //    var obj = _locationService.GetsTree(header, request);
        //    if (obj.error == 0 && obj.data != null)
        //    {
        //        return obj.data.items;
        //    }
        //    return null;
        //}
        private List<ProjectModel> GetPackagelst()
        {
            var request = new ProjectSearchRequest
            {
                limit = -1,
                page = 0,
            };
            var headercategory = Header;
            headercategory.Action = CustomConfigApi.Project.ListPackage;
            var objcategory = _projectService.Get(headercategory, request);
            if (objcategory.error == 0 && objcategory.data != null)
            {
                return objcategory.data.items;
            }
            return null;
        }
        public List<ProjectModel> GetTeamUuid()
        {
            var request = new TeamProjectRequest
            {
                type = 1
            };
            var header = Header;
            header.Action = CustomConfigApi.Project.GetTeam;
            var obj = _projectService.GetTeam(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        //private List<LocationModel> Getprovince()
        //{
        //    List<LocationModel> abc = new List<LocationModel>();
        //    var header = Header;
        //    header.Action = CustomConfigApi.Location.GetsTree;
        //    var request = new LocationRequest
        //    {

        //    };
        //    var obj = _locationService.Get(header, request);
        //    if (obj != null && (EnumError)obj.error == EnumError.Success)
        //    {
        //        for (var i = 0; i < obj.data.items.Count; i++)
        //        {
        //            if(obj.data.items[i].children[i].children.Count != 0)
        //            {
        //                var a = obj.data.items[i];
        //                abc.Add(a);
        //            }

        //        }

        //        return abc;
        //    }
        //    return null;
        //}
        public IActionResult DeleteCustomer(DeleteCustomerRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.DeleteCustomer;
            var obj = _projectService.DeleteCustomer(header, request);
            if (obj.error == 0)
            {
                return Ok(obj);
            }
            return NotFound();

        }
        public IActionResult UploadFileExcel(string id, ImportDevice model)
        {
            model.projectUuid = id;
            ViewBag.Customer = GetCustomersproject(id);
            ViewBag.Contract = GetContractsproject(id);
            return View(model);
        }
        public class Cuserror
        {
            public int error { get; set; }
            public string msg { get; set; }
        }
        [HttpPost]
        public IActionResult UploadFileExcel(ImportDevice request)
        {
            var check = request.listDevices.Where(p => p.supplierId == null || p.categoryId == null).Count();
            if (check > 0)
            {
                var error = new Cuserror()
                {
                    error = 5,
                    msg = "Sai thông tin Nhà cung cấp hoặc Thiết bị !",
                };
                return NotFound(error);
            }

            var header = Header;
            header.Action = CustomConfigApi.Project.Import;
            var objupload = _deviceService.Import(header, request);
            if (objupload != null && (EnumError)objupload.error == EnumError.Success)
            {
                return Ok(objupload);
            }
            return NotFound(objupload);
        }
        [HttpPost]
        public async Task<IActionResult> AutoUploadFileExcel(DeviceAddRequest entity)
        {
            var supplier = Getsupplier();
            var category = GetCategoryss();
            List<DeviceAddRequest> cashouts = await ReadFileExcel(entity.FileUploadExcel);
            if (cashouts == null)
            {
                return NotFound();
            }

            List<listDevicesip> lstResult =
           cashouts
           .Where(p => supplier.ContainsKey(p.supplier_name.Trim()) && category.ContainsKey(p.category_name.Trim()))
           .Select(res => new listDevicesip()
           {
               supplierId = supplier[res.supplier_name],
               categoryId = category[res.category_name],
               name = res.name,
               category_name = res.category_name,
               //expiredContract = res.expiredContract,
               expireDevice = res.expiredDevice,
               model = res.model,
               serial = res.serial,
               imei = res.imei,
               supplier_name = res.supplier_name,
               customerUuid = entity.customer_uuid,
               contractUuid = entity.contract_uuid,
           })
           .ToList();

            List<listDevicesip> lstNotIn =
               cashouts.Where(p => !supplier.ContainsKey(p.supplier_name.Trim()) || !category.ContainsKey(p.category_name.Trim()))
               .Select(res => new listDevicesip()
               {
                   name = res.name,
                   category_name = res.category_name,
                   //expiredContract = res.expiredContract,
                   expireDevice = res.expiredDevice,
                   model = res.model,
                   serial = res.serial,
                   imei = res.imei,
                   code = "1",
                   supplier_name = res.supplier_name,
                   customerUuid = entity.customer_uuid,
                   contractUuid = entity.contract_uuid,
               }).ToList();
            lstResult.AddRange(lstNotIn);
            var categoryname = (from p in lstNotIn.GroupBy(p => p.category_name)
                                select new
                                {
                                    count = p.Count(),
                                    p.First().category_name,
                                    state = 1,
                                }).ToList();
            var categorynametrue = (from p in lstResult.GroupBy(p => p.category_name)
                                    select new
                                    {
                                        count = p.Count(),
                                        p.First().category_name,
                                        state = 0,
                                    }).ToList();
            var obj = categoryname.Union(categorynametrue);

            //var header = Header;
            //header.Action = CustomConfigApi.Project.Import;
            //var request = new ImportDevice
            //{
            //    projectUuid = entity.project_uuid,
            //    listDevices = lstResult,
            //};
            //if (lstNotIn.Count == 0 && lstResult.Count != 0)
            //{
            //    var objupload = _deviceService.Import(header, request);
            //    if (objupload != null && (EnumError)objupload.error == EnumError.Success)
            //    {
            //        return Ok(obj);
            //    }
            //    return NotFound(obj);
            //}
            //return NotFound(obj);
            return Ok(lstResult);
        }
        public IActionResult SaveDevice()
        {
            return View();
        }
        [HttpPost]
        public IActionResult SaveDevice(ImportDevice model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.Import;
            var objupload = _deviceService.Import(header, model);
            if (objupload != null && (EnumError)objupload.error == EnumError.Success)
            {
                return Ok(objupload);
            }
            return NotFound(objupload);


        }
        public async Task<List<DeviceAddRequest>> ReadFileExcel(IFormFile file)
        {
            var files = HttpContext.Request.Form.Files;

            List<DeviceAddRequest> lstDevice = new List<DeviceAddRequest>();

            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);



                using (var excelPackage = new ExcelPackage(stream))
                {
                    //get the first worksheet in the workbook
                    //var workSheet = excelPackage.Workbook.Worksheets[2];
                    var workSheet = excelPackage.Workbook.Worksheets.LastOrDefault();
                    if (workSheet != null)
                    {
                        int colCount = workSheet.Dimension.End.Column;  //get Column Count
                        int rowCount = workSheet.Dimension.End.Row;     //get row count
                                                                        //for (int row = 0; row < rowCount; ++row)
                                                                        //{

                        //    for (int col = 0; col < colCount; ++col)
                        //    {
                        //        //Console.WriteLine(" Row:" + row + " column:" + col + " Value:" + workSheet.Cells[row, col].Value.ToString().Trim());
                        //        //this.OnReadFile(ref worksheet, ref data);
                        //    }
                        //}

                        while (rowCount >= 1)
                        {
                            var range = workSheet.Cells[rowCount, 1, rowCount, workSheet.Dimension.End.Column];
                            if (range.Any(c => !string.IsNullOrEmpty(c.Text)))
                            {
                                break;
                            }
                            rowCount--;
                        }

                        lstDevice = this.OnReadFile(ref workSheet, rowCount);

                        //Debug.WriteLine("h");
                    }
                }


            }

            return lstDevice;
        }



        public virtual bool AllowFileExtension(string fileExtension)
        {
            return true;
        }
        public List<DeviceAddRequest> OnReadFile(ref ExcelWorksheet worksheet, int rowCount)
        {

            List<DeviceAddRequest> lstDevice = new List<DeviceAddRequest>();
            for (int row = 2; row <= rowCount; ++row)
            {
                string category_name = worksheet.Cells[row, 1].Value.ToString().Trim().ToLower();
                string serial = worksheet.Cells[row, 2].Value.ToString().Trim().ToLower();
                string supplier_name = worksheet.Cells[row, 3].Value.ToString().Trim().ToLower();
                var device_ex = worksheet.Cells[row, 4].Value;
                //var contract_ex = worksheet.Cells[row, 5].Value;
                if (String.IsNullOrEmpty(category_name) || String.IsNullOrEmpty(serial) || String.IsNullOrEmpty(category_name) || String.IsNullOrEmpty(supplier_name))
                {
                    continue;
                }

                DeviceAddRequest cashout = new DeviceAddRequest()
                {


                    category_name = category_name,
                    supplier_name = supplier_name,
                    serial = serial,
                    //expiredDevice = contract_ex,
                    //expiredContract device_ex


                };

                //    string[] validDatetime = { "dd/MM/yyyy" };

                //if (device_ex != null)
                //{

                //    DateTime dateTime = DateTime.ParseExact(device_ex, "d/M/yyyy",null);
                //    cashout.expiredDevice = dateTime;
                //}
                //if (contract_ex != null)
                //{
                //    DateTime dateTime1 = DateTime.ParseExact(contract_ex, "dd/MM/yyyy", null);
                //    cashout.expiredContract = dateTime1;
                //}
                CultureInfo cultures = new CultureInfo("en-US");
                if (device_ex != null)
                {
                    DateTime dateTime = Convert.ToDateTime(device_ex, cultures);
                    cashout.expiredDevice = dateTime;
                }
                //if (contract_ex != null)
                //{
                //    DateTime dateTime1 = Convert.ToDateTime(contract_ex, cultures);
                //    cashout.expiredContract = dateTime1;
                //}

                lstDevice.Add(cashout);
            }
            return lstDevice;
        }
        public string ExportPackage(string id)
        {
            return CreateExport("Tong.xlsx", "Danh sách sản phẩm {0}.xlsx", id);
        }
        public string CreateExport(string filenametemplate, string exportfilenametemplate, string id)
        {
            var pathFileTemplate = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot//statics", filenametemplate);
            _logger.LogWarning(pathFileTemplate);
            FileInfo filetemplate = new FileInfo(pathFileTemplate);
            using (var excelPackage = new ExcelPackage(filetemplate))
            {
                var worksheet = excelPackage.Workbook.Worksheets[1];
                this.FillDataToExcel(ref worksheet, id);
                long unixTime = ((DateTimeOffset)DateTime.UtcNow).ToUnixTimeSeconds();
                var exportfilename = String.Format(exportfilenametemplate, unixTime);
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot//statics", exportfilename);
                FileInfo fileinfo = new FileInfo(path);
                excelPackage.SaveAs(fileinfo);
                var urlFormat = GetFormatUrl(exportfilename);
                return urlFormat;
            }
        }
        public void FillDataToExcel(ref ExcelWorksheet workSheet, string id)
        {
            ProjectGetPackage request = new ProjectGetPackage();
            var header = Header;
            header.Action = CustomConfigApi.Project.PackageDevice;
            request.package_uuid = id;

            var obj = _projectPackageService.GetPackage(header, request);
            var start_row = 2;

            foreach (var item in obj.data.items)
            {
                workSheet.Cells[start_row, 1].Value = item.id;
                workSheet.Cells[start_row, 2].Value = item.name;
                workSheet.Cells[start_row, 3].Value = item.timeMaintain;
                workSheet.Cells[start_row, 4].Value = item.count;

                ++start_row;
            }


        }
        public virtual string GetFormatUrl(string fileName)
        {
            return String.Format("{0}?fileName={1}", Url.Action("DownloadExcel"), fileName);
        }



        public ActionResult DownloadExcel(string fileName, string outputFolder = "wwwroot//statics")
        {
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), outputFolder, fileName);
            _logger.LogInformation(filePath);
            FileInfo fileInfo = new FileInfo(filePath);


            using (ExcelPackage excelPackage = new ExcelPackage(fileInfo))
            {
                byte[] fileContents = excelPackage.GetAsByteArray();

                return File(fileContents, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);

            }


        }
        public IActionResult ImportPackage(string id)
        {
            ViewBag.packagelst = GetPackagelst();
            var model = new PackageImportProject();
            model.projectUuid = id;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> ImportPackage(PackageImportProject entity)
        {
            var newdata = new ProjectAddPackageRequest();
            newdata.packageCategories = new List<total>();
            newdata.packageCodeID = entity.packageCodeID;
            newdata.name = entity.name;
            newdata.projectUuid = entity.projectUuid;
            List<PackageImportProject> cash = await ReadFileExcelPackage(entity.FileUploadExcel);
            if (cash == null)
            {
                return NotFound();
            }
            entity.packageCategories = new List<total>();
            for (var i = 0; i < cash.Count(); i++)
            {
                var total = new total();
                {
                    total.categoryId = cash[i].categoryId;
                    total.count = cash[i].count;
                    total.timeMaintain = cash[i].timeMaintain;
                }
                newdata.packageCategories.Add(total);
            }
            var header = Header;
            header.Action = CustomConfigApi.Project.AddPackage;
            var obj = _projectPackageService.AddPackage(header, newdata);
            return Ok(obj);

        }
        public async Task<List<PackageImportProject>> ReadFileExcelPackage(IFormFile file)
        {
            List<PackageImportProject> lstDevice = new List<PackageImportProject>();

            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);



                using (var excelPackage = new ExcelPackage(stream))
                {
                    //get the first worksheet in the workbook
                    // var workSheet = excelPackage.Workbook.Worksheets[1];
                    var workSheet = excelPackage.Workbook.Worksheets.LastOrDefault();
                    if (workSheet != null)
                    {
                        int colCount = workSheet.Dimension.End.Column;  //get Column Count
                        int rowCount = workSheet.Dimension.End.Row;     //get row count
                                                                        //for (int row = 0; row < rowCount; ++row)
                                                                        //{

                        //    for (int col = 0; col < colCount; ++col)
                        //    {
                        //        //Console.WriteLine(" Row:" + row + " column:" + col + " Value:" + workSheet.Cells[row, col].Value.ToString().Trim());
                        //        //this.OnReadFile(ref worksheet, ref data);
                        //    }
                        //}

                        while (rowCount >= 1)
                        {
                            var range = workSheet.Cells[rowCount, 1, rowCount, workSheet.Dimension.End.Column];
                            if (range.Any(c => !string.IsNullOrEmpty(c.Text)))
                            {
                                break;
                            }
                            rowCount--;
                        }

                        lstDevice = this.OnReadFilePackage(ref workSheet, rowCount);

                        //Debug.WriteLine("h");
                    }
                }


            }

            return lstDevice;
        }
        public List<PackageImportProject> OnReadFilePackage(ref ExcelWorksheet worksheet, int rowCount)
        {

            List<PackageImportProject> lstPackage = new List<PackageImportProject>();
            for (int row = 2; row <= rowCount; ++row)
            {
                string device_code = worksheet.Cells[row, 1].Value.ToString().Trim();
                string category_name = worksheet.Cells[row, 2].Value.ToString().Trim().ToLower();
                string timevalue = worksheet.Cells[row, 3].Value.ToString().Trim();
                string count = worksheet.Cells[row, 4].Value.ToString().Trim();

                if (String.IsNullOrEmpty(category_name) || String.IsNullOrEmpty(timevalue) || String.IsNullOrEmpty(count))
                {
                    continue;
                }

                var test = new PackageImportProject();
                test.categoryId = Int32.Parse(device_code);
                test.count = Int32.Parse(count);
                test.timeMaintain = Int32.Parse(timevalue);



                lstPackage.Add(test);
            }
            return lstPackage;
        }
        [Route("project/CloseSubContract/{id}")]
        public IActionResult CloseSubContract(string id)
        {

            var headerlock = Header;
            headerlock.Action = CustomConfigApi.Project.LockSubContract;
            var requestlock = new LockContractRequest()
            {

                subcontract_uuid = id,
            };
            var objlock = _projectContractservice.LockSubContract(headerlock, requestlock);
            if (objlock.error == 0)
            {
                return Ok(objlock);
            }
            return NotFound(objlock);
        }
        /// <summary>
        /// 
        /// </summary>
        #region
        public IActionResult Download()
        {
            string Files = "wwwroot/statics/ImportExcel/deivce-import-template.xlsx";
            byte[] fileBytes = System.IO.File.ReadAllBytes(Files);
            System.IO.File.WriteAllBytes(Files, fileBytes);
            MemoryStream ms = new MemoryStream(fileBytes);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, "deivce-import-template.xlsx");
        }
        #endregion
    }
}