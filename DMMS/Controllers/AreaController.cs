﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Extensions;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class AreaController : BaseController<AreaController>
    {
        private readonly ITeamService _teamService;
        private readonly IStaffService _staffService;
        private readonly ICustomerService _customerService;
        public AreaController(ITeamService teamService, IStaffService staffService, ICustomerService customerService)
        {
            _teamService = teamService;
            _staffService = staffService;
            _customerService = customerService;
        }

        //[DisplayName("Quản lý khu vực")]
        [CustomController(parentName: "", parentController: "Area", controllerName: "Area", name: MenuTitle.Group,
                    actionName: "Index", method: "", route: "/area/index", isPage: true,
                    enumAction: EnumAction.View, isShow: true, icon: Icon.Group)]
        public IActionResult Index()
        {
            // Start tranfer data Subheader
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Team,
                Title = MenuTitle.Team
            };

            ViewData[SubHeaderData] = subHeader;
            return View();
        }

        [CustomController(parentName: MenuTitle.Group, parentController: "Area", controllerName: "Area", name: "Get danh sách nhân viên",
                    actionName: "getTeams", method: "GET", route: "/teams-staff", isPage: false,
                    enumAction: EnumAction.View, isShow: false, icon: "")]
        [HttpGet("/teams-staff")]
        public IActionResult getTeams()
        {
            var header = Header;
            header.Action = CustomConfigApi.Team.Gets;

            var request = new TeamRequest
            {
                limit = 100
            };

            var obj = _teamService.Gets(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }

        [CustomController(parentName: MenuTitle.Group, parentController: "Area", controllerName: "Area", name: "Get danh sách đơn vị chưa quản lý",
                    actionName: "getCustomersNotManaged", method: "GET", route: "/customers-not-managed", isPage: false,
                    enumAction: EnumAction.View, isShow: false, icon: "")]
        [HttpGet("/customers-not-managed")]
        public IActionResult getCustomersNotManaged(string keyword)
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.CustomersNotManaged;

            var request = new
            {
                keyword,
                limit = 100
            };

            var obj = _customerService.GetCustomerNotManaged(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }

        [CustomController(parentName: MenuTitle.Group, parentController: "Area", controllerName: "Area", name: "Get danh sách đơn vị quản lý bởi team",
                    actionName: "getCustomersManagedByTeam", method: "GET", route: "/customers-managed-by-team", isPage: false,
                    enumAction: EnumAction.View, isShow: false, icon: "")]
        [HttpGet("/customers-managed-by-team")]
        public IActionResult getCustomersManagedByTeam(string team_uuid, string keyword)
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.CustomersManagedByTeam;

            var request = new
            {
                team_uuid,
                keyword,
                limit = 100
            };

            var obj = _customerService.GetCustomerManagedByTeam(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }

        [CustomController(parentName: MenuTitle.Group, parentController: "Area", controllerName: "Area", name: "Get danh sách đơn vị quản lý bởi nhân viên",
                    actionName: "getCustomersManagedByStaff", method: "GET", route: "/customers-managed-by-staff", isPage: false,
                    enumAction: EnumAction.View, isShow: false, icon: "")]
        [HttpGet("/customers-managed-by-staff")]
        public IActionResult getCustomersManagedByStaff(string staff_uuid, string keyword)
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.CustomersManagedByStaff;

            var request = new
            {
                staff_uuid,
                keyword,
                limit = 100
            };

            var obj = _customerService.GetCustomerManagedByStaff(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }

    }
}