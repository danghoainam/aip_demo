﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Services;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class TreeController : BaseController<TreeController>
    {
        private readonly ITreeService _treeservice;
        private readonly ITeamService _teamService;
        private readonly ITreeDicService _staff;

        public TreeController(ITreeService tree, ITeamService team, ITreeDicService staff)
        {
            _teamService = team;
            _treeservice = tree;
            _staff = staff;
        }
        public IActionResult Index()
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Tree,
                Title = MenuTitle.Tree,
          


            };
            ViewData[SubHeaderData] = subHeader;
            return View();
        }
        [HttpGet("/tree/gets")]
        public IActionResult gets(TreeRequest request)
        {
            ViewBag.getstaff = GetStaffs().Count();
            List<InitTree> test = new List<InitTree>();
            request.limit = 100;
            var header = Header;          
            header.Action = CustomConfigApi.Tree.Gets;
            var obj = _treeservice.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                for (var i = 0; i < obj.data.items.Count(); i++)
                {
                    //text = obj.data.items[i].name + "&nbsp&nbsp&nbsp&nbsp&nbsp<i class='" + "la la-flag-o setleader" + "' data-uuid =" + obj.data.items[i].target_uuid + " data-parent=" + obj.data.items[i].parent_id + " style='" + "font-size: 20px;'" + "></i>",
                    var inttree = new InitTree()
                    {
                        id = obj.data.items[i].id,
                        parent = obj.data.items[i].parent_id == null ? "#" : obj.data.items[i].parent_id,
                        //text = string.Format("{0} &nbsp&nbsp&nbsp&nbsp&nbsp<i class='la la-flag-o setleader' data-uuid ='{1}' data-parent='{2}' style='font-size: 20px;'></i>", obj.data.items[i].name, obj.data.items[i].target_uuid, obj.data.items[i].parent_id),
                        //icon = obj.data.items[i].type == 1 ? "/statics/images/Oikya_Front_Logo.png" : " /",
                        target_uuid = obj.data.items[i].target_uuid,
                        uuid = obj.data.items[i].uuid,
                        type = obj.data.items[i].type,
                        count_staff = ViewBag.getstaff
                    };
                    if(inttree.type == 2)
                    {
                        inttree.text = obj.data.items[i].name;
                        inttree.icon = "/statics/images/Vector.png";
                    }
                    else if(inttree.type == 1)
                    {
                        inttree.text = obj.data.items[i].name;
                        inttree.icon = "/statics/images/Group.png";
                    }
                    else if(inttree.type == 3)
                    {
                        //var zz = new TreeDataResponse();
                        inttree.text = string.Format("{0} &nbsp&nbsp&nbsp&nbsp&nbsp<i class='la la-flag-o setleader set{1}' data-uuid ='{2}' data-parent='{3}' style='font-size: 20px;'></i>", obj.data.items[i].name, obj.data.items[i].id, obj.data.items[i].target_uuid, obj.data.items[i].parent_id);
                        inttree.icon = "/statics/images/staff.png";
                    }
                    test.Add(inttree);
                   //if(inttree.type == 3)
                   // {
                   //     inttree.children = true;
                   // }
                   //else
                   // {
                   //     inttree.children = false;
                   // }
                   
                   
                }
                foreach (var cat in obj?.data?.items)
                {
                    _staff[cat.id] = cat;
                };
                for (var i =0;i< test.Count; i++)
                {
                    if(test[i].type == 1 && test[i].parent != "#")
                    {
                        if(_staff[test[i].parent].type == 3 || _staff[test[i].parent].type == 2)
                        {
                            test.RemoveAt(i);
                        }
                    }
                }
                //foreach (var checkRemove in test)
                //{
                //    if (checkRemove.type == 1)
                //    {

                //        if (_staff[checkRemove.parent].type == 3 || _staff[checkRemove.parent].type == 2)
                //        {
                //            test.Remove(checkRemove);
                //        }
                //    }
                //};
                foreach (var checkType in test)
                {
                    if(checkType.parent != "#")
                    {
                        checkType.type_parent = _staff[checkType.parent].type;
                    }
                             
                }
                return Ok(test);
            }
            return NotFound();
        }
        public IActionResult getuuidteam(TreeRequest request)
        {
            List<InitTree> test = new List<InitTree>();
            request.limit = 1;
            var header = Header;
            header.Action = CustomConfigApi.Tree.Gets;
            var obj = _treeservice.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        private List<TreeModel> getuuidteamwidthid(string id)
        {

            TreeRequestWidthId request = new TreeRequestWidthId();
            request.id = id;
            List<InitTree> test = new List<InitTree>();
            request.limit = 1;
            var header = Header;
            header.Action = CustomConfigApi.Tree.Gets;
            var obj = _treeservice.GetId(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        public IActionResult testabc()
        {
            var header = Header;
            header.Action = CustomConfigApi.Tree.StaffNotTree;
            var request = new TreeRequest
            {
                limit = 1000
            };
            var obj = _treeservice.GetStaff(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return Ok(obj.data.items);
            }
            return NotFound();
        }
        public IActionResult Delete(string id)
        {
            var request = new DeleteNode
            {
                uuid = id,
            };
            var header = Header;
            header.Action = CustomConfigApi.Tree.Delete;
            var obj = _treeservice.Delete(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
               
        public IActionResult Create(TreeAddRequest model)
        {
            ViewBag.getstaff = GetStaffs();
          
            return View(model);
        }
        [HttpPost]
        public IActionResult Create(TreeAddRequest model,string uuid)
        {
            var header = Header;
            header.Action = CustomConfigApi.Tree.Create;
            var request = new TreeAddRequest {
                type = 3,
                uuids = model.uuids,
                uuid = uuid
            };
            var obj = _treeservice.Create(header, request);
           if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        public IActionResult CreateTeam(TreeAddRequest model)
        {
            ViewBag.getteam = GetTeams();
            return View(model);
        }
        [HttpPost]
        public IActionResult CreateTeam(TreeAddRequest model,string uuid)
        {
            var header = Header;
            header.Action = CustomConfigApi.Tree.Create;
            var request = new TreeAddRequest
            {
                type = 2,
                uuids = model.uuids,
                uuid = uuid
            };
            var obj = _treeservice.Create(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
            
        }
        public IActionResult CreateTeamnew()
        {
            return View();
        }
        [HttpPost]
        public IActionResult CreateTeamnew(string name)
        {
            var header = Header;
            var request = new TeamUpsertRequest
            {
               name = name
            };
            header.Action = CustomConfigApi.Team.Upsert;
            var obj = _teamService.Add(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        private List<TreeModel> GetStaffs()
        {
            var header = Header;
            header.Action = CustomConfigApi.Tree.StaffNotTree;
            var request = new TreeRequest
            {
                limit = 1000
            };
            var obj = _treeservice.GetStaff(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        private List<TreeModel> DepartmentNotTree()
        {
            var header = Header;
            header.Action = CustomConfigApi.Tree.DepartmentNotTree;
            var request = new DepartmentNotTreeRequest()
            {
                type = 1,
            };
            var obj = _treeservice.DepartmentNotTree(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        public IActionResult CreateDepartment(TreeAddRequest model)
        {
            ViewBag.getstaff = DepartmentNotTree();
            return View(model);
        }
        [HttpPost]
        public IActionResult CreateDepartment(TreeAddRequest model, string uuid)
        {
            if(model.uuids != null && model.uuids.Count == 1)
            {
                model.uuids.RemoveAll(p => { return p == null; });
            }
            var header = Header;
            header.Action = CustomConfigApi.Tree.Create;
            var request = new TreeAddRequest
            {
                uuids = model.uuids,
                uuid = uuid,
                name = model.name,
                type = 1,
            };
            
            var obj = _treeservice.Department(header, request);
            if (obj.error == 0)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        private List<TreeModel> GetTeams()
        {
            var header = Header;
            var request = new TreeRequest
            {
                limit = 1000
            };  
            header.Action = CustomConfigApi.Tree.TeamNotTree;
            var obj = _treeservice.GetTeam(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
            
    }
}
