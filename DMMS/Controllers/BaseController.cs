﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.Extensions.DependencyInjection;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using DMMS.CustomAttributes;
using DMMS.Models;

namespace DMMS.Controllers
{
    [Authorize]
    public abstract class BaseController<T> : Controller where T : BaseController<T>
    {
        private IOptions<CustomConfigApi> _customConfig;
        private ILogger<T> _logger;        

        protected ILogger<T> Logger => _logger ?? (_logger = HttpContext.RequestServices.GetService<ILogger<T>>());
        protected IOptions<CustomConfigApi> CustomConfig => _customConfig ?? (_customConfig = HttpContext.RequestServices.GetService<IOptions<CustomConfigApi>>());

        protected ClaimsIdentity ClaimsIdentity => User.Identity as ClaimsIdentity;       

        protected CustomConfigApi CustomConfigApi => CustomConfig.Value;                    
        protected RequestHeader Header =>
            new RequestHeader
            {
                Authorization = DMMSToken,
                ContentType = "application/json",
                Method = "POST",
            };
             

        protected string DMMSToken => (ClaimsIdentity != null && ClaimsIdentity.Claims != null && ClaimsIdentity.Claims.Count() > 0) ? ClaimsIdentity.FindFirst(Constants.TOKEN).Value : string.Empty;       

        protected string DMMSRefreshToken => (ClaimsIdentity != null && ClaimsIdentity.Claims != null && ClaimsIdentity.Claims.Count() > 0) ?  ClaimsIdentity.FindFirst(Constants.REFRESH_TOKEN).Value : string.Empty;       

        public string Username => (ClaimsIdentity != null && ClaimsIdentity.Claims != null && ClaimsIdentity.Claims.Count() > 0) ? ClaimsIdentity.FindFirst(ClaimTypes.Name).Value : string.Empty;

        public string SubHeaderData => Constants.SubHeaderData;
    }
}