﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Extensions;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class PackageController : BaseController<PackageController>
    {
        private readonly IPackageService _packageService;
        public PackageController(IPackageService packageService)
        {
            _packageService = packageService;
        }

        [CustomController(parentName: MenuTitle.Package, parentController: "Package", controllerName: "Package", name: "Get danh sách phòng ban",
                    actionName: "gets", method: "GET", route: "/package/gets", isPage: false,
                    enumAction: EnumAction.View, isShow: false, icon: "")]
        public IActionResult gets(PackageRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Package.Get;

            if (request.limit == 0)
            {
                request.limit = 10;
            }

            var obj = _packageService.Get(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        [CustomController(parentName: MenuTitle.Package, parentController: "Package", controllerName: "Package", name: "Get danh sách gợi ý",
            actionName: "getHint", method: "GET", route: "/package/gethint", isPage: false,
            enumAction: EnumAction.View, isShow: false, icon: "")]
        [Route("package/gethint")]
        public IActionResult getHint(PackageCodeHintRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Package.GetHint;

            if (request.limit == 0)
            {
                request.limit = 10;
            }

            var obj = _packageService.GetHint(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                obj.data.items?.categories?.RemoveAll(x => x.category_id == 0);
               
                return Ok(obj);
            }

            return NotFound(obj);
        }

        [CustomController(parentName: "", parentController: "Package", controllerName: "Package", name: MenuTitle.Package,
                    actionName: "Index", method: "", route: "/package/index", isPage: true,
                    enumAction: EnumAction.View, isShow: true, icon: Icon.Package)]
        public IActionResult Index()
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Package,
                Title = MenuTitle.Package
            };
            ViewData[SubHeaderData] = subHeader;

            return View();
        }

        [CustomController(parentName: MenuTitle.Package, parentController: "Package", controllerName: "Package", name: "Thêm mới phòng ban",
                    actionName: "Create", method: "", route: "/package/create", isPage: true,
                    enumAction: EnumAction.View, isShow: false, icon: "")]
        public IActionResult Create()
        {
            var model = new PackageUpsertRequest();

            return View(model);
        }

        [CustomController(parentName: MenuTitle.Package, parentController: "Package", controllerName: "Package", name: "Thêm mới phòng ban",
                    actionName: "Create", method: "POST", route: "/package/create", isPage: false,
                    enumAction: EnumAction.Add, isShow: false, icon: "")]
        [HttpPost]
        public IActionResult Create(PackageUpsertRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Package.Upsert;

            var obj = _packageService.Upsert(header, model);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        [CustomController(parentName: MenuTitle.Package, parentController: "Package", controllerName: "Package", name: "Chỉnh sửa phòng ban",
                    actionName: "Edit", method: "", route: "/package/edit", isPage: true,
                    enumAction: EnumAction.View, isShow: true, icon: "")]
        public IActionResult Edit(int? id)
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Package,
                Title = MenuTitle.Package
            };
            ViewData[SubHeaderData] = subHeader;

            var model = new PackageUpsertRequest();
            var header = Header;
            header.Action = CustomConfigApi.Package.Detail;

            var request = new PackageDetailRequest
            {
                package_code_id = id,
            };

            var obj = _packageService.Detail(header, request);

            if ((EnumError)obj.error == EnumError.Success)
            {
                model = new PackageUpsertRequest()
                {
                    id = id,
                    code = obj.data.code,
                    name = obj.data.name,
                    description = obj.data.description,
                };

                return View(model);
            }
            return View(model);
        }

        [CustomController(parentName: MenuTitle.Package, parentController: "Package", controllerName: "Package", name: "Chỉnh sửa phòng ban",
                    actionName: "Edit", method: "PUT", route: "/package/edit", isPage: false,
                    enumAction: EnumAction.Edit, isShow: false, icon: "")]
        [HttpPut]
        public IActionResult Edit(PackageUpsertRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Package.Upsert;

            var request = new PackageUpsertRequest()
            {
                id = model.id,
                name = model.name,
                code = model.code,
                description = model.description,
            };

            var obj = _packageService.Upsert(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound();
        }

        [CustomController(parentName: MenuTitle.Package, parentController: "Package", controllerName: "Package", name: "Xóa phòng ban",
                    actionName: "Delete", method: "Delete", route: "/package/delete", isPage: false,
                    enumAction: EnumAction.Delete, isShow: false, icon: "")]
        [HttpDelete]
        public IActionResult Delete(int? id)
        {
            var header1 = Header;
            header1.Action = CustomConfigApi.Package.Detail;

            var request1 = new PackageDetailRequest
            {
                package_code_id = id,
            };

            var obj1 = _packageService.Detail(header1, request1);

            var header = Header;
            header.Action = CustomConfigApi.Package.Delete;

            var request = new PackageDeleteRequest()
            {
                id = obj1.data.id,
                code = obj1.data.code,
                name = obj1.data.name
            };

            var obj = _packageService.Delete(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }
    }
}