﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Extensions;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class ContractController : BaseController<ContractController>
    {
        private readonly IContractService _contractSevice;
        private readonly IProjectService _projectService;
        private readonly ICustomerService _customerService;



        public ContractController(IContractService contractService, IProjectService projectService, ICustomerService customerService)
        {
            _contractSevice = contractService;
            _projectService = projectService;
            _customerService = customerService;
        }

        //[DisplayName("Danh sách hợp đồng")]
        [CustomController(parentName: MenuTitle.Contract, parentController: "Contract", controllerName: "Contract", name: "Get danh hợp đồng",
                   actionName: "gets", method: "GET", route: "/contract/gets", isPage: false,
                   enumAction: EnumAction.View, isShow: false, icon: "")]
        public IActionResult gets(ContractSearchRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Contract.Gets;
           
                if(request.limit == 0)
            {
                request.limit = 20;
            }
            var obj = _contractSevice.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        [Route("contract/getcontractcus/{contract_uuid}")]
        public IActionResult getContractCus(ContractCustomerRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Contract.GetCustomer;

            if (request.limit == 0)
            {
                request.limit = 20;
            }
            var obj = _contractSevice.GetCustomer(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }


        [CustomController(parentName: "", parentController: "Contract", controllerName: "Contract", name: MenuTitle.Contract,
                   actionName: "Index", method: "", route: "/contract/index", isPage: true,
                   enumAction: EnumAction.View, isShow: true, icon: Icon.Contract)]
        public IActionResult Index()
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Contract,
                Title = MenuTitle.Contract
            };
            ViewData[SubHeaderData] = subHeader;

            ViewBag.Customers = GetCustomers();
            ViewBag.State = Utilities.ExtensionMethods.ToSelectList<EnumContractState>();

            return View();
        }
        public IActionResult Create()
        {
            ViewBag.getcustomer = GetCustomers();
            ViewBag.getProject = GetProjects();
            return View();
        }
        [DisplayName("Action: Thêm mới hợp đòng")]
        [HttpPost]
        public IActionResult Create(ConTractAddRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Contract.Upserts;
            var obj = _contractSevice.Upsert(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        [DisplayName("Action: Cập nhật hợp đồng")]        
        public IActionResult Update(string id, ConTractAddRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Contract.Details;
            var request = new ContractDetailRequest
            {
                contract_uuid = id,
            };
            var obj = _contractSevice.Detail(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                
            }
            return View(model);
        }
        [DisplayName("Action: Cập nhật hợp đồng")]
        [HttpPut]
        public IActionResult Update(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Contract.Upserts;
            var request = new ConTractAddRequest
            {

            };
            var obj = _contractSevice.Upsert(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }

        [CustomController(parentName: MenuTitle.Contract, parentController: "Contract", controllerName: "Contract", name: "Chi tiết hợp đồng",
                   actionName: "Details", method: "", route: "/contract/details", isPage: true,
                   enumAction: EnumAction.View, isShow: true, icon: Icon.Contract)]
        //[DisplayName("Action: Chi tiết đơn vị")]
        public IActionResult Details(string id)
        {
            var model = new ContractDataResponse();
            var header = Header;
            header.Action = CustomConfigApi.Contract.Details;
            var request = new ContractDetailRequest
            {
                contract_uuid = id,
            };

            var obj = _contractSevice.Detail(header, request);
            // Start tranfer data Subheader
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Contract,
                Title = MenuTitle.Contract,
                SubTitle = obj.data.code,
            };
            ViewData[SubHeaderData] = subHeader;
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                model = new ContractDataResponse
                {
                    uuid = obj.data.uuid,
                    projectName = obj.data.projectName,
                    projectUuid = obj.data.projectUuid,
                    node = obj.data.node,
                    code = obj.data.code,
                    investorName = obj.data.investorName,
                    timeSigned = obj.data.timeSigned,
                    timeValidStart = obj.data.timeValidStart,
                    timeValidEnd = obj.data.timeValidEnd,
                    timeValidValue = obj.data.timeValidValue,
                    timeMaintainLoop = obj.data.timeMaintainLoop,
                    state = obj.data.state,
                    cost = obj.data.cost
                    
                    
                };
                if (obj.data.timeValidEnd != null)
                {
                    model.comparedate = DateTime.Compare(DateTime.Now, obj.data.timeValidEnd.Value);
                }

            }
            return View(model);
        }

        [DisplayName("Action: Xóa hợp đồng")]
        [HttpDelete]
        public IActionResult Delete (string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Contract.Delete;
            var request = new ContractDetailRequest
            {
                contract_uuid = id,
            };
            var obj = _contractSevice.Delete(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }

        private List<ProjectModel> GetProjects()
        {
            var header = Header;
            header.Action = CustomConfigApi.Project.Gets;
            var request = new ProjectSearchRequest
            {
                limit = 1000,
            };
            var obj = _projectService.Get(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
        private List<CustomerModel> GetCustomers()
        {
            var header = Header;
            header.Action = CustomConfigApi.Customers.Gets;
            var request = new CustomerRequest
            {
                limit = 1000,
            };
            var obj = _customerService.Get(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }
    }
}