﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace DMMS.Controllers
{

    public class GroupController : BaseController<GroupController>
    {
        private readonly IGroupService _groupService;
        private readonly IStaffService _staffservice;
        public GroupController(IGroupService group, IStaffService staffService)
        {
            _groupService = group;
            _staffservice = staffService;
        }

        #region For Ajax

        /// <summary>
        /// Get customers not in group
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpGet("/group/customers-not-in")]
        public IActionResult groupCustomerNotIn()
        {
            var objReturn = new List<GroupCustomerStaffInforResponse>();
            var header = Header;
            header.Action = CustomConfigApi.Groups.GetGroupCustomer;
            var obj = _groupService.GetGroupCustomer(header, new GroupCustomerRequest
            {
                get_all = false
            }) ;
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null && obj.data.items!=null)
            {
                objReturn = obj.data.items.Select(i=> new GroupCustomerStaffInforResponse
                {
                    uuid = i.uuid,
                    name = i.name,
                    phonenumber = i.phone_number
                }).ToList();
            }
            return Ok(objReturn);
        }

        /// <summary>
        /// Add or update Customer/Staff in to Group
        /// </summary>
        /// <param name="uuid"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("/group/upsert/{uuid}")]
        public IActionResult groupUpsert(string uuid,GroupUpdatetRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Groups.Upsert;
            if (!string.IsNullOrEmpty(request.module))
            {
                if (string.IsNullOrEmpty(request.uuids))
                {
                    return NotFound(new
                    {
                        error = EnumError.Error,
                        msg = "Bạn chưa chọn bản ghi!"
                    });
                }

                if (request.module.ToLower().Equals("staff"))
                {
                    request.staff = request.uuids.Split(",".ToCharArray()).ToList();
                }

                if (request.module.ToLower().Equals("customers"))
                {
                    request.customer = request.uuids.Split(",".ToCharArray()).ToList();
                }
            }
            request.uuid = uuid;
            request.uuids = string.Empty;

            var obj = _groupService.Upsert(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }

        /// <summary>
        /// Get customers of group
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpGet("/group/customers/{uuid}")]
        public IActionResult groupCustomers(string uuid)
        {
            var objReturn = new List<GroupCustomerStaffInforResponse>();
            var header = Header;
            header.Action = CustomConfigApi.Groups.Detail;
            var obj = _groupService.Detail(header, new GroupDetailRequest
            {
                uuid = uuid
            });

            if(obj!=null && (EnumError)obj.error == EnumError.Success && obj.data!=null)
            {
                objReturn = obj.data.customers.Select(i => new GroupCustomerStaffInforResponse
                {
                    uuid = i.uuid,
                    name = i.name,
                    phonenumber = i.contact_phone_number
                }).ToList();
            }

            return Ok(objReturn);
        }

        /// <summary>
        /// Get customers not in group
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        [HttpGet("/group/staff-not-in")]
        public IActionResult groupStaffNotIn()
        {
            var objReturn = new List<GroupCustomerStaffInforResponse>();
            var header = Header;
            header.Action = CustomConfigApi.Groups.GetGroupEmployees;
            var obj = _groupService.GetGroupEmployees(header, new GroupEmployeesRequest());
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null && obj.data.items != null)
            {
                objReturn = obj.data.items.Select(i => new GroupCustomerStaffInforResponse
                {
                    uuid = i.uuid,
                    name = i.name,
                    phonenumber = i.phone_number
                }).ToList();
            }
            return Ok(objReturn);
        }

        

        /// <summary>
        /// Get customers of group
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        //[HttpGet("/group/staff/{uuid}")]
        //public IActionResult groupStaff(string uuid)
        //{
        //    var objReturn = new List<GroupCustomerStaffInforResponse>();
        //    var header = Header;
        //    header.Action = CustomConfigApi.Groups.Detail;
        //    var obj = _groupService.Detail(header, new GroupDetailRequest
        //    {
        //        uuid = uuid
        //    });

        //    if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
        //    {
        //        objReturn = obj.data.staff.Select(i => new GroupCustomerStaffInforResponse
        //        {
        //            uuid = i.uuid,
        //            name = i.name,
        //            phonenumber = i.phone_number
        //        }).ToList();
        //    }

        //    return Ok(objReturn);
        //}
        [DisplayName("Lấy danh sách khu vực")]
        public IActionResult gets(GroupRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Groups.Gets;
            if (request.limit == 0)
            {
                request.limit = 10;
            }
            var obj = _groupService.Get(header, request);

            return Ok(obj);
        }
        public IActionResult getList(GroupRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Groups.GetsOfList;
            if (request.limit == 0)
            {
                request.limit = 10;
            }
            var obj = _groupService.Get(header, request);

            return Ok(obj);
        }
        public IActionResult getCustomer(GroupRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Groups.GetGroupCustomer;
            if (request.limit == 0)
            {
                request.limit = 10;
            }
            var obj = _groupService.Get(header, request);

            return Ok(obj);
        } 
        public IActionResult getEmployees(GroupRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Groups.GetGroupEmployees;
            if (request.limit == 0)
            {
                request.limit = 10;
            }
            var obj = _groupService.Get(header, request);

            return Ok(obj);
        }

        #endregion
        // new
        [DisplayName("Danh sách khu vực")]
        public IActionResult Index(GroupRequest model)
        {
            // Start tranfer data Subheader
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Group,
                Title = MenuTitle.Group
            };

            ViewData[SubHeaderData] = subHeader;

            List<GroupModel> list = new List<GroupModel>();
            if (ModelState.IsValid)
            {
                var header = Header;
                header.Action = CustomConfigApi.Groups.Gets;
                var request = new GroupRequest
                {
                    limit = 10,
                };

                var obj = _groupService.Get(header, request);
                if (obj != null && obj.error == 0)
                {
                    list = obj.data.items;
                }
                else
                {
                    ModelState.AddModelError("", obj.msg == null ? "Co loi xay ra" : obj.msg);
                }
            }
            return View(list);
        }
        public IActionResult IndexList(GroupRequest model)
        {
            List<GroupModel> listOfGroup = new List<GroupModel>();
            if (ModelState.IsValid)
            {
                var header = Header;
                header.Action = CustomConfigApi.Groups.GetsOfList;
                var request = new GroupRequest
                {
                    keyword = model.keyword,
                    limit = 10,
                };
                var obj = _groupService.Get(header, request);
                if (obj != null && obj.error == 0)
                {
                    listOfGroup = obj.data.items;
                }
                else
                {
                    ModelState.AddModelError("", obj.msg == null ? "Co loi xay ra" : obj.msg);
                }
            }
            return View(listOfGroup);
        }
        //add
        [DisplayName("Thêm mới khu vực")]
        public IActionResult Create()
        {
            return View();
        }
        [DisplayName("Action: Thêm mới khu vực")]
        [HttpPost("group/create")]
        public IActionResult Create(GroupAddRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Groups.Upsert;
            var request = new GroupAddRequest
            {
                name = model.name,
                description = model.description
            };
            var obj = _groupService.Add(header, request);
            return Ok(obj);
        }
        //update
        [DisplayName("Cập nhật khu vực")]
        public IActionResult Update(string id)
        {
            var model = new GroupModel();
            var header = Header;
            header.Action = CustomConfigApi.Groups.Detail;
            var request = new GroupDetailRequest
            {
                uuid = id,
            };
            var obj = _groupService.Detail(header, request);
            if(obj!=null && (EnumError)obj.error == EnumError.Success && obj.data!=null)
            {
                model.uuid = id;
                model.name = obj.data.name;
                model.description = obj.data.description;
            }
            return View(model);
        }

        [DisplayName("Action: Cập nhật ticket")]
        [HttpPut("group/update/{uuid}")]
        public IActionResult Update(GroupUpdatetRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Groups.Upsert;
            var request = new
            {
                name = model.name,
                description = model.description,
                uuid = model.uuid
            };
            var obj = _groupService.Upsert(header, request);
            return Ok(obj);
        }

        [DisplayName("Chi tiết khu vực")]
        public IActionResult Detail(string id)
        {
            GroupDataResponse model = new GroupDataResponse();

            var header = Header;
            header.Action = CustomConfigApi.Groups.Detail;


            var request = new GroupDetailRequest
            {
                uuid = id
            };

            var obj = _groupService.Detail(header, request);
            if (obj.error == 0)
            {
                model = obj.data;

                return View(model);
            }
            else
            {
                return NotFound();
            }
        }

        [Route("/group/group-customer")]
        public IActionResult GetGroupCustomer()
        {
            return View();
        }

        [HttpPost("/group/get-group-customer")]
        public IActionResult GetGroupCustomer(GroupCustomerRequest request)
        {
            var listGroupCustomer = new List<GroupModel>();
            var header = Header;
            header.Action = CustomConfigApi.Groups.GetGroupCustomer;
            var obj = _groupService.GetGroupCustomer(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data.items != null)
            {
                return Ok(obj.data.items);
            }
            return NotFound();
        }

        [Route("/group/group-staff")]
        public IActionResult GetGroupEmployees()
        {
            return View();

        }

        [Route("/group/add-staff-to-group")]
        public IActionResult AddStaffToGroup(string group_uuid)
        {
            var model = new StaffInGroupModel();
            model.group_uuid = group_uuid;
            var header = Header;
            header.Action = CustomConfigApi.Groups.GetGroupEmployees;
            var obj = _groupService.GetGroupEmployees(header, new GroupEmployeesRequest
            {
                get_all = false
            });
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null && obj.data.items != null)
            {
                model.staffNotInGroup = obj.data.items.Select(i => new GroupCustomerStaffInforResponse
                {
                    uuid = i.uuid,
                    name = i.name,
                    phonenumber = i.phone_number
                }).ToList();
            }

           
            // tam dong cho api xong
            if (!string.IsNullOrEmpty(group_uuid))
            {
                header.Action = CustomConfigApi.Groups.Detail;
                var objGroup = _groupService.Detail(header, new GroupDetailRequest { 
                    uuid = group_uuid
                });

                if (objGroup != null && (EnumError)objGroup.error == EnumError.Success && objGroup.data != null)
                {
                    model.group = new GroupModel
                    {
                        uuid = group_uuid,
                        name = objGroup.data.name,
                        description = objGroup.data.description
                    };

                    if (objGroup.data.staff != null)
                    {
                        model.staff = new StaffModel
                        {
                            uuid = objGroup.data.staff.uuid,
                            name = objGroup.data.staff.name,
                            phone_number = objGroup.data.staff.phone_number,
                            email = objGroup.data.staff.email,
                            address = objGroup.data.staff.address
                        };
                    }
                }
            }
            

            return View(model);
        }

        [HttpPost("/group/add-new-user")]
        public IActionResult AddStaffToGroup(string group_uuid, string personal_uuid)
        {
            var model = new StaffInGroupModel();
            model.group_uuid = group_uuid;
            var header = Header;
            header.Action = CustomConfigApi.Groups.AddNewUser;
            var request = new
            {
                group_uuid,
                personal_uuid
            };
            var obj = _groupService.UserInGroupAction(header, request);

            if(obj!=null && (EnumError)obj.error == EnumError.Success && !string.IsNullOrEmpty(obj.msg))
            {
                return Ok(obj);
            }
            return NotFound();
        }

        [HttpPost("/group/delete-user")]
        public IActionResult DeleteUserInGroup(string group_uuid, string personal_uuid)
        {
            var model = new StaffInGroupModel();
            model.group_uuid = group_uuid;
            var header = Header;
            header.Action = CustomConfigApi.Groups.DeleteUser;
            var request = new
            {
                group_uuid,
                personal_uuid
            };
            var obj = _groupService.UserInGroupAction(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.code==0)
            {
                return Ok(obj);
            }
            return NotFound();
        }
    }
}