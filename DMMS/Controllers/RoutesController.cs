﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using DMMS.Extensions;
using DMMS.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ActionConstraints;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace DMMS.Controllers
{
    [AllowAnonymous]
    public class RoutesController : Controller
    {
        private readonly IActionDescriptorCollectionProvider _actionDescriptorCollectionProvider;

        public RoutesController(IActionDescriptorCollectionProvider actionDescriptorCollectionProvider)
        {
            _actionDescriptorCollectionProvider = actionDescriptorCollectionProvider;
        }
        public IActionResult Index()
        {
            //var routes = _actionDescriptorCollectionProvider.ActionDescriptors.Items.Where(
            // ad => ad.AttributeRouteInfo != null).Select(ad => new RouteModel
            // {
            //     Name = ad.AttributeRouteInfo.Template,
            //     Method = ad.ActionConstraints?.OfType<HttpMethodActionConstraint>().FirstOrDefault()?.HttpMethods.First(),
            // }).ToList();

            return Ok(_actionDescriptorCollectionProvider
                .ActionDescriptors
                .Items
                .OfType<ControllerActionDescriptor>()
                .Select(a => new RouteModel
                {
                    ControllerName = a.ControllerName,
                    ActionName = a.ActionName,
                    AttributeRouteTemplate = a.AttributeRouteInfo?.Template,
                    HttpMethods = string.Join(", ", a.ActionConstraints?.OfType<HttpMethodActionConstraint>().SingleOrDefault()?.HttpMethods ?? new string[] { "" }),
                    ActionMethodName = a.MethodInfo.Name,
                }));

        }

        public IActionResult Actions()
        {
            var obj = _actionDescriptorCollectionProvider
                .ActionDescriptors
                .Items
                .OfType<ControllerActionDescriptor>()
                .Select(a => new RouteModel
                {
                    ControllerName = a.ControllerName,
                    ActionName = a.ActionName,
                    AttributeRouteTemplate = a.AttributeRouteInfo?.Template,
                    HttpMethods = string.Join(", ", a.ActionConstraints?.OfType<HttpMethodActionConstraint>().SingleOrDefault()?.HttpMethods ?? new string[] { "" }),
                    ActionMethodName = a.MethodInfo.Name,
                    DisplayName = ReturnDisplayName(a.MethodInfo),
                    
                });

            var results = obj.Where(x=>string.IsNullOrEmpty(x.HttpMethods));            

            return Ok(results);
        }

        public RouteModel RouterRecusive(RouteModel source, RouteModel target)
        {
            if (string.IsNullOrEmpty(source.HttpMethods))
            {
                target = source;
                return target;
            }
            else
            {
                if (source.ControllerName.Equals(target.ControllerName))
                {
                    target.Childrents.Add(source);
                }
            }

            return RouterRecusive(source, target);
        }

        private string ReturnDisplayName(MethodInfo methodInfo)
        {
            var ActionAttributes = methodInfo.GetCustomAttributes(typeof(DisplayNameAttribute), false);
            if (ActionAttributes.Length > 0)
            {
                return  ((DisplayNameAttribute)ActionAttributes[0]).DisplayName;
            }

            return string.Empty;
        }

        public IActionResult GetActions()
        {
            return Ok(_actionDescriptorCollectionProvider
                .ActionDescriptors
                .Items
                .OfType<ControllerActionDescriptor>()
                .Select(a => new
                {                    
                    a.DisplayName,
                    a.ControllerName,
                    a.ActionName,
                    AttributeRouteTemplate = a.AttributeRouteInfo?.Template,
                    HttpMethods = string.Join(", ", a.ActionConstraints?.OfType<HttpMethodActionConstraint>().SingleOrDefault()?.HttpMethods ?? new string[] { "any" }),
                    Parameters = a.Parameters?.Select(p => new
                    {
                        Type = p.ParameterType.Name,
                        p.Name
                    }),
                    ControllerClassName = a.ControllerTypeInfo.FullName,
                    ActionMethodName = a.MethodInfo.Name,
                    Filters = a.FilterDescriptors?.Select(f => new
                    {
                        ClassName = f.Filter.GetType().FullName,
                        f.Scope //10 = Global, 20 = Controller, 30 = Action
                }),
                    Constraints = a.ActionConstraints?.Select(c => new
                    {
                        Type = c.GetType().Name
                    }),
                    RouteValues = a.RouteValues.Select(r => new
                    {
                        r.Key,
                        r.Value
                    }),
                }));
        }

        [HttpGet]
        [DisplayName("Action Display Name")]
        public IEnumerable<string> Get()
        {
            var ControllerDisplayName = string.Empty;
            var ActionsDisplayName = string.Empty;

            var ControllerAttributes = ControllerContext.ActionDescriptor.ControllerTypeInfo.GetCustomAttributes(typeof(DisplayNameAttribute), true);
            if (ControllerAttributes.Length > 0)
        {
                ControllerDisplayName = ((DisplayNameAttribute)ControllerAttributes[0]).DisplayName;
            }

            var ActionAttributes = ControllerContext.ActionDescriptor.MethodInfo.GetCustomAttributes(typeof(DisplayNameAttribute), false);
            if (ActionAttributes.Length > 0)
            {
                ActionsDisplayName = ((DisplayNameAttribute)ActionAttributes[0]).DisplayName;
            }

            return new string[] { ControllerDisplayName, ActionsDisplayName };
        }

        public IActionResult GetAllControllerActions()
        {
            Assembly asm = Assembly.GetAssembly(typeof(DMMS.Program));

            var controlleractionlist = asm.GetTypes()
                    .Where(type => typeof(Microsoft.AspNetCore.Mvc.Controller).IsAssignableFrom(type))
                    .SelectMany(type => type.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public))
                    .Where(m => !m.GetCustomAttributes(typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute), true).Any())
                    .Select(x => new {
                        Controller = x.DeclaringType.Name,
                        Action = x.Name,
                        ReturnType = x.ReturnType.Name,
                        Attributes = String.Join(",", x.GetCustomAttributes().Select(a => a.GetType().Name.Replace("Attribute", "")))
                    })
                    .OrderBy(x => x.Controller).ThenBy(x => x.Action).ToList();
            var list = new List<ControllerActionsModel>();
            foreach (var item in controlleractionlist)
            {
                list.Add(new ControllerActionsModel()
                {
                    Controller = item.Controller,
                    Action = item.Action,
                    Attributes = item.Attributes,
                    ReturnType = item.ReturnType
                });
            }
            return Ok(list);
        }

        public IActionResult ABC()
        {
            Assembly asm = Assembly.GetAssembly(typeof(DMMS.Program));

            var controlleractionlist = asm.GetTypes()
                .Where(type => typeof(Controller).IsAssignableFrom(type))
                .SelectMany(type => type.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public))                
                .ToList();

            var results = new List<ResourceModel>();
            foreach (var item in controlleractionlist)
            {
                var custom = item.GetCustomAttribute<CustomController>();
                if (custom != null)
                {
                    results.Add(new ResourceModel
                    {
                        actionName = custom.ActionName,
                        enumAction = custom.EnumAction.ToString(),
                        isPage = custom.IsPage,
                        isShow = custom.IsShow,
                        name = custom.Name,
                        method = custom.Method,
                        parentController = custom.ParentController,
                        parentName = custom.ParentName,
                        route = custom.Route,
                        icon = custom.Icon
                    });
                } 
            }

            var parents = results.Where(x => x.isPage && x.isShow).ToList();            
            if(parents!=null && parents.Count > 0)
            {
                foreach(var item in parents)
                {
                    var objChilds = results.Where(x => x.parentController.Equals(item.parentController) && !x.isShow).ToList();
                    item.Childrents = objChilds;                    
                }
            }


            return Ok(new { 
                all = results,
                parents                
            });
        }

        private List<ResourceModel> ResourceRecusive(ResourceModel parent, List<ResourceModel> source, List<ResourceModel> target)
        {
            if(parent != null)
            {
                
            }
            return target;
        }
    }
}