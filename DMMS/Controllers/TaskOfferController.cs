﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class TaskOfferController : BaseController<TaskOfferController>
    {
        private readonly ITaskOfferService _taskOfferService;
        public TaskOfferController(ITaskOfferService taskOfferService)
        {
            _taskOfferService = taskOfferService;
        }
        public IActionResult Index(TaskOfferModel model)
        {
            // Start tranfer data Subheader
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Task,
                Title = MenuTitle.TaskOffer,
            };
            ViewData[SubHeaderData] = subHeader;
            ViewBag.offersate = Utilities.ExtensionMethods.ToSelectList<EnumOffferState>();
            return View(model);
        }
        public IActionResult gets(TaskOfferRequest request)
        {
            
            var header = Header;

            header.Action = CustomConfigApi.TaskOffer.Gets;
            if (request.limit == 0)
            {
                request.limit = 10;
            }
            var obj = _taskOfferService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        public IActionResult Update (TaskOfferRequest request, string id)
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Task,
                Title = MenuTitle.TaskOffer,
            };
            ViewData[SubHeaderData] = subHeader;
            var model = new TaskOfferModel();
            var header = Header;
            header.Action = CustomConfigApi.TaskOffer.Details;
            request = new TaskOfferRequest
            {
                uuid = id,
            };
            var obj = _taskOfferService.Detail(header, request);
            subHeader.SubTitleSmal = obj.data.task != null ? obj.data.task.name : "";
            foreach(var item in obj.data.images)
            {
                item.path = CustomConfigApi.ImageDomain + "/" + item.path;
            }

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                 model = new TaskOfferModel
                {
                    uuid = request.uuid,
                    description = obj.data.description,
                    images = obj.data.images,
                    task = obj.data.task,
                    employee = obj.data.employee,
                    time_created = obj.data.time_created,
                    censor = obj.data.censor,
                     timecanceloffer = obj.data.time_last_update_format,
                     state = obj.data.state,
                     censor_note = obj.data.censor_note
                 }; 
            }
            return View(model);
        }
        public IActionResult Doneoffer(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.TaskOffer.Upserts;
            var request = new TaskOfferRequest
            {   
                uuid = id,
                state = 2
            };
            var obj = _taskOfferService.Upsert(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();

        }
        [HttpPost]
        public IActionResult Cancleoffer(string id, TaskOfferRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.TaskOffer.Upserts;
            request = new TaskOfferRequest
            {
                note = request.note,
                uuid = id,
                state = 3
            };
            var obj = _taskOfferService.Upsert(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();

        }

    }
}