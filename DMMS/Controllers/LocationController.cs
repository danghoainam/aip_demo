﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Extensions;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class LocationController : BaseController<CustomerController>
    {
        private readonly ILocationService _locationService;

        public LocationController(ILocationService location)
        {

            _locationService = location;
        }

        //[CustomController(parentName: MenuTitle.Category, parentController:"", controllerName:"Location", name: "Get danh sách đơn vị hành chính", "gets", "GET", "location/gets", false, EnumAction.View, false)]
        [CustomController(parentName: "", parentController: "Location", controllerName: "Location", name: "Get danh sách đơn vị hành chính",
                           actionName: "gets", method: "GET", route: "/location/gets", isPage: false,
                           enumAction: EnumAction.View, isShow: false, icon: "")]
        [HttpGet("/location/gets")]
        public IActionResult gets(LocationRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Location.GetsTree;
            var obj = _locationService.GetsTree(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(new
                {
                    data = obj.data.items
                });
            }
            return NotFound();
        }

        //[CustomController("", "Location", MenuTitle.Location, "Index", "", "location/index", true, EnumAction.View)]
        [CustomController(parentName: "", parentController: "Location", controllerName: "Location", name: MenuTitle.Location,
                            actionName: "Index", method: "", route: "/location", isPage: true,
                            enumAction: EnumAction.View, isShow: true, icon: "kt-menu__link-icon flaticon-responsive")]
        public IActionResult Index()
        {
            // Start tranfer data Subheader
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Location,
                Title = MenuTitle.Location,
            };

            ViewData[SubHeaderData] = subHeader;

            return View();
        }

        //[CustomController(MenuTitle.Location, "Location", "Thêm mới đơn vị hành chính", "Create", "", "location/create", false, EnumAction.View, false)]
        [CustomController(parentName: MenuTitle.Location, parentController: "Location", controllerName: "Location", name: "Thêm mới đơn vị hành chính",
                            actionName: "Create", method: "", route: "/location/create", isPage: true,
                            enumAction: EnumAction.View, isShow: false, icon: "kt-menu__link-icon flaticon-responsive")]
        public IActionResult Create()
        {
            var model = new LocationUpsertRequest();
            ViewBag.Location = GetLocations();

            return View(model);
        }

        //[CustomController(MenuTitle.Location, "Location", "Thêm mới đơn vị hành chính", "Create", "POST", "/location/create", false, EnumAction.Add, false)]
        [CustomController(parentName: MenuTitle.Location, parentController: "Location", controllerName: "Location", name: "Thêm mới đơn vị hành chính",
                            actionName: "Create", method: "POST", route: "/location/create", isPage: false,
                            enumAction: EnumAction.Add, isShow: false, icon: "")]
        [HttpPost("/location/create")]
        public IActionResult Create(LocationUpsertRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Location.Upsert;

            var obj = _locationService.Upsert(header, model);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.code == 0)
            {
                return Ok(obj);
            }
            else
            {
                return NotFound(obj);
            }

        }

        //[DisplayName("Cập nhật loại thiết bị")]
        //[CustomController(MenuTitle.Location, "Location", "Cập nhật đơn vị hành chính", "Edit", "", "/location/edit", true, EnumAction.View, false)]
        [CustomController(parentName: MenuTitle.Location, parentController: "Location", controllerName: "Location", name: "Cập nhật đơn vị hành chính",
                            actionName: "Edit", method: "", route: "/location/edit", isPage: true,
                            enumAction: EnumAction.Edit, isShow: false, icon: "kt-menu__link-icon flaticon-responsive")]
        public IActionResult Edit(int? id)
        {
            var model = new LocationUpsertRequest();
            ViewBag.Location = GetLocations();

            if (id == null)
            {
                return View(model);
            }
            else
            {
                var header = Header;
                header.Action = CustomConfigApi.Location.Details;

                var request = new LocationDetailRequest
                {
                    id = id
                };

                var obj = _locationService.Details(header, request);

                if (obj != null && (EnumError)obj.error == EnumError.Success)
                {
                    model = new LocationUpsertRequest
                    {
                        id = obj.data.id,
                        name = obj.data.name,
                        parent_id = obj.data.parent_id,
                        description = obj.data.description
                    };

                    return View(model);
                }

                return NotFound(); ;
            }
        }

        //[CustomController(MenuTitle.Location, "Location", controllerName: "Location", "Cập nhật đơn vị hành chính", "Edit", "PUT", "/location/edit", false, EnumAction.Edit, false)]
        [CustomController(parentName: MenuTitle.Location, parentController: "Location", controllerName: "Location", name: "Cập nhật đơn vị hành chính",
                            actionName: "Edit", method: "PUT", route: "/location/edit", isPage: false,
                            enumAction: EnumAction.Edit, isShow: false, icon: "")]
        [HttpPut("/location/edit/{id}")]
        public IActionResult Edit(LocationUpsertRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Location.Upsert;

            var request = new LocationUpsertRequest
            {
                id = model.id,
                parent_id = model.parent_id,
                name = model.name,
                description = model.description,
            };

            var obj = _locationService.Upsert(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.code == 0)
            {
                return Ok(obj);
            }
            else
            {
                return NotFound(obj);
            }
        }

        //[DisplayName("Xóa đơn vị hành chính")]
        //[CustomController(MenuTitle.Location, "Location", "Xóa đơn vị hành chính", "Delete", "DELETE", "/location/delete", false, EnumAction.Delete, false)]
        [CustomController(parentName: MenuTitle.Location, parentController: "Location", controllerName: "Location", name: "Xóa đơn vị hành chính", actionName: "Delete",
                            method: "DELETE", route: "/location/delete", isPage: false, enumAction: EnumAction.Delete, isShow: false)]
        [DisplayName("Xóa loại thiết bị")]
        [HttpDelete]
        public IActionResult Delete(int id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Location.Delete;

            var request = new LocationDetailRequest
            {
                id = id
            };

            var obj = _locationService.Delete(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }

        private List<LocationModel> GetLocations()
        {
            var header = Header;
            header.Action = CustomConfigApi.Location.Gets;

            var request = new LocationRequest
            {

            };

            var obj = _locationService.Get(header, request);
            if(obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return obj.data.items;
            }

            return new List<LocationModel>();
        }
    }  
}