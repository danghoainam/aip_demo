﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Extensions;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;

namespace DMMS.Controllers
{
    public class DeviceController : BaseController<DeviceController>
    {
        private readonly IDeviceService _deviceService;
        private readonly ISupplierService _supplierService;
        private readonly ICategoryService _categoryService;
        private readonly IProjectService _projectService;
        private readonly ICustomerService _customerService;
        public DeviceController(IDeviceService device, ISupplierService supplier, ICategoryService category, IProjectService project, ICustomerService customer)
        {
            _deviceService = device;
            _supplierService = supplier;
            _categoryService = category;
            _projectService = project;
            _customerService = customer;
        }

        //[DisplayName("Lấy danh sách lịch sử cập nhật thiết bị")]
        [CustomController(parentName: MenuTitle.Device, parentController: "Device", controllerName: "Device", name: "Get lịch sử cập nhật thiết bị",
                           actionName: "getHistory", method: "GET", route: "device/gethistory/{uuid}", isPage: false, enumAction: EnumAction.View, isShow: false, icon: "")]
        [Route("device/gethistory/{uuid}")]
        public IActionResult getHistory(DeviceHistoryRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Devices.History;

            var obj = _deviceService.History(header, request);

            foreach (var item in obj.data.items)
            {
                if (item.images != null)
                {
                    foreach (var img in item.images)
                    {
                        img.path = CustomConfigApi.ImageDomain + "/" + img.path;
                    }
                }
            }

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        //[DisplayName("Danh sách thiết bị")]
        [CustomController(parentName: MenuTitle.Device, parentController: "Device", controllerName: "Device", name: "Get danh sách thiết bị",
                           actionName: "gets", method: "GET", route: "/device/gets", isPage: false, enumAction: EnumAction.View, isShow: false, icon: "")]
        [Route("device/gets")]
        [Route("device/gets/{customer_uuid}/{categoryId}")]
        [Route("device/gets/{customer_uuid}/{categoryId}/{packageUuid}")]
        [Route("device/gets/{taskUuid}")]
        public IActionResult gets(DeviceSearchRequest request)
        {
            if(request.states == null)
            {
                request.states = new List<int>();
            }
            var header = Header;
            header.Action = CustomConfigApi.Project.ProjectDevice;
            if (request.limit == 0)
            {
                request.limit = 10;
            }

            var obj = _deviceService.Get(header, request);
            Console.WriteLine(obj);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        [CustomController(parentName: MenuTitle.Device, parentController: "Device", controllerName: "Device", name: "Get danh sách thiết bị",
                           actionName: "gets", method: "GET", route: "/device/getslist", isPage: false, enumAction: EnumAction.View, isShow: false, icon: "")]
        public IActionResult getslist(DeviceSearchRequest request, string state)
        {
            var header = Header;
            header.Action = CustomConfigApi.Devices.Getlist;

            request.limit = 1000;

            var obj = _deviceService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        [CustomController(parentName: MenuTitle.Device, parentController: "Device", controllerName: "Device", name: "Get danh sách thiết bị theo nhà cùng cung cấp",
                           actionName: "getsdevice", method: "GET", route: "/task/getsdevice/{supplier_uuid}", isPage: false, enumAction: EnumAction.View, isShow: false, icon: "")]
        [Route("/task/getsdevice/{supplier_uuid}")]
        //[Route("/task/getsdevice/{project_uuid}")]
        public IActionResult getsdevice(DeviceSearchRequest request, string state)
        {
            var header = Header;
            header.Action = CustomConfigApi.Devices.Gets;
            if (request.limit == 0)
            {
                request.limit = 10;
            }
            var obj = _deviceService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }

        [CustomController(parentName: MenuTitle.Device, parentController: "Device", controllerName: "Device", name: "Get danh sách thiết bị theo dự án",
                           actionName: "devicepro", method: "GET", route: "/device/devicepro/{project_uuid}", isPage: false, enumAction: EnumAction.View, isShow: false, icon: "")]
        [Route("/device/devicepro/{project_uuid}")]
        public IActionResult devicepro(DeviceSearchRequest request, string state)
        {

            var header = Header;
            header.Action = CustomConfigApi.Devices.Gets;
            if (request.limit == 0)
            {
                request.limit = 10;
            }
            var obj = _deviceService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }

        [CustomController(parentName: MenuTitle.Device, parentController: "Device", controllerName: "Device", name: "Get danh sách thiết bị theo đơn vị",
                   actionName: "getDeviceInCustomers", method: "GET", route: "/device/getDeviceInCustomers", isPage: false, enumAction: EnumAction.View, isShow: false, icon: "")]
        [Route("/device/getDeviceInCustomers/{customer_uuid}")]
        public IActionResult getDeviceInCustomers(DeviceInCustomerRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.Devices.DeviceInCustomer;
            if (request.limit == 0)
            {
                request.limit = 10;
            }
            var obj = _deviceService.DeviceInCustomer(header, request);
            Console.WriteLine(obj);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }

        [CustomController(parentName: "", parentController: "Device", controllerName: "Device", name: MenuTitle.Device,
                           actionName: "index", method: "", route: "/device/index", isPage: true, enumAction: EnumAction.View, isShow: true, icon: Icon.Device)]
        public IActionResult Index()
        {
            // Start tranfer data Subheader
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Device,
                Title = MenuTitle.Device,
                SubTitle = SubMenuTitle.DeviceList
            };

            ViewData[SubHeaderData] = subHeader;
            //ViewBag.devicestate = Utilities.ExtensionMethods.ToSelectList<EnumDeviceState>();
            ViewBag.TypeCategory = Utilities.ExtensionMethods.ToSelectList<Utilities.ListSupplies>();
            //ViewBag.category = GetCategory();
            //ViewBag.project = GetProject();
            ViewBag.supplier = GetSupplier();
            ViewBag.customer = GetCustomer();

            //List<DeviceModel> lst = new List<DeviceModel>();
            //var header = Header;
            //header.Action = CustomConfigApi.Devices.Gets;
            //var resquest = new DeviceSearchRequest
            //{
            //    limit = 10,
            //};
            //var obj = _deviceService.Get(header, resquest);
            //{
            //    lst = obj.data.items;
            //}
            return View();
        }

        [DisplayName("Chi tiết thiết bị")]
        public IActionResult Detail(string id)
        {
            DeviceDataResponse detail = new DeviceDataResponse();
            var header = Header;
            header.Action = CustomConfigApi.Devices.Details;
            var request = new DeviceDetailRequest
            {
                uuid = id,
            };
            var obj = _deviceService.Detail(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null)
            {
                var model = new DeviceDataResponse();
                {

                    if (obj.data.images.Count > 0)
                    {
                        for (var i = 0; i < obj.data.images.Count; i++)
                        {
                            model.avatar = model.avatar + " " + "<img class=\"img-thumbnail\" src=" + CustomConfigApi.ImageDomain + obj.data.images[i].path + " " + "width=100px > ";
                        }
                    }
                    else
                    {
                        model.avatar = "Chưa có ảnh thiết bị";
                    }
                    //model.name = obj.data.name;
                    //model.expire_device = obj.data.expire_device;
                    //model.expire_contract = obj.data.expire_contract;
                    //model.serial = obj.data.serial;
                    //model.imei = obj.data.imei;
                    //model.supplier_name = obj.data.supplier != null ? obj.data.supplier.name : "Chưa có nhà cung cấp";
                    //model.category_name = obj.data.category != null ? obj.data.category.name : "Chưa có loại thiết bị";
                    //model.customer_name = obj.data.customer != null ? obj.data.customer.name : "Chưa có đợn vị";
                }

                detail = obj.data;
                return View(model);
            };
            return NotFound();
        }
        //public JsonResult ProjectDetail(string uuid)
        //{
        //    ProjectDataResponse lst = new ProjectDataResponse();
        //    var header = Header;
        //    header.Action = CustomConfigApi.Project.Details;
        //    var request = new ProjecDetailRequest
        //    {
        //        uuid = uuid,
        //    };
        //    var obj = _projectService.Detail(header, request);
        //    if (obj.error == 0 && obj.data != null)
        //    {
        //        return Json()
        //    }
        //    return null;

        //}

        [CustomController(parentName: MenuTitle.Device, parentController: "Device", controllerName: "Device", name: "Thêm mới thiết bị",
                       actionName: "Create", method: "", route: "/device/Create", isPage: true, enumAction: EnumAction.View, isShow: true, icon: Icon.Device)]
        //[DisplayName("Thêm mới thiết bị")]
        public IActionResult Create()
        {
            ViewBag.devicestate = Utilities.ExtensionMethods.ToSelectList<EnumDeviceState>();
            ViewBag.category = GetCategory();
            ViewBag.supplier = GetSupplier();
            ViewBag.customer = GetCustomer();
            ViewBag.project = GetProject();
            return View();
        }

        //[DisplayName("Thêm mới thiết bị")]
        [CustomController(parentName: MenuTitle.Device, parentController: "Device", controllerName: "Device", name: "Thêm mới thiết bị",
                       actionName: "Create", method: "POST", route: "/device/Create", isPage: false, enumAction: EnumAction.View, isShow: false, icon: "")]
        [HttpPost]
        public IActionResult Create(DeviceAddRequest request)
        {
            ViewBag.devicestate = Utilities.ExtensionMethods.ToSelectList<EnumDeviceState>();
            var project = GetProject();
            ViewBag.project = project;
            var category = GetCategory();
            ViewBag.category = category;
            var supplier = GetSupplier();
            ViewBag.supplier = supplier;

            var header = Header;
            header.Action = CustomConfigApi.Devices.Upsert;
            var obj = _deviceService.UpSert(header, request);
            if (obj.error == 0 && obj.data != null)
            {

                return Ok(obj);
            }
            else
            {

                obj.msg = string.IsNullOrEmpty(obj.msg) ? Constants.MESSAGE_ERROR : obj.msg;
                return NotFound(obj);
            }



        }

        //[DisplayName("Cập nhật thiết bị")]
        [CustomController(parentName: MenuTitle.Device, parentController: "Device", controllerName: "Device", name: "Cập nhật thiết bị",
                      actionName: "Edit", method: "", route: "/device/Edit", isPage: true, enumAction: EnumAction.Add, isShow: true, icon: Icon.Device)]
        public IActionResult Edit(string id)
        {

            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Device,
                Title = MenuTitle.Device,
                SubTitle = SubMenuTitle.DeviceList,
            };

            ViewBag.devicestate = Utilities.ExtensionMethods.ToSelectList<EnumDeviceState>();
            ViewBag.supplier = GetSupplier();
            ViewBag.project = GetProject();
            var model = new DeviceModel();

            if (string.IsNullOrEmpty(id))
            {
                return View(model);
            }
            else
            {
                var header = Header;
                header.Action = CustomConfigApi.Devices.Details;
                var request = new DeviceDetailRequest
                {
                    uuid = id
                };
                var obj = _deviceService.Detail(header, request);
                if (obj.error == 0 && obj.data != null)
                {
                    var objDetail = obj.data;
                    subHeader.SubTitleSmal = objDetail.name;
                    model = new DeviceModel
                    {
                        uuid = id,
                        name = objDetail.name,
                        prefix_name = objDetail.prefix_name,
                        //expire_device = objDetail.expire_device,
                        model = objDetail.model,
                        state = objDetail.state,
                        imei = objDetail.imei,
                        serial = objDetail.serial,
                        code = objDetail.code,
                        customer_uuid = objDetail.customer_uuid,
                        customer_name = objDetail.customer_name,
                        supplier = objDetail.supplier,
                        project = objDetail.project,
                        //expire_contract = objDetail.expire_contract,
                        manufacturer = objDetail.manufacturer,
                        package = objDetail.package,
                        //category_id = objDetail.category != null ? objDetail.category.id : 0,
                        //expire_contract = objDetail.expire_contract,
                        //expire_device = objDetail.expire_device,
                        //serial = objDetail.serial,
                        //imei = objDetail.imei,
                        //supplier_uuid = objDetail.supplier != null ? objDetail.supplier.uuid : null,
                        //position = objDetail.position,
                        //state = objDetail.state,
                        //project_customer_uuid = objDetail.customer != null ? objDetail.customer.project_customer_uuid : null,
                        //project_uuid = objDetail.project != null ? objDetail.project.uuid : null,
                        //customer_name = objDetail.customer != null ? objDetail.customer.name : "Chưa có đơn vị",
                        //customer_uuid = objDetail.customer != null ? objDetail.customer.uuid : null,
                        //model = objDetail.model,
                        //code = objDetail.code
                    };
                    ViewData[SubHeaderData] = subHeader;
                    return View(model);

                }
                return RedirectToAction("index", "device");
            }
        }
        //[DisplayName("Cập nhật thiết bị")]
        [CustomController(parentName: MenuTitle.Device, parentController: "Device", controllerName: "Device", name: "Cập nhật thiết bị",
                     actionName: "Edit", method: "PUT", route: "/device/Edit", isPage: false, enumAction: EnumAction.Edit, isShow: false, icon: "")]
        [HttpPut]
        public IActionResult Edit(string id, DeviceAddRequest request)
        {
            ViewBag.devicestate = Utilities.ExtensionMethods.ToSelectList<EnumDeviceState>();
            ViewBag.category = GetCategory();
            ViewBag.supplier = GetSupplier();
            var header = Header;
            header.Action = CustomConfigApi.Devices.Upsert;
            var obj = _deviceService.UpSert(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return Ok(obj);
            }
            else
            {

                obj.msg = string.IsNullOrEmpty(obj.msg) ? Constants.MESSAGE_ERROR : obj.msg;
                return NotFound(obj);
            }
        }

        [HttpGet("device/detailwithparams/{device_uuid}")]
        public IActionResult DetailWithParams(string device_uuid)
        {
            var header = Header;
            header.Action = CustomConfigApi.Devices.DetailWithParams;

            var request = new DeviceDetailWithParamsRequest()
            {
                deviceUuid = device_uuid
            };

            var obj = _deviceService.DetailWithParams(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        //[DisplayName("Xóa thiết bị")]
        [CustomController(parentName: MenuTitle.Device, parentController: "Device", controllerName: "Device", name: "Xóa thiết bị",
                     actionName: "delete", method: "DELETE", route: "/device/delete", isPage: false, enumAction: EnumAction.Delete, isShow: false, icon: "")]
        [HttpDelete]
        public IActionResult Delete(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Devices.Delete;

            var request = new DeviceDetailRequest
            {
                uuid = id
            };

            var obj = _deviceService.Delete(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }

        [CustomController(parentName: MenuTitle.Device, parentController: "Device", controllerName: "Device", name: "Get thiết bị theo uuid",
                     actionName: "DetailAjax", method: "GET", route: "/device/detailajax/{uuid}", isPage: false, enumAction: EnumAction.View, isShow: false, icon: "")]
        [HttpGet("device/detailajax/{uuid}")]
        public IActionResult DetailAjax(string uuid)
        {
            var header = Header;
            header.Action = CustomConfigApi.Devices.Details;

            var request = new DeviceDetailRequest
            {
                uuid = uuid,
            };

            var obj = _deviceService.Detail(header, request);

            if ((EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        private List<CategoryModel> GetCategory()
        {
            var headercategory = Header;
            headercategory.Action = CustomConfigApi.Category.Gets;
            var objcategory = _categoryService.Gets(headercategory, null);
            if (objcategory.error == 0 && objcategory.data != null)
            {
                return objcategory.data.items;
            }
            return null;
        }
        private List<ProjectModel> GetProject()
        {
            var headerproject = Header;
            headerproject.Action = CustomConfigApi.Project.Gets;
            var request = new ProjectSearchRequest
            {
                limit = -1,
            };
            var objproject = _projectService.Get(headerproject, request);
            if (objproject.error == 0 && objproject.data != null)
            {
                return objproject.data.items;
            }
            return null;
        }
        private List<SupplierModel> GetSupplier()
        {
            var headersupplier = Header;
            headersupplier.Action = CustomConfigApi.Suppliers.Gets;
            var request = new SupplierSearchRequest
            {
                limit = 1000,
            };
            var objsupplier = _supplierService.Get(headersupplier, request);
            if (objsupplier.error == 0 && objsupplier.data != null)
            {
                return objsupplier.data.items;
            }
            return null;
        }
        private List<CustomerModel> GetCustomer()
        {
            var headercustomer = Header;
            headercustomer.Action = CustomConfigApi.Customers.Gets;
            var request = new CustomerRequest
            {
                limit = 1000,
            };
            var objcustomer = _customerService.Get(headercustomer, request);
            if (objcustomer.error == 0 && objcustomer.data != null)
            {
                return objcustomer.data.items;
            }
            return null;
        }

        public Dictionary<string, string> Getsupplier()
        {
            var header = Header;
            header.Action = CustomConfigApi.Suppliers.Gets;
            var request = new SupplierSearchRequest
            {
                limit = 2000,
            };
            var obj = _supplierService.Get(header, request);
            if (obj.data != null && obj.error == 0)
            {
                return obj.data.items.ToList().ToDictionary(t => t.name, t => t.uuid);
            }
            return null;
        }
        public Dictionary<string, int?> GetCategoryss()
        {
            var header = Header;
            header.Action = CustomConfigApi.Category.Gets;
            var obj = _categoryService.Gets(header, new CategoryRequest());
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items.ToList().ToDictionary(t => t.name, t => t.id);
            }
            return null;
        }

        [CustomController(parentName: MenuTitle.Device, parentController: "Device", controllerName: "Device", name: "Upload thiết bị",
                     actionName: "UploadFileExcel", method: "", route: "/device/UploadFileExcel", isPage: true, enumAction: EnumAction.View, isShow: false, icon: "")]
        public ActionResult UploadFileExcel()
        {
            return View(new DeviceAddRequest());
        }

        [CustomController(parentName: MenuTitle.Device, parentController: "Device", controllerName: "Device", name: "Upload thiết bị",
                     actionName: "UploadFileExcel", method: "POST", route: "/device/UploadFileExcel", isPage: false, enumAction: EnumAction.Import, isShow: false, icon: "")]
        [HttpPost]
        public ActionResult UploadFileExcel(DeviceAddRequest entity)
        {
            var supplier = Getsupplier();
            var category = GetCategoryss();
            List<DeviceAddRequest> cashouts = ReadFileExcel(entity.FileUploadExcel);
            List<DeviceAddRequest> lstResult =
                cashouts
                .Where(p => supplier.ContainsKey(p.supplier_name.Trim()) && category.ContainsKey(p.category_name.Trim()))
                .Select(res => new DeviceAddRequest()
                {
                    supplier_uuid = supplier[res.supplier_name],
                    category_id = category[res.category_name],
                    name = res.name,
                    category_name = res.category_name,
                })
                .ToList();
            List<DeviceAddRequest> lstNotIn =
                cashouts.Where(p => !supplier.ContainsKey(p.supplier_name) || !category.ContainsKey(p.category_name)).ToList();
            var categoryname = (from p in lstNotIn.GroupBy(p => p.category_name)
                                select new
                                {
                                    count = p.Count(),
                                    p.First().category_name,
                                    state = 1,
                                }).ToList();
            var categorynametrue = (from p in lstResult.GroupBy(p => p.category_name)
                                    select new
                                    {
                                        count = p.Count(),
                                        p.First().category_name,
                                        state = 0,
                                    }).ToList();
            var obj = categoryname.Union(categorynametrue);
            //List<DeviceAddRequest> lstResult = new List<DeviceAddRequest>();
            //foreach(var item in cashouts)
            //{
            //    if(supplier.ContainsKey(item.supplier_name) && category.ContainsKey(item.category_name))
            //    {
            //        lstResult.Add(new DeviceAddRequest()
            //        {
            //            supplier_uuid = supplier[item.supplier_name],
            //            category_id = category[item.category_name],
            //        });
            //    }
            //}
            //var header = Header;
            //var request = new DeviceAddRequest
            //{
            //    name = entity.name,


            //};
            //header.Action = CustomConfigApi.Devices.Upsert;
            //var obj = _deviceService.Excel(header, request);

            return NotFound(obj);
        }
        public virtual List<DeviceAddRequest> ReadFileExcel(IFormFile file)
        {
            List<DeviceAddRequest> lstDevice = new List<DeviceAddRequest>();


            if (file.Length > 0)
            {
                string fileName = file.FileName;

                string sFileExtension = Path.GetExtension(fileName).ToLower();

                if (!AllowFileExtension(sFileExtension))
                {
                    return lstDevice;
                }

                string fullPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\input", fileName);

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);

                }

                using (var excelPackage = new ExcelPackage(new FileInfo(fullPath)))
                {
                    //get the first worksheet in the workbook
                    var workSheet = excelPackage.Workbook.Worksheets[0];
                    int colCount = workSheet.Dimension.End.Column;  //get Column Count
                    int rowCount = workSheet.Dimension.End.Row;     //get row count
                    for (int row = 0; row < rowCount; ++row)
                    {
                        for (int col = 0; col < colCount; ++col)
                        {
                            //Console.WriteLine(" Row:" + row + " column:" + col + " Value:" + workSheet.Cells[row, col].Value.ToString().Trim());
                            //this.OnReadFile(ref worksheet, ref data);
                        }
                    }
                    lstDevice = this.OnReadFile(ref workSheet, rowCount);

                    //Debug.WriteLine("h");
                }
            }

            return lstDevice;

        }
        public virtual List<DeviceAddRequest> OnReadFile(ref ExcelWorksheet worksheet, int rowCount)
        {
            List<DeviceAddRequest> lstDevice = new List<DeviceAddRequest>();
            for (int row = 2; row <= rowCount; ++row)
            {
                string device_name = worksheet.Cells[row, 1].Value.ToString().Trim();
                string category_name = worksheet.Cells[row, 2].Value.ToString().Trim();
                string supplier_name = worksheet.Cells[row, 3].Value.ToString().Trim();
                string serial = worksheet.Cells[row, 4].Value.ToString().Trim();
                string imei = worksheet.Cells[row, 5].Value.ToString().Trim();
                string model = worksheet.Cells[row, 6].Value.ToString().Trim();
                string locaiton = worksheet.Cells[row, 7].Value.ToString().Trim();
                string device_ex = worksheet.Cells[row, 8].Value.ToString().Trim();
                string contract_ex = worksheet.Cells[row, 9].Value.ToString().Trim();
                if (String.IsNullOrEmpty(device_name) || String.IsNullOrEmpty(device_name) || String.IsNullOrEmpty(category_name) || String.IsNullOrEmpty(supplier_name))
                {
                    continue;
                }

                DeviceAddRequest cashout = new DeviceAddRequest()
                {

                    name = device_name,
                    category_name = category_name,
                    supplier_name = supplier_name,
                    serial = serial,
                    imei = imei,
                    model = model,
                    position = locaiton,



                };
                lstDevice.Add(cashout);
            }
            return lstDevice;
        }
        public virtual bool AllowFileExtension(string fileExtension)
        {
            return true;
        }

    }
}
