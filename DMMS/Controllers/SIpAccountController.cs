﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Controllers
{
    public class SipAccountController : BaseController<SipAccountController>
    {
        private readonly ISipAccountService _sipAccountService;
        private readonly IStaffService _employeeService;
        public SipAccountController(ISipAccountService sipAccountService, IStaffService employeeService)
        {
            _sipAccountService = sipAccountService;
            _employeeService = employeeService;
        }
        public IActionResult Index()
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Sipaccount,
                Title = MenuTitle.SipAccount,
            };
            ViewData[SubHeaderData] = subHeader;
            return View();
        }
        
        public IActionResult gets(SipAccountRequest request)
        {
           
            var header = Header;
            header.Action = CustomConfigApi.Account.Sip;
            if (request.limit == 0)
            {
                request.limit = 10;
            }
            var obj = _sipAccountService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null && obj.data.items != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        public IActionResult create()
        {
            ViewBag.ListAcc = GetAccounts();
            return View();
        }
        [HttpPost]
        public IActionResult create(SipAccountAddRequest model)
        { 
            ViewBag.ListAcc = GetAccounts();
            var header = Header;
            header.Action = CustomConfigApi.Account.AddSip;
            var request = new SipAccountAddRequest
            {
                uuid = model.uuidep,
                exten = model.exten,
                password = model.password
            };
            var obj = _sipAccountService.Add(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null && obj.data.items != null)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }
        public IActionResult Edit(string id)
        {
            //ViewBag.ListAcc = GetAccounts();
            var model = new SipAccountAddRequest();
            var header = Header;
            header.Action = CustomConfigApi.Account.Sip;
            var request = new SipAccountRequest()
            {
                owner_uuid = id,
                limit = 1,
            };
            var obj = _sipAccountService.Get(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null && obj.data.items != null)
            {
                model = new SipAccountAddRequest
                {
                    exten = obj.data.items[0].exten,
                    password = obj.data.items[0].password,                  
                    uuid = obj.data.items[0].owner_uuid,
                };


            }
            return View(model);
        }
        [HttpPut]
        public IActionResult Edit(SipAccountAddRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Account.AddSip;
            var obj = _sipAccountService.Add(header, model);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null && obj.data.items != null)
            {
                return Ok(obj);
            }
            return NotFound(obj);
           
        }

        [DisplayName("Xóa yêu cầu")]
        [HttpDelete]
        public IActionResult Delete(string id)
        {
            var header = Header;
            header.Action = CustomConfigApi.Account.Delete;

            var request = new SipAccountDetailRequest
            {
                uuid = id
            };

            var obj = _sipAccountService.Delete(header, request);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }

        public IActionResult AmiUser()
        {
            var model = new AmiUpdateRequest();
            var header = Header;
            header.Action = CustomConfigApi.Account.Ami;
            var obj = _sipAccountService.DetailSip(header, null);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null && obj.data.items != null)
            {
                model = new AmiUpdateRequest
                {
                    ami_user = obj.data.ami_user,
                    ami_password = obj.data.ami_password
                };
            }
                return View(model);
        }
        [HttpPut]
        public IActionResult AmiUser(AmiUpdateRequest model)
        {
            var header = Header;
            header.Action = CustomConfigApi.Account.UpdateAmi;
            var obj = _sipAccountService.UpdateSip(header, model);
            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound();
        }
        private List<StaffModel> GetAccounts()
        {
            var header = Header;
            header.Action = CustomConfigApi.Staff.GetlistAccount;
            var obj = _employeeService.GetAccounts(header, null);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null && obj.data.items != null)
            {
                return obj.data.items;
            }
            return new List<StaffModel>();


        }
    }
}