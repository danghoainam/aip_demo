﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;

namespace DMMS.Controllers
{
    public class ListOfferController : BaseController<ListOfferController>
    {
        private readonly IListOfferService _listOfferService;
        private readonly ISupplierService _supplierService;
        private readonly IBunkerService _bunkerService;
        private readonly IAccountService _accountService;
        private readonly IManageFiledService _manageFiledService;
        private readonly ITreeCategoriesService _treeCategoriesService;
        public ListOfferController(IListOfferService listOfferService, ISupplierService supplierService, IBunkerService bunkerService,
            IAccountService accountService, IManageFiledService manageFiledService, ITreeCategoriesService treeCategoriesService)
        {
            _listOfferService = listOfferService;
            _supplierService = supplierService;
            _bunkerService = bunkerService;
            _accountService = accountService;
            _manageFiledService = manageFiledService;
            _treeCategoriesService = treeCategoriesService;
        }
        public IActionResult gets(OderListRequest request)
        {
            var header = Header;
            header.Action = CustomConfigApi.ListOffer.Get;
            var obj = _listOfferService.Get(header, request);

            if (request.limit == 0)
            {
                request.limit = 20;
            }

            if (obj.error == 0 && obj.data != null)
            {
                return Ok(obj);
            }
            return NotFound();
        }

        public IActionResult Index()
        {
            var subHeader = new SubHeaderModel
            {
                Icon = Icon.Bunker,
                Title = "Danh sách đề xuất thiết bị - vật tư",
            };
            ViewData[SubHeaderData] = subHeader;

            return View();
        }

        public IActionResult Create()
        {
            ViewBag.Supp = GetSupplier();
            ViewBag.Supplies = GetSupplies();
            //ViewBag.Fields = GetField();
            ViewBag.Bunkers = GetBunker();
            var acc = GetInfo();

            var model = new OfferInBunkerAddRequest()
            {
                creator_name = acc.name,
                creator_uuid = acc.uuid,
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult Create(OfferInBunkerAddRequest model)
        {

            var header = Header;
            header.Action = CustomConfigApi.Bunker.AddOfferInBunker;

            var obj = _bunkerService.AddOfferInBunker(header, model);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }

            return NotFound(obj);
        }

        public IActionResult Edit(OderListDetailRequest request, string id)
        {
            ViewBag.StateOffer = Utilities.ExtensionMethods.ToSelectList<EnumOfferInBunker>().ToList();
            var subHeader = new SubHeaderModel()
            {
                Icon = Icon.Bunker,
                Title = "Đề xuất",
            };

            var model = new OderListModel();
            request.order_uuid = id;
            var header = Header;
            header.Action = CustomConfigApi.ListOffer.Detail;
            var obj = _listOfferService.Detail(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                model = new OderListModel()
                {
                    uuid = obj.data.uuid,
                    order_person = obj.data.order_person,
                    task = obj.data.task,
                    from = obj.data.from,
                    code = obj.data.code,
                    to = obj.data.to,
                    categories = obj.data.categories,
                    status = obj.data.state,
                    time_created = obj.data.time_created,
                    time_review = obj.data.time_review,
                    review_note = obj.data.review_note,
                    reviewer = obj.data.reviewer
                };
                subHeader.SubTitle = obj.data.name;
            }
            ViewData[SubHeaderData] = subHeader;
            //model.order_person = new orderperson();
            //model.task = new task();
            //model.from = new from();
            //model.order_person.name = obj.data.order_person.name;
            //model.task.name = obj.data.task.name;
            //model.from.name = obj.data.from.name;
            //model.status = obj.data.state;
            //model.categories = obj.data.categories;
            //model.name obj.data.name;
            return View(model);
        }
        [HttpPost]
        public IActionResult Edit(string id)
        {

            return View();
        }

        [HttpPost]
        public IActionResult Decide(DecideOrderRequest request)
        {
            var acc = GetInfo();

            var header = Header;
            header.Action = CustomConfigApi.ListOffer.Decide;

            request.reviewerUuid = acc.uuid;

            var obj = _listOfferService.Decide(header, request);

            if ((EnumError)obj.error == EnumError.Success)
            {
                return Ok(obj);
            }
            return NotFound(obj);
        }

        private List<SupplierModel> GetSupplier()
        {
            var headersupplier = Header;
            headersupplier.Action = CustomConfigApi.Suppliers.Gets;
            var request = new SupplierSearchRequest
            {
                limit = 1000,
            };
            var objsupplier = _supplierService.Get(headersupplier, request);
            if (objsupplier.error == 0 && objsupplier.data != null)
            {
                return objsupplier.data.items;
            }
            return null;
        }
        private List<BunkerModel> GetBunker()
        {
            var header = Header;
            header.Action = CustomConfigApi.Bunker.Get;
            var request = new BunkerRequest()
            {
                limit = -1,
            };
            var obj = _bunkerService.Gets(header, request);
            if (obj.error == 0 && obj.data != null)
            {
                return obj.data.items;
            }
            return null;
        }

        //Lấy danh sách thiết bị vật tư
        private List<TreeCategoriesModel> GetSupplies()
        {
            var entries = new List<TreeCategoriesModel>();

            var header = Header;
            header.Action = CustomConfigApi.TreeCategories.SearchLeaf;

            var request = new CategoreSearchLeaf()
            {
                category_type = 0,
                limit = -1
            };

            var obj = _treeCategoriesService.SearchLeaf(header, request);
            if ((EnumError)obj.error == EnumError.Success)
            {
                entries = obj.data.items;
                return entries;
            }
            return entries;
        }

        //Lấy danh sách lĩnh vực
        public List<ManageFieldModel> GetField()
        {
            var entries = new List<ManageFieldModel>();
            var header = Header;
            header.Action = CustomConfigApi.Field.Get;

            ManageFieldRequest request = new ManageFieldRequest()
            {
                limit = -1
            };

            var obj = _manageFiledService.Get(header, request);

            if (obj != null && (EnumError)obj.error == EnumError.Success)
            {
                entries = obj.data.items;
                return entries;
            }

            return entries;
        }

        //Lấy thông tin tài khoản đăng nhập
        private AccountModel GetInfo()
        {
            var model = new AccountModel();
            var header = Header;
            header.Action = CustomConfigApi.Account.Profile;

            var obj = _accountService.Profile(header);
            if ((EnumError)obj.error == EnumError.Success)
            {
                model = obj.data.user_info;
                return model;
            }
            return model;
        }
 
    }
}