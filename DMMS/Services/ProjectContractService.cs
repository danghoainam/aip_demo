﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class ProjectContractService : IProjectContractservice
    {
        private readonly IRestApi<ResponseData<ContractDataResponse>> _restApi;

        public ProjectContractService(IRestApi<ResponseData<ContractDataResponse>> restApi)
        {
            _restApi = restApi;
        }

        public ResponseData<ContractDataResponse> LockSubContract(RequestHeader header, LockContractRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }

            return _restApi.CallApi(header, inputJson);
        }

        ResponseData<ContractDataResponse> IProjectContractservice.Adds(RequestHeader header, ConTractAddRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }

            return _restApi.CallApi(header, inputJson);
        }
    }
}
