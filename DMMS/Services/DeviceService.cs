﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class DeviceService : IDeviceService
    {
        private readonly IRestApi<ResponseData<DeviceDataResponse>> _restApi;
        private readonly IRestApi<ResponseData<DeviceHistoryDataResponse>> _restApi2;
        private readonly IRestApi<ResponseData<DetailWithParamDataResponse>> _restApi3;
        private readonly IRestApi<ResponseData<DeviceInCustomerDataResponse>> _restApi4;
        public DeviceService(IRestApi<ResponseData<DeviceDataResponse>> restApi, IRestApi<ResponseData<DeviceHistoryDataResponse>> restApi2,
            IRestApi<ResponseData<DetailWithParamDataResponse>> restApi3, IRestApi<ResponseData<DeviceInCustomerDataResponse>> restApi4)
        {
            _restApi = restApi;
            _restApi2 = restApi2;
            _restApi3 = restApi3;
            _restApi4 = restApi4;
        }
        public ResponseData<DeviceDataResponse> UpSert(RequestHeader header, DeviceAddRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
               inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<DeviceDataResponse> Detail(RequestHeader header, DeviceDetailRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }
        public ResponseData<DeviceDataResponse> Get(RequestHeader header, DeviceSearchRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<DeviceDataResponse> Delete(RequestHeader header, DeviceDetailRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<DeviceDataResponse> Excel(RequestHeader header, DeviceAddRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<DeviceHistoryDataResponse> History(RequestHeader header, DeviceHistoryRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi2.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<DeviceDataResponse> ProjectGet(RequestHeader header, ProjectGetDeivce request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<DeviceDataResponse> Import(RequestHeader header, ImportDevice request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }


        public ResponseData<DetailWithParamDataResponse> DetailWithParams(RequestHeader header, DeviceDetailWithParamsRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi3.CallApi(header, inputJson);
        }

        public ResponseData<DeviceInCustomerDataResponse> DeviceInCustomer(RequestHeader header, DeviceInCustomerRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi4.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }
    }
}
