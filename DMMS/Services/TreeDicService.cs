﻿using DMMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class TreeDicService : ITreeDicService
    {
        private readonly Dictionary<string, TreeModel> dict = new Dictionary<string, TreeModel>();
        public TreeModel this[string id] { get => dict[id]; set => dict[id] = value; }
    }
}
