﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class PackageService : IPackageService
    {
        private readonly IRestApi<ResponseData<PackageDataResponse>> _restApi;
        private readonly IRestApi<ResponseData<PackageCodeHintDataResponse>> _restApi1;
        public PackageService(IRestApi<ResponseData<PackageDataResponse>> restApi, IRestApi<ResponseData<PackageCodeHintDataResponse>> restApi1)
        {
            _restApi = restApi;
            _restApi1 = restApi1;
        }

        public ResponseData<PackageDataResponse> Delete(RequestHeader header, PackageDeleteRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<PackageDataResponse> Detail(RequestHeader header, PackageDetailRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<PackageDataResponse> Get(RequestHeader header, PackageRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj.data != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<PackageDataResponse> Upsert(RequestHeader header, PackageUpsertRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<PackageCodeHintDataResponse> GetHint(RequestHeader header, PackageCodeHintRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi1.CallApi(header, inputJson);
        }
    }
}
