﻿using DMMS.Interfaces;
using DMMS.Models;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class TreeCategoryService : ITreeCategoryService
    {
        private readonly Dictionary<int?, TreeCategoriesModel> dict = new Dictionary<int?, TreeCategoriesModel>();
        public TreeCategoriesModel this[int? id] { get => dict[id]; set => dict[id] = value; }
    }
}
