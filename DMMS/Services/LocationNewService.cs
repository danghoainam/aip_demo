﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class LocationNewService : ILocationNewService
    {
        private readonly IRestApi<ResponseData<LocationNewDataResponse>> _restApi;
        public LocationNewService(IRestApi<ResponseData<LocationNewDataResponse>> restApi)
        {
            _restApi = restApi;
        }

        public ResponseData<LocationNewDataResponse> GetProvinces(RequestHeader header, ProvinceRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }

        public ResponseData<LocationNewDataResponse> GetVillages(RequestHeader header, VillageRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }
        public ResponseData<LocationNewDataResponse> GetTowns(RequestHeader header, TownRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }

        public ResponseData<LocationNewDataResponse> DetailProvinces(RequestHeader header, ProvinceDetail request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }
        public ResponseData<LocationNewDataResponse> DetailTowns(RequestHeader header, TownDetail request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }
        public ResponseData<LocationNewDataResponse> DetailVillages(RequestHeader header, VillageDetail request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }
    }
}
