﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class ImageService : IImageService
    {
        private readonly IRestApi<ResponseData<ImageDataResponse>> _restApi;
        public ImageService(IRestApi<ResponseData<ImageDataResponse>> restApi)
        {
            _restApi = restApi;
        }
        public ResponseData<ImageDataResponse> Add(RequestHeader header, ImageAddRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<ImageDataResponse> Add(RequestHeader header, string input)
        {
            return _restApi.CallApi(header, input);
        }

        public ResponseData<ImageDataResponse> PostFile(RequestHeader header, List<FileUpload> input)
        {
            return _restApi.PostFile(header, input);
        }
    }
}
