﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class ReportStatisticDetailService : IReportStatisticDetailService
    {
        private readonly IRestApi<ResponseData<ReportStatisticDetailResponse>> _restApi;
        private readonly IRestApi<ResponseData<ReportStatisticDetailTab2Response>> _restApi2;
        private readonly IRestApi<ResponseData<ReportStatisticDetailAreaResponse>> _restApi3;

        public ReportStatisticDetailService(IRestApi<ResponseData<ReportStatisticDetailResponse>> restApi, IRestApi<ResponseData<ReportStatisticDetailTab2Response>> restApi2, IRestApi<ResponseData<ReportStatisticDetailAreaResponse>> restApi3)
        {
            _restApi = restApi;
            _restApi2 = restApi2;
            _restApi3 = restApi3;
        }

        public ResponseData<ReportStatisticDetailResponse> Get(RequestHeader header, ReportStatisticDetailRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<ReportStatisticDetailAreaResponse> GetList(RequestHeader header, ReportStatisticDetailAreaRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi3.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<ReportStatisticDetailTab2Response> GetTab2(RequestHeader header, ReportStatisticDetailTab2Request request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi2.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }
    }
}
