﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class ProjectCustomerService : IProjectCustomerService

    {
        private readonly IRestApi<ResponseData<ProjectGetCusDataResponse>> _restApi;
        private readonly IRestApi<ResponseData<ProjectDetailCustomerDataResponse>> _restApi1;

        public ProjectCustomerService(IRestApi<ResponseData<ProjectGetCusDataResponse>> restApi, IRestApi<ResponseData<ProjectDetailCustomerDataResponse>> restApi1)
        {
            _restApi = restApi;
            _restApi1 = restApi1;
        }

        public ResponseData<ProjectDetailCustomerDataResponse> DetailStatistics(RequestHeader header, ProjectDetailDeviceRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi1.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<ProjectGetCusDataResponse> GetCustomer(RequestHeader header, ProjectGetCustomer request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<ProjectDetailCustomerDataResponse> GetDetalDevice(RequestHeader header, ProjectDetailDeviceRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi1.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }
    }
}
