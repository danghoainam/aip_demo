﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class ReportOverviewService : IReportOverviewService
    {
        private readonly IRestApi<ResponseData<ReportOverviewDataResponse>> _restProjectApi;
        private readonly IRestApi<ResponseData<ReportTicketDataResponse>> _restTicketApi;
        private readonly IRestApi<ResponseData<ReportTaskDataResponse>> _restTaskApi;
        public ReportOverviewService(IRestApi<ResponseData<ReportOverviewDataResponse>> restProjectApi, IRestApi<ResponseData<ReportTicketDataResponse>> restTicketApi
            , IRestApi<ResponseData<ReportTaskDataResponse>> restTasktApi)
        {
            _restProjectApi = restProjectApi;
            _restTicketApi = restTicketApi;
            _restTaskApi = restTasktApi;
        }
        public ResponseData<ReportOverviewDataResponse> GetProject(RequestHeader header, RequestReportOverview request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restProjectApi.CallApi(header, inputJson);
        }

        public ResponseData<ReportTicketDataResponse> GetTicket(RequestHeader header, RequestReportOverview request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restTicketApi.CallApi(header, inputJson);
        }
        public ResponseData<ReportTaskDataResponse> GetTask(RequestHeader header, RequestReportOverview request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restTaskApi.CallApi(header, inputJson);
        }
    }
}
