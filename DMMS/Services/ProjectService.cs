﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class ProjectService : IProjectService

    {
        private readonly IRestApi<ResponseData<ProjectDataResponse>> _restApi;
        private readonly IRestApi<ResponseData<InvestorDataResponse>> _restApi1;

        public ProjectService(IRestApi<ResponseData<ProjectDataResponse>> restApi, IRestApi<ResponseData<InvestorDataResponse>> restApi1)
        {
            _restApi = restApi;
            _restApi1 = restApi1;
        }
        
        public ResponseData<ProjectDataResponse> AddCustomer(RequestHeader header, ProjectAddCustomerRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<ProjectDataResponse> Detail(RequestHeader header, ProjecDetailRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<ProjectDataResponse> Get(RequestHeader header, ProjectSearchRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }
        public ResponseData<ProjectDataResponse> UpSert(RequestHeader header, ProjectAddRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }

            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<ProjectDataResponse> Delete(RequestHeader header, Delete request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }

            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<ProjectDataResponse> Edit(RequestHeader header, ProjectEditRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }

            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<ProjectDataResponse> DeleteCustomer(RequestHeader header, DeleteCustomerRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }

            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<ProjectDataResponse> EditCustomer(RequestHeader header, ProjectEditCustomerRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }

            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<ProjectDataResponse> DeletePackage(RequestHeader header, DeletePackage request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }

            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<ProjectDataResponse> EditPackage(RequestHeader header, ProjectAddPackageRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }

            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<InvestorDataResponse> GetInvestor(RequestHeader header)
        {
            var inputJson = string.Empty;
          

            return _restApi1.CallApi(header,null);
        }

        public ResponseData<ProjectDataResponse> GetTeam(RequestHeader header, TeamProjectRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }

            return _restApi.CallApi(header, inputJson);
        }
    }
}
