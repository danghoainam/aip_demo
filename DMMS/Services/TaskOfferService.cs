﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class TaskOfferService : ITaskOfferService
    {
        private readonly IRestApi<ResponseData<TaskOfferDataResponse>> _restApi;
        public TaskOfferService(IRestApi<ResponseData<TaskOfferDataResponse>> restApi)
        {
            _restApi = restApi;
        }
        public ResponseData<TaskOfferDataResponse> Delete(RequestHeader header, TaskOfferRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }

            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<TaskOfferDataResponse> Detail(RequestHeader header, TaskOfferRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }

            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<TaskOfferDataResponse> Get(RequestHeader header, TaskOfferRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj.data != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<TaskOfferDataResponse> Upsert(RequestHeader header, TaskOfferRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }

            return _restApi.CallApi(header, inputJson);
        }
    }
}
