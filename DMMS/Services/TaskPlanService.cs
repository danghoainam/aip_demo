﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class TaskPlanService : ITaskPlanService
    {
        private readonly IRestApi<ResponseData<TaskPlanDataResponse>> _restApi;
        private readonly IRestApi<ResponseData<CustomerInPlanDataResponse>> _restApi1;
        private readonly IRestApi<ResponseData<TaskInPlanDataResponse>> _restApi2;
        public TaskPlanService(IRestApi<ResponseData<TaskPlanDataResponse>> restApi,
            IRestApi<ResponseData<CustomerInPlanDataResponse>> restApi1, IRestApi<ResponseData<TaskInPlanDataResponse>> restApi2)
        {
            _restApi = restApi;
            _restApi1 = restApi1;
            _restApi2 = restApi2;
        }

        public ResponseData<TaskPlanDataResponse> Get(RequestHeader header, TaskPlanRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj.data != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<TaskPlanDataResponse> Upsert(RequestHeader header, TaskPlanUpsertRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }      
        public ResponseData<TaskPlanDataResponse> Detail(RequestHeader header, TaskPlanDetailRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<CustomerInPlanDataResponse> DetailCustomer(RequestHeader header, TaskPlanDetailCustomerRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi1.CallApi(header, inputJson);
            if (obj.data != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<TaskInPlanDataResponse> DetailTask(RequestHeader header, TaskPlanDetailTaskRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi2.CallApi(header, inputJson);
            if (obj.data != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<TaskPlanDataResponse> Decide(RequestHeader header, TaskPlanDecideRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<TaskPlanDataResponse> Delete(RequestHeader header, TaskPlanDetailRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<TaskPlanDataResponse> Deploy(RequestHeader header, TaskPlanDeployRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }
    }
}
