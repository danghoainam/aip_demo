﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IRestApi<ResponseData<CustomerDataResponse>> _restApi;
        private readonly IRestApi<ResponseData<CustomerDataResponse>> _restApiCustom;
        private readonly IRestApi<ResponseData<ContactDataResponse>> _restApiContact;
        private readonly IRestApi<ResponseData<CustomerManagedByTeamResponse>> _restApiManaged;
        private readonly IRestApi<ResponseData<CustomerNotManagedResponse>> _restApiNotManaged;
        private readonly IRestApi<ResponseData<CustomerProjectDataResponse>> _restApicusProject;
        public CustomerService(IRestApi<ResponseData<CustomerDataResponse>> restApi, IRestApi<ResponseData<CustomerDataResponse>> restApiCustom,
            IRestApi<ResponseData<ContactDataResponse>> restApiContact, IRestApi<ResponseData<CustomerManagedByTeamResponse>> restApiManaged,
            IRestApi<ResponseData<CustomerNotManagedResponse>> restApiNotManaged, IRestApi<ResponseData<CustomerProjectDataResponse>> restApicusProject)
        {
            _restApi = restApi;
            _restApiCustom = restApiCustom;
            _restApiContact = restApiContact;
            _restApiManaged = restApiManaged;
            _restApiNotManaged = restApiNotManaged;
            _restApicusProject = restApicusProject;
        }            
        public ResponseData<CustomerDataResponse> Upsert(RequestHeader header, CustomerAddRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

       
        public ResponseData<CustomerDataResponse> Detail(RequestHeader header, CustomerDetailRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<CustomerDataResponse> Get(RequestHeader header, CustomerRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0) {
                if(obj.data != null && obj.data.pagination!=null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }
        public ResponseData<CustomerDataResponse> AddAccount(RequestHeader header, CustomerAddAccountRequest request)
        {
            throw new NotImplementedException();
        }

        public ResponseData<CustomerDataResponse> GetList(RequestHeader header, CustomerRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApiCustom.CallApi(header, inputJson);
        }

        public ResponseData<CustomerDataResponse> ListMySelft(RequestHeader header, CustomerMySelft request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<CustomerDataResponse> Delete(RequestHeader header, CustomerDetailRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<ContactDataResponse> GetContact(RequestHeader header, ContactRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApiContact.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<ContactDataResponse> DeleteContact(RequestHeader header, ContactDetailRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApiContact.CallApi(header, inputJson);
        }

        public ResponseData<ContactDataResponse> DetailContact(RequestHeader header, ContactDetailRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApiContact.CallApi(header, inputJson);
        }

        public ResponseData<ContactDataResponse> UpsertContact(RequestHeader header, ContactUpsertRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApiContact.CallApi(header, inputJson);
        }

        public ResponseData<CustomerManagedByTeamResponse> GetCustomerManagedByTeam(RequestHeader header, object request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApiManaged.CallApi(header, inputJson);
        }

        public ResponseData<CustomerNotManagedResponse> GetCustomerNotManaged(RequestHeader header, object request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApiNotManaged.CallApi(header, inputJson);
        }

        public ResponseData<CustomerDataResponse> Add(RequestHeader header, object request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<CustomerDataResponse> Remove(RequestHeader header, object request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<CustomerManagedByTeamResponse> GetCustomerManagedByStaff(RequestHeader header, object request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApiManaged.CallApi(header, inputJson);
        }

        public ResponseData<CustomerDataResponse> FindCustomer(RequestHeader header, CustomerFindRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<CustomerProjectDataResponse> GetProject(RequestHeader header, CustomerProjectRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApicusProject.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }
    }
}
