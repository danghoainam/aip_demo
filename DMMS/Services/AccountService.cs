﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class AccountService : IAccountService
    {
        private readonly IRestApi<ResponseData<AccountDataResponse>> _restApi;

        public AccountService(IRestApi<ResponseData<AccountDataResponse>> restApi)
        {
            _restApi = restApi;
        }

        public ResponseData<AccountDataResponse> ChangePassword(RequestHeader header, object request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<AccountDataResponse> Gets(RequestHeader header, AccountSearchRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<AccountDataResponse> Profile(RequestHeader header)
        {
            return _restApi.CallApi(header, null);            
        }

        public ResponseData<AccountDataResponse> ProfileSip(RequestHeader header, ProfileSip request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<AccountDataResponse> ResetPassword(RequestHeader header, object request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }
    }
}
