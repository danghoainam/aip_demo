﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class TimeWarningService : ITimeWarningService
    {
        private readonly IRestApi<ResponseData<TimeWarningDataResponse>> _restApi;
        public TimeWarningService(IRestApi<ResponseData<TimeWarningDataResponse>> restApi)
        {
            _restApi = restApi;
        }

        public ResponseData<TimeWarningDataResponse> Delete(RequestHeader header, TimeWarningDetailRequest request)
        {
            var inputJson = string.Empty;
            if(request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<TimeWarningDataResponse> Details(RequestHeader header, TimeWarningDetailRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<TimeWarningDataResponse> Gets(RequestHeader header, TimeWarningRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<TimeWarningDataResponse> Upsert(RequestHeader header, List<WarningRequest> request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }
    }
}
