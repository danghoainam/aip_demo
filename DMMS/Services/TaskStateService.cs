﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class TaskStateService : ITaskStateService
    {
        private readonly IRestApi<ResponseData<TaskStateDataResponse>> _restApi;
        public TaskStateService(IRestApi<ResponseData<TaskStateDataResponse>> restApi)
        {
            _restApi = restApi;
        }
        public ResponseData<TaskStateDataResponse> Gets(RequestHeader header, TaskStateRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj!= null && obj.error == 0)
            {
                if (obj != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }


        public ResponseData<TaskStateDataResponse> Upsert(RequestHeader header, TaskStateAddRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }
    }
}
