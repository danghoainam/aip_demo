﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class ReportStatisticDetailSuppliesTaskService : IReportStatisticDetailSuppliesTaskService
    {
        private readonly IRestApi<ResponseData<ReportStatisticDetailSuppliesTaskResponse>> _restApi;
        public ReportStatisticDetailSuppliesTaskService(IRestApi<ResponseData<ReportStatisticDetailSuppliesTaskResponse>> restApi)
        {
            _restApi = restApi;
        }

        public ResponseData<ReportStatisticDetailSuppliesTaskResponse> Get(RequestHeader header, ReportStatisticDetailSuppliesTaskRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<ReportStatisticDetailSuppliesTaskResponse> GetTab2(RequestHeader header, ReportStatisticDetailSuppliesTaskRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }
    }
}
