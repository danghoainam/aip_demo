﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class AuthenService : IAuthenService
    {
        private readonly IRestApi<ResponseData<LoginDataResponse>> _restApi;

        public AuthenService(IRestApi<ResponseData<LoginDataResponse>> restApi)
        {
            _restApi = restApi;
        }

        public ResponseData<LoginDataResponse> ChangePassword(RequestHeader header, object request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<LoginDataResponse> Login(RequestHeader header, LoginRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<LoginDataResponse> Logout(RequestHeader header)
        {
            return _restApi.CallApi(header,"");
        }
        
        public ResponseData<LoginDataResponse> RefreshToken(RequestHeader header, RefreshTokenRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

    }
}
