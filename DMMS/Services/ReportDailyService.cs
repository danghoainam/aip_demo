﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class ReportDailyService : IReportDailyService
    {
        private readonly IRestApi<ResponseData<ReportDailyDataResponse>> _restApi;
        public ReportDailyService(IRestApi<ResponseData<ReportDailyDataResponse>> restApi)
        {
            _restApi = restApi;
        }
        public ResponseData<ReportDailyDataResponse> Upsert(RequestHeader header, ReportDailyAdd request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<ReportDailyDataResponse> Detail(RequestHeader header, ReportDailyDetailRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<ReportDailyDataResponse> Edit(RequestHeader header, ReportDailyRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<ReportDailyDataResponse> Get(RequestHeader header, ReportDailyRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }
    }
}
