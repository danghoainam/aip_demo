﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class NotificationService : INotificationService
    {
        private readonly IRestApi<ResponseData<NotificationDataResponse>> _restApi;
        public NotificationService(IRestApi<ResponseData<NotificationDataResponse>> restApi)
        {
            _restApi = restApi;
        }

        public ResponseData<NotificationDataResponse> Count(RequestHeader header, NotificationRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }
        public ResponseData<NotificationDataResponse> Upsert(RequestHeader header, NotificationRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }
        public ResponseData<NotificationDataResponse> Get(RequestHeader header, NotificationRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<NotificationDataResponse> ReadNoti(RequestHeader header, NotificationRequest request)
        {

            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }
    }
}
