﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IRestApi<ResponseData<CategoryDataResponse>> _restApi;
        public CategoryService(IRestApi<ResponseData<CategoryDataResponse>> restApi)
        {
            _restApi = restApi;
        }

        public ResponseData<CategoryDataResponse> Detail(RequestHeader header, CategoryDetailRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<CategoryDataResponse> GetsTree(RequestHeader header, CategoryRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<CategoryDataResponse> Gets(RequestHeader header, CategoryRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<CategoryDataResponse> Upsert(RequestHeader header, CategoryUpsertRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }

            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<CategoryDataResponse> Delete(RequestHeader header, CategoryDetailRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }
    }
}
