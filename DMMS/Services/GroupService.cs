﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class GroupService : IGroupService
    {
        private readonly IRestApi<ResponseData<GroupDataResponse>> _restApi;
        public GroupService(IRestApi<ResponseData<GroupDataResponse>> restApi)
        {
            _restApi = restApi;
        }

        public ResponseData<GroupDataResponse> Add(RequestHeader header, GroupAddRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }
        public ResponseData<GroupDataResponse> Upsert(RequestHeader header, GroupUpdatetRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<GroupDataResponse> Upsert(RequestHeader header, object request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<GroupDataResponse> Get(RequestHeader header, GroupRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj.data != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }
        public ResponseData<GroupDataResponse> Detail(RequestHeader header, GroupDetailRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }
        public ResponseData<GroupDataResponse> GetGroupCustomer(RequestHeader header, GroupCustomerRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }public ResponseData<GroupDataResponse> GetGroupEmployees(RequestHeader header, GroupEmployeesRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<GroupDataResponse> UserInGroupAction(RequestHeader header, object request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }
    }
}
