﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class LocationService : ILocationService
    {
        private readonly IRestApi<ResponseData<LocationDataResponse>> _restApi;
        public LocationService(IRestApi<ResponseData<LocationDataResponse>> restApi)
        {
            _restApi = restApi;
        }

        public ResponseData<LocationDataResponse> Get(RequestHeader header, LocationRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<LocationDataResponse> GetLocation(RequestHeader header, LocationRequest request)
        {
             var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<LocationDataResponse> GetsTree(RequestHeader header, LocationRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }
        public ResponseData<LocationDataResponse> Upsert(RequestHeader header, LocationUpsertRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }
        public ResponseData<LocationDataResponse> Details(RequestHeader header, LocationDetailRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }
        public ResponseData<LocationDataResponse> Delete(RequestHeader header, LocationDetailRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<LocationDataResponse> GetTown(RequestHeader header, LocationTownRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }
    }
}
