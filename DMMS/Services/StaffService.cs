﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static DMMS.Models.StaffAddRequest;
using static DMMS.Models.StaffModel;

namespace DMMS.Services
{
    public class StaffService : IStaffService
    {
        private readonly IRestApi<ResponseData<StaffAddDataResponse>> _restApi;
        private readonly IRestApi<ResponseData<StaffTaskDataResponse>> _restApi1;
        private readonly IRestApi<ResponseData<StaffBaloDataResponse>> _restApi2;
        private readonly IRestApi<ResponseData<ListImportDataResponse>> _restApi3;
        private readonly IRestApi<ResponseData<StaffTeamLeaderResponse>> _restApi4;

        public StaffService(IRestApi<ResponseData<StaffAddDataResponse>> restApi, IRestApi<ResponseData<StaffTaskDataResponse>> restApi1, IRestApi<ResponseData<StaffBaloDataResponse>> restApi2,
            IRestApi<ResponseData<ListImportDataResponse>> restApi3, IRestApi<ResponseData<StaffTeamLeaderResponse>> restApi4)
        {
            _restApi = restApi;
            _restApi1 = restApi1;
            _restApi2 = restApi2;
            _restApi3 = restApi3;
            _restApi4 = restApi4;
        }

        public ResponseData<StaffAddDataResponse> Add(RequestHeader header, StaffAddRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }



        public ResponseData<StaffAddDataResponse> Get(RequestHeader header, StaffGetListRequest request)
        {

            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<StaffAddDataResponse> GetList(RequestHeader header, StaffListRequest request)
        {

            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<StaffAddDataResponse> StaffDetail(RequestHeader header, StaffDetailRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<StaffAddDataResponse> AddAccount(RequestHeader header, StaffAddRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<StaffAddDataResponse> Edit(RequestHeader header, StaffAddRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<StaffAddDataResponse> StaffListByTeamLead(RequestHeader header, StaffListByTeamLeadRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<StaffAddDataResponse> StaffListByGM(RequestHeader header, StaffListByGMRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<StaffAddDataResponse> Lockacc(RequestHeader header, StaffModel request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<StaffAddDataResponse> GetAccounts(RequestHeader header, StaffModel request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<StaffAddDataResponse> UnLockacc(RequestHeader header, StaffModel request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<StaffAddDataResponse> Delete(RequestHeader header, StaffDetailRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<StaffTaskDataResponse> GetTask(RequestHeader header, StaffTaskRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi1.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<StaffBaloDataResponse> GetBalo(RequestHeader header, StaffTaskRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi2.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<ListImportDataResponse> GetImport(RequestHeader header, StaffTaskRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi3.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<StaffTeamLeaderResponse> GetTeamLeaders(RequestHeader header, TeamLeaderRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi4.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<StaffBaloDataResponse> GetExport(RequestHeader header, StaffTaskRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
