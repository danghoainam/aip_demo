﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class BunkerService : IBunkerService
    {
        private readonly IRestApi<ResponseData<BunkerDataResponse>> _restApi;
        private readonly IRestApi<ResponseData<SuppliesDeviceInBunkerDataResponse>> _restApi1;
        private readonly IRestApi<ResponseData<StaffInBunkerDataResponse>> _restApi2;
        private readonly IRestApi<ResponseData<OfferInBunkerDataResponse>> _restApi3;
        private readonly IRestApi<ResponseData<HistoryInBunkerDataResponse>> _restApi4;
        private readonly IRestApi<ResponseData<InventoryInBunkerDataResponse>> _restApi5;
        private readonly IRestApi<ResponseData<TreeCategoriesResponse>> _restApi6;
        private readonly IRestApi<ResponseData<ListImportDataResponse>> _restApi7;
        private readonly IRestApi<ResponseData<ImportDetailResponese>> _restApi8;
        private readonly IRestApi<ResponseData<ExportDetailResponese>> _restApi11;
        private readonly IRestApi<ResponseData<ImDetailCategoriesResponse>> _restApi9;
        private readonly IRestApi<ResponseData<ImDetailDeviceResponse>> _restApi10;
        public BunkerService(IRestApi<ResponseData<BunkerDataResponse>> restApi, IRestApi<ResponseData<SuppliesDeviceInBunkerDataResponse>> restApi1,
            IRestApi<ResponseData<StaffInBunkerDataResponse>> restApi2, IRestApi<ResponseData<OfferInBunkerDataResponse>> restApi3,
            IRestApi<ResponseData<HistoryInBunkerDataResponse>> restApi4, IRestApi<ResponseData<InventoryInBunkerDataResponse>> restApi5,
            IRestApi<ResponseData<TreeCategoriesResponse>> restApi6, IRestApi<ResponseData<ListImportDataResponse>> restApi7, IRestApi<ResponseData<ImportDetailResponese>> restApi8, IRestApi<ResponseData<ImDetailCategoriesResponse>> restApi9,
            IRestApi<ResponseData<ImDetailDeviceResponse>> restApi10, IRestApi<ResponseData<ExportDetailResponese>> restApi11)
        {
            _restApi = restApi;
            _restApi1 = restApi1;
            _restApi2 = restApi2;
            _restApi3 = restApi3;
            _restApi4 = restApi4;
            _restApi5 = restApi5;
            _restApi6 = restApi6;
            _restApi7 = restApi7;
            _restApi8 = restApi8;
            _restApi9 = restApi9;
            _restApi10 = restApi10;
            _restApi11 = restApi11;
        }

        public ResponseData<BunkerDataResponse> Delete(RequestHeader header, BunkerDetailRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<BunkerDataResponse> Details(RequestHeader header, BunkerDetailRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<BunkerDataResponse> Gets(RequestHeader header, BunkerRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }

        public ResponseData<BunkerDataResponse> Upsert(RequestHeader header, BunkerUpsertRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<BunkerDataResponse> Update(RequestHeader header, BunkerUpsertRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<SuppliesDeviceInBunkerDataResponse> GetSuppliesDeviceInBunker(RequestHeader header, SuppliesDeviceInBunkerRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi1.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }

        public ResponseData<SuppliesDeviceInBunkerDataResponse> GetSuppliesDeviceInInventory(RequestHeader header, SuppliesInInventoryRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi1.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }

        public ResponseData<SuppliesDeviceInBunkerDataResponse> DetailSuppliesInBunker(RequestHeader header, SuppliesDeviceInBunkerRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi1.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }

        public ResponseData<SuppliesDeviceInBunkerDataResponse> DetailDeviceInInventory(RequestHeader header, SuppliesInInventoryRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi1.CallApi(header, inputJson);
        }

        public ResponseData<SuppliesDeviceInBunkerDataResponse> ImportSupplies(RequestHeader header, ImportBunkerRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi1.CallApi(header, inputJson);
        }

        public ResponseData<SuppliesDeviceInBunkerDataResponse> RejectExport(RequestHeader header, RejectExportRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi1.CallApi(header, inputJson);
        }

        public ResponseData<SuppliesDeviceInBunkerDataResponse> RollbackDevice(RequestHeader header, RejectExportRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi1.CallApi(header, inputJson);
        }

        public ResponseData<SuppliesDeviceInBunkerDataResponse> ExportSupplies(RequestHeader header, ExportBunkerRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi1.CallApi(header, inputJson);
        }

        public ResponseData<StaffInBunkerDataResponse> GetStaffInBunker(RequestHeader header, StaffInBunkerRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi2.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }

        public ResponseData<StaffInBunkerDataResponse> GetStaffNotInBunker(RequestHeader header, StaffInBunkerRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi2.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }

        public ResponseData<StaffInBunkerDataResponse> AddStaffInBunker(RequestHeader header, AddStaffInBunkerRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi2.CallApi(header, inputJson);
        }
        public ResponseData<StaffInBunkerDataResponse> DeleteStaffInBunker(RequestHeader header, DeleteStaffRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi2.CallApi(header, inputJson);
        }

        public ResponseData<OfferInBunkerDataResponse> GetOfferInBunker(RequestHeader header, OfferInBunkerRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi3.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }

        public ResponseData<OfferInBunkerDataResponse> AddOfferInBunker(RequestHeader header, OfferInBunkerAddRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi3.CallApi(header, inputJson);
        }

        public ResponseData<OfferInBunkerDataResponse> DetailOfferInBunker(RequestHeader header, DetailOfferInBunkerRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi3.CallApi(header, inputJson);
        }

        public ResponseData<HistoryInBunkerDataResponse> GetHistoryOfBunker(RequestHeader header, HistoryOfBunkerRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi4.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }

        public ResponseData<InventoryInBunkerDataResponse> GetInventoryInBunker(RequestHeader header, InventoryRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi5.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }

        public ResponseData<InventoryInBunkerDataResponse> DetailInventoryInBunker(RequestHeader header, InventoryDetailRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi5.CallApi(header, inputJson);
        }

        public ResponseData<InventoryInBunkerDataResponse> AddInventoryInBunker(RequestHeader header, AddInventoryRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi5.CallApi(header, inputJson);
        }

        public ResponseData<TreeCategoriesResponse> DetailCategory(RequestHeader header, TreeCategoriesDelete request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi6.CallApi(header, inputJson);
        }

        public ResponseData<TreeCategoriesResponse> CategorySearchLeaves(RequestHeader header, TreeCategoriesRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi6.CallApi(header, inputJson);
        }

        public ResponseData<ListImportDataResponse> ListImport(RequestHeader header, ListImportRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi7.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }

        public ResponseData<ListImportDataResponse> ListExport(RequestHeader header, ListImportRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi7.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }

        public ResponseData<ImportDetailResponese> DetailImport(RequestHeader header, ImportDetailRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi8.CallApi(header, inputJson);
        }

        public ResponseData<ExportDetailResponese> DetailExport(RequestHeader header, ExportDetailRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi11.CallApi(header, inputJson);
        }

        public ResponseData<ImDetailCategoriesResponse> ImDetailCategories(RequestHeader header, ImDetailCategoriesRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi9.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }

        public ResponseData<ImDetailCategoriesResponse> ExDetailCategories(RequestHeader header, ExDetailCategoriesRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi9.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }

        public ResponseData<ImDetailDeviceResponse> ImDetailDevice(RequestHeader header, ImDetailDeviceRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi10.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }

        public ResponseData<ImDetailDeviceResponse> ExDetailDevice(RequestHeader header, ExDetailDeviceRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi10.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }
            return obj;
        }
    }
}
