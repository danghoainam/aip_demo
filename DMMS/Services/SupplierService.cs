﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class SupplierService : ISupplierService
    {
        private readonly IRestApi<ResponseData<SupplierDataResponse>> _restApi;
        private readonly IRestApi<ResponseData<DeviceDataResponse>> _restApi1;
        public SupplierService(IRestApi<ResponseData<SupplierDataResponse>> restApi, IRestApi<ResponseData<DeviceDataResponse>> restApi1)
        {
            _restApi = restApi;
            _restApi1 = restApi1;
        }
        public ResponseData<SupplierDataResponse> UpSert(RequestHeader header, SupplierAddRequest request)
        {
            var inputJson = string.Empty;
            if(request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
        
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<SupplierDataResponse> Detail(RequestHeader header, SupplierDetailRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<SupplierDataResponse> Get(RequestHeader header, SupplierSearchRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<SupplierDataResponse> Delete(RequestHeader header, SupplierDetailRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }

            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<DeviceDataResponse> GetDevice(RequestHeader header, SupplierDeviceRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi1.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }
    }
}
