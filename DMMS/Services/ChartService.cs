using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using Newtonsoft.Json;
namespace DMMS.Services
{
    public class ChartService : IChartService
    {
        private readonly IRestApi<ResponseData<ChartDataResponse>> _restApi;

        public ChartService(IRestApi<ResponseData<ChartDataResponse>> restApi)
        {
            _restApi = restApi;
        }

        public ResponseData<ChartDataResponse> Get(RequestHeader header, RequestChart request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }
    }
}