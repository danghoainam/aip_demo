﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class TreeService : ITreeService
    {
        private readonly IRestApi<ResponseData<TreeDataResponse>> _restApi;
        public TreeService(IRestApi<ResponseData<TreeDataResponse>> restApi)
        {
            _restApi = restApi;
        }

        public ResponseData<TreeDataResponse> Create(RequestHeader header, TreeAddRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<TreeDataResponse> Delete(RequestHeader header, DeleteNode request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<TreeDataResponse> Department(RequestHeader header, TreeAddRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<TreeDataResponse> DepartmentNotTree(RequestHeader header, DepartmentNotTreeRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<TreeDataResponse> Get(RequestHeader header, TreeRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<TreeDataResponse> GetId(RequestHeader header, TreeRequestWidthId request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<TreeDataResponse> GetStaff(RequestHeader header, TreeRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<TreeDataResponse> GetTeam(RequestHeader header, TreeRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<TreeDataResponse> MoveDown(RequestHeader header, TreeRequest request)
        {
            throw new NotImplementedException();
        }

        public ResponseData<TreeDataResponse> MoveUp(RequestHeader header, TreeRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
