﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class KnowledgeService : IKnowledgeService
    {
        private readonly IRestApi<ResponseData<KnowledgeDataResponse>> _restApi;
        public KnowledgeService(IRestApi<ResponseData<KnowledgeDataResponse>> restApi)
        {
            _restApi = restApi;
        }
        public ResponseData<KnowledgeDataResponse> Add(RequestHeader header, KnowledgeRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }
        public ResponseData<KnowledgeDataResponse> Topic(RequestHeader header, KnowledgeRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }
        public ResponseData<KnowledgeDataResponse> Get(RequestHeader header, KnowledgeRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<KnowledgeDataResponse> QaUpsert(RequestHeader header, QaUpsert request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<KnowledgeDataResponse> Delete(RequestHeader header, KnowledgeRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<KnowledgeDataResponse> DeleteQA(RequestHeader header, KnowledgeRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }
    }
}
