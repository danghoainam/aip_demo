﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class PermissionsService : IPermissionsSerivce
    {
        private readonly IRestApi<ResponseData<PermissionsDataResponse>> _restApi;
        public PermissionsService(IRestApi<ResponseData<PermissionsDataResponse>> restApi)
        {
            _restApi = restApi;
        }
        public ResponseData<PermissionsDataResponse> Gets(RequestHeader header, PermissionsRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }
            return _restApi.CallApi(header, inputJson);
        }
    }
}
