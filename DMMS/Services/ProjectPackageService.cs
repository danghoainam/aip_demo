﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class ProjectPackageService : IProjectPackageService

    {
        private readonly IRestApi<ResponseData<ProjectGetPacDataResponse>> _restApi;
        private readonly IRestApi<ResponseData<ProjectDetailDeviceDataResponse>> _restApi1;
        
        public ProjectPackageService(IRestApi<ResponseData<ProjectGetPacDataResponse>> restApi, IRestApi<ResponseData<ProjectDetailDeviceDataResponse>> restApi1)
        {
            _restApi = restApi;
            _restApi1 = restApi1;
        }

        public ResponseData<ProjectGetPacDataResponse> AddPackage(RequestHeader header, ProjectAddPackageRequest request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }

            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<ProjectGetPacDataResponse> ImportPackage(RequestHeader header, PackageImportProject request)
        {
            var inputJson = string.Empty;
            if (request != null)
            {
                inputJson = JsonConvert.SerializeObject(request);
            }

            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<ProjectDetailDeviceDataResponse> GetDetailDevice(RequestHeader header, ProjectGetDeivce request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi1.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<ProjectGetPacDataResponse> GetPackage(RequestHeader header, ProjectGetPackage request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

       
    }
}
