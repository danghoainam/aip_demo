﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class ReportStatisticDetailTaskService : IReportStatisticDetailTaskService
    {
        private readonly IRestApi<ResponseData<ReportStatisticDetailTaskResponse>> _restApi;
        private readonly IRestApi<ResponseData<ReportStatisticDetailTaskTab3Response>> _restApi3;
        public ReportStatisticDetailTaskService(IRestApi<ResponseData<ReportStatisticDetailTaskResponse>> restApi, IRestApi<ResponseData<ReportStatisticDetailTaskTab3Response>> restApi3)
        {
            _restApi = restApi;
            _restApi3 = restApi3;
        }

        public ResponseData<ReportStatisticDetailTaskResponse> Get(RequestHeader header, ReportStatisticDetailTaskRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<ReportStatisticDetailTaskResponse> GetTab2(RequestHeader header, ReportStatisticDetailTaskTab2Request request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }

        public ResponseData<ReportStatisticDetailTaskTab3Response> GetTab3(RequestHeader header, ReportStatisticDetailTaskTab3Request request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi3.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }
    }
}
