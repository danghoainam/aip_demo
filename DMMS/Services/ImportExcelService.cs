﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using OfficeOpenXml;
using RestSharp.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class ImportExcelService : IImportExcelService
    {

        private readonly IRestApi<ResponseData<ProjectsCheckingResponse>> _restApi;
        private readonly IRestApi<ResponseData<Array>> _restApiUpExcel;
        public ImportExcelService(IRestApi<ResponseData<ProjectsCheckingResponse>> restApi, IRestApi<ResponseData<Array>> restApiUpExcel)
        {
            _restApi = restApi;
            _restApiUpExcel = restApiUpExcel;
        }

        public ResponseData<ProjectsCheckingResponse> GetAllList(RequestHeader header, RequestProjectsChecking request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApi.CallApi(header, inputJson);
        }

        public ResponseData<Array> UpSert(RequestHeader header, List<RequestProjectsAddData> request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApiUpExcel.CallApi(header, inputJson);
        }
    }
}
