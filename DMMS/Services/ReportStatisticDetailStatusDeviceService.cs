﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class ReportStatisticDetailStatusDeviceService : IReportStatisticDetailStatusDeviceService
    {
        private readonly IRestApi<ResponseData<ReportStatisticDetailStatusDeviceResponse>> _restApi;
        public ReportStatisticDetailStatusDeviceService(IRestApi<ResponseData<ReportStatisticDetailStatusDeviceResponse>> restApi)
        {
            _restApi = restApi;
        }

        public ResponseData<ReportStatisticDetailStatusDeviceResponse> Get(RequestHeader header, ReportStatisticDetailStatusDeviceRequest request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            var obj = _restApi.CallApi(header, inputJson);
            if (obj != null && obj.error == 0)
            {
                if (obj.data != null && obj.data.pagination != null)
                {
                    obj.data.pagination = Helpers.MergePagination(obj.data.pagination, request.limit, request.page);
                }
                return obj;
            }

            return obj;
        }
    }
}
