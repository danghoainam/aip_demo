﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class RestApi<T> : IRestApi<T>
    {
        private readonly IOptions<CustomConfigApi> _customConfig;
        public RestApi(IOptions<CustomConfigApi> customConfig)
        {
            _customConfig = customConfig;
        }

        private string EndPoint => _customConfig.Value.EndPoint;

        public T CallApi(RequestHeader header, string input)
        {
            try
            {
                var restApi = string.Format("{0}/{1}", EndPoint, header.Action);
                var method = Helpers.ParseEnum<Method>(header.Method);
                var client = new RestClient(restApi);
                var request = new RestRequest(method);

                if (!string.IsNullOrEmpty(header.Authorization))
                {
                    request.AddHeader("Authorization", "Bearer " + header.Authorization);
                }
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Content-Type", string.IsNullOrEmpty(header.ContentType) ? "application/json" : header.ContentType);
                if (!string.IsNullOrEmpty(input))
                {
                    request.AddParameter("application/json", input, ParameterType.RequestBody);
                }

                IRestResponse response = client.Execute(request);



               return JsonConvert.DeserializeObject<T>(response.Content);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message, typeof(T).FullName, ex);
                //return default(T);
            }

        }

        public T PostFile(RequestHeader header, List<FileUpload> files)
        {
            try
            {
                var restApi = string.Format("{0}/{1}", EndPoint, header.Action);
                var method = Helpers.ParseEnum<Method>(header.Method);
                var client = new RestClient(restApi);
                var request = new RestRequest(method);
                request.AlwaysMultipartFormData = true;
                if (!string.IsNullOrEmpty(header.Authorization))
                {
                    request.AddHeader("Authorization", "Bearer " + header.Authorization);
                }
                request.AddHeader("cache-control", "no-cache");
                //request.AddHeader("Content-Type", string.IsNullOrEmpty(header.ContentType) ? "application/json" : header.ContentType);
                if (files != null && files.Count > 0)
                {
                    for (var i = 0; i < files.Count; i++)
                    {
                        request.AddFileBytes(string.Format(files[i].FileNameFormat, i), files[i].FileBinary, files[i].FileName, files[i].ContentType);
                        //request.AddFile(item.Name, item.FileBinary, item.FileName, item.ContentType);                        
                    }
                }

                IRestResponse response = client.Execute(request);

                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message, typeof(T).FullName, ex);
                //return default(T);
            }

        }
    }
}
