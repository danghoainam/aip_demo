﻿using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Services
{
    public class ImportExcelPackageCategoriesService : IImportExcelPackageCategoriesService
    {
        private readonly IRestApi<ResponseData<Array>> _restApiUpExcel;
        public ImportExcelPackageCategoriesService(IRestApi<ResponseData<Array>> restApiUpExcel)
        {
            _restApiUpExcel = restApiUpExcel;
        }

        public ResponseData<Array> UpSert(RequestHeader header, List<RequestPackageCategoriesAddData> request)
        {
            var inputJson = JsonConvert.SerializeObject(request);
            return _restApiUpExcel.CallApi(header, inputJson);
        }
    }
}
