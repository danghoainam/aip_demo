﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace DMMS.CustomAttributes
{
    public class AuthorizeAttribute : TypeFilterAttribute
    {
        public AuthorizeAttribute(params string[] claim) : base(typeof(AuthorizeFilter))
        {
            Arguments = new object[] { claim };
        }
    }

    public class AuthorizeFilter : IAuthorizationFilter
    {
        private readonly IAuthenService _authenService;
        readonly string[] _claim;

        public AuthorizeFilter(IAuthenService authenService, params string[] claim)
        {
            _claim = claim;
            _authenService = authenService;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            string controllerName = context.RouteData.Values["controller"].ToString();
            if (controllerName.ToLower().Equals("authen")) { return; }

            var IsAuthenticated = context.HttpContext.User.Identity.IsAuthenticated;
            var claimsIndentity = context.HttpContext.User.Identity as ClaimsIdentity;

            if (IsAuthenticated)
            {
                var isTokenTimeout = Helpers.IsTokenExpiration(claimsIndentity);
                if (isTokenTimeout)
                {
                    var ttl = (claimsIndentity != null && claimsIndentity.Claims != null && claimsIndentity.FindFirst(Constants.TTL) != null) ? claimsIndentity.FindFirst(Constants.TTL).Value : string.Empty;
                    if (string.IsNullOrEmpty(ttl))
                    {
                        if (context.HttpContext.Request.IsAjaxRequest())
                        {
                            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized; //Set HTTP 401
                        }
                        else
                        {
                            context.Result = new RedirectResult("~/authen/login");
                        }
                    }
                    else
                    {
                        var currentTime = DateTime.Now.AddMinutes(Constants.TTL_DELTA);
                        var timeOut = Convert.ToDateTime(ttl);
                        if (currentTime >= timeOut)
                        {
                            var refresh_token = (claimsIndentity != null && claimsIndentity.Claims != null) ? claimsIndentity.FindFirst(Constants.REFRESH_TOKEN).Value : string.Empty;
                            if (context.HttpContext.Request.IsAjaxRequest())
                            {
                                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized; //Set HTTP 401
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(refresh_token))
                                {
                                    context.Result = new RedirectResult("~/authen/login");
                                }
                                else
                                {
                                    // refresh_token
                                    var oldToken = (claimsIndentity != null && claimsIndentity.Claims != null) ? claimsIndentity.FindFirst(Constants.TOKEN).Value : string.Empty;
                                    var username = (claimsIndentity != null && claimsIndentity.Claims != null) ? claimsIndentity.FindFirst(ClaimTypes.Name).Value : string.Empty;
                                    RefreshAsync(context, oldToken, refresh_token, username);
                                    /*
                                    var actionName = context.RouteData.Values["action"].ToString().ToLower();
                                    var currentUrl = string.Empty;
                                    if (!string.IsNullOrEmpty(actionName))
                                    {
                                        currentUrl = string.Format("?currentUrl=/{0}/{1}", controllerName, actionName);
                                    }
                                    else
                                    {
                                        currentUrl = string.Format("?currentUrl=/{0}", controllerName);
                                    }
                                    context.Result = new RedirectResult("~/authen/refresh" + currentUrl);
                                    */
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if (context.HttpContext.Request.IsAjaxRequest())
                {
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized; //Set HTTP 401
                }
                else
                {
                    context.Result = new RedirectResult("~/authen/login");
                }
            }
            return;
        }

        private void RefreshAsync(AuthorizationFilterContext context, string oldToken, string refresh_token, string username)
        {
            var header = new RequestHeader
            {
                Authorization = oldToken,
                ContentType = "application/json",
                Method = "POST",
                Action = "accounts/refresh-token"
            };

            var request = new RefreshTokenRequest
            {
                refresh_token = refresh_token
            };

            var obj = _authenService.RefreshToken(header, request);

            if (obj != null && obj.error == 0)
            {
                var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, username),
                        new Claim(Constants.TOKEN, obj.data.token),
                        new Claim(Constants.REFRESH_TOKEN, refresh_token),
                        new Claim(Constants.TTL, obj.data.ttl.Value.ToString())
                    };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                context.HttpContext.SignInAsync(
                    scheme: CookieAuthenticationDefaults.AuthenticationScheme,
                    principal: new ClaimsPrincipal(claimsIdentity),
                    properties: new AuthenticationProperties
                    {
                        IsPersistent = true,
                    }).ConfigureAwait(false);
            }
            else
            {

                context.Result = new RedirectResult("~/authen/login");

            }
        }
    }
}
