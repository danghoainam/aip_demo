﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Objects
{
    public class ResponseObject
    {
        /// <summary>
        /// Error: 0: thanh cong; 1: that bai
        /// </summary>
        public int error { get; set; }
        public string msg { get; set; }

        /// <summary>
        /// Json data
        /// </summary>
        //public object data { get; set; }
    }

    public class PaginationResponse
    {
        /// <summary>
        /// Tong so ban ghi
        /// </summary>
        public int total { get; set; }
        /// <summary>
        /// Tong so page
        /// </summary>
        public int pages { get; set; }
        /// <summary>
        /// So item tren 1 page
        /// </summary>
        public int perpage { get; set; }
        /// <summary>
        /// Page hien tai dang dung
        /// </summary>
        public int page { get; set; }
    }

    public class ResponseData<T>
    {
        /// <summary>
        /// Error: 0: thanh cong; 1: that bai
        /// </summary>
        public int error { get; set; }
        public string msg { get; set; }
        public int code { get; set; }
        public T data { get; set; }
    }

}
