﻿using DMMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;

namespace DMMS.Objects
{
    public class CustomConfigApi
    {
        public string EndPoint { get; set; }
        public string ImageDomain { get; set; }
        public string DownloadExcel { get; set; }
        public Account Account { get; set; }
        public Customers Customers { get; set; }
        public Location Location { get; set; }
        public Staff Staff { get; set; }
        public Team Team { get; set; }
        public Category Category { get; set; }
        public Project Project { get; set; }
        public Groups Groups { get; set; }
        public Devices Devices { get; set; }
        public TaskState TaskState { get; set; }
        public Suppliers Suppliers { get; set; }
        public ReportDailys ReportDailys { get; set; }
        public ReportStatisticDetail ReportStatisticDetails { get; set; }
        public ReportStatisticTroubleshoot ReportStatisticTroubleshoot { get; set; }
        public ReportStatisticDetailTask ReportStatisticDetailTask { get; set; }
        public ReportStatisticDetailTaskTab2 ReportStatisticDetailTaskTab2 { get; set; }
        public ReportStatisticDetailTaskTab3 ReportStatisticDetailTaskTab3 { get; set; }
        public ReportStatisticDetailCall ReportStatisticDetailCall { get; set; }
        public ReportStatisticDetailStatusDevice ReportStatisticDetailStatusDevice { get; set; }
        public ReportStatisticDetailSuppliesTask ReportStatisticDetailSuppliesTask { get; set; }
        public ReportStatisticDetailStatusContract ReportStatisticDetailStatusContract { get; set; }
        public Ticket Ticket { get; set; }
        public Images Images { get; set; }
        public task Task { get; set; }
        public Chart Chart { get; set; }
        public CallLog CallLog { get; set; }
        public Notification Notification { get; set; }
        public TaskOffer TaskOffer { get; set; }
        public TimeWarning TimeWarning { get; set; }
        public Contract Contract { get; set; }
        public Tree Tree { get; set; }
        public Knowledge Knowledge { get; set; }
        public TaskPlan TaskPlan { get; set; }
        public Package Package { get; set; }
        public Bunker Bunker { get; set; }
        public ListSupplies ListSuplies { get; set; }
        public TreeCategories TreeCategories { get; set; }
        public LocationNew LocationNew { get; set; }
        public Field Field { get; set; }
        public ListOffer ListOffer { get; set; }
        public Dashboard Dashboard { get; set; }
        public Permissions Permissions { get; set; }
        public Roles Roles { get; set; }
    }
    public class ListOffer
    {
        public string Get { get; set; }
        public string Detail { get; set; }
        public string Decide { get; set; }
    }
    public class Roles
    {
        public string Gets { get; set; }
        public string Add { get; set; }
        public string Detail { get; set; }
        public string Edit { get; set; }
        public string Delete { get; set; }
    }
    public class Permissions
    {
        public string Gets { get; set; }
    }
    public class Field
    {
        public string Get { get; set; }
        public string Add { get; set; }
        public string Delete { get; set; }
        public string Detail { get; set; }
        public string GetCategories { get; set; }
        public string Edit { get; set; }
    }
    public class Account
    {
        public string Login { get; set; }
        public string Logout { get; set; }
        public string RefreshToken { get; set; }
        public string Gets { get; set; }
        public string Reset { get; set; }
        public string ChangePassword { get; set; }
        public string Profile { get; set; }
        public string Sip { get; set; }
        public string AddSip { get; set; }
        public string Ami { get; set; }
        public string UpdateAmi { get; set; }
        public string Delete { get; set; }

    }
    public class Tree
    {
        public string Gets { get; set; }
        public string MoveUp { get; set; }
        public string MoveDown { get; set; }
        public string Delete { get; set; }
        public string StaffNotTree { get; set; }
        public string TeamNotTree { get; set; }
        public string Create { get; set; }
        public string Department { get; set; }
        public string DepartmentNotTree { get; set; }
    }
    public class TaskOffer
    {
        public string Gets { get; set; }
        public string Upserts { get; set; }
        public string Details { get; set; }
    }
    public class Notification
    {
        public string Gets { get; set; }
        public string Counts { get; set; }
        public string Upserts { get; set; }
        public string Read { get; set; }
    }
    public class Contract
    {
        public string Gets { get; set; }
        public string Upserts { get; set; }
        public string Details { get; set; }
        public string Delete { get; set; }
        public string GetCustomer { get; set; }
        public string Edit { get; set; }
    }
    public class Customers
    {
        public string Gets { get; set; }
        public string GetList { get; set; }
        public string Edit { get; set; }
        public string Create { get; set; }
        public string Details { get; set; }
        public string ListGroup { get; set; }
        public string CustomerMySelft { get; set; }
        public string Delete { get; set; }
        public string GetContact { get; set; }
        public string DeleteContact { get; set; }
        public string AddContact { get; set; }
        public string EditContact { get; set; }
        public string DetailContact { get; set; }
        public string CustomersNotManaged { get; set; }
        public string CustomersManagedByTeam { get; set; }
        public string CustomersManagedByStaff { get; set; }
        public string Add { get; set; }
        public string Remove { get; set; }
        public string GetsContact { get; set; }
        public string GetsProject { get; set; }

    }
    public class TaskState
    {
        public string Gets { get; set; }
        public string Upsert { get; set; }
    }
    public class CallLog
    {
        public string Gets { get; set; }
        public string Plays { get; set; }
    }
    public class Project
    {
        public string Gets { get; set; }
        public string Upsert { get; set; }
        public string Details { get; set; }
        public string AddCustomer { get; set; }
        public string Delete { get; set; }
        public string ProjectCustomer { get; set; }
        public string ProjectPackage { get; set; }
        public string ProjectDevice { get; set; }
        public string ProjectContract { get; set; }
        public string ListPackage { get; set; }
        public string AddPackage { get; set; }
        public string AddContract { get; set; }
        public string PackageDevice { get; set; }
        public string GetPacCus { get; set; }
        public string Add { get; set; }
        public string Import { get; set; }
        public string DeviceDetail { get; set; }
        public string DeleteCustomer { get; set; }
        public string EditCustomer { get; set; }
        public string DeletePackage { get; set; }
        public string UpdatePackage { get; set; }
        public string DetailDeviceCustomer { get; set; }
        public string ProjectDetailStatistics { get; set; }
        public string LockSubContract { get; set; }
        public string ProjectrealContract { get; set; }
        public string GetInvestor { get; set; }
        public string GetUnitsigned { get; set; }
        public string GetTeam { get; set; }
        public string DetailSubContract { get; set; }
        public string UpdateSubContract { get; set; }
        public string ProjectChecking { get; set; }
        public string DoneProject { get; set; }
        public string UploadExcelData { get; set; }
    }
    public class Location
    {
        public string Gets { get; set; }
        public string Details { get; set; }
        public string Delete { get; set; }
        public string Upsert { get; set; }
        public string List { get; set; }
        public string GetsTree { get; set; }
        public string Provinces { get; set; }
        public string Town { get; set; }
        public string Village { get; set; }
    }
    public class Suppliers
    {
        public string Gets { get; set; }
        public string Upsert { get; set; }
        public string Details { get; set; }
        public string Delete { get; set; }
    }

    public class Staff
    {
        public string Gets { get; set; }
        public string GetsList { get; set; }
        public string GetsListByTeamLead { get; set; }
        public string GetsListByGM { get; set; }
        public string Add { get; set; }
        public string Edit { get; set; }
        public string Detail { get; set; }
        public string Locks { get; set; }
        public string GetlistAccount { get; set; }
        public string Unlocks { get; set; }
        public string Delete { get; set; }
        public string Creator { get; set; }
        public string GetTask { get; set; }
        public string GetTaskDetail { get; set; }
        public string GetBalo { get; set; }
        public string GetImport { get; set; }
        public string GetExport { get; set; }
        public string GetTeamLeaders { get; set; }

    }
    public class Devices
    {
        public string Gets { get; set; }
        public string Details { get; set; }
        public string Upsert { get; set; }
        public string Delete { get; set; }
        public string History { get; set; }
        public string Getlist { get; set; }
        public string DetailWithParams { get; set; }
        public string DeviceInCustomer { get; set; }
    }

    public class Team
    {
        public string List { get; set; }
        public string Gets { get; set; }
        public string Upsert { get; set; }
        public string Detail { get; set; }
        public string Filter { get; set; }
        public string Add { get; set; }
        public string Remove { get; set; }
        public string ChangeLeader { get; set; }
        public string Delete { get; set; }
    }

    public class Category
    {
        public string Gets { get; set; }
        public string GetsTree { get; set; }
        public string Upsert { get; set; }
        public string Detail { get; set; }
        public string Delete { get; set; }
    }
    public class Images
    {
        public string Upsert { get; set; }
    }
    public class Groups
    {
        public string Gets { get; set; }
        public string GetsOfList { get; set; }
        public string Upsert { get; set; }
        public string Detail { get; set; }
        public string GetGroupCustomer { get; set; }
        public string GetGroupEmployees { get; set; }
        public string AddNewUser { get; set; }
        public string DeleteUser { get; set; }
    }
    public class ReportDailys
    {
        public string Gets { get; set; }
        public string Detail { get; set; }
        public string Upsert { get; set; }
    }

    public class ReportStatisticDetail
    {
        public string Gets { get; set; }
        public string GetList { get; set; }
    }

    public class ReportStatisticTroubleshoot
    {
        public string Gets { get; set; }
    }

    public class ReportStatisticDetailTask
    {
        public string Gets { get; set; }
    }

    public class ReportStatisticDetailTaskTab2
    {
        public string Gets { get; set; }
    }

    public class ReportStatisticDetailTaskTab3
    {
        public string Gets { get; set; }
    }

    public class ReportStatisticDetailCall
    {
        public string Gets { get; set; }
        public string GetCallAway { get; set; }
    }

    public class ReportStatisticDetailStatusDevice
    {
        public string Gets { get; set; }
    }

    public class ReportStatisticDetailSuppliesTask
    {
        public string Gets { get; set; }
        public string GetTab2 { get; set; }
    }

    public class ReportStatisticDetailStatusContract
    {
        public string Gets { get; set; }
    }

    public class Ticket
    {
        public string Gets { get; set; }
        public string Detail { get; set; }
        public string Upsert { get; set; }
        public string Delete { get; set; }
        public string AddDevice { get; set; }
        public string Rate { get; set; }
        public string Create { get; set; }
        public string Cancel { get; set; }
    }
    public class task
    {
        public string Gets { get; set; }
        public string Detail { get; set; }
        public string Upsert { get; set; }
        public string UpdateState { get; set; }
        public string Delete { get; set; }
        public string ChangeState { get; set; }
    }

    public class Chart
    {
        public string All { get; set; }
    }

    public class TimeWarning
    {
        public string Gets { get; set; }
        public string Upsert { get; set; }
        public string Details { get; set; }
        public string Delete { get; set; }
    }

    public class Knowledge
    {
        public string Gets { get; set; }
        public string Upsert { get; set; }
        public string Topic { get; set; }
        public string Qa { get; set; }
        public string Qaupsert { get; set; }
        public string Qaupsertdetail { get; set; }
        public string Detail { get; set; }
        public string Delete { get; set; }
        public string DeleteQA { get; set; }
    }

    public class TaskPlan
    {
        public string Add { get; set; }
        public string Get { get; set; }
        public string Upsert { get; set; }
        public string Detail { get; set; }
        public string DetailCustomer { get; set; }
        public string DetailTask { get; set; }
        public string Decide { get; set; }
        public string Delete { get; set; }
        public string Deploy { get; set; }
        public string Complete { get; set; }
        public string Reported { get; set; }
    }

    public class Package
    {
        public string Get { get; set; }
        public string Upsert { get; set; }
        public string Detail { get; set; }
        public string Delete { get; set; }
        public string GetHint { get; set; }
        public string UploadExcelData { get; set; }
    }

    public class Bunker
    {
        public string Get { get; set; }
        public string Upsert { get; set; }
        public string Update { get; set; }
        public string Details { get; set; }
        public string Delete { get; set; }
        public string GetSuppliesDeviceInBunker { get; set; }
        public string DetailSuppliesInBunker { get; set; }
        public string ImportSupplies { get; set; }
        public string ExportSupplies { get; set; }
        public string GetStaffInBunker { get; set; }
        public string GetStaffNotInBunker { get; set; }
        public string AddStaffInBunker { get; set; }
        public string DeleteStaffInBunker { get; set; }
        public string GetOfferInBunker { get; set; }
        public string AddOfferInBunker { get; set; }
        public string DetailOfferInBunker { get; set; }
        public string GetHistoryOfBunker { get; set; }
        public string GetInventoryInBunker { get; set; }
        public string DetailInventoryInBunker { get; set; }
        public string AddInventoryInBunker { get; set; }
        public string DetailCategory { get; set; }
        public string CategorySearchLeaves { get; set; }
        public string GetSuppliesDeviceInInventory { get; set; }
        public string ListImport { get; set; }
        public string ListExport { get; set; }
        public string DetailImport { get; set; }
        public string ImDetailCategories { get; set; }
        public string ImDetailCategoriesDevices { get; set; }
        public string DetailExport { get; set; }
        public string ExDetailCategories { get; set; }
        public string ExDetailCategoriesDevices { get; set; }
        public string ListExportTo { get; set; }
        public string DetailDeviceInInventory { get; set; }
        public string RejectExport { get; set; }
        public string RollbackDevice { get; set; }
    }

    public class ListSupplies
    {
        public string Get { get; set; }
        public string Edit { get; set; }
    }
    public class TreeCategories
    {
        public string Get { get; set; }
        public string GetList { get; set; }
        public string Edit { get; set; }
        public string Create { get; set; }
        public string Subtree { get; set; }
        public string Delete { get; set; }
        public string Detail { get; set; }
        public string Search { get; set; }
        public string SearchLeaf { get; set; }
        public string CheckName { get; set; }
    }

    public class LocationNew
    {
        public string GetProvinces { get; set; }
        public string GetTowns { get; set; }
        public string GetVillages { get; set; }
        public string DetailProvinces { get; set; }
        public string DetailTowns { get; set; }
        public string DetailVillages { get; set; }
    }

    public class Dashboard
    {
        public string GetsProject { get; set; }
        public string GetsTicket { get; set; }
        public string GetsTask { get; set; }
    }
}
