﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IProjectCustomerService
    {     
        ResponseData<ProjectGetCusDataResponse> GetCustomer(RequestHeader header, ProjectGetCustomer request);
        ResponseData<ProjectDetailCustomerDataResponse> GetDetalDevice(RequestHeader header, ProjectDetailDeviceRequest request);
        ResponseData<ProjectDetailCustomerDataResponse> DetailStatistics(RequestHeader header, ProjectDetailDeviceRequest request);
        //public ResponseData<LoginDataResponse> Edit(RequestHeader header, LoginRequest request);
    }
}
