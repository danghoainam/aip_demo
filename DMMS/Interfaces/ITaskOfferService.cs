﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface ITaskOfferService
    {
        ResponseData<TaskOfferDataResponse> Get(RequestHeader header, TaskOfferRequest request);
        ResponseData<TaskOfferDataResponse> Upsert(RequestHeader header, TaskOfferRequest request);
        ResponseData<TaskOfferDataResponse> Detail(RequestHeader header, TaskOfferRequest request);
        ResponseData<TaskOfferDataResponse> Delete(RequestHeader header, TaskOfferRequest request);
    }
}
