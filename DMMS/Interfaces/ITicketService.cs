﻿using DMMS.Models;
using DMMS.Objects;
using DMMS.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface ITicketService
    {
        ResponseData<TicketDataResponse> Gets(RequestHeader header, TicketRequest request);
        ResponseData<TicketDataResponse> Detail(RequestHeader header, TicketDetailRequest request);
        ResponseData<TicketDataResponse> Upsert(RequestHeader header, TicketUpsertRequest request);
        ResponseData<TicketDataResponse> Delete(RequestHeader header, TicketDetailRequest request);
        ResponseData<TicketDataResponse> Cancel(RequestHeader header, TicketCancelRequest request);
        ResponseData<TicketDataResponse> Rate(RequestHeader header, TicketRateRequest request);
    }
}
