﻿using DMMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface ITreeCategoryService
    {
        /// <summary>
        /// Lấy trả về một đối tượng theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TreeCategoriesModel this[int? id]
        {
            get;
            set;
        }

        // Thêm interface của các phương thức tương tác với Category
        // GetSubcategory -> gọi toán tử trên cập nhật vào dictionary
    }
}
