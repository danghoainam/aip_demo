﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface ILocationService
    {
        ResponseData<LocationDataResponse> Get(RequestHeader header, LocationRequest request);
        ResponseData<LocationDataResponse> GetsTree(RequestHeader header, LocationRequest request);
        ResponseData<LocationDataResponse> GetTown(RequestHeader header, LocationTownRequest request);
        ResponseData<LocationDataResponse> GetLocation(RequestHeader header, LocationRequest request);
        ResponseData<LocationDataResponse> Upsert(RequestHeader header, LocationUpsertRequest request);
        ResponseData<LocationDataResponse> Details(RequestHeader header, LocationDetailRequest request);
        ResponseData<LocationDataResponse> Delete(RequestHeader header, LocationDetailRequest request);
    }
}
