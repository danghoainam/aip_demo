﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface ITokenService
    {
        Task<string> GetBearerToken(ClaimsPrincipal user);

        Task<AuthenticationResult> RequestTokenAsync(
            ClaimsPrincipal claimsPrincipal,
            string authorizationCode,
            string redirectUri,
            string resource);
        Task ClearCacheAsync(ClaimsPrincipal claimPrincipal);

    }
}
