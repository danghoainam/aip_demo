﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface ITreeService
    {
        ResponseData<TreeDataResponse> Get(RequestHeader header, TreeRequest request);
        ResponseData<TreeDataResponse> GetId(RequestHeader header, TreeRequestWidthId request);
        ResponseData<TreeDataResponse> MoveUp(RequestHeader header, TreeRequest request);
        ResponseData<TreeDataResponse> MoveDown(RequestHeader header, TreeRequest request);
        ResponseData<TreeDataResponse> Delete(RequestHeader header, DeleteNode request);
        ResponseData<TreeDataResponse> GetStaff(RequestHeader header, TreeRequest request);
        ResponseData<TreeDataResponse> GetTeam(RequestHeader header, TreeRequest request);
        ResponseData<TreeDataResponse> Create(RequestHeader header, TreeAddRequest request);
        ResponseData<TreeDataResponse> Department(RequestHeader header, TreeAddRequest request);
        ResponseData<TreeDataResponse> DepartmentNotTree(RequestHeader header, DepartmentNotTreeRequest request);
    }
}
