﻿using DMMS.Models;
using DMMS.Objects;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IImportExcelService
    {
        ResponseData<ProjectsCheckingResponse> GetAllList(RequestHeader header, RequestProjectsChecking request);
        ResponseData<Array> UpSert(RequestHeader header, List<RequestProjectsAddData> request);
    }
}
