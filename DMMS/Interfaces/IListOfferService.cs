﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IListOfferService
    {

        ResponseData<OderListResponse> Get(RequestHeader header, OderListRequest request);
        ResponseData<OderListResponse> Detail(RequestHeader header, OderListDetailRequest request);
        ResponseData<OderListResponse> Decide(RequestHeader header, DecideOrderRequest request);
    }
}
