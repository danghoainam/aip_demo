﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Models;
using DMMS.Objects;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.Interfaces
{
    public interface IReportStatisticDetailService
    {
        ResponseData<ReportStatisticDetailResponse> Get(RequestHeader header, ReportStatisticDetailRequest request);
        ResponseData<ReportStatisticDetailTab2Response> GetTab2(RequestHeader header, ReportStatisticDetailTab2Request request);
        ResponseData<ReportStatisticDetailAreaResponse> GetList(RequestHeader header, ReportStatisticDetailAreaRequest request);
    }
}
