﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IReportStatisticDetailStatusDeviceService
    {
        ResponseData<ReportStatisticDetailStatusDeviceResponse> Get(RequestHeader header, ReportStatisticDetailStatusDeviceRequest request);
    }
}
