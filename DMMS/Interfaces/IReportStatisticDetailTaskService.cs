﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IReportStatisticDetailTaskService
    {
        ResponseData<ReportStatisticDetailTaskResponse> Get(RequestHeader header, ReportStatisticDetailTaskRequest request);
        ResponseData<ReportStatisticDetailTaskResponse> GetTab2(RequestHeader header, ReportStatisticDetailTaskTab2Request request);
        ResponseData<ReportStatisticDetailTaskTab3Response> GetTab3(RequestHeader header, ReportStatisticDetailTaskTab3Request request);
    }
}
