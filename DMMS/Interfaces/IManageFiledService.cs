﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IManageFiledService
    {
        ResponseData<ManageFiledDataResponse> Get(RequestHeader header, ManageFieldRequest request);
        ResponseData<ManageFiledDataResponse> Edit(RequestHeader header, MangeFiledAddRequest request);
        ResponseData<ManageFiledDataResponse> Add(RequestHeader header, MangeFiledAddRequest request);
        ResponseData<ManageFiledDataResponse> Delete(RequestHeader header, MangeFiledDelete request);
        ResponseData<ManageFiledDataResponse> Detail(RequestHeader header, MangeFiledDelete request);
        ResponseData<ManageFiledDataResponse> GetCategories(RequestHeader header, FiledGetCategories request);
        ResponseData<ManageFiledDataResponse> Update(RequestHeader header, MangeFiledUpdateRequest request);
    }
}
