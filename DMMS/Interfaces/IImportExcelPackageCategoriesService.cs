﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IImportExcelPackageCategoriesService
    {
        //ResponseData<ProjectsCheckingResponse> GetAllList(RequestHeader header, RequestProjectsChecking request);
        ResponseData<Array> UpSert(RequestHeader header, List<RequestPackageCategoriesAddData> request);
    }
}
