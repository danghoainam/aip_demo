﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IBunkerService
    {
        ResponseData<BunkerDataResponse> Gets(RequestHeader header, BunkerRequest request);
        ResponseData<BunkerDataResponse> Upsert(RequestHeader header, BunkerUpsertRequest request);
        ResponseData<BunkerDataResponse> Update(RequestHeader header, BunkerUpsertRequest request);
        ResponseData<BunkerDataResponse> Details(RequestHeader header, BunkerDetailRequest request);
        ResponseData<BunkerDataResponse> Delete(RequestHeader header, BunkerDetailRequest request);
        ResponseData<SuppliesDeviceInBunkerDataResponse> GetSuppliesDeviceInBunker(RequestHeader header, SuppliesDeviceInBunkerRequest request);
        ResponseData<SuppliesDeviceInBunkerDataResponse> GetSuppliesDeviceInInventory(RequestHeader header, SuppliesInInventoryRequest request);
        ResponseData<SuppliesDeviceInBunkerDataResponse> DetailSuppliesInBunker(RequestHeader header, SuppliesDeviceInBunkerRequest request);
        ResponseData<SuppliesDeviceInBunkerDataResponse> ImportSupplies(RequestHeader header, ImportBunkerRequest request);
        ResponseData<SuppliesDeviceInBunkerDataResponse> ExportSupplies(RequestHeader header, ExportBunkerRequest request);
        ResponseData<SuppliesDeviceInBunkerDataResponse> RejectExport(RequestHeader header, RejectExportRequest request);
        ResponseData<SuppliesDeviceInBunkerDataResponse> RollbackDevice(RequestHeader header, RejectExportRequest request);
        ResponseData<SuppliesDeviceInBunkerDataResponse> DetailDeviceInInventory(RequestHeader header, SuppliesInInventoryRequest request);
        ResponseData<StaffInBunkerDataResponse> GetStaffInBunker(RequestHeader header, StaffInBunkerRequest request);
        ResponseData<StaffInBunkerDataResponse> GetStaffNotInBunker(RequestHeader header, StaffInBunkerRequest request);
        ResponseData<StaffInBunkerDataResponse> AddStaffInBunker(RequestHeader header, AddStaffInBunkerRequest request);
        ResponseData<StaffInBunkerDataResponse> DeleteStaffInBunker(RequestHeader header, DeleteStaffRequest request);
        ResponseData<OfferInBunkerDataResponse> GetOfferInBunker(RequestHeader header, OfferInBunkerRequest request);
        ResponseData<OfferInBunkerDataResponse> AddOfferInBunker(RequestHeader header, OfferInBunkerAddRequest request);
        ResponseData<OfferInBunkerDataResponse> DetailOfferInBunker(RequestHeader header, DetailOfferInBunkerRequest request);
        ResponseData<HistoryInBunkerDataResponse> GetHistoryOfBunker(RequestHeader header, HistoryOfBunkerRequest request);
        ResponseData<InventoryInBunkerDataResponse> GetInventoryInBunker(RequestHeader header, InventoryRequest request);
        ResponseData<InventoryInBunkerDataResponse> DetailInventoryInBunker(RequestHeader header, InventoryDetailRequest request);
        ResponseData<InventoryInBunkerDataResponse> AddInventoryInBunker(RequestHeader header, AddInventoryRequest request);
        ResponseData<TreeCategoriesResponse> DetailCategory(RequestHeader header, TreeCategoriesDelete request);
        ResponseData<TreeCategoriesResponse> CategorySearchLeaves(RequestHeader header, TreeCategoriesRequest request);
        ResponseData<ListImportDataResponse> ListImport(RequestHeader header, ListImportRequest request);
        ResponseData<ListImportDataResponse> ListExport(RequestHeader header, ListImportRequest request);
        ResponseData<ImportDetailResponese> DetailImport(RequestHeader header, ImportDetailRequest request);
        ResponseData<ExportDetailResponese> DetailExport(RequestHeader header, ExportDetailRequest request);
        ResponseData<ImDetailCategoriesResponse> ImDetailCategories(RequestHeader header, ImDetailCategoriesRequest request);
        ResponseData<ImDetailCategoriesResponse> ExDetailCategories(RequestHeader header, ExDetailCategoriesRequest request);
        ResponseData<ImDetailDeviceResponse> ImDetailDevice(RequestHeader header, ImDetailDeviceRequest request);
        ResponseData<ImDetailDeviceResponse> ExDetailDevice(RequestHeader header, ExDetailDeviceRequest request);
    }
}
