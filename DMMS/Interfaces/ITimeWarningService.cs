﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface ITimeWarningService
    {
        ResponseData<TimeWarningDataResponse> Gets(RequestHeader header, TimeWarningRequest request);
        ResponseData<TimeWarningDataResponse> Upsert(RequestHeader header, List<WarningRequest> request);
        ResponseData<TimeWarningDataResponse> Details(RequestHeader header, TimeWarningDetailRequest request);
        ResponseData<TimeWarningDataResponse> Delete(RequestHeader header, TimeWarningDetailRequest request);
    }
}
