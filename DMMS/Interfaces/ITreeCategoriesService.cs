﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface ITreeCategoriesService
    {
        ResponseData<TreeCategoriesResponse> Get(RequestHeader header, TreeCategoriesRequest request);
        ResponseData<TreeCategoriesResponse> GetList(RequestHeader header, TreeCategoriesSearchRequest request);
        ResponseData<TreeCategoriesResponse> Delete(RequestHeader header, TreeCategoriesDelete request);
        ResponseData<TreeCategoriesResponse> Add(RequestHeader header, TreeCategoriesAddRequest request);
        ResponseData<TreeCategoriesResponse> Detail(RequestHeader header, TreeCategoriesDetail request);
        ResponseData<TreeCategoriesResponse> Update(RequestHeader header, TreeCategoriesEditRequest request);
        ResponseData<TreeCategoriesResponse> Search(RequestHeader header, BaseRequest request);
        ResponseData<TreeCategoriesResponse> SearchLeaf(RequestHeader header, CategoreSearchLeaf request);
        ResponseData<TreeCategoriesResponse> CheckName(RequestHeader header, CategoriesCheckName request);

    }
}
