﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface ITaskPlanService
    {
        ResponseData<TaskPlanDataResponse> Get(RequestHeader header, TaskPlanRequest request);
        ResponseData<TaskPlanDataResponse> Upsert(RequestHeader header, TaskPlanUpsertRequest request);
        ResponseData<TaskPlanDataResponse> Detail(RequestHeader header, TaskPlanDetailRequest request);
        ResponseData<CustomerInPlanDataResponse> DetailCustomer(RequestHeader header, TaskPlanDetailCustomerRequest request);
        ResponseData<TaskInPlanDataResponse> DetailTask(RequestHeader header, TaskPlanDetailTaskRequest request);
        ResponseData<TaskPlanDataResponse> Decide(RequestHeader header, TaskPlanDecideRequest request);
        ResponseData<TaskPlanDataResponse> Deploy(RequestHeader header, TaskPlanDeployRequest request);
        ResponseData<TaskPlanDataResponse> Delete(RequestHeader header, TaskPlanDetailRequest request);
    }
}
