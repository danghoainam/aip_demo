﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IGroupService
    {
        ResponseData<GroupDataResponse> Get(RequestHeader header, GroupRequest request);
        ResponseData<GroupDataResponse> Add(RequestHeader header, GroupAddRequest request);
        ResponseData<GroupDataResponse> Upsert(RequestHeader header, GroupUpdatetRequest request);
        ResponseData<GroupDataResponse> Upsert(RequestHeader header, object request);
        ResponseData<GroupDataResponse> Detail(RequestHeader header, GroupDetailRequest request);
        ResponseData<GroupDataResponse> GetGroupCustomer(RequestHeader header, GroupCustomerRequest request);
        ResponseData<GroupDataResponse> GetGroupEmployees(RequestHeader header, GroupEmployeesRequest request);
        ResponseData<GroupDataResponse> UserInGroupAction(RequestHeader header, object request);
    }
}
