﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface ICustomerService
    {
        ResponseData<CustomerDataResponse> Get(RequestHeader header, CustomerRequest request);
        ResponseData<CustomerDataResponse> FindCustomer(RequestHeader header, CustomerFindRequest request);
        ResponseData<CustomerDataResponse> Upsert(RequestHeader header, CustomerAddRequest request);        
        ResponseData<CustomerDataResponse> Detail(RequestHeader header, CustomerDetailRequest request);
        ResponseData<CustomerDataResponse> AddAccount(RequestHeader header, CustomerAddAccountRequest request);

        ResponseData<CustomerDataResponse> GetList(RequestHeader header, CustomerRequest request);
        ResponseData<CustomerDataResponse> ListMySelft(RequestHeader header, CustomerMySelft request);
        ResponseData<CustomerDataResponse> Delete(RequestHeader header, CustomerDetailRequest request);
        ResponseData<ContactDataResponse> GetContact(RequestHeader header, ContactRequest request);
        ResponseData<CustomerProjectDataResponse> GetProject(RequestHeader header, CustomerProjectRequest request);
        ResponseData<ContactDataResponse> DeleteContact(RequestHeader header, ContactDetailRequest request);
        ResponseData<ContactDataResponse> DetailContact(RequestHeader header, ContactDetailRequest request);
        ResponseData<ContactDataResponse> UpsertContact(RequestHeader header, ContactUpsertRequest request);

        ResponseData<CustomerManagedByTeamResponse> GetCustomerManagedByTeam(RequestHeader header, object request);
        ResponseData<CustomerNotManagedResponse> GetCustomerNotManaged(RequestHeader header, object request);

        ResponseData<CustomerManagedByTeamResponse> GetCustomerManagedByStaff(RequestHeader header, object request);

        ResponseData<CustomerDataResponse> Add(RequestHeader header, object request);
        ResponseData<CustomerDataResponse> Remove(RequestHeader header, object request);

    }
}
