﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IRolesService
    {
        ResponseData<RolesDataResponse> Gets(RequestHeader header, RolesRequest request);
        ResponseData<RolesDataResponse> Add(RequestHeader header, RolesAddRequest request);
        ResponseData<RolesDataResponse> Detail(RequestHeader header, RolesDetailRequest request);
        ResponseData<RolesDataResponse> Edit(RequestHeader header, RolesUpdateRequest request);
        ResponseData<RolesDataResponse> Delete(RequestHeader header, RolesDeleteRequest request);
    }
}
