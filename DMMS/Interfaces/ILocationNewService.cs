﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface ILocationNewService
    {
        ResponseData<LocationNewDataResponse> GetProvinces(RequestHeader header, ProvinceRequest request);
        ResponseData<LocationNewDataResponse> GetTowns(RequestHeader header, TownRequest request);
        ResponseData<LocationNewDataResponse> GetVillages(RequestHeader header, VillageRequest request);
        ResponseData<LocationNewDataResponse> DetailProvinces(RequestHeader header, ProvinceDetail request);
        ResponseData<LocationNewDataResponse> DetailTowns(RequestHeader header, TownDetail request);
        ResponseData<LocationNewDataResponse> DetailVillages(RequestHeader header, VillageDetail request);
    }
}
