﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IReportStatisticDetailSuppliesTaskService
    {
        ResponseData<ReportStatisticDetailSuppliesTaskResponse> Get(RequestHeader header, ReportStatisticDetailSuppliesTaskRequest request);
        ResponseData<ReportStatisticDetailSuppliesTaskResponse> GetTab2(RequestHeader header, ReportStatisticDetailSuppliesTaskRequest request);
    }
}
