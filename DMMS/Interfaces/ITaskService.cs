﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface ITaskService
    {
        ResponseData<TaskDataResponse> Get(RequestHeader header, TaskRequest request);
        ResponseData<TaskDataResponse> Upsert(RequestHeader header, TaskAddRequest request);
        ResponseData<TaskDataResponse> Detail(RequestHeader header, TaskDetailRequest request);
        ResponseData<TaskDataResponse> Delete(RequestHeader header, TaskDetailRequest request);
        ResponseData<TaskDataResponse> ChangeState(RequestHeader header, ChangeStateRequest request);
    }
}
