using DMMS.Models;
using DMMS.Objects;
using System;
namespace DMMS.Interfaces
{
    public interface IChartService
    {
         ResponseData<ChartDataResponse> Get(RequestHeader header, RequestChart request);
    }
}