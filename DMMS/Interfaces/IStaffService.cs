﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static DMMS.Models.StaffAddRequest;
using static DMMS.Models.StaffModel;

namespace DMMS.Interfaces
{
    public interface IStaffService
    {
        ResponseData<StaffAddDataResponse> Get(RequestHeader header, StaffGetListRequest request);
        ResponseData<StaffAddDataResponse> GetList(RequestHeader header, StaffListRequest request);
        ResponseData<StaffAddDataResponse> Add(RequestHeader header, StaffAddRequest request);
        ResponseData<StaffTaskDataResponse> GetTask(RequestHeader header, StaffTaskRequest request);
        ResponseData<StaffBaloDataResponse> GetBalo(RequestHeader header, StaffTaskRequest request);
        ResponseData<ListImportDataResponse> GetImport(RequestHeader header, StaffTaskRequest request);
        ResponseData<StaffBaloDataResponse> GetExport(RequestHeader header, StaffTaskRequest request);
        ResponseData<StaffAddDataResponse> Edit(RequestHeader header, StaffAddRequest request);
        ResponseData<StaffAddDataResponse> AddAccount(RequestHeader header, StaffAddRequest request);
        ResponseData<StaffAddDataResponse> StaffDetail(RequestHeader header, StaffDetailRequest request);
        ResponseData<StaffAddDataResponse> StaffListByTeamLead(RequestHeader header, StaffListByTeamLeadRequest request);
        ResponseData<StaffAddDataResponse> StaffListByGM(RequestHeader header, StaffListByGMRequest request);
        ResponseData<StaffAddDataResponse> Lockacc(RequestHeader header, StaffModel request);
        ResponseData<StaffAddDataResponse> UnLockacc(RequestHeader header, StaffModel request);
        ResponseData<StaffAddDataResponse> GetAccounts(RequestHeader header, StaffModel request);
        ResponseData<StaffAddDataResponse> Delete(RequestHeader header, StaffDetailRequest request);
        ResponseData<StaffTeamLeaderResponse> GetTeamLeaders(RequestHeader header, TeamLeaderRequest request);


    }
}
