﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IReportDailyService
    {
     
        ResponseData<ReportDailyDataResponse> Get(RequestHeader header, ReportDailyRequest request);
        ResponseData<ReportDailyDataResponse> Upsert(RequestHeader header, ReportDailyAdd request);
        ResponseData<ReportDailyDataResponse> Edit(RequestHeader header, ReportDailyRequest request);
        ResponseData<ReportDailyDataResponse> Detail(RequestHeader header, ReportDailyDetailRequest request);
    }
}
