﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface INotificationService
    {
        ResponseData<NotificationDataResponse> Get(RequestHeader header, NotificationRequest request);
        ResponseData<NotificationDataResponse> Count(RequestHeader header, NotificationRequest request);
        ResponseData<NotificationDataResponse> Upsert(RequestHeader header, NotificationRequest request);
        ResponseData<NotificationDataResponse> ReadNoti(RequestHeader header, NotificationRequest request);
    }
}
