﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IPackageService
    {
        ResponseData<PackageDataResponse> Get(RequestHeader header, PackageRequest request);
        ResponseData<PackageDataResponse> Upsert(RequestHeader header, PackageUpsertRequest request);
        ResponseData<PackageDataResponse> Delete(RequestHeader header, PackageDeleteRequest request);
        ResponseData<PackageDataResponse> Detail(RequestHeader header, PackageDetailRequest request);
        ResponseData<PackageCodeHintDataResponse> GetHint(RequestHeader header, PackageCodeHintRequest request);
    }
}
