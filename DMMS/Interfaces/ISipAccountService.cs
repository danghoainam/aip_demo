﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface ISipAccountService
    {
        ResponseData<SipAccountDataResponse> Get(RequestHeader header, SipAccountRequest request);
        ResponseData<SipAccountDataResponse> Add(RequestHeader header, SipAccountAddRequest request);
        ResponseData<SipAccountDataResponse> DetailSip(RequestHeader header, SipAccountAddRequest request);
        ResponseData<SipAccountDataResponse> UpdateSip(RequestHeader header, AmiUpdateRequest request);
        ResponseData<SipAccountDataResponse> Delete(RequestHeader header, SipAccountDetailRequest request);
        
    }
}
