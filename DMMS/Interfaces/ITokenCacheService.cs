﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface ITokenCacheService
    {
        Task<TokenCache> GetCacheAsync(ClaimsPrincipal claimsPrincipal);
        Task ClearCacheAsync(ClaimsPrincipal claimsPrincipal);

    }
}
