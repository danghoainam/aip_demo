﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IContractService
    {
        ResponseData<ContractDataResponse> Get(RequestHeader header, ContractSearchRequest request);
        ResponseData<ContractDataResponse> Upsert(RequestHeader header, ConTractAddRequest request);
        ResponseData<ContractDataResponse> Edit(RequestHeader header, ContractEditRequest request);
        ResponseData<ContractDataResponse> Delete(RequestHeader header, ContractDetailRequest request);
        ResponseData<ContractDataResponse> Detail(RequestHeader header, ContractDetailRequest request);
        ResponseData<ContractDataResponse> DetailSubContract(RequestHeader header, SubContractDetailRequest request);
        ResponseData<ContractDataResponse> EditSubContract(RequestHeader header, ConTractAddRequest request);
        ResponseData<ContractCustomerDataResponse> GetCustomer(RequestHeader header, ContractCustomerRequest request);
    }
}
