﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IReportStatisticDetailStatusContractService
    {
        ResponseData<ReportStatisticDetailStatusContractResponse> Get(RequestHeader header, ReportStatisticDetailStatusContractRequest request);
    }
}
