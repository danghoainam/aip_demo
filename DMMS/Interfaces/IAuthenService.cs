﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IAuthenService
    {
        ResponseData<LoginDataResponse> RefreshToken(RequestHeader header, RefreshTokenRequest request);
        ResponseData<LoginDataResponse> Login(RequestHeader header, LoginRequest request);
        ResponseData<LoginDataResponse> Logout(RequestHeader header);

    }
}
