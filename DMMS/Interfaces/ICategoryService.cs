﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface ICategoryService
    {
        ResponseData<CategoryDataResponse> Gets(RequestHeader header, CategoryRequest request);
        ResponseData<CategoryDataResponse> GetsTree(RequestHeader header, CategoryRequest request);
        ResponseData<CategoryDataResponse> Upsert(RequestHeader header, CategoryUpsertRequest request);
        ResponseData<CategoryDataResponse> Detail(RequestHeader header, CategoryDetailRequest request);
        ResponseData<CategoryDataResponse> Delete(RequestHeader header, CategoryDetailRequest request);
    }
}
