﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IImageService
    {
        ResponseData<ImageDataResponse> Add(RequestHeader header, ImageAddRequest request);
        ResponseData<ImageDataResponse> Add(RequestHeader header, string input);
        ResponseData<ImageDataResponse> PostFile(RequestHeader header, List<FileUpload> input);
    }
}
