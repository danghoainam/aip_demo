﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IDeviceService
    {
        ResponseData<DeviceDataResponse> Get(RequestHeader header, DeviceSearchRequest request);
        ResponseData<DeviceDataResponse> UpSert(RequestHeader header, DeviceAddRequest request);
        ResponseData<DeviceDataResponse> Excel(RequestHeader header, DeviceAddRequest request);
        ResponseData<DeviceDataResponse> Detail(RequestHeader header, DeviceDetailRequest request);
        ResponseData<DeviceDataResponse> Delete(RequestHeader header, DeviceDetailRequest request);
        ResponseData<DeviceHistoryDataResponse> History(RequestHeader header, DeviceHistoryRequest request);
        ResponseData<DeviceDataResponse> ProjectGet(RequestHeader header, ProjectGetDeivce request);
        ResponseData<DeviceDataResponse> Import(RequestHeader header, ImportDevice request);
        ResponseData<DetailWithParamDataResponse> DetailWithParams(RequestHeader header, DeviceDetailWithParamsRequest request);
        ResponseData<DeviceInCustomerDataResponse> DeviceInCustomer(RequestHeader header, DeviceInCustomerRequest request);
    }
}
