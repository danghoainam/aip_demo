﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IListSuppliesService
    {
        ResponseData<ListSuppliesDataRespone> Get(RequestHeader header, ListSuppliesRequest request);
        ResponseData<ListSuppliesDataRespone> Edit(RequestHeader header, ListSuppliesAddRequest request);
    }
}
