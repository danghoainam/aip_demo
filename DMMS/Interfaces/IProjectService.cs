﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IProjectService
    {
        ResponseData<ProjectDataResponse> Get(RequestHeader header, ProjectSearchRequest request);
        ResponseData<ProjectDataResponse> GetTeam(RequestHeader header, TeamProjectRequest request);
        ResponseData<ProjectDataResponse> Detail(RequestHeader header, ProjecDetailRequest request);
        ResponseData<ProjectDataResponse> UpSert(RequestHeader header, ProjectAddRequest request);
        ResponseData<ProjectDataResponse> Edit(RequestHeader header, ProjectEditRequest request);
        ResponseData<ProjectDataResponse> AddCustomer(RequestHeader header, ProjectAddCustomerRequest request);
        ResponseData<ProjectDataResponse> EditCustomer(RequestHeader header, ProjectEditCustomerRequest request);
        ResponseData<ProjectDataResponse> Delete(RequestHeader header, Delete request);
        ResponseData<ProjectDataResponse> DeleteCustomer(RequestHeader header, DeleteCustomerRequest request);
        ResponseData<ProjectDataResponse> DeletePackage(RequestHeader header, DeletePackage request);
        ResponseData<ProjectDataResponse> EditPackage(RequestHeader header, ProjectAddPackageRequest request);
        ResponseData<InvestorDataResponse> GetInvestor(RequestHeader header);
        //public ResponseData<LoginDataResponse> Edit(RequestHeader header, LoginRequest request);
    }
}
