﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IProjectPackageService
    {     
        ResponseData<ProjectGetPacDataResponse> GetPackage(RequestHeader header, ProjectGetPackage request);
        ResponseData<ProjectGetPacDataResponse> AddPackage(RequestHeader header, ProjectAddPackageRequest request);
        ResponseData<ProjectGetPacDataResponse> ImportPackage(RequestHeader header, PackageImportProject request);
        ResponseData<ProjectDetailDeviceDataResponse> GetDetailDevice(RequestHeader header, ProjectGetDeivce request);
        
    }
}
