﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface ITeamService
    {
        ResponseData<TeamDataResponse> Gets(RequestHeader header, TeamRequest request);
        ResponseData<TeamDataResponse> Upsert(RequestHeader header, TeamUpsertRequest request);
        ResponseData<TeamDataResponse> Detail(RequestHeader header, TeamDetailRequest request);
        ResponseData<TeamDataResponse> Filter(RequestHeader header, TeamRequest request);
        ResponseData<TeamDataResponse> Add(RequestHeader header, object request);
        ResponseData<TeamDataResponse> Remove(RequestHeader header, object request);
        ResponseData<TeamDataResponse> ChangeLeader(RequestHeader header, object request);
        ResponseData<TeamDataResponse> Delete(RequestHeader header, TeamDetailRequest request);
    }
}
