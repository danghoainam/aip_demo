﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface ISupplierService
    {
        ResponseData<SupplierDataResponse> Get(RequestHeader header, SupplierSearchRequest request);
        ResponseData<SupplierDataResponse> UpSert(RequestHeader header, SupplierAddRequest request);
        ResponseData<SupplierDataResponse> Detail(RequestHeader header, SupplierDetailRequest request);      
        ResponseData<SupplierDataResponse> Delete(RequestHeader header, SupplierDetailRequest request);      
        ResponseData<DeviceDataResponse> GetDevice(RequestHeader header, SupplierDeviceRequest request);      
    }
}
