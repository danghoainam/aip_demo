﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface ICallLogService
    {
        ResponseData<CallLogDataResponse> Get(RequestHeader header, CallLogRequest request);
       
    }
}
