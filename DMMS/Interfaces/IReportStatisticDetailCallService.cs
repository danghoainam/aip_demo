﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IReportStatisticDetailCallService
    {
        ResponseData<ReportStatisticDetailCallResponse> Get(RequestHeader header, ReportStatisticDetailCallRequest request);
        ResponseData<ReportStatisticDetailCallResponse> GetCallAway(RequestHeader header, ReportStatisticDetailCallRequest request);
    }
}
