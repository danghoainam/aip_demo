﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IKnowledgeService
    {
        ResponseData<KnowledgeDataResponse> Get(RequestHeader header, KnowledgeRequest request);       
        ResponseData<KnowledgeDataResponse> Add(RequestHeader header, KnowledgeRequest request);
        ResponseData<KnowledgeDataResponse> Topic(RequestHeader header, KnowledgeRequest request);
        ResponseData<KnowledgeDataResponse> QaUpsert(RequestHeader header, QaUpsert request);
        ResponseData<KnowledgeDataResponse> Delete(RequestHeader header, KnowledgeRequest request);
        ResponseData<KnowledgeDataResponse> DeleteQA(RequestHeader header, KnowledgeRequest request);

    }
}
