﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IReportOverviewService
    {
        ResponseData<ReportOverviewDataResponse> GetProject(RequestHeader header, RequestReportOverview request); 
        ResponseData<ReportTicketDataResponse> GetTicket(RequestHeader header, RequestReportOverview request);
        ResponseData<ReportTaskDataResponse> GetTask(RequestHeader header, RequestReportOverview request);
    }
}
