﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface IProjectContractservice
    {     
        ResponseData<ContractDataResponse> Adds(RequestHeader header, ConTractAddRequest request);
        ResponseData<ContractDataResponse> LockSubContract(RequestHeader header, LockContractRequest request);
        //public ResponseData<LoginDataResponse> Edit(RequestHeader header, LoginRequest request);
    }
}
