﻿using DMMS.Models;
using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Interfaces
{
    public interface ITaskStateService
    {
        ResponseData<TaskStateDataResponse> Gets(RequestHeader header, TaskStateRequest request);
        ResponseData<TaskStateDataResponse> Upsert(RequestHeader header, TaskStateAddRequest request);
    }
}
