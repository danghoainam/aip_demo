﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class PermissionsModel : BaseModel
    {
        public int type { get; set; }
        public bool hasPermission { get; set; }
    }
    public class PermissionsRequest
    {
        public int type { get; set; }
    }
    public class PermissionsDataResponse
    {
        public PermissionsDataResponse()
        {
            items = new List<PermissionsModel>();
            pagination = new PaginationResponse();
        }
        public List<PermissionsModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
}
