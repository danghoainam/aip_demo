﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class TaskPlanModel : BaseModel
    {
        public long? costEstimate { get; set; } //Chi phí dự kiến
        public string costEstimate_string { get; set; }
        public string costEstimate_format
        {
            get
            {
                if (costEstimate.HasValue)
                {
                    return string.Format(CultureInfo.GetCultureInfo("vi-VN"), "{0:C0}", costEstimate);
                }
                return "";
            }
        }
        public int? state { get; set; }
        public string planStateName { get; set; } //Tên trạng thái
        public int? creatorID { get; set; } //Mã người tạo
        public string creatorName { get; set; } //Tên người tạo
        public string startdate { get; set; }
        public string enddate { get; set; }
        public string projectName { get; set; }
        public string projectUuid { get; set; }
        public string countCustomer { get; set; }
        public string teamUuid { get; set; }
        public DateTime? startTime { get; set; } //Thời gian bắt đầu
        public DateTime? finishTime { get; set; } //Thời gian kết thúc
        public string startTime_format
        {
            get
            {
                if (startTime.HasValue)
                {
                    return startTime.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
        public string finishTime_format
        {
            get
            {
                if (finishTime.HasValue)
                {
                    return finishTime.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
    }

    public class TaskPlanDetailModel
    {
        public List<Customers> customers { get; set; }
        public string uuid { get; set; }
        public string name { get; set; }
        public long? costEstimate { get; set; }
        public string description { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
        public string projectName { get; set; }
        public string projectUuid { get; set; }
        public string teamUuid { get; set; }
        public string responserUuid { get; set; }
        public DateTime? startTime { get; set; } //Thời gian bắt đầu
        public DateTime? finishTime { get; set; } //Thời gian kết thúc
        public string cost_update { get; set; }
        public string startTime_format
        {
            get
            {
                if (startTime.HasValue)
                {
                    return startTime.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }


        }
        public string finishTime_format
        {
            get
            {
                if (finishTime.HasValue)
                {
                    return finishTime.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
        public string costEstimate_format
        {
            get
            {
                if (costEstimate.HasValue)
                {
                    return string.Format(CultureInfo.GetCultureInfo("vi-VN"), "{0:C0}", costEstimate);
                }
                return "";
            }

        }
        public int? reviewId { get; set; }
        public string reviewName { get; set; }
        public string reviewNote { get; set; }
        public string reviewAt_fomat
        {
            get
            {
                if (reviewAt.HasValue)
                {
                    return reviewAt.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
        public string reviewAt_display { get; set; }
        public DateTime? reviewAt { get; set; }
        public int? state { get; set; }
        public List<string> customers_format { get; set; }
    }

    //customer in plan
    public class CustomerInPlanModel
    {
        public string uuid { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string coordinate { get; set; }
        public string provinceName { get; set; }
        public string townName { get; set; }
        public string villageName { get; set; }
    }

    //task in plan
    public class TaskInPlanModel
    {
        public int id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string note { get; set; }
        public int? state { get; set; }
        public string customerName { get; set; }
        public string technicianName { get; set; }
        public string technicianPhoneNumber { get; set; }
        public float percent { get; set; }
    }

    #region: Request
    public class TaskPlanRequest : BaseRequest
    {
        //public CustomerModel customer { get; set; }
        public string customerUuid { get; set; }
        //public List<int> filterStatus { get; set; }
        public List<int> filterStatus
        {
            get
            {
                if (!string.IsNullOrEmpty(stateIds))
                {
                    return stateIds.Split(",".ToCharArray()).ToList().Select(s => int.Parse(s)).ToList();
                }
                return new List<int>();
            }
        }
        public string stateIds { get; set; }
    }

    public class TaskPlanDetailRequest
    {
        public string plan_uuid { get; set; }
    }

    public class TaskPlanDetailCustomerRequest : BaseRequest
    {
        public string plan_uuid { get; set; }
    }

    public class TaskPlanDetailTaskRequest : BaseRequest
    {
        public string plan_uuid { get; set; }
        public List<int> filterStatusTask
        {
            get
            {
                if (!string.IsNullOrEmpty(taskStateIds))
                {
                    return taskStateIds.Split(",".ToCharArray()).ToList().Select(s => int.Parse(s)).ToList();
                }
                return new List<int>();
            }
        }
        public string taskStateIds { get; set; }
    }

    public class TaskPlanDecideRequest
    {
        public string plan_uuid { get; set; }
        public string reviewNote { get; set; }
        public string reviewUuid { get; set; }
        public bool accept { get; set; }
    }
    public class TaskPlanDeployRequest
    {
        public string planUuid { get; set; }
        public string reviewNote { get; set; }
        public string reviewUuid { get; set; }
    }
    public class TaskPlanUpsertRequest
    {
        public string planUuid { get; set; }
        //public string uuid { get; set; }
        public string name { get; set; }
        public long? costEstimate { get; set; } //chi phí
        public string creatorUuid { get; set; } //Người tạo(Nhân viên)
        //public List<PlanUpsertStaffRequest> staffUuids { get; set; } //Danh sách nhân viên tham gia
        public List<string> customerUuids { get; set; } //Danh sách khách hàng tham gia
        public List<string> customers { get; set; }
        public DateTime? startTime { get; set; } //Ngày dự kiến bắt đầu
        public DateTime? finishTime { get; set; } //Ngày dự kiến kết thúc
        public string startdate { get; set; }
        public string projectUuid { get; set; }
        public string enddate { get; set; }
        public string description { get; set; }
        public string costEstimate_string { get; set; }
        public string startTime_format { get; set; }
        public string finishTime_format { get; set; }
        public int? state { get; set; }
        public string reviewUuid { get; set; }
        public string reviewNote { get; set; }
        public string responserUuid { get; set; }
    }

    //public class PlanUpsertStaffRequest
    //{
    //    public string uuid { get; set; }
    //    public int? type { get; set; }
    //}
    #endregion

    #region: Response
    public class TaskPlanDataResponse : TaskPlanModel
    {
        public TaskPlanDataResponse()
        {
            items = new List<TaskPlanModel>();
            item = new TaskPlanDetailModel();
            pagination = new PaginationResponse();
        }

        public List<TaskPlanModel> items { get; set; }
        public TaskPlanDetailModel item { get; set; }
        public PaginationResponse pagination { get; set; }

    }
    public class Customers
    {
        public string uuid { get; set; }
        public string name { get; set; }
    }
    public class CustomerInPlanDataResponse : CustomerInPlanModel
    {
        public CustomerInPlanDataResponse()
        {

            items = new List<CustomerInPlanModel>();
            pagination = new PaginationResponse();
        }
        public List<CustomerInPlanModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }

    public class TaskInPlanDataResponse : TaskInPlanModel
    {
        public TaskInPlanDataResponse()
        {

            items = new List<TaskInPlanModel>();
            pagination = new PaginationResponse();
        }
        public List<TaskInPlanModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
    #endregion
}
