﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class AuthenModel
    {
    }

    public class LoginModel
    {
        [Required(ErrorMessage ="Bạn chưa nhập tên đăng nhập")]
        public string Username { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Bạn chưa nhập mật khẩu")]
        [MinLength(6, ErrorMessage = "Mật khẩu phải từ 6 ký tự trở lên")]
        public string Password { get; set; }
        public string ReturnUrl { get; set; }
    }

    #region Request
    public class LoginRequest
    {
        public string username { get; set; }
        public string password { get; set; }
    }

    public class RefreshTokenRequest
    {
        public string refresh_token { get; set; }
    }


    #endregion

    #region Response
    public class LoginDataResponse
    {
        public string token { get; set; }
        public string refresh_token { get; set; }
        public DateTime? ttl { get; set; }
    }
    #endregion 
}
