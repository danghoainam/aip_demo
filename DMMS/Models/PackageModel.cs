﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class PackageModel : BaseModel
    {
        public string code { get; set; }
    }

    public class PackageCodeHintModel
    {
        public List<PackageHint> packages { get; set; }
        public List<CategoryHint> categories { get; set; }
    }

    public class PackageHint
    {
        public int id { get; set; }
        public string uuid { get; set; }
        public string name { get; set; }
    }

    public class CategoryHint
    {
        public int id { get; set; }
        public string package_name { get; set; }
        public int category_id { get; set; }
        public string category_name { get; set; }
        public int count { get; set; }
        public int time_maintain { get; set; }
    }

    #region Request
    public class PackageRequest : BaseRequest
    {

    }

    public class PackageUpsertRequest
    {
        public int? id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string description { get; set; }
    }

    public class PackageDetailRequest
    {
        public int? package_code_id { get; set; }
    }

    public class PackageCodeHintRequest : PaginationRequest
    {
        public int packageCodeId { get; set; }
    }

    public class PackageDeleteRequest
    {
        public int? id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
    }
    #endregion

    #region Response
    public class PackageDataResponse : PackageModel
    {
        public PackageDataResponse()
        {
            items = new List<PackageModel>();
            pagination = new PaginationResponse();
        }
        public List<PackageModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }

    public class PackageCodeHintDataResponse
    {
        public PackageCodeHintDataResponse()
        {
            items = new PackageCodeHintModel();
        }
        public PackageCodeHintModel items { get; set; }
    }
    #endregion
}
