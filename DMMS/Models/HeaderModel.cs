﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class HeaderModel
    {
    }

    public class SubHeaderModel
    {
        public string Icon { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string SubTitleSmal { get; set; }
        public string TitleLink { get; set; }
        public int? Toolbar { get; set; }


    }
         
}
