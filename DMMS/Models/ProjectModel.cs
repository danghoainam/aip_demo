﻿using DMMS.Objects;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using static DMMS.Models.ProjectCustomerModel;

namespace DMMS.Models
{
    public class ProjectModel : BaseModel
    {
        public string team_name { get; set; }
        public string code { get; set; }
        public int count_package_device { get; set; }
        public int count_customers { get; set; }
        public string content { get; set; }
        public string name_display
        {
            get
            {
                if (!string.IsNullOrEmpty(name))
                {
                    var needsub = 70;
                    if (name.Length > needsub)
                    {
                        return name.Substring(0, needsub) + "...";
                    }
                    return name;
                }
                return string.Empty;
            }
        }
        public DateTime? time_valid_end { get; set; }
        public string time_valid_end_format
        {
            get
            {
                if (time_valid_end.HasValue)
                {
                    return time_valid_end.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
        public string field_name { get; set; }
        public string contract_code { get; set; }
        public long? contract_cost { get; set; }
        public string contract_cost_format
        {
            get
            {
                if (contract_cost.HasValue)
                {
                    return string.Format(CultureInfo.GetCultureInfo("vi-VN"), "{0:C0}", contract_cost);
                }
                return "";
            }
        }
        public string customer_singed { get; set; }
        public string province_name { get; set; }
        public int project_state_id { get; set; }
        public string project_state_name { get; set; }



    }
    public class Unit_signed
    {
        public string unit_signed_uuid { get; set; }
        public string unit_signed_name { get; set; }
    }
    public class Investor
    {
        public string investor_uuid { get; set; }
        public string investor_name { get; set; }
    }
    public class ProvinceCustomer
    {
        public int province_id { get; set; }
        public string province_name { get; set; }

    }

    #region Request
    public class ProjectSearchRequest : BaseRequest
    {
        public int? provinceId { get; set; }
        public int? fieldId { get; set; }
        public string teamUuid { get; set; }
    }
    public class TeamProjectRequest
    {
        public int? type { get; set; }
    }
    public class ProjecDetailRequest : BaseRequest
    {
        public string uuid { get; set; }
        public string project_uuid { get; set; }
    }

    public class ProjectAddRequest 
    {
       
        
        public string customer_uuid { get; set; }
        
       
       
        public string path { get; set; }
        public Projecta project { get; set; }
        
        
        public ConTractAddRequest contract { get; set; }
        public string cost_string { get; set; }
        public int province_display { get; set; }

    }
    public class Projecta
    {
        public string teamUuid { get; set; }
        public bool isEnable { get; set; }
        public string uuid { get; set; }
        public string name { get; set; }
        public string name_display
        {
            get
            {
                if (!string.IsNullOrEmpty(name))
                {
                    var needsub = 70;
                    if (name.Length > needsub)
                    {
                        return name.Substring(0, needsub) + "...";
                    }
                    return name;
                }
                return string.Empty;
            }
        }
        public int? fieldId { get; set; }
        public int? provinceId { get; set; }
        public string content { get; set; }
        public string description { get; set; }
        public string code { get; set; }
    }
    public class ProjectEditRequest
    {
        public Projecttest project { get; set; }
        public ConTractAddRequest contract { get; set; }
    }
    public class Projecttest
    {
        public string uuid { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string code { get; set; }
        public Boolean isEnable { get; set; }
        public string content { get; set; }
        public int? fieldId { get; set; }
        public int? provinceId { get; set; }
        public string teamUuid { get; set; }
    }
    public class ProjectUpdateRequest
    {
        public string uuid { get; set; }
        public string name { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Mo ta khong duoc de trong")]
        [MinLength(6, ErrorMessage = "Nhập trên 6")]
        public string description { get; set; }
    }

    public class ProjectAddPackageRequest
    {
        public string packageUuid { get; set; }
        public string package_name { get; set; }
        public string uuid { get; set; }
        public string name { get; set; }
        public string projectUuid { get; set; }
        public int? packageCodeID { get; set; }
        public List<total> packageCategories { get; set; }
    }
    public class PackageImportProject
    {
        public string packageUuid { get; set; }
        public string package_name { get; set; }
        public string uuid { get; set; }
        public string name { get; set; }
        public string projectUuid { get; set; }
        public int? packageCodeID { get; set; }
        public List<total> packageCategories { get; set; }
        public IFormFile FileUploadExcel { get; set; }
        public int categoryId { get; set; }
        public int count { get; set; }
        public int timeMaintain { get; set; }
    }
    public class ProjectAddContract
    {
        public string uuid { get; set; }
    }
    public class total
    {
        public int categoryId { get; set; }
        public int count { get; set; }
        public int timeMaintain { get; set; }

    }
    public class ProjectGetCustomer : BaseRequest
    {
        public int? districtId { get; set; }
        public int? provinceId { get; set; }
        public int? wardId { get; set; }
        public string packageuuid { get; set; }
        public string projectuuid { get; set; }
        public int? contractState { get; set; }
    }
    public class ProjectDetailDeviceRequest : BaseRequest
    { 
        public string projectUuid { get; set; }
        public string packageUuid { get; set; }
        public string customerUuid { get; set; }
        public string categoryUuid { get; set; }
    }
    public class ProjectGetPackage : BaseRequest
    {
        public int? packageCodeId { get; set; }
        public string package_uuid { get; set; }
        public string projectUuid { get; set; }
        public string customerUuid { get; set; }
        public string contractUuid { get; set; }
        public string project_uuid { get; set; }
    }
    public class ProjectPackageModel
    {
        public string uuid { get; set; }
        public int? packageCodeID { get; set; }
        public string packageCode { get; set; }
        public string name { get; set; }
        public string projectUuid { get; set; }
        public List<PackageCategories> packageCategories { get; set; }
        public int id { get; set; }
        public string model { get; set; }
        public int count { get;set; }
        public string description { get; set; }
        public  int count_device { get; set; }
        public int countDevices { get; set; }
        public int? timeMaintain { get; set; }
        public string prefixName { get; set; }
        public string customerUuid { get; set; }


    }
    public class PackageCategories
    {
        public string uuid { get; set; }
        public string model { get; set; }
        public int count { get; set; }
        public string packageUuid { get; set; }
        public int deviceCategoryID { get; set; }
    }
    public class ProjectCustomerModel
    {
        public string customerUuid { get; set; }
        public string customerName { get; set; }
        public string customerAddress { get; set; }
        public string contractCode { get; set; }
        public string contractUuid { get; set; }
        public Provinces? province { get; set; }
        public Towns? town { get; set; }
        public Villages? village { get; set; }
        public List<Packages> packages { get; set; }
        public Guid customid { get { return Guid.NewGuid(); } }
        public DateTime? contract_Timehandover { get; set; }
        public string contract_Timehandover_format
        {
            get
            {
                if (contract_Timehandover.HasValue)
                {
                    return contract_Timehandover.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
        public class ProjectDetailDeviceModel : BaseModel
        {
            public string serial { get; set; }
            public string imei { get; set; }
            public Supplier supplier { get; set; }
            public DateTime? expireDevice { get; set; }
            public string expireDevice_format_nohh
            {
                get
                {
                    if (expireDevice.HasValue)
                    {
                        return expireDevice.Value.ToString("dd/MM/yyyy");
                    }
                    return string.Empty;
                }
            }
            public string packageName { get; set; }
            public State state { get; set; }
            public string categoryUuid { get; set; }
            public int? count_total_device { get; set; }
            public int? count_total_device_identified { get; set; }
            public int? count_total_device_unidentified { get; set; }
            public string customerUuid { get; set; }
            public string prefixName { get; set; }


        }
        public class Supplier
        {
            public int id { get; set; }
            public string uuid { get; set; }
            public DateTime timeCreated { get; set; }
            public int isEnable { get; set; }
            public string name { get; set; }
            public string description { get; set; }
            public List<string> device { get; set; }
        }
        public class State
        {
            public int id { get; set; }
            public string name { get; set; }
            public List<string> device { get; set; }
        }
        public DateTime? contract_TimeValidEnd { get; set; }
        public string contract_TimeValidEnd_string {
            get
            {
                if (contract_TimeValidEnd.HasValue)
                {
                    return contract_TimeValidEnd.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
        public int state { get; set; }
    }
    public class Provinces
    {
        public int? id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
    }
    public class Towns
    {
        public int? id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public int provinceId { get; set; }
    }
    public class Villages
    {
        public int? id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public int? townId { get; set; }
    }
    public class Packages
    {
        public string name { get; set; }
        public string uuid { get; set; }
        public int countPackageDeploys { get; set; }
        public int countDevice { get; set; }
    }
    public class Delete
    {
        public string project_uuid { get; set; }
    }
    public class DeletePackage
    {
        public string projectUuid { get; set; }
        public string packageUuid { get; set; }
    }
    public class ProjectAddCustomerRequest
    {
        public string uuid { get; set; }
        public string customerUuid { get; set; }
        public string customer_uuid { get; set; }
        public string projectUuid { get; set; }
        public string contractUuid { get; set; }
        public DateTime? timeHandOver { get; set; }
        public string timeHandOverstring { get; set; }
        public List<orderPackages> orderPackages { get; set; }
        public IFormFile uploadexcel { get; set; }
        public int state { get; set; }
    }
    public class ProjectEditCustomerRequest
    {
        public string projectUuid { get; set; }
        public string contractUuid { get; set; }
        public string customerUuid { get; set; }
        public DateTime? timeHandover { get; set; }
        public int state { get;set; }
        public List<orderPackages> orderPackages { get; set; }
        public string Date_display { get; set; }
        public string Customer_display { get; set; }
        public string Contract_display { get; set; }
    }
    public class orderPackages
    {
        public string packageUuid { get; set; }
        public int quantity { get; set; }
    }
    public class FieldPro
    {
        public int field_id { get; set; }
        public string field_name { get; set; }
    }

    #endregion

    #region Response

    public class ProjectDataResponse
    {
        public ProjectDataResponse()
        {

            items = new List<ProjectModel>();
            pagination = new PaginationResponse();
        }

        public List<ProjectModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string content { get; set; }
        public List<ImageModel> images { get; set; }
        public List<CustomerModel> customers { get; set; }
        public string uuid { get; set; }
        public string code { get; set; }
        public ProvinceCustomer province { get; set; }
        public TeamProject team { get; set; }
        public long? cost { get; set; }
        public string investor_name { get; set; }
        public DateTime? time_signed { get; set; }
        public int? time_valid_value { get; set; }
        public int? time_maintain_loop { get; set; }
        public int? time_maintain_value { get; set; }
        public FieldPro field { get; set; }
        public string contract_code { get; set; }
        public DateTime? time_valid_start { get; set; }
        public DateTime? time_valid_end { get; set; }
        public string cost_diplay
        {
            get
            {
                if (cost.HasValue)
                {
                    return string.Format(CultureInfo.GetCultureInfo("vi-VN"), "{0:C0}", cost);
                }
                return "";
            }
        }
        public CustomerDetail customer { get; set; }
        public string contract_uuid { get; set; }
        public string unit_signed_name { get; set; }
        public Unit_signed unit_signed { get; set; }
        public Investor investor { get; set; }
        public States state { get; set; }
        public int contract_isautoclose { get; set; }
    }
    public class States
    {
        public int state_id { get; set; }
        public string state_name { get; set; }
    }
    public class TeamProject
    {
        public string team_uuid { get; set; }
        public string team_name { get; set; }
    }
    public class CustomerDetail
    {
        public string customerUuid { get; set; }
    }
    public class ProjectGetCusDataResponse
    {
        public ProjectGetCusDataResponse()
        {

            items = new List<ProjectCustomerModel>();
            pagination = new PaginationResponse();
        }

        public List<ProjectCustomerModel> items { get; set; }
        public PaginationResponse pagination { get; set; }


    }
    public class ProjectDetailCustomerDataResponse
    {
        public ProjectDetailCustomerDataResponse()
        {
            items = new List<ProjectDetailDeviceModel>();
            pagination = new PaginationResponse();
        }
        public List<ProjectDetailDeviceModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
    #endregion
    public class ProjectGetPacDataResponse
    {
        public ProjectGetPacDataResponse()
        {

            items = new List<ProjectPackageModel>();
            pagination = new PaginationResponse();
        }

        public List<ProjectPackageModel> items { get; set; }
        public PaginationResponse pagination { get; set; }


    }
    public class Device1
    {
       
    }
    public class Supplier 
    { 
        public int id { get; set; }
        public string uuid { get; set; }
        public DateTime timeCreated { get; set; }
        public int isEnable { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public List<Device1> device { get; set; }

    }
    public class State
    {
        public int? id { get; set; }
        public string name { get; set; }
        public List<Device> deivce { get; set; }
    }
    public class ProjectDetaiDeviceModel : BaseModel
    {
        public string serial { get; set; }
        public string imei { get; set; }
        public Supplier supplier { get; set; }
        public DateTime? expireDevice { get; set; }
        public string time_expireDevice_format
        {
            get
            {
                if (expireDevice.HasValue)
                {
                    return expireDevice.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
        public string packageName { get; set; }
        public State state { get; set; }

    }
    public class ProjectDetailDeviceDataResponse 
    {
        public ProjectDetailDeviceDataResponse()
        {

            items = new List<ProjectDetaiDeviceModel>();
            pagination = new PaginationResponse();
        }

        public List<ProjectDetaiDeviceModel> items { get; set; }
        public PaginationResponse pagination { get; set; }


    }
    public class InvestorModel
    {
        public string uuid { get; set; }
        public string name { get; set; }
    }
    public class InvestorDataResponse
    {
        public InvestorDataResponse()
        {
            items = new List<InvestorModel>();
        }
        public List<InvestorModel> items { get; set; }
    }

}
