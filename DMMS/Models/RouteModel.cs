﻿using DMMS.Extensions;
using DMMS.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class ResourceModel 
    {
        public ResourceModel()
        {
            Childrents = new List<ResourceModel>();
        }
        public int id { get; set; }
        public string name { get; set; }
        public string parentName { get; set; }
        public string route { get; set; }
        public string method { get; set; }
        public bool isPage { get; set; }
        public string parentController { get; set; }
        public string actionName { get; set; }
        public bool isShow { get; set; }
        public string enumAction { get; set; }
        public string icon { get; set; }
        public List<ResourceModel> Childrents { get; set; }
    }

    public class RouteModel
    {
        public RouteModel()
        {
            Childrents = new List<RouteModel>();
        }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string HttpMethods { get; set; }
        public string ActionMethodName { get; set; }
        public string AttributeRouteTemplate { get; set; }
        public string DisplayName { get; set; }        
        public bool IsApp { get; set; }
        public bool IsShow { get; set; }
        
        public List<RouteModel> Childrents { get; set; }
        public bool IsPage
        {
            get
            {
                return string.IsNullOrEmpty(HttpMethods) ? true : false;
            }
        }
    }

    public class ControllerActionsModel
    {
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Attributes { get; set; }
        public string ReturnType { get; set; }
    }


}
