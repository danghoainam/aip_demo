﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class MerchantModel : BaseModel
    {        
        public ImageModel image { get; set; }
    }

    public class MerchantDataModel
    {
        public MerchantSearchRequest Request { get; set; }
        public List<MerchantModel> Merchants { get; set; }
    }

    #region Request
    public class MerchantSearchRequest : BaseRequest
    {
        
    }
    public class MerchantAddRequest
    {
        public string uuid { get; set; }
        public string name { get; set; }
        public ImageModel image { get; set; }
        public string description { get; set; }
    }

    #endregion

    #region Response
    public class MerchantDataResponse
    {
        public MerchantDataResponse()
        {
            items = new List<MerchantModel>();
        }
        public List<MerchantModel> items { get; set; }
    }
    #endregion
}
