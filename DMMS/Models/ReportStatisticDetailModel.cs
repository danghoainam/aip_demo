﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class ReportStatisticDetailModel : BaseModel
    {
        public string code { get; set; }
        public string project_name { get; set; }
        public int field_id { get; set; }
        public string field_name { get; set; }
        public int province_id { get; set; }
        public string province_name { get; set; }
        public int customer_count { get; set; }
        public int package_deloy_count { get; set; }
        public string code_contract { get; set; }
        public string unitsigned_uuid { get; set; }
        public string unitsigned_name { get; set; }
        public string customer_uuid { get; set; }
        public string customer_name { get; set; }
        public DateTime? time_start { get; set; }
        public DateTime? time_end { get; set; }
        public string time_valid { get; set; }
        public string package_code_id { get; set; }
        public string package_code_name { get; set; }
        public string package_name { get; set; }
        public string time_start_format
        {
            get
            {
                if (time_start.HasValue)
                {
                    return time_start.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }

        public string time_end_format
        {
            get
            {
                if (time_end.HasValue)
                {
                    return time_end.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
    }

    public class ReportStatisticDetailAreaModel : BaseModel
    {
        public int? province_id { get; set; }
        public string name { get; set; }
        public int? project_deploy { get; set; }
        public int? customer_deploy { get; set; }
        public int? package_deploy { get; set; }
        public int? device_deploy { get; set; }
        public long? cost { get; set; }
        public string cost_format
        {
            get
            {
                if (cost.HasValue)
                {
                    return string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:C0}", cost);
                }
                return "";
            }
        }
    }

    public class ReportStatisticDetailRequest : BaseRequest
    {
        public string timeFrom { get; set; }
        public string timeTo { get; set; }
        public int? viewDetail { get; set; }
        public int? fieldId { get; set; }
        public int? provinceId { get; set; }
        public string unitSignedUuid { get; set; }
        public string teamUuid { get; set; }
    }

    public class ReportStatisticDetailTab2Request : BaseRequest
    {
        public string timeFrom { get; set; }
        public string timeTo { get; set; }
        public int? viewDetail { get; set; }
        public int? fieldId { get; set; }
        public int? provinceId { get; set; }
        public string unitSignedUuid { get; set; }
        public string teamUuid { get; set; }
    }

    public class ReportStatisticDetailAreaRequest : BaseRequest
    {
        public string timeFrom { get; set; }
        public string timeTo { get; set; }
        public int? fieldId { get; set; }
        public int? provinceId { get; set; }
        public string unitSignedUuid { get; set; }
        public string teamUuid { get; set; }
    }

    public class ReportStatisticDetailResponse
    {
        public ReportStatisticDetailResponse()
        {
            items = new List<ReportStatisticDetailModel>();
            pagination = new PaginationResponse();
        }
        public List<ReportStatisticDetailModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }

    public class ReportStatisticDetailTab2Response
    {
        public ReportStatisticDetailTab2Response()
        {
            items = new List<ReportStatisticDetailModel>();
            pagination = new PaginationResponse();
        }
        public List<ReportStatisticDetailModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }

    public class ReportStatisticDetailAreaResponse
    {
        public ReportStatisticDetailAreaResponse()
        {
            items = new List<ReportStatisticDetailAreaModel>();
            pagination = new PaginationResponse();
        }
        public List<ReportStatisticDetailAreaModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
}
