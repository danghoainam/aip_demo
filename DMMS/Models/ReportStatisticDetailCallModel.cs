﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class ReportStatisticDetailCallModel: BaseModel
    {

    }

    public class ReportStatisticDetailCallRequest : BaseRequest
    {
        public string timeFrom { get; set; }
        public string timeTo { get; set; }
    }

    public class ReportStatisticDetailCallResponse
    {
        public ReportStatisticDetailCallResponse()
        {
            items = new List<ReportStatisticDetailCallModel>();
            pagination = new PaginationResponse();
        }
        public List<ReportStatisticDetailCallModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
}
