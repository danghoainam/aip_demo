﻿using DMMS.Objects;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class SupplierModel : BaseModel
    {
        //public List<IFormFile> FileToUpload { get; set; }
        //public List<ImageModel> images { get; set; }

        /// <summary>
        /// Id cua image
        /// </summary>
        /// 
        public string name { get; set; }
       
        public string test { get; set; }
        public ImageModel image { get; set; }
        public List<Total> totals { get; set; }
        
    }
    public class Total
    {
        public int number { get; set; }
        public string unit { get; set; }
        public string classify { get; set; }
        public string uuid { get; set; }
    }
    public class SupplierSearchRequest : BaseRequest
    {

    }
    public class SupplierDetailRequest
    {
        public string uuid { get; set; }
    }
    public class SupplierDeviceRequest : BaseRequest
    { 
        public string supplierUuid { get; set; }
        public string staffUuid { get; set; }
        public List<int> states { get; set; }
    }
    public class SupplierAddRequest
    {
        public string uuid { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        /// <summary>
        /// id cua anh
        /// </summary>
        public string image { get; set; }
    }
  
    public class SupplierDataResponse : SupplierModel
    {
        public SupplierDataResponse()
        {
            items = new List<SupplierModel>();
            pagination = new PaginationResponse();
            image = new ImageModel();
        }
        public List<SupplierModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
        //public List<ImageModel> images { get; set; }
        public ImageModel image { get; set; }

    }

}
