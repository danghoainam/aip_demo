﻿using DMMS.Objects;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{

    public class CustomerModel : BaseModel
    {
        public string contact_name { get; set; }
        public string contact_phone_number { get; set; }
        public string address { get; set; }
        public string coordinate { get; set; }
        public string email { get; set; }
        public LocationModel location { get; set; }
        public List<GroupModel> groups { get; set; }
        public Account account { get; set; }
        public string project_customer_uuid { get; set; }
        public ImageModel image { get; set; }
        public string name_display
        {
            get
            {
                var tmp_name = name + (location != null ? (" - " + location.name) : "");
                if (!string.IsNullOrEmpty(tmp_name))
                {
                    var needsub = 100;
                    if (tmp_name.Length > needsub)
                    {
                        return tmp_name.Substring(0, needsub) + "...";
                    }
                    return tmp_name;
                }
                return string.Empty;
            }
        }
        public StaffModel employee { get; set; }
        public Provinces Provinces { get; set; }
        public Towns Towns { get; set; }
        public Villages Villages { get; set; }
        public int location_assembly_id { get; set; }
        public Tech tech { get; set; }
    }
    public class Tech
    {
        public string uuid { get; set; }
        public string name { get; set; }
        public string phone_number { get; set; }
    }
    public class CustomerMySelft
    {
        public string keywork { get; set; }
    }

    public class CustomerRequest : GroupCustomerRequest
    {
        public int? province_id { get; set; }
        public int? town_id { get; set; }
        public int? village_id { get; set; }
        public string group_uuid { get; set; }
        public string uuid { get; set; }
        public string phone_number { get; set; }

    }
    public class CustomerProjectRequest : BaseRequest
    {
        public string customerUuid { get; set; }
    }
    public class CustomerFindRequest
    {
        public int limit { get; set; }
        public int province_id { get; set; }
    }

    public class CustomerDetailRequest : BaseRequest
    {

        public string uuid { get; set; }
    }
    public class CustomerAddRequest
    {
        public string uuid { get; set; }
        public string address { get; set; }
        public string contact_phone_number { get; set; }
        public string contact_name { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        //public string location_id { get; set; }
        public string coordinate { get; set; }
        public string description { get; set; }
        public Account account { get; set; }
        public string image { get; set; }
        public string avatar { get; set; }
        public List<ContactAddRequest> contacts { get; set; }
        public int? province_id { get; set; }
        public int? town_id { get; set; }
        public int? village_id { get; set; }
    }

    public class ContactAddRequest
    {
        public string name { get; set; }
        public string phone_number { get; set; }
        public string duty { get; set; }
    }
    public class LocationAssembly
    {
        public int? province_id { get; set; }
        public int? town_id { get; set; }
        public int? village_id { get; set; }
    }

    public class CustomerAddAccountRequest
    {
        public string username { get; set; }
        public string password { get; set; }
    }
    public class CustomerDataResponse : CustomerModel
    {
        public CustomerDataResponse()
        {

            //items = new List<CustomerModel>();
            //pagination = new PaginationResponse();            
            //location = new LocationModel();
        }
        public List<CustomerModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
        public string uuid { get; set; }
        public LocationModel location { get; set; }
        public int? location_id { get; set; }
        public string address { get; set; }
        public string contact_phone_number { get; set; }
        public string contact_name { get; set; }
        //public string name { get; set; }
        public string email { get; set; }
        //public string location_name { get; set; }
        public string coordinate { get; set; }
        public string description { get; set; }
        public ImageModel image { get; set; }
        public Account account { get; set; }
        public string avatar { get; set; }
        public string username { get; set; }

        public LocationAssembly location_assembly { get; set; }
    }
    public class CustomerProjectModel
    {
        public string uuid { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public Field field { get; set; }
        public Contract contract { get; set; }
        public State state { get; set; }
        public int? packageCount { get; set; }
    }
    public class Field
    {
        public int? fieldId { get; set; }
        public string fieldName { get; set; }
    }
    public class Contract
    {
        public int id { get; set; }
        public string code { get; set; }
        public long? cost { get; set; }
        public string costEstimate_format
        {
            get
            {
                if (cost.HasValue)
                {
                    return string.Format(CultureInfo.GetCultureInfo("vi-VN"), "{0:C0}", cost);
                }
                return "";
            }
        }
        public State state { get; set; }
    }
    public class CustomerProjectDataResponse
    {
        public  CustomerProjectDataResponse()
        {
            items = new List<CustomerProjectModel>();
            pagination = new PaginationResponse();
        }
        public List<CustomerProjectModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }

    #region model for area menu

    public class CustomerNotManagedModel
    {
        
        public int id { get; set; }
        public string name { get; set; }
        public List<CustomerModel> customers { get; set; }

    }

    public class CustomerNotManagedResponse
    {
        
        public List<CustomerNotManagedModel> items { get; set; }
    }


    public class CustomerManagedByTeamModel: CustomerModel
    {
        
        public TeamModel team { get; set; }
    }
    public class CustomerManagedByTeamModelteam
    {
        public string uuid { get; set; }
        public string owner_uuid { get; set; }
        public string name { get; set; }
        public string contact_name { get; set; }
        public string contact_phone_number { get; set; }
        public string address { get; set; }
        public string coordinate { get; set; }
        public string email { get; set; }
        public int? location_id { get; set; }
        public StaffModel team { get; set; }
    }
    public class CustomerManagedByTeamModelstaff
    {
        public StaffModel employee { get; set; }
        public List<CustomerModel> customers { get; set; }
    }
    public class CustomerManagedByTeamResponse
    {
        
        public List<CustomerManagedByTeamModel> items { get; set; }
        public List<CustomerManagedByTeamModelteam> teams { get; set; }
        public List<CustomerManagedByTeamModelstaff> staff { get; set; }

    }

    #endregion
}
