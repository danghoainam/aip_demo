﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class ListOfferModel : BaseModel
    {

    }
    public class ListOfferRequest : BaseRequest
    {

    }
    public class ListOfferAddRequest
    {

    }
    public class ListOfferDataResponse
    {
        public ListOfferDataResponse()
        {
            items = new List<ListOfferModel>();
            pagination = new PaginationResponse();
        }
        public List<ListOfferModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
}
