﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class OderListModel : BaseModel
    {

        public string code { get; set; }
        public orderperson order_person { get; set; }
        public task task { get; set; }
        public from from { get; set; }
        public to to { get; set; }
        public int status { get; set; }
        public List<categories> categories { get; set; }
        public string review_note { get; set; }
        public DateTime? time_review { get; set; }
        public reviewer reviewer { get; set; }
        public string time_review_format
        {
            get
            {
                if (time_review.HasValue)
                {
                    return time_review.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
    }
    public class OderListRequest : BaseRequest
    {
        public string storageUuid { get; set; }
        public int? status { get; set; }

    }
    public class OderListDetailRequest
    {
        public string order_uuid { get; set; }
    }
    public class orderperson
    {
        public string uuid { get; set; }
        public string name { get; set; }
    }
    public class task
    {
        public string uuid { get; set; }
        public string code { get; set; }
        public string name { get; set; }
    }
    public class from
    {
        public string uuid { get; set; }
        public string name { get; set; }
        public int type { get; set; }
    }
    public class to
    {
        public string uuid { get; set; }
        public string name { get; set; }
    }
    public class reviewer
    {
        public string uuid { get; set; }
        public string name { get; set; }
    }
    public class categories
    {
        public int id { get; set; }
        public string code { get; set; }
        public string prefixName { get; set; }
        public string name { get; set; }
        public int type { get; set; }
        public int offer_quantity { get; set; }
        public string unit { get; set; }
    }

    public class DecideOrderRequest
    {
        public string order_uuid { get; set; }
        public string reviewerUuid { get; set; }
        public string reviewNote { get; set; }
        public bool accept { get; set; } //True - duyệt, False - từ chối
    }

    public class OderListResponse
    {
        public OderListResponse()
        {
            items = new List<OderListModel>();
            pagination = new PaginationResponse();
        }
        public List<OderListModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
        public string uuid { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public orderperson order_person { get; set; }
        public DateTime? time_created { get; set; }
        public from from { get; set; }
        public to to { get; set; }
        public string content { get; set; }
        public task task { get; set; }
        public string review_note { get; set; }
        public DateTime? time_review { get; set; }
        public reviewer reviewer { get; set; }
        public List<categories> categories { get; set; }
        public int state { get; set; }
        public string time_created_format
        {
            get
            {
                if (time_created.HasValue)
                {
                    return time_created.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
    }
}
