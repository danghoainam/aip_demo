﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class ReportStatisticDetailSuppliesTaskModel: BaseModel
    {

    }

    public class ReportStatisticDetailSuppliesTaskRequest : BaseRequest
    {
        public string timeFrom { get; set; }
        public string timeTo { get; set; }
    }

    public class ReportStatisticDetailSuppliesTaskResponse
    {
        public ReportStatisticDetailSuppliesTaskResponse()
        {
            items = new List<ReportStatisticDetailSuppliesTaskModel>();
            pagination = new PaginationResponse();
        }
        public List<ReportStatisticDetailSuppliesTaskModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
}
