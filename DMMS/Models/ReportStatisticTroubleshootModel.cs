﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class ReportStatisticTroubleshootModel : BaseModel
    {
        public string ticket_uuid { get; set; }
        public string ticket_code { get; set; }
        public string ticket_title { get; set; }
        public string ticket_content { get; set; }
        public string customer_uuid { get; set; }
        public string customer_name { get; set; }
        public string contact_name { get; set; }
        public string contact_phone_number { get; set; }
        public string customer_province { get; set; }
        public int? priority { get; set; }
        public int? role { get; set; }
        public string creator_customer_name { get; set; }
        public string creator_staff_name { get; set; }
        public int? state { get; set; }
    }

    public class ReportStatisticTroubleshootRequest : BaseRequest
    {
        public string customerUuid { get; set; }
        public int? state { get; set; }
        public string timeFrom { get; set; }
        public string timeTo { get; set; }
    }

    public class ReportStatisticTroubleshootResponse
    {
        public ReportStatisticTroubleshootResponse()
        {
            items = new List<ReportStatisticTroubleshootModel>();
            pagination = new PaginationResponse();
        }
        public List<ReportStatisticTroubleshootModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
}
