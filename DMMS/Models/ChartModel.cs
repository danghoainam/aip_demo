using System;
using System.Collections.Generic;
namespace DMMS.Models
{
    public class ChartModel
    {
        public List<int> ids { get; set; }
        public int id {get;set;}
        public int count {get;set;}
        public string name {get;set;}
        public List<int> data { get; set; }
        public int level { get; set; }
        public List<info> info { get; set; }
    }
    public class info
    {
        public int state { get; set; }
        public int count { get; set; }
    }
    
    public class RequestChart
    {
        public long from { get; set; }
        public long to { get; set; }
    }
    public class BaseCharModel{
        public string name {get;set;}
        public int id { get; set; }
        public string key { get; set; }
    }

    public class ColumnChartModel : BaseCharModel{
        public int[] data {get;set;}
    }
    public class ChartModelcus : BaseCharModel
    {
        public List<int> data{ get; set; }
    }
    public class PieChartModel : BaseCharModel{
        public int y {get;set;}
    }    

    public class ChartDataResponse{
        public ChartDataResponse()
        {
            items = new itemsmodel();
        }
        public itemsmodel items { get; set; }
        public class itemsmodel
        {
        public List<ChartModel> tasks { get; set; }
        public List<ChartModel> contracts { get; set; }
        public List<ChartModel> offers { get; set; }
        public List<ChartModel> devices { get; set; }
        public List<ChartModel> tickets { get; set; }
        public List<ChartModel> daily_reports { get; set; }
        }
        
       
    }
}