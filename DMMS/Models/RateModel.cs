﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class RateModel
    {
    }

    public class RateRequest
    {
        public int ticket_id { get; set; }
        public int rate { get; set; }
        public string content { get; set; }
    }
}
