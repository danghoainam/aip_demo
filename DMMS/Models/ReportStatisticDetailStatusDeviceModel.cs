﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class ReportStatisticDetailStatusDeviceModel : BaseModel
    {

    }

    public class ReportStatisticDetailStatusDeviceRequest : BaseRequest
    {
        public string timeFrom { get; set; }
        public string timeTo { get; set; }
    }

    public class ReportStatisticDetailStatusDeviceResponse
    {
        public ReportStatisticDetailStatusDeviceResponse()
        {
            items = new List<ReportStatisticDetailStatusDeviceModel>();
            pagination = new PaginationResponse();
        }
        public List<ReportStatisticDetailStatusDeviceModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
}
