﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class ImportExcelPackageCategoriesModel
    {
        public string projectCode { get; set; }
        public string packageName { get; set; }
        public string packageCodeName { get; set; }
        public string categoriesName { get; set; }
        public string categoriesModel { get; set; }
        public string categoriesManufacturer { get; set; }
        public string categoriesOriginName { get; set; }
        public long categoriesExpPriceRef { get; set; }
        public long categoriesImpPriceRef { get; set; }
        public string categoriesUnitName { get; set; }
        public string categoriesDescription { get; set; }
        public int categoriesType { get; set; } = 1;
        public int categoriesCount { get; set; }
        public int categoriesTimeMaintainValue { get; set; }
    }

    public class PackageCategoriesModel
    {
        public string packageName { get; set; }
        public string packageCodeName { get; set; }
        public string categoriesName { get; set; }
        public string categoriesModel { get; set; }
        public string categoriesManufacturer { get; set; }
        public string categoriesOriginName { get; set; }
        public long categoriesExpPriceRef { get; set; }
        public long categoriesImpPriceRef { get; set; }
        public string categoriesUnitName { get; set; }
        public string categoriesDescription { get; set; }
        public int categoriesType { get; set; } = 1;
        public int categoriesCount { get; set; }
        public int categoriesTimeMaintainValue { get; set; }
    }

    public class ImportPackage2
    {
        public string packageName { get; set; }
        public string packageCodeName { get; set; }
    }

    public class ImportCategories
    {
        public string categoryName { get; set; }
        public string model { get; set; }
        public string manufacturer { get; set; }
        public string originName { get; set; }
        public long expPriceRef { get; set; }
        public long impPriceRef { get; set; }
        public string unitName { get; set; }
        public string description { get; set; }
        public int type { get; set; } = 1;
        public int count { get; set; }
        public int timeMaintainValue { get; set; }
    }

    public class ImportPackageCategories
    {
        public ImportPackage2 package { get; set; }
        public List<ImportCategories> categories { get; set; }
        public ImportPackageCategories()
        {
            package = new ImportPackage2();
            categories = new List<ImportCategories>();
        }
    }

    public class RequestPackageCategoriesAddData
    {
        public string projectCode { get; set; }
        public List<ImportPackageCategories> packageCategories { get; set; }
        public RequestPackageCategoriesAddData()
        {
            projectCode = "";
            packageCategories = new List<ImportPackageCategories>();
        }
    }
}
