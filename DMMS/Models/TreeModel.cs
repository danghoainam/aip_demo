﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class TreeModel 
    {
        public string parent_id { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string uuid { get; set; }
        public int type { get; set; }
        public string target_uuid { get; set; }
        public string phone_number { get; set; }
        public string name_format
        {
            get
            {
                if(phone_number != null)
                {
                    return name + '-' + phone_number;
                }
                else
                {
                    return name + '-' + "Chưa có SĐT";
                }
            }
        }
       
    }
    public class TreeRequest : BaseRequest
    {
        public int? id { get; set; }
    }
    public class TreeRequestWidthId : BaseRequest
    {
        public string id { get; set; }
    }
    public class TreeAddRequest
    {
        public int type { get; set; }
        public List<string> uuids { get; set; }
        public string node_uuid { get; set; }
        public string name { get; set; }
        public string uuid { get; set; }
    }
    public class DepartmentNotTreeRequest
    {
        public int type { get; set; }
    }
    public class DeleteNode
    {
        public string uuid { get; set; }
    }
    public class InitTree
    {
        public string id { get; set; }
        public string parent { get; set; }
        public string text { get; set; }
        public string icon { get; set; }
        public string uuid { get; set; }
        public int type { get; set; }
        public bool is_staff { get; set; }
        public bool is_leader { get; set; }
        public string target_uuid { get; set; }
        public int count_staff { get; set; }
        public int type_parent { get; set; }


    }
    public class TreeDataResponse
    {
        public TreeDataResponse()
        {
            items = new List<TreeModel>();
            pagination = new PaginationResponse();
        }
        public List<TreeModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
}
