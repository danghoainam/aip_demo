﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class LocationModel : BaseModel
    { 
        public List<LocationModel> children { get; set; }
        public int? parent_id { get; set; }
        public string type { get; set; } 
        public int provinceId { get; set; }
    }
    public class LocationRequest : BaseRequest
    {
        
    }
    public class LocationTownRequest
    {
        public int? province_id { get; set; }
        public int limit { get; set; }
        public int page { get; set; }
        public int? town_id { get; set; }
    }
  

    public class abc
    {
        public int? id { get; set; }
        public string text { get; set; }
        public int? parent { get; set; }
    }
   public class LocationDetailRequest
    {
        public int? id { get; set; }
    }
    public class LocationUpsertRequest
    {
        public int? id { get; set; }
        public int? parent_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }
    public class LocationDataResponse
    {
        public LocationDataResponse()
        {
            items = new List<LocationModel>();

        }
        public List<CategoryModel> children { get; set; }
        public List<LocationModel> items { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int? parent_id { get; set; }
    }
}
