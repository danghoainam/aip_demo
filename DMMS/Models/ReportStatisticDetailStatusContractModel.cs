﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class ReportStatisticDetailStatusContractModel : BaseModel
    {

    }

    public class ReportStatisticDetailStatusContractRequest : BaseRequest
    {
        public string timeFrom { get; set; }
        public string timeTo { get; set; }
    }

    public class ReportStatisticDetailStatusContractResponse
    {
        public ReportStatisticDetailStatusContractResponse()
        {
            items = new List<ReportStatisticDetailStatusContractModel>();
            pagination = new PaginationResponse();
        }
        public List<ReportStatisticDetailStatusContractModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
}
