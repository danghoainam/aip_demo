﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class SipAccountModel : BaseModel
    {
        public string owner_uuid { get; set; }
        public string exten { get; set; }
        public string password { get; set; }
        public string ami_user { get; set; }
        public string ami_password { get; set; }
        public AccountModel account { get; set; }
    }
    public  class SipAccountRequest : BaseRequest
    {
        public string owner_uuid { get; set; }
    }

    public class SipAccountDetailRequest
    {
        public string uuid { get; set; }
    }

    public class SipAccountAddRequest
    {
        public string uuid { get; set; }
        public string exten { get; set; }
        public string password { get; set; }
        public string ami_user { get; set; }
        public string ami_password { get; set; }
        public string uuidep { get; set; }
    }
    public class AmiUpdateRequest
    {
        public string ami_user { get; set; }
        public string ami_password { get; set; }
    }
    public class SipAccountDataResponse : SipAccountModel
    {
        public SipAccountDataResponse()
        {
            items = new List<SipAccountModel>();
            pagination = new PaginationResponse();
        }
        public List<SipAccountModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
        
    }
}
