﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class TreeCategoriesModel : BaseModel
    {
        public int? parentId { get; set; } //Theo api mới
        public int isLeaf { get; set; } //Theo api mới
        public string prefixName { get; set; } //Theo api mới
        public string unit { get; set; } //Theo api mới
        public int? parent_id { get; set; }
        public string quantity { get; set; }
        public int type { get; set; }
        public int is_leaf { get; set; }
        public string prefix_name { get; set; }
        public string prefix_name_format
        {
            get
            {
                var output = "";
                if (type == 1) output = string.Format("{0} (TB)", prefix_name);
                else if (type == 2) output = string.Format("{0} (VT)", prefix_name);
                else if (type == 3) output = string.Format("{0} (DCLĐ)", prefix_name);
                else if (type == 4) output = string.Format("{0} (PM)", prefix_name);

                return output;
            }
        }
        public List<int> fields { get; set; }
        public Detail detail { get; set; }

        public string imp_price_ref_dis
        {
            get
            {
                if (detail != null && detail.imp_price_ref != 0)
                {
                    return string.Format(CultureInfo.GetCultureInfo("vi-VN"), "{0:C0}", detail.imp_price_ref);
                }
                return "";
            }
        }
        public string exp_price_ref_dis
        {
            get
            {
                if (detail != null && detail.exp_price_ref != 0)
                {
                    return string.Format(CultureInfo.GetCultureInfo("vi-VN"), "{0:C0}", detail.exp_price_ref);
                }
                return "";
            }
        }
        public string imp_price_ref_format
        {
            get
            {
                if (detail != null && detail.imp_price_ref != 0)
                {
                    return string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:C0}", detail.imp_price_ref);
                }
                return "";
            }
        }
        public string name_display
        {
            get
            {
                if (detail != null)
                {
                    return !string.IsNullOrEmpty(detail.code) ? string.Format("{0} - {1} - {2}", detail.code, name, detail.manufacturer) :
                        !string.IsNullOrEmpty(detail.manufacturer) ? string.Format("{0} - {1}", name, detail.manufacturer) : name;
                }
                return name;
            }
        }
        public string exp_price_ref_format
        {
            get
            {
                if (detail != null && detail.exp_price_ref != 0)
                {
                    return string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"), "{0:C0}", detail.exp_price_ref);
                }
                return null;
            }
        }
        public string name_format { get; set; }

    }

    public class Detail
    {
        public int category_id { get; set; }
        public string code { get; set; }
        public string model { get; set; }
        public long imp_price_ref { get; set; }
        public long exp_price_ref { get; set; }
        public string imp_price_ref_string { get; set; }
        public string exp_price_ref_string { get; set; }
        public string manufacturer { get; set; }
        public string unit { get; set; }
        public int status { get; set; }
        public string origin { get; set; }
    }

    public class TreeCategoriesRequest : BaseRequest
    {
        public int? parent_id { get; set; }
        public int category_type { get; set; }
        public List<int> fields { get; set; }
        public int level { get; set; }
    }

    public class TreeCategoriesSearchRequest : BaseRequest //Theo api mới
    {
        public string categoryOrderUuid { get; set; }
    }

    public class InitTreeCategories
    {
        public int? id { get; set; }
        public string text { get; set; }
        public string icon { get; set; }
        public string uuid { get; set; }
        public int? type { get; set; }
        public int? parent_id { get; set; }
        public string children { get; set; }
        public string prefix_name { get; set; }
        public int is_leaf { get; set; }
        public int page { get; set; }
        public int pages { get; set; }

    }
    public class InitTreeCategoriesSearch
    {
        public int? id { get; set; }
        public string text { get; set; }
        public string icon { get; set; }
        public string uuid { get; set; }
        public int? type { get; set; }
        public int? parent { get; set; }
        public string prefix_name { get; set; }
        public int is_leaf { get; set; }
        public int page { get; set; }
        public int pages { get; set; }
        public int? parent_id { get; set; }
        public int is_search { get; set; }
    }

    public class TreeCategoriesDelete
    {
        public string category_uuid { get; set; }
        public int category_id { get; set; }
    }
    public class TreeCategoriesAddRequest
    {
        public string name { get; set; }
        public string description { get; set; }
        public int? parent_id { get; set; }
        public int? is_leaf { get; set; }
        public int? type { get; set; }
        public string model { get; set; }
        public string manufacturer { get; set; }
        public List<int> fields { get; set; }

        public int is_enable { get; set; }
        public Detail detail { get; set; }
    }
    public class TreeCategoriesEditRequest
    {
        public string name { get; set; }
        public string description { get; set; }
        public List<int> fields { get; set; }
        public Detail detail { get; set; }
        public int? id { get; set; }
    }
    public class CategoreSearchLeaf : BaseRequest
    {
        public List<int> fields { get; set; }
        public int category_type { get; set; }
    }
    public class CategoriesCheckName
    {
        public string name { get; set; }
    }
    public class CategoryDetail
    {
        public string uuid { get; set; }
        public int category_id { get; set; }
        public string code { get; set; }
        public string unit { get; set; }
        public int imp_price_ref { get; set; }
        public int exp_price_ref { get; set; }
        public int status { get; set; }
    }
    public class TreeCategoriesDetail
    {
        public int category_id { get; set; }
        public string category_uuid { get; set; }
        public string storage_uuid { get; set; }
    }
    public class TreeCategoriesDetailRespose
    {
        public string name { get; set; }
        public string description { get; set; }
        public int? parent_id { get; set; }
        public int is_leaf { get; set; }
        public int? type { get; set; }
        public string model { get; set; }
        public string manufacturer { get; set; }
        public List<int> fields { get; set; }
        public string isCheck { get; set; }
        public int is_enable { get; set; }
        public Detail detail { get; set; }
        public int? id { get; set; }
        public List<int> field_of { get; set; }
    }
    public class TreeCategoriesResponse : TreeCategoriesModel
    {
        public TreeCategoriesResponse()
        {
            items = new List<TreeCategoriesModel>();
            pagination = new PaginationResponse();
        }
        public List<TreeCategoriesModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
    public class TreeCategoriesResponseRequest
    {
        public TreeCategoriesResponseRequest()
        {
            items = new List<TreeCategoriesModel>();
            pagination = new PaginationResponse();
        }
        public List<TreeCategoriesModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
}
