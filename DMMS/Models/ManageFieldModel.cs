﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class ManageFieldModel : BaseModel
    {
        public string IsCheck { get; set; }
        public string code { get; set; }
        public string prefix_name { get; set; }
        public int type { get; set; }
    }
    public class ManageFieldRequest : BaseRequest
    {

    }
    public class Categories
    {
        public int id { get; set; }
        public string name { get; set; }
        public int? parent_id { get; set; }
        public string prefix_name { get; set; }
        public int type { get; set; }
        public string uuid { get; set; }
        public int is_leaf { get; set; }
    }
    public class MangeFiledAddRequest
    {
        public string name { get; set; }
        public string description { get; set; }
        public List<int> categoryId { get; set; }

    }
    public class MangeFiledUpdateRequest
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int is_enable { get; set; }
    }
    public class MangeFiledDelete
    {
        public int field_id { get; set; }
    }
    public class FiledGetCategories : BaseRequest
    {
        public int id { get; set; }
        public int? category_id { get; set; }
        public int? type { get; set; }
    }
    public class ManageFiledDataResponse
    {
        public ManageFiledDataResponse()
        {
            items = new List<ManageFieldModel>();
            pagination = new PaginationResponse();
        }
        public List<ManageFieldModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
        public int id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public List<Categories> categories { get; set; }
    }
}
