﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class Notification
    {
        public string uuid { get; set; }
        public string content { get; set; }
        public int status { get; set; }
        public DateTime time_created { get; set; }
        public string data { get; set; }
        public int type { get; set; }
    }
    public class NotificationRequest : BaseRequest
    {
        public string uuid { get; set; }
    }
    public class NotificationDataResponse
    {
        public NotificationDataResponse()
        {
            items = new List<Notification>();
            pagination = new PaginationResponse();
        }
        public List<Notification> items { get; set; }
        public PaginationResponse pagination { get; set; }
        public int notification_count { get; set; }
    }
}
