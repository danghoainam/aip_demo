﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class KnowledgeModel :BaseModel
    {
        public string question { get; set; }
        public string answer { get; set; }
        public string topic_uuid { get; set; }
        public Knowledgetopic knowledge_base_topic { get; set; }
    

    }
    public class Knowledgetopic
    {
        public string uuid { get; set; }
        public string name { get; set; }
    }
    public class QaUpsert
    {
        public string question { get; set; }
        public string answer { get; set; }
        public string topic_uuid { get; set; }
        public string uuid { get; set; }
    }
    public class KnowledgeRequest : BaseRequest
    {
        public string name { get; set; }
        public string uuid { get; set; }
    }
    public class KnowledgeDataResponse
    {
        public KnowledgeDataResponse()
        {
            items = new List<KnowledgeModel>();
            pagination = new PaginationResponse();
        }
        public List<KnowledgeModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
        public string uuid { get; set; }
        public string question { get; set; }
        public string answer { get; set; }
        public Knowledgetopic knowledge_base_topic { get; set; }
        public string name { get; set; }
    }
}
