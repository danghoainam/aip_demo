﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class TicketModel : BaseModel
    {
        public string code { get; set; }
        public StaffModel creator { get; set; }
        public CustomerModel customer { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public int? state { get; set; }
        public string contact_name { get; set; }
        public string contact_phone_number { get; set; }
        public string close_note { get; set; }
        public List<Device> devices { get; set; }
        public DateTime? time_last_update { get; set; }
        public int? priority { get; set; }
        public TaskModel task { get; set; }

        public string time_last_update_format
        {
            get
            {
                if (time_last_update.HasValue)
                {
                    return time_last_update.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
        public int? rating { get; set; }
    }
    public class Ticket_Devices
    {
        public int ticket_id { get; set; }
        public string serial { get; set; }
        public string imei { get; set; }
        public string name { get; set; }
        public string prefix_name { get; set; }
        public string project_name { get; set; }
        public string package_name { get; set; }
        public DateTime? time_valid_end { get; set; }
        public string time_valid_end_format
        {
            get
            {
                if (time_valid_end.HasValue)
                {
                    return time_valid_end.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
    }

    #region Request
    public class TicketRequest : BaseRequest
    {
        public string customer_uuid { get; set; }
        public string creator_uuid { get; set; }
        public int? state { get; set; }
        public string owner_uuid { get; set; }
        public int? priority { get; set; }
    }

    public class TicketDetailRequest
    {
        public string uuid { get; set; }
    }
    public class TicketCancelRequest
    {
        public string ticket_uuid { get; set; }
        public string reason { get; set; }
    }
    public class TicketUpsertRequest
    {
        public string uuid { get; set; }
        public string customer_uuid { get; set; }
        public string content { get; set; }
        public string title { get; set; }
        public string contact_name { get; set; }
        public string contact_phone_number { get; set; }
        //public List<string> imagestring { get; set; }
        public string image_uuids { get; set; }
        public int? state { get; set; }
        public int? state_id { get; set; }
        public string device_uuid_str { get; set; }
        public int? priority { get; set; }
        public int? rating { get; set; }
        public string feedback { get; set; }
        public List<Device> devices { get; set; }
        public string contact_uuid { get; set; }
        public string customer_name { get; set; }
        public List<Ticket_Devices> ticket_devices { get; set; }
        public TaskModel task { get; set; }
    }

    public class Device
    {
        public string device_id { get; set; }
        public string image_uuids { get; set; }
        public List<int> images 
        { 
            get
            {
                if (!string.IsNullOrEmpty(image_uuids))
                {
                    return image_uuids.Split(",".ToCharArray()).ToList().Select(s => int.Parse(s)).ToList();
                }
                return new List<int>();
            }
        }
    }

    public class TicketRateRequest
    {
        public string uuid { get; set; }
        public int? rating { get; set; }
        public string feedback { get; set; }
    }

    public class TicketUpsertModel : TicketUpsertRequest
    {
        public List<string> fullImages { get; set; }
    }

    #endregion


    #region Response
    public class TicketDataResponse
    {
        public TicketDataResponse()
        {
            items = new List<TicketModel>();
            pagination = new PaginationResponse();
            //customer = new CustomerModel();
            //creator = new StaffModel();
        }
        public List<TicketModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
        public CustomerModel customer { get; set; }
        public StaffModel creator { get; set; }
        public TaskModel task { get; set; }
        public int creator_id { get; set; }
        public List<Device> devices { get; set; }
        public List<ImageModel> images { get; set; }
        public string uuid { get; set; }
        public string code { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public int? state { get; set; }
        public string contact_name { get; set; }
        public string contact_phone_number { get; set; }
        public string feedback { get; set; }
        public DateTime time_created { get; set; }
        public DateTime time_last_update { get; set; }
        public string close_note { get; set; }
        public string state_update_at { get; set; }
        public int? priority { get; set; }
        public int? rating { get; set; }
        public List<Ticket_Devices> ticket_devices { get; set; }
        public string contact_uuid { get; set; }
        public string reason { get; set; }
    }
    #endregion
}
