﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class TeamModel : BaseModel
    {
        public string code { get; set; }
        public StaffModel leader { get; set; }
        public List<StaffModel> staff { get; set; }
    }

    #region Request
    public class TeamRequest : BaseRequest
    {
        public string uuid { get; set; }
    }

    public class TeamDetailRequest
    {
        public string uuid { get; set; }
    }

    public class TeamUpsertRequest
    {
        public TeamUpsertRequest()
        {
            staff = new List<string>();
        }
        public string uuid { get; set; }
        public string name { get; set; }        
        public string description { get; set; }
        public string leader_uuid { get; set; }
        public List<string> staff { get; set; }
        public string staff_uuid_str { get; set; }
        /// <summary>
        /// staff
        /// </summary>
        public string module { get; set; }
        public string uuids { get; set; }
    } 

 
    #endregion

    #region response
    public class TeamDataResponse
    {
        public TeamDataResponse()
        {
            //items = new List<TeamModel>();
            //pagination = new PaginationResponse();
            //leader = new StaffModel();
            //staff = new List<StaffModel>();
        }

        public string uuid { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string code { get; set; }
        public List<TeamModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
        public StaffModel leader { get; set; }
        public List<StaffModel> staff { get; set; }
    }
    #endregion
}
