﻿using DMMS.Objects;
using Microsoft.AspNetCore.Http.Features;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class ListSuppliesModel : BaseModel
    {
        public int type { get; set; }
    }
    public class ListSuppliesRequest : BaseRequest
    {

    }
    public class ListSuppliesAddRequest
    {
        public string name { get; set; }
        public int? id { get; set; }
        public string prefix_name { get; set; }
        public string money { get; set; }
        public int type { get; set; }
        public int status { get; set; }
        public long? imp_price_ref { get; set; }
        public long? exp_price_ref { get; set; }
        public string imp_price_ref_format { get; set; }
        public string exp_price_ref_format { get; set; }
        public string unit { get; set; }
        public string manufacturer { get; set; }
        public string origin { get; set; }


    }
    public class ListSuppliesDataRespone
    {
        public ListSuppliesDataRespone()
        {
            items = new List<ListSuppliesModel>();
            pagination = new PaginationResponse();
        }
        public List<ListSuppliesModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
}
