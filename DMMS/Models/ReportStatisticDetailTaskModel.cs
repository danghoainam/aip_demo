﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class ReportStatisticDetailTaskModel : BaseModel
    {
        public string code { get; set; }
        public string name { get; set; }
        public string note { get; set; }
        public int? priority { get; set; }
        public string customer_uuid { get; set; }
        public string customer_name { get; set; }
        public string staff_uuid { get; set; }
        public string staff_name { get; set; }
        public string team_uuid { get; set; }
        public string team_name { get; set; }
        public string state { get; set; }
        public string project_uuid { get; set; }
        public string project_code { get; set; }
        public string project_name { get; set; }
        public string field_name { get; set; }
        public string complete_value { get; set; }
        public string count { get; set; }
    }

    public class ReportStatisticDetailTaskTab3Model : BaseModel
    {

    }

    public class ReportStatisticDetailTaskRequest : BaseRequest
    {
        public int type { get; set; }
        public string timeFrom { get; set; }
        public string timeTo { get; set; }
        public string staff_uuid { get; set; }
        public string customerUuid { get; set; }
        public string teamUuid { get; set; }
        public int? stateId { get; set; }
    }

    public class ReportStatisticDetailTaskTab2Request : BaseRequest
    {
        public int type { get; set; }
        public string timeFrom { get; set; }
        public string timeTo { get; set; }
        public string staff_uuid { get; set; }
        public string customerUuid { get; set; }
        public string teamUuid { get; set; }
        public int? stateId { get; set; }
    }

    public class ReportStatisticDetailTaskTab3Request : BaseRequest
    {
        public int type { get; set; }
        public string timeFrom { get; set; }
        public string timeTo { get; set; }
        public string staff_uuid { get; set; }
        public string customerUuid { get; set; }
        public string teamUuid { get; set; }
        public int? stateId { get; set; }
    }

    public class ReportStatisticDetailTaskResponse
    {
        public ReportStatisticDetailTaskResponse()
        {
            items = new List<ReportStatisticDetailTaskModel>();
            pagination = new PaginationResponse();
        }
        public List<ReportStatisticDetailTaskModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }

    public class ReportStatisticDetailTaskTab3Response
    {
        public ReportStatisticDetailTaskTab3Response()
        {
            items = new List<ReportStatisticDetailTaskTab3Model>();
            pagination = new PaginationResponse();
        }
        public List<ReportStatisticDetailTaskTab3Model> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
}
