﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class ContractModel : BaseModel
    {
        public string customerName { get; set; }
        public string code { get; set; }
        public CustomerModel customer { get; set; }
        public string projectName { get; set; }
        public string projectUuid { get; set; }
        public int? timeValidValue { get; set; }
        public int? consumerCount { get; set; } //Số lượng khách hàng
        public string investorName { get; set; } //Tên nhà đầu tư
        public DateTime? timeValidStart { get; set; } //Ngày ký hợp đồng
        public DateTime? timeValidEnd { get; set; } //Ngày hết hạn hợp đồng
        public DateTime? timeSigned { get; set; } //Ngày hết hạn hợp đồng
        public int? totalMonthsGuarantee { get; set; } //Tổng số tháng bảo hành
        public long? cost { get; set; } //Giá trị hợp đồng
        public int? timeMaintainLoop { get; set; } //Thời gian bảo dưỡng định kỳ
        public int? timeMaintainValue { get; set; }
        public string node { get; set; }
        public int? countCustomer { get; set; }
        public string note { get; set; }
        public int? comparedate { get; set; }
        public DateTime? timeClosed { get; set; }
        public State state { get; set; } 
        public string projectCode { get; set; }
        public string contractCode { get; set; }
        public string timeValidStart_format 
        {
            get
            {
                if (timeValidStart.HasValue)
                {
                    return timeValidStart.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
        public string timeValidEnd_format
        {
            get
            {
                if (timeValidEnd.HasValue)
                {
                    return timeValidEnd.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
        public string timeSigned_format
        {
            get
            {
                if (timeSigned.HasValue)
                {
                    return timeSigned.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
        public string code_format
        {
            get
            {
                if (cost.HasValue)
                {
                    return string.Format(System.Globalization.CultureInfo.GetCultureInfo("vi-VN"),"{0:C0}", cost);
                }
                return "";
            }
        }
        public string customerUuid { get; set; }
        public int stateId { get; set; }
    }
    
    public class ContractCustomerModel
    {
        public string uuid { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public Province province { get; set; }
        public Town town { get; set; }
        public Village village { get; set; }
        public int? packageDeployCount { get; set; }
        public int? countDevice { get; set; }
        public string customerUuid { get; set; }
        public string customerName { get; set; }
        public string customerAddress { get; set; }
        public DateTime? contract_Timehandover { get; set; }
        public DateTime? contract_TimeValidEnd { get; set; }
        public int? state { get; set; }
        public string contractCode { get; set; }
        public string contractUuid { get; set; }
    }

    public class ContractSearchRequest : BaseRequest
    {
        public string projectUuid { get; set; }
        public string contractUuid { get; set; }
        public string project_uuid { get; set; }
        public int? contractStatus { get; set; }
        public string investor { get; set; }
        public int? state { get; set; }
    }
    public class LockContractRequest
    {
        public string subcontract_uuid { get; set; }
    }
    public class ConTractAddRequest
    {
        public string ProjectUuid { get; set; }
        public string uuid { get; set; }
        public string code { get; set; }
        public ProjectModel project { get; set; }
        public CustomerModel customer { get; set; }
        public string note { get; set; }
        public long? cost { get; set; }
        public string coststring { get; set; }
        public DateTime? timeSigned { get; set; } // thoi gian ki hop dong
        public DateTime? timeValidStart { get; set; } // thoi gian bat dau bh
        //public DateTime? timeValidEnd { get; set; } // thoi gian ket thuc bh
        public int? timeMaintainLoop { get; set; }
        public int? timeValidValue { get; set; }
        public int? timeMaintainValue { get; set; }
        public string investorUuid { get; set; }
        public string timeSignedstring { get; set; }
        public string timeValidStartstring { get; set; }
        public string timeValidEndstring { get; set; }
        public string customerUuid { get; set; }
        public string contractUuid { get; set; }
        public string InvestorName { get; set; }
        public string UnitSignedName { get; set; }
        public string subcontract_uuid { get; set; }
        public bool isAutoClose { get; set; }
        public string cost_diplay
        {
            get
            {
                if (cost.HasValue)
                {
                    return string.Format(CultureInfo.GetCultureInfo("vi-VN"), "{0:C0}", cost);
                }
                return "";
            }
        }
        public string unitSignedUuid { get;set; }
    }
    public class ContractEditRequest
    {
        public string uuid { get; set; }
        public int? timeValidValue { get; set; }
        public DateTime timeSigned { get; set; }
        public DateTime timeValidStart { get; set; }
        public DateTime timeValidEnd { get; set; }
        public int? timeMaintainLoop { get; set; }
        public string investorName { get; set; }
        public string code { get; set; }

    }
    public class SubContractDetailRequest
    {
        public string subcontract_uuid { get; set; }
    }

    public class ContractCustomerRequest : BaseRequest
    {
        public string contract_uuid { get; set; }
        public int? districtId { get; set; }
        public int? provinceId { get; set; }
        public int? wardId { get; set; }
    }

    public class Province
    {
        public int? id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string province_name { get; set; }
        public int? province_id { get; set; }
    }

    public class Town
    {
        public int? id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public int? provinceId { get; set; }
        public string town_name { get; set; }
        public int? town_id { get; set; }
    }

    public class Village
    {
        public int? id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public int? townId { get; set; }
        public string village_name { get; set; }
        public int? village_id { get; set; }
    }

    public class ContractDetailRequest
    {
        public string contract_uuid { get; set; }

    }

    public class ContractDataResponse : ContractModel
    {
        public ContractDataResponse()
        {

            items = new List<ContractModel>();
            pagination = new PaginationResponse();
        }
        public List<ContractModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
      
        public int? totalMonths { get; set; } //Tổng số tháng bảo hành
        
    }

    public class ContractCustomerDataResponse : ContractCustomerModel
    {
        public ContractCustomerDataResponse()
        {

            items = new List<ContractCustomerModel>();
            pagination = new PaginationResponse();
        }
        public List<ContractCustomerModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
}
