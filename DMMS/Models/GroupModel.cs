﻿using DMMS.Objects;
using System.Collections.Generic;


namespace DMMS.Models
{
    public class GroupModel : BaseModel
    {
        /// <summary>
        /// Dung cho customer/staff
        /// </summary>
        public string phone_number { get; set; }
        public List<GroupModel> groups { get; set; }
        public List<CustomerModel> customer { get; set; }
        public StaffModel saff { get; set; }
    }
    public class GroupRequest : PaginationRequest
    {
        public string keyword { get; set; }

        
    }
    public class GroupCustomerRequest : BaseRequest
    {
        public GroupCustomerRequest()
        {
            get_all = true;
        }
        /// <summary>
        /// Nếu truyền true thì lấy toàn bộ Đơn vị
        /// Nếu truyền false hoặc bỏ trống chỉ lấy Đơn vị không trực thuộc group nào
        /// </summary>
        public bool get_all { get; set; }

        /// <summary>
        /// Truyền thêm trong trường hợp get_all = true, không hiển thị những Đơn vị trực thuộc những group này
        /// uuid: id group không muốn hiển thị
        /// </summary>
        public List<string> group_inorge { get; set; }

    }
    public class GroupEmployeesRequest : BaseRequest
    {
        public GroupEmployeesRequest()
        {
            get_all = true;
        }
        /// <summary>
        /// Nếu truyền true thì lấy toàn bộ Đơn vị
        /// Nếu truyền false hoặc bỏ trống chỉ lấy Đơn vị không trực thuộc group nào
        /// </summary>
        public bool get_all { get; set; }

        /// <summary>
        /// Truyền thêm trong trường hợp get_all = true, không hiển thị những Đơn vị trực thuộc những group này
        /// uuid: id group không muốn hiển thị
        /// </summary>
        public List<string> group_inorge { get; set; }

    }
    public class GroupUpdatetRequest
    {
        public string uuid { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public List<string> staff { get; set; }
        public List<string> customer { get; set; }
        /// <summary>
        /// customer or staff
        /// </summary>
        public string module { get; set; }
        public string uuids { get; set; }

    }
    
    public class GroupAddRequest
    {
        public string name { get; set; }
        public string description { get; set; }

    }
    public class GroupDetailRequest : PaginationRequest
    {
        public string uuid { get; set; }
    }
    public class GroupDataResponse
    {
        public GroupDataResponse()
        {
            items = new List<GroupModel>();
            pagination = new PaginationResponse();
            customers = new List<CustomerModel>();
            groups = new List<GroupModel>();
            

        }
        public List<GroupModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
        public string uuid { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public List<CustomerModel> customers { get; set; }
        public List<GroupModel> groups { get; set; }
        
        // tam dong doi api chuan
        //public StaffModel staff { get; set; }
        public StaffModel staff { get; set; }
    }

    public class GroupCustomerStaffInforResponse
    {
        public string uuid { get; set; }
        public string name { get; set; }
        public string phonenumber { get; set; }
        public string display
        {
            get
            {
                if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(phonenumber))
                {
                    return string.Format("{0} - {1}", name, phonenumber);
                }
                else if(string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(phonenumber))
                {
                    return phonenumber;
                }
                else
                {
                    return name;
                }
            }
        }
    }

    public class StaffInGroupModel
    {
        public StaffInGroupModel()
        {
            group = new GroupModel();
            staffNotInGroup = new List<GroupCustomerStaffInforResponse>();
        }
        public StaffModel staff { get; set; }
        public List<GroupCustomerStaffInforResponse> staffNotInGroup { get; set; }
        public string group_uuid { get; set; }
        public GroupModel group { get; set; }
    }
}
