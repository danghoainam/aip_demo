﻿using DMMS.Objects;
using DMMS.Utilities;
using RestSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    //Kho
    public class BunkerModel : BaseModel
    {
        public string code { get; set; }
        public string address { get; set; }
        public LocationNewModel province { get; set; } //Tỉnh/ Thành phố
        public LocationNewModel town { get; set; } //Huyện/ Quận
        public LocationNewModel village { get; set; } //Xã
        public int? state { get; set; }
        public UnitSigned unit_signed { get; set; }
    }

    //Vật tư - thiết bị trong kho
    public class SuppliesDeviceInBunkerModel : BaseModel
    {
        public int category_id { get; set; }
        public string code { get; set; }
        public string prefix_name { get; set; } //Tên thư mục cha
        public string prefix_name_format
        {
            get
            {
                var output = "";
                if (type == 1) output = string.Format("{0} (TB)", prefix_name);
                else if (type == 2) output = string.Format("{0} (VT)", prefix_name);
                else if (type == 3) output = string.Format("{0} (DCLĐ)", prefix_name);
                else if (type == 4) output = string.Format("{0} (PM)", prefix_name);

                return output;
            }
        }
        public int? type { get; set; }
        public string type_format
        {
            get
            {
                var listType = ExtensionMethods.ToSelectList<Utilities.ListSupplies>().ToList();
                if (type.HasValue)
                {
                    return listType[type.Value - 1].Text;
                }
                return "";
            }
        }
        public int? quantity_exist { get; set; } //số lượng tồn kho
        public int? real_quantity { get; set; } //Số lượng thực tồn
        public int? deviations_quantity
        {
            get
            {
                if (quantity_exist.HasValue && real_quantity.HasValue)
                {
                    return quantity_exist - real_quantity;
                }
                return 0;
            }
        } //Sai lệch
        public int? broken_quantity { get; set; } //Số lượng hỏng
        public int? document_quantity { get; set; } //số lượng trên chứng từ
        public long? unit_price { get; set; } //Đơn giá
        public string unit { get; set; } //Đơn vị
        public DetailInSupplies detail { get; set; }
        public string name_display
        {
            get
            {
                if (detail != null)
                {
                    return !string.IsNullOrEmpty(code) ? (!string.IsNullOrEmpty(detail.the_firm) ? string.Format("{0} - {1} - {2}", code, name, detail.the_firm) : string.Format("{0} - {1}", code, name)) : name;
                }
                return !string.IsNullOrEmpty(code) ? string.Format("{0} - {1}", code, name) : name;
            }
        }
        public string serial { get; set; }
        public DateTime? expire_device { get; set; }
        public string expire_device_string
        {
            get
            {
                if (expire_device.HasValue)
                {
                    return expire_device.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
        //public string imei { get; set; }
        public int? state { get; set; }
        public int? status { get; set; }
        public int? state_in_inventory { get; set; }

        //Biến tạm
        public List<DetailDeviceInInventoryModel> detail_temp { get; set; }
    }

    public class DetailDeviceInInventoryModel
    {
        public string uuid { get; set; }
        public int category_id { get; set; }
        public string code { get; set; }
        public string prefix_name { get; set; }
        public string name { get; set; }
        public int? type { get; set; }
        public string serial { get; set; }
        public DateTime? expire_device { get; set; }
        public string expire_device_string
        {
            get
            {
                if (expire_device.HasValue)
                {
                    return expire_device.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
        public string name_display
        {
            get
            {
                if (code != null)
                {
                    return string.Format("{0} - {1}", code, name);
                }
                return name;
            }
        }
        public int? state_in_inventory { get; set; }
    }

    //Đề xuất
    public class OfferInBunkerModel
    {
        public string store_uuid { get; set; }
        public string uuid { get; set; }
        public string code { get; set; }
        public StaffInBunkerModel order_person { get; set; }
        public StaffInBunkerModel reviewer { get; set; }
        public TaskModel task { get; set; }
        public BunkerModel from { get; set; }
        public BunkerModel to { get; set; }
        public int? state { get; set; }
        public string review_uuid { get; set; }
        public int? status { get; set; }
        public DateTime? time_created { get; set; }
        public string time_created_format
        {
            get
            {
                if (time_created.HasValue)
                {
                    return time_created.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
        public List<CategoriesInDetailOffer> categories { get; set; }
        public string content { get; set; }
    }

    public class CategoriesInDetailOffer
    {
        public int? id { get; set; }
        public string code { get; set; }
        public string prefixName { get; set; }
        public string name { get; set; }
        public int? type { get; set; }
        public int? offer_quantity { get; set; }
        public string unit { get; set; }
    }

    //Lịch sử kho
    public class HistoryOfBunkerModel
    {
        public string uuid { get; set; }
        public string code { get; set; }
        public StaffInBunkerModel create { get; set; }
        public string note { get; set; }
        public int? type_of_ballot { get; set; } //Loại phiếu nhập xuất
        public int? type { get; set; }
        public DateTime? time_created { get; set; }
        public string time_created_format
        {
            get
            {
                if (time_created.HasValue)
                {
                    return time_created.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
    }

    //Kiểm kê
    public class InventoryModel
    {
        public string uuid { get; set; }
        public string code { get; set; }
        public StaffInBunkerModel storage_keeper { get; set; }
        public List<HumanJoinInventory> staff { get; set; }
        public string description { get; set; }
        public DateTime? time_stock_take { get; set; }
        public string time_stock_take_format
        {
            get
            {
                if (time_stock_take.HasValue)
                {
                    return time_stock_take.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
    }

    public class StaffInBunkerModel
    {
        public string uuid { get; set; }
        public string name { get; set; }
        public string phone_number { get; set; }
    }

    #region Request
    public class BunkerRequest : BaseRequest
    {
        public int? province_id { get; set; }
        public int? town_id { get; set; }
        public int? village_id { get; set; }
        public int? state { get; set; }
    }

    //Lấy danh sách thiết bị - vật tư trong kho
    public class SuppliesDeviceInBunkerRequest : BaseRequest
    {
        public string storage_uuid { get; set; } //uuid kho
        public int? category_id { get; set; } //Tìm kiếm theo chủng loại
    }

    //Lấy danh sách nhân viên kho
    public class StaffInBunkerRequest : PaginationRequest
    {
        public string storage_uuid { get; set; } //uuid kho
    }

    //---Begin: Nhập kho---
    public class ImportBunkerRequest
    {
        public string exportUuid { get; set; }
        public string document_origin { get; set; } //Chứng từ gốc
        public int type { get; set; }
        public string supplier_uuid { get; set; } //Mã nhà cung cấp (nếu là nhập từ NCC)
        public string from_uuid { get; set; } //uuid kho nguồn/ uuid nvkt (Null trong trường hợp nhập từ nhà cung cấp)
        public string to_uuid { get; set; } //uuid Kho đích
        public string from_staff_uuid { get; set; } //Nhận từ nhân viên nào hoặc thủ kho của kho FromUuid (null trong trường hợp nhập từ nhà cung cấp)
        public string to_staff_uuid { get; set; } //Người gửi lên yêu cầu nhập kho
        public string to_staff_name { get; set; } //Tên người gửi lên yêu cầu nhập kho
        //public string deliverer { get; set; } //Người giao hàng
        public string description { get; set; }
        public List<DetailInBunker> detail { get; set; }
    }

    public class DetailInSupplies
    {
        public int? category_id { get; set; }
        public string code { get; set; }
        public string unit { get; set; }
        public int? export_price { get; set; }
        public int? import_price { get; set; }
        public string the_firm { get; set; }
        public int? state { get; set; }
    }

    public class DetailInBunker
    {
        public int? category_id { get; set; }
        public int? document_quantity { get; set; } //Số lượng trên chứng từ
        public int? real_quantity { get; set; } //Số lượng thực nhập vào kho
        public long? unit_price { get; set; } //Đơn giá
        public string unit_price_string { get; set; } //convert đơn giá thành #,###
        public long? total_money
        {
            get
            {
                if (real_quantity != null && unit_price != null)
                {
                    return (real_quantity * unit_price);
                }
                return 0;
            }
        }
        public List<DeviceInDetail> device { get; set; }
    }

    public class DeviceInDetail
    {
        public string uuid { get; set; }
        public string serial { get; set; } //gửi kèm khi nhập từ NCC
        //public string imei { get; set; } //gửi kèm khi nhập từ NCC
        public DateTime? expire_device //gửi kèm khi nhập từ NCC
        {
            get
            {
                if (!string.IsNullOrEmpty(expire_device_string))
                {
                    return DateTime.ParseExact(expire_device_string, "dd/MM/yyyy", null);
                }

                return null;
            }
        }
        public string expire_device_string { get; set; }
    }
    //---End: Nhập kho---

    //Xóa nhân viên khỏi kho
    public class DeleteStaffRequest
    {
        public string storage_uuid { get; set; } //uuid kho
        public string storage_keeper_uuid { get; set; } //uuid nhân viên kho
    }

    //Thêm nhân viên kho
    public class AddStaffInBunkerRequest
    {
        public string storage_uuid { get; set; }
        public List<string> storage_keeper { get; set; }
    }

    //Lấy danh sách kiểm kê kho
    public class InventoryRequest : BaseRequest
    {
        public string storage_uuid { get; set; }
        public DateTime? from
        {
            get
            {
                if (!string.IsNullOrEmpty(date_string))
                {
                    var temp = date_string.Split('-')[0].Trim();

                    return !string.IsNullOrEmpty(temp) ? DateTime.ParseExact(temp, "dd/MM/yyyy", null) : DateTime.Now;
                }
                return DateTime.Now;
            }
        }
        public DateTime? to
        {
            get
            {
                if (!string.IsNullOrEmpty(date_string))
                {
                    var temp = date_string.Split('-')[1].Trim();

                    return !string.IsNullOrEmpty(temp) ? DateTime.ParseExact(temp, "dd/MM/yyyy", null) : DateTime.Now;
                }
                return DateTime.Now;
            }
        }
        public string date_string { get; set; }
    }

    //Begin: Xuất kho
    public class ExportBunkerRequest
    {
        public int type { get; set; }
        public string to_uuid { get; set; } //Mã kho đích/ Mã nvkt
        public string to_staff_uuid { get; set; } //Giao cho nhân viên nào(null trong trường hợp xuất trả Nhà Cung Cấp hoặc Thanh Lý)
        public string from_uuid { get; set; } //Kho xuất phát
        public string from_name { get; set; } //Tên Kho xuất phát
        public string from_staff_uuid { get; set; } //Ai là người lấy thiết bị từ kho để xuất kho, thường sẽ là thủ kho hoặc người trực ở kho
        public string from_staff_name { get; set; }
        public string supplier_uuid { get; set; }
        public string description { get; set; }
        public string document_origin { get; set; } //Tài liệu tham chiếu phiếu xuất kho
        public List<DetailExportInBunker> detail { get; set; }
        public List<CategoriesInDetailOffer> categories { get; set; }
    }

    public class DetailExportInBunker
    {
        public int category_id { get; set; }
        public int document_quantity { get; set; } //Số lượng trên chứng từ
        public int? real_quantity { get; set; } //Số lượng thực nhập vào kho
        public long? unit_price { get; set; }
        public string unit_price_string { get; set; } //convert đơn giá thành #,###
        public List<string> device { get; set; }
    }
    //End: Xuất kho

    //Thêm mới kho
    public class BunkerUpsertRequest
    {
        public string uuid { get; set; }
        public string name { get; set; }
        public int? province_id { get; set; }
        public int? town_id { get; set; }
        public int? village_id { get; set; }
        public string address { get; set; }
        public string code { get; set; }
        public int? state { get; set; }
        public List<string> storage_keeper { get; set; } //nhân viên trong kho
        public string unitSignedUuid { get; set; } //Đơn vị công tác
        public string unitSignedName { get; set; }
    }

    //---Begin: Thêm kiểm kê---
    public class AddInventoryRequest
    {
        public DateTime? time_stock_take { get; set; }
        public string creator_uuid { get; set; }
        public string creator_name { get; set; }
        public string storage_uuid { get; set; }
        public string description { get; set; }
        public List<HumanJoinInventory> staff { get; set; }
        public List<CategoriesInInventory> categories { get; set; }
        public List<DetailCategoriesInInventory> device { get; set; }
    }

    public class HumanJoinInventory  //Người tham gia kiểm kê
    {
        public string uuid { get; set; }
        public string name { get; set; }
        public string position { get; set; } //chức vụ
        public string role { get; set; } //Vai trò
    }

    public class CategoriesInInventory //Thông tin thiết bị - vật tư kiểm kê
    {
        public int? category_id { get; set; }
        public int quantity_exist { get; set; } //số lượng tồn kho
        public int real_quantity { get; set; } //số lượng tồn kho thực tế
        public int broken_quantity { get; set; } //Số lượng hỏng
    }

    public class DetailCategoriesInInventory //Thông tin chi tiết thiết bị
    {
        public string uuid { get; set; }
        public int? state { get; set; }
    }
    //---End: Thêm kiểm kê---

    public class DocumentOrigin
    {
        public string uuid { get; set; }
        public string code { get; set; }
    }

    public class OfferInBunkerRequest : BaseRequest
    {
        public string storageUuid { get; set; }
        public int? status { get; set; }
    }

    public class DetailOfferInBunkerRequest
    {
        public string order_uuid { get; set; }
    }

    public class HistoryOfBunkerRequest : BaseRequest
    {
        public string storage_uuid { get; set; }
        public int? type_of_ballot { get; set; }
        public int? type { get; set; }
    }

    //public class DetailSuppliesInBunkerRequest : BaseRequest
    //{
    //    public string storage_uuid { get; set; }
    //    public int? category_id { get; set; }
    //}

    public class BunkerDetailRequest
    {
        public string storage_uuid { get; set; }
    }

    public class SuppliesInInventoryRequest : PaginationRequest
    {
        public string inventory_uuid { get; set; }
        public int? category_id { get; set; }
    }

    public class InventoryDetailRequest
    {
        public string inventory_uuid { get; set; }
    }

    public class OfferInBunkerAddRequest
    {
        public string storage_uuid { get; set; }
        public string storage_name { get; set; }
        public string creator_uuid { get; set; }
        public string creator_name { get; set; }
        public string content { get; set; }
        public List<CategoryInOffer> categories { get; set; }
    }

    public class CategoryInOffer
    {
        public int? category_id { get; set; }
        public int? offer_quantity { get; set; }
    }

    public class UnitSigned
    {
        public string unit_signed_uuid { get; set; }
        public string unit_signed_name { get; set; }
    }

    //Từ chối nhập kho từ thông báo phiếu nhập
    public class RejectExportRequest
    {
        public string export_uuid { get; set; }
    }

    #endregion

    #region Response
    public class BunkerDataResponse : BunkerModel
    {
        public BunkerDataResponse()
        {
            items = new List<BunkerModel>();
            pagination = new PaginationResponse();
        }
        public List<BunkerModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }

    public class SuppliesDeviceInBunkerDataResponse : SuppliesDeviceInBunkerModel
    {
        public SuppliesDeviceInBunkerDataResponse()
        {
            items = new List<SuppliesDeviceInBunkerModel>();
            pagination = new PaginationResponse();
        }
        public List<SuppliesDeviceInBunkerModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }

    public class StaffInBunkerDataResponse : StaffInBunkerModel
    {
        public StaffInBunkerDataResponse()
        {
            items = new List<StaffInBunkerModel>();
            pagination = new PaginationResponse();
        }
        public List<StaffInBunkerModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }

    public class OfferInBunkerDataResponse : OfferInBunkerModel
    {
        public OfferInBunkerDataResponse()
        {
            items = new List<OfferInBunkerModel>();
            pagination = new PaginationResponse();
        }
        public List<OfferInBunkerModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
        //public string content { get; set; }
        public CustomerModel customer { get; set; }
        //public StaffInBunkerModel reviewer { get; set; }
        public string review_node { get; set; }
        public DateTime? time_review { get; set; }
        public string time_review_format
        {
            get
            {
                if (time_review.HasValue)
                {
                    return time_review.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
        //public CategoryModel MyProperty { get; set; }
        //public List<CategoriesInDetailOffer> categories { get; set; }
        //public int? state { get; set; }
    }

    public class InventoryInBunkerDataResponse : InventoryModel
    {
        public InventoryInBunkerDataResponse()
        {
            items = new List<InventoryModel>();
            pagination = new PaginationResponse();
        }
        public List<InventoryModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }

    public class HistoryInBunkerDataResponse : HistoryOfBunkerModel
    {
        public HistoryInBunkerDataResponse()
        {
            items = new List<HistoryOfBunkerModel>();
            pagination = new PaginationResponse();
        }
        public List<HistoryOfBunkerModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
        public string deliverer { get; set; } //Người giao
        public DocumentOrigin document_origin { get; set; }
        public BunkerModel from { get; set; }
        public BunkerModel to { get; set; }
        public SupplierModel supplier { get; set; }
    }
    #endregion
}