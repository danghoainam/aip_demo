﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class ListImportModel : BaseModel
    {
        public string code { get; set; }
        public string documentOrigin { get; set; }
        public Type type { get; set; }
        public StorageTo storageTo { get; set; }
        public StaffTo staffTo { get; set; }
        public taskTo taskTo { get; set; }
        public supplierTo supplierTo { get; set; }
        public ToStaff toStaff { get; set; }
        public fromStaff fromStaff { get; set; }
        public StaffFrom StaffFrom { get; set; }
        public SupplierFrom SupplierFrom { get; set; }
        public StorageFrom StorageFrom { get; set; }
        public TaskFrom TaskFrom { get; set; }
        public string namefrom { get; set; }
        public string uuidfrom { get; set; }
        public string nameto { get; set; }
        public int status { get; set; }
        public DateTime? timeCreated { get; set; }
        public string time_created
        {
            get
            {
                if (timeCreated.HasValue)
                {
                    return timeCreated.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
    }
    public class supplierTo
    {
        public string name { get; set; }
        public string uuid { get; set; }
    }
    public class ListImportRequest : BaseRequest
    {
        public string storageUuid { get; set; }
        public int? status { get; set; }
    }
    public class Type
    {
        public int type_Id { get; set; }
        public string type_Name { get; set; }
    }
    public class taskTo
    {
        public string uuid { get; set; }
        public string name { get; set; }
    }
    public class fromStaff
    {
        public string uuid { get; set; }
        public string name { get; set; }
    }
    public class StaffTo
    {
        public string uuid { get; set; }
        public string name { get; set; }
    }
    public class StorageTo
    {
        public string uuid { get; set; }
        public string name { get; set; }
    }
    public class ToStaff
    {
        public string uuid { get; set; }
        public string name { get; set; }
    }
    /// <summary>
    /// Tu nha vien toi kho type = 1 TH2: tu nhan vien toi nhan vien type = 4
    /// </summary>
    public class StaffFrom
    {
        public string uuid { get; set; }
        public string name { get; set; }
    }
    /// <summary>
    /// Tu nha cung cap toi kho  type = 2 Th2: tu nha cung cap toi nhan vien type = 6
    /// </summary>
    public class SupplierFrom
    {
        public string uuid { get; set; }
        public string name { get; set; }
    }
    /// <summary>
    /// Tu kho den kho type = 3 Th2: tu kho toi nhan vien type = 5
    /// </summary>
    public class StorageFrom
    {
        public string name { get; set; }
        public string uuid { get; set; }
    }
    public class TaskFrom
    {
        public string name { get; set; }
        public string uuid { get; set; }
    }
    /// <summary>
    /// detail
    /// </summary>
    public class ImportDetailRequest
    {
        public string import_uuid { get; set; }
    }
    public class ExportDetailRequest
    {
        public string export_uuid { get; set; }
    }
    public class ImDetailCategoriesRequest : BaseRequest
    {
        public string importUuid { get; set; }
    }
    public class ExDetailCategoriesRequest : BaseRequest
    {
        public string exportUuid { get; set; }
    }
    public class ImDetailCategoriesModel
    {
        public int id { get; set; }
        public string prefixName { get; set; }
        public string name { get; set; }
        public long? unitPrice { get; set; }
        public string unit { get; set; }
        public string manufacturer { get; set; }
        public string code { get; set; }
        public int type { get; set; }
        public int? documentQuantity { get; set; }
        public int? realQuantity { get; set; }
        //public long expPriceRef { get; set; }
        //public long impPriceRef { get; set; }
        public string name_display
        {
            get
            {
                if (!string.IsNullOrEmpty(code))
                {
                    return !string.IsNullOrEmpty(manufacturer) ? string.Format("{0} - {1} - {2}", code, name, manufacturer)
                                                    : string.Format("{0} - {1}", code, name);
                }
                return !string.IsNullOrEmpty(manufacturer) ? string.Format("{0} - {1}", name, manufacturer)
                                                    : name;
            }
        }
    }
    public class ImDetailDeviceModel
    {
        public string uuid { get; set; }
        public string name { get; set; }
        public string prefixName { get; set; }
        public string serial { get; set; }
        public string imei { get; set; }
        public DateTime? expireDevice { get; set; }
        public string timecreatedformat
        {
            get
            {
                if (expireDevice.HasValue)
                {
                    return expireDevice.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
        public string expireDevice_string
        {
            get
            {
                if (expireDevice.HasValue)
                {
                    return expireDevice.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
    }
    public class ImDetailDeviceRequest : BaseRequest
    {
        public string importUuid { get; set; }
        public int categoryId { get; set; }

    }
    public class ExDetailDeviceRequest : BaseRequest
    {
        public string exportUuid { get; set; }
        public int categoryId { get; set; }
    }
    public class ImDetailDeviceResponse
    {
        public ImDetailDeviceResponse()
        {
            items = new List<ImDetailDeviceModel>();
            pagination = new PaginationResponse();
        }
        public PaginationResponse pagination { get; set; }
        public List<ImDetailDeviceModel> items { get; set; }
    }
    public class ImDetailCategoriesResponse
    {
        public ImDetailCategoriesResponse()
        {
            items = new List<ImDetailCategoriesModel>();
            pagination = new PaginationResponse();
        }
        public List<ImDetailCategoriesModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }

    public class ImportDetailResponese
    {
        public ImportDetailResponese()
        {

        }
        public string uuid { get; set; }
        public int type { get; set; }
        public DateTime? time_created { get; set; }
        public string timecreatedformat
        {
            get
            {
                if (time_created.HasValue)
                {
                    return time_created.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
        public StaffTo staff_create { get; set; }
        public string document_origin { get; set; }
        public string description { get; set; }
        public string to { get; set; }
        public string from { get; set; }
        public string staff_name { get; set; }
        public string store_uuid { get; set; }
    }
    public class ExportDetailResponese
    {
        public ExportDetailResponese()
        {

        }
        public string uuid { get; set; }
        public int type { get; set; }
        public DateTime? time_created { get; set; }
        public string timecreatedformat
        {
            get
            {
                if (time_created.HasValue)
                {
                    return time_created.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
        public StaffTo staff_create { get; set; }
        public string document_origin { get; set; }
        public string description { get; set; }
        public To to { get; set; }
        public From from { get; set; }
        public string staff_name { get; set; }
        public string store_uuid { get; set; }
        public string code { get; set; }
    }
    public class To
    {
        public string uuid { get; set; }
        public string name { get; set; }
    }
    public class From
    {
        public string uuid { get; set; }
        public string name { get; set; }
    }
    public class ListImportDataResponse
    {
        public ListImportDataResponse()
        {
            items = new List<ListImportModel>();
            pagination = new PaginationResponse();
        }
        public List<ListImportModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
    public class MessengerImportResponse
    {
        public MessengerImportResponse()
        {

        }
        public string uuid { get; set; }
        public int type { get; set; }
        public DateTime? time_created { get; set; }
        public string timecreatedformat
        {
            get
            {
                if (time_created.HasValue)
                {
                    return time_created.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
        public string staff_create { get; set; }
        public string document_origin { get; set; }
        public string description { get; set; }
        public StorageTo to { get; set; }
        public StorageFrom from { get; set; }
        public string staff_name { get; set; }
    }
}
