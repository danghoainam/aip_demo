﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class TaskStateModel : BaseModel
    {
        public int? state { get; set; }
        public List<ImageModel> images { get; set; }
        public string note { get; set; }
        public string task_uuid { get; set; }
    }
    #region Request
    public class TaskStateRequest : BaseRequest
    {
        public string uuid { get; set; }
        //public string task_uuid { get; set; }
        public int state { get; set; }      
    }
    public class TaskStateAddRequest 
    {
        public string uuid { get; set; }
        public string task_uuid { get; set; }
        public int? state { get; set; }
        public string note { get; set; }
        public List<string> images {
            get
            {
                if (!string.IsNullOrEmpty(uuid_images))
                {
                    var abc = uuid_images.Split(",".ToCharArray()).ToList();
                    abc.RemoveAt(0);
                    return abc;

                }
                return null;
            }
            
        }
        public string uuid_images { get; set; }

    }
    #endregion
    #region Response
    public class TaskStateDataResponse
    {
        public TaskStateDataResponse()
        {
            items = new List<TaskStateModel>();
            pagination = new PaginationResponse();
        }
        public List<TaskStateModel> items { get; set; }
        public PaginationResponse pagination { get; set; }

    }
   
    #endregion
}
