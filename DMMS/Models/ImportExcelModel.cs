﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class ImportExcelModel
    {
        public string projectCode { get; set; }
        public string projectProvinceName { get; set; }
        public string projectName { get; set; }
        public string projectDescription { get; set; }
        public string projectTeamName { get; set; }
        public string projectFieldName { get; set; }
        public string contractCode { get; set; }
        public long contractCost { get; set; }
        public string contractTimeSigned { get; set; }
        public int contractTimeValidValue { get; set; }
        public string contractTimeValidStart { get; set; }
        public int contractTimeMaintainLoop { get; set; }
        public int contractTimeMaintainValue { get; set; }
        public string contractUnitSignedName { get; set; }
        public string contractInvestorName { get; set; }
        public bool contractIsAutoClose { get; set; }
        public string contractStateName { get; set; }
        public string customerName { get; set; }
        public string customerAddress { get; set; }
        public string customerProvinceName { get; set; }
        public string customerTownName { get; set; }
        public string customerVillageName { get; set; }
        public string customerEmail { get; set; }
        public string customerContactName { get; set; }
        public string customerContactPhone { get; set; }
        public string customerContactDuty { get; set; }
        public string customerStateName { get; set; }
        public string packageName { get; set; }
        public string packageCodeName { get; set; }
        public int packageCount { get; set; }       
    }

    public class CustomerAndPackageModel
    {
        public string customerName { get; set; }
        public string customerAddress { get; set; }
        public string customerProvinceName { get; set; }
        public string customerTownName { get; set; }
        public string customerVillageName { get; set; }
        public string customerEmail { get; set; }
        public string customerContactName { get; set; }
        public string customerContactPhone { get; set; }
        public string customerContactDuty { get; set; }
        public string customerStateName { get; set; }     
        public string packageName { get; set; }
        public string packageCodeName { get; set; }
        public int packageCount { get; set; }
    }

    public class ImportProject
    {
        public string code { get; set; }
        public string provinceName { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string teamName { get; set; }
        public string fieldName { get; set; }
    }

    public class ImportContract
    {
        public string code { get; set; }
        public long cost { get; set; }
        public string timeSigned { get; set; }
        public int timeValidValue { get; set; }
        public string timeValidStart { get; set; }
        public int timeMaintainLoop { get; set; }
        public int timeMaintainValue { get; set; }
        public string unitSignedName { get; set; }
        public string investorName { get; set; }
        public bool isAutoClose { get; set; }
        public string stateName { get; set; }
    }

    public class ImportCustomer
    {
        public string name { get; set; }
        public string address { get; set; }
        public string provinceName { get; set; }
        public string townName { get; set; }
        public string villageName { get; set; }
        public string email { get; set; }
        public string contactName { get; set; }
        public string contactPhone { get; set; }
        public string contactDuty { get; set; }
        public string stateName { get; set; }
    }

    public class ImportPackage
    {
        public string name { get; set; }
        public string packageCodeName { get; set; }
        public int count { get; set; }
    }

    public class ImportCustomerAndPackage
    {
        public ImportCustomer customer { get; set; }
        public List<ImportPackage> packages { get; set; }      
        public ImportCustomerAndPackage()
        {
            customer = new ImportCustomer();
            packages = new List<ImportPackage>();
        }
    }

    public class RequestProjectsAddData
    {
        public ImportProject project { get; set; }
        public ImportContract contract { get; set; }
        public List<ImportCustomerAndPackage> customerAndPackage { get; set; }
        public RequestProjectsAddData()
        {
            project = new ImportProject();
            contract = new ImportContract();
            customerAndPackage = new List<ImportCustomerAndPackage>();
        }
    }

    public class RequestProjectsChecking
    {
        public List<string> projectCodes { get; set; }
        public List<string> customerNames { get; set; }
        public List<string> packageNames { get; set; }
        public List<string> packageCodes { get; set; }
        public List<LocationsName> locations { get; set; }
        public List<CustomerAndLocation> customerAndLocations { get; set; }
        public RequestProjectsChecking()
        {
            projectCodes = new List<string>();
            customerNames = new List<string>();
            packageNames = new List<string>();
            packageCodes = new List<string>();
            locations = new List<LocationsName>();
            customerAndLocations = new List<CustomerAndLocation>();
        }
    }

    public class LocationsName
    {
        public string provinceName { get; set; }
        public string townName { get; set; }
        public string villageName { get; set; }
    }

    public class CustomerAndLocation
    {
        public string customerName { get; set; }
        public string provinceName { get; set; }
    }

    public class ProjectsCheckingResponse
    {
        public ProjectCode project_code { get; set; }
        public LocationAssembl location_assembly { get; set; }
        public CustomerName customer_name { get; set; }
        public PackageName package_name { get; set; }
        public PackageCode package_code { get; set; }
        public CustomerAndLcationResponse customer_and_location { get; set; }
        

    }

    public class CustomerAndLcationResponse
    {
        public List<CustomerAndLocation> exist { get; set; }
        public List<CustomerAndLocation> not_exits { get; set; }
    }

    public class ProjectCode
    {
        public List<string> exist { get; set; }
        public List<string> not_exits { get; set; }
    }

    public class LocationAssembl
    {
        public List<string> exist { get; set; }
        public List<LocationsName> not_exits { get; set; }
    }

    public class CustomerName
    {
        public List<string> exist { get; set; }
        public List<string> not_exits { get; set; }
    }

    public class PackageName
    {
        public List<string> exist { get; set; }
        public List<string> not_exits { get; set; }
    }

    public class PackageCode
    {
        public List<string> exist { get; set; }
        public List<string> not_exits { get; set; }
    }

}
