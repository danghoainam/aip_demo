﻿using DMMS.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class BaseModel
    {
        public string uuid { get; set; }
        public int? id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? closed_at { get; set; }
        public DateTime? state_update_at { get; set; }
        public DateTime? complete_at { get; set; }
        public DateTime? deadline { get; set; }
        public DateTime? time_created { get; set; }
        public DateTime? timeCreated { get; set; }
        public DateTime? time_late_update { get; set; }
        public DateTime? date_report { get; set; }
        public DateTime? time_last_update { get; set; }
        public DateTime? time_created_at { get; set; }
        public string time_created_display { get; set; }
        public string time_created_at_format
        {
            get
            {
                if (time_created_at.HasValue)
                {
                    return time_created_at.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
        public string time_created_format
        {
            get
            {
                if (time_created.HasValue)
                {
                    return time_created.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
        public string timecreatedformat
        {
            get
            {
                if (timeCreated.HasValue)
                {
                    return timeCreated.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
        public string time_created_format_nohh
        {
            get
            {
                if (time_created.HasValue)
                {
                    return time_created.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
        public string date_report_format
        {
            get
            {
                if (date_report.HasValue)
                {
                    return date_report.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
        public string date_report_formatnohh
        {
            get
            {
                if (date_report.HasValue)
                {
                    return date_report.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
        public string time_last_update_format
        {
            get
            {
                if (time_last_update.HasValue)
                {
                    return time_last_update.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
        public string time_late_update_format
        {
            get
            {
                if (time_late_update.HasValue)
                {
                    return time_late_update.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
        public string deadline_format
        {
            get
            {
                if (deadline.HasValue)
                {
                    return deadline.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }

        public string complete_at_format
        {
            get
            {
                if (complete_at.HasValue)
                {
                    return complete_at.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }

        public string state_update_at_format
        {
            get
            {
                if (state_update_at.HasValue)
                {
                    return state_update_at.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }

        public string closed_at_format
        {
            get
            {
                if (closed_at.HasValue)
                {
                    return closed_at.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }

        public string created_at_format
        {
            get
            {
                if (created_at.HasValue)
                {
                    return created_at.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
    }
}
