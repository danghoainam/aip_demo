﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class SuppliesDevice
    {
        public string name { get; set; }
        public int? type { get; set; }
        public int? linhvuc { get; set; } //Lĩnh vực
        public string code { get; set; }
        public string hang { get; set; } //hãng
        public string model { get; set; }
    }
}
