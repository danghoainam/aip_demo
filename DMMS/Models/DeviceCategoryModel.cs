﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class DeviceCategoryModel : BaseModel
    {        
        public string parent_id { get; set; }
        public DeviceCategoryModel childrents { get; set; }
    }

    #region Request
    public class DeviceCategoryAddRequest
    {
        public int? id { get; set; }
        public int parent_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }

    #endregion

    #region Response
    public class DeviceCategoryDataResponse
    {
        public DeviceCategoryDataResponse()
        {
            items = new List<DeviceCategoryModel>();
        }
        public List<DeviceCategoryModel> items { get; set; }
    }

    #endregion
}
