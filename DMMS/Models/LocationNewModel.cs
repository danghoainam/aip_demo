﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class LocationNewModel
    {
        public int? id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public int? provinceId { get; set; }
        public int? townId { get; set; }
        public string province { get; set; }
        public string town { get; set; }
        public List<int> locationAssembly { get; set; }
        public List<int> locationTown { get; set; }
        public List<int> locationVillage { get; set; }
    }

    public class ProvinceRequest : PaginationRequest { }
    public class TownRequest : PaginationRequest 
    {
        public int? province_id { get; set; }
    }

    public class VillageRequest : PaginationRequest
    {
        public int? town_id { get; set; }
    }

    public class ProvinceDetail
    {
        public int? province_id { get; set; }
    }

    public class TownDetail
    {
        public int? town_id { get; set; }
    }

    public class VillageDetail
    {
        public int? villeage_id { get; set; }
    }

    public class LocationNewDataResponse : LocationNewModel
    {
        public LocationNewDataResponse()
        {
            items = new List<LocationNewModel>();
            pagination = new PaginationResponse();
        }
        public List<LocationNewModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
}
