﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class ContactModel : BaseModel
    {
        public string phone_number { get; set; }
        public string duty { get; set; }
        public int? state { get; set; }
        public string name_display 
        {
            get
            {
                if(phone_number != null)
                {
                    return name + " - " + phone_number;
                }
                else
                {
                    return name + " - Chưa có số điện thoại";
                }
            }
        }
    }

    #region: request
    public class ContactRequest : BaseRequest
    {
        public string customer_uuid { get; set; }
    }

    public class ContactDetailRequest
    {
        public string uuid { get; set; }
    }

    public class ContactUpsertRequest
    {
        public string uuid { get; set; }
        public string name { get; set; }
        public string phone_number { get; set; }
        public string customer_uuid { get; set; }
        public string duty { get; set; }
    }
    #endregion
    #region:response
    public class ContactDataResponse : ContactModel
    {
        public ContactDataResponse()
        {

            items = new List<ContactModel>();
            pagination = new PaginationResponse();
        }
        public List<ContactModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
        public CustomerModel customer { get; set; }
    }
    #endregion
}
