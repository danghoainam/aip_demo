﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class ImageModel: BaseModel
    {        
        public string path { get; set; }
        public string owner_uuid { get; set; }
        public int is_enable { get; set; }

    }
    public class ImageAddRequest
    {
        public ImageAddRequest()
        {
            images = new List<string>();
        }
        public List<string> images { get; set; }
    }
    public class ImageDataResponse
    {
        public ImageDataResponse()
        {
            items = new List<ImageModel>();
            pagination = new PaginationResponse();
        }
        public List<ImageModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }

    public class FileUpload
    {
        public FileUpload()
        {
            FileNameFormat = "images[{0}]";
        }
        public byte[] FileBinary { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// File name format for upload
        /// </summary>
        public string FileNameFormat { get; set; }
    }
}
