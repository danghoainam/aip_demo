using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class CategoryModel : BaseModel
    {
        public int? parent_id { get; set; }
        public List<CategoryModel> children { get; set; }
    }

    #region request
    public class CategoryRequest : BaseRequest
    {
        public string keyword { get; set; }
    }

    public class CategoryDetailRequest
    {
        public int? id { get; set; }
    }

    public class CategoryUpsertRequest
    {
        public int? id { get; set; }
        public int? parent_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }
    #endregion

    #region response
    public class CategoryDataResponse
    {
        public CategoryDataResponse()
        {
            items = new List<CategoryModel>();
        }
        public List<CategoryModel> children { get; set; }
        public List<CategoryModel> items { get; set; }
        public int id { get; set; }
        public int? parent_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }
    #endregion

}
