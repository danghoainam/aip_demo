﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class StaffModel : BaseModel
    {
        public string address { get; set; }
        public string phone_number { get; set; }
        public string email { get; set; }
        public string team_uuid { get; set; }
        public Account account { get; set; }
        public Staff_Team team { get; set; }
        public Staff_Team department { get; set; }
        public List<Staff_Group> groups { get; set; }
        public int? state { get; set; }
        public int? type { get; set; }
        public int? img_avatar_id { get; set; }
        public string img_avatar_path { get; set; }
        public int status { get; set; }
        public string creator { get; set; }
        public Villages villages { get; set; }
        public Provinces provinces { get; set; }
        public Towns towns { get; set; }


    }
    public class StaffTaskModel : BaseModel
    {
        public priority priority { get; set; }
        public type type { get; set; }
        public string time_created
        {
            get
            {
                if (timeCreated.HasValue)
                {
                    return timeCreated.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
        public DateTime? timeCreated { get; set; }
        public string node { get; set; }
        public string code { get; set; }
        public type state { get; set; }
    }
    public class StaffBaloModel
    {
        public category category { get; set; }
        public int? quantity { get; set; }
        public string unit { get; set; }
        public type type { get; set; }
    }
    public class category
    {
        public string name { get; set; }
        public string prefixName { get; set; }

    }

    public class type
    {
        public int? id { get; set; }
        public string name { get; set; }
    }
    public class priority
    {
        public string name { get; set; }
        public int? id { get; set; }
    }
    public class Staff_Team
    {
        public string uuid { get; set; }
        public string name { get; set; }
    }
    public class LockAccount
    {
        public string id { get; set; }
    }
    public class Staff_Group
    {
        public string uuid { get; set; }
        public string name { get; set; }
        public string id { get; set; }
    }
    public class Account
    {
        public string uuid { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public int status { get; set; }
        
    }

    #region Request
    public class StaffGetListRequest
    {
        public string uuid { get; set; }
        public string name { get; set; }
        public string id { get; set; }
    }
    public class StaffListRequest : BaseRequest
    {
        public int? province_id { get; set; }
        public int? town_id { get; set; }
        public int? village_id { get; set; }
        public string team_uuid { get; set; }
        public string group_uuid { get; set; }
        public int? type { get; set; }
        public string phone_number { get; set; }
    }

    public class TeamLeaderRequest : PaginationRequest
    {
        public string teamUuid { get; set; }
    }

    public class StaffAddRequest
    {
        public string uuid { get; set; }
        public string address { get; set; }
        public string email { get; set; }
        public string phone_number { get; set; }
        public string name { get; set; }
        public string team_uuid { get; set; }
        public Account? account { get; set; }
        public string image { get; set; }
        public string avatar { get; set; }
        public string acc { get; set; }
        public int? type { get; set; }
        public int? province_id { get; set; }
        public int? town_id { get; set; }
        public int? village_id { get; set; }
        public Staff_Team team { get; set; }
        public Staff_Team department { get; set; }
        public string unit_signed_uuid { get; set; }
        public string unit_signed_name { get; set; }
    }
    public class StaffDetailRequest
    {
        public string uuid { get; set; }
        public int? id { get; set; }
    }
    public class StaffListByTeamLeadRequest
    {
        public int state { get; set; }
    }
    public class StaffListByGMRequest
    {
        public int state { get; set; }
        public string team_uuid { get; set; }
        public string group_uuid { get; set; }
        public string staff_leader_uuid { get; set; }
        public string task_uuid { get; set; }

    }
    public class StaffTaskRequest : BaseRequest
    {
        public string staffUuid { get; set; }
        public int? state { get; set; }
    }


    #endregion


    #region Response
    public class StaffAddDataResponse
    {
        public StaffAddDataResponse()
        {
            items = new List<StaffModel>();
            team = new Staff_Team();
            groups = new List<Staff_Group>();
            account = new Account();
            pagination = new PaginationResponse();
        }

        public List<StaffModel> items { get; set; }
        public Staff_Team team { get; set; }
        public Staff_Team department { get; set; }
        public List<Staff_Group> groups { get; set; }
        public Account? account { get; set; }
        public PaginationResponse pagination { get; set; }
        public string uuid { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string email { get; set; }
        public string phone_number { get; set; }
        public ImageModel image { get; set; }
        public string team_uuid { get; set; }
        public int? state { get; set; }
        public int? type { get; set; }
        public LocationAssembly location_assembly { get; set; }
        public string unit_signed_uuid { get; set; }


    }

    public class StaffResponse : ResponseObject
    {
        public StaffAddDataResponse data { get; set; }
    }
    public class StaffTaskDataResponse
    {
        public StaffTaskDataResponse()
        {
            items = new List<StaffTaskModel>();
            pagination = new PaginationResponse();
        }
        public PaginationResponse pagination { get; set; }
        public List<StaffTaskModel> items { get; set; }
    }
    public class StaffBaloDataResponse
    {
        public StaffBaloDataResponse()
        {
            items = new List<StaffBaloModel>();
            pagination = new PaginationResponse();
        }
        public PaginationResponse pagination { get; set; }
        public List<StaffBaloModel> items { get; set; }
    }

    public class StaffTeamLeaderResponse
    {
        public StaffTeamLeaderResponse()
        {
            items = new List<Staff_Team>();
            pagination = new PaginationResponse();
        }
        public PaginationResponse pagination { get; set; }
        public List<Staff_Team> items { get; set; }
    }
    #endregion


}
