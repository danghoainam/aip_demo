﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class ActionModel
    {
        public bool view { get; set; }
        public bool create { get; set; }
        public bool edit { get; set; }
        public bool delete { get; set; }
        public bool import { get; set; }
        public bool export { get; set; }
    }

    public class PermissionModel
    {
        public PermissionModel()
        {
            childs = new List<PermissionModel>();
        }

        public int id { get; set; }
        public string displayName { get; set; }
        public string route { get; set; }
        public string controllerName { get; set; }
        public string actionName { get; set; }
        public bool isShow { get; set; }
        public bool isApp { get; set; }
        public string method { get; set; }             
        public int parentId { get; set; }        
        public ActionModel permission { get; set; }

        public List<PermissionModel> childs { get; set; }

    }

}
