﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class CallLogModel
    {
        public string Authorization { get; set; }
        public CallModel call_log { get; set; }
        public CustomerModel customer { get; set; }
    }
    public class CallLogRequest : BaseRequest
    {
        public string caller { get; set; }
    }
    public class CallModel
    {
        public string uuid { get; set; }
        public string call_log_uuid { get; set; }
        public string cause_code { get; set; }
        public string called { get; set; }
        public string dateTime
        {
            get
            {
                long unixDate = time_start;
                System.DateTime dateTime = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);

                // Add the timestamp (number of seconds since the Epoch) to be converted
                dateTime = dateTime.AddSeconds(unixDate).ToLocalTime();
                return dateTime.ToString("dd/MM/yyyy HH:mm");
            }
        }      
        
        public long time_start { get; set; }
        public int? time_finish { get; set; }
        public int? time_answer { get; set; }
        public string audio { get; set; }
        public string caller { get; set; }
        public int state { get; set; }
        public Notes notes { get; set; }


    }
    public class Notes
    {
        public int id { get; set; }
        public string call_log_uuid { get; set; }
        public string tittle { get; set; }
        public string description { get; set; }
        public int note_type { get; set; }
        public DateTime time_created { get; set; }
        public int state { get; set; }
        public string uuid { get; set; }
    }
    public class CallLogDataResponse
    {
        public CallLogDataResponse()
        {
            items = new List<CallLogModel>();
            pagination = new PaginationResponse();
        }      
        public List<CallLogModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
}
