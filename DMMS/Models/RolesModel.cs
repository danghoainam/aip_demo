﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class RolesModel : BaseModel
    {
        public int is_default { get; set; }
    }
    public class RolesRequest : BaseRequest
    {
       
    }
    public class RolesAddRequest
    {
        public string name { get; set; }
        public bool is_default { get; set; }
        public string description { get; set; }
    }
    public class RolesDetailRequest
    {
        public string role_uuid { get; set; }
        public string keyword { get; set; }
    }
    public class RolesUpdateRequest
    {
        public string role_uuid { get; set; }
        public string name { get; set; }
        public int is_default { get; set; }
        public string description { get; set; }
        public List<string> permissions { get; set; }
    }
    public class RolesDeleteRequest
    {
        public string role_uuid { get; set; }
    }
    public class Role
    {
        public string name { get; set; }
        public int id { get; set; }
        public string description { get; set; }
        public string uuid { get; set; }
        public int is_default { get; set; }
    }
    public class Permissions 
    {
        public int type { get; set; }
        public List<PermissionsModel> permissions { get; set; }
    }

    public class RolesDataResponse
    {
        public RolesDataResponse()
        {
            items = new List<RolesModel>();
            pagination = new PaginationResponse();

        }
        public List<RolesModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
        public Role role { get; set; }
        public List<Permissions> permissions { get; set; }
        public List<string> listpermissinos { get; set; }
    }
}
