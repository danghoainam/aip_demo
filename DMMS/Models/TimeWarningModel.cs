﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class TimeWarningModel
    {
        public string uuid { get; set; }
        public int type { get; set; }
        public float warningValue { get; set; }
        public int warningLevel { get; set; }
        public bool isEnable { get; set; }
        public int? state { get; set; }
        public int? unit { get; set; }
    }

    #region: request
    public class TimeWarningRequest
    {
        public int? type { get; set; }
    }

    public class TimeWarningDetailRequest
    {
        public string uuid { get; set; }
    }

    public class TimeWarningUpsertRequest
    {
        //public string uuid { get; set; }
        //public int type { get; set; }
        //public float warningValue { get; set; }
        //public int warningLevel { get; set; }
        //public bool isEnable { get; set; }
        //public int? state { get; set; }
        ///// <summary>
        ///// Đơn vị đo thời gian
        /////1 - Minute
        /////2 - Hour
        /////3 - Day
        /////4 - Week
        /////5 - Month
        ///// </summary>
        //public int? unit { get; set; }
        public List<WarningRequest> warnings { get; set; }
    }

    public class WarningRequest
    {
        public string uuid { get; set; }
        public int type { get; set; }
        public float warningValue { get; set; }
        public int warningLevel { get; set; }
        public bool isEnable { get; set; }
        public int? state { get; set; }
        /// <summary>
        /// Đơn vị đo thời gian
        ///1 - Minute
        ///2 - Hour
        ///3 - Day
        ///4 - Week
        ///5 - Month
        /// </summary>
        public int? unit { get; set; }
    }
    #endregion

    #region: reponse
    public class TimeWarningDataResponse : TimeWarningModel
    {
        public TimeWarningDataResponse()
        {
            items = new List<TimeWarningModel>();
            pagination = new PaginationResponse();
        }
        public List<TimeWarningModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
    #endregion
}
