﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class ReportDailyModel : BaseModel
    {
        
        public int? state { get; set; }
        public StaffModel staff { get; set; }
        public Reviewer reviewer { get; set; }
        public TaskModel task { get; set; }
        public CustomerModel customer { get; set; }
        public string code { get; set; }
        
    }

    public class CalendarModel
    {
        public string title { get; set; }
        public string description { get; set; }
        public string className { get; set; }
        public int state { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public string id { get; set; }

        
    }
    public class Reviewer
    {
        public string uuid { get; set; }
        public string name { get; set; }
    }
    public class ReportDailyAdd 
    {
        public string uuid { get; set; }
        public string task_uuid { get; set; }
        public string customer_uuid { get; set; }
        public string content { get; set; }
        public DateTime? time_start { get; set; }
        public DateTime? time_end { get; set; }
        public DateTime? date_report { get; set; }
        public string totaltime { get;set; }
        public List<ImageModel> image { get; set; }
        public string taskmodel { get; set; }
        public string customermodel { get; set; }
        public string start
        {
            get
            {
                if (time_start.HasValue)
                {
                    return time_start.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
        public string datereport
        {
            get
            {
                if (date_report.HasValue)
                {
                    return date_report.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
        public string end
        {
            get
            {
                if (time_end.HasValue)
                {
                    return time_end.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
       
        public string staff_name { get; set; }
        public string reviewer_name { get; set; }
        public string date { get; set; }
        
    }
    public class ReportDailyRequest : BaseRequest
    {
        //public DateTime date_report { get; set; }
        public int? state { get; set; }
        public string staff_uuid { get; set; }
        public string task_uuid { get; set; }
        public string uuids { get; set; }
        public string date_report1 { get; set; }
        public double date_report { get; set; }
        public bool is_manager { get; set; }
        public int check { get; set; }

    }
    public class ReportDailyDetailRequest
    {
        public string uuid { get; set; }
    }
    public class ReportDailyDataResponse : ReportDailyAdd
    {
        public ReportDailyDataResponse()
        {
            items = new List<ReportDailyModel>();
            pagination = new PaginationResponse();
            staff = new StaffModel();
            reviewer = new Reviewer();
        }
        public List<ReportDailyModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
        public StaffModel staff { get; set; }
        public Reviewer reviewer { get; set; }
        public CustomerModel customer { get; set; }
        public TaskModel task { get; set; }
        public int state { get; set; }
        public List<ImageModel> image { get; set; }

    }
}
