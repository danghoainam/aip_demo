﻿using DMMS.Objects;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class DeviceModel : BaseModel
    {
        //public int? state { get; set; }
        //public string serial { get; set; }
        //public string imei { get; set; }
        //public string position { get; set; }
        //public List<ImageModel> images { get; set; }
        ////public List<ImageModel> image { get; set; }
        //public CategoryModel category { get; set; }
        //public CustomerModel customer { get; set; }
        //public SupplierModel supplier { get; set; }
        //public ProjectModel project { get; set; }
        //public DateTime? expire_device { get; set; }
        //public DateTime? expire_contract { get; set; }
        //public string model { get; set; }
        //public string code { get; set; }
        //public string expire_contract_format
        //{
        //    get
        //    {
        //        if (expire_contract.HasValue)
        //        {
        //            return expire_contract.Value.ToString("dd/MM/yyyy");
        //        }
        //        return string.Empty;
        //    }
        //}
        //public string supplierID { get; set; }
        //public string supplierName { get; set; }
        //public string categoryName { get; set; }
        public string code { get; set; }
        public string model { get; set; }
        public string prefix_name { get; set; }
        public string package_uuid { get; set; }
        public DateTime? expireDevice { get; set; }
        public DateTime? expiredContract { get; set; }

        public int state { get; set; }
        public string serial { get; set; }
        public string imei { get; set; }
        public SupplierModel supplier { get; set; } //Lấy uuid và name
        public string manufacturer { get; set; } //Hãng
        public string name_display
        {
            get
            {
                if (!string.IsNullOrEmpty(code))
                {
                    return !string.IsNullOrEmpty(model) ? string.Format("{0} - {1} - {2}", code, name, model) :
                        string.Format("{0} - {1}", code, name);
                }
                else return !string.IsNullOrEmpty(model) ? string.Format("{0} - {1}", name, model) : name;
            }
        }
        public string customer_uuid { get; set; }
        public string customer_name { get; set; }
        public ProjectModel project { get; set; } //Lấy name và uuid
        public PackageModel package { get; set; } //Lấy name và uuid
        public int total_devices { get; set; }
        public int identified_devices { get; set; }
        public int categoryId { get; set; }
        public string categoryName { get; set; }
        public int supplierId { get; set; }
        public string supplierName { get; set; }
        public string packageUuid { get; set; }
        public string packageName { get; set; }
        public DateTime? expired_contract { get; set; }
        public string expiredContract_nohh
        {
            get
            {
                if (expiredContract.HasValue)
                {
                    return expiredContract.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }

        public string expiredDevice_nohh
        {
            get
            {
                if (expireDevice.HasValue)
                {
                    return expireDevice.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
        public DateTime? time_valid_end { get; set; }
        public string time_valid_end_format
        {
            get
            {
                if (time_valid_end.HasValue)
                {
                    return time_valid_end.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
        public string prefixName { get; set; }
    }

    public class DeviceInCustomerModel
    {
        public int category_id { get; set; }
        public string prefix_name { get; set; }
        public string name { get; set; }
        public string pkg_name { get; set; }
        public int? total_devices { get; set; }
        public int? count_defined_devices { get; set; }
        public string package_uuid { get; set; }
    }

    public class DeviceInCustomerRequest : BaseRequest
    {
        public string customer_uuid { get; set; }
    }

    public class DetailWithParamModel
    {
        public int? category { get; set; }
        public string code { get; set; }
        public string uuid { get; set; }
        public string prefix_name { get; set; }
        public string name { get; set; }
        public int? type { get; set; }
        public string serial { get; set; }
        public string imei { get; set; }
        public DateTime? expire_device { get; set; }
        public string expire_device_format
        {
            get
            {
                if (expire_device.HasValue)
                {
                    return expire_device.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
    }

    public class DeviceHistoryModel
    {
        public string uuid { get; set; }
        public int? state { get; set; }
        public StaffModel staff { get; set; }
        public TaskModel task { get; set; }
        public List<ImageModel> images { get; set; }
        public string note { get; set; }
        public DateTime? time_created { get; set; }
        public string time_created_format
        {
            get
            {
                if (time_created.HasValue)
                {
                    return time_created.Value.ToString("dd/MM/yyyy");
                }
                return string.Empty;
            }
        }
    }

    #region Request
    public class DeviceSearchRequest : BaseRequest
    {
        public int? state { get; set; }
        public string supplier_uuid { get; set; }
        public string customerUuid { get; set; }
        public string customer_uuid { get; set; }
        public string packageUuid { get; set; }
        public int? categoryId { get; set; }
        public List<int> states { get; set; }
        public string taskUuid { get; set; }

    }
    public class ProjectGetDeivce : BaseRequest
    {
        public string projectUuid { get; set; }
        public int? category_id { get; set; }
        public List<int> states { get; set; }
        //public string customerUuid { get; set; }
        //public int? supplierID { get; set; }
        //public int? numberOfDevice { get; set; }
    }
    public class ImportDevice
    {
        public string projectUuid { get; set; }
        public int fileUploadId { get; set; }
        public string customerUuid { get; set; }
        public string contractUuid { get; set; }
        public List<listDevicesip> listDevices { get; set; }
        public IFormFile FileUploadExcel { get; set; }
    }
    public class listDevicesip
    {
        public string category_name { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string model { get; set; }
        public string serial { get; set; }
        public string imei { get; set; }
        public int? categoryId { get; set; }
        public int? supplierId { get; set; }
        public string supplier_name { get; set; }
        public DateTime expireDevice { get; set; }
        public DateTime expiredContract { get; set; }
        public string packageUuid { get; set; }
        public string customerUuid { get; set; }
        public string contractUuid { get; set; }
    }
    public class DeviceDetailRequest
    {
        public string uuid { get; set; }
    }

    public class DeviceDetailWithParamsRequest
    {
        public string serial { get; set; }
        public string deviceUuid { get; set; }
        public int? category_id { get; set; }
    }

    public class ErrorUpload
    {
        public int count { get; set; }
        public string category_name { get; set; }
    }
    public class DeviceAddRequest : Readexcel
    {
        public string customer_uuid { get; set; }
        public string contract_uuid { get; set; }
        public string customer_name { get; set; }
        public string uuid { get; set; }
        public string name { get; set; }
        public int? category_id { get; set; }
        public int? categoryId { get; set; }
        public int? supplierId { get; set; }
        public DateTime? expire_device { get; set; }
        public DateTime? expire_contract { get; set; }
        public string serial { get; set; }
        public string imei { get; set; }
        public string supplier_uuid { get; set; }
        public string position { get; set; }
        public int? state { get; set; }
        public string project_customer_uuid { get; set; }
        public string project_uuid { get; set; }
        public string project_name { get; set; }
        public string category_name { get; set; }
        public string supplier_name { get; set; }
        public string code { get; set; }
        public string model { get; set; }
        public DateTime expiredDevice { get; set; }
        public DateTime expiredContract { get; set; }

        public List<string> images
        {
            get
            {
                if (!string.IsNullOrEmpty(uuid_images))
                {
                    var abc = uuid_images.Split(",".ToCharArray()).ToList();
                    abc.RemoveAt(0);
                    return abc;

                }
                return null;
            }

        }
        public IFormFile FileUploadExcel { get; set; }
        public string uuid_images { get; set; }
    }
    public class DeleteCustomerRequest
    {
        public string projectUuid { get; set; }
        public string contractUuid { get; set; }
        public string customerUuid { get; set; }
    }
    public class Readexcel
    {
        public ArrayList test { get; set; }
    }

    public class DeviceHistoryRequest : PaginationRequest
    {
        public string uuid { get; set; }
    }
    #endregion

    #region Response
    public class DeviceDataResponse : DeviceModel
    {
        public DeviceDataResponse()
        {
            items = new List<DeviceModel>();
            pagination = new PaginationResponse();
        }
        public List<DeviceModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
        //public string supplier_name { get; set; }
        public string category_name { get; set; }
        //public string imei { get; set; }
        //public string customer_name { get; set; }
        //public string customer_uuid { get; set; }
        public string avatar { get; set; }
        //public string project_customer_uuid { get; set; }
        public string code { get; set; }
        public List<ImageModel> images { get; set; }
    }

    public class DeviceHistoryDataResponse : DeviceHistoryModel
    {
        public DeviceHistoryDataResponse()
        {
            items = new List<DeviceHistoryModel>();
            pagination = new PaginationResponse();
        }
        public List<DeviceHistoryModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }

    public class DetailWithParamDataResponse : DetailWithParamModel
    {
        public DetailWithParamDataResponse()
        {
            items = new List<DetailWithParamModel>();
            pagination = new PaginationResponse();
        }
        public List<DetailWithParamModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }

    public class DeviceInCustomerDataResponse : DeviceInCustomerModel
    {
        public DeviceInCustomerDataResponse()
        {
            items = new List<DeviceInCustomerModel>();
            pagination = new PaginationResponse();
        }
        public List<DeviceInCustomerModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
    #endregion
}
