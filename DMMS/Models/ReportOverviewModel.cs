﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class DataModel
    {

         public int count { get; set; }
         public string field_name { get; set; }
        //public string title { get; set; }
    }
    public class ReportOverviewModel
    {
        public string title { get; set; }
        public List<DataModel> data { get; set; }
    }
    public class RequestReportOverview
    {
        public string timeFrom { get; set; }
        public string timeTo { get; set; }
        public string teamUuid { get; set; }
        public int? provinceId { get; set; }
        public int? fieldId { get; set; }
        public string unitSignedUuid { get; set; }

    }
    public class ReportOverviewDataResponse
    {
        public List<ReportOverviewModel> items { get; set; }
        public string title { get; set; }
        public ReportOverviewDataResponse()
        {
            items = new List<ReportOverviewModel>();
        }
        
    }

    public class TicketOverviewModel
    {
        public string date { get; set; }
        public string count { get; set; }
        public string type { get; set; }
        public string state { get; set; }
    }
    public class ReportTicketDataResponse
    {
        public List<TicketOverviewModel> items { get; set; }
    }
    public class ReportTaskDataResponse
    {
        public List<TicketOverviewModel> items { get; set; }
    }

}
