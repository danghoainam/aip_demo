﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class TaskModel : BaseModel
    {         
        
        public TicketModel ticket { get; set; }
        public StaffModel staff { get; set; }
        public List<StaffModel> staffs { get; set; }
        public int? state { get; set; }
        public string code { get; set; }
        public int? type { get; set; }
        public List<EmployeesModel> employees { get; set; }
        public CustomerModel customer { get; set; }
        public List<DeviceModel> devices { get; set; }
        public string staff_uuid { get; set; }
        public int? priority { get; set; }
        public DateTime? start_estimate { get; set; }
        public DateTime? finish_estimate { get; set; }
        public string start_estimate_format
        {
            get
            {
                if (start_estimate.HasValue)
                {
                    return start_estimate.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
        public string finish_estimate_format
        {
            get
            {
                if (finish_estimate.HasValue)
                {
                    return finish_estimate.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
        public string starttime { get; set; }
        public string finishtime { get; set; }
        public long? cost { get; set; }
        public string note_plan { get; set; }
        public string cost_format
        {
            get
            {
                if (cost.HasValue)
                {
                    return string.Format(CultureInfo.GetCultureInfo("vi-VN"), "{0:C0}", cost);
                }
                return "";
            }
        }
    }
 
    public class EmployeesModel
    {
        public string uuid { get; set; }
        public string name { get; set; }
        public string phone_number { get; set; }
        public ImageModel image { get; set; }
        public bool responsible { get; set; }
        public string job { get; set; }
    }

    #region Request
    public class TaskRequest : BaseRequest
    {
        public string owner_uuid { get; set; }
        public string staff_uuid { get; set; }
        public List<string> state {
            get
            {
                if (!string.IsNullOrEmpty(stateIds))
                {
                    var abc = stateIds.Split(",".ToCharArray()).ToList();
                    
                    return abc;

                }
                return null;
            }
        }
        public List<string> uuids { get; set; }
        public string uuid { get; set; }
        public string stateIds { get; set; }
        public int? priority { get; set; }
    }

    public class ChangeStateRequest
    {
        public string uuid { get; set; }
        public int? state { get; set; }
        public long? deadline { get; set; }
        public string offer { get; set; }
        public string note { get; set; }
        public List<int> images { get; set; }
        public string image_uuids { get; set; }
        public string deadline_str { get; set; }

    }

    public class TaskAddRequest
    {
        public string uuid { get; set; }
        public string staff_uuid { get; set; }
        public string description { get; set; }
        public string name { get; set; }
        public DateTime? deadline { get; set; }
        public string deadline_format
        {
            get
            {
                if (deadline.HasValue)
                {
                    return deadline.Value.ToString("dd/MM/yyyy HH:mm");
                }
                return string.Empty;
            }
        }
        public int state { get; set; }
        public string code { get; set; }
        public int priority { get; set; }
        //public DateTime? start_estimate { get; set; }
        //public DateTime? finish_estimate { get; set; }
        //public string start_estimate_format
        //{
        //    get
        //    {
        //        if (start_estimate.HasValue)
        //        {
        //            return start_estimate.Value.ToString("dd/MM/yyyy HH:mm");
        //        }
        //        return string.Empty;
        //    }
        //}
        //public string finish_estimate_format
        //{
        //    get
        //    {
        //        if (start_estimate.HasValue)
        //        {
        //            return start_estimate.Value.ToString("dd/MM/yyyy HH:mm");
        //        }
        //        return string.Empty;
        //    }
        //}
        //public string starttime { get; set; }
        //public string finishtime { get; set; }
    }

    public class TaskDetailRequest
    {
        public string uuid { get; set; }
    }

    #endregion


    #region Response
    public class TaskAddDataResponse
    {
        public TaskAddDataResponse()
        {
            items = new List<TaskModel>();
        }
        public List<TaskModel> items { get; set; }

    }
   
    public class TaskDataResponse : TaskModel
    {
        public TaskDataResponse()
        {
            items = new List<TaskModel>();
            pagination = new PaginationResponse();
        }
        public List<TaskModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
    }
    #endregion
}
