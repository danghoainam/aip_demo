﻿using DMMS.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Models
{
    public class TaskOfferModel : BaseModel
    {
        public string censor_note { get; set; }
        public int state { get; set; }
        public int task_id { get; set; }
        public int? task_state { get; set; }
        public List<ImageModel> images { get; set; } 
        public Censor censor { get; set; }
        public EmployeesModel employee { get; set; }
        public TaskModel task { get; set; }
        public string timecanceloffer { get; set; }

    }
    public class Censor
    {
        public int id { get; set; }
        public string uuid { get; set; }
        public string name { get; set; }
        public ImageModel image { get; set; }
    }
    public class TaskOfferRequest : BaseRequest
    {
        public string note { get; set; }
        public string uuid { get; set; }
        public int state { get; set; }
    }
    public class TaskOfferAddRequest : TaskOfferModel
    {
       

    }
    public class TaskOfferDataResponse : TaskOfferModel
    {
        public TaskOfferDataResponse()
        {
            items = new List<TaskOfferModel>();
            pagination = new PaginationResponse();
        }
        public List<TaskOfferModel> items { get; set; }
        public PaginationResponse pagination { get; set; }
        //public List<ImageModel> images { get; set; }
        //public TaskModel task { get; set; }
        //public EmployeesModel employee { get; set; }
    }
}
