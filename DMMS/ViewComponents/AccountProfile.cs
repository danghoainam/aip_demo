﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DMMS.Interfaces;
using DMMS.Models;
using DMMS.Objects;
using DMMS.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace DMMS.ViewComponents
{
    public class AccountProfile : ViewComponent
    {
        private readonly IAccountService _accountService;
        protected ClaimsIdentity ClaimsIdentity => User.Identity as ClaimsIdentity;
        public AccountProfile(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var model = new AccountModel();             
            var header = new RequestHeader
            {
                Authorization = (ClaimsIdentity != null && ClaimsIdentity.Claims != null && ClaimsIdentity.Claims.Count() > 0) ? ClaimsIdentity.FindFirst(Constants.TOKEN).Value : string.Empty,
                ContentType = "application/json",
                Method = "POST",
                Action = "accounts/profile"
            };            
            var obj = _accountService.Profile(header);
            if (obj != null && (EnumError)obj.error == EnumError.Success && obj.data != null && obj.data.user_info != null)
            {
                model = new AccountModel
                {
                    address = obj.data.user_info.address,
                    email = obj.data.user_info.email,
                    name = obj.data.user_info.name,
                    phone_number = obj.data.user_info.phone_number,
                    status = obj.data.user_info.status,
                    username = obj.data.username,
                    id = obj.data.id,
                    uuid = obj.data.user_info.uuid,
                    image = obj.data.user_info.image
                };
            }

            return View(model);
        }
    }
}