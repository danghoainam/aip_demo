using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DMMS.Extensions;
using DMMS.Interfaces;
using DMMS.Objects;
using DMMS.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;

namespace DMMS
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var mvc = services.AddControllersWithViews();

#if (DEBUG)
            mvc.AddRazorRuntimeCompilation();
#endif

            services.Configure<FormOptions>(o => {
                o.ValueLengthLimit = int.MaxValue;
                o.MultipartBodyLengthLimit = int.MaxValue;
                o.MemoryBufferThreshold = int.MaxValue;
            });

            services.AddSingleton<ITreeCategoryService,TreeCategoryService>();
            services.AddSingleton<ITreeDicService,TreeDicService>();

            services.AddSingleton<IConfiguration>(Configuration);
            services.Configure<CustomConfigApi>(Configuration.GetSection("CustomConfigApi"));
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options=> {
                        options.AccessDeniedPath = new PathString("/Authen/AccessDenined");
                        options.Cookie = new CookieBuilder
                        {                            
                            HttpOnly = true,
                            Name = ".DMMD.Security.Cookie",
                            Path = "/",
                            SameSite = SameSiteMode.Lax,
                            SecurePolicy = CookieSecurePolicy.SameAsRequest,                            
                        };
                        options.Events = new CookieAuthenticationEvents
                        {
                            OnSignedIn = context =>
                            {
                                Console.WriteLine("{0} - {1}: {2}", DateTime.Now,
                                    "OnSignedIn", context.Principal.Identity.Name);
                                return Task.CompletedTask;
                            },
                            OnSigningOut = context =>
                            {
                                Console.WriteLine("{0} - {1}: {2}", DateTime.Now,
                                    "OnSigningOut", context.HttpContext.User.Identity.Name);
                                return Task.CompletedTask;
                            },
                            OnValidatePrincipal = context =>
                            {
                                Console.WriteLine("{0} - {1}: {2}", DateTime.Now,
                                    "OnValidatePrincipal", context.Principal.Identity.Name);
                                return Task.CompletedTask;
                            }
                        };                        
                        options.LoginPath = new PathString("/Authen/Login");
                        options.LogoutPath = new PathString("/Authen/Logout");
                        options.ReturnUrlParameter = "RequestPath";
                        options.SlidingExpiration = true;
                    });

            services.AddHttpContextAccessor();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            #region DI

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddSingleton(typeof(IRestApi<>), typeof(RestApi<>));
            services.AddSingleton(typeof(ITicketService), typeof(TicketService));
            services.AddSingleton(typeof(IAuthenService), typeof(AuthenService));
            services.AddSingleton(typeof(ICustomerService), typeof(CustomerService));
            services.AddSingleton(typeof(IDeviceCategoryService), typeof(DeviceCategoryService));
            services.AddSingleton(typeof(IDeviceService), typeof(DeviceService));
            services.AddSingleton(typeof(IDeviceSupplierService), typeof(DeviceSupplierService));
            services.AddSingleton(typeof(IEmployeeService), typeof(EmployeeService));
            services.AddSingleton(typeof(IGroupService), typeof(GroupService));
            services.AddSingleton(typeof(IImageService), typeof(ImageService));
            services.AddSingleton(typeof(IReportDailyService), typeof(ReportDailyService));
            services.AddSingleton(typeof(ITaskService), typeof(TaskService));
            services.AddSingleton(typeof(IProjectService), typeof(ProjectService));
            services.AddSingleton(typeof(ITaskStateHistoryService), typeof(TaskStateHistoryService));
            services.AddSingleton(typeof(IStaffService), typeof(StaffService));
            services.AddSingleton(typeof(ILocationService), typeof(LocationService));
            services.AddSingleton(typeof(ITeamService), typeof(TeamService));
            services.AddSingleton(typeof(ICategoryService), typeof(CategoryService));
            services.AddSingleton(typeof(ILocationService), typeof(LocationService));
            services.AddSingleton(typeof(IAccountService), typeof(AccountService));
            services.AddSingleton(typeof(ISupplierService), typeof(SupplierService));
            services.AddSingleton(typeof(ITaskStateService), typeof(TaskStateService));
            services.AddSingleton(typeof(IChartService), typeof(ChartService));
            services.AddSingleton(typeof(ICallLogService), typeof(CallLogService));
            services.AddSingleton(typeof(INotificationService), typeof(NotificationService));
            services.AddSingleton(typeof(ISipAccountService), typeof(SipAccountService));
            services.AddSingleton(typeof(ITaskOfferService), typeof(TaskOfferService));
            services.AddSingleton(typeof(ITimeWarningService), typeof(TimeWarningService));
            services.AddSingleton(typeof(IContractService), typeof(ContractService));
            services.AddSingleton(typeof(ITreeService), typeof(TreeService));
            services.AddSingleton(typeof(IKnowledgeService), typeof(KnowledgeService));
            services.AddSingleton(typeof(IProjectCustomerService), typeof(ProjectCustomerService));
            services.AddSingleton(typeof(IProjectPackageService), typeof(ProjectPackageService));
            services.AddSingleton(typeof(IPackageService), typeof(PackageService));
            services.AddSingleton(typeof(IProjectContractservice), typeof(ProjectContractService));
            services.AddSingleton(typeof(ITaskPlanService), typeof(TaskPlanService));
            services.AddSingleton(typeof(IBunkerService), typeof(BunkerService));
            services.AddSingleton(typeof(IListSuppliesService), typeof(ListSuppliesService));
            services.AddSingleton(typeof(IManageFiledService), typeof(ManageFiledService));
            services.AddSingleton(typeof(IListOfferService), typeof(ListOfferService));
            services.AddSingleton(typeof(ITreeCategoriesService), typeof(TreeCategoriesService));
            services.AddSingleton(typeof(ILocationNewService), typeof(LocationNewService));
            services.AddSingleton(typeof(IReportOverviewService), typeof(ReportOverviewService));
            services.AddSingleton(typeof(IReportStatisticDetailService), typeof(ReportStatisticDetailService));
            services.AddSingleton(typeof(IReportStatisticTroubleshootService), typeof(ReportStatisticTroubleshootService));
            services.AddSingleton(typeof(IReportStatisticDetailTaskService), typeof(ReportStatisticDetailTaskService));
            services.AddSingleton(typeof(IReportStatisticDetailCallService), typeof(ReportStatisticDetailCallService));
            services.AddSingleton(typeof(IReportStatisticDetailStatusDeviceService), typeof(ReportStatisticDetailStatusDeviceService));
            services.AddSingleton(typeof(IReportStatisticDetailSuppliesTaskService), typeof(ReportStatisticDetailSuppliesTaskService));
            services.AddSingleton(typeof(IReportStatisticDetailStatusContractService), typeof(ReportStatisticDetailStatusContractService)); 
            services.AddSingleton(typeof(IImportExcelService), typeof(ImportExcelService));
            services.AddSingleton(typeof(IPermissionsSerivce), typeof(PermissionsService));
            services.AddSingleton(typeof(IRolesService), typeof(RolesService));
            services.AddSingleton(typeof(IImportExcelPackageCategoriesService), typeof(ImportExcelPackageCategoriesService));

            #endregion

            services.AddControllers().AddNewtonsoftJson(o =>
            {
                //o.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                //o.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                //o.SerializerSettings.DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Ignore;
            });

            services.AddControllers(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                                .RequireAuthenticatedUser()
                                .Build();
                config.Filters.Add(new Microsoft.AspNetCore.Mvc.Authorization.AuthorizeFilter(policy));
            });

            services.Configure<GzipCompressionProviderOptions>(options => options.Level = System.IO.Compression.CompressionLevel.Fastest);
            // Add Response compression services
            services.AddResponseCompression(options =>
            {
                options.MimeTypes = new string[]
                {
                    "text/html",
                    "text/css",
                    "application/javascript",
                    "text/javascript",
                    "application/json"
                };
                options.Providers.Add<GzipCompressionProvider>();
                options.EnableForHttps = true;
            });            
            services.AddMvc(options => {
                options.EnableEndpointRouting = false;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/error");
                //app.UseDeveloperExceptionPage();
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(
                            Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "dist")),
                //RequestPath = new PathString("/app-images")
            });

            app.UseRouting();

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseRouting();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseResponseCompression();

            app.UseStaticFiles();

            app.UseMvc();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            

            
        }
    }
}
