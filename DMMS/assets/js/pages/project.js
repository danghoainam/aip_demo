"use strict";
// Class definition

var DMMSProject = function () {
    // Private functions

    var dataTableInstance = null;
    var drawer = null;
    var Packagetable = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    // demo initializer
    var initTable = function () {
        var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
        if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
            $.notify(editSucessCache.massage, { type: 'success' });
            localStorage.removeItem('edit_success');
        }
        dataTableInstance = $('.kt-datatable').KTDatatable({


            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/project/gets',
                    },
                    response: {
                        map: function (res) {

                            const raw = res && res.data || {};


                            if (raw.items != null) {
                                $.each(raw.items, function (index, item) {
                                    //Thực hiện lấy các property của item
                                    for (var property in item) {
                                         //Thực hiện repace với các thuộc tính có giá trị và dạng string
                                        if (item[property] && typeof item[property] == "string") {
                                           item[property] = item[property].replace(/</g, "&lt;").replace(/>/g, "&gt;");
                                            //item.description = item.description.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                                        }
                                    }
                                });
                            };

                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? 0 : pagination.page;
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword, provinceId, fieldId, teamUuid } = query;
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: keyword || $('#js-filter-keyword').val(),
                            provinceId: $('#js-filter-province').val(),
                            fieldId: $('#js-filter-filed').val(),
                            teamUuid: $('#js-filter-team').val(),
                            page: (keyword == undefined || keyword == '') ? pagination.page : 1,

                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },
            search: {
                //input: $('#js-filter-keyword'),
                onEnter: true,
            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 20,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'code',
                    title: 'Mã dự án',
                    width: 60
                },
                {
                    field: 'name_display',
                    title: 'Tên dự án',
                    template: function (data) {
                        var output = '<span title="'+ data.name +'">' + data.name_display + '</span>';
                        return output;
                    }
                },
                {
                    field: 'field_name',
                    title: 'Lĩnh vực',
                    width:170
                },
                //{
                //    field: 'count_package_device',
                //    title: 'SL Thiết bị',
                //    width: 100,
                //    textAlign: 'center',
                //    template: function (row) {
                //        var class1 = '';
                //        var n = '';
                //        n = row.count_package_device;
                //        n > 0 ? n : 0;
                //        if (n == 0) {
                //            class1 = 'text-danger'
                //        }
                //        else {
                //            class1 = 'text-primary'
                //        }
                //        return "<span class = " + class1 + ">" + n + " Thiết bị";
                //    },
                //},
                {
                    field: 'count_customers',
                    title: 'Đơn vị',
                    width: 70,
                    sortable: false,
                    overflow: 'visible',
                    textAlign: 'left',
                    //autoHide: false,
                    textAlign: 'center',
                    template: function (row) {
                        var class1 = '';
                        var n = '';
                        n = row.count_customers;
                        n > 0 ? n : 0;
                        if (n == 0) {
                            class1 = 'text-danger'
                        }
                        else {
                            class1 = 'text-primary'
                        }
                        return "<span class = " + class1 + ">" + n + " Đơn vị";
                    },
                },
                {
                    field: 'contract_code',
                    title: 'Số hiệu hợp đồng',
                    width:110
                },
                {
                    field: 'province_name',
                    title: 'Tỉnh thành',
                    width: 100
                },
                {
                    field: 'team_name',
                    title: 'Phòng ban',
                    width: 100
                },
                
                {
                    field: 'contract_cost_format',
                    title: 'Giá trị hợp đồng(đ)',
                    width:130
                },
                {
                    field: 'project_state_id',
                    title: 'Trạng thái',
                    width: 90,
                    template: function (data) {
                        debugger
                        var states = {
                            0: { 'title': 'Nháp', 'class': 'kt-badge--warning' }, 
                            1: { 'title': 'Hoàn tất khai báo', 'class': ' kt-badge--success' },
                           
                        };
                        if (data.project_state_id != undefined) {
                            return '<span class="kt-badge ' + states[data.project_state_id].class + ' kt-badge--inline kt-badge--pill"' + 'style="' + states[data.project_state_id].style + '">' + states[data.project_state_id].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới</span>';
                        }
                    },
                    width: 150,
                    textAlign: 'center'
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 90,
                    overflow: 'visible',
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        return `
                             <a href="/project/edit/${data.uuid}" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </a>
                           
                              <button data-id="${data.uuid}" type="button" title="Xóa" class="btn btn-sm btn-danger btn-icon btn-icon-md js-delete-project">
                                <i class="la la-trash"></i>
                           </button>
                                                     
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };
    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };
    var selectEditForm = function () {
        if ($('#field_uuid').val != '' && $('#field_uuid').val != undefined) {
            $('#js-select-field').selectpicker('val', $('#field_uuid').val());
        }
        else {
            $('#js-select-field').selectpicker();
        }

        if ($('#timeSigned').val() != '' && $('#timeSigned').val() != undefined) {
            $('#kt-datepicker-start').datepicker({
                format: 'dd/mm/yyyy',
            }).selectpicker('setdate', $('#timeSigned').val());
        }
        else {
            $('#kt-datepicker-start').datepicker({
                format: 'dd/mm/yyyy',
            });
        }

        if ($('#timeValidStart').val() != '' && $('#timeValidStart').val() != undefined) {
            $('#kt-datepicker-start1').datepicker({
                format: 'dd/mm/yyyy',
            }).selectpicker('setdate', $('#timeValidStart').val());
        }
        else {
            $('#kt-datepicker-start1').datepicker({
                format: 'dd/mm/yyyy',
            });
        }

        if ($('#timeValidEnd').val() != '' && $('#timeValidEnd').val() != undefined) {
            $('#kt-datepicker-end').datepicker({
                format: 'dd/mm/yyyy',
            }).selectpicker('setdate', $('#timeValidEnd').val());
        }
        else {
            $('#kt-datepicker-end').datepicker({
                format: 'dd/mm/yyyy',
            });
        }

        if ($('#proviceID').val() != '' && $('#proviceID').val() != undefined) {
            $('#js-select-location').selectpicker('val', $('#proviceID'));
        }
        else {
            $('#js-select-location').selectpicker();
        }
    }
    var initDetailDrawer = function (id) {
        const title = id ? 'Chi tiết dự án' : '';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--detailProject' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/project/detail/${id}` : '',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var deleteProject = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa dự án...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/project/delete/${id}`,
                method: 'DELETE',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa dự án thành công!', { type: 'success' });

                    if (dataTableInstance) {
                        dataTableInstance.reload();
                    }
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa dự án không thành công!', { type: 'danger' });
                });
        }
    };
    var doneProject = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang chuyển trạng thái...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/project/done/${id}`,
                method: 'GET',
            })
                .done(function () {
                    loading.hide();
                    $.notify('Chuyển trạng thái thành công!', { type: 'success' });
                    window.location = window.location.origin + `/project/edit/${id}`;
                })
                .fail(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(data.responseJSON.msg, { type: 'danger' });
                });
        }
    };
    var initProjectDrawer = function (id) {
        const title = id ? 'Sửa thông tin dự án' : 'Thêm dự án';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--project' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/project/edit/${id}` : '/project/create',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                    $('#js-filter-team1').selectpicker();
                    $('#closeproject').selectpicker();
                   
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-project')
            .on('click', '.js-delete-project', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Dự án sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteProject(id);
                    }
                });
            })
            .off('click', '.js-done-project')
            .on('click', '.js-done-project', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Dự án sẽ được chuyển sang hoàn tất.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        doneProject(id);
                    }
                });
            })
            .off('click', '.js-edit-project')
            .on('click', '.js-edit-project', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
               
                initProjectDrawer(id || '');
               
            })
            //    .off('click', '.js-list-project')
            //    .on('click', '.js-list-project', function () {
            //    const $this = $(this);
            //    const id = $this.attr('data-id');

            //    initProjectDrawer1(id || '');
            //});
            .on('click', '.js-detail-project', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                initDetailDrawer(id || '');
            })
            

    };
    var initForm = function ($form) {
        initValidation(true);
    }
    var initValidation = function (isForm) {
        $(".dmms-project__form").validate({
            rules: {
                'project.name': {
                    required: true,
                    maxlength: 500
                },
                'contract.code': {
                    required: true
                },
                'cost_string': {
                    test:true
                },
                'project.teamUuid': {
                    required:true
                },
                'contract.timeMaintainLoop': {
                    min:0
                },
                'contract.timeMaintainValue': {
                    min:0
                }
            },
            messages: {
                'project.name': {
                    required: 'Vui lòng tên dự án',
                    maxlength: 'Tên dự án không quá 500 kí tự'
                },
                'contract.code': {
                    required:'Vui lòng nhập mã hợp đồng.'
                },
                'cost_string': {
                    required: 'Vui lòng không nhập chữ'
                },
                'project.teamUuid': {
                    required: 'Vui lòng chọn phòng ban'
                },
                'contract.timeMaintainLoop': {
                    min: 'Vui lòng nhập lớn hơn 0'
                    
                },
                'contract.timeMaintainValue': {
                    min: 'Vui lòng nhập lớn hơn 0'
                    
                }

              
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo('.dmms-project__form', -200);
                $('#js-filter-team1').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });
                $('#js-filter-team').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });
            },
            submitHandler: function (form) {
                //$('.createproject').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                $(form).find('button[type="submit"]').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                const data = $(form).serializeJSON() || {};
                var uuid = $('#project_uuid').val();

                $.ajax({
                    url: uuid ? `/project/edit/` : 'project/create',
                    method: uuid ? 'PUT' : 'POST',
                    data,
                })
                    .done(function (data) {
                        $(form).find('button[type="submit"]').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        KTApp.block('.dmms-project__form');
                        const successMsg = uuid ? 'Sửa thông tin thành công!' : 'Thêm dự án thành công!';
                        $.notify(successMsg, { type: 'success' });

                        if (isForm) {
                            window.location = window.location.origin + `/project/edit/${data.data.uuid}`;
                        } else {

                            localStorage['edit_success'] = JSON.stringify({ from: (uuid ? `/project/edit/` : '/project/create'), massage: successMsg });
                            window.location = window.location.origin + "/project";
                        }
                    })
                    .fail(function (data) {
                        $(form).find('button[type="submit"]').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        KTApp.unblock('.dmms-project__form');
                        const errorMsg = uuid ? data.responseJSON.msg + '!' : data.responseJSON.msg + '!';
                        $.notify(errorMsg, { type: 'danger' });
                    });

                return false;
            },
        });
    }
    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-project__form');
        initForm($form);
        $(".js-select-field").change(function () {
            debugger
            if ($(this).val() == "") {
                $('.checkInvestor').remove();
                $('.appendInvestor').append(`<div class="kt-input-icon">
                <input name="contract.InvestorName" class="form-control" placeholder="Chủ đầu tư" />
            </div>`);

            }
                
            
        });
        $(".js-select-unitSignedUuid").change(function () {
            debugger
            if ($(this).val() == "") {
                $('.checkUnitsigned').remove();
                $('.appendUnitsigned').append(`<div class="kt-input-icon">
                <input name="contract.UnitSignedName" class="form-control" placeholder="Đơn vị ký HĐ" />
            </div>`);

            }
                
            
        });
        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

        $('.js-select-field').selectpicker();
        $('.js-select-unitSignedUuid').selectpicker();
        $('#js-filter-customer').selectpicker();
        $('.selectcuspro').selectpicker();
        $('#js-select-field').selectpicker();
        $('#js-select-field1').selectpicker();
        $('#js-select-province').selectpicker();
        $('#js-select-date_expire').selectpicker();
        $('#js-select-date_expire_loop').selectpicker();
        $('#kt-datepicker-start').datepicker({
            format: 'dd/mm/yyyy',
        });
        $('#kt-datepicker-end').datepicker({
            format: 'dd/mm/yyyy',
        });
        $('#createcontract').datepicker({
            format: 'dd/mm/yyyy',
        });
        $("#js-select-date_expire").change(function () {
            debugger
            var a = $('#kt-datepicker-start').val();
            var b = $('#js-select-date_expire').val()
            var new_date = moment(a, "DD/MM/YYYY").add(b, 'months');
            var newdate = new_date.format('DD/MM/YYYY');
            $('.endgame').val(newdate);
        });
        $("#kt-datepicker-start").change(function () {
            debugger
            var a = $('#kt-datepicker-start').val();
            var b = $('#js-select-date_expire').val()
            var new_date = moment(a, "DD/MM/YYYY").add(b, 'months');
            var newdate = new_date.format('DD/MM/YYYY');
            $('.endgame').val(newdate);
        });
        $('.kt-close').on('click', function () {
            debugger
            drawer.close();
        });
    };
   

    var initSearch = function () {
        $('#js-filter-category')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'category_id');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-categoryId')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'categoryId');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-supplierID')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'supplierID');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-customerUuid')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'customerUuid');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-supplier')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'supplier_uuid');
                    START_PAGE = 1;
                }
            })
            .selectpicker();


        $('#js-filter-states')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'state');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-keywordcontract')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            })
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().trim().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });
        $('#js-filter-filed')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().trim().toLowerCase(), 'fieldId');
                    START_PAGE = 1;
                }
            });
        $('#js-filter-province')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().trim().toLowerCase(), 'provinceId');
                    START_PAGE = 1;
                }
            });
        $('#js-filter-team')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().trim().toLowerCase(), 'teamUuid');
                    START_PAGE = 1;
                }
            });

    };
    $('.js-select-field').on('change', function () {
                        debugger
                    if ($(this).val() == "") {
                        $('.editcheckInvestor').remove();
                        $('.appendeditcheckInvestor').append(`
                <input name="contract.InvestorName" class="form-control" placeholder="Chủ đầu tư" />
           `);
                        $('.appendeditcheckInvestor').addClass('kt-input-icon col-9')
                    }
    })
    $('.js-select-field1').on('change', function () {
                        debugger
                    if ($(this).val() == "") {
                        $('.editcheckUnitsigned').remove();
                        $('.appededitcheckUnitsigned').append(`
                <input name="contract.UnitSignedName" class="form-control" placeholder="Đơn vị kí HĐ" />
           `);
                        $('.appededitcheckUnitsigned').addClass('kt-input-icon col-9')
                    }
    })
    return {
        // Public functions
        init: function () {
            initTable();
            initEventListeners();
            initValidation();
            initSearch();
            //$('#js-filter-category').selectpicker();
            //$('#js-filter-supplier').selectpicker();
            //$('#js-filter-states').selectpicker();
            $('#js-filter-category').selectpicker();
            $('.filterpackage').selectpicker();
            $('.filtercustomer').selectpicker();
            $('.js-select-field').selectpicker();
            $('.js-select-field1').selectpicker();
            $('.filtercontract').selectpicker();
            $('.care').selectpicker();
            
            $('.fiterdevice').selectpicker();
            if (window.location.pathname != "/project") {
                selectEditForm();
            }
            $("#js-select-date_expire").change(function () {
                debugger
                var a = $('#kt-datepicker-start1').val();
                var b = $('#js-select-date_expire').val()
                var new_date = moment(a, "DD/MM/YYYY").add(b, 'months');
                var newdate = new_date.format('DD/MM/YYYY');
                $('.endgame').val(newdate);
            });
            $("#kt-datepicker-start1").change(function () {
                debugger
                var a = $('#kt-datepicker-start1').val();
                var b = $('#js-select-date_expire').val()
                var new_date = moment(a, "DD/MM/YYYY").add(b, 'months');
                var newdate = new_date.format('DD/MM/YYYY');
                $('.endgame').val(newdate);
            });
            $('.selectcuspro').selectpicker();
            $("#contract_coststring").on('keyup', function () {
                $("#contract_coststring").unbind('keyup');
                $("#contract_coststring").val('');
                $("#contract_coststring").simpleMoneyFormat();
            })
            $('#js-filter-filed').selectpicker();
            $('#js-filter-team').selectpicker();
            $('#js-filter-province').selectpicker();
            $('#closeproject').selectpicker();
            
            //$('.js-select-field').unbind('change');
            //$('.js-select-field').bind('change', function () {
            //            debugger
            //        if ($(this).val() == "") {
            //            $('.editcheckInvestor').remove();
            //            $('.appendeditcheckInvestor').append(`<div class="kt-input-icon">
            //    <input name="contract.InvestorName" class="form-control" placeholder="Chủ đầu tư" />
            //</div>`);

            //        }
            //    })

        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-project').length) {

        DMMSProject.init();
    }
});
