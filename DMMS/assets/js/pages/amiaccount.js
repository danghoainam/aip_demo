"use strict";
// Class definition

var DMMSAmiAccount = function () {
    // Private functions

    var dataTableInstance = null;
    var drawer = null;  
    // demo initializer
    
    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };
    var initEventListeners = function () {
        $(document)
            
            .off('click', '.js-edit-ami')
            .on('click', '.js-edit-ami', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initAmiDrawer(id || '');
            })
            

    };
  
   
    var initAmiDrawer = function (id) {
        const title = 'Cấu hình Ami_User';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--amiaccount' });
        drawer.on('ready', () => {
            $.ajax({
                url: `sipaccount/AmiUser`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);

                    jQuery.fn.preventDoubleSubmission = function () {
                        $(this).on('submit', function (e) {
                            var $form = $(this);

                            if ($form.data('submitted') === true) {
                                // Previously submitted - don't submit again
                                e.preventDefault();
                            } else {
                                // Mark it so that the next submit can be ignored
                                $form.data('submitted', true);
                            }
                        });

                        // Keep chainability
                        return this;
                    };
                    $('form.dmms-supplier__form').preventDoubleSubmission();
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-sipaccount__form');
        initForm($form);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

      
    };
    var initForm = function ($form) {
        $form.validate({
            rules: {
                ami_user: {
                    required: true,
                },
                ami_password: {
                    required: true,
                },
            },
            messages: {
                ami_user: {
                    required: 'Vui lòng nhập tài khoản.'
                },
                ami_password: {
                    required: 'Vui lòng mật khẩu.'
                }
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {

                const data = $(form).serializeJSON() || {};
                const { id } = data;
                console.log(data, id);
                $.ajax({
                    url:  'sipaccount/AmiUser',
                    method: 'PUT',
                    data,
                })
                    .done(function () {
                        KTApp.block($form);
                        const successMsg = id ? 'Sửa thông tin thành công!' : 'Cấu hình Ami thành công!';
                        $.notify(successMsg, { type: 'success' });

                        /**
                         * Close drawer
                         */
                        drawer.close();

                        reloadTable();
                    })
                    .fail(function () {
                        KTApp.unblock($form);
                        const errorMsg = id ? 'Sửa thông tin không thành công!' : 'Sửa thông tin không thành công!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }

  
    return {
        // Public functions
        init: function () {
           
            initEventListeners();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-sipaccount').length) {
        DMMSAmiAccount.init();
    }
});
