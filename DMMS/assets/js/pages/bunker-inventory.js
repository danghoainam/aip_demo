﻿"use strict";
// Class definition

var DMMSBunker = function () {
    // Private functions

    var dataTableInstance = null;
    var subDataTableInstance = null;
    var drawer = null;
    var subDrawer = null;
    var drawerInner = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;

    $('.bunker-tab-inventory').on('click', function () {
        if (dataTableInstance !== null) dataTableInstance.destroy();
        const storage_uuid = $('#uuid').val();
        dataTableInstance = $('.kt-datatable--inventory').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: `/bunker/getinventoryinbunker/${storage_uuid}`,
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? 0 : pagination.page;
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { date_string } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            date_string: (date_string || $('#kt-datepicker').val()),
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,

            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 30,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: '#',
                    title: 'Nhân viên kiểm kê',
                    template: function (data) {
                        var output = '';
                        if (data.storage_keeper !== null) {
                            output += `<a href="/staff/update/${data.storage_keeper.uuid}" title="Chi tiết nhân viên">
                                ${data.storage_keeper.name}
                            </a>`;
                        }
                        return output;
                    },
                },
                {
                    field: 'description',
                    title: 'Ghi chú',
                    width: 120
                },
                {
                    field: 'time_stock_take_format',
                    title: 'Thời gian kiểm kê',
                    width: 120
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 50,
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        return `<button data-id="${data.uuid}" type="button" title="Chi tiết" class="btn btn-sm btn-primary btn-icon btn-icon-md js-detail-inventory">
                                <i class="la la-info"></i>
                            </button>`;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    });

    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var initSearch = function () {
        $('#kt-datepicker')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val(), 'date_string');
                    START_PAGE = 1;
                }
            }).daterangepicker({
                timePicker: true,

                locale: {
                    cancelLabel: "Hủy",
                    applyLabel: "Xác nhận",
                    format: 'DD/MM/YYYY'
                }
            });
    };

    var addInventoryToBunker = function () {
        const title = 'PHIẾU KIỂM KÊ THIẾT BỊ KHO VẬT TƯ MỚI';
        drawer = new KTDrawer({ title, uuid: 'new', className: 'dmms-drawer--inventory' });

        var storage_uuid = $('#uuid').val();

        drawer.on('ready', () => {
            $.ajax({
                url: `/bunker/addinventory/${storage_uuid}`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);

                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    var detailSuppliesInInventoryInner = function (category_id) {
        const wrapper = $(`.wrapper-fill tr[id="${category_id}"]`);
        var devices = wrapper.find('.list_device input').serializeJSON();

        const states_obj = {
            1: "Tốt",
            2: "Hỏng",
            3: "Không tìm thấy"
        };
        const title = '<span class="text-uppercase">Danh sách thiết bị kiểm kê trong kho</span>';
        subDrawer = new KTDrawer({ title, category_id: category_id || 'new', className: 'dmms-drawer--detailsuppliesininventory' });

        subDrawer.on('ready', () => {
            var response = `<form class="dmms-form-bunker__deviceininventory" onsubmit="return false">
                    <div class="add-detail-supplies_inner">
                        <table class="table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Tên thiết bị</th>
                                    <th scope="col">Serial</th>
                                    <th scope="col">Trạng thái thiết bị</th>
                                </tr>
                            </thead>
                            <tbody class="wrapper-fill">`;
            if (devices.categories) {
                $.each(devices.categories[0].detail, function (index, item) {
                    response += `<tr id="${item.uuid}">
                                            <td>${index + 1}</td>        
                                            <td class="text-left kt-padding-l-10">
                                                <input name="device[][uuid]" value="${item.uuid}" hidden />
                                                <span class="text-primary">${item.prefix_name}</span><br/>
                                                <span>${item.name_display}</span>
                                            </td>
                                            <td>
                                                <input class="form-control" placeholder="Nhập" value="${item.serial}" hidden />
                                                <span>${item.serial}</span>
                                            </td>
                                            <td style="max-width: 180px">
                                                <select class="form-control kt-select-device_inventory" name="device[][state]">`;
                    $.each(states_obj, function (id, item_state) {
                        if (id == item.state_in_inventory) {
                            response += `<option value="${id}" selected>${item_state}</option>`;
                        }
                        else response += `<option value="${id}">${item_state}</option>`;
                    });
                    response += `</select>
                                            </td>
                                        </tr>`;
                });
                response += `</tbody>
                            </table>
                        </div>
                        <div class="text-right kt-margin-t-20">
                            <button type="button" class="btn btn-danger kt-close">
                                Hủy bỏ
                            </button>
                        </div>
                    </form>`;
                const $content = subDrawer.setBody(response);
                initSaveDetailDevice($content, category_id);

                //Đóng drawer
                $('.kt-close').on('click', function () {
                    subDrawer.close();
                })
            }
        });
        subDrawer.open();
    };

    var gotoEmport = function (id) {
        const title = `<span class="text-uppercase">PHIẾU XUẤT KHO THIẾT BỊ - VẬT TƯ ĐẾN XUẤT TIÊU HAO</span>`;

        drawerInner = new KTDrawer({ title, id: `export_${id}` || 'new', className: 'dmms-drawer--aceptimport' });

        const storage_uuid = $('#uuid').val();
        const storage_name = $('#name').val();

        drawerInner.on('ready', () => {

            $.ajax({
                url: `/bunker/exporttoinventory/${id}/${storage_uuid}/${storage_name}`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawerInner.setBody(response);
                    initContentExport($content, id);
                })
                .fail(function () {
                    drawerInner.error();
                });
        });
        drawerInner.open();
    }

    var detailInventoryInBunker = function (inventory_uuid) {
        const title = 'CHI TIẾT KIỂM KÊ';
        drawer = new KTDrawer({ title, inventory_uuid: inventory_uuid || 'new', className: 'dmms-drawer--detailinventory' });

        drawer.on('ready', () => {
            $.ajax({
                url: `/bunker/detailinventory/${inventory_uuid}`,
                method: 'GET',
            })
                .done(function (response) {
                    drawer.setBody(response);
                    $('.detail-supplies_inner').off('click').on('click', function () {
                        const category_id = $(this).attr('data-id');

                        detailSuppliesInInventoryInner(category_id);
                    });

                    //Chỉ hoạt động khi trạng thái kho là kiểm kê
                    if ($('#state').val() != 1) {
                        $('.goto-export').attr('disabled', true);
                    }

                    //Chuyển sang export tiêu hao
                    $('.goto-export').on('click', function () {
                        drawer.close();
                        gotoEmport(inventory_uuid);
                    });

                    //đóng drawer
                    $('.kt-close').on('click', function () {
                        drawer.close();
                    });
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-add-inventory')
            .on('click', '.js-add-inventory', function () {

                addInventoryToBunker();
            })
            .off('click', '.js-detail-inventory')
            .on('click', '.js-detail-inventory', function () {
                const inventory_uuid = $(this).attr('data-id');

                detailInventoryInBunker(inventory_uuid);
            });
    };

    var initForm = function ($form) {
        $form.validate({
            rules: {
                'staff[][uuid]': {
                    required: true
                },
                'staff[][position]': {
                    required: true
                },
                'staff[][role]': {
                    required: true
                },
                'categories[]': {
                    required: true
                },
                //'categories[][real_quantity]': {
                //    required: true
                //}
                //time_stock_take: {
                //    requited: true
                //},
                //creator_uuid: {
                //    requited: true
                //}
            },
            messages: {
                'staff[][uuid]': {
                    required: "Vui lòng chọn nhân viên!",
                },
                'staff[][position]': {
                    required: "Vui lòng nhập chức vụ!"
                },
                'staff[][role]': {
                    required: "Vui lòng nhập vai trò!"
                },
                'categories[]': {
                    required: "Vui lòng nhập danh mục!",
                },
            },
            //errorPlacement: function (error, element) {
            //    if (element.attr('name') == 'categories[][real_quantity]' && element.val() === '') {
            //        $.notify('Vui lòng nhập số lượng thực!', { type: 'warning' });
            //    }
            //},

            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
                $('#kt-select-staff').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });
            },
            submitHandler: function (form) {
                $(form).find('button[type="submit"]').prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading
                var check_quantity_temp = true;
                var check_real_quantity_temp = true;
                const data = $(form).serializeJSON() || {};
                $.each(data.categories, function (index, item) {
                    if (item.real_quantity == '') {
                        check_real_quantity_temp = false;
                    }

                    if (parseInt(item.real_quantity) > parseInt(item.quantity_exist) || parseInt(item.broken_quantity) > parseInt(item.real_quantity)) {
                        check_quantity_temp = false;
                    }
                });

                if (check_real_quantity_temp == true) {
                    if (check_quantity_temp == true) {
                        $.ajax({
                            url: '/bunker/addinventory',
                            method: 'POST',
                            data,
                        })
                            .done(function () {
                                KTApp.block($form);
                                const successMsg = 'Thêm thông tin thành công!';
                                $.notify(successMsg, { type: 'success' });

                                /**
                                 * Close drawer
                                 */
                                drawer.close();

                                reloadTable();
                            })
                            .fail(function (res) {
                                KTApp.unblock($form);
                                $(form).find('button[type="submit"]').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
                                const successMsg = 'Thêm thông tin không thành công!';
                                $.notify(res.responseJSON.msg, { type: 'danger' });
                            });
                    }
                    else {
                        KTApp.unblock($form);
                        $(form).find('button[type="submit"]').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
                        $.notify("Số lượng tồn thực hoặc số lượng hỏng vượt quá số lượng cho phép!", { type: 'warning' });
                    }
                }
                else {
                    KTApp.unblock($form);
                    $(form).find('button[type="submit"]').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
                    $.notify("Vui lòng nhập vào số lượng thực!", { type: 'warning' });
                }

                return false;
            },
        });
    };

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-form-add__inventory');
        initForm($form);

        /*
            Init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

        $('#kt-select-staff').selectpicker();

        //Thay đổi số lượng thực => hiển thị sai lệch
        $('input[name="categories[][real_quantity]"]').on('change', function () {
            const category_id = $(this).attr('data-id');
            const wrapper = $(`tr[id=${category_id}]`);
            const exist_quantity = parseInt(wrapper.find('input[name="categories[][quantity_exist]"]').val());
            const real_quantity = parseInt($(this).val())

            if (real_quantity <= exist_quantity) {
                wrapper.find('input[name="categories[][real_quantity]"]').removeAttr('title');
            }
            else {
                wrapper.find('input[name="categories[][real_quantity]"]').attr('title', `Số lượng thực không được lớn hơn ${exist_quantity}`);
            }
            if ((exist_quantity - real_quantity) >= 0) {
                wrapper.find('input[name="categories[][broken_quantity]"]').attr('max', real_quantity);
                wrapper.find('.deviations_quantity_after').html('').html(real_quantity);
            }
            else {
                wrapper.find('input[name="categories[][broken_quantity]"]').attr('max', 0);
                wrapper.find('.deviations_quantity_after').html('').html(exist_quantity);
            }

            wrapper.find('.deviations_quantity').html('').html(exist_quantity - real_quantity);
        });

        $('input[name="categories[][broken_quantity]"]').on('change', function () {
            const category_id = $(this).attr('data-id');
            const max = parseInt($(this).attr('max'));
            const wrapper = $(`tr[id=${category_id}]`);
            const broken_quantity = parseInt($(this).val())

            if (broken_quantity <= max) {
                wrapper.find('input[name="categories[][broken_quantity]"]').removeAttr('title');
            }
            else {
                wrapper.find('input[name="categories[][broken_quantity]"]').attr('title', `Số lượng hỏng không được lớn hơn ${max}`)
            }
        });

        //done tab thiết bị
        $('.done-tab2').on('change', function () {
            const isCheck = $(this).find('input[type="checkbox"]').prop('checked');

            if (isCheck == true) {
                $('.tab2').html('').append('&nbsp&nbsp<i class="la la-check" />');

                if ($('.done-tab3').find('input[type="checkbox"]').prop('checked') == true && $('.done-tab4').find('input[type="checkbox"]').prop('checked') == true) $('.submit-inventory').prop('disabled', false);
            }
            else {
                $('.tab2').html('');

                $('.submit-inventory').prop('disabled', true);
            }
        });

        //done tab vật tư
        $('.done-tab3').on('change', function () {
            const isCheck = $(this).find('input[type="checkbox"]').prop('checked');

            if (isCheck == true) {
                $('.tab3').html('').append('&nbsp&nbsp<i class="la la-check" />');

                if ($('.done-tab2').find('input[type="checkbox"]').prop('checked') == true && $('.done-tab4').find('input[type="checkbox"]').prop('checked') == true) $('.submit-inventory').prop('disabled', false);
            }
            else {
                $('.tab3').html('');

                $('.submit-inventory').prop('disabled', true);
            }
        });

        //done tab dụng cụ lao động
        $('.done-tab4').on('change', function () {
            const isCheck = $(this).find('input[type="checkbox"]').prop('checked');

            if (isCheck == true) {
                $('.tab4').html('').append('&nbsp&nbsp<i class="la la-check" />');

                if ($('.done-tab2').find('input[type="checkbox"]').prop('checked') == true && $('.done-tab3').find('input[type="checkbox"]').prop('checked') == true) $('.submit-inventory').prop('disabled', false);
            }
            else {
                $('.tab4').html('');

                $('.submit-inventory').prop('disabled', true);
            }
        });

        //Thêm mới raw nhân viên trong kiểm kê
        $('#js-add-staffininventory').off('click').on('click', function () {

            addStaffInInventory();
        });

        //Xem sửa chi tiết của category
        $('.detail-supplies').off('click').on('click', function () {
            const $this = $(this);
            const category_id = $this.attr('data-id');

            detailSuppliesInInventory(category_id);
        });

        //Đóng drawer
        $('.kt-close').on('click', function () {
            drawer.close();
        });
    };

    var addStaffInInventory = function () {
        const wrapper = $('.wrapper-tab-1');
        const staff = wrapper.find('#kt-select-staff');
        const type = wrapper.find('#kt-select-type');
        const role = wrapper.find('#kt-select-role');

        if (staff.val() !== '' && type.val() !== '' && role.val() !== '') {
            wrapper.append(`<div class="row">
                                        <div class="form-group col-4">
                                            <div class="kt-input-icon">
                                                <input class="form-control" placeholder="Tên" value="${staff.find('option:selected').text()}" readonly />
                                                <input name="staff[][uuid]" value="${staff.val()}" type="hidden" />
                                            </div>
                                        </div>
                                        <div class="form-group col-3">
                                            <div class="kt-input-icon">
                                                <input class="form-control" name="staff[][position]" placeholder="Chức vụ" value="${type.val()}" readonly />
                                            </div>
                                        </div>
                                        <div class="form-group col-3">
                                            <div class="kt-input-icon">
                                                <input class="form-control" name="staff[][role]" value="${role.val()}" placeholder="Vai trò" readonly />
                                            </div>
                                        </div>
                                        <div class="form-group ml-auto kt-margin-r-10"><i class="btn btn-danger la la-trash ml-auto remove_field_tab1" data-id="${staff.val()}"></i></div>
                                    </div>`);
            $('#kt-select-staff').find(`option[value="${staff.val()}"]`).attr('disabled', true);
            $('#kt-select-staff').selectpicker('refresh');

            staff.selectpicker('val', '');
            type.val('');
            role.val('');
        }
        else $.notify('Vui lòng điền đầy đủ các trường!', { type: 'danger' });

        $(wrapper).on("click", ".remove_field_tab1", function (e) { //user click on remove text
            e.preventDefault();
            const $this = $(this);
            const staff_uuid = $this.attr('data-id');

            $('#kt-select-staff').find(`option[value="${staff_uuid}"]`).attr('disabled', false);
            $('#kt-select-staff').selectpicker('refresh');
            $this.parent('div').parent('div').remove();
        });
    }

    var detailSuppliesInInventory = function (category_id) {
        const wrapper = $(`.wrapper-fill tr[id="${category_id}"]`);
        var devices = wrapper.find('.list_device input').serializeJSON();

        const storage_uuid = $('#uuid').val();
        const states_obj = {
            1: "Tốt", 2: "Hỏng", 3: "Không tìm thấy"
        };
        const title = '<span class="text-uppercase">Danh sách thiết bị kiểm kê trong kho</span>';
        subDrawer = new KTDrawer({ title, category_id: category_id || 'new', className: 'dmms-drawer--detailsuppliesininventory' });
        var stt = 1;
        subDrawer.on('ready', () => {
            $.ajax({
                url: `/bunker/getdetailsuppliesinbunker/${storage_uuid}/${category_id}`,
                method: 'GET',
                data: { limit: -1 }
            })
                .done(function (data) {
                    var response = `<form class="dmms-form-bunker__deviceininventory" onsubmit="return false">
                    <div class="add-detail-supplies_inner">
                        <table class="table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Tên thiết bị</th>
                                    <th scope="col">Serial</th>
                                    <th scope="col">Trạng thái thiết bị</th>
                                </tr>
                            </thead>
                            <tbody class="wrapper-fill">`;
                    $.each(data.data.items, function (index, item) {
                        response += `<tr id="${item.uuid}">
                                            <td>${stt++}</td>        
                                            <td class="text-left kt-padding-l-10">
                                                <input name="device[][uuid]" value="${item.uuid}" hidden />
                                                <span class="text-primary">${item.prefix_name}</span><br/>
                                                <span>${item.code} - ${item.name}</span>
                                            </td>
                                            <td>
                                                <input class="form-control" placeholder="Nhập" value="${item.serial}" hidden />
                                                <span>${item.serial}</span>
                                            </td>
                                            <td style="max-width: 180px">
                                                <select class="form-control kt-select-device_inventory" name="device[][state]">`;
                        $.each(states_obj, function (id, item_state) {
                            if (devices.device != undefined && devices.device[index].state != undefined) {
                                if (id == devices.device[index].state) {
                                    response += `<option value="${id}" selected>${item_state}</option>`;
                                }
                                else response += `<option value="${id}">${item_state}</option>`;
                            }
                            else response += `<option value="${id}">${item_state}</option>`;
                        });
                        response += `</select>
                                            </td>
                                        </tr>`;
                    });
                    response += `</tbody>
                            </table>
                        </div>
                        <div class="text-right kt-margin-t-20">
                            <button type="submit" class="btn btn-info">
                                Áp dụng
                            </button>
                            <button type="button" class="btn btn-danger kt-close">
                                Hủy bỏ
                            </button>
                        </div>
                    </form>`;
                    const $content = subDrawer.setBody(response);
                    initSaveDetailDevice($content, category_id);

                    $('.kt-close').on('click', function () {
                        subDrawer.close();
                    });
                })
                .fail(function () {
                    subDrawer.error();
                });
        });
        subDrawer.open();
    }

    var initFormDetailDevice = function ($form, category_id) {
        $form.validate({
            rules: {

            },
            messages: {

            },

            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
                //$('#kt-select-staff').on('hide.bs.select', function () {
                //    $(this).trigger("focusout");
                //});
            },
            submitHandler: function (form) {
                const wrapper = $(`.wrapper-fill tr[id=${category_id}]`);
                const quantity_exist = parseInt(wrapper.find(`input[name="categories[][quantity_exist]"]`).val());
                const data = $(form).serializeJSON() || {};
                wrapper.find('.list_device').html('');
                var broken = 0;
                var deviations_quantity = 0;
                if (data != null) {
                    $.each(data.device || [], function (index, item) {
                        if (item.state == 2) {
                            broken++;
                        }
                        else if (item.state == 3) {
                            deviations_quantity++;
                        }
                        wrapper.find(`.list_device`).append(`<input name="device[][uuid]" value="${item.uuid}" hidden />
                                <input name="device[][state]" value="${item.state}" hidden />`);
                    });
                    wrapper.find('.broken_quantity').html(broken);
                    wrapper.find('input[name="categories[][broken_quantity]"]').val(broken);
                    wrapper.find('.real_quantity').html(data.device.length - deviations_quantity);
                    wrapper.find('.deviations_quantity_after').html(data.device.length - deviations_quantity);
                    wrapper.find('input[name="categories[][real_quantity]"]').val(data.device.length - deviations_quantity);
                    wrapper.find('.deviations_quantity').html('').html(deviations_quantity);
                    /**
                    * Close drawer
                    */
                    subDrawer.close();
                }

                return false;
            },
        });
    };

    var initFormDetailDeviceInner = function ($formInner, category_id) {
        $formInner.validate({
            rules: {

            },
            messages: {

            },

            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($formInner, -200);
                //$('#kt-select-staff').on('hide.bs.select', function () {
                //    $(this).trigger("focusout");
                //});
            },
            submitHandler: function (form) {

                const wrapper = $(`.wrapper-fill tr[id=${category_id}]`);
                const money_string = wrapper.find('input[name="detail[][unit_price_string]"]').val();
                const money = money_string != "" ? parseInt(money_string.replace(/,/g, '')) : 0;
                const data = $(form).serializeJSON() || {};
                var quantity_real = 0;
                wrapper.find('.list_device').html('');
                if (data != null) {
                    $.each(data.detail[0].device || [], function (index, item) {
                        if (item.check) {
                            quantity_real++;
                            wrapper.find(`.list_device`).append(`<input name="detail[][device[]]" value="${item.uuid}" hidden />`);
                        }
                    });
                    wrapper.find('input[name="detail[][real_quantity]"]').val(quantity_real);
                    wrapper.find('.real_quantity').html(quantity_real);
                    wrapper.find('.money_string').html(quantity_real * money);
                    $('.money_string').simpleMoneyFormat();

                    /**
                    * Close drawer
                    */
                    subDrawer.close();
                }

                return false;
            },
        });
    }

    var initSaveDetailDevice = function ($content, category_id) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-form-bunker__deviceininventory');
        initFormDetailDevice($form, category_id);
        const $formInner = $content.find('.dmms-form-bunker__devicesexport');
        initFormDetailDeviceInner($formInner, category_id);

        /*
            Init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

        $('.kt-select-device_inventory').selectpicker();

        $('.check-all').on('change', function () {
            const isCheckAll = $(this).find('input[type="checkbox"]').prop('checked');
            var checkboxInBody = $(".wrapper-fill").find('input[type="checkbox"]');

            if (isCheckAll === true) {
                checkboxInBody.prop('checked', true);
            }
            else {
                checkboxInBody.prop('checked', false);
            }
        });
    };

    var initFormExport = function ($form, inventory_uuid) {
        $form.validate({
            rules: {

            },
            messages: {

            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                $.ajax({
                    url: `/bunker/exportsupplies/${inventory_uuid}`,
                    method: 'POST',
                    data,
                })
                    .done(function (data) {
                        KTApp.block($form);
                        $.notify('Thêm thông tin thành công!', { type: 'success' });

                        /**
                         * Close drawer
                         */
                        drawerInner.close();
                        reloadTable();
                    })
                    .fail(function (data) {
                        KTApp.unblock($form);
                        $.notify(data.responseJSON.msg, { type: 'danger' });
                    });

                return false;
            },
        });
    }

    var initContentExport = function ($content, id) {
        const $form = $content.find('.dmms-form-exporttoinventory');
        initFormExport($form, id);

        /*
            Init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

        $('.unit_price').simpleMoneyFormat();

        //Thay đổi số lượng thực => thay đổi tổng tiền
        $('input[name="detail[][real_quantity]"]').on('change', function () {
            const category_id = $(this).attr('data-category');
            const wrapper = $('.wrapper-fill').find(`tr[id="${category_id}"]`);
            const money_string = wrapper.find('input[name="detail[][unit_price_string]"]').val();

            if (money_string != '') {
                const money = parseInt(money_string.replace(/,/g, ''));
                wrapper.find('.money_string').html('').html($(this).val() * money);
                wrapper.find('.money_string').simpleMoneyFormat();
            }
            else wrapper.find('.money_string').html('').html('0');
        });

        //Thay đổi số đơn giá => thay đổi tổng tiền
        $('input[name="detail[][unit_price_string]"]').on('change', function () {
            const category_id = $(this).attr('data-category');
            const wrapper = $('.wrapper-fill').find(`tr[id="${category_id}"]`);
            const money_string = wrapper.find('input[name="detail[][unit_price_string]"]').val();
            const money = parseInt(money_string.replace(/,/g, ''));
            const realQuantity = wrapper.find('input[name="detail[][real_quantity]"]').val();

            if (realQuantity != '' && realQuantity != 0) {
                wrapper.find('.money_string').html('').html(realQuantity * money);
                wrapper.find('.money_string').simpleMoneyFormat();
            }
            else wrapper.find('.money_string').html('').html('0');
        });

        //Xem sửa chi tiết thiết bị
        $('.detail-supplies_inner').off('click').on('click', function () {
            const category_id = $(this).attr('data-category');

            detailDeviceExport(id, category_id)
        });

        //Đóng drawerInner
        $('.kt-close').on('click', function () {
            drawerInner.close();
        });
    };

    var detailDeviceExport = function (id, category_id) {
        const wrapper = $(`.wrapper-fill tr[id="${category_id}"]`);
        var devices = wrapper.find('.list_device input').serializeJSON();
        const title = '<span class="text-uppercase">Danh sách thiết bị kiểm kê trong kho</span>';
        subDrawer = new KTDrawer({ title, category_id: category_id || 'new', className: 'dmms-drawer--detaildevicesexport' });
        var stt = 1;
        subDrawer.on('ready', () => {
            $.ajax({
                async: false,
                url: `/bunker/DetailDeviceInInventoryAjax/${id}/${category_id}`,
                method: 'GET',
                data: { limit: -1 }
            })
                .done(function (data) {
                    var response = `<form class="dmms-form-bunker__devicesexport" onsubmit="return false">
                    <div class="add-detail-supplies_inner">
                        <table class="table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">                                
                                        <div class="kt-checkbox-inline check-all" data-all="false" style="margin-top:-20px">
                                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--light">
                                                <input type="checkbox">
                                                <span></span>
                                            </label>
                                        </div>
                                    </th>
                                    <th scope="col">Tên thiết bị</th>
                                    <th scope="col">Serial</th>
                                    <th scope="col">Thời hạn bảo hành</th>
                                </tr>
                            </thead>
                            <tbody class="wrapper-fill">`;
                    $.each(data.data.items, function (index, item) {
                        if (item.state_in_inventory == 3) {
                            response += `<tr id="${item.uuid}">
                                             <td>
                                                <div class="kt-checkbox-inline" style="margin-top:-20px">
                                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">`;
                            var item_device = devices.detail[0].device;
                            if (item_device.filter(st => st == item.uuid).length > 0) response += '<input name="detail[][device[][check]]" type="checkbox" checked>';
                            else response += '<input name="detail[][device[][check]]" type="checkbox">';
                            response += `<span></span>
                                                    </label>
                                                </div>
                                                <td class="text-left kt-padding-l-10">
                                                <input name="detail[][device[][uuid]]" value="${item.uuid}" hidden />
                                                <span class="text-primary">${item.prefix_name}</span><br/>
                                                <span>${item.code} - ${item.name}</span>
                                            </td>
                                            <td>
                                                <input class="form-control" placeholder="Nhập" value="${item.serial}" hidden />
                                                <span>${item.serial}</span>
                                            </td>
                                            <td>
                                                <input class="form-control" placeholder="Nhập" value="${item.expire_device_string}" hidden />
                                                <span>${item.expire_device_string}</span>
                                            </td>
                                        </tr>`;
                        }
                    });
                    response += `</tbody>
                            </table>
                        </div>
                        <div class="text-right kt-margin-t-20">
                            <button type="submit" class="btn btn-info">
                                Áp dụng
                            </button>
                            <button type="button" class="btn btn-danger kt-close">
                                Hủy bỏ
                            </button>
                        </div>
                    </form>`;
                    const $content = subDrawer.setBody(response);
                    initSaveDetailDevice($content, category_id);
                })
                .fail(function () {
                    subDrawer.error();
                });
        });
        subDrawer.open();
    };

    return {
        // Public functions
        init: function () {
            initSearch();
            initEventListeners();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-bunker').length) {
        DMMSBunker.init();
    }
});