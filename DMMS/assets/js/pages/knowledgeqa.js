"use strict";
// Class definition

var DMMSKnowledge = function () {
    // Private functions

    var dataTableInstance = null;
    var drawer = null;
    var image = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    // demo initializer
    var initTable = function () {
        var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
        if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
            $.notify(editSucessCache.massage, { type: 'success' });
            localStorage.removeItem('edit_success');
        }
        dataTableInstance = $('.kt-datatable').KTDatatable({

            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/Knowledge/getsqa',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page,
                                CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            //Gán các thuộc tính của images{0} vào đối tượng item
                            if (raw.items != null) {
                                (raw.items).forEach(function (e) {

                                    e.path = e.image != null ? e.image.path : null;

                                    return e;


                                })
                            }


                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,
            },

            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 20,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'question',
                    title: 'Tiêu đề câu hỏi',
                }, {
                    field: 'knowledge_base_topic.name',
                    title: 'Chủ đề',
                },
                {
                    field: 'time_created_format',
                    title: 'Ngày tạo',
                    width: 110,

                },
                {
                    field: 'time_last_update_format',
                    title: 'Ngày cập nhật',
                    width: 110,

                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 110,
                    overflow: 'visible',

                    textAlign: 'center',
                    template: function (data) {
                        return `
                           <a data-id="${data.uuid}" title="Chỉnh sửa" class="js-knowledge btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </a>
                            <button data-id="${data.uuid}" title="Xóa" class="js-delete-supplier btn btn-sm btn-danger btn-icon btn-icon-md ml-2">
                                <i class="la la-trash"></i>
                            </button>
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };
    var initSearch = function () {
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            })


    };
    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var deleteSupplier = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa ...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/knowledge/deleteqa/${id}`,
                method: 'DELETE',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa câu hỏi thành công!', { type: 'success' });

                    reloadTable();
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa không thành công!', { type: 'danger' });
                });
        }
    };

    var initSupplierDrawer = function (id) {
        const title = id ? 'Chi tiết câu hỏi câu trả lời' : 'Thêm câu hỏi và trả lời';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--knowledgeqa' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/knowledge/editqa/${id}` : '/knowledge/qa',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    $('.summernote').summernote({
                        dialogsInBody: true,
                        placeholder: '',
                        fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New'],
                        tabsize: 2,
                        height: 200,
                        callbacks: {
                            onImageUpload: function (files, editor, welEditable) {
                                for (var i = 0; i < files.length; i++) {
                                    SendFile(files[i], this);
                                }

                            },
                        },
                    });
                    function SendFile(file, el) {
                        var data = new FormData();
                        data.append("file", file);

                        //CallAjax("/images/upload", data, function (url) {
                        //    debugger

                        //    $(el).summernote('editor.insertImage', url);

                        //}, "post");
                        $.ajax({
                            url: "/images/upload",
                            type: 'POST',
                            data: data,
                            cache: false,
                            dataType: 'json',
                            processData: false, // Don't process the files
                            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                            success: function (data, textStatus, jqXHR) {
                                var url = "";
                                for (var i = 0; i < data.length; i++) {
                                    url = "http://35.240.155.147:1237/" + data[i].path;
                                    $(el).summernote('editor.insertImage', url);
                                }


                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                //Handle errors here


                            }
                        });
                    }
                    initContent($content);

                    jQuery.fn.preventDoubleSubmission = function () {
                        $(this).on('submit', function (e) {
                            var $form = $(this);

                            if ($form.data('submitted') === true) {
                                // Previously submitted - don't submit again
                                e.preventDefault();
                            } else {
                                // Mark it so that the next submit can be ignored
                                $form.data('submitted', true);
                            }
                        });

                        // Keep chainability
                        return this;
                    };
                    $('form.dmms-knowledge__formqa').preventDoubleSubmission();
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var initSupplierDrawer1 = function (id) {
        const title = id ? 'Chi tiết câu hỏi câu trả lời' : 'Thêm câu hỏi và trả lời';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--knowledgeqa' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/knowledge/editqa/${id}` : '/knowledge/createqa',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);

                    $('.summernote').summernote({
                        dialogsInBody: true,
                        placeholder: '',
                        fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New'],
                        tabsize: 2,
                        height: 200,
                        callbacks: {
                            onImageUpload: function (files, editor, welEditable) {
                                for (var i = 0; i < files.length; i++) {
                                    SendFile(files[i], this);
                                }

                            },
                        },
                    });
                    function SendFile(file, el) {
                        var data = new FormData();
                        data.append("file", file);

                        //CallAjax("/images/upload", data, function (url) {
                        //    debugger

                        //    $(el).summernote('editor.insertImage', url);

                        //}, "post");
                        $.ajax({
                            url: "/images/upload",
                            type: 'POST',
                            data: data,
                            cache: false,
                            dataType: 'json',
                            processData: false, // Don't process the files
                            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                            success: function (data, textStatus, jqXHR) {
                                var url = "";
                                for (var i = 0; i < data.length; i++) {
                                    url = "http://35.240.155.147:1237/" + data[i].path;
                                    $(el).summernote('editor.insertImage', url);
                                }


                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                //Handle errors here


                            }
                        });
                    }
                    initContent($content);

                    jQuery.fn.preventDoubleSubmission = function () {
                        $(this).on('submit', function (e) {
                            var $form = $(this);

                            if ($form.data('submitted') === true) {
                                // Previously submitted - don't submit again
                                e.preventDefault();
                            } else {
                                // Mark it so that the next submit can be ignored
                                $form.data('submitted', true);
                            }
                        });

                        // Keep chainability
                        return this;
                    };
                    $('form.dmms-knowledge__formqa').preventDoubleSubmission();
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-supplier')
            .on('click', '.js-delete-supplier', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Nhà cung cấp sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteSupplier(id);
                    }
                });
            })


            .off('click', '.js-edit-knowledgeqacreate')
            .on('click', '.js-edit-knowledgeqacreate', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initSupplierDrawer1(id || '');
            })
            .off('click', '.js-knowledge')
            .on('click', '.js-knowledge', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initSupplierDrawer(id || '');
            });
    };
    var initForm = function ($form) {
        initValidation(true);
    }
    var initValidation = function (isForm) {
        $(".dmms-knowledge__formqa").validate({
            rules: {
                name: {
                    required: true,
                },
                description: {
                    required: true,
                },

            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên nhà cung cấp.'
                },

                description: {
                    required: 'Vui lòng nhập mô tả.'
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('.dmms-knowledge__formqa', -200);
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                console.log(data, uuid);
                $.ajax({
                    url: uuid ? `/knowledge/editqa` : '/knowledge/CreateQa',
                    method: uuid ? 'PUT' : 'POST',
                    data,
                })
                    .done(function () {
                        $(".submit").attr("disabled", true);
                        KTApp.block('.dmms-knowledge__formqa');
                        const successMsg = uuid ? 'Sửa thông tin thành công!' : 'Thêm câu hỏi câu trả lời thành công!';
                        $.notify(successMsg, { type: 'success' });
                        if (isForm) {
                            drawer.close();
                            reloadTable();
                        } else {

                            localStorage['edit_success'] = JSON.stringify({ from: (uuid ? `/supplier/edit/` : '/supplier/create'), massage: successMsg });
                            window.location = window.location.origin + "/supplier";
                        }
                    })
                    .fail(function () {

                        KTApp.unblock('.dmms-knowledge__formqa');
                        const errorMsg = uuid ? 'Sửa thông tin không thành công!' : 'Thêm nhà cung cấp không thành công!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-knowledge__formqa');
        initForm($form);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));
        $('#js-filter-qa').selectpicker({

            size: 4
        });

        /**
         * Init Dropzone
         */
        //initDropzone();
    };


    var avatar = function () {
        image = new KTAvatar('kt_user_avatar');
    }
    var avata = function () {
        $('input[type=file]').on('change', function (event) {
            var files = event.target.files;
            event.stopPropagation(); // Stop stuff happening
            event.preventDefault(); // Totally stop stuff happening

            //Create a formdata object and add the files
            var data = new FormData();
            $.each(files, function (key, value) {
                data.append(key, value);
            });
            $.ajax({
                url: "/images/upload",
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function (data, textStatus, jqXHR) {
                    if (data != null && data.length > 0) {
                        $("#image").val(data[0].id);
                        //$("#imgAvatar").attr('src', "https://api.ngot.club/" + response[0].path);
                    }
                    if (typeof data.error === 'undefined') {
                        //Success so call function to process the form
                        //submitForm(event, data);

                    } else {
                        //Handle errors here
                        alert('ERRORS: ' + textStatus);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //Handle errors here
                    alert('ERRORS: ' + textStatus);

                }
            });
        });
    }

    return {
        // Public functions
        init: function () {
            avatar();
            avata();
            initTable();
            initSearch();
            initEventListeners();
            initValidation();

        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-knowledgeqa').length) {
        DMMSKnowledge.init();
    }
});
