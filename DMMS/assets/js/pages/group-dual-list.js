"use strict";
// Class definition

var DMMSGroupDualList = function () {
    var drawerInstance = null;
    var staffDrawer = null;
    var group_name = '', group_desc = '';
    var renderDualList = function (left, right, module) {
        const options = [];
        $.each(left, (index, { uuid, name, phonenumber }) => {
            const text = [name || ''];
            if (phonenumber) {
                text.push(phonenumber);
            }

            options.push({
                text: text.join(' - ') || '-- No name --',
                value: uuid,
            });
        });

        $.each(right, (index, { uuid, name, phonenumber }) => {
            const text = [name || ''];
            if (phonenumber) {
                text.push(phonenumber);
            }
            options.push({
                text: text.join(' - ') || '-- No name --',
                value: uuid,
                selected: true,
            });
        });

        const optsHtml = [];
        $.each(options, (index, item) => {
            optsHtml.push(`<option value="${item.value}" ${item.selected ? 'selected' : ''}>${item.text}</option>`);
        });

        const $content = drawerInstance.setBody(`
      <select class="kt-dual-listbox" multiple>${optsHtml.join('')}</select>
    `);


        new KTDualListBox($content.find('.kt-dual-listbox')[0], {
            availableTitle: module === 'staff' ? 'Danh sách nhân viên' : 'Danh sách đơn vị',
            selectedTitle: module === 'staff' ? 'Nhân viên trong khu vực' : 'đơn vị trong khu vực',
        });
    };

    var initDrawer = function ({ id, module }) {
        const title = module === 'staff' ? 'Danh sách nhân viên trong khu vực' : 'Danh sách đơn vị trong khu vực';
        drawerInstance = new KTDrawer({
            title,
            id: `group-${module}`,
            onRenderFooter: (callback) => {
                callback(`
          <button data-module="${module}" data-id="${id}" type="button" class="btn btn-primary js-save-group-staff">
            <i class="la la-save"></i>
            Lưu
          </button>
        `);
            },
        });

        drawerInstance.on('ready', () => {
            var left = null;
            var right = null;

            $.ajax({
                url: `/group/${module}-not-in`,
                method: 'GET',
            })
                .done((res) => {
                    if ($.isArray(res)) {
                        left = res;
                    }

                    if (left && right) {
                        renderDualList(left, right, module);
                    }
                })
                .fail((error) => {
                    drawerInstance.error(error);
                });

            $.ajax({
                url: `/group/${module}/${id}`,
                method: 'GET',
            })
                .done((res) => {
                    if ($.isArray(res)) {
                        right = res;
                    }

                    if (left && right) {
                        renderDualList(left, right);
                    }
                })
                .fail((error) => {
                    drawerInstance.error(error);
                });
        });

        drawerInstance.open();
    };

    var saveResult = function ({ id, module }) {
        drawerInstance.block();
        const $drawer = drawerInstance.getElement();
        const selected = $drawer.find('select').val();
        const successMsg = module === 'staff' ? 'Lưu nhân viên thành công!' : 'Lưu đơn vị thành công!';
        const errorMsg = module === 'staff' ? 'Lưu nhân viên không thành công!' : 'Lưu đơn vị không thành công!';
        $.ajax({
            // url: `/group/${module}/${id}`,
            url: `/group/upsert/${id}`,
            method: 'POST',
            data: {
                "request": {
                    "uuid": id,
                    "uuids": selected.join(','),
                    "name": group_name,
                    "description": group_desc,
                    "module": module
                }
            }
        }).done((res) => {
            drawerInstance.unblock();
            drawerInstance.close();
            $.notify(successMsg, { type: 'success' });
        }).fail((res) => {
            drawerInstance.unblock();
            $.notify(errorMsg, { type: 'danger' });
        });
    };

    var initStaffDrawer = function (group_uuid, uuid) {
        const title = 'Thêm nhân viên vào khu vực';
        staffDrawer = new KTDrawer({ title, uuid: uuid || 'new', className: 'dmms-drawer--staff' });

        staffDrawer.on('ready', () => {
            $.ajax({
                url: uuid ? `/group/add-staff-to-group?group_uuid=${group_uuid}&personal_uuid=${uuid}` : `/group/add-staff-to-group?group_uuid=${group_uuid}`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = staffDrawer.setBody(response);
                    initContent($content);
                    
                })
                .fail(function () {
                    staffDrawer.error();
                });
        });

        staffDrawer.open();
    }

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-list-staff')
            .on('click', '.js-list-staff', function () {
                const $this = $(this);
                const groupUuid = $this.attr('data-group-uuid');
                group_name = $this.attr('data-group-name');
                group_desc = $this.attr('data-group-desc');
                initStaffDrawer(groupUuid, '');
            })
            .off('click', '.js-list-customer')
            .on('click', '.js-list-customer', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                group_name = $this.attr('data-group-name');
                group_desc = $this.attr('data-group-desc');
                initDrawer({
                    id,
                    module: 'customers',
                });
            })
            .off('click', '.js-save-group-staff')
            .on('click', '.js-save-group-staff', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                saveResult({
                    id,
                    module: $this.attr('data-module')
                });
            })
            .off('click', '.js-delete-staff-group')
            .on('click', '.js-delete-staff-group', function () {
                const $this = $(this);
                const personal_uuid = $this.attr('data-personal-uuid');
                const group_uuid = $this.attr('data-group-uuid');

                $.ajax({
                    url: `/group/delete-user`,
                    method: 'POST',
                    data: {
                        personal_uuid,
                        group_uuid
                    }
                }).done((res) => {
                    staffDrawer.unblock();
                    staffDrawer.close();
                    $.notify('Xóa nhân viên khỏi khu vực thành công', { type: 'success' });
                }).fail((res) => {
                    staffDrawer.unblock();
                    $.notify('Không xóa được nhân viên khỏi khu vực', { type: 'danger' });
                });
            });
    };

    var initStaffForm = function ($form) {
        $form.validate({
            rules: {
                staff_uuid: {
                    required: true,
                },
            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên nhân viên.'
                }
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                const { group_uuid, staff_uuid } = data;
                $.ajax({
                    url: `/group/add-new-user?group_uuid=${group_uuid}&personal_uuid=${staff_uuid}`,
                    method: 'POST'
                })
                .done(function (res) {
                    const successMsg = 'Thêm nhân viên vào khu vực thành công!';
                    if (res != null && res.error == 1) {
                        if (res.msg) {
                            $.notify(res.msg, { type: 'danger' });
                        }
                        else {
                            $.notify("Có lỗi xảy ra. Vui lòng kiểm tra lại dữ liệu", { type: 'danger' });
                        }
                    }
                    else {
                        KTApp.block($form);
                        $.notify(successMsg, { type: 'success' });
                        staffDrawer.close();
                        //reloadTable();
                    }
                })
                .fail(function () {
                    KTApp.unblock($form);
                    const errorMsg = 'Thêm nhân viên vào khu vực không thành công!';
                    $.notify(errorMsg, { type: 'danger' });
                });
                return false;
            },
        });
        $("#group-title").text(group_name);
    }

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-add-staff-to-group__form');
        initStaffForm($form);
        $("#kt-select-staff").selectpicker();
        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

    };

    return {
        // Public functions
        init: function () {
            initEventListeners();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-group').length) {
        DMMSGroupDualList.init();
    }
});
