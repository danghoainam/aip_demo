"use strict";
// Class definition

var DMMSKnowledge = function () {
    // Private functions

    var dataTableInstance = null;
    var drawer = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    var json = "";
    
    // demo initializer
    var initTable = function () {
        var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
        if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
            $.notify(editSucessCache.massage, { type: 'success' });
            localStorage.removeItem('edit_success');
        }
        dataTableInstance = $('.kt-datatable').KTDatatable({

            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/ManageFiled/gets',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page,
                                CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            //Gán các thuộc tính của images{0} vào đối tượng item
                            if (raw.items != null) {
                                (raw.items).forEach(function (e) {

                                    e.path = e.image != null ? e.image.path : null;

                                    return e;


                                })
                            }


                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,
            },

            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 35,
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    },
                    textAlign: 'center'
                },
                {
                    field: 'name',
                    title: 'Tên lĩnh vực',
                    width: 190,
                    

                },
                {
                    field: 'description',
                    title: 'Mô tả lĩnh vực',


                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 110,
                    overflow: 'visible',
                    textAlign: 'center',
                    template: function (data) {
                        return `
                           <a href="/managefiled/edit/${data.id}" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </a>
                            <button data-id="${data.id}" title="Xóa" class="js-delete-field btn btn-sm btn-danger btn-icon btn-icon-md ml-2">
                                <i class="la la-trash"></i>
                            </button>
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };
    var initSearch = function () {
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            })
        $('#js-filter-type')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'type');
                    START_PAGE = 1;
                }
            })
        $('#js-filter-keyword1')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            })


    };
    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var deleteField = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/ManageFiled/delete/${id}`,
                method: 'DELETE',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa lĩnh vực thành công!', { type: 'success' });

                    reloadTable();
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa không thành công!', { type: 'danger' });
                });
        }
    };

    var arr = []
    var initCreateManageDrawer = function (id) {
        debugger
        const title = 'Thêm mới lĩnh vực';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--managefiled' });

        drawer.on('ready', () => {
            $.ajax({
                url: '/ManageFiled/create',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    $('#chkSelectAll').change(function () {
                        debugger
                        if ($('#chkSelectAll').is(":checked")) {
                            $("#kt_tree_4").jstree("check_all");
                        }
                        else {
                            $("#kt_tree_4").jstree("uncheck_all");
                        }
                    });

                    $("#kt_tree_4").jstree({
                        "checkbox": {
                            "keep_selected_style": false,
                            "tie_selection": false,
                            'three_state': false,
                            'cascade': 'down',

                        },

                        "core": {
                            "animation": 0,
                            'check_callback': true,
                            'data': json,
                            "themes": { "variant": "large" },

                        },

                        "plugins": ["checkbox"]
                    });
                    $("#kt_tree_4").on("check_node.jstree", function (e, data) {
                        debugger
                         arr = [];
                        var selectedNodes = $('#kt_tree_4').jstree().get_checked();
                        $.each(selectedNodes, function (index, item) {
                            if (item.indexOf('j') < 0 && item.indexOf('t') < 0 && item.indexOf('d') < 0) {
                                arr.push(item);
                            }
                        })
                        console.log(arr);
                        if (e.type == "uncheck_node") {
                            $("#chkSelectAll").prop("checked", false);
                        }
                        else if (e.type == "check_node") {
                            if ($(this).jstree().get_json('#', { flat: true }).length === $(this).jstree().get_checked(true).length)
                                $("#chkSelectAll").prop("checked", true);
                        }
                    });
                    $("#kt_tree_4").on("uncheck_node.jstree", function (e, data) {
                         arr = [];
                        var selectedNodes = $('#kt_tree_4').jstree().get_checked();
                        $.each(selectedNodes, function (index, item) {
                            if (item.indexOf('j') < 0 && item.indexOf('t') < 0 && item.indexOf('d') < 0) {
                                arr.push(item);
                            }
                        })
                        if (e.type == "uncheck_node") {
                            $("#chkSelectAll").prop("checked", false);
                        }
                        else if (e.type == "check_node") {
                            if ($(this).jstree().get_json('#', { flat: true }).length === $(this).jstree().get_checked(true).length)
                                $("#chkSelectAll").prop("checked", true);
                        }
                    });
                  
                    //Lấy id đã được selected
                    //$("#jstree_demo").jstree('get_selected')
                    $.ajax({
                        url: '/TreeCategories/SubCategories',
                        method: 'POST',
                        data: {
                            category_type: 0,
                            limit: -1,
                            level: 0,
                        },
                    })
                        .done(function (data) {
                            debugger
                            $.each(data, function (index, item) {
                                if (item.type == 1) {
                                    item.children = "xx";
                                    $('#kt_tree_4').jstree(true).create_node("thietbi", item, 'last');
                                }
                                else if (item.type == 2) {
                                    item.children = "xx";
                                    $('#kt_tree_4').jstree(true).create_node("vattu", item, 'last');
                                }
                                else if (item.type == 3) {
                                    item.children = "xx";
                                    $('#kt_tree_4').jstree(true).create_node("dungcu", item, 'last');
                                }
                            })
                        })
                        .fail(function (data) {

                            const errorMsg = 'Có lỗi xảy ra';
                            $.notify(errorMsg, { type: 'danger' });
                        });
                    //$('#kt_tree_4').on("check_node.jstree", function (e, data) {
                    //    debugger
                    //    alert('xxx')
                    //});
                    $('#kt_tree_4').on("open_node.jstree", function (e, data) {
                        debugger
                        if (data.node.original.type != null && data.node.id != "thietbi" && data.node.id != "vattu" && data.node.id != "dungcu") {
                            debugger
                            $('#kt_tree_4').jstree(true).delete_node(data.node.children);
                            $.ajax({
                                url: '/TreeCategories/SubCategories',
                                method: 'POST',
                                data: {
                                    category_type: data.node.original.type,
                                    limit: -1,
                                    level: 0,
                                    parent_id: data.node.id,
                                },
                            })
                                .done(function (data) {
                                    $.each(data, function (index, item) {
                                        item.children = 'xx';
                                        $('#kt_tree_4').jstree(true).create_node(data[0].parent_id, item, 'last');
                                    })
                                })
                                .fail(function (data) {

                                    const errorMsg = 'Có lỗi xảy ra';
                                    $.notify(errorMsg, { type: 'danger' });
                                });
                        }
                    });
                    
                    console.log(json)




                    initContent($content);

                    jQuery.fn.preventDoubleSubmission = function () {
                        $(this).on('submit', function (e) {
                            var $form = $(this);

                            if ($form.data('submitted') === true) {
                                // Previously submitted - don't submit again
                                e.preventDefault();
                            } else {
                                // Mark it so that the next submit can be ignored
                                $form.data('submitted', true);
                            }
                        });

                        // Keep chainability
                        return this;
                    };
                    $('form.dmms-managefield__form').preventDoubleSubmission();
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var initForm = function ($form) {
        initValidation(true);
    }
    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-field')
            .on('click', '.js-delete-field', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Lĩnh vực sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteField(id);
                    }
                });
            })
            .off('click', '.js-create-managefield')
            .on('click', '.js-create-managefield', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initCreateManageDrawer(id || '');
            });
    };
    var initForm = function ($form) {
        debugger
        initValidation(true);
    }

    var initValidation = function (isForm) {
        $(".dmms-managefield__form").validate({
            rules: {
                name: {
                    required: true,
                },

            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên lĩnh vực.'
                },


            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('.dmms-managefield__form', -200);
            },
            submitHandler: function (form) {
                $(form).find('button[type="submit"]').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

                debugger
                $(".submit").attr("disabled", true);
                const data = $(form).serializeJSON() || {};
                data.categoryId = arr;
                const { id } = data;
                $.ajax({
                    url: id ? '/ManageFiled/edit' : '/ManageFiled/create' ,
                    method: id? 'PUT': 'POST',
                    data,
                })
                    .done(function () {
                        $(".submit").attr("disabled", false);
                        KTApp.block('.dmms-knowledge__form');
                        const successMsg = id ? 'Sửa thông tin thành công!' : 'Thêm lĩnh vực thành công!';
                        $.notify(successMsg, { type: 'success' });
                        
                        if (isForm) {
                            drawer.close();
                            reloadTable();
                        }
                        else {
                            window.location = window.location.origin + "/ManageFiled";
                        }
                    })
                    .fail(function () {
                        $(form).find('button[type="submit"]').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);

                        KTApp.unblock('.dmms-managefield__form');
                        const successMsg = id ? 'Sửa thông tin thành công!' : 'Thêm lĩnh vực thành công!';
                        $.notify(successMsg, { type: 'danger' });


                    });
                return false;
            },
        });
    }

    $(".linkfiled").on("click", function () {
        debugger
        var id = document.getElementById("id").value;

        dataTableInstance = $('.kt-datatablefiled').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {

                        url: `/ManageFiled/inittable/${id}`,
                    },

                    response: {

                        map: function (res) {
                            debugger
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? undefined : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const { keyword,type } = query;
                        const pagination = data.pagination || {};


                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: $('#js-filter-keyword1').val().trim(),
                            type: $('#js-filter-type').val().trim(),
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,

            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 30,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
              
                {
                    field: 'name',
                    title: 'Tên thiết bị - vật tư',
                    template: function (data) {
                        debugger
                        var output = '<strong>' + data.prefix_name + '</strong>';
                        if (data.name) {
                            output += '<br/>' + data.name + '</span>';
                        }
                        return output;
                    }

                },
               
                {
                    field: 'type',
                    title: 'Phân loại',
                    width: 90,
                    textAlign:'center',
                    template: function (data) {
                        debugger
                        var ouput = ""
                           
                        if (data.type == 1) {
                            ouput = "Thiết bị"
                        }
                        else if (data.type == 2) {
                            ouput = "Vật tư"
                        }
                        else if (data.type == 3) {
                            ouput = "Dụng cụ lao động"
                        }
                        return ouput;
                    }
                },
            ],
            layout: {
                scroll: !0,
                height: null,
                footer: !1
            },
            sortable: false,
            pagination: true,
            responsive: true,
        });
    });
    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-listsupplies__form');
        initForm($form);


        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));


        /**
         * Init Dropzone
         */
        //initDropzone();
    };


    var getJsonTree = function () {
        $.ajax({
            async: true,
            type: "GET",
            url: "/TreeCategories/gets",
            type: "Get",
            dataType: "json",
            success: function (data) {
                debugger
                json = data;
                $.each(json, function (index, item) {
                    if (item.id == 100) {
                        item.id = "thietbi"
                    }
                    else if (item.id == 101) {
                        item.id = "vattu"
                    }
                    else if (item.id == 102) {
                        item.id = "dungcu"
                    }
                })
                console.log(json)
            },

            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });

    }
    $('#js-filter-type').selectpicker();
    return {
        // Public functions
        init: function () {
            initTable();
            initSearch();
            initEventListeners();
            initValidation();
            getJsonTree();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-managefiled').length) {
        DMMSKnowledge.init();
    }
});
