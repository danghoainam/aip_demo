﻿"use strict";
//Class definition
var DMMSTeam = function () {

    var drawer = null;
    var _this = {};
    var team = {
        $container: $('#js-display-team'),
        $title: $('#js-title-2'),

        load: function () {
            KTApp.block(this.$container);
            $.ajax({
                url: '/teams',
                method: 'GET',
                data: {
                    keyword: $("#js-filter-keyword").val(),
                },
            }).done((res) => {
                KTApp.unblock(this.$container);
                if (res && res.data) {
                    this.render(res.data || {});
                }
            }).fail(() => {
                KTApp.unblock(this.$container);
                $.notify('Không kết nối được đến máy chủ', { type: 'danger' });
            });
        },

        render: function (data) {
            var html = [];
            var firstTeamUUID = null;
            $.each(data.items || [], function (index, item) {
                if (index === 0) {
                    firstTeamUUID = item.uuid;
                }
                html.push('<div class="kt-team">');
                html.push(`
                    <div data-name="${item.name}" data-uuid="${item.uuid}" class="kt-team__title d-flex align-items-center js-team ">
                        <i class="flaticon2-group mr-2"></i>
                        ${item.name}
                        <div class="ml-auto">
                            <a data-uuid="${item.uuid}" class="kt-team__btn text-primary js-edit-team">
                                <i class="flaticon2-edit"></i>
                            </a>
                            <a data-uuid="${item.uuid}" data-name="${item.name}" class="kt-team__btn text-danger js-delete-team">
                                <i class="flaticon2-trash"></i>
                            </a>
                        </div>
                    </div>
                `);
                html.push('<div class="kt-team__list">');
                $.each(item.staff || [], function (index, staff) {
                    html.push(`
                        <div data-name="${staff.name}" data-uuid="${staff.uuid}" class="kt-team__item d-flex align-items-center js-staff">
                            <i class="flaticon-users mr-2"></i>
                            ${staff.name}
                            <div class="ml-auto">
                                <a data-uuid="${staff.uuid}" class="text-primary js-lead-staff kt-team__btn">
                                    <i class="flaticon2-cup"></i>
                                </a>
                                <a data-uuid="${staff.uuid}" class="text-danger js-remove-staff kt-team__btn">
                                    <i class="flaticon2-fast-next"></i>
                                </a>
                            </div>
                        </div>
                    `);
                });
                html.push('</div>');
                html.push('</div>');
            });

            this.$container.html(html.join(''));

            this.select(firstTeamUUID);
        },

        deleteTeam: function (uuid) {
            if (uuid) {
                var loading = new KTDialog({
                    'type': 'loader',
                    'placement': 'top center',
                    'message': 'Đang xóa đội-nhóm...'
                });
                KTApp.blockPage();
                $.ajax({
                    url: `/team/delete/${uuid}`,
                    method: 'DELETE',
                })
                    .done(function (data) {
                        loading.hide();
                        KTApp.unblockPage();
                        $.notify(data.msg, { type: 'success' });

                        team.load();
                    })
                    .fail(function (data) {
                        loading.hide();
                        KTApp.unblockPage();
                        $.notify(data.responseJSON.msg, { type: 'danger' });
                    });
            }
        },

        select: function (uuid) {
            const $team = $(`.js-team[data-uuid="${uuid}"]`);

            const $parent = $team.parent('.kt-team');
            $parent.find('.active').removeClass('active');
            const $active = $parent.siblings('.active');
            $active.removeClass('active').find('.active').removeClass('active');

            const name = $team.attr('data-name');
            this.$title.html(name);

            $parent.addClass('active');
            $team.addClass('active');

            _this.team_uuid = uuid;
            _this.staff_uuid = null;
        },

        selectStaff: function (uuid) {
            const $team = $(`.js-staff[data-uuid="${uuid}"]`);

            const $active = $team.siblings('.active');
            $active.removeClass('active');

            const name = $team.attr('data-name');
            this.$title.html(name);

            $team.addClass('active');

            _this.team_uuid = null;
            _this.staff_uuid = uuid;
        },

        add: function (params) {
            KTApp.block(this.$container);
            KTApp.block(staff.$container);
            $.ajax({
                url: '/team/add-staff',
                method: 'POST',
                data: params || {},
            }).done((res) => {
                KTApp.unblock(this.$container);
                if (res && res.code === 0) {
                    team.load();
                    staff.load();
                } else {
                    KTApp.unblock(staff.$container);
                    KTApp.unblock(this.$container);
                }
            }).fail(() => {
                KTApp.unblock(this.$container);
                KTApp.unblock(staff.$container);
                $.notify('Thêm nhân viên không thành công!', { type: 'danger' });
            });
        },

        remove: function (params) {
            KTApp.block(this.$container);
            KTApp.block(staff.$container);
            $.ajax({
                url: '/team/remove-staff',
                method: 'POST',
                data: params || {},
            }).done((res) => {
                if (res && res.code === 0) {
                    team.load();
                    staff.load();
                } else {
                    KTApp.unblock(staff.$container);
                    KTApp.unblock(this.$container);
                }
            }).fail(() => {
                KTApp.unblock(this.$container);
                KTApp.unblock(staff.$container);
                $.notify('Xóa nhân viên không thành công!', { type: 'danger' });
            });
        },
    };

    var staff = {
        $container: $('#js-team-2'),
        
        load: function () {
            KTApp.block(this.$container);
            $.ajax({
                url: "/team/staff-not-in",
                method: 'GET',
                data: {
                    keyword: $("#js-filter-staff").val()
                }
            }).done((res) => {
                KTApp.unblock(this.$container);

                this.render(res || {});
            }).fail(() => {
                KTApp.unblock(this.$container);
                $.notify('Không kết nối được đến máy chủ', { type: 'danger' });
            });
        },

        render: function (data) {
            var html = [];
            $.each(data || [], function (index, item) {
                html.push('<div class="kt-team kt-team--staff">');
                html.push('<div class="kt-team__list">');
                    html.push(`
                        <div data-uuid="${item.uuid}" class="kt-team__item d-flex align-items-center">
                            ${item.name}
                            <button data-uuid="${item.uuid}" class="kt-team__btn text-primary js-add-staff ml-auto">
                                <i class="flaticon2-fast-back"></i>
                            </button>
                        </div>
                    `);
                html.push('</div>');
                html.push('</div>');
            });

            this.$container.html(html.join(''));
        },

        select: function () { },
    };

    var initTeamDrawer = function (uuid) {
        const title = uuid ? 'Sửa thông tin đội nhóm' : 'Thêm mới đội nhóm';
        drawer = new KTDrawer({ title, uuid: uuid || 'new', className: 'dmms-drawer--team' });

        drawer.on('ready', () => {
            $.ajax({
                url: uuid ? `/team/edit/${uuid}` : 'team/create',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-team')
            .on('click', '.js-delete-team', function () {
                const uuid = $(this).attr('data-uuid');
                const name = $(this).attr('data-name');
                $.ajax({
                    url: "/customers-managed-by-team",
                    method: "GET",
                    data: {
                        team_uuid: uuid,
                    },
                }).done((res) => {
                    KTApp.swal({
                        title: res.data.items != '' ? `Đội [${name}] đang quản lý ${res.data.items.length} [đơn vị triển khai]` : `Đội [${name}] sẽ được xóa` ,
                        text: 'Xác nhận xóa',
                        icon: 'warning',
                        dangerMode: true,
                    }).then((confirm) => {
                        if (confirm) {
                            team.deleteTeam(uuid);
                        }
                    });
                }).fail(() => {
                    $.notify('Không kết nối được đến máy chủ', { type: 'danger' });
                });
            })
            .off('click', '.js-edit-team')
            .on('click', '.js-edit-team', function () {
                const $this = $(this);
                const uuid = $this.attr('data-uuid');

                initTeamDrawer(uuid || '');
            })
            .off('click', '.js-team')
            .on('click', '.js-team', function () {
                const uuid = $(this).attr('data-uuid');
                if (uuid) {
                    $("#js-staff-uuid").val('');
                    $("#js-team-uuid").val(uuid);
                    team.select(uuid);
                }
            })
            .off('click', '.js-staff')
            .on('click', '.js-staff', function () {
                const uuid = $(this).attr('data-uuid');
                if (uuid) {
                    $("#js-staff-uuid").val(uuid);
                    $("#js-team-uuid").val('');
                    team.selectStaff(uuid);
                }
            })
            .off('click', '.js-add-staff')
            .on('click', '.js-add-staff', function () {
                const uuid = $(this).attr('data-uuid');
                if (uuid) {
                    const params = {
                        staff_uuid: uuid,
                    };

                    params.team_uuid = _this.team_uuid;

                    team.add(params);
                }
            })
            .off('click', '.js-remove-staff')
            .on('click', '.js-remove-staff', function () {
                const uuid = $(this).attr('data-uuid');
                if (uuid) {
                    const params = {
                        staff_uuid: uuid,
                    };

                    team.remove(params);
                }
            })
            .off('change', '#js-filter-staff')
            .on('change', '#js-filter-staff', function () {
                if ($(this).val() !== '') {
                    staff.load();
                }
            })
            .off('change', '#js-filter-keyword')
            .on('change', '#js-filter-keyword', function () {
                if ($(this).val() !== '') {
                    team.load();
                }
            });

    };

    var initForm = function ($form) {
        $form.validate({
            rules: {
                name: {
                    required: true,
                },
            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên Đội-nhóm.'
                }
            },

            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                $.ajax({
                    url: uuid ? `/team/edit/${uuid}` : 'team/create',
                    method: uuid ? 'PUT' : 'POST',
                    data,
                })
                    .done(function () {
                        KTApp.block($form);
                        const successMsg = uuid ? 'Sửa thông tin đội-nhóm thành công!' : 'Thêm đội-nhóm thành công!';
                        $.notify(successMsg, { type: 'success' });

                        /**
                         * Close drawer
                         */
                        drawer.close();

                        team.load();
                    })
                    .fail(function () {
                        KTApp.unblock($form);
                        const errorMsg = uuid ? 'Sửa thông tin đội-nhóm không thành công!' : 'Thêm đội-nhóm không thành công!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    };


    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-team__form');
        initForm($form);

        /*
            Init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));
    };


    return {
        // Public functions
        init: function () {
            team.load();
            staff.load();
            initEventListeners();
        },
    };
}();
$(document).ready(function () {
    if ($('.dmms-team').length) {
        DMMSTeam.init();
    }
});