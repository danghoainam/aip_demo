"use strict";
// Class definition

var DMMSProjectCus = function () {
    // Private functions

    var dataTableInstancecontract = null;
    var drawer = null;
    var LIMIT = 20;
    var START_PAGE = 0;
    var CURRENT_PAGE = 1;
    // demo initializer

    //var reloadTable = function () {
    //    if (dataTableInstance) {
    //        dataTableInstance.reload();
    //    }
    //};
    //var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
    //if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
    //    $.notify(editSucessCache.massage, { type: 'success' });
    //    localStorage.removeItem('edit_success');
    //}
  
    var dataCustom = null;
    var initPackageContractDrawer = function (project, contract) {
        
        const title = 'TẠO HỢP ĐỒNG GIA HẠN CHO ĐƠN VỊ TRIỂN KHAI'
        drawer = new KTDrawer({ title, project: project || 'new', className: 'dmms-drawer--contractproject' });

        drawer.on('ready', () => {
            $.ajax({
                url: project ? `/project/CreateContract/${project}/''` : '',
                method: 'GET',

            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    dataCustom = response ? contract.data : null;
                    $(document).ready(function () {
                        $('.addinputcontract').click(function (e) {
                            
                            e.preventDefault();
                            //if ($('.controlSelect').val() == "" ){
                            //    $('.test').append(`Thêm đủ dữ liệu`);
                            //    return;
                            //}
                            initControlSelect(dataCustom)
                            $('.bootstrap-select').selectpicker();
                        });
                    });
                    $('.addcontract').on("click", ".deleteinput", function (e) { //user click on remove text
                        e.preventDefault();
                        $(this).parent('div').remove();
                        $('.bootstrap-select').selectpicker('refresh');
                    });
                    $('#timeselect').selectpicker();
                    $('#kt-select-customer1').selectpicker();
                    $('.datecomplete').datepicker({ format: 'dd/mm/yyyy' }).datepicker('setDate', new Date());
                    
                    $('.datesigned').datepicker({ format: 'dd/mm/yyyy' }).datepicker('setDate', new Date());
                    
                    $("#timeinput").change(function () {
                        debugger
                        var a = $('#timeinput').val();
                        var b = $('#timeselect').val()
                        var new_date = moment(a, "DD/MM/YYYY").add(b, 'months');
                        var newdate = new_date.format('DD/MM/YYYY');
                        $('.endgame').val(newdate);
                    });
                    $("#timeselect").change(function () {
                        debugger
                        var a = $('#timeinput').val();
                        var b = $('#timeselect').val()
                        var new_date = moment(a, "DD/MM/YYYY").add(b, 'months');
                        var newdate = new_date.format('DD/MM/YYYY');
                        $('.endgame').val(newdate);
                    });
                    initContent($content);

                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();

    };
    var initPackageContractUpdateDrawer = function (subcontract, project) {
        
        const title = 'CHỈNH SỬA HỢP ĐỒNG GIA HẠN CHO ĐƠN VỊ TRIỂN KHAI'
        drawer = new KTDrawer({ title, subcontract: subcontract || 'new', className: 'dmms-drawer--contractproject' });

        drawer.on('ready', () => {
            $.ajax({
                url: subcontract ? `/project/UpdateContract/${subcontract}/${project}` : '',
                method: 'GET',

            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    dataCustom = response ? contract.data : null;
                    $(document).ready(function () {
                        $('.addinputcontract').click(function (e) {
                            
                            e.preventDefault();
                            //if ($('.controlSelect').val() == "" ){
                            //    $('.test').append(`Thêm đủ dữ liệu`);
                            //    return;
                            //}
                            initControlSelect(dataCustom)
                            $('.bootstrap-select').selectpicker();
                        });
                    });
                    $('.addcontract').on("click", ".deleteinput", function (e) { //user click on remove text
                        e.preventDefault();
                        $(this).parent('div').remove();
                        $('.bootstrap-select').selectpicker('refresh');
                    });
                    $('.datesignedupdate').datepicker({ format: 'dd/mm/yyyy' });
                    $('.datecompleteupdate').datepicker({ format: 'dd/mm/yyyy' });
                    $('#timeselect').selectpicker();
                    $('#kt-select-customer1').selectpicker();
                    $('.datecomplete').datepicker({ format: 'dd/mm/yyyy' }).datepicker('setDate', new Date());
                    $('.datesigned').datepicker({ format: 'dd/mm/yyyy' }).datepicker('setDate', new Date());
                    $("#timeinput").change(function () {
                        debugger
                        var a = $('#timeinput').val();
                        var b = $('#timeselect').val()
                        var new_date = moment(a, "DD/MM/YYYY").add(b, 'months');
                        var newdate = new_date.format('DD/MM/YYYY');
                        $('.endgame').val(newdate);
                    });
                    $("#timeselect").change(function () {
                        debugger
                        var a = $('#timeinput').val();
                        var b = $('#timeselect').val()
                        var new_date = moment(a, "DD/MM/YYYY").add(b, 'months');
                        var newdate = new_date.format('DD/MM/YYYY');
                        $('.endgame').val(newdate);
                    });
                    initContent($content);

                })
                .fail(function (response) {
                    debugger
                    drawer.error(response.responseJSON);
                });
        });

        drawer.open();

    };
    var initEventListeners = function () {
        $('body')

          
            .off('click', '#contract')
            .on('click', '#contract', function () {
                
                const $this = $(this);
                const project = $('#project_uuid').val();
                        const contract = null;
                        initPackageContractDrawer(project, contract || '', contract);
            })
            .off('click', '.js-update-contract')
            .on('click', '.js-update-contract', function () {
                
                const $this = $(this);
                const project = $('#project_uuid').val();
                var subcontract = $(this).attr('data-id');
                initPackageContractUpdateDrawer(subcontract, project);
            })
            .off('click', '.js-lock-subcontract')
            .on('click', '.js-lock-subcontract', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Bạn có muốn xóa hợp đồng gia hạn.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        Locksubcontract(id);
                    }
                });
            })


    };
    var Locksubcontract = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa hợp đồng gia hạn...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/project/CloseSubContract/${id}`,
                method: 'DELETE',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa hợp đồng thành công!', { type: 'success' });

                    if (dataTableInstancecontract) {
                        dataTableInstancecontract.reload();
                    }
                })
                .fail(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(data.responseJSON.msg +'!', { type: 'danger' });
                });
        }
    };
    var initControlSelect = function (data) {
        //if (data != null && data.length > 0) {
        //Thực hiện lấy danh sách các gói đã select

        var lstItem = $(".controlSelect"),
            arrItem = [],
            //Dánh sách lưu các id được seclect
            lstItemResult = [];//tạo mảng danh sách mới



        for (var i = 0; i < lstItem.length; i++) {
            var valueItem = $(lstItem[i]).val();
            if (valueItem != "") {
                arrItem.push(valueItem);

            }
        }
        
        //Danh sách mới các item sẽ không có các giá trị selected
        //Thực hiện lọc lại danh sách
        //Sử dụng findler để lọc

        data.forEach(function (item) {

            var countSelected = arrItem.length;
            if (countSelected > 0) {
                arrItem.forEach(function (i, e) {
                    if (item.id != i) {
                        countSelected--;
                    }
                    if (countSelected == 0) {
                        lstItemResult.push(item);
                    }
                })
            } else {
                lstItemResult = data;
                return
            }


        })
        
        var select = `
                            <div class="kt-input-icon form-group row">
                               
                                    <select data-live-search="true" name = "customers[]" class="form-control bootstrap-select col-10 kt-ml-10 controlSelect controlSelect1"><option value="">--- Chọn phòng triển khai---</option>`;
        if (lstItemResult == null && lstItemResult.length == 0) {

            select += '<option value=""></option>';
        }
        else {
            //dataCustom = lstItemResult;
            $(data).each(function (index, item) {
                select += '<option value="' + item.uuid + '">' + item.name + '</option>';
            });
        }
        var html = ` </select>
   <i class="btn btn-danger kt-ml-10 la la-minus deleteinput"></i>        `;
        $('.addcontract').append(select + html);
        $('.controlSelect').unbind('change');
        $('.controlSelect')
            .bind('change', function () {
                
                $('.controlSelect1').selectpicker('refresh');
                var lstItem1 = $(".controlSelect"),
                    arrItem1 = [];
                for (var i = 0; i < lstItem1.length; i++) {
                    var valueItem = $(lstItem1[i]).val();
                    if (valueItem != "") {
                        arrItem1.push(valueItem);

                    }
                }
                var abcd = $(this).val()
                var az = arrItem1.filter(item => item == abcd);
                if (az != "" && az.length > 1) {
                    
                    $.notify('Bạn đã chọn đơn vị này rồi. Vui lòng chọn lại!', { type: 'danger' });
                    $(this).val("");

                    $('.controlSelect').selectpicker('refresh');
                }

            });
        //}

    };
    $(".linkcontract").on("click", function () {
        if (!$('.nav-item').find('.active').hasClass("linkcontract")) {
            debugger
            var projectUuid = document.getElementById("project_uuid").value;
            var contractUuid = document.getElementById("contract_uuid").value;
            //if (dataTableInstancecontract != null) dataTableInstancecontract.table();
            if (dataTableInstancecontract != null) {
                dataTableInstancecontract.load();
            }
            else{
                dataTableInstancecontract = $('.kt-datatablecontract').KTDatatable({
                    data: {
                        type: 'remote',
                        source: {
                            read: {

                                url: `/project/projectgetcontract/${projectUuid}/${contractUuid}`,
                            },

                            response: {

                                map: function (res) {

                                    const raw = res && res.data || {};
                                    const { pagination } = raw;
                                    const perpage = pagination === undefined ? undefined : pagination.perpage;
                                    const page = pagination === undefined ? undefined : pagination.page
                                    CURRENT_PAGE = page || 1;
                                    LIMIT = perpage;
                                    START_PAGE = 0;
                                    return {
                                        data: raw.items || [],
                                        meta: {
                                            ...pagination || {},
                                            perpage: perpage || LIMIT,
                                            page: page || 1,
                                        },
                                    }
                                },

                            },
                            filter: function (data = {}) {
                                const query = data.query || {};
                                const pagination = data.pagination || {};
                                const { category_id, supplier_uuid, state, keyword } = query;

                                if (START_PAGE === 1) {
                                    pagination.page = START_PAGE;
                                }
                                return {
                                    limit: pagination.perpage || LIMIT,
                                    category_id: $('#js-filter-category').val(),
                                    supplier_uuid: supplier_uuid || $('#js-filter-supplier').val(),
                                    keyword: (keyword || $('#js-filter-keywordcontract').val()).trim(),
                                    state: state || $('#js-filter-states').val(),
                                    page: pagination.page,
                                };
                            }
                        },
                        pageSize: LIMIT,
                        serverPaging: true,
                        serverFiltering: true,
                    },

                    search: {
                        onEnter: true,

                    },
                    columns: [
                        {
                            field: 'uuid',
                            title: '#',
                            width: 30,
                            textAlign: 'center',
                            template: function (data, row) {
                                return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                            },
                            responsive: {
                                visible: 'md',
                                hidden: 'sm'
                            }
                        },
                        {
                            field: 'code',
                            title: 'Số HĐ gia hạn',
                            width: 60,

                        },
                        {
                            field: 'timeSigned_format',
                            title: 'Ngày kí HĐ',
                            width: 100,

                        },
                        //{
                        //    field: 'ticket.title',
                        //    title: 'Tiêu đề yêu cầu',
                        //}, 
                        {
                            field: 'timeValidValue',
                            title: 'Thời hạn BH',
                            width: 70,
                            textAlign: 'center',
                            template: function (data) {
                                if (data.timeValidValue != null) {
                                    return `${data.timeValidValue} Tháng`
                                }
                                return `Chưa rõ`
                            }

                        },


                        {
                            field: 'timeValidStart_format',
                            title: 'Thời gian hiệu lực',
                            width: 100,

                        },


                        {
                            field: 'timeMaintainLoop',
                            title: 'Số lần BH',
                            textAlign: 'center',
                            width: 70,
                            template: function (data) {
                                debugger
                                data.timeMaintainValue = data.timeMaintainValue == null ? 0 : data.timeMaintainValue;
                                data.timeMaintainLoop = data.timeMaintainLoop == null ? 0 : data.timeMaintainLoop;
                                return `${data.timeMaintainValue}/${data.timeMaintainLoop}`;
                            }
                        },

                        {
                            field: 'customerName',
                            title: 'Đơn vị',

                        },
                        {
                            field: 'state',
                            title: 'Trạng thái',

                            textAlign: 'center',
                            width: 110,

                            template: function (row) {
                                var states = {
                                    1: { 'class': 'kt-badge--primary' },
                                    2: { 'class': 'kt-badge--danger' },
                                    3: { 'class': 'kt-badge--warning' },
                                    4: { 'class': ' kt-badge--danger' },
                                    5: { 'class': 'kt-badge--success' },
                                    6: { 'class': 'kt-badge--info' },
                                    7: { 'class': 'kt-badge--danger' },
                                };
                                if (row.state) {
                                    return '<p class="kt-badge ' + states[row.state.id].class + ' kt-badge--inline kt-badge--pill">' + row.state.name + '</p>';
                                }
                                else {
                                    return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới</span>';
                                }
                            }
                        },
                        {
                            field: 'Tác vụ',
                            title: 'Tác vụ',
                            sortable: false,
                            width: 75,
                            overflow: 'visible',

                            textAlign: 'center',
                            template: function (data) {
                                return `
                            <button data-id="${data.uuid}" title="Chỉnh sửa" class="js-update-contract btn btn-sm btn-primary btn-icon btn-icon-md">
                             <i class="la la-edit"></i>
                            </button>
                            <button data-id="${data.uuid}" title="Khóa HĐ gia hạn" class="js-lock-subcontract btn btn-sm btn-danger btn-icon btn-icon-md ml-2">
                                <i class="la la-trash"></i>
                            </button>
                        `;
                            },
                        }
                    ],
                    layout: {
                        scroll: !0,
                        height: null,
                        footer: !1
                    },
                    sortable: false,
                    pagination: true,
                    responsive: true,

                });
            }
            
           
        }
    });
    var initValidation = function () {
        $('.dmms-project-contract__form').validate({
            rules:
            {
                code: {
                    required: true,
                },
                timeMaintainLoop: {
                    required: true,
                },
                timeMaintainValue: {
                    required: true,
                },
                customerUuid: {
                    required: true,
                },
                timeValidValue: {
                    required: true,
                },

            },
            messages: {
                code: {
                    required: 'Vui lòng nhập số hiệu.'
                },

                timeMaintainLoop: {
                    required: 'Vui lòng nhập số lần BH.'
                },
                timeMaintainValue: {
                    required: 'Vui lòng nhập số lần đã BH.'
                },
                customerUuid: {
                    required: 'Vui lòng chọn khách hàng.'
                },
                timeValidValue: {
                    required: 'Vui lòng chọn thời gian.'
                },
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo('.dmms-project-contract__form', -200);
                $('#kt-select-customer1').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });
                $('#timeselect').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });
            },
            submitHandler: function (form) {
                $(form).find('button[type="submit"]').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                const data = $(form).serializeJSON() || {};
                //const { uuid } = data;
                $.ajax({
                    url: `/project/CreateContract/`,
                    method: 'POST',
                    data: data,
                })
                    .done(function (data) {
                        
                        //$(".submit").attr("disabled", true);
                        //KTApp.block('.dmms-project-customer__form');
                        const successMsg = 'Thêm mới thành công.';
                        $.notify(successMsg, { type: 'success' });
                        drawer.close();
                        reloadTable();
                        /**
                         * Close drawer
                         */


                        //reloadTable();
                        //localStorage['edit_success'] = JSON.stringify({ from: (`/project/addcustomer/`), massage: successMsg });

                        //window.location.replace("customer");
                        //window.location = window.location.origin + "/project";
                    })
                    .fail(function (data) {
                        $(form).find('button[type="submit"]').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);

                        KTApp.unblock('.dmms-project-contract__form');
                        const errorMsg = data.responseJSON.msg + '!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }
    var initForm1  = function ($form1) {
        $form1.validate({
            rules:
            {
                package: {
                    required: true,
                },
                //description: {
                //    required: true,
                //},

            },
            messages: {
                package: {
                    required: 'Vui lòng nhập.'
                },

                //description: {
                //    required: 'Vui lòng nh?p mô t?.'
                //}
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form1, -200);
            },
            submitHandler: function (form) {
                
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                $.ajax({
                    url: `/project/EditPackage/`,
                    method: 'POST',
                    data,


                })

                    .done(function (data) {
                        
                        //$(".submit").attr("disabled", true);
                        //KTApp.block('.dmms-project-customer__form');
                        const successMsg = 'Cập nhật thành công.';
                        $.notify(successMsg, { type: 'success' });
                        reloadTable();
                        /**
                         * Close drawer
                         */


                        //reloadTable();
                        //localStorage['edit_success'] = JSON.stringify({ from: (`/project/addcustomer/`), massage: successMsg });

                        //window.location.replace("customer");
                        //window.location = window.location.origin + "/project";
                    })
                    .fail(function () {
                        KTApp.unblock($form1);
                        const errorMsg = 'Cập nhật không thành công';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }
    var initForm2 = function ($form2) {
        $form2.validate({
            rules:
            {
                package: {
                    required: true,
                },
                //description: {
                //    required: true,
                //},

            },
            messages: {
                package: {
                    required: 'Vui lòng nhập.'
                },

                //description: {
                //    required: 'Vui lòng nh?p mô t?.'
                //}
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form2, -200);
            },
            submitHandler: function (form) {
                
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                $.ajax({
                    url: `/project/UpdateContract/`,
                    method: 'POST',
                    data,


                })

                    .done(function (data) {
                        
                        //$(".submit").attr("disabled", true);
                        //KTApp.block('.dmms-project-customer__form');
                        const successMsg = 'Cập nhật thành công.';
                        $.notify(successMsg, { type: 'success' });
                        drawer.close();
                        reloadTable();
                        /**
                         * Close drawer
                         */


                        //reloadTable();
                        //localStorage['edit_success'] = JSON.stringify({ from: (`/project/addcustomer/`), massage: successMsg });

                        //window.location.replace("customer");
                        //window.location = window.location.origin + "/project";
                    })
                    .fail(function () {
                        KTApp.unblock($form2);
                        const errorMsg = 'Cập nhật không thành công';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }
    var initForm = function ($form) {
        initValidation(true);
    }

    var reloadTable = function () {
        
        if (dataTableInstancecontract) {
            dataTableInstancecontract.reload();
        }
    };
    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-project-contract__form');
        const $form1 = $content.find('.dmms-project-packageedit__form');
        const $form2 = $content.find('.dmms-project-contractupdate__form');

        initForm($form);
        initForm1($form1);
        initForm2($form2);
        /*
            init auto-resize
        */
        $('#js-filter-guarantee').selectpicker();
        $('.datecomplete').datepicker({ format: 'dd/mm/yyyy' }).datepicker('setDate', new Date());
        $('.exprivecontract').datepicker({ format: 'dd/mm/yyyy' }).datepicker('setDate', new Date());
        $('.datecreate').datepicker({ format: 'dd/mm/yyyy' }).datepicker('setDate', new Date());

        autosize($content.find('textarea.auto-resize'));

        $('#js-filter-customer').selectpicker();


    };
   
    var initSearch = function () {
        $('#js-filter-category')
            .on('change', function () {
                if (dataTableInstancecontract) {
                    dataTableInstancecontract.search($(this).val().toLowerCase(), 'category_id');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-supplier')
            .on('change', function () {
                if (dataTableInstancecontract) {
                    dataTableInstancecontract.search($(this).val().toLowerCase(), 'supplier_uuid');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-states')
            .on('change', function () {
                if (dataTableInstancecontract) {
                    dataTableInstancecontract.search($(this).val().toLowerCase(), 'state');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-keywordcontract')
            .on('change', function () {
                if (dataTableInstancecontract) {
                    dataTableInstancecontract.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            })
       

    };
    return {
        // Public functions
        init: function () {
            //initValidation();
            initSearch();
            initEventListeners();
            $('#js-filter-category').selectpicker();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-project').length) {
        
        DMMSProjectCus.init();
    }
});