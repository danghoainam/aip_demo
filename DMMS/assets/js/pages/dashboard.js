"use strict";
// Class definition

var DMMSDashboard = function () {
    var commonOptions = {
        credits: {
            enabled: false,
        },

        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            backgroundColor: '#00336C',
        },

        title: {
            text: '',
            style: {
                display: 'none'
            }
        },

        subtitle: {
            text: '',
            style: {
                display: 'none'
            }
        },

        yAxis: {
            title: {
                enabled: false,
            },
        },

        plotOptions: {
            column: {
                borderWidth: 0
            },

            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                },
                showInLegend: true,
            }
        },

        legend: {
            padding: 0,
            itemMarginTop: 5,
            itemMarginBottom: 5,
            itemStyle: {
                lineHeight: '20px',
                fontSize: '12px',
                fontWeight: 500,
            },

        },
    };
    // Private functions
    var initTask = function () {
        const chartOptions = {
            colors: ['#7AC943', '#3FA9F5', '#FF931E', '#9B51E0'],

            chart: {
                type: 'pie',
            },

            title: {
                text: '270',
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                style: {
                    fontFamily: 'inherit',
                    fontSize: 24,
                    fontWeight: 'bold',
                    display: 'block',
                },
            },

            tooltip: {
                pointFormat: '{point.y}'
            },

            plotOptions: {
                pie: {
                    dataLabels: {
                        format: '{point.y}'
                    },
                }
            },

            series: [{
                innerSize: '40%',
                colorByPoint: true,
                data: [{
                    name: 'Công việc mới',
                    y: 70
                }, {
                    name: 'Đã tiếp nhận',
                    y: 50
                }, {
                    name: 'Đang xử lý',
                    y: 100
                }, {
                    name: 'Yêu cầu thay thế',
                    y: 50
                }]
            }],

            legend: {
                labelFormatter: function labelFormatter() {
                    return `<b>${this.name}</b>`;
                },
            },
        };

        Highcharts.chart('dmms-chart-task', $.extend(true, {}, commonOptions, chartOptions));
    };

    var initTime = function () {
        const chartOptions = {
            colors: ['#27AE60', '#F1994A', '#E85656'],

            chart: {
                type: 'column'
            },

            xAxis: {
                categories: [
                    'Công việc mới',
                    'Đã tiếp nhận',
                    'Đang xử lý',
                    'Yêu cầu thay thế',
                ],
                crosshair: true
            },

            yAxis: {
                min: 0,
                title: {
                    text: ' '
                }
            },

            tooltip: {
                headerFormat: '<span>{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },

            series: [{
                name: 'Dưới 4h',
                data: [49, 71, 100, 129]

            }, {
                name: 'Từ 4h - 8h',
                data: [83, 78, 98, 93]

            }, {
                name: 'Trên 8h',
                data: [48, 38, 39, 41]

            }]
        };

        Highcharts.chart('dmms-chart-time', $.extend(true, {}, commonOptions, chartOptions));
    };

    var initContract = function () {
        const chartOptions = {
            colors: ['#27AE60', '#F2994A', '#9B51E0', '#EB5757'],

            chart: {
                type: 'pie',
            },

            tooltip: {
                pointFormat: '<b>{point.y} HĐ</b>'
            },

            plotOptions: {
                pie: {
                    dataLabels: {
                        format: '{point.y} HĐ'
                    },
                }
            },

            series: [{
                colorByPoint: true,
                data: [{
                    name: 'HSD 7 ngày',
                    y: 70
                }, {
                    name: 'Hết HSD 1 ngày',
                    y: 50
                }, {
                    name: 'Hết HSD 5 ngày',
                    y: 100
                }, {
                    name: 'Hết HSD 10 ngày',
                    y: 50
                }]
            }],

            legend: {
                labelFormatter: function labelFormatter() {
                    return `<b>${this.name}</b>`;
                },
            },

        };

        Highcharts.chart('dmms-chart-contract', $.extend(true, {}, commonOptions, chartOptions));
    };

    var initStaff = function () {
        const chartOptions = {
            colors: ['#27AE60', '#F2994A', '#9B51E0', '#EB5757'],

            chart: {
                type: 'pie',
            },

            tooltip: {
                pointFormat: '<b>{point.y} NV</b>'
            },

            plotOptions: {
                pie: {
                    dataLabels: {
                        format: '{point.y} NV'
                    },
                }
            },

            series: [{
                colorByPoint: true,
                data: [{
                    name: 'NV Rảnh',
                    y: 70
                }, {
                    name: 'NV Bận',
                    y: 50
                }, {
                    name: 'NV Đi công tác',
                    y: 100
                }, {
                    name: 'NV Nghỉ phép',
                    y: 50
                }]
            }],

            legend: {
                labelFormatter: function labelFormatter() {
                    return `<b>${this.name}</b>`;
                },
            },

        };

        Highcharts.chart('dmms-chart-staff', $.extend(true, {}, commonOptions, chartOptions));
    };

    var initDevice = function () {
        const chartOptions = {
            colors: ['#27AE60', '#F2994A', '#E85656'],

            chart: {
                type: 'column'
            },

            xAxis: {
                categories: [
                    'Thiết bị đang sửa',
                ],
                crosshair: true
            },


            yAxis: {
                min: 0,
                title: {
                    text: ' '
                }
            },

            series: [{
                name: '< 5 ngày',
                data: [49]

            }, {
                name: '5 - 10 ngày',
                data: [83]

            }, {
                name: '> 10 ngày',
                data: [48]
            }]
        };

        Highcharts.chart('dmms-chart-device', $.extend(true, {}, commonOptions, chartOptions));
    };

    var initPR = function () {
        const chartOptions = {
            colors: ['#27AE60', '#3FA9F5', '#EB5757'],

            chart: {
                type: 'pie',
            },

            tooltip: {
                pointFormat: '<b>{point.y} Đề xuất</b>'
            },

            plotOptions: {
                pie: {
                    dataLabels: {
                        format: '{point.y} ĐX'
                    },
                }
            },

            series: [{
                colorByPoint: true,
                data: [{
                    name: 'Đề xuất dưới 2h',
                    y: 70
                }, {
                    name: 'Đề xuất từ 2-4h',
                    y: 50
                }, {
                    name: 'Đề xuất trên 4h',
                    y: 100
                }]
            }],

            legend: {
                labelFormatter: function labelFormatter() {
                    return `<b>${this.name}</b>`;
                },
            },

        };

        Highcharts.chart('dmms-chart-pr', $.extend(true, {}, commonOptions, chartOptions));
    };

    return {
        // Public functions
        init: function () {
            initTask();
            initTime();
            initContract();
            initStaff();
            initDevice();
            initPR();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-dashboard').length) {
        DMMSDashboard.init();
    }
});
