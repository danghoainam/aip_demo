﻿"use strict";
// Class definition

var DMMSBunker = function () {
    // Private functions

    var dataTableInstance = null;
    var drawer = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    const uuid = $('#uuid').val();

    var initTable = function () {
        if (dataTableInstance !== null) dataTableInstance.destroy();
        const storage_uuid = uuid;
        dataTableInstance = $('.kt-datatable--staff').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: `/bunker/getstaffinbunker/${storage_uuid}`,
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? 0 : pagination.page;
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: (keyword || $('#js-filter-keyword').val()) !== undefined ? (keyword || $('#js-filter-keyword').val()).trim() : (keyword || $('#js-filter-keyword').val()),
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,

            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 30,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'name',
                    title: 'Nhân viên kho',
                },
                {
                    field: 'phone_number',
                    title: 'Số điện thoại',
                    width: 150
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 90,
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        var output = '';
                        if ($('#state').val() != 1) {
                            output += `<button type="button" title="Xóa" class="btn btn-sm btn-danger btn-icon btn-icon-md js-delete-staffinbunker"
                                data-id="${data.uuid}" data-storage_uuid="${uuid}" disabled>
                                <i class="la la-trash"></i>
                            </button>`;
                        }
                        else {
                            output += `<button type="button" title="Xóa" class="btn btn-sm btn-danger btn-icon btn-icon-md js-delete-staffinbunker"
                                data-id="${data.uuid}" data-storage_uuid="${uuid}">
                                <i class="la la-trash"></i>
                            </button>`;
                        }
                        return output;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };

    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var initSearch = function () {
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });
    };

    var addStaffToBunker = function () {
        const title = 'THÊM NHÂN VIÊN VÀO KHO';
        drawer = new KTDrawer({ title, uuid: 'new', className: 'dmms-drawer--staffinbunker' });

        drawer.on('ready', () => {
            $.ajax({
                url: `/bunker/addstaff/${uuid}`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);

                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    var deleteStaffInBunker = function (id, storage_uuid) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa nhân viên...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/bunker/deletestaff`,
                method: 'DELETE',
                data: {
                    storage_uuid: storage_uuid,
                    storage_keeper_uuid: id
                }
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify("Xóa thành công!", { type: 'success' });

                    if (dataTableInstance) {
                        dataTableInstance.reload();
                    }
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage(data);
                    $.notify(data.responseJSON.msg, { type: 'danger' });
                });
        }
    };

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-staffinbunker')
            .on('click', '.js-delete-staffinbunker', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                const storage_uuid = $this.attr('data-storage_uuid');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Nhân viên sẽ bị xóa khỏi kho.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteStaffInBunker(id, storage_uuid);
                    }
                });
            })
            .off('click', '.js-add-stafftobunker')
            .on('click', '.js-add-stafftobunker', function () {

                addStaffToBunker();
            });
    };

    var initForm = function ($form) {
        $form.validate({
            rules: {
                storage_uuid: {
                    required: true,
                },
                'storage_keeper[]': {
                    required: true,
                }
            },
            messages: {
                storage_uuid: {
                    required: "Vui lòng nhập tên kho!",
                },
                'storage_keeper[]': {
                    required: "Vui lòng nhập nhân viên!",
                },
            },

            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                $.ajax({
                    url: '/bunker/addstaff',
                    method: 'POST',
                    data,
                })
                    .done(function () {
                        KTApp.block($form);
                        const successMsg = 'Thêm nhân viên vào kho thành công!';
                        $.notify(successMsg, { type: 'success' });

                        /**
                         * Close drawer
                         */
                        drawer.close();

                        reloadTable();
                    })
                    .fail(function () {
                        KTApp.unblock($form);
                        const successMsg = 'Thêm nhân viên vào kho không thành công!';
                        $.notify(data.responseJSON.msg, { type: 'danger' });
                    });
                return false;
            },
        });
    };

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-staff__form');
        initForm($form);

        $('#kt-select-staffintab').selectpicker();

        $('#kt-select-staffintab').on('change', function () {
            const $this = $(this);
            var isAllSelected = $this.find('option[value=""]').prop('selected');
            var lastAllSelected = $(this).data('all');
            var selectedOptions = ($this.val()) ? $this.val() : [];
            var allOptionsLength = $this.find('option[value!=""]').length;
            var selectedOptionsLength = selectedOptions.length;

            if (isAllSelected == lastAllSelected) {

                if ($.inArray("", selectedOptions) >= 0) {
                    selectedOptionsLength -= 1;
                }

                if (allOptionsLength <= selectedOptionsLength) {

                    $this.find('option[value=""]').prop('selected', true).parent().selectpicker('refresh');
                    isAllSelected = true;
                } else {
                    $this.find('option[value=""]').prop('selected', false).parent().selectpicker('refresh');
                    isAllSelected = false;
                }

            } else {
                $this.find('option').prop('selected', isAllSelected).parent().selectpicker('refresh');
            }

            var text_display = $('.kt-selectbox').find('.filter-option-inner-inner').text();
            $('.kt-selectbox').find('.filter-option-inner-inner').html(text_display.replace('Chọn tất cả, ', ''));

            $(this).data('all', isAllSelected);
        }).trigger('change');
    };

    return {
        // Public functions
        init: function () {
            initTable();
            initSearch();
            initEventListeners();
        },
    };
}();

$(document).ready(function () {
    $('.bunker-tab-staff').on('click', function () {
        if ($('.dmms-bunker').length) {
            DMMSBunker.init();
        }
    })
});