﻿"use strict";
// Class definition

var DMMSCategory = function () {
    // Private functions
    var tableCategory = null;
    var drawer = null;
    var index = 0;

    var columns = [
        {
            "title": "<span class='text-white'>#</span>",
            "target": 0,
            "data": function (item) {
                console.log(item);
                if (item.parent_id === null) {
                    return ++index;
                }
                return '';
            },
            "width": "35px"
        },
        {
            "title": "",
            "target": 1,
            "className": 'treegrid-control text-center"',
            "data": function (item) {
                if (item.children && item.children.length>0) {
                    return '<span><i class="flaticon2-plus"></i></span>';
                }
                return '';
            },
            "width":"40px"
        },
        {
            "title": "<span class='text-white'>Tên thiết bị</span>",
            "target": 2,
            "data": function (item) {
                return item.name;
            }
        },
        {
            "title": "<span class='text-white'>Mô tả</span>",
            "target": 3,
            "data": function (item) {
                return item.description;
            }
        },
        {
            title: "<span class='text-white'>Tác vụ</span>",
            target: 4,
            width: "110px",
            data: function (item) {
                return `
                            <button data-id="${item.id}" title="Chỉnh sửa" class="js-edit-category btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </button>
                            <button data-id="${item.id}" title="Xóa" class="js-delete-category btn btn-sm btn-danger btn-icon btn-icon-md">
                                <i class="la la-trash"></i>
                            </button>
                        `;
            }
        },
    ];

    var initCategoryDrawer = function (id) {
        const title = id ? 'Sửa thông tin loại thiết bị' : 'Thêm loại thiết bị';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--category' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/category/edit/${id}` : '/category/create',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    var deleteCategory = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa loại thiết bị...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/category/delete/${id}`,
                method: 'DELETE',
            })
                .done(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(data.msg, { type: 'success' });

                    reloadTable();
                })
                .fail(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(data.msg, { type: 'danger' });
                });
        }
    }

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-category')
            .on('click', '.js-delete-category', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Loại thiết bị sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteCategory(id);
                    }
                });
            })
            .off('click', '.js-edit-category')
            .on('click', '.js-edit-category', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initCategoryDrawer(id || '');
            });

    };

    // demo initializer
    var initTable = function () {
        tableCategory = $('#category-table').DataTable({
            columns: columns,
            ajax: {
                url: "/category/gets",
                type: "GET",
                datatype: "json",
                error: function (err) {
                    const errorMsg = 'Cố lỗi xảy ra nên không thực hiện được thao tác!'
                    $.notify(errorMsg, { type: 'danger' });
                }
            },
            
            treeGrid: {
                left: 20,
                expandIcon: '<span><i class="flaticon2-plus"></i></span>',
                collapseIcon: '<span><i class="flaticon2-line"></i></span>'
            },
            paging: false,
            ordering: false,
            info: false,
            searching: true,
            columnDefs: [
                {
                    "targets": 0,
                    "className": "text-center",
                },
                {
                    "targets": 4,
                    "className": "text-center",
                },

            ],
            language: {
                loadingRecords: "Đang tải dữ liệu..."
            }
        });

        $("#js-filter-keyword")
            .on('keyup click', function () {             
                tableCategory.search($(this).val()).draw();
            });
    };

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-category__form');
        initForm($form);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

        //$('#kt-select-devicecategory').selectpicker();
        if ($("#parent_id").val() !== '') {
            $('#kt-select-devicecategory').selectpicker('val', $("#parent_id").val());
        }
        else {
            $('#kt-select-devicecategory').selectpicker();
        }
    };

    var initForm = function ($form) {
        $form.validate({
            rules: {
                name: {
                    required: true,
                },
                description: {
                    required: true,
                },
            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên loại thiết bị.'
                },
                description: {
                    required: 'Vui lòng nhập mô tả.'
                }
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {

                const data = $(form).serializeJSON() || {};
                const { id } = data;
                console.log(data, id);
                $.ajax({
                    url: id ? `/category/edit` : 'category/create',
                    method: id ? 'PUT' : 'POST',
                    data,
                })
                    .done(function () {
                        KTApp.block($form);
                        const successMsg = id ? 'Sửa thông tin thành công!' : 'Thêm loại thiết bị thành công!';
                        $.notify(successMsg, { type: 'success' });

                        /**
                         * Close drawer
                         */
                        drawer.close();

                        reloadTable();
                    })
                    .fail(function () {
                        KTApp.unblock($form);
                        const errorMsg = id ? 'Sửa thông tin không thành công!' : 'Thêm loại thiết bị không thành công!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }

    var reloadTable = function () {
        if (tableCategory) {
            tableCategory.ajax.reload();
            index = 0;
        }
    };

    return {
        // Public functions
        init: function () {
            initTable();
            initEventListeners();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-category').length) {
        DMMSCategory.init();
    }
});
