﻿"use strict";
// Class definition
var DMMSGroup = function () {
    // Private functions

    var dataTableInstance = null;

    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var drawer = null;
    var START_PAGE = 0;
    // demo initializer
    var initTable = function () {
        dataTableInstance = $('#js-group-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/group/gets',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            CURRENT_PAGE = pagination.page || 1;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: pagination.perpage || LIMIT,
                                    page: pagination.page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE
                        }
                        return {
                            limit: pagination.perpage || LIMIT,                          
                            keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            page: pagination.page,

                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },
            // layout definition
            layout: {
                theme: 'default',
                scroll: false,
                height: null,
                footer: false,
            },
            search: {
                onEnter: true,
            },
            columns: [              
                {
                    field: 'uuid',
                    title: '#',
                    width: 20,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'name',
                    title: 'Tên khu vực',
                    autoHide: false, 
                },
                {
                    field: 'description',
                    title: 'Mô tả',
                    autoHide: false,
                },
                {
                    field: 'saff',
                    title: 'Nhân viên',
                    width: 70,
                    sortable: false,
                    overflow: 'visible',
                    textAlign: 'center',
                    
                    template: function (row) {
                        return '\
		                  <a title="Thêm nhân viên vào nhóm" data-group-name="'+ row.name + '" data-group-desc="' + row.description + '" data-group-uuid="' + row.uuid + '"\
                                class="js-list-staff btn btn-default la la-user-plus" >\
		                  </a>';

                    },
                },
                {
                    field: 'customer',
                    title: 'Số lượng đơn vị',
                    width: 115,
                    sortable: false,
                    overflow: 'visible',
                    textAlign: 'center',
                    template: function (row) {
                        console.log(row);
                        var result = '';
                        if (row.customer != null && row.customer.length > 0) {
                            result = `<a><a class="text-danger">` + data.staff.length + `</a> đơn vị</a>`;
                        }
                        else {
                            result = `<a><a class="la la-warning text-warning"></a> Chưa có </a>`;
                        }
                        return result += '\
		                  <a title="Thêm khách hàng vào nhóm" data-group-name="'+ row.name + '" data-group-desc="' + row.description + '" data-id="' + row.uuid + '"\
                            class="js-list-customer ml-1 btn btn-default la la-user-plus" >\
		                  </a>';

                    },
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 110,
                    overflow: 'visible',
                    textAlign: 'center',
                    template: function (row) {
                        return '\
                            <button data-id="'+ row.uuid +'" title="Chỉnh sửa" class="js-edit-group btn btn-sm btn-primary btn-icon btn-icon-md">\
                                <i class="la la-edit"></i>\
                            </button>\
                            <button data-id="'+ row.uuid +'" title="Chi tiết" class="js-detail-group btn btn-sm btn-success btn-icon btn-icon-md">\
                                <i class="la la-info"></i>\
                            </button>\
                            <button data-id="'+ row.uuid +'" type = "button" title = "Xóa" class="js-delete-group btn btn-sm btn-danger btn-icon btn-icon-md">\
                                <i class="la la-trash"></i>\
                            </button>\
                        ';
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true
        });

        var portlet = dataTableInstance.closest('.kt-portlet');

        portlet.find('#kt_form_status').on('change', function () {
            dataTableInstance.search($(this).val().toLowerCase(), 'Status');
        });

        portlet.find('#kt_form_type').on('change', function () {
            dataTableInstance.search($(this).val().toLowerCase(), 'Type');
        });

        portlet.find('#kt_form_status,#kt_form_type').selectpicker();
    };
    var initSearch = function () {
        $('#js-filter-keyword')
        .on('change', function () {
            if (dataTableInstance) {
                dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                START_PAGE = 1;
            }
        });
    }
    var initValidation = function () {
        $("#dmms-group__form").validate({
            rules: {

                name: {
                    required: true,
                },

            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên nhóm.'
                },
                description: {
                    required: 'Vui lòng nhập mô tả.'
                },

            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('#dmms-group__form', -200);
            },
            submitHandler: function (form) {
                form[0].submit(); // submit the form
            },
        });
    };

    var initGroupDrawer = function (id) {
        const title = id ? 'Sửa thông tin nhóm' : 'Thêm nhóm';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--group' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/group/update/${id}` : '/group/create',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    var initGroupDetailDrawer = function (id) {
        const title = id ? 'Chi tiết khu vực' : '';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-detail-drawer--group' });

        drawer.on('ready', () => {
            $.ajax({
                url: `/group/detail/${id}`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    }

    var initGroupEventListeners = function () {
        $(document)
            .off('click', '.js-delete-group')
            .on('click', '.js-delete-group', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Nhóm sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteGroup(id);
                    }
                });
            })
            .off('click', '.js-edit-group')
            .on('click', '.js-edit-group', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initGroupDrawer(id || '');
            })
            .on('click', '.js-detail-group', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initGroupDetailDrawer(id || '');
            });

    };

    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var deleteGroup= function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa nhóm...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/group/delete/${id}`,
                method: 'DELETE',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa nhóm thành công!', { type: 'success' });

                    reloadTable();
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa nhóm không thành công!', { type: 'danger' });
                });
        }
    };

    var initForm = function ($form) {
        $form.validate({
            rules: {
                name: {
                    required: true,
                },
                description: {
                    required: true,
                },

            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên nhóm.'
                },

                description: {
                    required: 'Vui lòng nhập mô tả.'
                }
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                $.ajax({
                    url: uuid ? `/group/update/${uuid}` : 'group/create',
                    method: uuid ? 'PUT' : 'POST',
                    data,
                })
                .done(function (res) {
                    const successMsg = uuid ? 'Sửa thông tin thành công!' : 'Thêm nhóm thành công!';
                    if (res != null && res.error == 1) {
                        $.notify("Có lỗi xảy ra. Vui lòng kiểm tra lại dữ liệu", { type: 'danger' });
                    }
                    else {
                        KTApp.block($form);
                        $.notify(successMsg, { type: 'success' });
                        drawer.close();
                        reloadTable();
                    }
                })
                .fail(function () {
                    KTApp.unblock($form);
                    const errorMsg = uuid ? 'Sửa thông tin không thành công!' : 'Thêm nhóm không thành công!';
                    $.notify(errorMsg, { type: 'danger' });
                });
                return false;
            },
        });
    }

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-group__form');
        initForm($form);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

    };

    return {
        // Public functions
        init: function () {
            initTable();
            initGroupEventListeners();
            initSearch();
            initValidation();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-group').length) {
        DMMSGroup.init();
    }
});
