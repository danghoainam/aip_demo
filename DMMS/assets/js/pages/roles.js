"use strict";
// Class definition

var DMMSDevice = function () {
    // Private functions

    var dataTableInstance = null;
    var drawer = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    // demo initializer
    var initTable = function () {
        dataTableInstance = $('.kt-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url:'/roles/gets',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page;
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        debugger
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const {  type } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            //category_id: category_id || $('#js-filter-category').val(),
                            //supplier_uuid: supplier_uuid || $('#js-filter-supplier').val(),
                            //project_uuid: project_uuid || $('#js-filter-project').val(),
                            //customer_uuid: customer_uuid || $('#js-filter-customer').val(),
                            type: type,
                            page: pagination.page

                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,
            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 25,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'name',
                    title: 'Tên chức năng',
                    //autoHide: false,
                },
                {
                    field: 'description',
                    title: 'Mô tả',
                    
                },
                {
                    field: 'is_default',
                    title: 'Mặc định',
                    width: 70,
                    textAlign:'center',
                    template: function (data) {
                        debugger
                        var ouput = ''
                        if (data.is_default == 1) {
                            ouput = 'Có'
                        }
                        else {
                            ouput = 'Không'
                        }
                        return ouput;
                    }
                },

                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 80,
                    overflow: 'visible',
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        return `
                            <a href="/roles/edit/${data.uuid}" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </a>                           
                            <button data-id="${data.uuid}" type="button" title="Xóa" class="btn btn-sm btn-danger btn-icon btn-icon-md js-delete-device">
                                <i class="la la-trash"></i>
                            </button>
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });

        //remove item in localStorage
    };
    var initSearch = function () {
        $('#js-filter-type')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'type');
                    START_PAGE = 1;
                }
            })
            .selectpicker();

    };
    var names = $('.getvalueweb input:checked').map(function () {
        return this.value;
    }).get();
    var deleteDevice = function (id) {
        var uuid = id
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa thiết bị...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/roles/delete/${uuid}`,
                method: 'DELETE',
            })
                .done(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa chức năng thành công!', { type: 'success' });

                    if (dataTableInstance   ) {
                        dataTableInstance.reload();
                    }
                })
                .fail(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(data.responseJSON.msg, { type: 'danger' });
                });
        }
    };
    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
        
    }
    $(document).on('change', '#js-filter-project', function () {
        //$('#variant').find('.tesst').remove();
        $('#variant').find('.test').remove();
        var project_uuid = $(this).val();
        if (project_uuid != '') {
            $.ajax({
                url: `/project/projectgetcustomer/${project_uuid}`,
                method: 'POST',
                dataType: "json",
                data: {
                    limit: 1000,
                },
                success: function (data) {

                    console.log(data);
                    var customer = data.data.items;
                    var json = JSON.stringify(customer);
                    var $select = $('#variant');

                    //for (var i = 0; i < customer.length; i++) {
                    //    $select.append('<option value=' + customer[i].project_customer_uuid + '>' + customer[i].name + '</option>');
                    //    $select.selectpicker('refresh');
                    //}

                    $.each(customer, function (index, item) {
                        console.log(item);
                        $select.append('<option value=' + item.customerUuid + '>' + item.customerName + '</option>');
                        $select.selectpicker('refresh');
                    })
                }
            })
        } else {
            $('#model').html('<option value="">Select Model</option>');
        }

        $('#variant').html('<option value="">---Chọn đơn vị---</option>');
    });

    $(document).on('change', '#js-filter-projectedit', function () {
        $('#variant1').find('.test').remove();
        var project_uuid = $(this).val();
        if (project_uuid != '') {
            $.ajax({
                url: `/project/getCustomer/${project_uuid}`,
                method: 'POST',
                dataType: "json",
                success: function (data) {

                    console.log(data);
                    var customer = data.data.customers;
                    var json = JSON.stringify(customer);
                    console.log(json);
                    var $select = $('#variant1');

                    for (var i = 0; i < customer.length; i++) {
                        $select.append('<option value=' + customer[i].project_customer_uuid + '>' + customer[i].name + '</option>');
                        $select.selectpicker('refresh');
                    }


                }
            })
        } else {
            $('#model').html('<option value="">Select Model</option>');
        }

        $('#variant1').html('<option value="">---Chọn đơn vị---</option>');
    });



    if (document.getElementById("project_uuid")) {



        const id = document.getElementById("project_uuid").value;
        if (id != '') {


            $.ajax({
                url: `/project/Detail/${id}`,
                method: 'POST',
                dataType: "json",
                success: function (data) {
                    //$('.getcuspro').find('.remove1').remove();

                    var firtid = $('.remove1').val();
                    console.log(data);
                    var customer = data.customers;
                    var json = JSON.stringify(customer);
                    console.log(json);
                    var $select = $('.getcuspro');
                    for (var i = 0; i < customer.length; i++) {
                        //debugger
                        if (customer[i].project_customer_uuid !== firtid) {
                            $select.append('<option value=' + customer[i].project_customer_uuid + ' class ="test">' + customer[i].name + '</option>');
                            $select.selectpicker('refresh');
                            //$('.getcuspro').selectpicker();

                        }

                    }

                    //$('#variant').selectpicker('val', json.val());

                }
            })
        } else {
            $('#model').html('<option value="">Select Model</option>');
        }
    }


    var initDetailDrawer = function (id) {
        const title = id ? 'Chi tiết đơn vị' : 'Thêm mới chức năng';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--roles' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/roles/add/${id}` : '/roles/add',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    const $form = $content.find('.dmms-roles__form');
                    initValidation($form);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-device')
            .on('click', '.js-delete-device', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Chức năng sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteDevice(id);
                    }
                });

            })
            .off('click', '.addroles')
            .on('click', '.addroles', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                initDetailDrawer(id || '');
            });


    };
    
    var initValidation = function () {

        $(".dmms-roles__form").validate({
            rules: {

                name: {
                    required: true,
                },
                
            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên chức năng.'
                },
                
            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('.dmms-roles__form', -200);
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;

                console.log(data, uuid);
                $.ajax({
                    url: uuid ? `/device/edit` : '/roles/add',
                    method: uuid ? 'PUT' : 'POST',
                    data,
                })
                    .done(function () {
                        $(".submit").attr("disabled", true);
                        const successMsg = 'Thêm chức năng thành công!';
                        $.notify(successMsg, { type: 'succes' });
                        drawer.close();
                        reloadTable();
                    })
                    .fail(function (data) {
                        const errorMsg = uuid ? data.responseJSON.msg + '!' : data.responseJSON.msg + '!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    };
    var initValidation1 = function () {

        $(".dmms-rolesedit__form").validate({
            rules: {

                name: {
                    required: true,
                },

            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên chức năng.'
                },

            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('.dmms-rolesedit__form', -200);
            },
            submitHandler: function (form) {
                debugger
                $(form).find('button[type="submit"]').prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading
                const data = $(form).serializeJSON() || {};
                data.names = names;
                const { uuid } = data;

                console.log(data, uuid);
                $.ajax({
                    url:`/roles/edit`,
                    method:'PUT',
                    data,
                })
                    .done(function (data) {
                        var uuid = $('#role_uuid').val();
                        debugger
                        $(".submit").attr("disabled", true);
                        const successMsg = 'Cập nhật chức năng thành công!';
                        $.notify(successMsg, { type: 'succes' });
                        window.location = window.location.origin + `/roles/edit/${uuid}`;
                    })
                    .fail(function (data) {
                        $(form).find('button[type="submit"]').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
                        const errorMsg = uuid ? data.responseJSON.msg + '!' : data.responseJSON.msg + '!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    };
    $('.filter-keyword').on('change', function () {
       
        debugger
        var role_uuid = $('#role_uuid').val();
        var keyword = $(this).val();
        $.ajax({
            url: `/roles/details`,
            method: 'GET',
            data: { keyword: keyword, role_uuid: role_uuid}
        })
            .done(function (data) {
                var html = [];
                debugger
                $('.initper').empty();
                $.each(data.data.permissions, function (key, value) {
                    if (value.type == 1) {
                        $('.initper').append(`<label class="control-label col-3 text-uppercase"><b>Web</b></label>`);
                        $('.initper').append(`<div class="form-group row type1" style="margin-top:0px;margin-bottom: 10px;padding-top: 10px;background-color: #f9f9fc;border: 1px solid #ebedf2;">`);
                        $.each(value.permissions, function (index, item) {
                            debugger
                            var check = '';
                            var val = item.uuid;
                            var checkval = names.filter(word => word == val);
                            if (checkval.length != 0) {
                                check = "checked";
                            }
                            $('.type1').append(`<label class="control-label col-3 text-uppercase">${item.name}</label>
                                <div class="kt-input-icon col-1">
                                    <input name="listpermissinos[]" value="${item.uuid}" class="form-control" type="checkbox" ${check} style="height:65%;" />
                                </div>`);
                        })
                        
                       
                        $('.initper').append('</div>');
                    }
                    if (value.type == 2) {
                        $('.initper').append(`<label class="control-label col-3 text-uppercase"><b>App</b></label>`);
                        $('.initper').append(`<div class="form-group row type2" style="margin-top:0px;margin-bottom: 10px;padding-top: 10px;background-color: #f9f9fc;border: 1px solid #ebedf2;">`);
                        $.each(value.permissions, function (index, item) {
                            var check = '';
                            if (item.hasPermission == true) {
                                check = "checked";
                            }
                            $('.type2').append(`<label class="control-label col-3 text-uppercase">${item.name}</label>
                                <div class="kt-input-icon col-1">
                                    <input name="listpermissinos[]" value="${item.uuid}" class="form-control" type="checkbox" ${check} style="height:65%;" />
                                </div>`);
                        })


                        $('.initper').append('</div>');
                    }
                    if (value.type == 3) {
                        $('.initper').append(`<label class="control-label col-3 text-uppercase"><b>CRM</b></label>`);
                        $('.initper').append(`<div class="form-group row type3" style="margin-top:0px;margin-bottom: 10px;padding-top: 10px;background-color: #f9f9fc;border: 1px solid #ebedf2;">`);
                        $.each(value.permissions, function (index, item) {
                            var check = '';
                            if (item.hasPermission == true) {
                                check = "checked";
                            }
                            $('.type3').append(`<label class="control-label col-3 text-uppercase">${item.name}</label>
                                <div class="kt-input-icon col-1">
                                    <input name="listpermissinos[]" value="${item.uuid}" class="form-control" type="checkbox" ${check} style="height:65%;" />
                                </div>`);
                        })


                        $('.initper').append('</div>');
                    }
                    if (value.type == 4) {
                        $('.initper').append(`<label class="control-label col-3 text-uppercase"><b>Customer</b></label>`);
                        $('.initper').append(`<div class="form-group row type4" style="margin-top:0px;margin-bottom: 10px;padding-top: 10px;background-color: #f9f9fc;border: 1px solid #ebedf2;">`);
                        $.each(value.permissions, function (index, item) {
                            var check = '';
                            if (item.hasPermission == true) {
                                check = "checked";
                            }
                            $('.type4').append(`<label class="control-label col-3 text-uppercase">${item.name}</label>
                                <div class="kt-input-icon col-1">
                                    <input name="listpermissinos[]" value="${item.uuid}" class="form-control" type="checkbox" ${check} style="height:65%;" />
                                </div>`);
                        })


                        $('.initper').append('</div>');
                    }
                })
                $("input:checkbox").change(function () {
                    debugger
                    //var names = $('.getvalueweb input:checked').map(function () {
                    //    return this.value;
                    //}).get();

                    var ischecked = $(this).is(':checked');
                    if (!ischecked) {
                        var val = $(this).val();
                        var checkval = names.filter(word => word == val);
                        if (checkval.length != 0) {
                            names.splice(names.indexOf(val), 1);
                        }
                    }
                    else {
                        var val = $(this).val();
                        var checkval = names.filter(word => word == val);
                        if (checkval.length == 0) {
                            names.push(val);
                        }
                    }
                }); 
               
            })
            .fail(function (data) {
                $.notify('Có lỗi xảy ra !', { type: 'danger' });
            });
        
    })
 
    const $form = $('.dmms-rolesedit__form');
    initValidation1($form);
    $("input:checkbox").change(function () {
        debugger
        //var names = $('.getvalueweb input:checked').map(function () {
        //    return this.value;
        //}).get();

        var ischecked = $(this).is(':checked');
        if (!ischecked) {
            var val = $(this).val();
            var checkval = names.filter(word => word == val);
            if (checkval.length != 0) {
                names.splice(names.indexOf(val), 1);
            }
        }
        else {
            var val = $(this).val();
            var checkval = names.filter(word => word == val);
            if (checkval.length == 0) {
                names.push(val);
            }
        }
    }); 
    return {
        // Public functions
        init: function () {
            initTable();
            initSearch();
            initValidation();
            initValidation1();
            initEventListeners();

        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-roles').length) {
        DMMSDevice.init();


    }
});
