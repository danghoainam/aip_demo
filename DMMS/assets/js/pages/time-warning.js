﻿"use strict";

// Class definition
var DMMSTimeWarning = function () {
    // Private functions

    //var _this = {};
    //var warning = {
    //    $container: $('#js-display-warning'),
    //    $title: $('#js-title-3'),

    //    load: function () {
    //        KTApp.block(this.$container);
    //        $.ajax({
    //            url: '/timewarning/gets',
    //            method: 'GET',
    //        }).done((res) => {
    //            KTApp.unblock(this.$container);
    //            if (res && res.data) {
    //                this.render(res.data || {});
    //            }
    //        }).fail(() => {
    //            KTApp.unblock(this.$container);
    //            //$.notify('Không kết nối được đến máy chủ', { type: 'danger' });
    //            this.render({});
    //        });
    //    },

    //    render: function (data) {
    //        var html = [];
    //        //Object chứa các type
    //        var types = {};
    //        //Object names chứa tên tương ứng với type
    //        var names = {
    //            //0 - Ticket
    //            0: "Yêu cầu đề xuất",
    //            //1 - Device
    //            1: "Thiết bị",
    //            //Task
    //            2: "Công việc",
    //            //3 - DailyReport
    //            3: "Báo cáo hàng ngày",
    //            //4 - ContractExpired
    //            4: "HSD hợp đồng",
    //        };

    //        //Object các trạng thái của từng type
    //        var states = {
    //            //0 - Ticket
    //            0: {
    //                1: "Mới",
    //                2: "Đã tiếp nhận",
    //                3: "Đang xử lý",
    //                4: "Đã xử lý xong",
    //                5: "Hoàn thành",
    //            },
    //            //1 - Device
    //            1: {
    //                1: "Đang sử dụng",
    //                2: "Đang yêu cầu sửa chữa",
    //                3: "Đến lịch bảo hành",
    //                4: "Đang xử lý theo yêu cầu",
    //                5: "Cần thay thế",
    //                6: "Mang đi sửa chữa-Không có thiết bị thay thế",
    //                7: "Mang đi sửa chữa-Có thiết bị thay thế",
    //                8: "Thay thế",
    //                9: "Hỏng",
    //            },
    //            //2 - Task
    //            2: {
    //                1: "Mới",
    //                2: "Yêu cầu hỗ trợ",
    //                3: "Đã tiếp nhận",
    //                4: "Đã hẹn lịch",
    //                5: "Đang xử lý",
    //                6: "Đợi duyệt đề xuất xử lý",
    //                7: "Yêu cầu tiếp tục",
    //                8: "Hoàn thành",
    //            },
    //            //3 - DailyReport
    //            3: {
    //                1: "Chưa tạo",
    //                2: "Chưa duyệt",
    //                3: "Đã duyệt",
    //                4: "Từ chối",
    //            },
    //            //4 - ContractExpired
    //            4: {
    //                1: "Còn hạn",
    //                2: "Quá hạn",
    //                3: "Đã hỏng",
    //            },
    //        };

    //        //unitState
    //        var units = {
    //            1: { 'nameVN': 'Phút', 'nameEng': 'Minute' },
    //            2: { 'nameVN': 'Giờ', 'nameEng': 'Hour' },
    //            3: { 'nameVN': 'Ngày', 'nameEng': 'Day' },
    //            4: { 'nameVN': 'Tuần', 'nameEng': 'Week' },
    //            5: { 'nameVN': 'Tháng', 'nameEng': 'Month' },
    //        };

    //        $.each(names || {}, function (i) {//Lọc các warning theo type => add vào object types
    //            var list = [];
    //            $.each(data.items || [], function (index, item) {//Duyệt các warning
    //                if (item.type == i) {
    //                    list.push(item);
    //                }
    //            });
    //            //Add to types object
    //            types[i] = list;
    //        }) 

    //        $.each(types || {}, function (index, warning) {//Duyệt từng phần tử trong types
    //            html.push(`<div class="kt-warning">
    //                            <div data-name="${names[index]}" data-type="${index}" class="kt-warning__title d-flex align-items-center js-warning">
    //                                <span class="text-uppercase">${names[index]}</span>
    //                                <span class="ml-auto kt-switch kt-switch--sm kt-switch--icon">
    //					    <label class="btn-on-off">
    //					        <input type="checkbox" checked="checked" id="js-on-off" data-type="${index}">
    //					        <span></span>
    //					    </label>
    //					</span>
    //                            </div>`);
    //            html.push(`<form action="timewarning/Index" data-type="${index}" data-name="${names[index]}" class="kt-warning__list dmms-warning__form" onsubmit="return false">`);
    //            $.each(states[index] || [], function (index1) {//Duyệt từng trạng thái trong object states
    //                html.push(`<div class="kt-warning__item js-select-warning">
    //                                <div class="row" data-state="${index1}">
    //                                    <span class="kt-state-title text-uppercase col-md-4">${this}</span>`)

    //                //Hiển thị 3 mức độ theo từng state
    //                for (var i = 1; i <= 3; i++) {
    //                    html.push(`<div class="col-md-2 row kt-margin-r-20" data-level="${i}">
    //                                        <input name="warnings[][type]" value="${index}" type="hidden" />
    //                                        <input name="warnings[][uuid]" type="hidden" />
    //                                        <input name="warnings[][state]" value="${index1}" type="hidden" />
    //                                        <input name="warnings[][isEnable]" class="isEnable-change" value="true" type="hidden" />
    //                                        <input name="warnings[][warningLevel]" value="${i}" type="hidden" />
    //                                        <div class="col-md-8 text-uppercase"><span class="float-right">Mức độ ${i}</span></div>
    //                                        <input class="col-md-2 text-center" name="warnings[][warningValue]" value="0" />
    //                                        <span class="col-md-2 text-uppercase display-unit">GIỜ</span>
    //                                        <input name="warnings[][timeUnit]" value="2" type="hidden" />
    //                                    </div>`)
    //                }
    //                //Chọn mức giá trị thời gian
    //                html.push(`<div class="col-md-1">
    //                                <select class="text-uppercase js-select-unittime" data-type="${index}" data-state="${index1}">`);
    //                $.each(units || {}, function (i) {
    //                    html.push(`<option value="${i}" class="text-uppercase">${this.nameVN}</option>`);
    //                })
    //                html.push('</select>');
    //                html.push('</div>');

    //                //Icon tắt bật theo từng state
    //                html.push(`<div class="ml-auto" style="height: 23px;">
    //                                <span class="ml-auto kt-switch kt-switch--sm kt-switch--icon">
    //					    <label class="btn-on-off">
    //					        <input type="checkbox" checked="checked" class="js-on-off-state" data-type="${index}" data-state="${index1}">
    //					        <span></span>
    //					    </label>
    //					</span>
    //                            </div>`);
    //                html.push(`</div>`);
    //                html.push(`</div>`);
    //            });
    //            //html.push(`<div class= "container row">
    //            //               <div class="kt-margin-b-20  kt-margin-t-10"><i class="cpl-md-9">Lưu ý: Nếu không muốn nhận thông báo về các trạng thái. Bạn vui lòng đưa thời gian về con số [ 0 ]</i></div>
    //            //               <div class="ml-auto kt-margin-b-20  kt-margin-t-10">
    //            //                   <button type="button" class="btn btn-danger js-reset" data-type="${index}">Reset</button>
    //            //                   <button type="submit" class="btn btn-primary" data-type="${index}">Lưu cấu hình</button>
    //            //               </div>
    //            //           </div>`)

    //            //Lưu cấu hình
    //            html.push(`<div class= "row kt-warning-footer">
    //                            <div class="ml-auto">
    //                                <button type="submit" class="btn btn-primary" data-type="${index}">Lưu cấu hình</button>
    //                            </div>
    //                       </div>`)
    //            html.push(`</form>`)
    //            html.push(`</div>`)
    //        });

    //        this.$container.html(html.join(''));

    //        //$(".js-select-unit").selectpicker();

    //        //fill những giá trị đã tạo
    //        $.each(types || {}, function (ind) {
    //            const $checkbox = $(`.kt-warning`).find(`.kt-warning__title input[data-type="${ind}"]`);
    //            var check = false;

    //            $.each(this || {}, function (index, item) {
    //                const $unitTime = $(`form[data-type="${ind}"]`).find(`div[data-state="${item.state}"] .js-select-unittime`);
    //                const $checkboxState = $(`form[data-type="${ind}"]`).find(`div[data-state="${item.state}"] .js-on-off-state`);
    //                if (item.isEnable == true) {
    //                    check = true;
    //                }
    //                //fill value for select chooseen unitTime
    //                if (item.timeUnit !== '') $unitTime.val(item.timeUnit);

    //                if (item.isEnable === false) {
    //                    if ($checkboxState.prop("checked") === true) {
    //                        $checkboxState.removeAttr("checked");
    //                    }
    //                }
    //                else {
    //                    if ($checkboxState.prop("checked") === false) {
    //                        $checkboxState.attr("checked", "checked");
    //                    }
    //                }

    //                const $form = $(`form[data-type="${item.type}"]`).find(`div[data-state="${item.state}"]`).find(`div[data-level="${item.warningLevel}"]`);

    //                $form.find(`input[name="warnings[][uuid]"]`).val(`${item.uuid}`); //fill uuid của từng items
    //                $form.find(`input[name="warnings[][warningValue]"]`).val(`${item.warningValue}`); //fill warningValue của từng items
    //                $form.find(`input[name="warnings[][timeUnit]"]`).val(`${item.timeUnit}`); //fill timeUnit của từng items
    //                $form.find(`input[name="warnings[][isEnable]"]`).val(`${item.isEnable}`); //fill isEnable của từng items
    //                $form.find($(".display-unit")).text(`${units[item.timeUnit].nameVN}`); //Hiển thị giá trị thời gian timeUnit
    //            })
    //            if (check === true) {
    //                if ($checkbox.prop("checked") === false) {
    //                    $checkbox.attr("checked", "checked");
    //                }
    //            }
    //            else {
    //                if ($checkbox.prop("checked") === true) {
    //                    $checkbox.removeAttr("checked");
    //                }
    //            }
    //        })
    //    },

    //    select: function (type) {
    //        const $warning = $(`.js-warning[data-type="${type}"]`);

    //        const $parent = $warning.parent('.kt-warning');
    //        $parent.find('.active').removeClass('active');
    //        const $active = $parent.siblings('.active');
    //        $active.removeClass('active').find('.active').removeClass('active');

    //        const name = $warning.attr('data-name');
    //        this.$title.html(name);

    //        $parent.addClass('active');
    //        $warning.addClass('active');

    //        _this.warning_type = type;
    //    },
    //};

    var select = function (type) {
        const $warning = $(`.js-warning[data-type="${type}"]`);

        const $parent = $warning.parent('.kt-warning');
        $parent.find('.active').removeClass('active');
        const $active = $parent.siblings('.active');
        $active.removeClass('active').find('.active').removeClass('active');

        //const name = $warning.attr('data-name');
        //this.$title.html(name);

        $parent.addClass('active');
        $warning.addClass('active');

        //_this.warning_type = type;
    }

    var initValidation = function (type) {
        const $form = $(`form[data-type="${type}"]`);
        const name = $form.attr('data-name');
        $form.validate({
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                const count_data = data.warnings.length / 3;
                var check = true;
                for (var i = 1; i <= count_data; i++) {
                    const arrState = data.warnings.filter(function (st) { if (st.state == i) return st; });
                    var level1 = parseInt(arrState[0].warningValue);
                    var level2 = parseInt(arrState[1].warningValue);
                    var level3 = parseInt(arrState[2].warningValue);
                    if (level2 <= level1 || level3 <= level2) {
                        check = false;
                    }
                }
                if (check == true) {
                    $.ajax({
                        url: '/timewarning/index',
                        method: 'POST',
                        data,
                    })
                        .done(function () {
                            //$.notify(`Cấu hình ${name} thành công!`, { type: 'success' });
                            KTApp.swal({
                                text: `Cấu hình ${name} thành công!`,
                                icon: 'success',
                                aceptModeCustom: true,
                            }).then((confirm) => {
                                if (confirm) {
                                    window.location.pathname = "/timewarning";
                                }
                            });
                        })
                        .fail(function () {
                            //$.notify(`Cấu hình ${name} không thành công!`, { type: 'danger' });
                            KTApp.swal({
                                text: `Cấu hình ${name} không thành công!`,
                                icon: 'error',
                                dangerModeCustom: true,
                            });
                        });
                }
                else {
                    KTApp.swal({
                        text: 'Thời gian mức độ cảnh báo sau không được nhỏ hơn hay bằng mức độ cảnh báo trước. Vui lòng nhập lại!',
                        icon: 'error',
                        dangerModeCustom: true,
                    });
                }
                return false;
            },

        });
    }

    var saveState = function (type, isTrue, state, $this) {
        const $form = $(`form[data-type="${type}"]`);
        const data = $($form[0]).serializeJSON() || {};
        //const count_data = data.warnings.length / 3;
        var check = true;

        const arrState = data.warnings.filter(function (st) { if (st.state == state) return st; });
        data.warnings = arrState;
        var level1 = parseInt(arrState[0].warningValue);
        var level2 = parseInt(arrState[1].warningValue);
        var level3 = parseInt(arrState[2].warningValue);


        if (level2 <= level1 || level3 <= level2) {
            check = false
            //if (level1 == 0 && level2 == 0 && level3 == 0) {
            //    $.ajax({
            //        url: '/timewarning/index',
            //        method: 'POST',
            //        data,
            //    })
            //        .done(function () {
            //            const msg = isTrue === true ? "Bật cấu hình thành công!" : "Tắt cấu hình thành công!";
            //            window.location.pathname = "/timewarning";
            //            //$.notify(msg, { type: 'success' });
            //            KTApp.swal({
            //                text: msg,
            //                icon: 'success',
            //                dangerModeCustom: true,
            //            });
            //        })
            //        .fail(function () {
            //            const msg = isTrue === true ? "Bật cấu hình không thành công!" : "Tắt cấu hình không thành công!";
            //            //$.notify(msg, { type: 'danger' });
            //            KTApp.swal({
            //                text: msg,
            //                icon: 'error',
            //                dangerModeCustom: true,
            //            });
            //        });
            //}
            //else {
            //    KTApp.swal({
            //        text: 'Thời gian mức độ cảnh báo sau không được nhỏ hơn hay bằng mức độ cảnh báo trước. Vui lòng nhập lại!',
            //        icon: 'error',
            //        dangerModeCustom: true,
            //    });
            //}
        }
        if (check == true) {
            $.ajax({
                url: '/timewarning/index',
                method: 'POST',
                data,
            })
                .done(function () {
                    const msg = isTrue === true ? "Bật cấu hình thành công!" : "Tắt cấu hình thành công!";
                    //$.notify(msg, { type: 'success' });
                    KTApp.swal({
                        text: msg,
                        icon: 'success',
                        aceptModeCustom: true,
                    }).then((confirm) => {
                        if (confirm) {
                            window.location.pathname = "/timewarning";
                        }
                    });
                })
                .fail(function () {
                    if ($this.prop('checked') === true) {
                        $(`form[data-type="${type}"] div[data-state="${state}"] .isEnable-change`).val(false);
                        $(`form[data-type="${type}"] div[data-state="${state}"] .js-on-off-state`).prop('checked', false);
                    }
                    else {
                        $(`form[data-type="${type}"] div[data-state="${state}"] .isEnable-change`).val(true);
                        $(`form[data-type="${type}"] div[data-state="${state}"] .js-on-off-state`).prop('checked', true);
                    }
                    const msg = isTrue === true ? "Bật cấu hình không thành công!" : "Tắt cấu hình không thành công!";
                    //$.notify(msg, { type: 'danger' });
                    KTApp.swal({
                        text: msg,
                        icon: 'error',
                        dangerModeCustom: true,
                    });
                });
        }
        else {
            if ($this.prop('checked') === true) {
                $(`form[data-type="${type}"] div[data-state="${state}"] .isEnable-change`).val(false);
                $(`form[data-type="${type}"] div[data-state="${state}"] .js-on-off-state`).prop('checked', false);
            }
            else {
                $(`form[data-type="${type}"] div[data-state="${state}"] .isEnable-change`).val(true);
                $(`form[data-type="${type}"] div[data-state="${state}"] .js-on-off-state`).prop('checked', true);
            }
            KTApp.swal({
                text: 'Thời gian mức độ cảnh báo sau không được nhỏ hơn hay bằng mức độ cảnh báo trước. Vui lòng nhập lại!',
                icon: 'error',
                dangerModeCustom: true,
            });
        }
        return false;
    };

    var saveStateAll = function (type, isTrue, $this) {
        const $form = $(`form[data-type="${type}"]`);
        const data = $($form[0]).serializeJSON() || {};
        const count_data = data.warnings.length / 3;
        var check = true;

        for (var i = 1; i <= count_data; i++) {
            const arrState = data.warnings.filter(function (st) { if (st.state == i) return st; });
            var level1 = parseInt(arrState[0].warningValue);
            var level2 = parseInt(arrState[1].warningValue);
            var level3 = parseInt(arrState[2].warningValue);

            if (level2 <= level1 || level3 <= level2) {
                check = false;
                //if (level1 == 0 && level2 == 0 && level3 == 0) {
                //    $.ajax({
                //        url: '/timewarning/index',
                //        method: 'POST',
                //        data,
                //    })
                //        .done(function () {
                //            const msg = isTrue === true ? "Bật cấu hình thành công!" : "Tắt cấu hình thành công!";
                //            window.location.pathname = "/timewarning";
                //            //$.notify(msg, { type: 'success' });
                //            KTApp.swal({
                //                text: msg,
                //                icon: 'success',
                //                dangerModeCustom: true,
                //            });
                //        })
                //        .fail(function () {
                //            const msg = isTrue === true ? "Bật cấu hình không thành công!" : "Tắt cấu hình không thành công!";
                //            //$.notify(msg, { type: 'danger' });
                //            KTApp.swal({
                //                text: msg,
                //                icon: 'error',
                //                dangerModeCustom: true,
                //            });
                //        });
                //}
                //else {
                //    KTApp.swal({
                //        text: 'Thời gian mức độ cảnh báo sau không được nhỏ hơn hay bằng mức độ cảnh báo trước. Vui lòng nhập lại!',
                //        icon: 'error',
                //        dangerModeCustom: true,
                //    });
                //}
            }
        }

        if (check == true) {
            $.ajax({
                url: '/timewarning/index',
                method: 'POST',
                data,
            })
                .done(function () {
                    const msg = isTrue === true ? "Bật cấu hình thành công!" : "Tắt cấu hình thành công!";
                    //$.notify(msg, { type: 'success' });
                    KTApp.swal({
                        text: msg,
                        icon: 'success',
                        aceptModeCustom: true,
                    }).then((confirm) => {
                        if (confirm) {
                            window.location.pathname = "/timewarning";
                        }
                    });
                })
                .fail(function () {
                    const msg = isTrue === true ? "Bật cấu hình không thành công!" : "Tắt cấu hình không thành công!";
                    //$.notify(msg, { type: 'danger' });
                    if ($this.prop('checked') === true) {
                        $(`form[data-type="${type}"] .isEnable-change`).val(false);
                        $(`form[data-type="${type}"] .js-on-off-state`).prop('checked', false);
                    }
                    else {
                        $(`form[data-type="${type}"] .isEnable-change`).val(true);
                        $(`form[data-type="${type}"] .js-on-off-state`).prop('checked', true);
                    }
                    KTApp.swal({
                        text: msg,
                        icon: 'error',
                        dangerModeCustom: true,
                    });
                });
        }
        else {
            if ($this.prop('checked') === true) {
                $(`#js-on-off[data-type="${type}"]`).prop('checked', false);
                $(`form[data-type="${type}"] .isEnable-change`).val(false);
                $(`form[data-type="${type}"] .js-on-off-state`).prop('checked', false);
            }
            else {
                $(`#js-on-off[data-type="${type}"]`).prop('checked', true);
                $(`form[data-type="${type}"] .isEnable-change`).val(true);
                $(`form[data-type="${type}"] .js-on-off-state`).prop('checked', true);
            }
            KTApp.swal({
                text: 'Thời gian mức độ cảnh báo sau không được nhỏ hơn hay bằng mức độ cảnh báo trước. Vui lòng nhập lại!',
                icon: 'error',
                dangerModeCustom: true,
            });
        }
        return false;
    };

    var checkAll = function () {
        const warning_count = $('.kt-warning').length;
        for (var i = 0; i < warning_count; i++) {
            const count_checked = $(`.kt-warning__list[data-type="${i}"]`).find('.js-on-off-state[checked="checked"]').length;
            debugger;
            if (count_checked > 0) {
                $(`#js-on-off[data-type="${i}"]`).prop('checked', true);
                $(`.dmms-warning__form[data-type="${i}"]`).find('button[type="submit"]').attr('disabled', false);
            }
            else {
                $(`#js-on-off[data-type="${i}"]`).prop('checked', false);
                $(`.dmms-warning__form[data-type="${i}"]`).find('button[type="submit"]').attr('disabled', true);
            }
        }
    };

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-warning')
            .on('click', '.js-warning', function () {
                const type = $(this).attr('data-type');

                initValidation(type);
                select(type);
            })
            .on('change', '#js-on-off', function () {
                const $this = $(this);
                const type = $this.attr('data-type');
                var isTrue = true;

                if ($this.prop('checked') === true) {
                    $(`form[data-type="${type}"] .isEnable-change`).val(true);
                    $(`form[data-type="${type}"] .js-on-off-state`).prop('checked', true);

                    saveStateAll(type, isTrue, $this);
                }
                else {
                    $(`form[data-type="${type}"] .isEnable-change`).val(false);
                    $(`form[data-type="${type}"] .js-on-off-state`).prop('checked', false);

                    saveStateAll(type, false, $this);
                }
            })
            .on('change', '.js-on-off-state', function () {
                const $this = $(this);
                const type = $this.attr('data-type');
                const state = $this.attr('data-state');
                var isTrue = true;

                if ($this.prop('checked') === true) {
                    $(`form[data-type="${type}"] div[data-state="${state}"] .isEnable-change`).val(true);
                    $(`form[data-type="${type}"] div[data-state="${state}"] .js-on-off-state`).prop('checked', true);

                    saveState(type, isTrue, state, $this);
                }
                else {
                    $(`form[data-type="${type}"] div[data-state="${state}"] .isEnable-change`).val(false);
                    $(`form[data-type="${type}"] div[data-state="${state}"] .js-on-off-state`).prop('checked', false);

                    saveState(type, false, state, $this);
                }
            })
            .on('change', '.js-select-unittime', function () {
                const $this = $(this);
                const type = $this.attr('data-type');
                const state = $this.attr('data-state');
                const $form = $(`form[data-type="${type}"]`).find(`div[data-state="${state}"]`);

                var EnumUnitState = { 1: 'Phút', 2: 'Giờ', 3: 'Ngày', 4: 'Tuần', 5: 'Tháng' };

                if ($this.val() !== '') {
                    debugger;
                    $form.find('input[name="warnings[][unit]"]').val($this.val());
                    $form.find('.display-unit').html(EnumUnitState[$this.val()]);
                }
            });
    };

    return {
        // Public functions
        init: function () {
            initEventListeners();
            checkAll();
            //warning.load();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-timewarning').length) {
        DMMSTimeWarning.init();
    }
});