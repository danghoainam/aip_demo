﻿"use strict";

// Class definition 
var DMMSReportChart = function () {
    let tfrom = moment().subtract(6, 'days').format('YYYY-MM-DD').toString();
    let tto = moment().format('YYYY-MM-DD').toString();
    var daterangepickerInit = function () {
        if ($('#kt_dashboard_daterangepicker').length == 0) {
            return;
        }
        var picker = $('#kt_dashboard_daterangepicker');
        var start = moment().subtract(6, 'days');
        var end = moment();
        function cb(start, end, label) {
            var title = '';
            var range = '';
            if ((end - start) < 100 || label == 'Hôm nay') {
                title = 'Hôm nay: ';
                range = start.format('DD-MM-YYYY');
            } else if (label == 'Hôm qua') {
                title = 'Hôm qua: ';
                range = start.format('DD-MM-YYYY');
            } else {
                range = start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY');
            }
            tfrom = start.format('YYYY-MM-DD');
            tto = end.format('YYYY-MM-DD');
            $('#charpie1').remove();
            $('.charpie1').append('<div id="charpie1"></div>');
            $('#charpie2').remove();
            $('.charpie2').append('<div id="charpie2"></div>');
            $('#charpie3').remove();
            $('.charpie3').append('<div id="charpie3"></div>');
            $('#chartmix').remove();
            $('.chartmix').append('<div id="chartmix"></div>');
            $('#chartstack1').remove();
            $('.chartstack1').append('<div id="chartstack1"></div>');
            $('#chartstack2').remove();
            $('.chartstack2').append('<div id="chartstack2"></div>');
            pieChartProjectInit(tfrom, tto);
            columnChartTicketInit(tfrom, tto);
            stackChartTaskInit(tfrom, tto);
            $('#kt_dashboard_daterangepicker_date').html(range);
            $('#kt_dashboard_daterangepicker_title').html(title);
        }
        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Hôm nay': [moment(), moment()],
                'Hôm qua': [moment().subtract(1, 'days'), moment()],
                '7 ngày trước': [moment().subtract(6, 'days'), moment()],
                '30 ngày trước': [moment().subtract(29, 'days'), moment()],
                'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            locale: {
                applyLabel: 'Áp dụng',
                cancelLabel: 'Hủy bỏ',
                customRangeLabel: 'Tùy chỉnh',
                firstDay: 1
            }
        }, cb);
    }

    var optionscolumn = {
        series: [{
            //  type: 'column',
            name: 'Thiết bị sửa chữa: 95',
            data: [5, 20, 15, 25, 30, 35, 40]
        }, {
            //    type: 'line',
            name: 'Thiết bị thay thế: 110',
            data: [20, 10, 5, 30, 40, 10, 25]
        },
        {
            //    type: 'line',
            name: 'Thiết bị bảo hành: 77',
            data: [15, 5, 20, 35, 20, 15, 45]
        }

        ],
        chart: {
            type: 'bar',
            //type: 'line',
            height: 350,
            toolbar: {
                show: false
            }
        },
        stroke: {
            width: [0, 4]
        },
        colors: ['#feb019', '#008ffb', '#ff4560'],
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: '80%',
                // endingShape: 'rounded'
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
        },
        xaxis: {
            categories: ['Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug'],
        },
        yaxis: {/*-- >
            @* title: {
        text: '$ (thousands)'
    }*@
        < !--*/},
        fill: {
            opacity: 1,
            colors: ['#feb019', '#008ffb', '#ff4560']
        },
        tooltip: {
            y: {
                formatter: function (val) {
                    return "$ " + val + " thousands"
                }
            }
        }
    };

    var optionsline = {
        series: [{
            name: 'series1',
            data: [31, 40, 28, 51, 42, 109, 100]
        }],
        chart: {
            height: 350,
            type: 'area',
            toolbar: {
                show: false
            }
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'smooth'
        },

        fill: {
            type: 'gradient',
            gradient: {
                opacityFrom: 0.6,
                opacityTo: 0.8,
            }
        },
        xaxis: {
            type: 'datetime',
            categories: ["2018-09-19T00:00:00.000Z", "2018-09-19T01:30:00.000Z", "2018-09-19T02:30:00.000Z", "2018-09-19T03:30:00.000Z", "2018-09-19T04:30:00.000Z", "2018-09-19T05:30:00.000Z", "2018-09-19T06:30:00.000Z"],

        },

        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        },
    };

    var optionshorizontal = {
        series: [{
            name: "sales",
            data: [300, 430, 258, 100, 540]
        }],
        chart: {
            type: 'bar',
            height: 350,
            toolbar: {
                show: false
            }
        },

        plotOptions: {
            bar: {
                horizontal: true,

            }
        },
        dataLabels: {
            enabled: false,

        },
        xaxis: {
            categories: ['South Korea', 'Canada', 'United Kingdom', 'Netherlands', 'Italy'
            ],

        },/*-- >
           @* annotations: {
        position: 'back',
            }*@
    

        < !--*/};


    var initBieuDoNgang = function () {
        var horizontal = new ApexCharts(document.querySelector("#horizontal"), optionshorizontal);
        horizontal.render();
    }

    var initBieuDoLine = function () {
        var chartline = new ApexCharts(document.querySelector("#chartline"), optionsline);
        chartline.render();
    }

    var initBieuDoCot1 = function () {
        var optionscolumn1 = {
            series: [{
                name: 'Thiết bị sửa chữa',
                data: [5, 20, 15, 25, 30, 35, 40]
            }, {
                name: 'Thiết bị thay thế',
                data: [20, 10, 5, 30, 40, 10, 25]
            },
            {
                name: 'Thiết bị bảo hành',
                data: [15, 5, 20, 35, 20, 15, 45]
            }

            ],
            chart: {
                type: 'bar',
                height: 350,
                toolbar: {
                    show: false
                }
            },
            stroke: {
                width: [0, 4]
            },
            colors: ['#FF931E', '#924FDA', '#0085FF'],
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '80%',
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: ['Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug'],
            },
            yaxis: {},
            fill: {
                opacity: 1,
                colors: ['#FF931E', '#924FDA', '#0085FF']
            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return val
                    }
                }
            }
        };

        var chartcolumn1 = new ApexCharts(document.querySelector("#chartcolumn1"), optionscolumn1);
        chartcolumn1.render();
    }

    var initBieuDoCot2 = function () {
        var chartcolumn2 = new ApexCharts(document.querySelector("#chartcolumn2"), optionscolumn);
        chartcolumn2.render();
    }

    var initBieuDoCot3 = function () {
        var chartcolumn3 = new ApexCharts(document.querySelector("#chartcolumn3"), optionscolumn);
        chartcolumn3.render();
    }

    var initBieuDoStack1 = function (categories, data1, data2, data3) {
        // state 1,2 : chưa xử lý
        //      8: Hoàn thành   
        // Còn lại đang xử lý
        var optionsstacked1 = {
            series: [{
                name: 'Chưa tiếp nhận',
                data: data1
            }, {
                name: 'Đang xử lý',
                data: data2
            }, {
                name: 'Đã hoàn thành',
                data: data3
            }],
            chart: {
                type: 'bar',
                height: 350,
                stacked: true,
                toolbar: {
                    show: false
                },
            },
            yaxis: {
                labels: {
                    formatter: function (val) {
                        return val.toFixed(0)
                    }
                }
            },
            colors: ['#36B37E', '#FF931E', '#EB5757'],
            responsive: [{
                breakpoint: 480,
                options: {
                    legend: {
                        position: 'bottom',
                        offsetX: -10,
                        offsetY: 0
                    }
                }
            }],
            plotOptions: {
                bar: {
                    horizontal: false,
                },
            },
            dataLabels: {
                enabled: false
            },
            xaxis: {
                //  type: 'datetime',
                categories: categories,
            }, fill: {
                opacity: 1
            }
        };
        var chartstack1 = new ApexCharts(document.querySelector("#chartstack1"), optionsstacked1);
        chartstack1.render();
    }

    var initBieuDoStack2 = function (categories, data1, data2, data3) {
        var optionsstacked2 = {
            series: [{
                name: 'Chưa xử lý',
                data: data1
            }, {
                name: 'Đang xử lý',
                data: data2
            }, {
                name: 'Đã hoàn thành',
                data: data3
            }],
            chart: {
                type: 'bar',
                height: 350,
                stacked: true,
                toolbar: {
                    show: false
                },
            },
            yaxis: {
                labels: {
                    formatter: function (val) {
                        return val.toFixed(0)
                    }
                }
            },
            colors: ['#36B37E', '#FF931E', '#EB5757'],
            responsive: [{
                breakpoint: 480,
                options: {
                    legend: {
                        position: 'bottom',
                        offsetX: -10,
                        offsetY: 0
                    }
                }
            }],
            plotOptions: {
                bar: {
                    horizontal: false,
                },
            },
            dataLabels: {
                enabled: false
            },
            xaxis: {
                //  type: 'datetime',
                categories: categories,
            }, fill: {
                opacity: 1
            }
        };
        var chartstack2 = new ApexCharts(document.querySelector("#chartstack2"), optionsstacked2);
        chartstack2.render();
    }

    var initBieuDoMix = function (categories, data1, data2, data3) {
        var optionsmix = {
            series: [{
                name: 'Yêu cầu tạo bởi NVKT',
                type: 'column',
                data: data1
            }, {
                name: 'Yêu cầu tạo từ CSKH',
                type: 'column',
                data: data2
            }, {
                name: 'Tổng',
                type: 'line',
                data: data3
            }],
            chart: {
                height: 350,
                type: 'line',
                // stacked: false,
                toolbar: {
                    show: false
                }
            },
            colors: ['#FF931E', '#36B37E', '#189EFF'],
            dataLabels: {
                enabled: false
            },
            xaxis: {
                categories: categories,
            },
            yaxis: {
                labels: {
                    formatter: function (val) {
                        return val.toFixed(0)
                    }
                }
            },
            stroke: {
                width: [1, 1, 4],

            },
            markers: {
                size: 4,
                colors: ["#FFFFFF"],
                strokeColors: "#189EFF",
                strokeWidth: 2,
                hover: {
                    size: 7,
                }
            },
            tooltip: {
                fixed: {
                    enabled: true,
                    position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
                    offsetY: 30,
                    offsetX: 60,                   
                },
            },
            legend: {
                markers: {
                    radius: 0
                }
               /* horizontalAlign: 'left',
                offsetX: 40*/
            }
        };
        var chartmix = new ApexCharts(document.querySelector("#chartmix"), optionsmix);
        chartmix.render();
    }

    var innitBieuDoTron1 = function (dataSeries, total) {
        let optionsDuAn = {
            series: dataSeries,
            chart: {
                width: 280,
                type: 'pie',
            },
            colors: ['#F64E60', '#8950FC', '#FFA800', '#36B37E'],
            legend: {
                display: true,
                show: false,
            },
            labels: ['Giáo dục & đào tạo', 'Y tế & sức khỏe', 'Xử lý môi trường', 'Quan trắc môi trường'],
            dataLabels: {
                enabled: true,    
                style: {
                    colors: ['#fff'],
                    fontSize: '14px',
                },
            },           
            plotOptions: {
                pie: {
                    dataLabels: {                                                                   
                        minAngleToShowLabel: 10,
                        offset: -5,
                    }, 
                }
            },
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                }
            }],
            noData: {
                text: "Không có dữ liệu hiển thị báo cáo",
                align: 'center',
                verticalAlign: 'middle',
                offsetX: 0,
                offsetY: 0,
                style: {
                    fontSize: '14px',
                }
            },
            tooltip: {
                enabled: true,
                y: {
                    formatter: function (value) {
                        return (value / total * 100).toFixed(1) + "% (" + value + ")";
                    }
                }
            },

        };
        var charpie1 = new ApexCharts(document.querySelector("#charpie1"), optionsDuAn);
        charpie1.render();
    }

    var innitBieuDoTron2 = function (dataSeries, total) {
        let optionsDonVi = {
            series: dataSeries,
            chart: {
                width: 280,
                type: 'pie',
            },
            colors: ['#F64E60', '#8950FC', '#FFA800', '#36B37E'],
            legend: {
                display: true,
                show: false,
            },
            labels: ['Giáo dục & đào tạo', 'Y tế & sức khỏe', 'Xử lý môi trường', 'Quan trắc môi trường'],
            dataLabels: {
                style: {
                    colors: ['#fff'],
                    fontSize: '14px',
                },
                // offsetX: 50,
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        minAngleToShowLabel: 10,
                        offset: -5,
                    },
                }
            },
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                }
            }],
            noData: {
                text: "Không có dữ liệu hiển thị báo cáo",
                align: 'center',
                verticalAlign: 'middle',
                offsetX: 0,
                offsetY: 0,
                style: {
                    fontSize: '14px',
                }
            },
            tooltip: {
                enabled: true,
                y: {
                    formatter: function (value) {
                        return (value / total * 100).toFixed(1) + "% (" + value + ")";
                    }
                }
            },
        };
        var charpie2 = new ApexCharts(document.querySelector("#charpie2"), optionsDonVi);
        charpie2.render();
    }

    var innitBieuDoTron3 = function (dataSeries, total) {
        let optionsThietBi = {
            series: dataSeries,
            chart: {
                width: 280,
                type: 'pie',
            },
            colors: ['#F64E60', '#8950FC', '#FFA800', '#36B37E'],
            legend: {
                display: true,
                show: false,
            },
            labels: ['Giáo dục & đào tạo', 'Y tế & sức khỏe', 'Xử lý môi trường', 'Quan trắc môi trường'],
            dataLabels: {
                style: {
                    colors: ['#fff'],
                    fontSize: '14px',
                    
                },
                // offsetX: 50,
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        minAngleToShowLabel: 10,
                        offset: -5,
                    },
                }
            },
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                }
            }],
            noData: {
                text: "Không có dữ liệu hiển thị báo cáo",
                align: 'center',
                verticalAlign: 'middle',
                offsetX: 0,
                offsetY: 0,
                style: {
                    fontSize: '14px',
                }
            },
            tooltip: {
                enabled: true,
                y: {
                    formatter: function (value) {
                        return (value / total * 100).toFixed(1) + "% (" + value + ")";
                    }
                }
            },
        };
        var charpie3 = new ApexCharts(document.querySelector("#charpie3"), optionsThietBi);
        charpie3.render();
    }

    var pieChartProjectInit = function (timeFrom, timeTo) {
        let seriesThongKeDuAn = [];
        let seriesDonVi = [];
        let seriesThietBi = [];
        let totalDuAn = 0;
        let totalDonVi = 0;
        let totalThietBi = 0;
        KTApp.block($('.w-100'));
        $.ajax({
            url: `/projectoverview/all`,
            method: 'POST',
            cache: false,
            data: {
                request: {
                    timeFrom: timeFrom, timeTo: timeTo
                }
            }
        })
            .done(function (res) {
                if (res) {
                    if (res.data.items != null) {
                        $.each(res.data.items, function (index, item) {
                            if (index == 0) {
                                $("#duantrienkhai").text(item.data[0].count);
                            };
                            if (index == 1) {
                                item.data.forEach(function (item) {
                                    seriesThongKeDuAn.push(item.count);
                                    totalDuAn += parseInt(item.count);
                                });
                            };
                            if (index == 2) {
                                $("#donvitrienkhai").text(item.data[0].count);
                            };
                            if (index == 3) {
                                item.data.forEach(function (item) {
                                    seriesDonVi.push(item.count);
                                    totalDonVi += parseInt(item.count);
                                });
                            };
                            if (index == 4) {
                                $("#sanphamtrienkhai").text(item.data[0].count);
                            };
                            if (index == 6) {
                                $("#thietbitrienkhai").text(item.data[0].count);
                            };
                            if (index == 7) {
                                item.data.forEach(function (item) {
                                    seriesThietBi.push(item.count);
                                    totalThietBi += parseInt(item.count);
                                });
                            };
                        });
                    };
                    innitBieuDoTron1(seriesThongKeDuAn, totalDuAn);
                    innitBieuDoTron2(seriesDonVi, totalDonVi);
                    innitBieuDoTron3(seriesThietBi, totalThietBi);
                    //$("#duantrienkhai").text(totalDuAn);
                    //$("#donvitrienkhai").text(totalDonVi);
                    KTApp.unblock($('.w-100'));
                }
            })
            .fail(function () {
                $.notify('Có lỗi xảy ra, vui lòng thử lại.', { type: 'danger' });
            });
    }

    var stackChartTaskInit = function (timeFrom, timeTo) {
        let categoriesTask1 = [];
        let data1Task1 = [];
        let data2Task1 = [];
        let data3Task1 = [];
        var checkShow1 = 0;
        let categoriesTask2 = [];
        let data1Task2 = [];
        let data2Task2 = [];
        let data3Task2 = [];
        var checkShow2 = 0;
        $.ajax({
            url: `/taskoverview/all`,
            method: 'POST',
            cache: false,
            data: {
                request: {
                    timeFrom: timeFrom, timeTo: timeTo
                }
            }
        })
            .done(function (res) {
                //var data = res.
                $.each(res, function (index, item) {
                    var bookmark1Data1 = 0;
                    var bookmark1Data2 = 0;
                    var bookmark1Data3 = 0;
                    var bookmark2Data1 = 0;
                    var bookmark2Data2 = 0;
                    var bookmark2Data3 = 0;
                    checkShow1 = 0;
                    checkShow2 = 0;
                    $.each(item, function (index, item) {
                        //Công việc khắc phục sự cố
                        if (item.type == "1" || item.type == 100) {
                            if (item.state == 100) {
                                bookmark1Data1 += parseInt(item.count);
                                bookmark1Data2 += parseInt(item.count);
                                bookmark1Data3 += parseInt(item.count);
                                checkShow1 = 1;
                            }
                            else
                                if (item.state == "8") {
                                    bookmark1Data3 += parseInt(item.count);
                                    checkShow1 = 1;
                                }
                                else
                                    if (item.state == "1" || item.state == "2") {
                                        bookmark1Data1 += parseInt(item.count);
                                        checkShow1 = 1;
                                    }
                                    else {
                                        bookmark1Data2 += parseInt(item.count);
                                        checkShow1 = 1;
                                    }
                        }
                        //Công việc bảo trì - Bảo dưỡng
                        if (item.type == "3" || item.type == 100) {
                            if (item.state == 100) {
                                bookmark2Data1 += parseInt(item.count);
                                bookmark2Data2 += parseInt(item.count);
                                bookmark2Data3 += parseInt(item.count);
                                checkShow2 = 1;
                            }
                            else
                                if (item.state == "8") {
                                    bookmark2Data3 += parseInt(item.count);
                                    checkShow2 = 1;
                                }
                                else
                                    if (item.state == "1" || item.state == "2") {
                                        bookmark2Data1 += parseInt(item.count);
                                        checkShow2 = 1;
                                    }
                                    else {
                                        bookmark2Data2 += parseInt(item.count);
                                        checkShow2 = 1;
                                    }
                        }
                        
                    });
                    if (checkShow1 == 1) {
                        categoriesTask1.push(index);
                        data1Task1.push(parseInt(bookmark1Data1));
                        data2Task1.push(parseInt(bookmark1Data2));
                        data3Task1.push(parseInt(bookmark1Data3));
                    }
                    if (checkShow2 == 1) {
                        categoriesTask2.push(index);
                        data1Task2.push(parseInt(bookmark2Data1));
                        data2Task2.push(parseInt(bookmark2Data2));
                        data3Task2.push(parseInt(bookmark2Data3));
                    }
                });
                console.log(data1Task1[data1Task1.length-2]);
                let sumTask1 = data1Task1[data1Task1.length - 2] + data2Task1[data2Task1.length - 2] + data3Task1[data3Task1.length - 2];
                let sumTask2 = data1Task2[data1Task2.length - 2] + data2Task2[data2Task2.length - 2] + data3Task2[data3Task2.length - 2];
                if (data1Task1.length == 1) sumTask1 = 0;
                if (data1Task2.length == 1) sumTask2 = 0;
                $("#tongSoTask1").text(sumTask1);
                $("#tongSoTask2").text(sumTask2); 
                initBieuDoStack1(categoriesTask1, data1Task1, data2Task1, data3Task1);
                initBieuDoStack2(categoriesTask2, data1Task2, data2Task2, data3Task2);
            })
            .fail(function () {
                $.notify('Có lỗi xảy ra, vui lòng thử lại.', { type: 'danger' });
            });
    }

    var columnChartTicketInit = function (timeFrom, timeTo) {
        let categoriesTicket = [];
        let data1Ticket = [];
        let data2Ticket = [];
        let data3Ticket = [];
        var checkShow = 0;
        $.ajax({
            url: `/ticketoverview/all`,
            method: 'POST',
            cache: false,
            data: {
                request: {
                    timeFrom: timeFrom, timeTo: timeTo
                }
            }
        })
            .done(function (res) {
                if (res) {
                    var sumTiket = 0;
                    //var data = res.
                    $.each(res, function (index, item) {
                        var bookmarkData1 = 0;
                        var bookmarkData2 = 0;
                        var bookmarkTotal = 0;
                        checkShow = 0;
                        $.each(item, function (index, item) {
                            if (item.type == 100) {
                                bookmarkData1 += parseInt(item.count);
                                bookmarkData2 += parseInt(item.count);
                                bookmarkTotal += parseInt(item.count);
                                checkShow = 1;
                            }
                            if (item.type == "1") {
                                bookmarkData1 += parseInt(item.count);
                                bookmarkTotal += parseInt(item.count);
                                checkShow = 1;
                            }
                            if (item.type == "2") {
                                bookmarkData2 += parseInt(item.count);
                                bookmarkTotal += parseInt(item.count);
                                checkShow = 1;
                            }
                        });
                        if (checkShow == 1) {
                            categoriesTicket.push(index);
                            data1Ticket.push(parseInt(bookmarkData1));
                            data2Ticket.push(parseInt(bookmarkData2));
                            data3Ticket.push(parseInt(bookmarkTotal));
                            sumTiket += bookmarkTotal;
                        }
                    });
                    $("#tongSoTicket").text(sumTiket);
                    initBieuDoMix(categoriesTicket, data1Ticket, data2Ticket, data3Ticket);
                }
            })
            .fail(function () {
                $.notify('Có lỗi xảy ra, vui lòng thử lại.', { type: 'danger' });
            });
    }

    return {
        init: function () {
            $('#kt_dashboard_daterangepicker_title').text("");
            $("#kt_dashboard_daterangepicker_date").text("7 ngày trước");
            pieChartProjectInit(tfrom, tto);
            columnChartTicketInit(tfrom, tto);
            stackChartTaskInit(tfrom, tto);
            initBieuDoCot1();
            initBieuDoCot2();
            initBieuDoCot3();
            initBieuDoNgang();
            initBieuDoLine();
            daterangepickerInit();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-reportoverview').length) {
        DMMSReportChart.init();
    }
});