"use strict";
// Class definition

var DMMSTicket = function () {
    // Private functions
    var dataTableInstance = null;

    var drawer = null;
    var arr = [];
    var LIMIT = 20;
    var START_PAGE = 0;
    function subTableInit(e) {
        //for (var property in e.data.task) {
        //    if (e.data.task[property]) {
        //        arr.push(e.data.task[property])
        //    }
        //}
        var tast = [e.data.task]
        
        $('<div/>').attr('id', 'child_data_local_' + e.data.uuid).appendTo(e.detailCell).KTDatatable({
            data: {
                type: 'local',
                source: tast,
                contentType: 'application/json',
                
            },
           
            // layout definition
            layout: {
                scroll: true,
                height: 300,
                footer: false,

                // enable/disable datatable spinner.
                spinner: {
                    type: 1,
                    theme: 'default',
                },
            },
           
            sortable: false,
            pagination: false,
            responsive: true,
            // columns definition
            columns: [
                {
                    field: 'code',
                    title: 'Mã công việc',
                    width: 80,
                },
                {
                    field: 'name',
                    title: 'Tên công việc',
                    template: function (data) {
                        var output = '<strong>' + data.name + '</strong>';
                        if (data.description) {
                            output += '<br/>' + data.description + '</span>';
                        }
                        return output;
                    },
                },
                {
                    field: 'staff.name',
                    title: 'Thông tin kỹ thuật phụ trách',
                    template: function (data) {
                        if (data.staff != null) {
                            var output = `<div class="kt-user-card__details">
                                          <strong>${data.staff.name}</strong><br>`;
                            if (data.contact_phone_number != null) {
                                output += `<span>${data.contact_phone_number}</span></div>`
                            }

                            return output;
                        }
                        return `Chưa có người phụ trách`;
                    }
                },
                {
                    field: 'state',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    width: 110,
                    template: function (data) {
                        debugger
                        var states = {
                            1: { 'title': 'Chưa tiếp nhận', 'class': 'kt-badge--danger' },
                            2: { 'title': 'Yêu cầu hỗ trợ', 'class': ' kt-badge--warning' },
                            3: { 'title': 'Đã tiếp nhận', 'class': ' kt-badge--primary' },
                            4: { 'title': 'Đã hẹn lịch', 'class': ' kt-badge--info' },
                            5: { 'title': 'Đang xử lý', 'class': ' kt-badge--danger' },
                            6: { 'title': 'Đợi duyệt đề xuất xử lý', 'class': ' kt-badge--dark' },
                            7: { 'title': 'Yêu cầu tiếp tục', 'class': ' kt-badge--info' },
                            8: { 'title': 'Hoàn thành', 'class': ' kt-badge--success' },
                            9: { 'title': 'Cập nhật số lượng TB', 'class': ' kt-badge--success' },
                            10: { 'title': 'Hủy', 'class': ' kt-badge--danger' },
                        };
                        if (data.state) {
                            return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill">' + states[data.state].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới tạo</span>';
                        }
                    }
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 50,
                    overflow: 'visible',
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        return `
                           <a href="/task/update/${data.uuid}" title="Chi tiết" class="btn btn-sm btn-success btn-icon btn-icon-md">
                                <i class="la la-info"></i>
                            </a>  
                        `;
                    },
                }],

        });
       console.log(tast)
    };
    // back up button
    //<button data-id="${data.uuid}" title="Chi tiết" class="js-detail-task btn btn-sm btn-success btn-icon btn-icon-md">
    //    <i class="la la-info"></i>
    //</button>
    //var subTableInit = function (e) {
    //    $('<div/>').attr('id', 'child_data_local_' + e.data.uuid).appendTo(e.detailCell).KTDatatable({
    //        data: {
    //            type: 'local',
    //            source: e.data.task,
    //            pageSize: 10,
    //        },

    //        // layout definition
    //        layout: {
    //            scroll: true,
    //            height: 300,
    //            footer: false,

    //            // enable/disable datatable spinner.
    //            spinner: {
    //                type: 1,
    //                theme: 'default',
    //            },
    //        },

    //        sortable: true,

    //        // columns definition
    //        columns: [
    //            {
    //                field: 'name',
    //                title: 'name',
    //                width: 100
    //            }, {
    //                field: 'code',
    //                title: 'code',
    //            }, {
    //                field: 'ShipName',
    //                title: 'Ship Name',
    //            }, {
    //                field: 'TotalPayment',
    //                title: 'Payment',
    //                type: 'number',
    //            }, {
    //                field: 'Status',
    //                title: 'Status',
    //                // callback function support for column rendering
    //                template: function (row) {
    //                    var status = {
    //                        1: { 'title': 'Pending', 'class': 'kt-badge--brand' },
    //                        2: { 'title': 'Delivered', 'class': ' kt-badge--danger' },
    //                        3: { 'title': 'Canceled', 'class': ' kt-badge--primary' },
    //                        4: { 'title': 'Success', 'class': ' kt-badge--success' },
    //                        5: { 'title': 'Info', 'class': ' kt-badge--info' },
    //                        6: { 'title': 'Danger', 'class': ' kt-badge--danger' },
    //                        7: { 'title': 'Warning', 'class': ' kt-badge--warning' },
    //                    };
    //                    return '<span class="kt-badge ' + status[row.Status].class + ' kt-badge--inline kt-badge--pill">' + status[row.Status].title + '</span>';
    //                },
    //            },
    //            {
    //                field: 'Type',
    //                title: 'Type',
    //                autoHide: false,
    //                // callback function support for column rendering
    //                template: function (row) {
    //                    var status = {
    //                        1: { 'title': 'Online', 'state': 'danger' },
    //                        2: { 'title': 'Retail', 'state': 'primary' },
    //                        3: { 'title': 'Direct', 'state': 'success' },
    //                    };
    //                    return '<span class="kt-badge kt-badge--' + status[row.Type].state + ' kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-' +
    //                        status[row.Type].state + '">' +
    //                        status[row.Type].title + '</span>';
    //                },
    //            }],
    //    });
    //};
    // demo initializer
    var initTable = function () {
        dataTableInstance = $('.kt-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/ticket/gets',
                    },
                    response: {
                        map: function (res) {
                            debugger
                            if (res.code == 404 || res.error == 1) {
                                $.notify('Không kết nối được đến máy chủ', { type: 'danger' });
                            }
                            const raw = res && res.data || {};
                            
                            if (raw.items != null) {
                                $.each(raw.items, function (index, item) {
                                    for (var property in item) {
                                        if (item[property] && typeof item[property] === "object") {
                                            $.each(item[property], function (i, v) {
                                                for (var pro in item[property]) {
                                                    if (item[property][pro] && typeof item[property][pro] === "string") {
                                                        item[property][pro] = item[property][pro].replace(/</g, "&lt;").replace(/>/g, "&gt;");
                                                    }
                                                }
                                            })
                                        }
                                        if (item[property] && typeof item[property] === "string") {
                                            item[property] = item[property].replace(/</g, "&lt;").replace(/>/g, "&gt;");
                                        }
                                    }
                                })
                            }
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? undefined : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    fail: function (data) {
                        alert('xx')
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { customer_uuid, creator_uuid, state, keyword, priority } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            customer_uuid: customer_uuid || $('#js-filter-customer').val(),
                            creator_uuid: creator_uuid || $('#js-filter-creator').val(),
                            state: state || $('#js-filter-state').val(),
                            priority: priority || $('#js-filter-priority').val(),
                            keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            page: pagination.page
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },
            layout: {
                scroll: true
            },
            search: {
                onEnter: true,
            },
            detail: {
                title: 'Đang tải dữ liệu ...',
                content: subTableInit,
            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    sortable: false,
                    textAlign: 'center',
                    width: 20,
                    //template: function (data, type, row) {
                    //    return row.index() + 1;
                    //}
                },
                {
                    field: 'title',
                    title: 'Nội dung yêu cầu',

                    template: function (data) {                        
                        var subconten = '';
                        var countstring = data.content.split(" ");
                        if (countstring.length > 10) {
                            for (var i = 0; i < countstring.length; i++) {
                                if (i <= 10) {
                                    subconten = subconten + countstring[i] + " ";
                                }
                                else {
                                    subconten = subconten + "..."; break;
                                }
                            }

                        }
                        else {
                            subconten = data.content
                        }
                        var output = '<div class="kt-user-card__details">\
                                               <span><strong>' + data.title + '</strong></span><br>\
                                               <span>' + subconten +'</span>';
                                        //<span>Tên khách hàng: </span><strong>' + data.customer.name + '</strong><br>';
                        //if (data.contact_name) {
                        //    output += '<span>Tên liên hệ: </span><strong>' + data.contact_name + '</strong><br>';
                        //}
                        //if (data.contact_phone_number) {
                        //    output += '<span>Số điện thoại: </span><strong>' + data.contact_phone_number + '</strong><br>';
                        //}
                        output += "</div>";
                        return output;
                    },

                },
                {
                    field: '#',
                    title: 'Đơn vị-Người liên hệ',

                    template: function (data) {
                        var output = '';
                        if (data.customer != null) {
                            output += '<div class="kt-user-card__details">\
                                        <a href = "/customer/update/'+data.customer.uuid+'"><strong>'+ data.customer.name + '</strong></a><br>';
                            if (data.contact_name != null) {
                                output += '<span>' + data.contact_name + '</span>';
                            }
                            else {
                                output = output;
                            }
                            if (data.contact_phone_number != null) {
                                output += ' - <span>' + data.contact_phone_number + '</span><br>';
                            }
                            else {
                                output = output
                            }
                            output += "</div>";
                        }
                        else {
                            output += '<div class="kt-user-card__details">\
                                        <strong> Chưa có đơn vị </strong><br>';
                        }

                        return output;
                    },  
                },
                //{
                //    field: 'customer.name',
                //    title: 'Đơn vị',

                //},
                {
                    field: 'creator.name',
                    title: 'Thông tin người tạo',
                    width: 150,
                    template: function (data) {
                        var output = '<div class="kt-user-card__details">';
                        if (data.creator !== null && data.creator.name !== '') {
                            output += '<a href = "/staff/update/' + data.creator.uuid + '"><span><strong>' + data.creator.name + '</strong></span ></a> <br>';
                        }
                        output += '<span>' + data.time_created_format + '</span><br>';
                        //if (data.time_last_update_format) {
                        //    output += '<span>Cập nhật cuối: </span><span>' + data.time_last_update_format + '</span><br>';
                        //}
                        output += "</div>";
                        return output;
                    }
                },
                {
                    field: 'priority',
                    title: 'Độ ưu tiên',
                    width: 80,
                    textAlign: 'center',
                    template: function (row) {
                        var priorities = {
                            1: { 'title': 'Khẩn cấp', 'class': ' kt-badge--danger' },
                            2: { 'title': 'Ưu tiên', 'class': ' kt-badge--warning' },
                            3: { 'title': 'Bình thường', 'class': 'kt-badge--primary' },
                        };
                        if (row.priority) {
                            return '<p class="kt-badge ' + priorities[row.priority].class + ' kt-badge--inline kt-badge--pill">' + priorities[row.priority].title + '</p>';
                        }
                    }
                },
                {
                    field: 'state',
                    title: 'Trạng thái',

                    textAlign: 'center',
                    width: 110,

                    template: function (row) {
                        var states = {
                            1: { 'title': 'Chưa tiếp nhận', 'class': 'kt-badge--danger' },
                            2: { 'title': 'Đã tiếp nhận', 'class': ' kt-badge--warning' },
                            3: { 'title': 'Đang xử lý', 'class': ' kt-badge--primary' },
                            4: { 'title': 'Đã xử lý xong', 'class': ' kt-badge--primary' },
                            5: { 'title': 'Hoàn thành', 'class': ' kt-badge--success' },
                            6: { 'title': 'Đã hủy', 'class': ' kt-badge--danger' },
                        };
                        if (row.state) {
                            return '<p class="kt-badge ' + states[row.state].class + ' kt-badge--inline kt-badge--pill">' + states[row.state].title + '</p>';
                        }
                        else {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới</span>';
                        }
                    }
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 70,
                    overflow: 'visible',
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        return `
                            <a href="ticket/edit/${data.uuid}" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </a>
                            <button data-id="${data.uuid}" type="button" title="Xóa" class="js-delete-ticket btn btn-sm btn-danger btn-icon btn-icon-md">
                                <i class="la la-trash"></i>
                            </button>
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };

    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var initSearch = function () {
        //const creator = $('#js-filter-creator');
        //creator.selectpicker();
        //const count_keyword = 2;
        //$('#search-creator .bs-searchbox input').attr('placeholder', `Nhập ${count_keyword} ký tự trở lên rồi enter!`)
        //$('#search-creator .bs-searchbox input').on('keyup', function (event) {
        //    const keyword = $(this).val();
        //    if (keyword !== "" && keyword.length >= count_keyword) {
        //        if (event.which == 13) {
        //            creator.html('').append('<option value="">Lựa chọn</option>');
        //            creator.attr('title', 'Chọn người tạo');
        //            creator.selectpicker('refresh');
        //            $.ajax({
        //                url: `/ticket/getstaffajax/${keyword}`,
        //                method: 'GET'
        //            })
        //                .done(function (data) {
        //                    $.each(data.data.items, function (index, item) {
        //                        creator.append(`<option value="${item.uuid}">${item.name}</option>`);
        //                        creator.selectpicker('refresh');
        //                    })
        //                })
        //        }
        //    }
        //    else {
        //        creator.html('').append('<option value="">Lựa chọn</option>');
        //        creator.attr('title', 'Chọn người tạo');
        //        creator.selectpicker('refresh');
        //    }
        //});

        $('#js-filter-creator').on('change', function () {
            if (dataTableInstance) {
                dataTableInstance.search($(this).val().toLowerCase(), 'creator_uuid');
                START_PAGE = 1;
            }
        }).selectpicker();
        $('#js-filter-customer')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'customer_uuid');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-state')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'state');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });
        $('#js-filter-priority')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'priority');
                    START_PAGE = 1;
                }
            })
            .selectpicker();

        if ($("#customer_uuid").val() !== '') {
            $('#kt-select-customer').selectpicker('val', $("#customer_uuid").val());
        }
        else {
            $('#kt-select-customer').selectpicker();
        }

        if ($("#state_id").val() !== '') {
            $('#kt-ticket-state-select').selectpicker('val', $("#state_id").val());
            $('#kt-task-state-select').selectpicker('val', $("#state_id").val());
        }
        else {
            $('#kt-ticket-state-select').selectpicker();
            $('#kt-task-state-select').selectpicker();
        }

        if ($("#staff_uuid").val() !== '') {
            $('#kt-staff-select').selectpicker('val', $("#staff_uuid").val());
        }
        else {
            $('#kt-staff-select').selectpicker();
        }

        if ($("#priority").val() !== '') {
            $('#kt-priority-select').selectpicker('val', $("#priority").val());
        }
        else {
            $('#kt-priority-select').selectpicker();
        }

        if ($("#device_uuid_str").val() !== '') {
            $('#kt-select-device').selectpicker('val', $("#device_uuid_str").val());
        }
        else {
            $('#kt-select-device').selectpicker();
        }
    };

    var deleteTicket = function (uuid) {
        if (uuid) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa ticket...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `ticket/details/${uuid}`,
                method: "GET",
            })
                .done(function (res) {
                    if (res.data.rating == null && res.data.state == 1) {
                        $.ajax({
                            url: `/ticket/delete/${uuid}`,
                            method: 'DELETE',
                        })
                            .done(function (data) {
                                loading.hide();
                                KTApp.unblockPage();
                                $.notify(data.msg, { type: 'success' });

                                reloadTable();
                            })
                            .fail(function (data) {
                                loading.hide();
                                KTApp.unblockPage();
                                $.notify(data.responseJSON.msg, { type: 'danger' });
                            });
                    }
                    else {
                        loading.hide();
                        KTApp.unblockPage();
                        $.notify('Không thể xóa ticket đã đóng hoặc đã tiếp nhận!', { type: 'danger' });
                    }
                })
            
        }
    };
    var cancelTicket = function (uuid) {
        debugger
        var form = $(".reason");
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang hủy ticket...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/ticket/cancel/${uuid}`,
                method: "GET",
                data: form.serializeJSON(),
            })
                .done(function (res) {
                                loading.hide();
                                KTApp.unblockPage();
                                $.notify('Đóng yêu cầu thành công !', { type: 'success' });
                            window.location = window.location.origin + `/ticket/edit/${uuid}`;
                            })
                            .fail(function (data) {
                                loading.hide();
                                KTApp.unblockPage();
                                $.notify(data.responseJSON.msg, { type: 'danger' });
                            });
                    
                    
                
            
        
    };

    var initDetailTicketDrawer = function (uuid) {
        const title = uuid ? 'Chi tiết ticket' : '';
        drawer = new KTDrawer({ title, uuid: uuid || 'new', className: 'dmms-drawer--detailTicket' });

        drawer.on('ready', () => {
            $.ajax({
                url: `/ticket/details/${uuid}`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });
        drawer.open();
    }

    var initDetailDrawer = function (id) {
        const title = id ? 'Chi tiết công việc' : '';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--detailtask' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/task/detail/${id}` : '',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });
        drawer.open();
    };

    var initTicketDrawer = function (uuid) {
        const title = uuid ? 'Sửa thông tin yêu cầu' : 'Thêm yêu cầu mới';
        drawer = new KTDrawer({ title, uuid: uuid || 'new', className: 'dmms-drawer--ticket' });

        drawer.on('ready', () => {
            $.ajax({
                url: uuid ? `/ticket/edit/${uuid}` : 'ticket/create',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });
        drawer.open();
    }

    var initTaskDrawer = function (ticketUuid, taskUuid) {
        const title = taskUuid ? 'Sửa thông tin công việc' : 'Thêm công việc mới';
        drawer = new KTDrawer({ title, uuid: taskUuid || 'new', className: 'dmms-drawer--task' });
        var url = `/task/upsert/${ticketUuid}`;
        if (taskUuid) {
            url += `/${taskUuid}`
        }
        drawer.on('ready', () => {
            $.ajax({
                url: url,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    }

    var initRateDrawer = function (uuid) {

        const title = "ĐÁNH GIÁ QUY TRÌNH XỬ LÝ SỰ CỐ CỦA KHÁCH HÀNG";
        drawer = new KTDrawer({ title, uuid: uuid || 'new', className: 'dmms-drawer--rateticket' });

        drawer.on('ready', () => {
            $.ajax({
                url: window.location.origin + `/ticket/rateticket/${uuid}`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initRateContent($content, uuid);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    }

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-ticket')
            .on('click', '.js-delete-ticket', function () {
                const $this = $(this);
                const uuid = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Ticket sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteTicket(uuid);
                    }
                });
            })
            .off('click', '.cancel-ticket')
            .on('click', '.cancel-ticket', function () {
                const $this = $(this);
                const uuid = $this.attr('data-id');
                var input = document.createElement("textarea");
                input.className = "reason form-control";
                input.cols = "40";
                input.rows = "5";
                input.name = ("reason");
                KTApp.swal({
                  
                    text: 'Lý do hủy yêu cầu khỏi hệ thống.',
                    content: input,
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        debugger
                        cancelTicket(uuid);
                    }
                });
            })
            //.on('click', '.js-detail-task', function () {
            //    const $this = $(this);
            //    const id = $this.attr('data-id');            
            //    initDetailDrawer(id || '');
            //})
            .on('click', '.js-detail-ticket', function () {
                const $this = $(this);
                const uuid = $this.attr('data-id');

                initDetailTicketDrawer(uuid || '');
            })
            .off('click', '.js-edit-ticket')
            .on('click', '.js-edit-ticket', function (res) {
                const $this = $(this);
                const uuid = $this.attr('data-id');

                initTicketDrawer(uuid || '');
            })
            .off('click', '.js-edit-task')
            .on('click', '.js-edit-task', function () {
                const $this = $(this);
                const taskUuid = $this.attr('data-id');
                const ticketUuid = $this.attr('data-ticketid');

                initTaskDrawer(ticketUuid || '', taskUuid || '');
            })
            .off('click', '.js-rate-ticket')
            .on('click', '.js-rate-ticket', function () {
                const $this = $(this);
                const uuid = $this.attr('data-id');

                initRateDrawer(uuid || '');
            });
    };

    var initRateForm = function ($form, ticket_uuid) {
        $form.validate({
            rules: {
                rating: {
                    required: true,
                },
            },
            //messages: {
            //    rating: {
            //        required: 'Vui lòng đánh giá yêu cầu!',
            //    }
            //},
            errorPlacement: function (error, element) {
                console.log(element.val());
                if (element.val() === '') {
                    $.notify('Vui lòng nhập đánh giá yêu cầu!', { type: 'danger' });
                }       
            },

            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                $.ajax({
                    url: `/ticket/rateticket/${uuid}`,
                    method: 'POST',
                    data,
                })
                    .done(function (data) {
                        KTApp.block($form);
                        //const successMsg = 'Đánh giá thành công';
                        $.notify(data.msg, { type: 'success' });

                        /**
                         * Close drawer
                         */
                        drawer.close();

                        window.location = window.location.origin + `/ticket/edit/${ticket_uuid}`;
                    })
                    .fail(function (data) {
                        KTApp.unblock($form);
                        //const errorMsg = 'Đánh giá không thành công!';
                        $.notify(data.responseJSON.msg, { type: 'danger' });
                    });
                return false;
            },
        });
    }

    var initRateContent = function ($content, ticket_uuid) {
        const $form = $content.find('.dmms-rate__form');
        initRateForm($form, ticket_uuid);

        $('#kt-close').on('click', function () {
            /**
             * Close drawer
             */
            drawer.close();
        });

        /*
            Init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

        //Rating ticket
        $(document).ready(function () {
            /* 1. Visualizing things on Hover - See next part for action on click */
            $('#stars li').on('mouseover', function () {
                var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

                // Now highlight all the stars that's not after the current hovered star
                $(this).parent().children('li.star').each(function (e) {
                    if (e < onStar) {
                        $(this).addClass('hover');
                    }
                    else {
                        $(this).removeClass('hover');
                    }
                });

            }).on('mouseout', function () {
                $(this).parent().children('li.star').each(function (e) {
                    $(this).removeClass('hover');
                });
            });

            /* 2. Action to perform on click */
            $('#stars li').on('click', function () {
                var onStar = parseInt($(this).data('value'), 10); // The star currently selected
                var stars = $(this).parent().children('li.star');

                for (var i = 0; i < stars.length; i++) {
                    $(stars[i]).removeClass('selected');
                }

                for (var i = 0; i < onStar; i++) {
                    $(stars[i]).addClass('selected');
                }

                // JUST RESPONSE (Not needed)
                var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
                responseMessage(ratingValue);
            });
        });

        function responseMessage(ratingValue) {
            $('#value-rate').val(ratingValue);
        }
    }

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-ticket__form');
        const $formTask = $content.find('.dmms-task__form');
        initTaskForm($formTask);
        initTicketForm($form);

        initDropzone();
        /*
            Init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

        //if ($("#device_uuid_str").val() !== '') {
        //    $('#kt-multiple-select-device').selectpicker('val', JSON.parse($("#device_uuid_str").val()));
        //}
        //else {
        //    $('#kt-multiple-select-device').selectpicker();
        //}
        if ($("#customer_uuid").val() !== '') {
            $('#kt-select-customer').selectpicker('val', $("#customer_uuid").val());
        }
        else {
            $('#kt-select-customer').selectpicker();
        }

        if ($("#state_id").val() !== '') {
            $('#kt-ticket-state-select').selectpicker('val', $("#state_id").val());
            $('#kt-task-state-select').selectpicker('val', $("#state_id").val());
        }
        else {
            $('#kt-ticket-state-select').selectpicker();
            $('#kt-task-state-select').selectpicker();
        }

        if ($("#staff_uuid").val() !== '') {
            $('#kt-staff-select').selectpicker('val', $("#staff_uuid").val());
        }
        else {
            $('#kt-staff-select').selectpicker();
        }

        if ($("#priority").val() !== '') {
            $('#kt-priority-select').selectpicker('val', $("#priority").val());
        }
        else {
            $('#kt-priority-select').selectpicker();
        }

        $('#kt-select-device').selectpicker();

        $('#kt-select-contact').selectpicker();
    };

    var initTaskForm = function ($form) {
        if ($("#deadline_str").val() !== '') {
            $('.kt_datepicker_2').datepicker().datepicker('setDate', new Date($("#deadline_str").val()));
        }
        else {
            $('.kt_datepicker_2').datepicker().datepicker('setDate', new Date());
        }

        $form.validate({
            rules: {
                name: {
                    required: true,
                },
                description: {
                    required: true,
                },
                state: {
                    required: true
                },
                contact_phone_number: {
                    phoneVN: true,
                },
            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên công việc.'
                },

                description: {
                    required: 'Vui lòng nhập mô tả cho công việc.'
                },
                state: {
                    required: 'Vui lòng chọn trạng thái của công việc.'
                },
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {

                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                $.ajax({
                    url: uuid ? `/task/update/` : '/task/create',
                    method: uuid ? 'PUT' : 'POST',
                    data,
                })
                    .done(function () {
                        KTApp.block($form);
                        const successMsg = uuid ? 'Sửa thông tin thành công!' : 'Thêm công việc thành công!';
                        $.notify(successMsg, { type: 'success' });

                        /**
                         * Close drawer
                         */
                        drawer.close();

                        reloadTable();
                    })
                    .fail(function () {
                        KTApp.unblock($form);
                        const errorMsg = uuid ? 'Sửa thông tin không thành công!' : 'Thêm công việc không thành công!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }

    //auto fill
    $(document)
        .on('change', '#kt-select-customer', function () {
            var id = $(this).val();
            var wrapper = $("#kt-select-contact"); //Fields wrapper
            var wrapper_device = $("#kt-select-device"); //Fields wrapper device
            
            //Refresh select box #kt-select-contact
            $(wrapper).html('');
            $(wrapper_device).html('');

            //Refresh input name và phone_number
            $('#variant_name').val('');
            $('#variant_phone').val('');

            //fill contacts
            if (id != '') {
                $.ajax({
                    url: `/customer/getContactAjax/${id}`,
                    method: 'GET',
                })
                    .done(function (data) {
                        debugger
                        $(wrapper).append('<option value="">Lựa chọn liên hệ</option>');
                        if (data.length !== 0) {
                            $(data).each(function (index, item) {
                                $(wrapper).append('<option value="' + item.uuid + '">' + item.name + '</option>');
                            });
                        }
                        //Refresh selectpicker
                        $(wrapper).selectpicker('refresh');
                    })
            }
            else {
                $(wrapper).append('<option value="">Lựa chọn liên hệ</option>');

                //Refresh selectpicker
                $(wrapper).selectpicker('refresh');
            }

            //fill devices
            if (id != '') {
                debugger;
                $.ajax({
                    url: `/device/getslist`,
                    method: 'GET',
                    data: {
                        customer_uuid: id,
                       
                    },
                })
                    .done(function (data) {
                        debugger
                        $(wrapper_device).append('<option value="">Lựa chọn thiết bị</option>');
                        if (data != null) {
                            $(data.data.items).each(function (index, item) {
                                $(wrapper_device).append('<option value="' + item.uuid + '" data-subtext="' + item.name + ' - ' + 'Serial:' + item.serial + ' - ' + 'Ngày HH:' + item.time_valid_end_format + '">' + item.prefix_name + '</option>');
                            });
                        }
                        //Refresh selectpicker
                        $(wrapper_device).selectpicker('refresh');
                    })
            }
            else {
                $(wrapper_device).append('<option value="">Lựa chọn thiết bị</option>');

                //Refresh selectpicker
                $(wrapper_device).selectpicker('refresh');
            }
        });

    //Auto fill name và phone_number khi select từ #kt-select-contact
    $(document).on('change', '#kt-select-contact', function () {
        console.log($(this).val())
        //Refresh input name và phone_number
        $('#variant_name').val('');
        $('#variant_phone').val('');
        var text =  $("#kt-select-contact option:selected").text();
        var subname = text.substr(0, text.indexOf('-')); 
        var subphone = text.split('-').pop();
        //var contact_uuid = $(this).val();
        //if (contact_uuid != '') {
        //    $.ajax({
        //        url: `/customer/detailContactAjax/${contact_uuid}`,
        //        method: 'GET',
        //        dataType: "json",
        //        success: function (res) {
        //            $('#variant_name').val(res.data.name);
        //            $('#variant_phone').val(res.data.phone_number);
        //        }
        //    })
        //}
        $('#variant_name').val(subname.trim());
        $('#variant_phone').val(subphone.trim());
        
    });

    //Auto fill name và phone_number khi select từ #kt-select-device
    $(document).on('change', '#kt-select-device', function () {
        debugger
        const $this = $(this);
        const uuid = $this.val();
        var warapper = $('#js-create-device');
        //var name = $("#kt-select-device option:selected").text()
        var name = $(this).find(':selected')[0].dataset.subtext;

        
        $("#kt-select-device option[value='" + $this.val() + "']").attr("disabled", true);
        $("#kt-select-device").selectpicker('refresh');
            
        warapper.append(`<div class="form-group" data-id="${uuid}">
                            <input name="devices[][device_id]" value="${uuid}" type="hidden" />
                            <div class="form-group form-group-last">
                                <label class="control-label">${name}</label>
                                <div>
                                    <div class="dropzone dropzone-default" id="kt_dropzone_image_${uuid}" data-id="${uuid}">

                                        <div class="dropzone-items">
                                            <div class="dropzone-item" style="display:none">
                                                <div class="dropzone-file">
                                                    <div class="dropzone-filename" title="some_image_file_name.jpg">
                                                        <span data-dz-name></span>
                                                    </div>
                                                    <div class="dropzone-error" data-dz-errormessage></div>
                                                </div>
                                                <div class="dropzone-progress">
                                                    <div class="progress">
                                                        <div class="progress-bar kt-bg-brand" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress></div>
                                                    </div>
                                                </div>
                                                <div class="dropzone-toolbar">
                                                    <span class="dropzone-delete" data-dz-remove><i class="flaticon2-cross"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="form-text text-muted">Dung lượng file lớn nhất 5MB</span>
                                </div>
                            </div>
                            <input name="devices[][image_uuids]" id="image_uuids_${uuid}" type="hidden" />
                            <input name="devices[][images[]]" type="hidden" />
                        </div>`)
        initDropzone(uuid);
                  
       
    });

    var initTicketForm = function ($form) {
        initValidation(true);
    }
    var initValidation = function (isForm) {
        if ($('#contact_uuid').val() !== '') {
            $('#kt-select-contact').selectpicker('val', $('#contact_uuid').val());
        }
        else {
            $('#kt-select-contact').selectpicker();
        }

        $(".dmms-ticket__form").validate({
            rules: {
                title: {
                    required: true,
                },
                customer_uuid: {
                    required: true,
                },
                content: {
                    required: true,
                },

            },
            messages: {
                title: {
                    required: 'Vui lòng nhập tiêu đề yêu cầu.'
                },
                customer_uuid: {
                    required: 'Vui lòng nhập tên đơn vị.'
                },
                content: {
                    required: 'Vui lòng nhập mô tả cho yêu cầu.'
                },
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo('.dmms-ticket__form', -200);
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                $.ajax({
                    url: uuid ? `/ticket/edit/${uuid}` : '/ticket/create',
                    method: uuid ? 'PUT' : 'POST',
                    data,
                })
                    .done(function (data) {
                        KTApp.block('.dmms-ticket__form');
                        //const successMsg = uuid ? 'Sửa thông tin thành công!' : 'Thêm yêu cầu thành công!';
                        $.notify(data.msg, { type: 'success' });

                        if (isForm) {
                            drawer.close();
                            reloadTable();
                        } else {
                            /**
                              * Close drawer
                              */
                            //localStorage['edit_success'] = JSON.stringify({ from: (uuid ? `/staff/update/` : '/staff/create'), massage: successMsg });

                            window.location = window.location.origin + `/ticket`;
                        }
                    })
                    .fail(function (data) {
                        KTApp.unblock('.dmms-ticket__form');
                        $.notify(data.responseJSON.msg, { type: 'danger' });
                    });
                return false;
            },
        });
    }


    var initDropzone = function (uuid) {
        var dropZone = $(`#kt_dropzone_image_${uuid}`);
        if (dropZone !== undefined && dropZone.length > 0) {
            new Dropzone('#kt_dropzone_image_' + uuid , {
                url: "/images/upload",
                paramName: "images",
                maxFiles: 5,
                maxFilesize: 5, // MB
                addRemoveLinks: true,
                dictDefaultMessage: 'Bấm hoặc kéo thả ảnh vào đây để tải ảnh lên',
                dictRemoveFile: 'Xóa ảnh',
                dictCancelUpload: 'Hủy',
                dictCancelUploadConfirmation: 'Bạn thực sự muốn hủy?',
                dictMaxFilesExceeded: 'Bạn không thể tải thêm ảnh!',
                dictFileTooBig: "Ảnh tải lên dung lượng quá lớn ({{filesize}}MB). Max filesize: {{maxFilesize}}MB.",
                autoProcessQueue: true,
                accept: function (file, done) {
                    done();
                },
                success: function (file, response) {
                    if (response != null && response.length > 0) {
                        var imageId = $("#image_uuids_" + uuid).val();
                        if (imageId == '') {
                            imageId = response[0].id;
                        }
                        else {
                            imageId += "," + response[0].id;
                        }
                        $("#image_uuids_" + uuid).val(imageId);
                    }
                    else {
                        $(file.previewElement).addClass("dz-error").find('.dz-error-message').text("Không tải được ảnh lên.");
                    }
                },
                error: function (file, response, errorMessage) {
                    if (file.accepted) {
                        $(file.previewElement).addClass("dz-error").find('.dz-error-message').text("Có lỗi xảy ra, không tải được ảnh lên.");
                    }
                    else {
                        $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(response);
                    }
                }
            });
        }
    }

    return {
        // Public functions
        init: function () {
            initTable();
            initSearch();
            initEventListeners();
            initValidation();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-ticket').length) {
        DMMSTicket.init();
    }
});
