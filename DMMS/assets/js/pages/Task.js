﻿"use strict";
// Class definition

var DMMSTask = function () {
    // Private functions

    var dataTableInstance = null;
    var dataTableInstanceDevice = null;

    var drawer = null;
    var CURRENT_PAGE = 1;
    var LIMIT = 20;
    var MAX_ITEM_INPAGE = 10;
    var START_PAGE = 0;

    var arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    // demo initializer
    var initTable = function () {
        var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
        if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
            $.notify(editSucessCache.massage, { type: 'success' });
            localStorage.removeItem('edit_success');
        }
        dataTableInstance = $('.kt-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: `/task/gets`,
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            if (raw.items != null) {
                                $.each(raw.items, function (index, item) {
                                    for (var property in item) {
                                        if (item[property] && typeof item[property] === "object") {
                                            $.each(item[property], function (i, v) {
                                                for (var pro in item[property]) {
                                                    if (item[property][pro] && typeof item[property][pro] === "string") {
                                                        item[property][pro] = item[property][pro].replace(/</g, "&lt;").replace(/>/g, "&gt;");
                                                    }
                                                }
                                            })
                                        }
                                        if (item[property] && typeof item[property] === "string") {
                                            item[property] = item[property].replace(/</g, "&lt;").replace(/>/g, "&gt;");
                                        }
                                    }
                                })
                            }
                            const perpage = pagination === undefined ? undefined : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page
                            CURRENT_PAGE = page || 1;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { owner_uuid, staff_uuid, state, keyword, priority } = query;
                        MAX_ITEM_INPAGE = pagination.perpage || LIMIT;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }                       
                        return {
                            limit: MAX_ITEM_INPAGE,
                            owner_uuid: owner_uuid || $('#js-filter-ticket').val(),
                            staff_uuid: staff_uuid || $('#js-filter-staff').val(),
                            state: state || $('#js-filter-state').val().toString(),
                            priority: priority || $('#js-filter-priority').val(),
                            stateIds: state || $('#js-filter-state').val().toString(),
                            keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,

            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 30,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * MAX_ITEM_INPAGE;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'code',
                    title: 'Mã CV',
                    width:65
                },
                {
                    field: 'name',
                    title: 'Công việc',
                    template: function (data) {
                        var subconten = '';
                        if (data.description != null) {
                            var countstring = data.description.split(" ");
                            if (countstring.length > 10) {
                                for (var i = 0; i < countstring.length; i++) {
                                    if (i <= 10) {
                                        subconten = subconten + countstring[i] + " ";
                                    }
                                    else {
                                        subconten = subconten + "..."; break;
                                    }
                                }

                            }
                            else {
                                subconten = data.description
                            }
                            var output = subconten
                            var checktype = '';
                            if (data.type == 1) {
                                checktype = 'Xử lý Ticket'
                            }
                            if (data.type == 2) {
                                checktype = 'Xử lý CV'
                            }
                            if (data.type == 3) {
                                checktype = 'Xử lý kế hoạch CV'
                            }
                            var outputdone = '<strong>' + checktype + ' ' + data.code + '</strong>';
                            if (data.name != null) {
                                outputdone += '<br/></span>' + data.name + '</span>'
                            }
                            if (output != null) {
                                outputdone += '<br/></span>' + output + '</span>'
                            }
                            //<span>Tên khách hàng: </span><strong>' + data.customer.name + '</strong><br>';
                            //if (data.contact_name) {
                            //    output += '<span>Tên liên hệ: </span><strong>' + data.contact_name + '</strong><br>';
                            //}
                            //if (data.contact_phone_number) {
                            //    output += '<span>Số điện thoại: </span><strong>' + data.contact_phone_number + '</strong><br>';
                            //}

                            return outputdone;
                        }
                        return subconten;
                    },
                },   
                {
                    field: 'customer.name',
                    title: 'Đơn vị',
                    
                },
                {
                    field: 'staff.name',
                    title: 'Tên nhân viên',
                    title: 'Người phụ trách',
                    width: 120,
                    textAlign: 'center',
                },
                {
                    field: 'time_created_format',
                    title: 'Ngày tạo',
                    textAlign: 'center',
                    width: 110,
                    
                },
                {
                    field: 'priority',
                    title: 'Độ ưu tiên',
                    width: 80,
                    textAlign:'center',
                    autoHide: false,
                    template: function (row) {
                        var priorities = {
                            1: { 'title': 'Khẩn cấp', 'class': ' kt-badge--danger' },
                            2: { 'title': 'Ưu tiên', 'class': ' kt-badge--warning' },
                            3: { 'title': 'Bình thường', 'class': 'kt-badge--primary' },
                        };
                        if (row.priority) {
                            return '<p class="kt-badge ' + priorities[row.priority].class + ' kt-badge--inline kt-badge--pill">' + priorities[row.priority].title + '</p>';
                        }
                    }
                },
                {
                    field: 'state',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    width: 110,
                    autoHide: false,
                    template: function (data) {
                        var states = {
                            1: { 'title': 'Chưa tiếp nhận', 'class': 'kt-badge--danger' },
                            2: { 'title': 'Yêu cầu hỗ trợ', 'class': ' kt-badge--danger' },
                            3: { 'title': 'Đã tiếp nhận', 'class': 'kt-badge--primary'  },
                            4: { 'title': 'Đã hẹn lịch', 'class': ' kt-badge--info' },
                            5: { 'title': 'Đang xử lý', 'class': ' kt-badge--warning' },
                            6: { 'title': 'Đợi đề xuất', 'class': ' kt-badge--warning' },
                            7: { 'title': 'Yêu cầu tiếp tục', 'class': ' kt-badge--success' },
                            8: { 'title': 'Hoàn thành', 'class': ' kt-badge--success' },
                            9: { 'title': 'Cập nhật số lượng TB', 'class': ' kt-badge--success' },
                            10: { 'title': 'Hủy', 'class': ' kt-badge--danger' },
                        };
                        if (data.state) {
                            return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill">' + states[data.state].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge ' + states[1].class + ' kt-badge--inline kt-badge--pill">' + states[1].title + '</span>';
                        }
                    }
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,                 
                    width: 50,                    
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        //var ticketUuid = '';
                        //if (data.ticket !== null && data.ticket.uuid) {
                        //    ticketUuid = data.ticket.uuid;
                        //}
                        return `
                           <a href="/task/update/${data.uuid}" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </a>                           
                        `;
                    },
                }],
            layout: {
                scroll: !0,
                height: null,
                footer: !1
            },
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };
    //<button data-id="${data.uuid}" type="button" title="Xóa" class="js-delete-task btn btn-sm btn-danger btn-icon btn-icon-md">
    //    <i class="la la-trash"></i>
    //</button>
    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var initSearch = function () {
        $('#js-filter-ticket')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'owner_uuid');
                    START_PAGE = 1;
                }
            }).selectpicker();
            $('#js-filter-staff')
                .on('change', function () {
                    if (dataTableInstance) {
                        dataTableInstance.search($(this).val().toLowerCase(), 'staff_uuid');
                        START_PAGE = 1;
                    }
                }).selectpicker();
        $('#js-filter-priority')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'priority');
                    START_PAGE = 1;
                }
            }).selectpicker();


        var stateIds = [""];

        if ($("#stateIds").val() !== '' && $("#stateIds").val() !== undefined) {
            stateIds = $("#stateIds").val().split(",");
        }

        $('#js-filter-state')
            .on('change', function () {
                $('#ckbTest:checkbox:checked').length > 0 ? $('#ckbTest').val(1) : $('#ckbTest').val() == null;
                $('#ckbTest2:checkbox:checked').length > 0 ? $('#ckbTest2').val(2) : $('#ckbTest2').val(null);
                if (dataTableInstance) {
                    console.log($(this).val());
                    var stateArr = $(this).val();
                    dataTableInstance.search(stateArr.toString(), 'state');
                    // tam dong
                    //dataTableInstance.search($(this).val(), 'state');
                    START_PAGE = 1;
                }
            })
            .selectpicker('val', stateIds);
        $('#kt-staff-select').selectpicker();
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });
    };

    var deleteTask = function (uuid) {
        if (uuid) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa công việc...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/task/delete/${uuid}`,
                method: 'DELETE',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa công việc thành công!', { type: 'success' });

                    reloadTable();
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa công việc không thành công!', { type: 'danger' });
                });
        }
    };

    var initTaskDrawer = function (ticketUuid, taskUuid) {
        const title = taskUuid ? 'Sửa thông tin công việc' : 'Thêm công việc mới';
        drawer = new KTDrawer({ title, uuid: taskUuid || 'new', className: 'dmms-drawer--task' });
       
        var url = `/task/upsert`;
        if (taskUuid && ticketUuid) {
            url += `/${ticketUuid}/${taskUuid}`
        } else if (taskUuid && !ticketUuid) {
            url += `/${taskUuid}`
        }
        drawer.on('ready', () => {
            $.ajax({
                url: url,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    }

    var initDetailDrawer = function (id) {
        const title = id ? 'Chi tiết công việc' : '';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--detailtask' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/task/detail/${id}` : '',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    var initChangeStateDrawer = function (id) {
        const title = 'Thay đổi trạng thái';
        drawer = new KTDrawer({ title, uuid: id || 'new', className: 'dmms-drawer--task' });

        drawer.on('ready', () => {
            $.ajax({
                url: `/task/changestate/${id}`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    }

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-task')
            .on('click', '.js-delete-task', function () {
                const $this = $(this);
                const uuid = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Công việc sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteTask(uuid);
                    }
                });
            })
            .off('click', '.js-edit-task')
            .on('click', '.js-edit-task', function () {
                const $this = $(this);
                const uuid = $this.attr('data-id');
                const taskUuid = $this.attr('data-id');
                const ticketUuid = $this.attr('data-ticketid');
                initTaskDrawer(ticketUuid || '', taskUuid || '');
            })
            .on('click', '.js-detail-task', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                initDetailDrawer(id || '');
            })
            .on('click', '#js-change-state', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                initChangeStateDrawer(id || '');
            });
    };
    var initForm = function ($form) {
        initValidation(true);
    }
   var initValidation = function (isForm) {
        if ($("#deadline_str").val() !== '') {
            $('.kt_datepicker_2').datepicker().datepicker('setDate', new Date($("#deadline_str").val()));
        }
        else {
            $('.kt_datepicker_2').datepicker().datepicker('setDate', new Date());
        }

       //$('.js-datetime').datetimepicker({
       //    todayHighlight: true,
       //    autoclose: true,
       //    pickerPosition: 'top-right',
       //    format: 'dd-mm-yyyy hh:ii',
       //    ignoreReadonly: true
       //});

        $('.dmms-task__form').validate({
            rules: {
                name: {
                    required: true,
                },
                staff: {
                    required: true,
                },
            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên công việc.'
                },
                staff: {
                    required: 'Vui lòng nhập tên nhân viên.'
                },
            },

            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo('.dmms-task__form', -200);
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                $.ajax({
                    url: uuid ? `/task/update` : 'task/create',
                    method: uuid ? 'PUT' : 'POST',
                    data,
                })
                    .done(function () {
                        KTApp.block('.dmms-task__form');
                        const successMsg = uuid ? 'Sửa thông tin công việc thành công!' : 'Thêm công việc thành công!';
                        $.notify(successMsg, { type: 'success' });
                        if (isForm) {
                            drawer.close();
                            reloadTable();
                        }
                        else {
                            localStorage['edit_success'] = JSON.stringify({ from: (uuid ? `/task/update/` : '/task/create'), massage: successMsg });

                            window.location = window.location.origin + `/task`;
                        }
                        /**
                         * Close drawer
                         */
                        drawer.close();

                        reloadTable();
                    })
                    .fail(function () {
                        KTApp.unblock('.dmms-task__form');
                        const errorMsg = uuid ? 'Sửa thông tin công việc không thành công!' : 'Thêm công việc không thành công!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }

    if ($("#priority").val() !== '') {
        $('#kt-task-priority').selectpicker('val', $("#priority").val());
    }
    else {
        $('#kt-task-priority').selectpicker();
    }

    var initDatePicker = function () {
        $('#kt_datepicker').datepicker({
            rtl: KTUtil.isRTL(),
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            templates: arrows,
            format: 'dd/mm/yyyy'
        });
    }

    var initFormChange = function ($formChange) {
        initDropzone();
        $(document).on('change', '#kt-task-state-select', function () {
            //var $form = $('.kt-form-deadline');
            if ($(this).val() == 4) {
                $(`.kt-form-deadline input`).removeAttr('disabled');
                $('.kt-form-deadline input').attr('readonly', true);
                //$form.html('');
                //$form.append(`<input name="deadline_str" class="form-control" placeholder="Ngày hẹn" id="kt_datepicker" readonly />
                //              <span class="kt-input-icon__icon"><span><i class="la la-calendar"></i></span></span>`);
            }
            else {
                $(`.kt-form-deadline input`).removeAttr('readonly');
                $('.kt-form-deadline input').attr('disabled', true);
                $('input[name = "deadline_str"]').val('');
                //$form.html('');
                //$form.append(`<input name="" class="form-control" placeholder="Ngày hẹn" id="kt_datepicker" disabled />
                //              <span class="kt-input-icon__icon"><span><i class="la la-calendar"></i></span></span>`);

            }
            initDatePicker();
        });

        $formChange.validate({
            rules: {
                uuid: {
                    required: true,
                },
                state: {
                    required: true,
                },
            },
            messages: {
                uuid: {
                    required: 'Vui lòng nhập mã công việc.'
                },
                state: {
                    required: 'Vui lòng nhập trạng thái.'
                },
            },

            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($formChange , -200);
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                $.ajax({
                    url: `/task/changestate/${uuid}`,
                    method: 'POST',
                    data,
                })
                    .done(function (data) {
                        KTApp.block($formChange);
                        $.notify(data.msg, { type: 'success' });
                        /**
                         * Close drawer
                         */
                        drawer.close();

                        window.location.pathname = `/task/update/${uuid}`;
                    })
                    .fail(function (data) {
                        KTApp.unblock($formChange);
                        $.notify(data.responseJSON.msg, { type: 'danger' });
                    });
                return false;
            },
        });
    }

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-task__form');
        const $formChange = $content.find('.dmms-changestate__form');
        initForm($form);
        initFormChange($formChange);

        /*
            Init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

        //$('#kt-task-state-select').selectpicker();
        if ($("#state_id").val() !== '') {
            $('#kt-task-state-select').selectpicker('val', $("#state_id").val());
        }
        else {
            $('#kt-task-state-select').selectpicker();
        }
        if ($("#priority").val() !== '') {
            $('#kt-task-priority').selectpicker('val', $("#priority").val());
        }
        else {
            $('#kt-task-priority').selectpicker();
        }
        //$('#kt-staff-select').selectpicker();
        if ($("#staff_uuid").val() !== '') {
            $('#kt-staff-select').selectpicker('val', $("#staff_uuid").val());
        }
        else {
            $('#kt-staff-select').selectpicker();
        }

        if ($("#customer_uuid").val() !== '') {
            $('#kt-customer-select').selectpicker('val', $("#customer_uuid").val());
        }
        else {
            $('#kt-customer-select').selectpicker();
        }
        
    };

    var initDropzone = function () {
        var dropZone = $('#kt_dropzone_image');
        if (dropZone !== undefined && dropZone.length > 0) {
            new Dropzone('#kt_dropzone_image', {
                url: "/images/upload",
                paramName: "images",
                maxFiles: 5,
                maxFilesize: 5, // MB
                addRemoveLinks: true,
                dictDefaultMessage: 'Bấm hoặc kéo thả ảnh vào đây để tải ảnh lên',
                dictRemoveFile: 'Xóa ảnh',
                dictCancelUpload: 'Hủy',
                dictCancelUploadConfirmation: 'Bạn thực sự muốn hủy?',
                dictMaxFilesExceeded: 'Bạn không thể tải thêm ảnh!',
                dictFileTooBig: "Ảnh tải lên dung lượng quá lớn ({{filesize}}MB). Max filesize: {{maxFilesize}}MB.",
                autoProcessQueue: true,
                accept: function (file, done) {
                    done();
                },
                success: function (file, response) {
                    if (response != null && response.length > 0) {
                        var imageId = $("#image_uuids").val();
                        if (imageId == '') {
                            imageId = response[0].id;
                        }
                        else {
                            imageId += "," + response[0].id;
                        }
                        $("#image_uuids").val(imageId);
                    }
                    else {
                        $(file.previewElement).addClass("dz-error").find('.dz-error-message').text("Không tải được ảnh lên.");
                    }
                },
                error: function (file, response, errorMessage) {
                    if (file.accepted) {
                        $(file.previewElement).addClass("dz-error").find('.dz-error-message').text("Có lỗi xảy ra, không tải được ảnh lên.");
                    }
                    else {
                        $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(response);
                    }
                }
            });
        }
    }
    $(".linkdevice").on("click", function () {
        if (!$('.nav-item').find('.active').hasClass("linkdevice")) {
            debugger
            var taskUuid = document.getElementById("uuid").value;
            if (dataTableInstanceDevice != null) {
                dataTableInstanceDevice.load();
            }
            else {
                dataTableInstanceDevice = $('.kt-datatabledevice').KTDatatable({
                    data: {
                        type: 'remote',
                        source: {
                            read: {

                                url: `/device/gets/${taskUuid}`,
                            },

                            response: {

                                map: function (res) {

                                    const raw = res && res.data || {};
                                    const { pagination } = raw;
                                    const perpage = pagination === undefined ? undefined : pagination.perpage;
                                    const page = pagination === undefined ? undefined : pagination.page
                                    CURRENT_PAGE = page || 1;
                                    LIMIT = perpage;
                                    START_PAGE = 0;
                                    return {
                                        data: raw.items || [],
                                        meta: {
                                            ...pagination || {},
                                            perpage: perpage || LIMIT,
                                            page: page || 1,
                                        },
                                    }
                                },

                            },
                            filter: function (data = {}) {
                                const query = data.query || {};
                                const pagination = data.pagination || {};
                                const { category_id, supplier_uuid, state, keyword } = query;

                                if (START_PAGE === 1) {
                                    pagination.page = START_PAGE;
                                }
                                return {
                                    limit: pagination.perpage || LIMIT,
                                    category_id: $('#js-filter-category').val(),
                                    supplier_uuid: supplier_uuid || $('#js-filter-supplier').val(),
                                    //keyword: (keyword || $('#js-filter-keywordcontract').val()).trim(),
                                    state: state || $('#js-filter-states').val(),
                                    page: pagination.page,
                                };
                            }
                        },
                        pageSize: LIMIT,
                        serverPaging: true,
                        serverFiltering: true,
                    },

                    search: {
                        onEnter: true,

                    },
                    columns: [
                        {
                            field: 'uuid',
                            title: '#',
                            width: 30,
                            textAlign: 'center',
                            template: function (data, row) {
                                return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                            },
                            responsive: {
                                visible: 'md',
                                hidden: 'sm'
                            }
                        },
                        //{
                        //    field: 'code',
                        //    title: 'Mã',
                        //    width: 53,
                        //    textAlign:'center'
                        //},
                        //{
                        //    field: 'model',
                        //    title: 'Model',
                        //    width:110
                        //},
                        {
                            field: 'name_display',
                            title: 'Tên thiết bị',
                            template: function (data) {
                                var output = '';
                                if (data.name != null) {
                                    output += `<span class="text-primary">${data.prefixName}</span>
                                        <br /><span>${data.name_display}</span>`;
                                }
                                return output;
                            }
                            //autoHide: false,
                        },
                        {
                            field: 'serial',
                            title: 'Serial',
                            width: 100
                        },
                        {
                            field: 'packageName',
                            title: 'Sản phẩm',
                            width: 110
                        },
                        //{
                        //    field: 'supplierName',
                        //    title: 'Nhà cung cấp',
                        //    width: 120,
                        //},
                        //{
                        //    field: 'packageName',
                        //    title: 'Gói thiết bị',
                        //    width: 100,
                        //},
                        {
                            field: 'expiredDevice_nohh',
                            title: 'Ngày hết hạn TB',
                            width: 120,
                            template: function (data) {
                                if (data.expiredDevice_nohh == "01/01/0001") {
                                    return 'Chưa rõ'
                                }
                                return data.expiredDevice_nohh
                            }
                        },
                        {
                            field: 'state',
                            title: 'Trạng thái',
                            width: 150,
                            template: function (data) {
                                var states = {
                                    //1: { 'title': 'Đang sử dụng', 'class': 'kt-badge--brand' },
                                    //2: { 'title': 'Đang yêu cầu sửa chữa', 'class': ' kt-badge--warning' },
                                    //3: { 'title': 'Đến lịch bảo hành', 'class': ' kt-badge--primary' },
                                    //4: { 'title': 'Đang xử lý theo yêu cầu', 'class': 'kt-badge--success' },
                                    //5: { 'title': 'Cần thay thế', 'class': 'kt-badge--danger' },
                                    //6: { 'title': 'Mang đi sửa chữa - Không có thiết bị thay thế', 'class': 'kt-badge--info' },
                                    //7: { 'title': 'Mang đi sửa chữa - Có thiết bị thay thế', 'class': 'kt-badge--info' },
                                    //8: { 'title': 'Thay thế', 'class': 'kt-badge--info' },
                                    //9: { 'title': 'Hỏng', 'class': 'kt-badge--info' },
                                    //10: { 'title': 'Trong kho-Trong kho vật lý,ba lô', 'class': 'kt-badge--info' },
                                    //11: { 'title': 'Trong kho-Hỏng sau khi kiểm kê', 'class': 'kt-badge--info' },
                                    //12: { 'title': 'Đang vận chuyển', 'class': 'kt-badge--info' },
                                    //13: { 'title': 'Đã xuất thanh lý', 'class': 'kt-badge--info' },
                                    //14: { 'title': 'Đã xuất tiêu hao', 'class': 'kt-badge--info' },
                                    //15: { 'title': 'Đã hoàn trả nhà cung cấp', 'class': 'kt-badge--info' },
                                    //16: { 'title': 'Thêm mới/cập nhật thiết bị', 'class': 'kt-badge--info' },
                                    //17: { 'title': 'Đã sửa xong thiết bị', 'class': 'kt-badge--info' },
                                    1: { 'title': 'Hoạt động', 'class': 'kt-badge--success' },
                                    2: { 'title': 'Sửa chữa', 'class': ' kt-badge--warning' },
                                    3: {
                                        'title': 'Đến lịch bảo hành', 'class': ' kt-badge--primary', 'style': 'background-color:#074648c4'
                                    },
                                    4: { 'title': 'Sửa chữa', 'class': 'kt-badge--warning' },
                                    5: { 'title': 'Sửa chữa', 'class': 'kt-badge--warning' },
                                    6: { 'title': 'Sửa chữa', 'class': 'kt-badge--warning' },
                                    7: { 'title': 'Sửa chữa', 'class': 'kt-badge--warning' },
                                    8: { 'title': 'Thay thế tạm thời', 'class': 'kt-badge--info' },
                                    9: { 'title': 'Hỏng', 'class': 'kt-badge--danger' },
                                    10: { 'title': 'Hoạt động', 'class': 'kt-badge--danger' },
                                    11: { 'title': 'Hỏng', 'class': 'kt-badge--warning' },
                                    12: { 'title': 'Hoạt động', 'class': 'kt-badge--success' },
                                    13: { 'title': 'Đã xuất thanh lý', 'class': 'kt-badge--primary' },
                                    14: { 'title': 'Đã xuất tiêu hao', 'class': 'kt-badge--primary' },
                                    15: { 'title': 'Đã hoàn trả nhà cung cấp', 'class': 'kt-badge--primary' },
                                    16: { 'title': 'Thêm mới/cập nhật thiết bị', 'class': 'kt-badge--info' },
                                    17: { 'title': 'Hoạt động', 'class': 'kt-badge--success' },
                                };
                                if (data.state) {
                                    return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill"' + 'style="' + states[data.state].style + '">' + states[data.state].title + '</span>';
                                }
                                else {
                                    return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới</span>';
                                }
                            },
                            width: 150,
                            textAlign: 'center'
                        },

                        {
                            field: 'Tác vụ',
                            title: 'Tác vụ',
                            sortable: false,
                            width: 80,
                            overflow: 'visible',
                            autoHide: false,
                            textAlign: 'center',
                            template: function (data) {
                                return `
                            <a href="/device/edit/${data.uuid}" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </a>                           
                            <button data-id="${data.uuid}" type="button" title="Xóa" class="btn btn-sm btn-danger btn-icon btn-icon-md js-delete-device">
                                <i class="la la-trash"></i>
                            </button>
                        `;
                            },
                        
                    }],
                    layout: {
                        scroll: !0,
                        height: null,
                        footer: !1
                    },
                    sortable: false,
                    pagination: true,
                    responsive: true,

                });
            }


        }
    });
    return {
        // Public functions
        init: function () {
            initSearch();

            initTable();
            initEventListeners();
            
            initValidation();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-task').length) {
        DMMSTask.init();
    }
});
