﻿"use strict"

// Class definition
var DMMSProjectImportExcel = function () {
    var drawer = null;
    var listData = null;
    var initEventListeners = function () {
        $('body')
            .off('click', '#import-project')
            .on('click', '#import-project', function () {
                initImport();
            });
    };

    var initImport = function () {
        const title = `THÊM DỰ ÁN TỪ FILE EXCEL`;
        drawer = new KTDrawer({ title, className: 'dmms-bunker' });

        drawer.on('ready', () => {
            $.ajax({
                url: `/importexcel/importproject`,
                method: 'POST',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    const $form = $content.find('.dmms-import_form');
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    var initContent = function ($content) {
        const $form = $content.find('.dmms-import_form');
        autosize($content.find('textarea.auto-resize'));
        autoupload();
    };

    var autoupload = function () {
        $('input[type=file]').on('change', function (event) {
            var fileExtension = ['xls', 'xlsx'];
            var filename = $('#fileupload').val();
            if (filename.length == 0) {
                $.notify('Vui lòng chọn một file để import !', { type: 'danger' });
                return false;
            }
            else {
                var extension = filename.replace(/^.*\./, '');
                if ($.inArray(extension, fileExtension) == -1) {
                    $.notify('Vui lòng chọn file excel !', { type: 'danger' });
                    return false;
                }
            }
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'center',
                'message': 'Vui lòng chờ...'
            });
            loading.show();
            KTApp.blockPage();
            var files = event.target.files;
            event.stopPropagation(); // Stop stuff happening
            event.preventDefault(); // Totally stop stuff happening
            var fdata = new FormData();
            var fileUpload = $("#fileupload").get(0);
            var files = fileUpload.files;
            fdata.append(files[0].name, files[0]);
            $.ajax({
                type: "POST",
                url: "/ImportExcel/ImportExcelProject",
                data: fdata,
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response.length == 0)
                        $.notify('Không đọc được dữ liệu, sai cấu trúc file hoặc dữ liệu rỗng!', { type: 'danger' });
                    else {
                        $('#divPrint').html(response);
                        initEventSubmit();
                    }
                    loading.hide();
                    KTApp.unblockPage();
                    $('#checksubmit').val(1);
                    $('.submitForm').attr('disabled', false);

                },
                error: function (e) {
                    $('#divPrint').html(e.responseText);
                }
            });
            $.ajax({
                type: "POST",
                url: "/ImportExcel/ImportExcelPackage",
                data: fdata,
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response.length == 0)
                        $.notify('Không đọc được dữ liệu, sai cấu trúc file hoặc dữ liệu rỗng!', { type: 'danger' });
                    else {
                        $('#divPrint2').html(response);
                        initEventSubmit();
                    }
                    loading.hide();
                    KTApp.unblockPage();
                    $('#checksubmit').val(1);
                    $('.submitForm').attr('disabled', false);

                },
                error: function (e) {
                    $('#divPrint').html(e.responseText);
                }
            });
        }
        )
    };

    var initEventSubmit = function () {
        $('body')
            .off('click', '.submitForm')
            .on('click', '.submitForm', function () {
                var kiemTraClick = $('#checksubmit').val();
                if (kiemTraClick == 0) return;
                if (kiemTraClick == 1) {
                    $('#checksubmit').val(0);
                    $('.submitForm').attr('disabled', true);
                    var loading = new KTDialog({
                        'type': 'loader',
                        'placement': 'center',
                        'message': 'Vui lòng chờ...'
                    });
                    loading.show();
                    KTApp.blockPage();
                    $.ajax({
                        url: `/ImportExcel/UpsetExcelProject`,
                        type: 'POST',
                    })
                        .done(function (data) {
                            loading.hide();
                            KTApp.unblockPage();
                            $.notify('Thêm dự án thành công!', { type: 'success' });                            
                            //drawer.close();
                            //location.reload();
                            $.ajax({
                                url: `/ImportExcel/UpsetExcelPackage`,
                                type: 'POST',
                            }).done(function (data) {
                                loading.hide();
                                KTApp.unblockPage();
                                $.notify('Thêm chi tiết sản phẩm thành công!', { type: 'success' });
                                drawer.close();
                                location.reload();
                            })
                                .fail(function (data) {
                                    loading.hide();
                                    KTApp.unblockPage();
                                    $.notify(data.responseJSON.msg, { type: 'danger' });
                                    $('#checksubmit').val(1);
                                    $('.submitForm').attr('disabled', false);
                                });
                        })
                        .fail(function (data) {
                            loading.hide();
                            KTApp.unblockPage();
                            $.notify(data.responseJSON.msg, { type: 'danger' });
                            $('#checksubmit').val(1);
                            $('.submitForm').attr('disabled', false);
                            $.ajax({
                                url: `/ImportExcel/UpsetExcelPackage`,
                                type: 'POST',
                            }).done(function (data) {
                                loading.hide();
                                KTApp.unblockPage();
                                $.notify('Thêm chi tiết sản phẩm thành công!', { type: 'success' });
                                //drawer.close();
                                //location.reload();
                            })
                                .fail(function (data) {
                                    loading.hide();
                                    KTApp.unblockPage();
                                    $.notify(data.responseJSON.msg, { type: 'danger' });
                                    $('#checksubmit').val(1);
                                    $('.submitForm').attr('disabled', false);
                                });
                        });
                }
            });
    };

    return {
        // Public functions
        init: function () {
            initEventListeners();
            autoupload();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-project').length) {

        DMMSProjectImportExcel.init();
    }
});