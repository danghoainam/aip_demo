﻿"use strict";
// Class definition

var DMMSStaff = function () {
    // Private functions
    var image = null;
    var dataTableInstance = null;
    var dataTableInstanceBalo = null;
    var dataTableDetailIm = null;
    var dataTableInstanceDevice = null;
    var dataTableInstanceEx = null;
    var dataTableInstanceIm = null;
    var drawer = null;
    var drawerim = null;
    var drawerDetail = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    // demo initializer
    var initTable = function () {
        var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
        if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
            $.notify(editSucessCache.massage, { type: 'success' });
            localStorage.removeItem('edit_success');
        }
        dataTableInstance = $('.kt-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/staff/gets',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            if (raw.items != null) {
                                $.each(raw.items, function (index, item) {
                                    for (var property in item) {
                                        if (item[property] && typeof item[property] === "object") {
                                            $.each(item[property], function (i, v) {
                                                for (var pro in item[property]) {
                                                    if (item[property][pro] && typeof item[property][pro] === "string") {
                                                        item[property][pro] = item[property][pro].replace(/</g, "&lt;").replace(/>/g, "&gt;");
                                                    }
                                                }
                                            })
                                        }
                                        if (item[property] && typeof item[property] === "string") {
                                            item[property] = item[property].replace(/</g, "&lt;").replace(/>/g, "&gt;");
                                        }
                                    }
                                })
                            }
                            debugger
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page;
                            CURRENT_PAGE = page || 1;
                            START_PAGE = 0;
                            LIMIT = perpage;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        debugger
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword, team_uuid, province_id, village_id, town_id, type, phone_number } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }

                        return {
                            limit: pagination.perpage || LIMIT,
                            province_id: $('#js-filter-location').val(),
                            village_id: $('#locationvillage').val(),
                            town_id: $('#locationtown').val(),
                            type: type || $('#kt_select_type_staff').val(),
                            team_uuid: team_uuid || $('#js-filter-team').val(),
                            keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            phone_number: phone_number || $('#js-filter-phone').val(),
                            page: pagination.page,

                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,

            },

            search: {
                onEnter: true,

            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 20,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                //{
                //    field: 'account.username',
                //    title: 'Tài khoản',
                //    width: 100,
                //    autoHide: false,

                //},
                {
                    field: 'name',
                    title: 'Tên nhân viên',
                    autoHide: false,
                    template: function (data) {
                        var stateNo = KTUtil.getRandomInt(0, 6);
                        var states = [
                            'success',
                            'brand',
                            'danger',
                            'success',
                            'warning',
                            'primary',
                            'info'];
                        var state = states[stateNo];
                        var output = '';
                        output = '<div class="kt-user-card-v2">\
								<div class="kt-user-card-v2__pic row" style="width:200%;">\
									<div class="kt-badge kt-badge--xl kt-badge--' + state + '">' + data.name.substring(0, 1) + '</div>\
                        <div class="kt-user-card-v2__details">\
									<span class="kt-user-card-v2__name">' + data.name + '</span><br/>\
                            <span class="kt-user-card-v2__name">' + data.email + '</span><br/>\
									<a href="#" class="kt-user-card-v2__email kt-link">' +
                            data.phone_number + '</a>\
								</div>\
								</div>\
							</div>';
                        return output;
                    }

                },
                //{
                //    field: 'email',
                //    title: 'Liên hệ',
                //    autoHide: false,
                //    template: function (data) {
                //        var output = '<strong>' + data.email + '</strong>';
                //        if (data.phone_number) {
                //            output += '<br/>' + data.phone_number + '</span>';
                //        }
                //        return output;
                //    }


                //},
                //{
                //    field: 'email',
                //    title: 'Email',
                //    width: 160
                //},
                {
                    field: 'address',
                    title: 'Đ/C chi tiết',
                    width: 120,
                    width_percent: 10
                },
                {
                    field: 'provinces.name',
                    title: 'Tỉnh/Thành',
                    autoHide: false,
                    width: 100,
                    width_percent: 10
                },
                {
                    field: 'towns.name',
                    title: 'Quận/Huyện/Thị Xã',
                    autoHide: false,
                    width: 110,
                    width_percent: 10
                },
                {
                    field: 'villages.name',
                    title: 'Phường/Xã/Thị Trấn',
                    autoHide: false,
                    width: 100,
                    width_percent: 10
                },
                {
                    field: 'type',
                    title: 'Chức Vụ',
                    //autoHide: false,
                    width: 90,
                    width_percent: 10,
                    template: function (data) {
                        var output = "";
                        if (data.type == 1) {
                            output = "Kỹ thuật"
                        }
                        else if (data.type == 2) {
                            output = "CSKH"
                        }
                        else if (data.type == 3) {
                            output = "Quản lý"
                        }
                        else if (data.type == 4) {
                            output = "Kho"
                        }
                        return output;
                    },
                },
                {
                    field: 'team.name',
                    title: 'Đội',
                    autoHide: false,
                    width: 100,
                    width_percent: 10,
                    template: function (data) {
                        return data.team != null ? `<span>${data.team.name}</span>` : '<span class="text-danger">Chưa có</span>'
                    }
                },
                {
                    field: 'department.name',
                    title: 'Phòng ban',
                    autoHide: false,
                    width: 80,
                    width_percent: 10,
                    template: function (data) {
                        return data.department != null ? `<span>${data.department.name}</span>` : '<span class="text-danger">Chưa có</span>'
                    }
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 110,
                    width_percent: 10,
                    overflow: 'visible',
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        var button = ''
                        if (data.account != null) {
                            if (data.account.status == 0) {
                                var title = "Mở tài khoản";
                                var class1 = "js-unlock-staff btn btn-sm btn-success btn-icon btn-icon-md";
                                var icon = "la-unlock-alt";

                            }
                            else {
                                var title = "Khóa tài khoản";
                                var class1 = "js-detail-staff btn btn-sm btn-warning btn-icon btn-icon-md";
                                var icon = "la-unlock";
                            }
                            button = `<button data-id="${data.account.uuid}" title=${title} class="${class1}">
        <i class="la ${icon}"></i>
        </button>`;
                        }
                        else {
                            button = `<button title=${title} class="js-detail-staff btn btn-sm btn-warning btn-icon btn-icon-md" disabled>
        <i class="la la-unlock"></i>
        </button>`
                        }
                        return `
         <a href="/staff/update/${data.uuid}" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </a>
        ${button}
        <button data-id="${data.uuid}" type="button" title="Xóa" class="btn btn-sm btn-danger btn-icon btn-icon-md js-delete-staff">
        <i class="la la-trash"></i>
        </button>
`;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });

    };
    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var initSearch = function () {
        $('#js-filter-team')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'team_uuid');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#kt_select_type_staff')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'type');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });
        $('#js-filter-phone')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase().trim(), 'phone_number');
                    START_PAGE = 1;
                }
            });

        $('#kt_form_team_add').selectpicker();
        $('#kt_form_team_update').selectpicker();
        $('#js-filter-location')
            .on('change', function () {
                if ($(this).val() == '') {
                    $('#js-filter-location').val('');
                    $('#js-filter-location').selectpicker('refresh');
                    $('#locationtown').val('');
                    $('#locationtown').attr('disabled', true);
                    $('#locationtown').selectpicker('refresh');
                    $('#locationvillage').val('');
                    $('#locationvillage').attr('disabled', true);
                    $('#locationvillage').selectpicker('refresh')
                    if (dataTableInstance) {
                        dataTableInstance.search($('#js-filter-location').val(), 'provinceId'),
                            START_PAGE = 1
                    };
                }
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'location_id');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#locationtown')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'town_id');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#locationvillage')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'village_id');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-states')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'state');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
    };

    $(".linkdetail").on("click", function () {
        debugger
        var staffUuid = document.getElementById("uuid").value;
        if (dataTableInstance) {
            dataTableInstance.load()
        }
        else {
            dataTableInstance = $('.kt-datatable1').KTDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {

                            url: `/staff/gettask/${staffUuid}`,
                        },

                        response: {

                            map: function (res) {
                                const raw = res && res.data || {};
                                const { pagination } = raw;
                                const perpage = pagination === undefined ? undefined : pagination.perpage;
                                const page = pagination === undefined ? undefined : pagination.page
                                CURRENT_PAGE = page || 1;
                                LIMIT = perpage;
                                START_PAGE = 0;
                                return {
                                    data: raw.items || [],
                                    meta: {
                                        ...pagination || {},
                                        perpage: perpage || LIMIT,
                                        page: page || 1,
                                    },
                                }
                            },
                        },
                        filter: function (data = {}) {
                            const query = data.query || {};
                            const pagination = data.pagination || {};
                            const { state } = query;

                            if (START_PAGE === 1) {
                                pagination.page = START_PAGE;
                            }
                            return {
                                limit: pagination.perpage || LIMIT,

                                state: state || $('#js-filter-state').val(),
                                page: pagination.page,
                            };
                        }
                    },
                    pageSize: LIMIT,
                    serverPaging: true,
                    serverFiltering: true,
                },

                search: {
                    onEnter: true,

                },
                columns: [
                    {
                        field: 'uuid',
                        title: '#',
                        width: 30,
                        textAlign: 'center',
                        template: function (data, row) {
                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                        },
                        responsive: {
                            visible: 'md',
                            hidden: 'sm'
                        }
                    },
                    //{
                    //    field: 'code',
                    //    title: 'Mã công việc',
                    //    width: 80
                    //},
                    {
                        field: 'name',
                        title: 'Công việc',

                    },
                    //{
                    //    field: 'ticket.title',
                    //    title: 'Tiêu đề yêu cầu',
                    //}, 
                    //{
                    //    field: 'customer.name',
                    //    title: 'Đơn vị',
                    //    width: 220,
                    //},
                    {
                        field: 'type.name',
                        title: 'Loại công việc',
                        textAlign: 'center',
                        width: 150,
                    },
                    {
                        field: 'time_created',
                        title: 'Ngày tạo',
                        textAlign: 'center',
                        width: 100,
                    },
                    {
                        field: 'priority.name',
                        title: 'Mức độ ưu tiên',
                        textAlign: 'center',
                        width: 100,
                        template: function (row) {
                            var priorities = {
                                1: { 'title': 'Khẩn cấp', 'class': ' kt-badge--danger' },
                                2: { 'title': 'Ưu tiên', 'class': ' kt-badge--warning' },
                                3: { 'title': 'Bình thường', 'class': 'kt-badge--primary' },
                            };
                            if (row.priority) {
                                return '<p class="kt-badge ' + priorities[row.priority.id].class + ' kt-badge--inline kt-badge--pill">' + row.priority.name + '</p>';
                            }
                        }
                    },
                    {
                        field: 'state',
                        title: 'Trạng thái',
                        textAlign: 'center',
                        width: 140,
                        template: function (data) {
                            var states = {
                                1: { 'title': 'Chưa tiếp nhận', 'class': 'kt-badge--danger' },
                                2: { 'title': 'Yêu cầu hỗ trợ', 'class': ' kt-badge--danger' },
                                3: { 'title': 'Đã tiếp nhận', 'class': 'kt-badge--primary' },
                                4: { 'title': 'Đã lên kế hoạch', 'class': ' kt-badge--info' },
                                5: { 'title': 'Đang xử lý', 'class': ' kt-badge--warning' },
                                6: { 'title': 'Đợi đề xuất', 'class': ' kt-badge--warning' },
                                7: { 'title': 'Yêu cầu tiếp tục', 'class': ' kt-badge--success' },
                                8: { 'title': 'Hoàn thành', 'class': ' kt-badge--success' },
                                9: { 'title': 'Cập nhật số lượng TB', 'class': ' kt-badge--success' },
                                10: { 'title': 'Hủy', 'class': ' kt-badge--danger' },
                            };
                            if (data.state) {
                                return '<span class="kt-badge ' + states[data.state.id].class + ' kt-badge--inline kt-badge--pill">' + data.state.name + '</span>';
                            }
                            else {
                                return '<span class="kt-badge ' + states[1].class + ' kt-badge--inline kt-badge--pill">' + states[1].title + '</span>';
                            }
                        }
                    }],
                layout: {
                    scroll: !0,
                    height: null,
                    footer: !1
                },
                sortable: false,
                pagination: true,
                responsive: true,
            });
        }

    });
    $(".staffdevice").on("click", function () {
        debugger
        var staffUuid = document.getElementById("uuid").value;
        if (dataTableInstanceDevice) {
            dataTableInstanceDevice.load();
        }
        else {
            dataTableInstanceDevice = $('.kt-datatablestaffdevice').KTDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {

                            url: `/staff/getdevice/${staffUuid}`,
                        },

                        response: {

                            map: function (res) {
                                debugger
                                const raw = res && res.data || {};
                                const { pagination } = raw;
                                const perpage = pagination === undefined ? undefined : pagination.perpage;
                                const page = pagination === undefined ? undefined : pagination.page
                                CURRENT_PAGE = page || 1;
                                LIMIT = perpage;
                                START_PAGE = 0;
                                return {
                                    data: raw.items || [],
                                    meta: {
                                        ...pagination || {},
                                        perpage: perpage || LIMIT,
                                        page: page || 1,
                                    },
                                }
                            },
                        },
                        filter: function (data = {}) {
                            const query = data.query || {};
                            const pagination = data.pagination || {};


                            if (START_PAGE === 1) {
                                pagination.page = START_PAGE;
                            }
                            return {
                                limit: pagination.perpage || LIMIT,

                                //keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                                page: pagination.page,
                            };
                        }
                    },
                    pageSize: LIMIT,
                    serverPaging: true,
                    serverFiltering: true,
                },

                search: {
                    onEnter: true,

                },
                columns: [
                    {
                        field: 'uuid',
                        title: '#',
                        width: 30,
                        textAlign: 'center',
                        template: function (data, row) {
                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                        },
                        responsive: {
                            visible: 'md',
                            hidden: 'sm'
                        }
                    },
                    //{
                    //    field: 'code',
                    //    title: 'Mã công việc',
                    //    width: 80
                    //},
                    {
                        field: 'name',
                        title: 'Tên thiết bị',
                        template: function (data) {
                            var output = `<span class="text-primary">${data.prefixName}</span>`;
                            data.code != "" ? output += `<br><span>${data.code} - ${data.name}</span>` : `<br><span>${data.name}</span>`;

                            return output;
                        },
                    },
                    //{
                    //    field: 'ticket.title',
                    //    title: 'Tiêu đề yêu cầu',
                    //}, 
                    {
                        field: 'serial',
                        title: 'Serial',
                        width: 220,
                    },


                    {
                        field: 'supplierName',
                        title: 'Nhà cung cấp',
                        textAlign: 'center',
                        width: 110,
                    },
                    {
                        field: 'packageName',
                        title: 'Gói thiết bị',
                        textAlign: 'center',
                        width: 110,
                    },
                    {
                        field: 'expiredContract_nohh',
                        title: 'Ngày hết hạn HĐ',
                        textAlign: 'center',
                        width: 110,
                        template: function (data) {
                            if (data.expiredContract == "0001-01-01T00:00:00" || data.expiredContract == null) return '<span class="text-danger">Chưa có</span>';
                            return `<span>${data.expiredContract_nohh}</span>`
                        },
                    },
                    {
                        field: 'expiredDevice_nohh',
                        title: 'Ngày hết hạn TB',
                        textAlign: 'center',
                        width: 110,
                        template: function (data) {
                            if (data.expireDevice == "0001-01-01T00:00:00" || data.expireDevice == null) return '<span class="text-danger">Chưa có</span>';
                            return `<span>${data.expiredDevice_nohh}</span>`
                        },
                    },
                    {
                        field: 'state',
                        title: 'Trạng thái',
                        textAlign: 'center',
                        width: 140,
                        template: function (data) {
                            var states = {
                                1: { 'title': 'Đang sử dụng', 'class': 'kt-badge--brand' },
                                2: { 'title': 'Đang yêu cầu sửa chữa', 'class': ' kt-badge--warning' },
                                3: { 'title': 'Đến lịch bảo hành', 'class': ' kt-badge--primary' },
                                4: { 'title': 'Đang xử lý theo yêu cầu', 'class': 'kt-badge--success' },
                                5: { 'title': 'Cần thay thế', 'class': 'kt-badge--danger' },
                                6: { 'title': 'Mang đi sửa chữa - Không có thiết bị thay thế', 'class': 'kt-badge--warning' },
                                7: { 'title': 'Mang đi sửa chữa - Có thiết bị thay thế', 'class': 'kt-badge--dark' },
                                8: { 'title': 'Thay thế', 'class': 'kt-badge--warning' },
                                9: { 'title': 'Hỏng', 'class': 'kt-badge--danger' },
                                10: { 'title': 'Trong kho-Trong kho vật lý,ba lô', 'class': 'kt-badge--info' },
                                11: { 'title': 'Trong kho-Hỏng sau khi kiểm kê', 'class': 'kt-badge--warning' },
                                12: { 'title': 'Đang vận chuyển', 'class': 'kt-badge--info' },
                                13: { 'title': 'Đã xuất thanh lý', 'class': 'kt-badge--success' },
                                14: { 'title': 'Đã xuất tiêu hao', 'class': 'kt-badge--success' },
                                15: { 'title': 'Đã hoàn trả nhà cung cấp', 'class': 'kt-badge--success' },
                                16: { 'title': 'Thêm mới/cập nhật thiết bị', 'class': 'kt-badge--info' },
                                17: { 'title': 'Đã sửa xong thiết bị', 'class': 'kt-badge--success' },
                            };
                            if (data.state) {
                                return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill">' + states[data.state].title + '</span>';
                            }
                            else {
                                return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới</span>';
                            }
                        }
                    },
                    {
                        field: 'Tác vụ',
                        title: 'Tác vụ',
                        sortable: false,
                        width: 80,
                        overflow: 'visible',
                        autoHide: false,
                        textAlign: 'center',
                        template: function (data) {
                            return `
                            <a href="/device/edit/${data.uuid}" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </a>                           
                            <button data-id="${data.uuid}" type="button" title="Xóa" class="btn btn-sm btn-danger btn-icon btn-icon-md js-delete-device">
                                <i class="la la-trash"></i>
                            </button>
                        `;
                        },


                    }],
                layout: {
                    scroll: !0,
                    height: null,
                    footer: !1
                },
                sortable: false,
                pagination: true,
                responsive: true,
            });
        }

    });
    $(".staffbalo").on("click", function () {
        debugger
        var staffUuid = document.getElementById("uuid").value;
        if (dataTableInstanceBalo) {
            dataTableInstanceBalo.load()
        }
        else {
            dataTableInstanceBalo = $('.kt-datatablestaffbalo').KTDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {

                            url: `/staff/getbalo/${staffUuid}`,
                        },

                        response: {

                            map: function (res) {
                                debugger
                                const raw = res && res.data || {};
                                const { pagination } = raw;
                                const perpage = pagination === undefined ? undefined : pagination.perpage;
                                const page = pagination === undefined ? undefined : pagination.page
                                CURRENT_PAGE = page || 1;
                                LIMIT = perpage;
                                START_PAGE = 0;
                                return {
                                    data: raw.items || [],
                                    meta: {
                                        ...pagination || {},
                                        perpage: perpage || LIMIT,
                                        page: page || 1,
                                    },
                                }
                            },
                        },
                        filter: function (data = {}) {
                            const query = data.query || {};
                            const pagination = data.pagination || {};


                            if (START_PAGE === 1) {
                                pagination.page = START_PAGE;
                            }
                            return {
                                limit: pagination.perpage || LIMIT,

                                //keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                                page: pagination.page,
                            };
                        }
                    },
                    pageSize: LIMIT,
                    serverPaging: true,
                    serverFiltering: true,
                },

                search: {
                    onEnter: true,

                },
                columns: [
                    {
                        field: '#',
                        title: '#',
                        width: 10,
                        textAlign: 'center',
                        template: function (data, row) {
                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                        },
                        responsive: {
                            visible: 'md',
                            hidden: 'sm'
                        }
                    },
                    //{
                    //    field: 'code',
                    //    title: 'Mã công việc',
                    //    width: 80
                    //},
                    {
                        field: 'name',
                        title: 'Tên vật tư - Dụng cụ lao động',
                        template: function (data) {
                            var output = '<strong>' + data.category.name + '</strong>';
                            if (data.category) {
                                output += '<br/>' + data.category.prefixName + '</span>';
                            }
                            return output;
                        }

                    },
                    //{
                    //    field: 'ticket.title',
                    //    title: 'Tiêu đề yêu cầu',
                    //}, 
                    {
                        field: 'type.name',
                        title: 'Phân loại',
                        width: 80,
                    },


                    {
                        field: 'quantity',
                        title: 'Số lượng',
                        width: 70,
                    },
                    {
                        field: 'unit',
                        title: 'Đơn vị tính',
                        width: 100,
                    },
                ],
                layout: {
                    scroll: !0,
                    height: null,
                    footer: !1
                },
                sortable: false,
                pagination: true,
                responsive: true,
            });
        }

    });
    $(".staffhistory").on("click", function () {
        debugger
        var staffUuid = document.getElementById("uuid").value;
        if (dataTableInstanceIm) {
            dataTableInstanceIm.load()
        }
        else {
            dataTableInstanceIm = $('.kt-datatablestaffhistory').KTDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {

                            url: `/staff/GetImport/${staffUuid}`,
                        },

                        response: {

                            map: function (res) {
                                debugger
                                const raw = res && res.data || {};
                                const { pagination } = raw;
                                const perpage = pagination === undefined ? undefined : pagination.perpage;
                                const page = pagination === undefined ? undefined : pagination.page
                                CURRENT_PAGE = page || 1;
                                LIMIT = perpage;
                                START_PAGE = 0;
                                return {
                                    data: raw.items || [],
                                    meta: {
                                        ...pagination || {},
                                        perpage: perpage || LIMIT,
                                        page: page || 1,
                                    },
                                }
                            },
                        },
                        filter: function (data = {}) {
                            const query = data.query || {};
                            const pagination = data.pagination || {};


                            if (START_PAGE === 1) {
                                pagination.page = START_PAGE;
                            }
                            return {
                                limit: pagination.perpage || LIMIT,

                                //keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                                page: pagination.page,
                            };
                        }
                    },
                    pageSize: LIMIT,
                    serverPaging: true,
                    serverFiltering: true,
                },

                search: {
                    onEnter: true,

                },
                columns: [
                    {
                        field: 'uuid',
                        title: '#',
                        width: 20,
                        textAlign: 'center',
                        template: function (data, row) {
                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                        },
                        responsive: {
                            visible: 'md',
                            hidden: 'sm'
                        }
                    },
                    //{
                    //    field: 'code',
                    //    title: 'Mã công việc',
                    //    width: 80
                    //},
                    {
                        field: 'code',
                        title: 'Số chứng từ',
                        width: 100

                    },
                    {
                        field: 'type.type_Name',
                        title: 'Loại phiếu nhập',
                        width: 200,
                    },
                    {
                        field: 'namefrom',
                        title: 'Nhập từ-kho-NVKT-khác',
                    },
                    {
                        field: 'staffTo.name',
                        title: 'Nhân viên nhập',
                        width: 100,
                    },
                    {
                        field: 'time_created',
                        title: 'Thời gian',
                        width: 100,
                        textAlign: 'center'
                    },
                    {
                        field: 'Tác vụ',
                        title: 'Tác vụ',
                        sortable: false,
                        width: 50,
                        autoHide: false,
                        textAlign: 'center',
                        template: function (data) {
                            return `<button data-id="${data.uuid}" data-name="${data.namefrom}"  type="button" title="Chi tiết" class="btn btn-sm btn-primary btn-icon btn-icon-md js-detail-ticketimport">
                                <i class="la la-info"></i>
                            </button>`;
                        },
                    }
                ],
                layout: {
                    scroll: !0,
                    height: null,
                    footer: !1
                },
                sortable: false,
                pagination: true,
                responsive: true,
            });
        }

    });
    $(".staffhistoryex").on("click", function () {
        debugger
        var staffUuid = document.getElementById("uuid").value;
        if (dataTableInstanceEx) { dataTableInstanceEx.load }
        else {
            dataTableInstanceEx = $('.kt-datatablestaffhistoryex').KTDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {

                            url: `/staff/GetExport/${staffUuid}`,
                        },

                        response: {

                            map: function (res) {
                                debugger
                                const raw = res && res.data || {};
                                const { pagination } = raw;
                                const perpage = pagination === undefined ? undefined : pagination.perpage;
                                const page = pagination === undefined ? undefined : pagination.page
                                CURRENT_PAGE = page || 1;
                                LIMIT = perpage;
                                START_PAGE = 0;
                                return {
                                    data: raw.items || [],
                                    meta: {
                                        ...pagination || {},
                                        perpage: perpage || LIMIT,
                                        page: page || 1,
                                    },
                                }
                            },
                        },
                        filter: function (data = {}) {
                            const query = data.query || {};
                            const pagination = data.pagination || {};


                            if (START_PAGE === 1) {
                                pagination.page = START_PAGE;
                            }
                            return {
                                limit: pagination.perpage || LIMIT,

                                //keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                                page: pagination.page,
                            };
                        }
                    },
                    pageSize: LIMIT,
                    serverPaging: true,
                    serverFiltering: true,
                },

                search: {
                    onEnter: true,

                },
                columns: [
                    {
                        field: 'uuid',
                        title: '#',
                        width: 20,
                        textAlign: 'center',
                        template: function (data, row) {
                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                        },
                        responsive: {
                            visible: 'md',
                            hidden: 'sm'
                        }
                    },
                    //{
                    //    field: 'code',
                    //    title: 'Mã công việc',
                    //    width: 80
                    //},
                    {
                        field: 'code',
                        title: 'Số chứng từ',
                        width: 100

                    },
                    {
                        field: 'type.type_Name',
                        title: 'Loại phiếu nhập',
                        width: 200,
                    },
                    {
                        field: 'namefrom',
                        title: 'Nhập từ-kho-NVKT-khác',
                    },
                    {
                        field: 'staffTo.name',
                        title: 'Nhân viên nhập',
                        width: 100,
                    },
                    {
                        field: 'time_created',
                        title: 'Thời gian',
                        width: 100,
                        textAlign: 'center'
                    },
                    {
                        field: 'Tác vụ',
                        title: 'Tác vụ',
                        sortable: false,
                        width: 50,
                        autoHide: false,
                        textAlign: 'center',
                        template: function (data) {
                            return `<button data-name="${data.namefrom}"  data-id="${data.uuid}" type="button" title="Chi tiết" class="btn btn-sm btn-primary btn-icon btn-icon-md js-detail-ticketexport">
                                <i class="la la-info"></i>
                            </button>`;
                        },
                    }
                ],
                layout: {
                    scroll: !0,
                    height: null,
                    footer: !1
                },
                sortable: false,
                pagination: true,
                responsive: true,
            });
        }

    });
    var saveResult = function (id) {
        drawerInstance.block();
        const $drawer = drawerInstance.getElement();
        const selected = $drawer.find('select').val();
        const successMsg = 'Lưu đội thành công!';
        const errorMsg = 'Lưu đội không thành công!';
        $.ajax({
            // url: `/group/${module}/${id}`,
            url: `/team/upsert/${id}`,
            method: 'POST',
            data: {
                request: {
                    uuid: id,
                    uuids: selected.join(','),
                    name: team_name,
                    description: team_desc,
                }
            }
        }).done((res) => {
            drawerInstance.unblock();
            drawerInstance.close();
            $.notify(successMsg, { type: 'success' });
        }).fail((res) => {
            drawerInstance.unblock();
            $.notify(errorMsg, { type: 'danger' });
        });
    };
    var deleteStaffReal = function (id) {

        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa tài khoản...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/staff/delete/${id}`,
                method: 'DELETE',
            })
                .done(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(data.msg, { type: 'success' });

                    if (dataTableInstance) {
                        dataTableInstance.reload();
                    }
                })
                .fail(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(data.responseJSON.msg, { type: 'danger' });
                });
        }
    };

    var lockStaff = function (id) {

        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang khóa tài khoản...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/staff/LockAccount/${id}`,
                method: 'GET',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Khóa tài khoản thành công!', { type: 'success' });

                    if (dataTableInstance) {
                        dataTableInstance.reload();
                    }
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Khóa tài khoản không thành công!', { type: 'danger' });
                });
        }
    };
    var unlockStaff = function (id) {

        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang mở khóa tài khoản...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/staff/UnlockAccount/${id}`,
                method: 'GET',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Mở khóa tài khoản thành công!', { type: 'success' });

                    if (dataTableInstance) {
                        dataTableInstance.reload();
                    }
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Mở khóa tài khoản không thành công!', { type: 'danger' });
                });
        }
    };
    var initEditDrawer = function (id) {

        const title = id ? 'Cập nhật thông tin nhân viên' : 'Thêm nhân viên';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--staff' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/staff/update/${id}` : '/staff/create',
                method: 'GET',
            })
                .done(function (response) {

                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var resetPassword = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang tải dữ liệu...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/account/resetpassword/${id}`,
                method: 'POST',
            })
                .done(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(`Đặt lại mật khẩu thành công!`, { type: 'success' });

                    if (dataTableInstance) {
                        dataTableInstance.reload();
                    }
                })
                .fail(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(`Đặt lại mật khẩu không thành công!`, { type: 'danger' });
                });
        }
    };

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-staff')
            .on('click', '.js-delete-staff', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Nhân viên sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteStaffReal(id);
                    }
                });
            })
            .off('click', '.js-detail-staff')
            .on('click', '.js-detail-staff', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Bạn có muốn khóa tài khoản.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        lockStaff(id);
                    }
                });
            })
            .off('click', '.js-unlock-staff')
            .on('click', '.js-unlock-staff', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Bạn có muốn mở khóa tài khoản.',
                    icon: 'info',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        unlockStaff(id);
                    }
                });
            })
            .off('click', '.js-reset-account')
            .on('click', '.js-reset-account', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                KTApp.swal({
                    title: 'Bạn có muốn đặt lại mật khẩu?',
                    text: 'Hệ thống sẽ gửi bạn mật khẩu sau ít phút.',
                    icon: 'info',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        resetPassword(id);
                    }
                });
            })
            .off('click', '.js-edit-staff')
            .on('click', '.js-edit-staff', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initEditDrawer(id || '');
            })
            //+
            .off('click', '.js-list-team-staff')
            .on('click', '.js-list-team-staff', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initDrawer({
                    id
                });
            })
            .off('click', '.js-save-team-staff')
            .on('click', '.js-save-team-staff', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                saveResult({
                    id,
                });
            })
            .off('click', '.js-detail-ticketimport')
            .on('click', '.js-detail-ticketimport', function () {
                const id = $(this).attr('data-id');
                const name = $(this).attr('data-name');

                detailImportTicket(id, name);
            })
            .off('click', '.js-detail-ticketexport')
            .on('click', '.js-detail-ticketexport', function () {
                const id = $(this).attr('data-id');
                const name = $(this).attr('data-name');
                detailExportTicket(id, name);
            })
            .off('click', '.js-detail-import-device')
            .on('click', '.js-detail-import-device', function () {
                const categoryId = $(this).attr('data-id');
                const importUuid = $(this).attr('data-importUuid');
                detailImportDevice(categoryId, importUuid);
            })
            .off('click', '.js-detail-export-device')
            .on('click', '.js-detail-export-device', function () {
                const categoryId = $(this).attr('data-id');
                const exportUuid = $(this).attr('data-exportUuid');
                detailExportDevice(categoryId, exportUuid);
            });
    };

    var detailExportDevice = function (categoryId, exportUuid) {

        var title = `<span class="text-uppercase">Danh sách chi tiết thiết bị</span>`;



        drawerDetail = new KTDrawer({ title, id: null || 'new', className: 'dmms-drawer--detailimportticket' });

        drawerDetail.on('ready', () => {
            $.ajax({
                url: `/bunker/DetailSupplies`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawerDetail.setBody(response);
                    dataTableInstanceImDevice = $('.kt-datatable--detailsuppliesdevice').KTDatatable({
                        data: {
                            type: 'remote',
                            source: {
                                read: {
                                    url: `/bunker/DetailDeviceEx`,
                                },
                                response: {
                                    map: function (res) {
                                        debugger
                                        const raw = res && res.data || {};
                                        const { pagination } = raw;
                                        const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                                        const page = pagination === undefined ? 0 : pagination.page;
                                        CURRENT_PAGE = page || 1;
                                        LIMIT = perpage;
                                        START_PAGE = 0;
                                        return {
                                            data: raw.items || [],
                                            meta: {
                                                ...pagination || {},
                                                perpage: perpage || LIMIT,
                                                page: page || 1,
                                            },
                                        }
                                    },
                                },
                                filter: function (data = {}) {
                                    debugger
                                    const query = data.query || {};
                                    //const { categoryId, exportUuid } = query
                                    const pagination = data.pagination || {};
                                    if (START_PAGE === 1) {
                                        pagination.page = START_PAGE;
                                    }
                                    return {
                                        limit: pagination.perpage || LIMIT,
                                        page: pagination.page,
                                        categoryId: categoryId,
                                        exportUuid: exportUuid,
                                    }
                                }
                            },
                            pageSize: LIMIT,
                            serverPaging: true,
                            serverFiltering: true,
                        },

                        search: {
                            onEnter: true,

                        },
                        columns: [
                            {
                                field: 'uuid',
                                title: '#',
                                width: 30,
                                textAlign: 'center',
                                template: function (data, row) {
                                    return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                                },
                                responsive: {
                                    visible: 'md',
                                    hidden: 'sm'
                                }
                            },
                            {
                                field: 'name',
                                title: 'Tên thiết bị',
                                template: function (data) {
                                    var output = '';
                                    if (data.prefixName) {
                                        output += '<strong class="text-primary">' + data.prefixName + '</strong></br>';
                                    }
                                    output += '<span>' + data.name + '</span>';
                                    return output;
                                },
                            },
                            {
                                field: 'serial',
                                title: 'Serial',
                                width: 150
                            },
                            {
                                field: 'timecreatedformat',
                                title: 'Ngày hết hạn',
                                width: 120
                            }
                        ],
                        sortable: false,
                        pagination: true,
                        responsive: true,
                    });
                    //initContent($content);
                })
                .fail(function () {
                    drawerDetail.error();
                });
        });
        drawerDetail.open();
    };

    var detailImportDevice = function (categoryId, importUuid) {
        var title = `<span class="text-uppercase">Danh sách chi tiết thiết bị</span>`;

        drawerDetail = new KTDrawer({ title, id: null || 'new', className: 'dmms-drawer--detailimportticket' });

        drawerDetail.on('ready', () => {
            $.ajax({
                url: `/bunker/DetailSupplies`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawerDetail.setBody(response);
                    dataTableInstanceImDevice = $('.kt-datatable--detailsuppliesdevice').KTDatatable({
                        data: {
                            type: 'remote',
                            source: {
                                read: {
                                    url: `/bunker/DetailDeviceIm`,
                                },
                                response: {
                                    map: function (res) {
                                        debugger
                                        const raw = res && res.data || {};
                                        const { pagination } = raw;
                                        const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                                        const page = pagination === undefined ? 0 : pagination.page;
                                        CURRENT_PAGE = page || 1;
                                        LIMIT = perpage;
                                        START_PAGE = 0;
                                        return {
                                            data: raw.items || [],
                                            meta: {
                                                ...pagination || {},
                                                perpage: perpage || LIMIT,
                                                page: page || 1,
                                            },
                                        }
                                    },
                                },
                                filter: function (data = {}) {
                                    debugger
                                    const query = data.query || {};
                                    const pagination = data.pagination || {};
                                    if (START_PAGE === 1) {
                                        pagination.page = START_PAGE;
                                    }
                                    return {
                                        limit: pagination.perpage || LIMIT,
                                        page: pagination.page,
                                        categoryId: categoryId,
                                        importUuid: importUuid
                                    }
                                }
                            },
                            pageSize: LIMIT,
                            serverPaging: true,
                            serverFiltering: true,
                        },

                        search: {
                            onEnter: true,

                        },
                        columns: [
                            {
                                field: 'uuid',
                                title: '#',
                                width: 30,
                                textAlign: 'center',
                                template: function (data, row) {
                                    return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                                },
                                responsive: {
                                    visible: 'md',
                                    hidden: 'sm'
                                }
                            },
                            {
                                field: 'name',
                                title: 'Tên thiết bị',
                                template: function (data) {
                                    var output = '';
                                    if (data.prefixName) {
                                        output += '<strong class="text-primary">' + data.prefixName + '</strong></br>';
                                    }
                                    output += '<span>' + data.name + '</span>';
                                    return output;
                                },
                            },
                            {
                                field: 'serial',
                                title: 'Serial',
                                width: 150
                            },
                            {
                                field: 'timecreatedformat',
                                title: 'Ngày hết hạn',
                                width: 120
                            }

                        ],
                        sortable: false,
                        pagination: true,
                        responsive: true,
                    });
                    //initContent($content);
                })
                .fail(function () {
                    drawerDetail.error();
                });
        });
        drawerDetail.open();
    };

    //
    var detailImportTicket = function (id, name) {
        if (name != "null") {
            var title = `<span class="text-uppercase">Phiếu nhập kho từ ${name}</span>`;
        }
        else {
            var title = `<span class="text-uppercase">Phiếu nhập kho</span>`;
        }
        drawerim = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--detailimportticket' });

        drawerim.on('ready', () => {
            $.ajax({
                url: `/bunker/detailimportticket`,
                method: 'GET',
                data: { import_uuid: id }
            })
                .done(function (response) {
                    debugger
                    const $content = drawerim.setBody(response);
                    $(".importdetail").on("click", function () {
                        debugger
                        var importUuid = $('.importUuid').val()

                        if (dataTableDetailIm != null) dataTableDetailIm.destroy();


                        if ($('.dmms-form-bunker__importticket').find('.kt-datatableim').length > 0) {
                            dataTableDetailIm = $('.kt-datatableim').KTDatatable({
                                data: {
                                    type: 'remote',
                                    source: {
                                        read: {
                                            url: `/bunker/DetailCategoriesImport/${importUuid}`,
                                        },
                                        response: {
                                            map: function (res) {
                                                const raw = res && res.data || {};
                                                const { pagination } = raw;
                                                const perpage = pagination === undefined ? undefined : pagination.perpage;
                                                const page = pagination === undefined ? undefined : pagination.page;
                                                START_PAGE = 0;
                                                return {
                                                    data: raw.items || [],
                                                    meta: {
                                                        ...pagination || {},
                                                        perpage: perpage || LIMIT,
                                                        page: page || 1,
                                                    },
                                                }
                                            },
                                        },
                                        filter: function (data = {}) {

                                            const query = data.query || {};
                                            const pagination = data.pagination || {};
                                            if (START_PAGE === 1) {
                                                pagination.page = START_PAGE;
                                            }
                                            return {

                                                limit: pagination.perpage || LIMIT,
                                                page: pagination.page
                                            };
                                        }
                                    },
                                    pageSize: LIMIT,
                                    serverPaging: true,
                                    serverFiltering: true,
                                },
                                layout: {
                                    scroll: true
                                },
                                search: {
                                    onEnter: true,
                                },
                                columns: [
                                    {
                                        field: 'uuid',
                                        title: '#',
                                        sortable: false,
                                        textAlign: 'center',
                                        width: 20,
                                        template: function (data, row) {
                                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                                        },
                                    },
                                    {
                                        field: 'name',
                                        title: 'Tên vật tư-thiết bị',
                                        template: function (data) {
                                            var output = '';
                                            if (data.prefixName) {
                                                output += '<strong class="text-primary">' + data.prefixName + '</strong></br>';
                                            }
                                            output += '<span>' + data.name + '</span>';
                                            return output;
                                        },
                                    },
                                    {
                                        field: 'type',
                                        title: 'Phân loại',
                                        width: 50,
                                        autoHide: false,
                                        template: function (data) {
                                            if (data.type == 1) {
                                                return "Thiết bị";
                                            }
                                            else if (data.type == 2) {
                                                return "Vật tư";
                                            }
                                            else if (data.type == 3) {
                                                return "Dụng cụ";
                                            }
                                        },
                                    },

                                    {
                                        field: 'documentQuantity',
                                        title: 'SL theo CT',
                                        autoHide: false,
                                        width: 70
                                    },
                                    {
                                        field: 'realQuantity',
                                        title: 'SL thực',
                                        autoHide: false,
                                        width: 70
                                    },
                                    {
                                        field: 'unitPrice',
                                        title: 'Đơn giá',
                                        autoHide: false,
                                        width: 80,
                                        template: function (data) {
                                            if (data.unitPrice != null) {
                                                var formatter = new Intl.NumberFormat('vi-VN', {
                                                    style: 'currency',
                                                    currency: 'VND',
                                                    minimumFractionDigits: 0
                                                })
                                                return formatter.format(data.unitPrice)
                                            }

                                        }

                                    },
                                    {
                                        field: '#4',
                                        title: 'Thành tiền',
                                        autoHide: false,
                                        width: 110,
                                        autoHide: false,
                                        template: function (data) {
                                            if (data.unitPrice) {
                                                var formatter = new Intl.NumberFormat('vi-VN', {
                                                    style: 'currency',
                                                    currency: 'VND',
                                                    minimumFractionDigits: 0
                                                })
                                                return formatter.format(data.unitPrice * data.documentQuantity)
                                            }
                                        }
                                    },
                                    {
                                        field: 'Tác vụ',
                                        title: 'Tác vụ',
                                        sortable: false,
                                        width: 80,
                                        autoHide: false,
                                        textAlign: 'center',
                                        template: function (data) {
                                            var output = "";
                                            if (data.type != 1) {
                                                output = `<button data-id="${data.id}" data-importUuid="${importUuid}" type="button" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md" disabled>
                                                     <i class="la la-edit"></i>
                                                    </button>`;
                                            }
                                            else {
                                                output = `<button data-id="${data.id}" data-importUuid="${importUuid}" type="button" title="Chỉnh sửa" class="js-detail-import-device btn btn-sm btn-primary btn-icon btn-icon-md">
                                                     <i class="la la-edit"></i>
                                                    </button>`;
                                            }
                                            return output;
                                        },
                                    }],
                                sortable: false,
                                pagination: true,
                                responsive: true,
                            });
                        }
                        else {
                            dataTableDetailIm = $('.kt-datatableim_nottype2').KTDatatable({
                                data: {
                                    type: 'remote',
                                    source: {
                                        read: {
                                            url: `/bunker/DetailCategoriesImport/${importUuid}`,
                                        },
                                        response: {
                                            map: function (res) {
                                                const raw = res && res.data || {};
                                                const { pagination } = raw;
                                                const perpage = pagination === undefined ? undefined : pagination.perpage;
                                                const page = pagination === undefined ? undefined : pagination.page;
                                                START_PAGE = 0;
                                                return {
                                                    data: raw.items || [],
                                                    meta: {
                                                        ...pagination || {},
                                                        perpage: perpage || LIMIT,
                                                        page: page || 1,
                                                    },
                                                }
                                            },
                                        },
                                        filter: function (data = {}) {

                                            const query = data.query || {};
                                            const pagination = data.pagination || {};
                                            if (START_PAGE === 1) {
                                                pagination.page = START_PAGE;
                                            }
                                            return {

                                                limit: pagination.perpage || LIMIT,
                                                page: pagination.page
                                            };
                                        }
                                    },
                                    pageSize: LIMIT,
                                    serverPaging: true,
                                    serverFiltering: true,
                                },
                                layout: {
                                    scroll: true
                                },
                                search: {
                                    onEnter: true,
                                },
                                columns: [
                                    {
                                        field: 'uuid',
                                        title: '#',
                                        sortable: false,
                                        textAlign: 'center',
                                        width: 20,
                                        template: function (data, row) {
                                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                                        },
                                    },
                                    {
                                        field: 'name',
                                        title: 'Tên vật tư-thiết bị',
                                        template: function (data) {
                                            var output = '';
                                            if (data.prefixName) {
                                                output += '<strong class="text-primary">' + data.prefixName + '</strong></br>';
                                            }
                                            output += '<span>' + data.name + '</span>';
                                            return output;
                                        },
                                    },
                                    {
                                        field: 'type',
                                        title: 'Phân loại',
                                        width: 50,
                                        autoHide: false,
                                        template: function (data) {
                                            if (data.type == 1) {
                                                return "Thiết bị";
                                            }
                                            else if (data.type == 2) {
                                                return "Vật tư";
                                            }
                                            else if (data.type == 3) {
                                                return "Dụng cụ";
                                            }
                                        },
                                    },

                                    {
                                        field: 'documentQuantity',
                                        title: 'SL theo CT',
                                        autoHide: false,
                                        width: 70
                                    },
                                    {
                                        field: 'realQuantity',
                                        title: 'SL thực',
                                        autoHide: false,
                                        width: 70
                                    },
                                    {
                                        field: 'Tác vụ',
                                        title: 'Tác vụ',
                                        sortable: false,
                                        width: 80,
                                        autoHide: false,
                                        textAlign: 'center',
                                        template: function (data) {
                                            var output = "";
                                            if (data.type != 1) {
                                                output = `<button data-id="${data.id}" data-importUuid="${importUuid}" type="button" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md" disabled>
                                                     <i class="la la-edit"></i>
                                                    </button>`;
                                            }
                                            else {
                                                output = `<button data-id="${data.id}" data-importUuid="${importUuid}" type="button" title="Chỉnh sửa" class="js-detail-import-device btn btn-sm btn-primary btn-icon btn-icon-md">
                                                     <i class="la la-edit"></i>
                                                    </button>`;
                                            }
                                            return output;
                                        },
                                    }],
                                sortable: false,
                                pagination: true,
                                responsive: true,
                            });
                        }
                    });

                    $('.kt-close').on('click', function () {
                        drawerim.close();
                    });
                    //initContent($content);
                })
                .fail(function () {
                    drawerim.error();
                });
        });
        drawerim.open();
    };
    //
    var detailExportTicket = function (id, name) {
        if (name != "null") {
            var title = `<span class="text-uppercase">Phiếu xuất kho từ ${name}</span>`;
        }
        else {
            var title = `<span class="text-uppercase">Phiếu xuất kho</span>`;
        }
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--detailexportticket' });

        drawer.on('ready', () => {
            $.ajax({
                url: `/bunker/DetailExportDetail`,
                method: 'GET',
                data: { export_uuid: id }
            })
                .done(function (response) {
                    debugger
                    const $content = drawer.setBody(response);
                    $(".exportdetail").on("click", function () {

                        var exportUuid = $('.exportUuid').val()

                        //if (dataTableInstancepackage != null) dataTableInstancepackage.destroy();
                        if ($('.dmms-form-bunker__exportticket').find('.kt-datatableex').length > 0) {
                            dataTableInstanceEx = $('.kt-datatableex').KTDatatable({
                                data: {
                                    type: 'remote',
                                    source: {
                                        read: {
                                            url: `/bunker/DetailCategoriesExport/${exportUuid}`,
                                        },
                                        response: {
                                            map: function (res) {
                                                const raw = res && res.data || {};
                                                const { pagination } = raw;
                                                const perpage = pagination === undefined ? undefined : pagination.perpage;
                                                const page = pagination === undefined ? undefined : pagination.page;
                                                START_PAGE = 0;
                                                return {
                                                    data: raw.items || [],
                                                    meta: {
                                                        ...pagination || {},
                                                        perpage: perpage || LIMIT,
                                                        page: page || 1,
                                                    },
                                                }
                                            },
                                        },
                                        filter: function (data = {}) {

                                            const query = data.query || {};
                                            const pagination = data.pagination || {};

                                            if (START_PAGE === 1) {
                                                pagination.page = START_PAGE;
                                            }
                                            return {

                                                limit: pagination.perpage || LIMIT,
                                                page: pagination.page
                                            };
                                        }
                                    },
                                    pageSize: LIMIT,
                                    serverPaging: true,
                                    serverFiltering: true,
                                },
                                layout: {
                                    scroll: true
                                },
                                search: {
                                    onEnter: true,
                                },
                                columns: [
                                    {
                                        field: 'uuid',
                                        title: '#',
                                        sortable: false,
                                        textAlign: 'center',
                                        width: 20,
                                        template: function (data, row) {
                                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                                        },
                                    },
                                    {
                                        field: 'name',
                                        title: 'Tên vật tư-thiết bị',
                                        template: function (data) {
                                            var output = '';
                                            if (data.prefixName) {
                                                output += '<strong class="text-primary">' + data.prefixName + '</strong></br>';
                                            }
                                            output += '<span>' + data.name + '</span>';
                                            return output;
                                        },
                                    },
                                    {
                                        field: 'type',
                                        title: 'Phân loại',
                                        width: 70,
                                        autoHide: false,
                                        template: function (data) {
                                            if (data.type == 1) {
                                                return "Thiết bị";
                                            }
                                            else if (data.type == 2) {
                                                return "Vật tư";
                                            }
                                            else if (data.type == 3) {
                                                return "Dụng cụ";
                                            }
                                        },
                                    },
                                    {
                                        field: 'documentQuantity',
                                        title: 'SL theo CT',
                                        width: 60,
                                        autoHide: false,
                                    },
                                    {
                                        field: 'realQuantity',
                                        title: 'SL thực',
                                        width: 60,
                                        autoHide: false,
                                    },
                                    {
                                        field: 'unitPrice',
                                        title: 'Đơn giá',
                                        autoHide: false,
                                        width: 70,
                                        autoHide: false,
                                    },
                                    {
                                        field: 'unitPrice',
                                        title: 'Thành tiền',
                                        autoHide: false,
                                        width: 100,
                                        autoHide: false,
                                    },

                                    {
                                        field: 'Tác vụ',
                                        title: 'Tác vụ',
                                        sortable: false,
                                        width: 100,
                                        autoHide: false,
                                        textAlign: 'center',
                                        template: function (data) {
                                            var output = ''
                                            if (data.type == 1) {
                                                output = `<button data-id="${data.id}" data-exportUuid = "${exportUuid}"type="button" title="Chỉnh sửa" class="js-detail-export-device btn btn-sm btn-primary btn-icon btn-icon-md">
                                                             <i class="la la-edit" style="background-color: transparent;"></i>
                                                            </button>`
                                            }
                                            else {
                                                output = `<button data-id="${data.id}" data-exportUuid = "${exportUuid}"type="button" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md" disabled>
                                                             <i class="la la-edit" style="background-color: transparent;"></i>
                                                            </button>`
                                            }
                                            return output;
                                        }
                                    }],
                                sortable: false,
                                pagination: true,
                                responsive: true,
                            });
                        }
                        else {
                            dataTableInstanceEx = $('.kt-datatableex_nottype4').KTDatatable({
                                data: {
                                    type: 'remote',
                                    source: {
                                        read: {
                                            url: `/bunker/DetailCategoriesExport/${exportUuid}`,
                                        },
                                        response: {
                                            map: function (res) {
                                                const raw = res && res.data || {};
                                                const { pagination } = raw;
                                                const perpage = pagination === undefined ? undefined : pagination.perpage;
                                                const page = pagination === undefined ? undefined : pagination.page;
                                                START_PAGE = 0;
                                                return {
                                                    data: raw.items || [],
                                                    meta: {
                                                        ...pagination || {},
                                                        perpage: perpage || LIMIT,
                                                        page: page || 1,
                                                    },
                                                }
                                            },
                                        },
                                        filter: function (data = {}) {

                                            const query = data.query || {};
                                            const pagination = data.pagination || {};

                                            if (START_PAGE === 1) {
                                                pagination.page = START_PAGE;
                                            }
                                            return {

                                                limit: pagination.perpage || LIMIT,
                                                page: pagination.page
                                            };
                                        }
                                    },
                                    pageSize: LIMIT,
                                    serverPaging: true,
                                    serverFiltering: true,
                                },
                                layout: {
                                    scroll: true
                                },
                                search: {
                                    onEnter: true,
                                },
                                columns: [
                                    {
                                        field: 'uuid',
                                        title: '#',
                                        sortable: false,
                                        textAlign: 'center',
                                        width: 20,
                                        template: function (data, row) {
                                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                                        },
                                    },
                                    {
                                        field: 'name',
                                        title: 'Tên vật tư-thiết bị',
                                        template: function (data) {
                                            var output = '';
                                            if (data.prefixName) {
                                                output += '<strong class="text-primary">' + data.prefixName + '</strong></br>';
                                            }
                                            output += '<span>' + data.name + '</span>';
                                            return output;
                                        },
                                    },
                                    {
                                        field: 'type',
                                        title: 'Phân loại',
                                        width: 70,
                                        autoHide: false,
                                        template: function (data) {
                                            if (data.type == 1) {
                                                return "Thiết bị";
                                            }
                                            else if (data.type == 2) {
                                                return "Vật tư";
                                            }
                                            else if (data.type == 3) {
                                                return "Dụng cụ";
                                            }
                                        },
                                    },
                                    {
                                        field: 'documentQuantity',
                                        title: 'SL theo CT',
                                        width: 60,
                                        autoHide: false,
                                    },
                                    {
                                        field: 'realQuantity',
                                        title: 'SL thực',
                                        width: 60,
                                        autoHide: false,
                                    },
                                    {
                                        field: 'Tác vụ',
                                        title: 'Tác vụ',
                                        sortable: false,
                                        width: 100,
                                        autoHide: false,
                                        textAlign: 'center',
                                        template: function (data) {
                                            var output = ''
                                            if (data.type == 1) {
                                                output = `<button data-id="${data.id}" data-exportUuid = "${exportUuid}"type="button" title="Chỉnh sửa" class="js-detail-export-device btn btn-sm btn-primary btn-icon btn-icon-md">
                                                             <i class="la la-edit" style="background-color: transparent;"></i>
                                                            </button>`
                                            }
                                            else {
                                                output = `<button data-id="${data.id}" data-exportUuid = "${exportUuid}"type="button" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md" disabled>
                                                             <i class="la la-edit" style="background-color: transparent;"></i>
                                                            </button>`
                                            }
                                            return output;
                                        },
                                    }],
                                sortable: false,
                                pagination: true,
                                responsive: true,
                            });
                        }

                    });
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });
        drawer.open();
    };
    //+
    var renderDualList1 = function (left, right) {
        const options = [];
        $.each(left, (index, { uuid, name }) => {
            const text = [name || ''];
            options.push({
                text: text.join(' - ') || '-- No name --',
                value: uuid,
            });
        });

        $.each(right, (index, { uuid, name }) => {
            const text = [name || ''];

            options.push({
                text: text.join(' - ') || '-- No name --',
                value: uuid,
                selected: true,
            });
        });

        const optsHtml = [];
        $.each(options, (index, item) => {
            optsHtml.push(`<option value="${item.value}" ${item.selected ? 'selected' : ''}>${item.text}</option>`);
        });

        const $content = drawerInstance.setBody(`
            <select class="kt-dual-listbox-staff" multiple>${optsHtml.join('')}</select>
        `);

        new KTDualListBox($content.find('.kt-dual-listbox-staff')[0], {
            availableTitle: 'Danh sách đội',
            selectedTitle: 'Danh sách đội của nhân viên',
            //availableTitle: module === 'staff' ? 'Danh sách nhân viên' : 'Danh sách khách hàng',
            //selectedTitle: module === 'staff' ? 'Nhân viên trong nhóm' : 'Khách hàng trong nhóm',
        });
    };

    //+
    var initDrawer = function ({ id }) {
        const title = 'Danh sách nhóm';
        drawerInstance = new KTDrawer({
            title,
            id: `team-all`,
            onRenderFooter: (callback) => {
                callback(`
          <button data-id=${id} type="button" class="btn btn-primary js-save-team-staff">
            <i class="la la-save"></i>
            Lưu
          </button>
        `);
            },
        });

        drawerInstance.on('ready', () => {
            var left = null;
            var right = null;
            $.ajax({
                url: `/staff/get-list-team`,
                method: 'GET',
            })
                .done((res) => {
                    /**
                     * Set data cột trái
                     */
                    if ($.isArray(res)) {
                        left = res;
                    }

                    /**
                     * Load đủ data --> Render List
                     */
                    if (left && right) {
                        renderDualList1(left, right);
                    }
                })
                .fail((error) => {
                    drawerInstance.error(error);
                });

            $.ajax({
                url: `/team/staff/${id}`,
                method: 'GET',
            })
                .done((res) => {
                    /**
                     * Set data cột phải
                     */
                    if ($.isArray(res)) {
                        right = res;
                    }

                    /**
                     * Load đủ data --> Render List
                     */
                    if (left && right) {
                        renderDualList1(left, right);
                    }
                })
                .fail((error) => {
                    drawerInstance.error(error);
                })
        });

        drawerInstance.open();
    };

    var initForm = function ($form) {
        initValidation(true);
    }
    var initValidation = function (isForm) {
        if ($("#type").val() !== '') {
            $('#kt_select_type_staff').selectpicker('val', $("#type").val());
        }
        else {
            $('#kt_select_type_staff').selectpicker();
        }

        $(".dmms-staff__form").validate({
            rules: {
                name: {
                    required: true,
                },
                email: {
                    email: true,
                    required: true,
                },
                phone_number: {
                    phoneVN: true,
                    required: true,
                },
                address: {
                    required: true,
                },
                province_id: {
                    required: true
                },
                //'account.username': {
                //    required: true,
                //},
                //'account.password': {
                //    required: true,
                //    minlength: 6
                //},
                re_password: {
                    equalTo: "#password",
                    minlength: 6
                },
            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên nhân viên.'
                },
                email: {
                    required: 'Vui lòng nhập email nhân viên.'
                },
                phone_number: {
                    required: 'Vui lòng nhập số điện thoại.'
                },
                address: {
                    required: 'Vui lòng nhập địa chỉ.'
                },
                province_id: {
                    required: 'Vui lòng nhập tỉnh thành.'
                },
                // 'account.username': {
                //    required: 'Vui lòng nhập tài khoản',
                //},
                //'account.password': {
                //    required: 'Vui lòng nhập mật khẩu',
                //    minlength: 'Hãy nhập tối thiểu 6 ký tự'
                //},
                re_password: {
                    equalTo: "Hai password phải giống nhau",
                    minlength: "Hãy nhập ít nhất 6 ký tự"
                }
            },

            //display error alert on form submit
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('.dmms-staff__form', -200);
                $('.kt-select-location').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });
            },

            submitHandler: function (form) {
                $(form).find('button[type="submit"]').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                $.ajax({
                    url: uuid ? `/staff/update/` : 'staff/create',
                    method: uuid ? 'PUT' : 'POST',
                    data,
                })
                    .done(function (data) {
                        KTApp.block('.dmms-staff__form');
                        $(".submit").attr("disabled", true);
                        const successMsg = uuid ? 'Sửa thông tin nhân viên thành công!' : 'Thêm nhân viên thành công!';
                        $.notify(data.msg, { type: 'success' });
                        if (isForm) {
                            drawer.close();
                            reloadTable();
                        } else {
                            /**
                      * Close drawer
                      */
                            localStorage['edit_success'] = JSON.stringify({ from: (uuid ? `/staff/update/` : '/staff/create'), massage: successMsg });

                            window.location = window.location.origin + "/staff";
                        }


                    })
                    .fail(function (data) {
                        $(form).find('button[type="submit"]').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        KTApp.unblock('.dmms-staff__form');
                        const errorMsg = uuid ? 'Sửa thông tin nhân viên không thành công!' : 'Thêm nhân viên không thành công!';
                        $.notify(data.responseJSON.msg, { type: 'danger' });
                    });
                return false;
            },
        });
    }
    var initContent = function ($content) {
        /*
        Init form
        */
        $('.kt_select_type_staff').selectpicker();
        $('.js-select-unitSignedUuid').selectpicker();
        $('.kt_select_type_team').selectpicker();
        $('.kt-select-location').selectpicker();
        $('.kt-select-location')

            .on('change', function () {
                debugger
                $('#locationtown1').val('');
                $('#locationvillage1').val('');
                var provinceid = $(this).val();
                $.ajax({
                    url: `/customer/getlocationtown/${provinceid}`,
                    method: 'GET',
                })
                    .done(function (data) {
                        debugger
                        console.log(data);
                        var town = data.data.items;

                        var $select = $('#locationtown1');

                        $('#locationvillage1').attr('disabled', true);
                        $('#locationvillage1').selectpicker('refresh')
                        $select.find('option').remove();
                        $select.append(`<option value=''>---Quận/Huyện/Thị Xã---</option>)`);
                        $.each(town, function (index, item) {

                            console.log(item);
                            $select.append('<option value=' + item.id + '>' + item.name + '</option>');
                            $select.attr('disabled', false)
                            $select.selectpicker('refresh');
                        })


                    })
                    .fail(function () {

                    });
            });
        $('#locationtown1')
            .on('change', function () {
                $('#locationvillage1').val('');
                var town = $(this).val();
                $.ajax({
                    url: `/customer/getlocationvillages/${town}`,
                    method: 'GET',
                })
                    .done(function (data) {
                        debugger
                        console.log(data);
                        var village = data.data.items;

                        var $select = $('#locationvillage1');
                        $select.find('option').remove();
                        $select.append(`<option value=''>---Phường/Xã/Thị Trấn---</option>)`);
                        $.each(village, function (index, item) {

                            console.log(item);
                            $select.append('<option value=' + item.id + '>' + item.name + '</option>');
                            $select.attr('disabled', false)
                            $select.selectpicker('refresh');
                        })


                    })
                    .fail(function () {

                    });
            });
        $('#kt_form_team_add').selectpicker();
        $("#test").change(function () {
            debugger
            if ($(this).val() == "") {
                $('.checkUnitsigned').remove();
                $('.appendUnitsigned').append(`<div class="kt-input-icon">
                <input name="unit_signed_name" class="form-control" placeholder="Đơn vị công tác" />
            </div>`);

            }
            else {
                var name = $(this).find('option:selected').text();
                $('.appendUnitsigned').append(`<input name="unit_signed_name" value=${name} hidden />`);
            }


        });
        const $form = $content.find('.dmms-staff__form');
        initForm($form);

        /*
        Init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));
        avata();
        avatar();
        initDropzone();
    };
    var avata = function () {
        $('input[type=file]').on('change', function (event) {

            var files = event.target.files;
            event.stopPropagation(); // Stop stuff happening
            event.preventDefault(); // Totally stop stuff happening
            $("#avatar-status").text("Loading new avatar...");
            $("#avatar").css("opacity", "0.4");
            $("#avatar").css("filter", "alpha(opacity=40);");
            //Create a formdata object and add the files
            var data = new FormData();
            $.each(files, function (key, value) {
                data.append(key, value);
            });
            $.ajax({
                url: "/images/UploadAva",
                type: 'POST',
                data: data,
                enctype: 'multipart/form-data',
                processData: false,  // do not process the data as url encoded params
                contentType: false,   // by default jQuery sets this to urlencoded string
                success: function (data, textStatus, jqXHR) {

                    if (data != null && data.length > 0) {
                        $(".erroavata").find(".deleteno").remove();
                        $("#image").val(data[0].id);
                        //$("#imgAvatar").attr('src', "https://api.ngot.club/" + response[0].path);
                    }
                    else {
                        $(".erroavata").append('Lỗi abbbb')
                        //Handle errors here
                        alert('ERRORS: ' + textStatus);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //Handle errors here
                    $(".erroavata").find(".deleteno").remove();
                    var html = "</br><span class = 'text-danger deleteno'>Không tải được ảnh lên vui lòng thử lại!</span>"
                    $(".erroavata").append(html)
                    //alert('ERRORS: ' + textStatus);

                }
            });
        });
    }

    var avatar = function () {
        image = new KTAvatar('kt_user_avatar');
    }
    var initDropzone = function () {
        var dropZone = $('#kt_dropzone_image');
        if (dropZone !== undefined && dropZone.length > 0) {
            new Dropzone('#kt_dropzone_image', {
                url: "/images/upload",
                paramName: "images",
                maxFiles: 1,
                maxFilesize: 5, // MB
                addRemoveLinks: true,
                dictDefaultMessage: 'Bấm hoặc kéo thả ảnh vào đây để tải ảnh lên',
                dictRemoveFile: 'Xóa ảnh',
                dictCancelUpload: 'Hủy',
                dictCancelUploadConfirmation: 'Bạn thực sự muốn hủy?',
                dictMaxFilesExceeded: 'Bạn không thể tải thêm ảnh!',
                dictFileTooBig: "Ảnh tải lên dung lượng quá lớn ({{filesize}}MB). Max filesize: {{maxFilesize}}MB.",
                autoProcessQueue: true,
                accept: function (file, done) {
                    done();
                },
                success: function (file, response) {
                    if (response != null && response.length > 0) {
                        $("#image").val(response[0].id);
                        //$("#imgAvatar").attr('src', "https://api.ngot.club/" + response[0].path);
                    }
                    else {
                        $(file.previewElement).addClass("dz-error").find('.dz-error-message').text("Không tải được ảnh lên.");
                    }
                },
                error: function (file, response, errorMessage) {
                    console.log(file, response);
                    if (file.accepted) {
                        $(file.previewElement).addClass("dz-error").find('.dz-error-message').text("Có lỗi xảy ra, không tải được ảnh lên.");
                    }
                    else {
                        $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(response);
                    }
                }
            });
        }
    }
    var location = function () {

    }
    var location = function () {
        $('#js-filter-location')
            .on('change', function () {

                debugger
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val(), 'provinceId');
                    START_PAGE = 1;
                }
                $('#locationtown').val('');
                $('#locationvillage').val('');
                var provinceid = $(this).val();
                $.ajax({
                    url: `/project/getlocationtown/${provinceid}`,
                    method: 'GET',
                })
                    .done(function (data) {
                        debugger
                        console.log(data);
                        var town = data.data.items;

                        var $select = $('#locationtown');

                        $('#locationvillage').attr('disabled', true);
                        $('#locationvillage').selectpicker('refresh')
                        $select.find('option').remove();
                        $select.append(`<option value=''>---Quận/Huyện/Thị Xã---</option>)`);
                        $.each(town, function (index, item) {

                            console.log(item);
                            $select.append('<option value=' + item.id + '>' + item.name + '</option>');
                            $select.attr('disabled', false)
                            $select.selectpicker('refresh');
                        })


                    })
                    .fail(function () {

                    });
            });
        $('#locationtown')
            .on('change', function () {
                debugger
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val(), 'districtId');
                    START_PAGE = 1;
                }
                $('#locationvillage').val('');
                var town = $(this).val();
                $.ajax({
                    url: `/project/getlocationvillages/${town}`,
                    method: 'GET',
                })
                    .done(function (data) {
                        debugger
                        console.log(data);
                        var village = data.data.items;

                        var $select = $('#locationvillage');

                        //for (var i = 0; i < customer.length; i++) {
                        //    $select.append('<option value=' + customer[i].project_customer_uuid + '>' + customer[i].name + '</option>');
                        //    $select.selectpicker('refresh');
                        //}
                        $select.find('option').remove();
                        $select.append(`<option value=''>---Phường/Xã/Thị Trấn---</option>)`);
                        $.each(village, function (index, item) {

                            console.log(item);
                            $select.append('<option value=' + item.id + '>' + item.name + '</option>');
                            $select.attr('disabled', false)
                            $select.selectpicker('refresh');
                        })


                    })
                    .fail(function () {

                    });
            });
    }

    return {
        // Public functions
        init: function () {
            avata();
            avatar();
            initTable();
            initSearch();
            initEventListeners();
            if (window.location.pathname != '/staff') {
                initDropzone();
            }
            $('.filterstaff').selectpicker();
            $('.js-select-field1').selectpicker();

            //initContent();
            initValidation();
            location();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-staff').length) {
        DMMSStaff.init();
    }
});
