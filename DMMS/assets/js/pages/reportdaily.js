"use strict";
// Class definition

var DMMSReportdaily = function () {
    // Private functions

    var dataTableInstance = null;
    var drawer = null;
    var LIMIT = 20;
    var uuid_images = "";

    // demo initializer
    var initTable = function () {
        dataTableInstance = $('.kt-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/reportdaily/gets',
                    },
                    response: {
                        map: function (res) {
                            debugger
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            if (raw.items != null) {
                                $.each(raw.items, function (index, item) {
                                    for (var property in item) {
                                        if (item[property] && typeof item[property] === "object") {
                                            $.each(item[property], function (i, v) {
                                                for (var pro in item[property]) {
                                                    if (item[property][pro] && typeof item[property][pro] === "string") {
                                                        item[property][pro] = item[property][pro].replace(/</g, "&lt;").replace(/>/g, "&gt;");
                                                    }
                                                }
                                            })
                                        }
                                        if (item[property] && typeof item[property] === "string") {
                                            item[property] = item[property].replace(/</g, "&lt;").replace(/>/g, "&gt;");
                                        }
                                    }
                                })
                            }
                            const perpage = pagination === undefined ? 0 : pagination.perpage;
                            const page = pagination === undefined ? 0 : pagination.page;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { state, staff_uuid, task_uuid, date_report1,check } = query;
                        return {
                            limit: pagination.perpage || LIMIT,
                            staff_uuid: staff_uuid || $('#js-filter-staff').val(),
                            check: check || $('#js-filter-report').val(),
                            task_uuid: task_uuid || $('#js-filter-task').val(),
                            page: pagination.page,
                            state: state || $('#js-filter-states').val(),
                            
                            date_report1: date_report1 || $('.kt_datepicker_expire_device').val(),

                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: false,
                serverFiltering: true,
            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 20,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'date_report_formatnohh',
                    title: 'Ngày báo cáo',
                    width: 90,
                    textAlign: 'center'
                },
                {
                    field: 'task.name',
                    title: 'Công việc báo cáo',

                },
                {
                    field: 'staff.name',
                    title: 'Người báo cáo',
                    width: 120,
                    template: function (data) {
                        var output = '';
                        if (data.staff != null && data.staff.name != null) {
                            output += '<a href = "/staff/update/' + data.staff.uuid + '"><span><strong>' + data.staff.name + '</strong></span ></a>';
                        }
                        return output;
                    }
                },
                {
                    field: 'reviewer.name',
                    title: 'Người duyệt',
                    width:120
                },
                {
                    field: 'state',
                    title: 'Trạng thái',
                    template: function (data) {
                        var states = {
                            1: { 'title': 'Chưa tạo', 'class': 'kt-badge--brand' },
                            2: { 'title': 'Chưa duyệt', 'class': ' kt-badge--warning' },
                            3: { 'title': 'Đã duyệt', 'class': ' kt-badge--primary' },
                            4: { 'title': 'Từ chối', 'class': 'kt-badge--success' },
                        };
                        if (data.state) {
                            return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill">' + states[data.state].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới</span>';
                        }
                    },
                    width: 100,
                    textAlign: 'center'
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    textAlign:'center',
                    width: 40,
                    overflow: 'visible',
                    autoHide: false,
                    template: function (data) {
                        return `
                              <a href="/reportdaily/edit/${data.uuid}" title="Xem chi tết" class="btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-info"></i>
                            </a>
                            
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };
    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };
    var deleteReport = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa báo cáo...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/reportdaily/delete/${id}`,
                method: 'DELETE',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa báo cáo thành công!', { type: 'success' });

                    if (dataTableInstance) {
                        dataTableInstance.reload();
                    }
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa báo cáo không thành công!', { type: 'danger' });
                });
        }
    };
    var initSearch = function () {
        $('#js-filter-staff')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'staff_uuid');
                }
            })
            .selectpicker();
        $('#js-filter-task')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'task_uuid');
                }
            })
            .selectpicker();
       
        $('#js-filter-states')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'state');
                }
            })
            .selectpicker();
        $('#js-filter-report')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'check');
                }
            })
            .selectpicker();
        $('.kt_datepicker_expire_device')
            .on('change', function () {
                
                if (dataTableInstance) {
                    
                    dataTableInstance.search($(this).val().toLowerCase(), 'date_report1');
                }
            })
            .selectpicker();


    };

    var initSupplierDrawer = function (id) {
        const title = id ? 'Sửa thông tin báo' : 'Thêm báo cáo';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--reportdaily' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/reportdaily/edit/${id}` : '/reportdaily/create',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-reportdaily')
            .on('click', '.js-delete-reportdaily', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Báo cáo sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteReport(id);
                    }
                });
            })
            .off('click', '.js-edit-reportdaily')
            .on('click', '.js-edit-reportdaily', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initSupplierDrawer(id || '');
            })
            .on('click', '.js-detail-report', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                initDetailDrawer(id || '');
            });

    };
    var initDetailDrawer = function (id) {
        const title = id ? 'Chi tiết báo cáo công việc' : '';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--detailreport' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/reportdaily/details/${id}` : '',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var initForm = function ($form) {
        
        $form.validate({
            rules: {
                name: {
                    required: true,
                },
                description: {
                    required: true,
                },

            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên nhà cung cấp.'
                },

                description: {
                    required: 'Vui lòng nhập mô tả.'
                }
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {

                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                console.log(data, uuid);
                $.ajax({
                    url: uuid ? `/reportdaily/edit/${uuid}` : 'reportdaily/create',
                    method: uuid ? 'PUT' : 'POST',
                    data,
                })
                    .done(function () {
                        KTApp.block($form);
                        const successMsg = uuid ? 'Sửa thông tin thành công!' : 'Thêm báo cáo thành công!';
                        $.notify(successMsg, { type: 'success' });

                        /**
                         * Close drawer
                         */
                        drawer.close();

                        reloadTable();
                    })
                    .fail(function () {
                        KTApp.unblock($form);
                        const errorMsg = uuid ? 'Sửa thông tin không thành công!' : 'Thêm báo cáo không thành công!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-report__form');
        initForm($form);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));
        $('.kt-multiple-select-taskstate').selectpicker();
        $('.kt_datepicker_2').datepicker();

        /**
         * Init Dropzone
         */
        initDropzone();
    };
    var initDropzone = function () {
        
        new Dropzone('#kt_dropzone_image', {
            url: "/images/upload",
            paramName: "images",
            maxFiles: 4,
            maxFilesize: 1, // MB
            addRemoveLinks: true,
            dictDefaultMessage: 'Bấm hoặc kéo thả ảnh vào đây để tải ảnh lên',
            dictRemoveFile: 'Xóa ảnh',
            dictCancelUpload: 'Hủy',
            dictCancelUploadConfirmation: 'Bạn thực sự muốn hủy?',
            dictMaxFilesExceeded: 'Bạn không thể tải thêm ảnh!',
            dictFileTooBig: "Ảnh tải lên dung lượng quá lớn ({{filesize}}MB). Max filesize: {{maxFilesize}}MB.",
            accept: function (file, done) {
                done();
            },

            success: function (file, response) {
                if (response != null && response.length > 0) {
                    var uuid_images = $('#uuid_images').val();
                    for (var i = 0; i < response.length; i++) {
                        uuid_images = uuid_images + "," + response[i].id;
                        $('#uuid_images').val(uuid_images);
                    }

                    console.log(response, uuid_images, $('#uuid_images').val());

                }
                else {
                    $(file.previewElement).addClass("dz-error").find('.dz-error-message').text("Không tải được ảnh lên.");
                }

            },
            error: function (file, response, errorMessage) {
                console.log(file, response);
                if (file.accepted) {
                    $(file.previewElement).addClass("dz-error").find('.dz-error-message').text("Có lỗi xảy ra, không tải được ảnh lên.");
                }
                else {
                    $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(response);
                }
            }
        });
    }

    return {
        // Public functions
        init: function () {
            initTable();
            initSearch();
            initEventListeners();
            if ($("#expire_contract_str").val() !== '') {
                $('.kt_datepicker_expire_device').datepicker({ format: 'dd/mm/yyyy' }).datepicker('setDate', new Date());
            }
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-reportdaily').length) {
        DMMSReportdaily.init();


    }
});