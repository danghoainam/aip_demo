﻿"use strict";
// Class definition

var DMMSTeamDualList = function () {
    var drawerInstance = null;
    var team_name = '', team_desc = '';
    var renderDualList = function (left, right) {
        const options = [];
        $.each(left, (index, { uuid, name, phonenumber }) => {
            const text = [name || ''];
            if (phonenumber) {
                text.push(phonenumber);
            }

            options.push({
                text: text.join(' - ') || '-- No name --',
                value: uuid,
            });
        });

        $.each(right, (index, { uuid, name, phonenumber }) => {
            const text = [name || ''];
            if (phonenumber) {
                text.push(phonenumber);
            }
            options.push({
                text: text.join(' - ') || '-- No name --',
                value: uuid,
                selected: true,
            });
        });

        const optsHtml = [];
        $.each(options, (index, item) => {
            optsHtml.push(`<option value="${item.value}" ${item.selected ? 'selected' : ''}>${item.text}</option>`);
        });

        const $content = drawerInstance.setBody(`
            <select class="kt-dual-listbox-staff" multiple>${optsHtml.join('')}</select>
        `);


        new KTDualListBox($content.find('.kt-dual-listbox-staff')[0], {
            availableTitle: 'Danh sách nhân viên',
            selectedTitle: 'Nhân viên trong nhóm',
        });
    };

    var initDrawer = function ({ id }) {
        const title = 'Danh sách nhân viên trong nhóm';
        drawerInstance = new KTDrawer({
            title,
            id: `team-staff`,
            onRenderFooter: (callback) => {
                callback(`
                  <button data-module="staff" data-id="${id}" type="button" class="btn btn-primary js-save-team-staff">
                    <i class="la la-save"></i>
                    Lưu
                  </button>
                  `);
            },
        });

        drawerInstance.on('ready', () => {
            var left = null;
            var right = null;

            $.ajax({
                url: `/team/staff-not-in`,
                method: 'GET',
            })
                .done((res) => {
                    if ($.isArray(res)) {
                        left = res;
                    }

                    if (left && right) {
                        renderDualList(left, right);
                    }
                })
                .fail((error) => {
                    drawerInstance.error(error);
                });

            $.ajax({
                url: `/team/staff/${id}`,
                method: 'GET',
            })
                .done((res) => {
                    if ($.isArray(res)) {
                        right = res;
                    }

                    if (left && right) {
                        renderDualList(left, right);
                    }
                })
                .fail((error) => {
                    drawerInstance.error(error);
                });
        });

        drawerInstance.open();
    };

    var saveResult = function ({ id, module }) {
        drawerInstance.block();
        const $drawer = drawerInstance.getElement();
        const selected = $drawer.find('select').val();
        const successMsg = 'Lưu nhân viên thành công!';
        const errorMsg = 'Lưu nhân viên không thành công!';
        $.ajax({
            // url: `/group/${module}/${id}`,
            url: `/team/upsert/${id}`,
            method: 'POST',
            data: {
                "request": {
                    "uuid": id,
                    "uuids": selected.join(','),
                    "name": team_name,
                    "description": team_desc,
                    "module": module
                }
            }
        }).done((res) => {
            drawerInstance.unblock();
            drawerInstance.close();
            $.notify(successMsg, { type: 'success' });
        }).fail((res) => {
            drawerInstance.unblock();
            $.notify(errorMsg, { type: 'danger' });
        });
    };

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-list-staff')
            .on('click', '.js-list-staff', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                team_name = $this.attr('data-team-name');
                team_desc = $this.attr('data-team-desc');
                initDrawer({
                    id,
                    module: 'staff',
                });
            })
            .off('click', '.js-save-team-staff')
            .on('click', '.js-save-team-staff', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                saveResult({
                    id,
                    module: $this.attr('data-module')
                });
            })
    };

    return {
        // Public functions
        init: function () {
            initEventListeners();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-team').length) {
        DMMSTeamDualList.init();
    }
});
