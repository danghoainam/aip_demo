"use strict";
// Class definition

var DMMSSipAccount = function () {
    // Private functions

    var dataTableInstance = null;
    var drawer = null;
    var dataGlobal = null;
    var LIMIT = 1000;
    var CURRENT_PAGE = 1;
    // demo initializer
    var initTable = function () {
     

        dataTableInstance = $('.kt-datatable').KTDatatable({


            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/sipaccount/gets',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            //const { pagination } = raw;
                            //const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            //const page = pagination === undefined ? 0 : pagination.page;
                            //CURRENT_PAGE = page || 1;
                            //LIMIT = perpage;
                            return {
                                data: raw.items || [],
                                //meta: {
                                //    ...pagination || {},
                                //    perpage: perpage || LIMIT,
                                //    page: page || 1,
                                //},
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        //const pagination = data.pagination || {};
                        const { keyword } = query;
                        return {
                            //limit: pagination.perpage || LIMIT,
                            keyword: keyword || '',
                            page: (keyword == undefined || keyword == '')/* ? pagination.page : 1*/,

                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: false,
                serverFiltering: false,
            },

            search: {
                input: $('#js-filter-keyword'),
            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 20,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    }, 
                },
                {
                    field: 'account.name',
                    title: 'Người phụ trách',
                }, 
                {
                    field: 'exten',
                    width:100,
                    title: 'Số máy lẻ',
                    textAlign:'center'
                },
               
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 90,
                    overflow: 'visible',
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        return `
                             <button data-id="${data.owner_uuid}" type="button" title="Chỉnh sửa" class="js-edit-sip btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </button>                  
                            <button data-id="${data.owner_uuid}" type="button" title="Xóa" class="js-delete-sip btn btn-sm btn-danger btn-icon btn-icon-md">
                                <i class="la la-trash"></i>
                            </button>
                                `;
                        
                    },
                }],
            sortable: false,
            pagination: false,
            responsive: true,
        });
    };
    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };
    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-sip')
            .on('click', '.js-delete-sip', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Sip account sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteSip(id);
                    }
                });
            })
            .off('click', '.js-edit-sip')
            .on('click', '.js-edit-sip', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initSipDrawer(id || '');
            })
           

    };
    var deleteSip = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa Sip Account...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/sipaccount/delete/${id}`,
                method: 'DELETE',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa Sip Account thành công!', { type: 'success' });

                    reloadTable();
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa Sip Account không thành công!', { type: 'danger' });
                });
        }
    };
    var initSipDrawer = function (id) {
        const title = id ? 'Sửa thông tin Sip Account' : 'Thêm Sip Account';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--sipaccount' });
        debugger
        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/sipaccount/edit/${id}` : '/sipaccount/create',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);

                    jQuery.fn.preventDoubleSubmission = function () {
                        $(this).on('submit', function (e) {
                            var $form = $(this);

                            if ($form.data('submitted') === true) {
                                // Previously submitted - don't submit again
                                e.preventDefault();
                            } else {
                                // Mark it so that the next submit can be ignored
                                $form.data('submitted', true);
                            }
                        });

                        // Keep chainability
                        return this;
                    };
                    $('form.dmms-supplier__form').preventDoubleSubmission();
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
   
    var initContent = function ($content) {
        /*
            Init form
        */
        $('.kt_sip_team_add').selectpicker({
        });
        $('.kt_form_team_add').selectpicker({
        });
        if ($('select.kt_sip_team_add')[0] != undefined && $('select.kt_sip_team_add')[0].length == 1) {
            $('.filter-option-inner-inner').text('---Chưa có người phụ trách---')
        }
       

        const $form = $content.find('.dmms-sipaccount__form');
        initForm($form);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

       
    };
    var initForm = function ($form) {
        $form.validate({
            rules: {

                exten: {
                    number: true,
                    required: true,
                },
                password: {
                    required: true,
                },
            },
            messages: {
                exten: {
                    required: 'Vui lòng nhập số máy lẻ.'
                },
                password: {
                    required: 'Vui lòng mật khẩu.'
                }
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {
                debugger

                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                const { uuidep } = data;
                console.log(data, uuid);
                $.ajax({
                    url: uuid ? 'sipaccount/edit' :'sipaccount/create',
                    method:uuid ? 'PUT' : 'POST',
                    data,
                })
                    .done(function () {
                        KTApp.block($form);
                        const successMsg = uuid ? 'Sửa thông tin thành công!' : 'Thêm Sip Account thành công!';
                        $.notify(successMsg, { type: 'success' });

                        /**
                         * Close drawer
                         */
                        drawer.close();

                        reloadTable();
                    })
                    .fail(function (data) {
                        KTApp.unblock($form);
                        const errorMsg = uuid ? data.responseJSON.msg + '!' : data.responseJSON.msg + '!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }

  
    return {
        // Public functions
        init: function () {
            initTable();
            initEventListeners();
            $('.kt_form_team_add').selectpicker();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-sipaccount').length) {
        DMMSSipAccount.init();
    }
});
