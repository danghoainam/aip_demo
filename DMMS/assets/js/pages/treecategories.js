﻿"use strict";

var a = [];
var check = localStorage.getItem("mang");
if (!check) {
    localStorage.setItem("mang", JSON.stringify(a))
}
var listname = [];
var drawer = null;
var itempage = null;
var KTTreeview = function () {
    var getJsonTree = function () {

        $.ajax({
            async: true,
            type: "GET",
            url: "/TreeCategories/gets",
            type: "Get",
            dataType: "json",
            success: function (json) {
                $.each(json, function (index, item) {
                    if (item.id == 100) {
                        item.id = "thietbi"
                    }
                    else if (item.id == 101) {
                        item.id = "vattu"
                    }
                    else if (item.id == 102) {
                        item.id = "dungcu"
                    }
                    else if (item.id == 103) {
                        item.id ="phanmem"
                    }
                })
                demo4(json);


            },

            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }

    var demo4 = function (json) {
        debugger

        $.each(json, function (index, item) {
            item.children = 'xx';
        })




        console.log(json)
        $("#kt_tree_4").jstree({
            "core": {
                'check_callback': true,
                'data': json,
                "themes": {
                    "variant": "large"
                }

            },
            "plugins": ["contextmenu", "search"],
            "search": {
                "show_only_matches": true,
                "ajax": function (str, callback) {
                    $.ajax({
                        "url": "/TreeCategories/SubCategories",
                        "dataType": "json",
                        "contentType": "application/json;",
                        "data": {
                            'keyword': str,
                            'parent_id': 2,
                            'limit': -1
                        }
                    })
                        .done(function (data) {
                            debugger
                            console.log(data)
                            data = data.d;
                            callback(data);
                        });
                }
            },

            "contextmenu": {
                "items": function ($node) {
                    debugger
                    if ($node.original.type == 1 && $node.original.is_leaf == 0) {
                        return {
                            "Create1": {
                                "label": "Thêm thiết bị",

                                action: function (obj) {
                                    debugger
                                    console.log($node)
                                    var id = $node.original.id;
                                    var name = $node.original.text;
                                    var perfixname = $node.original.prefix_name;
                                    var type = $node.original.type;
                                    const title = 'THÊM DANH MỤC THIẾT BỊ MỚI';
                                    drawer = new KTDrawer({ title, id: 'new', className: 'dmms-drawer--device' });

                                    drawer.on('ready', () => {
                                        $.ajax({
                                            url: `/treecategories/createdevice/`,
                                            method: 'GET',
                                            dataType: "text",
                                            data: {
                                            },
                                        })
                                            .done(function (response) {
                                                $.ajax({
                                                    url: `/treecategories/getidsubtree/${id}`,
                                                    method: 'GET',
                                                    dataType: "text",
                                                    data: {
                                                    },
                                                })
                                                    .done(function (response) {
                                                        var myobj = JSON.parse(response);
                                                        if (myobj.length == 6) {
                                                            var html = `<label class="control-label">Mã thiết bị</label>
                        <div class="form-group row">
                            <div class="form-group col-6">
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Nhập tên danh mục thiết bị" name="detail[code]" id="codedevice" disabled/>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <select class="form-control" id="checkauto">
                                    <option value="0">Auto</option>
                                    <option value="1">Nhập text</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="form-group col-6">
                                <label class="control-label">Hãng thiết bị</label>
                                <div class="kt-input-icon">
                                    <input class="form-control autocomplete" placeholder="Nhập hãng thiết bị" name="detail[manufacturer]"/>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <label class="control-label">Xuất xứ</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Xuất xứ" name="detail[origin]" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="form-group col-6">
                                <label class="control-label">Giá nhập vào</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Giá nhập vào" name="detail[imp_price_ref_string]" />
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <label class="control-label">Giá xuất ra</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Giá xuất ra" name="detail[exp_price_ref_string]" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Đơn vị tính</label>
                            <div class="kt-input-icon">
                                <input class="form-control" placeholder="Đơn vị tính" name="detail[unit]" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Mô tả</label>
                            <div class="kt-input-icon">
                                <textarea class="form-control" placeholder="Mô tả" name="description"> </textarea>
                            </div>
                        </div>

                        `;
                                                            $('.addfield').append(html);
                                                            auto();
                                                            $("input[name*='detail[exp_price_ref_string]']").simpleMoneyFormat();
                                                            $("input[name*='detail[imp_price_ref_string]']").simpleMoneyFormat();
                                                            $('#checkauto').selectpicker();
                                                            $('#checkauto')
                                                                .on('change', function () {
                                                                    debugger
                                                                    if ($('#checkauto').val() == 1) {
                                                                        $('#codedevice').prop("disabled", false)
                                                                    }
                                                                    else {
                                                                        $('#codedevice').prop("disabled", true)
                                                                    }
                                                                })
                                                                .selectpicker();
                                                            $('.filedlast').prop("checked", true);
                                                            debugger
                                                            $('.removefiledlast').prop("disabled", true);

                                                        }

                                                        if (myobj !== null) {
                                                            $.each(myobj.reverse(), function (index, item) {
                                                                debugger
                                                                $('.kt-body-content').append(` <a class="active" data-id="${item.item1}" >
                                             ${item.item2} <i class="la la-angle-right"></i>
                                                    </a>`)
                                                                $('.kt-body-content a').unbind('click');
                                                                $('.kt-body-content a').bind("click", function () {
                                                                    if ($('.kt-body-content a').length != 1) {
                                                                        $('.kt-body-content').children().last().remove();
                                                                        $('.addfield').children().remove();
                                                                        $('.removefiledlast').prop("checked", true);
                                                                        $('.removefiledlast').prop("disabled", false);
                                                                        $('.addfield').append(`<input name="parent_id" value="" type="hidden" />`);
                                                                        $('.addfield').append(`<input name="type" value="${type}" type="hidden" />`);
                                                                    }
                                                                })

                                                            })

                                                        }

                                                    });

                                                const $content = drawer.setBody(response);
                                                $('.addfield').append(`<input name="parent_id" value="" type="hidden" />`);
                                                $('.addfield').append(`<input name="type" value="${type}" type="hidden" />`);
                                                $('.filedlast').click(function () {
                                                    debugger
                                                    var html = `<label class="control-label">Mã thiết bị</label>
                        <div class="form-group row">
                            <div class="form-group col-6">
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Nhập tên danh mục thiết bị" name="detail[code]" id="codedevice" disabled/>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <select class="form-control" id="checkauto">
                                    <option value="0">Auto</option>
                                    <option value="1">Nhập text</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="form-group col-6">
                                <label class="control-label">Hãng thiết bị</label>
                                <div class="kt-input-icon">
                                    <input class="form-control autocomplete" placeholder="Nhập hãng thiết bị" name="detail[manufacturer]"/>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <label class="control-label">Xuất xứ</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Xuất xứ" name="detail[origin]" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="form-group col-6">
                                <label class="control-label">Giá nhập vào</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Giá nhập vào" name="detail[imp_price_ref_string]" />
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <label class="control-label">Giá xuất ra</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Giá xuất ra" name="detail[exp_price_ref_string]" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Đơn vị tính</label>
                            <div class="kt-input-icon">
                                <input class="form-control" placeholder="Đơn vị tính" name="detail[unit]" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Mô tả</label>
                            <div class="kt-input-icon">
                                <textarea class="form-control" placeholder="Mô tả" name="description"> </textarea>
                            </div>
                        </div>

                        `;
                                                    if ($('.addfield').children().length == 0 || $('.addfield').children().length == 2) {
                                                        debugger
                                                        $('.addfield').append(html);
                                                        auto();
                                                        //autoComplete1();
                                                        $("input[name*='detail[exp_price_ref_string]']").simpleMoneyFormat();
                                                        $("input[name*='detail[imp_price_ref_string]']").simpleMoneyFormat();
                                                    }
                                                    $('#checkauto').selectpicker();
                                                    $('#checkauto')
                                                        .on('change', function () {
                                                            debugger
                                                            if ($('#checkauto').val() == 1) {
                                                                $('#codedevice').prop("disabled", false)
                                                            }
                                                            else {
                                                                $('#codedevice').prop("disabled", true)
                                                            }
                                                        })
                                                        .selectpicker();

                                                    $('.removefiledlast').click(function () {
                                                        $('.addfield').children().remove()
                                                    })

                                                })


                                                //$('.kt-body-content').children().last().append(` <i class="fa fa-times" style="font-size: 13px;position: absolute;top: 0px;">x</i>`)


                                                $('#js-getstafffdep').selectpicker();


                                                const $form = $content.find('.dmms-treecategoriesadd__form');
                                                initValidation($form, id);
                                            })
                                            .fail(function () {
                                                drawer.error();
                                            });
                                    });

                                    drawer.open();
                                }
                            },
                            "Rename": {

                                "separator_before": false,
                                "separator_after": true,
                                "label": "Cập nhật thiết bị",
                                action: function (obj) {
                                    debugger
                                    console.log($node)
                                    var uuid = $node.original.uuid;
                                    var id = $node.original.id;

                                    //$node = tree.create_node($node, { text: 'New File', type: 'file', icon: 'glyphicon glyphicon-file' });
                                    //tree.deselect_all();
                                    //tree.select_node($node);
                                    const title = 'Cập nhật thiết bị';
                                    drawer = new KTDrawer({ title, className: 'dmms-drawer--device' });

                                    drawer.on('ready', () => {
                                        $.ajax({
                                            url: `/treecategories/EditDevice/`,
                                            method: 'GET',
                                            data: {
                                                category_id: id,
                                                category_uuid: uuid,
                                            },
                                        })
                                            .done(function (response) {
                                                $.ajax({
                                                    url: `/treecategories/getidsubtree/${id}`,
                                                    method: 'GET',
                                                    dataType: "text",
                                                    data: {

                                                    },
                                                })
                                                    .done(function (response) {
                                                       
                                                        var myobj = JSON.parse(response);
                                                        if (myobj !== null) {
                                                            $.each(myobj.reverse(), function (index, item) {
                                                                debugger
                                                                $('.kt-body-content').append(` <a class="active" data-id="${item.item1}" >
                                             ${item.item2} <i class="la la-angle-right"></i>
                                                    </a>`)
                                                            })

                                                        }
                                                        auto();
                                                    });
                                                debugger
                                                const $content = drawer.setBody(response);
                                               
                                                $("input[name*='detail[exp_price_ref_string]']").simpleMoneyFormat();
                                                $("input[name*='detail[imp_price_ref_string]']").simpleMoneyFormat();

                                                const $form = $content.find('.dmms-treecategoriesedit__form');
                                                initValidationEdit($form);
                                            })
                                            .fail(function () {
                                                drawer.error();
                                            });
                                    });

                                    drawer.open();

                                }
                            },

                            "Remove": {
                                "separator_before": false,
                                "separator_after": false,
                                "label": "Xóa",
                                "action": function (obj) {
                                    debugger
                                    var category_uuid = $node.original.uuid;
                                    var category_id = $node.original.id;
                                    var parent_id = $node.parent;
                                    KTApp.swal({
                                        title: 'Xác nhận',
                                        text: 'Bạn có muốn xóa không.',
                                        icon: 'warning',
                                        dangerMode: true,
                                    }).then((confirm) => {
                                        if (confirm) {
                                            DeleteNode(category_uuid, category_id, parent_id);

                                        }
                                    });
                                }
                            },

                        };
                    }
                    else if ($node.original.type == 1 && $node.original.is_leaf == 1) {
                        return {
                            "Rename": {

                                "separator_before": false,
                                "separator_after": true,
                                "label": "Cập nhật thiết bị",
                                action: function (obj) {
                                    debugger
                                    console.log($node)
                                    var uuid = $node.original.uuid;
                                    var id = $node.original.id;

                                    //$node = tree.create_node($node, { text: 'New File', type: 'file', icon: 'glyphicon glyphicon-file' });
                                    //tree.deselect_all();
                                    //tree.select_node($node);
                                    const title = 'Cập nhật thiết bị';
                                    drawer = new KTDrawer({ title, className: 'dmms-drawer--device' });

                                    drawer.on('ready', () => {
                                        $.ajax({
                                            url: `/treecategories/EditDevice/`,
                                            method: 'GET',
                                            data: {
                                                category_id: id,
                                                category_uuid: uuid,
                                            },
                                        })
                                            .done(function (response) {
                                               
                                                $.ajax({
                                                    url: `/treecategories/getidsubtree/${id}`,
                                                    method: 'GET',
                                                    dataType: "text",
                                                    data: {

                                                    },
                                                })
                                                    .done(function (response) {
                                                       
                                                        var myobj = JSON.parse(response);
                                                        if (myobj !== null) {
                                                            $.each(myobj.reverse(), function (index, item) {
                                                                $("input[name*='detail.imp_price_ref_string']").on('keyup', function () {
                                                                    $("input[name*='detail.imp_price_ref_string']").unbind('keyup');
                                                                    $("input[name*='detail.imp_price_ref_string']").val(''); 
                                                                    $("input[name*='detail.imp_price_ref_string']").simpleMoneyFormat();
                                                                })
                                                                    

                                                                $("input[name*='detail.exp_price_ref_string']").on('keyup', function () {
                                                                    $("input[name*='detail.exp_price_ref_string']").unbind('keyup');
                                                                    $("input[name*='detail.exp_price_ref_string']").val('');
                                                                    $("input[name*='detail.exp_price_ref_string']").simpleMoneyFormat();
                                                                })
                                                                
                                                                debugger
                                                                $('.kt-body-content').append(` <a class="active" data-id="${item.item1}" >
                                             ${item.item2} <i class="la la-angle-right"></i>
                                                    </a>`)

                                                            })

                                                        }
                                                        $('#checkauto').selectpicker();
                                                        $('#checkauto')
                                                            .on('change', function () {
                                                                debugger
                                                                if ($('#checkauto').val() == 1) {
                                                                    $('#codedevice').prop("disabled", false)
                                                                }
                                                                else {
                                                                    $('#codedevice').prop("disabled", true)
                                                                }
                                                            })
                                                            .selectpicker();
                                                    });
                                                debugger
                                                const $content = drawer.setBody(response);
                                                auto();

                                                const $form = $content.find('.dmms-treecategoriesedit__form');
                                                initValidationEdit($form);
                                            })
                                            .fail(function () {
                                                drawer.error();
                                            });
                                    });

                                    drawer.open();

                                }
                            },

                            "Remove": {
                                "separator_before": false,
                                "separator_after": false,
                                "label": "Xóa",
                                "action": function (obj) {

                                    var category_uuid = $node.original.uuid;
                                    var category_id = $node.original.id;
                                    var parent_id = $node.parent;
                                    KTApp.swal({
                                        title: 'Xác nhận',
                                        text: 'Bạn có muốn xóa không.',
                                        icon: 'warning',
                                        dangerMode: true,
                                    }).then((confirm) => {
                                        if (confirm) {
                                            DeleteNode(category_uuid, category_id, parent_id);

                                        }
                                    });
                                }
                            },

                        };
                    }
                    //else if ($node.original.type == 1 && $node.original.is_leaf == 0 && $node.original.is_search == 1) {
                    //    return {
                    //        "Create1": {
                    //            "label": "Thêm thiết bị",

                    //            action: function (obj) {
                    //                debugger
                    //                console.log($node)
                    //                var id = $node.original.id;
                    //                var name = $node.original.text;
                    //                var perfixname = $node.original.prefix_name;
                    //                var type = $node.original.type;
                    //                const title = 'THÊM DANH MỤC THIẾT BỊ MỚI';
                    //                drawer = new KTDrawer({ title, id: 'new', className: 'dmms-drawer--device' });

                    //                drawer.on('ready', () => {
                    //                    $.ajax({
                    //                        url: `/treecategories/createdevice/`,
                    //                        method: 'GET',
                    //                        dataType: "text",
                    //                        data: {
                    //                        },
                    //                    })
                    //                        .done(function (response) {
                    //                            $.ajax({
                    //                                url: `/treecategories/getidsubtree/${id}`,
                    //                                method: 'GET',
                    //                                dataType: "text",
                    //                                data: {
                    //                                },
                    //                            })
                    //                                .done(function (response) {
                    //                                    var myobj = JSON.parse(response);
                    //                                    if (myobj.length == 3) {

                    //                                    }

                    //                                    if (myobj !== null) {
                    //                                        $.each(myobj.reverse(), function (index, item) {
                    //                                            debugger
                    //                                            $('.kt-body-content').append(` <a class="active" data-id="${item.item1}" >
                    //                         ${item.item2} <i class="la la-angle-right"></i>
                    //                                </a>`)
                    //                                            $('.kt-body-content a').unbind('click');
                    //                                            $('.kt-body-content a').bind("click", function () {
                    //                                                if ($('.kt-body-content a').length != 1) {
                    //                                                    $('.kt-body-content').children().last().remove();
                    //                                                }
                    //                                            })

                    //                                        })

                    //                                    }

                    //                                });

                    //                            const $content = drawer.setBody(response);
                    //                            $('.addfield').append(`<input name="parent_id" value="" type="hidden" />`);
                    //                            $('.addfield').append(`<input name="type" value="${type}" type="hidden" />`);
                    //                            $('.filedlast').click(function () {
                    //                                var html = `  <div class="form-group">
                    //        <label class="control-label">Mã thiết bị</label>
                    //        <div class="kt-input-icon">
                    //            <input class="form-control" placeholder="Nhập tên danh mục thiết bị" name="detail[code]" />
                    //        </div>
                    //    </div>
                    //    <div class="row">
                    //        <div class="form-group col-6">
                    //            <label class="control-label">Hãng thiết bị</label>
                    //            <div class="kt-input-icon">
                    //                <input class="form-control" placeholder="Nhập hãng thiết bị" name="detail[manufacturer]" />
                    //            </div>
                    //        </div>
                    //        <div class="form-group">
                    //            <label class="control-label">Model</label>
                    //            <div class="kt-input-icon">
                    //                <input class="form-control" placeholder="Nhập model" name="detail[model]" />
                    //            </div>
                    //        </div>
                    //    </div>
                    //    <div class="row">
                    //        <div class="form-group col-6">
                    //            <label class="control-label">Giá nhập vào</label>
                    //            <div class="kt-input-icon">
                    //                <input class="form-control" placeholder="Giá nhập vào" name="detail[imp_price_ref]" />
                    //            </div>
                    //        </div>
                    //        <div class="form-group">
                    //            <label class="control-label">Giá xuất ra</label>
                    //            <div class="kt-input-icon">
                    //                <input class="form-control" placeholder="Giá xuất ra" name="detail[exp_price_ref]" />
                    //            </div>
                    //        </div>
                    //    </div>
                    //    <div class="form-group">
                    //        <label class="control-label">Đơn vị tính</label>
                    //        <div class="kt-input-icon">
                    //            <input class="form-control" placeholder="Đơn vị tính" name="detail[unit]" />
                    //        </div>
                    //    </div>`;
                    //                                $('.addfield').append(html);
                    //                                $('.removefiledlast').click(function () {
                    //                                    $('.addfield').children().remove()
                    //                                })

                    //                            })


                    //                            //$('.kt-body-content').children().last().append(` <i class="fa fa-times" style="font-size: 13px;position: absolute;top: 0px;">x</i>`)


                    //                            $('#js-getstafffdep').selectpicker();


                    //                            const $form = $content.find('.dmms-treecategoriesadd__form');
                    //                            initValidationsearch($form, id);
                    //                        })
                    //                        .fail(function () {
                    //                            drawer.error();
                    //                        });
                    //                });

                    //                drawer.open();
                    //            }
                    //        },
                    //        "Rename": {

                    //            "separator_before": false,
                    //            "separator_after": true,
                    //            "label": "Cập nhật thiết bị",
                    //            action: function (obj) {
                    //                debugger
                    //                console.log($node)
                    //                var uuid = $node.original.uuid;
                    //                var id = $node.original.id;

                    //                //$node = tree.create_node($node, { text: 'New File', type: 'file', icon: 'glyphicon glyphicon-file' });
                    //                //tree.deselect_all();
                    //                //tree.select_node($node);
                    //                const title = 'Cập nhật thiết bị';
                    //                drawer = new KTDrawer({ title, className: 'dmms-drawer--device' });

                    //                drawer.on('ready', () => {
                    //                    $.ajax({
                    //                        url: `/treecategories/EditDevice/`,
                    //                        method: 'GET',
                    //                        data: {
                    //                            category_id: id,
                    //                            category_uuid: uuid,
                    //                        },
                    //                    })
                    //                        .done(function (response) {
                    //                            $.ajax({
                    //                                url: `/treecategories/getidsubtree/${id}`,
                    //                                method: 'GET',
                    //                                dataType: "text",
                    //                                data: {

                    //                                },
                    //                            })
                    //                                .done(function (response) {
                    //                                    var myobj = JSON.parse(response);
                    //                                    if (myobj !== null) {
                    //                                        $.each(myobj.reverse(), function (index, item) {
                    //                                            debugger
                    //                                            $('.kt-body-content').append(` <a class="active" data-id="${item.item1}" >
                    //                         ${item.item2} <i class="la la-angle-right"></i>
                    //                                </a>`)
                    //                                        })

                    //                                    }

                    //                                });
                    //                            debugger
                    //                            const $content = drawer.setBody(response);


                    //                            const $form = $content.find('.dmms-treecategoriesedit__form');
                    //                            initValidationEdit($form);
                    //                        })
                    //                        .fail(function () {
                    //                            drawer.error();
                    //                        });
                    //                });

                    //                drawer.open();

                    //            }
                    //        },

                    //        "Remove": {
                    //            "separator_before": false,
                    //            "separator_after": false,
                    //            "label": "Xóa",
                    //            "action": function (obj) {
                    //                debugger
                    //                var category_uuid = $node.original.uuid;
                    //                var category_id = $node.original.id;
                    //                var parent_id = $node.parent;
                    //                KTApp.swal({
                    //                    title: 'Xác nhận',
                    //                    text: 'Bạn có muốn xóa không.',
                    //                    icon: 'warning',
                    //                    dangerMode: true,
                    //                }).then((confirm) => {
                    //                    if (confirm) {
                    //                        DeleteNode(category_uuid, category_id, parent_id);

                    //                    }
                    //                });
                    //            }
                    //        },

                    //    };
                    //}
                    else if ($node.original.type == 2 && $node.original.is_leaf == 0) {
                        return {
                            "Create1": {
                                "label": "Thêm vật tư",

                                action: function (obj) {
                                    console.log($node)
                                    var id = $node.original.id;
                                    var type = $node.original.type;
                                    var children = $node.children;
                                    const title = 'DANH MỤC VẬT TƯ MỚI';
                                    drawer = new KTDrawer({ title, className: 'dmms-drawer--CreateSupplies' });

                                    drawer.on('ready', () => {
                                        $.ajax({
                                            url: `/TreeCategories/CreateSupplies/`,
                                            method: 'GET',
                                            dataType: "text",
                                            data: {


                                            },
                                        })
                                            .done(function (response) {

                                                $.ajax({
                                                    url: `/treecategories/getidsubtree/${id}`,
                                                    method: 'GET',
                                                    dataType: "text",
                                                    data: {

                                                    },
                                                })
                                                    .done(function (response) {
                                                        var myobj = JSON.parse(response);
                                                        if (myobj.length == 6) {
                                                            var html = `<label class="control-label">Mã vật tư</label>
                        <div class="form-group row">
                            <div class="form-group col-6">
                                <div class="kt-input-icon">
                                    <input class="form-control codedevice" placeholder="Nhập tên danh mục vật tư" name="detail[code]" disabled/>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <select class="form-control" id ="checkauto">
                                    <option value="0">Auto</option>
                                    <option value="1">Nhập text</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                        <div class="form-group col-6">
                            <label class="control-label">Hãng vật tư</label>
                            <div class="kt-input-icon">
                                <input class="form-control" placeholder="Nhập hãng vật tư" name="detail[manufacturer]" />
                            </div>
                        </div>
                        <div class="form-group col-6">
                            <label class="control-label">Xuất xứ</label>
                            <div class="kt-input-icon">
                                <input class="form-control" placeholder="Xuất xứ" name="detail[origin]" />
                            </div>
                        </div>
                        </div>
                        <div class="form-group row">
                            <div class="form-group col-6">
                                <label class="control-label">Giá nhập vào</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Giá nhập vào" name="detail[imp_price_ref_string]" />
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <label class="control-label">Giá xuất ra</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Giá xuất ra" name="detail[exp_price_ref_string]" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Đơn vị tính</label>
                            <div class="kt-input-icon">
                                <input class="form-control" placeholder="Đơn vị tính" name="detail[unit]" />
                            </div>
                        </div>
`;
                                                            $('.addfield').append(html);
                                                            $("input[name*='detail[exp_price_ref_string]']").simpleMoneyFormat();
                                                            $("input[name*='detail[imp_price_ref_string]']").simpleMoneyFormat();
                                                            $('.filedlast').prop("checked", true);
                                                            debugger
                                                            $('.removefiledlast').prop("disabled", true);
                                                            $('#checkauto').selectpicker();
                                                            $('#checkauto')
                                                                .on('change', function () {
                                                                    debugger
                                                                    if ($('#checkauto').val() == 1) {
                                                                        $('#codedevice').prop("disabled", false)
                                                                    }
                                                                    else {
                                                                        $('#codedevice').prop("disabled", true)
                                                                    }
                                                                })
                                                                .selectpicker();

                                                        }
                                                        if (myobj !== null) {
                                                            $.each(myobj.reverse(), function (index, item) {
                                                                debugger
                                                                $('.kt-body-content').append(` <a class="active" data-id="${item.item1}" >
                                             ${item.item2} <i class="la la-angle-right"></i>
                                                    </a>`)
                                                                $('.kt-body-content a').unbind('click');
                                                                $('.kt-body-content a').bind("click", function () {
                                                                    if ($('.kt-body-content a').length != 1) {
                                                                        $('.kt-body-content').children().last().remove();
                                                                        $('.addfield').children().remove();
                                                                        $('.removefiledlast').prop("checked", true);
                                                                        $('.removefiledlast').prop("disabled", false);
                                                                        $('.addfield').append(`<input name="parent_id" value="" type="hidden" />`);
                                                                        $('.addfield').append(`<input name="type" value="${type}" type="hidden" />`);
                                                                    }
                                                                })

                                                            })

                                                        }

                                                    });
                                                const $content = drawer.setBody(response);
                                                $('.filedlast').click(function () {
                                                    var html = `<label class="control-label">Mã vật tư</label>
                        <div class="form-group row">
                            <div class="form-group col-6">
                                <div class="kt-input-icon">
                                    <input class="form-control codedevice" placeholder="Nhập tên danh mục vật tư" name="detail[code]" disabled/>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <select class="form-control" id ="checkauto">
                                    <option value="0">Auto</option>
                                    <option value="1">Nhập text</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                        <div class="form-group col-6">
                            <label class="control-label">Hãng vật tư</label>
                            <div class="kt-input-icon">
                                <input class="form-control" placeholder="Nhập hãng vật tư" name="detail[manufacturer]" />
                            </div>
                        </div>
                        <div class="form-group col-6">
                            <label class="control-label">Xuất xứ</label>
                            <div class="kt-input-icon">
                                <input class="form-control" placeholder="Xuất xứ" name="detail[origin]" />
                            </div>
                        </div>
                        </div>
                        <div class="form-group row">
                            <div class="form-group col-6">
                                <label class="control-label">Giá nhập vào</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Giá nhập vào" name="detail[imp_price_ref_string]" />
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <label class="control-label">Giá xuất ra</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Giá xuất ra" name="detail[exp_price_ref_string]" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Đơn vị tính</label>
                            <div class="kt-input-icon">
                                <input class="form-control" placeholder="Đơn vị tính" name="detail[unit]" />
                            </div>
                        </div>
`;
                                                    if ($('.addfield').children().length == 0 || $('.addfield').children().length == 2 || $('.addfield').children().length == 3) {
                                                        debugger
                                                        $('.addfield').append(html);
                                                        $("input[name*='detail[exp_price_ref_string]']").simpleMoneyFormat();
                                                        $("input[name*='detail[imp_price_ref_string]']").simpleMoneyFormat();
                                                        $('#checkauto').selectpicker();
                                                        $('#checkauto')
                                                            .on('change', function () {
                                                                debugger
                                                                if ($('#checkauto').val() == 1) {
                                                                    $('.codedevice').prop("disabled", false)
                                                                }
                                                                else {
                                                                    $('.codedevice').prop("disabled", true)
                                                                }
                                                            })
                                                            .selectpicker();
                                                    }

                                                })
                                                $('.removefiledlast').click(function () {
                                                    $('.addfield').children().remove()
                                                })

                                                const $form = $content.find('#dmms-treecategoriesaddsupplies__form');
                                                $('.addfield').append(`<input name="parent_id" value="" type="hidden" />`);
                                                $('.addfield').append(`<input name="type" value="${type}" type="hidden" />`);
                                                $('.addfield').append(`<input name="chil[]" value="${children}" type="hidden" />`);
                                                initValidationSupplies($form);
                                            })
                                            .fail(function () {
                                                drawer.error();
                                            });
                                    });

                                    drawer.open();
                                }
                            },
                            "Rename": {
                                "separator_before": false,
                                "separator_after": false,
                                "label": "Cập nhật vật tư",
                                action: function (obj) {

                                    console.log($node)
                                    var uuid = $node.original.uuid;
                                    var id = $node.original.id;
                                    const title = 'CẬP NHẬT VẬT TƯ';
                                    drawer = new KTDrawer({ title, className: 'dmms-drawer--device' });

                                    drawer.on('ready', () => {
                                        $.ajax({
                                            url: `/treecategories/EditSupplies/`,
                                            method: 'GET',
                                            dataType: "text",
                                            data: {
                                                category_id: id,
                                                category_uuid: uuid,
                                            },
                                        })
                                            .done(function (response) {
                                                $.ajax({
                                                    url: `/treecategories/getidsubtree/${id}`,
                                                    method: 'GET',
                                                    dataType: "text",
                                                    data: {

                                                    },
                                                })
                                                    .done(function (response) {
                                                        debugger
                                                        var myobj = JSON.parse(response);
                                                        if (myobj !== null) {
                                                            $.each(myobj.reverse(), function (index, item) {
                                                                debugger
                                                                $('.kt-body-content').append(` <a class="active" data-id="${item.item1}" >
                                             ${item.item2} <i class="la la-angle-right"></i>
                                                    </a>`)
                                                            })
                                                        }
                                                    });
                                                $('#checkauto').selectpicker();
                                                $('#checkauto')
                                                    .on('change', function () {
                                                        debugger
                                                        if ($('#checkauto').val() == 1) {
                                                            $('#codedevice').prop("disabled", false)
                                                        }
                                                        else {
                                                            $('#codedevice').prop("disabled", true)
                                                        }
                                                    })
                                                    .selectpicker();
                                                const $content = drawer.setBody(response);

                                                const $form = $content.find('.dmms-treecategoriesaddsupplies__form');
                                                initValidationEditSupplies($form);
                                            })
                                            .fail(function () {
                                                drawer.error();
                                            });
                                    });

                                    drawer.open();
                                }
                            },

                            "Remove": {
                                "separator_before": false,
                                "separator_after": false,
                                "label": "Xóa",
                                "action": function (obj) {
                                    var category_uuid = $node.original.uuid;
                                    var category_id = $node.original.id;
                                    var parent_id = $node.parent;
                                    KTApp.swal({
                                        title: 'Xác nhận',
                                        text: 'Bạn có muốn xóa không.',
                                        icon: 'warning',
                                        dangerMode: true,
                                    }).then((confirm) => {
                                        if (confirm) {
                                            DeleteNode(category_uuid, category_id, parent_id);

                                        }
                                    });
                                }
                            },

                        };
                    }
                    else if ($node.original.type == 2 && $node.original.is_leaf == 1) {
                        return {
                            "Rename": {
                                "separator_before": false,
                                "separator_after": false,
                                "label": "Cập nhật vật tư",
                                action: function (obj) {

                                    console.log($node)
                                    var uuid = $node.original.uuid;
                                    var id = $node.original.id;
                                    const title = 'CẬP NHẬT VẬT TƯ';
                                    drawer = new KTDrawer({ title, className: 'dmms-drawer--device' });

                                    drawer.on('ready', () => {
                                        $.ajax({
                                            url: `/treecategories/EditSupplies/`,
                                            method: 'GET',
                                            dataType: "text",
                                            data: {
                                                category_id: id,
                                                category_uuid: uuid,
                                            },
                                        })
                                            .done(function (response) {
                                                $.ajax({
                                                    url: `/treecategories/getidsubtree/${id}`,
                                                    method: 'GET',
                                                    dataType: "text",
                                                    data: {

                                                    },
                                                })
                                                    .done(function (response) {
                                                        var myobj = JSON.parse(response);
                                                        if (myobj !== null) {
                                                            $("input[name*='detail.imp_price_ref_string']").on('keyup', function () {
                                                                $("input[name*='detail.imp_price_ref_string']").unbind('keyup');
                                                                $("input[name*='detail.imp_price_ref_string']").val('');
                                                                $("input[name*='detail.imp_price_ref_string']").simpleMoneyFormat();
                                                            })


                                                            $("input[name*='detail.exp_price_ref_string']").on('keyup', function () {
                                                                $("input[name*='detail.exp_price_ref_string']").unbind('keyup');
                                                                $("input[name*='detail.exp_price_ref_string']").val('');
                                                                $("input[name*='detail.exp_price_ref_string']").simpleMoneyFormat();
                                                            })
                                                            $.each(myobj.reverse(), function (index, item) {
                                                                debugger
                                                                $('.kt-body-content').append(` <a class="active" data-id="${item.item1}" >
                                             ${item.item2} <i class="la la-angle-right"></i>
                                                    </a>`)
                                                            })
                                                        }
                                                    });
                                                const $content = drawer.setBody(response);
                                                $('#checkauto').selectpicker();
                                                $('#checkauto')
                                                    .on('change', function () {
                                                        debugger
                                                        if ($('#checkauto').val() == 1) {
                                                            $('.codedevice').prop("disabled", false)
                                                        }
                                                        else {
                                                            $('.codedevice').prop("disabled", true)
                                                        }
                                                    })
                                                    .selectpicker();
                                                const $form = $content.find('.dmms-treecategoriesaddsupplies__form');
                                                initValidationEditSupplies($form);
                                            })
                                            .fail(function () {
                                                drawer.error();
                                            });
                                    });

                                    drawer.open();
                                }
                            },

                            "Remove": {
                                "separator_before": false,
                                "separator_after": false,
                                "label": "Xóa",
                                "action": function (obj) {
                                    var category_uuid = $node.original.uuid;
                                    var category_id = $node.original.id;
                                    var parent_id = $node.parent;
                                    KTApp.swal({
                                        title: 'Xác nhận',
                                        text: 'Bạn có muốn xóa không.',
                                        icon: 'warning',
                                        dangerMode: true,
                                    }).then((confirm) => {
                                        if (confirm) {
                                            DeleteNode(category_uuid, category_id, parent_id);

                                        }
                                    });
                                }
                            },

                        };
                    }
                    else if ($node.original.type == 3 && $node.original.is_leaf == 0) {
                        return {
                            "Create1": {
                                "label": "Thêm dụng cụ",

                                action: function (obj) {
                                    console.log($node)
                                    var id = $node.original.id;
                                    var type = $node.original.type;

                                    const title = 'DANH MỤC DỤNG CỤ LAO ĐỘNG MỚI';
                                    drawer = new KTDrawer({ title, className: 'dmms-drawer--CreateTools' });

                                    drawer.on('ready', () => {
                                        $.ajax({
                                            url: `/TreeCategories/CreateTools/`,
                                            method: 'GET',
                                            dataType: "text",
                                            data: {
                                            },
                                        })
                                            .done(function (response) {
                                                $.ajax({
                                                    url: `/treecategories/getidsubtree/${id}`,
                                                    method: 'GET',
                                                    dataType: "text",
                                                    data: {

                                                    },
                                                })
                                                    .done(function (response) {
                                                        var myobj = JSON.parse(response);
                                                        if (myobj.length == 6) {
                                                            var html = `<label class="control-label">Mã dụng cụ</label>
                        <div class="form-group row">
                            <div class="form-group col-6">
                                <div class="kt-input-icon">
                                    <input class="form-control codedevice" placeholder="Nhập tên danh mục dụng cụ" name="detail[code]" disabled/>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <select class="form-control" id ="checkauto">
                                    <option value="0">Auto</option>
                                    <option value="1">Nhập text</option>
                                </select>
                            </div>
                        </div>
                    <div class="form-group row">
                            <div class="form-group col-6">
                               <label class="control-label">Hãng dụng cụ lao động</label>
                                <div class="kt-input-icon">
                                 <input class="form-control" placeholder="Nhập hãng dụng cụ lao động" name="detail[manufacturer]" />
                                </div>
                            </div>
                            <div class="form-group col-6">
                                 <label class="control-label">Xuất xứ</label>
            <div class="kt-input-icon">
                <input class="form-control" placeholder="Xuất xứ" name="detail[origin]" />
            </div>
                            </div>
                        </div>
 <div class="form-group row">
                            <div class="form-group col-6">
                                <label class="control-label">Giá nhập vào</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Giá nhập vào" name="detail[imp_price_ref_string]" />
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <label class="control-label">Giá xuất ra</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Giá xuất ra" name="detail[exp_price_ref_string]" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Đơn vị tính</label>
                            <div class="kt-input-icon">
                                <input class="form-control" placeholder="Đơn vị tính" name="detail[unit]" />
                            </div>
                        </div>`
                                                            $('.addfield').append(html);
                                                            $("input[name*='detail[exp_price_ref_string]']").simpleMoneyFormat();
                                                            $("input[name*='detail[imp_price_ref_string]']").simpleMoneyFormat();
                                                            $('#checkauto').selectpicker();
                                                            $('#checkauto')
                                                                .on('change', function () {
                                                                    debugger
                                                                    if ($('#checkauto').val() == 1) {
                                                                        $('.codedevice').prop("disabled", false)
                                                                    }
                                                                    else {
                                                                        $('.codedevice').prop("disabled", true)
                                                                    }
                                                                })
                                                                .selectpicker();
                                                            $('.filedlast').prop("checked", true);
                                                            debugger
                                                            $('.removefiledlast').prop("disabled", true);
                                                        }
                                                        if (myobj !== null) {
                                                            $.each(myobj.reverse(), function (index, item) {
                                                                debugger
                                                                $('.kt-body-content').append(` <a class="active" data-id="${item.item1}" >
                                             ${item.item2} <i class="la la-angle-right"></i>
                                                    </a>`)
                                                                $('.kt-body-content a').unbind('click');
                                                                $('.kt-body-content a').bind("click", function () {
                                                                    debugger
                                                                    if ($('.kt-body-content a').length != 1) {
                                                                        $('.kt-body-content').children().last().remove();
                                                                        $('.addfield').children().remove();
                                                                        $('.removefiledlast').prop("checked", true);
                                                                        $('.removefiledlast').prop("disabled", false);
                                                                        $('.addfield').append(`<input name="parent_id" value="" type="hidden" />`);
                                                                        $('.addfield').append(`<input name="type" value="${type}" type="hidden" />`);
                                                                    }
                                                                })

                                                            })

                                                        }

                                                    });

                                                const $content = drawer.setBody(response);
                                                $('.filedlast').click(function () {
                                                    var html = `<label class="control-label">Mã dụng cụ</label>
                        <div class="form-group row">
                            <div class="form-group col-6">
                                <div class="kt-input-icon">
                                    <input class="form-control codedevice" placeholder="Nhập tên danh mục dụng cụ" name="detail[code]" disabled/>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <select class="form-control" id ="checkauto">
                                    <option value="0">Auto</option>
                                    <option value="1">Nhập text</option>
                                </select>
                            </div>
                        </div>
                    <div class="form-group row">
                            <div class="form-group col-6">
                               <label class="control-label">Hãng dụng cụ lao động</label>
                                <div class="kt-input-icon">
                                 <input class="form-control" placeholder="Nhập hãng dụng cụ lao động" name="detail[manufacturer]" />
                                </div>
                            </div>
                            <div class="form-group col-6">
                                 <label class="control-label">Xuất xứ</label>
            <div class="kt-input-icon">
                <input class="form-control" placeholder="Xuất xứ" name="detail[origin]" />
            </div>
                            </div>
                        </div>
 <div class="form-group row">
                            <div class="form-group col-6">
                                <label class="control-label">Giá nhập vào</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Giá nhập vào" name="detail[imp_price_ref_string]" />
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <label class="control-label">Giá xuất ra</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Giá xuất ra" name="detail[exp_price_ref_string]" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Đơn vị tính</label>
                            <div class="kt-input-icon">
                                <input class="form-control" placeholder="Đơn vị tính" name="detail[unit]" />
                            </div>
                        </div>`;
                                                    if ($('.addfield').children().length == 0 || $('.addfield').children().length == 2) {
                                                        $('.addfield').append(html);
                                                        $("input[name*='detail[imp_price_ref_string]']").simpleMoneyFormat();
                                                        $("input[name*='detail[exp_price_ref_string]']").simpleMoneyFormat();
                                                        $('#checkauto').selectpicker();
                                                        $('#checkauto')
                                                            .on('change', function () {
                                                                debugger
                                                                if ($('#checkauto').val() == 1) {
                                                                    $('.codedevice').prop("disabled", false)
                                                                }
                                                                else {
                                                                    $('.codedevice').prop("disabled", true)
                                                                }
                                                            })
                                                            .selectpicker();
                                                    }
                                                })
                                                $('.removefiledlast').click(function () {
                                                    $('.addfield').children().remove()
                                                })
                                                $('.addfield').append(`<input name="parent_id" value="" type="hidden" />`);
                                                $('.addfield').append(`<input name="type" value="${type}" type="hidden" />`);
                                                const $form = $content.find('.dmms-treecategoriesaddtools__form');
                                                initValidationTools($form);
                                            })
                                            .fail(function () {
                                                drawer.error();
                                            });
                                    });

                                    drawer.open();
                                }
                            },
                            "Rename": {
                                "separator_before": false,
                                "separator_after": false,
                                "label": "Cập nhật dụng cụ",
                                action: function (obj) {

                                    console.log($node)
                                    var uuid = $node.original.uuid;
                                    var id = $node.original.id;
                                    const title = 'CẬP NHẬT DỤNG CỤ LAO ĐỘNG';
                                    drawer = new KTDrawer({ title, className: 'dmms-drawer--CreateTools' });

                                    drawer.on('ready', () => {
                                        $.ajax({
                                            url: `/treecategories/EditTools/`,
                                            method: 'GET',
                                            dataType: "text",
                                            data: {
                                                category_id: id,
                                                category_uuid: uuid,
                                            },
                                        })
                                            .done(function (response) {
                                                const $content = drawer.setBody(response);
                                                $('#checkauto').selectpicker();
                                                $('#checkauto')
                                                    .on('change', function () {
                                                        debugger
                                                        if ($('#checkauto').val() == 1) {
                                                            $('.codedevice').prop("disabled", false)
                                                        }
                                                        else {
                                                            $('.codedevice').prop("disabled", true)
                                                        }
                                                    })
                                                    .selectpicker();
                                                const $form = $content.find('.dmms-treecategoriesedittools__form');
                                                initValidationEditTools($form);
                                            })
                                            .fail(function () {
                                                drawer.error();
                                            });
                                    });

                                    drawer.open();
                                }
                            },

                            "Remove": {
                                "separator_before": false,
                                "separator_after": false,
                                "label": "Xóa",
                                "action": function (obj) {
                                    var category_uuid = $node.original.uuid;
                                    var category_id = $node.original.id;
                                    var parent_id = $node.parent;
                                    KTApp.swal({
                                        title: 'Xác nhận',
                                        text: 'Bạn có muốn xóa không.',
                                        icon: 'warning',
                                        dangerMode: true,
                                    }).then((confirm) => {
                                        if (confirm) {
                                            DeleteNode(category_uuid, category_id, parent_id);

                                        }
                                    });
                                }
                            },

                        };
                    }
                    if ($node.original.type == 3 && $node.original.is_leaf == 1) {
                        return {
                            "Rename": {
                                "separator_before": false,
                                "separator_after": false,
                                "label": "Cập nhật dụng cụ",
                                action: function (obj) {

                                    console.log($node)
                                    var uuid = $node.original.uuid;
                                    var id = $node.original.id;
                                    const title = 'CẬP NHẬT DỤNG CỤ LAO ĐỘNG';
                                    drawer = new KTDrawer({ title, className: 'dmms-drawer--CreateTools' });

                                    drawer.on('ready', () => {
                                        $.ajax({
                                            url: `/treecategories/EditTools/`,
                                            method: 'GET',
                                            dataType: "text",
                                            data: {
                                                category_id: id,
                                                category_uuid: uuid,
                                            },
                                        })
                                            .done(function (response) {
                                                const $content = drawer.setBody(response)
                                                     $("input[name*='detail.imp_price_ref_string']").on('keyup', function () {
                                                         $("input[name*='detail.imp_price_ref_string']").unbind('keyup');
                                                         $("input[name*='detail.imp_price_ref_string']").val('');
                                                         $("input[name*='detail.imp_price_ref_string']").simpleMoneyFormat();
                                                     })
                                                                    

                                                                $("input[name*='detail.exp_price_ref_string']").on('keyup', function () {
                                                         $("input[name*='detail.exp_price_ref_string']").unbind('keyup');
                                                         $("input[name*='detail.exp_price_ref_string']").val('');
                                                         $("input[name*='detail.exp_price_ref_string']").simpleMoneyFormat();
                                                     })
                                                $('#checkauto').selectpicker();
                                                $('#checkauto')
                                                    .on('change', function () {
                                                        debugger
                                                        if ($('#checkauto').val() == 1) {
                                                            $('.codedevice').prop("disabled", false)
                                                        }
                                                        else {
                                                            $('.codedevice').prop("disabled", true)
                                                        }
                                                    })
                                                    .selectpicker();
                                                const $form = $content.find('.dmms-treecategoriesedittools__form');
                                                initValidationEditTools($form);
                                            })
                                            .fail(function () {
                                                drawer.error();
                                            });
                                    });

                                    drawer.open();
                                }
                            },

                            "Remove": {
                                "separator_before": false,
                                "separator_after": false,
                                "label": "Xóa",
                                "action": function (obj) {
                                    var category_uuid = $node.original.uuid;
                                    var category_id = $node.original.id;
                                    var parent_id = $node.parent;
                                    KTApp.swal({
                                        title: 'Xác nhận',
                                        text: 'Bạn có muốn xóa không.',
                                        icon: 'warning',
                                        dangerMode: true,
                                    }).then((confirm) => {
                                        if (confirm) {
                                            DeleteNode(category_uuid, category_id, parent_id);

                                        }
                                    });
                                }
                            },

                        };
                    }
                    var datatree = $node.original;
                    console.log(datatree)
                    var tree = $("#kt_tree_4").jstree(true);

                }
            }
        }).on('hover_node.jstree', function (e, data) {

            var $node = $("#" + data.node.id);
            $node.prop({ 'title': 'Chuột phải thêm danh mục' });

        }).bind("refresh.jstree", function (e, data) {
            debugger
            $(this).jstree("open_all");
        })

        $('#kt_tree_4').on("open_node.jstree close_node.jstree", function (e, data) {
            itempage = null;
            var limits = -1;
            debugger
            if (data.node.original.is_leaf == 1) {
                limits = 5
            }
            if (data.node.original.type != null && data.node.id != "thietbi" && data.node.id != "vattu" && data.node.id != "dungcu") {
                debugger
                $('#kt_tree_4').jstree(true).delete_node(data.node.children);
                $.ajax({
                    url: '/TreeCategories/SubCategories',
                    method: 'POST',
                    data: {
                        category_type: data.node.original.type,
                        limit: limits,
                        level: 0,
                        parent_id: data.node.id,
                    },
                })
                    .done(function (data) {
                        $.each(data, function (index, item) {

                            debugger
                            if (item.is_leaf == 0) {
                                item.children = 'xx';
                            }
                            if (index == 0 && item.is_leaf == 1 && item.pages != 1) {
                                item.text = item.text + `&nbsp&nbsp&nbsp&nbsp&nbsp` + `<a class="paginationtree" style="color: #189EFF;text-decoration: none;background-color:transparent">Read more</a>`
                            }
                            $('#kt_tree_4').jstree(true).create_node(data[0].parent_id, item, 'last');
                        })
                    })
                    .fail(function (data) {

                        const errorMsg = 'Có lỗi xảy ra';
                        $.notify(errorMsg, { type: 'danger' });
                    });
            }
            else {
                $('#kt_tree_4').jstree(true).delete_node(data.node.children);
                $.ajax({
                    url: '/TreeCategories/SubCategories',
                    method: 'POST',
                    data: {
                        category_type: data.node.original.type,
                        limit: -1,
                        level: 0,
                        parent_id: null,
                    },
                })
                    .done(function (data) {
                        if (data != null) {
                            $.each(data, function (index, item) {

                                debugger
                                if (item.is_leaf == 0) {
                                    item.children = 'xx';
                                }
                                if (index == 0 && item.is_leaf == 1 && item.pages != 1) {
                                    item.text = item.text + `&nbsp&nbsp&nbsp&nbsp&nbsp` + `<a class="paginationtree" style="color: #189EFF;text-decoration: none;background-color:transparent">Read more</a>`
                                }
                                if (data[0].type == 1) {
                                    $('#kt_tree_4').jstree(true).create_node(thietbi, item, 'last')
                                }
                                else if (data[0].type == 2) {
                                    $('#kt_tree_4').jstree(true).create_node(vattu, item, 'last')
                                }
                                else if (data[0].type == 3) {
                                    $('#kt_tree_4').jstree(true).create_node(dungcu, item, 'last')
                                }
                                ;
                            })
                        }

                    })
                    .fail(function (data) {

                        const errorMsg = 'Có lỗi xảy ra';
                        $.notify(errorMsg, { type: 'danger' });
                    });
            }

        });


    }

    $('#kt_tree_4').on("select_node.jstree", function (e, data) {
        debugger
        var page = data.node.original.page;
        if (itempage != null) {
            page = itempage + 1;
        }
        else {
            if (data.node.original.page == 0) {
                page = page + 2;
            }
        }

        $('body').unbind('click');
        $('body').on("click", '.paginationtree', function () {
            KTApp.blockPage();
            $.ajax({
                url: '/TreeCategories/SubCategories',
                method: 'POST',
                data: {
                    category_type: data.node.original.type,
                    limit: 5,
                    level: 0,
                    parent_id: data.node.original.parent_id,
                    page: page,
                },
            })
                .done(function (data) {
                    KTApp.unblockPage();
                    if (data != null) {
                        $('body .paginationtree').remove();
                    }
                    $.each(data, function (index, item) {

                        debugger
                        if (item.is_leaf == 0) {
                            item.children = 'xx';
                        }
                        if (item.page != 0 && item.page != 1) {
                            itempage = item.page;
                        }
                        $('#kt_tree_4').jstree(true).create_node(data[0].parent_id, item, 'last');
                    })
                })
                .fail(function (data) {
                    KTApp.unblockPage();
                    const errorMsg = 'Có lỗi xảy ra';
                    $.notify(errorMsg, { type: 'danger' });
                });
        })


    });

    var initEventListeners = function () {
        $(document)
            .on('change', '.check-all', function () {
                debugger
                const $this = $(this);

                var isCheckAll = $this.find('input[type="checkbox"]').prop('checked');
                var checkboxInBody = $(".kt-form-right--body").find('input[type="checkbox"]');

                if (isCheckAll === true) {
                    checkboxInBody.prop('checked', true);
                }
                else {
                    checkboxInBody.prop('checked', false);
                }

            })
            .on('click', '#kt-close', function () {
                drawer.close()
            })

    };

    var initValidation = function () {
        debugger
        $(".dmms-treecategoriesadd__form").validate({
            rules: {

                name: {
                    required: true,
                    existName:true,
                },

            },
            'onkeyup': false,
            messages: {
                name: {
                    required: 'Vui lòng nhập tên thiết bị.',
                    existName:'Tên này đã tồn tại.',
                },

            },
           

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('.dmms-treecategoriesadd__form', -200);
                //$(".submit").attr("disabled", true);
            },
            submitHandler: function (form) {
                debugger
                const data = $(form).serializeJSON() || {};
                var test = $('.kt-body-content').children().last().attr('data-id');
                data.parent_id = $('.kt-body-content').children().last().attr('data-id');
                $.ajax({
                    url: '/TreeCategories/CreateDevice/',
                    method: 'POST',
                    data,
                })
                    .done(function (data) {
                        debugger
                        var b = JSON.parse(localStorage.getItem("mang"))
                        if (data.data.detail != null) {
                            if (data.data.detail.manufacturer != null) {
                                debugger
                                var checkword = b.filter(word => word == data.data.detail.manufacturer)

                                if (checkword.length == 0) {
                                        b.push(data.data.detail.manufacturer);
                                    }
                               
                               
                            }
                        }
                        
                        localStorage.setItem("mang", JSON.stringify(b))
                        
                        debugger
                        const successMsg = 'Thêm mới thành công';
                        $.notify(successMsg, { type: 'success' });
                        $('.kt-form-right--body').scrollTop(0);
                        $('.kt-form-left').scrollTop(0);
                        $('.dmms-drawer__content').scrollTop(0);
                        $('#example-form').trigger("reset");
                        $('.kt-body-content').append(` <a class="active" data-id="${data.data.id}" >
                                              ${data.data.name}<i class="la la-angle-right"></i>
                                                    </a>`);
                        if ($('.kt-body-content').children().length == 7 && $('.addfield').children().length < 7) {
                            var html = `
                       <label class="control-label">Mã thiết bị</label>
                        <div class="form-group row">
                            <div class="form-group col-6">
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Nhập tên danh mục thiết bị" name="detail[code]" id="codedevice" disabled/>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <select class="form-control" id="checkauto">
                                    <option value="0">Auto</option>
                                    <option value="1">Nhập text</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="form-group col-6">
                                <label class="control-label">Hãng thiết bị</label>
                                <div class="kt-input-icon">
                                    <input class="form-control autocomplete" placeholder="Nhập hãng thiết bị" name="detail[manufacturer]" id="autocomplete"/>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <label class="control-label">Xuất xứ</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Xuất xứ" name="detail[origin]" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="form-group col-6">
                                <label class="control-label">Giá nhập vào</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Giá nhập vào" name="detail[imp_price_ref_string]" />
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <label class="control-label">Giá xuất ra</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Giá xuất ra" name="detail[exp_price_ref_string]" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Đơn vị tính</label>
                            <div class="kt-input-icon">
                                <input class="form-control" placeholder="Đơn vị tính" name="detail[unit]" />
                            </div>
                        </div>
                    
                            `;
                            $('.addfield').append(html);
                            auto();
                            $("input[name*='detail[exp_price_ref_string]']").simpleMoneyFormat();
                            $("input[name*='detail[imp_price_ref_string]']").simpleMoneyFormat();
                            $('.filedlast').prop("checked", true);
                            $('.removefiledlast').prop("disabled", true);
                            $('#checkauto').selectpicker();
                            $('#checkauto')
                                .on('change', function () {
                                    debugger
                                    if ($('#checkauto').val() == 1) {
                                        $('#codedevice').prop("disabled", false)
                                    }
                                    else {
                                        $('#codedevice').prop("disabled", true)
                                    }
                                })
                                .selectpicker();
                            $('.addfield').append(`<input name="parent_id" value="" type="hidden" />`);
                            $('.addfield').append(`<input name="type" value="${data.data.type}" type="hidden" />`);

                        }
                        if (data.data.is_leaf == 1) {
                            $('.filedlast').prop("checked", true);
                            $('.removefiledlast').prop("disabled", true);
                        }
                        $('.kt-body-content a').unbind('click');
                        $('.kt-body-content a').bind("click", function () {
                            debugger
                            if ($('.kt-body-content a').length != 1) {
                                $('.kt-body-content').children().last().remove();
                                $('.addfield').children().remove();
                                $('.removefiledlast').prop("checked", true);
                                $('.removefiledlast').prop("disabled", false);
                                $('.addfield').append(`<input name="parent_id" value="" type="hidden" />`);
                                $('.addfield').append(`<input name="type" value="${data.data.type}" type="hidden" />`);
                            }
                        })


                        if (test === undefined) {
                            $("#kt_tree_4").jstree().close_node(thietbi);
                            $("#kt_tree_4").jstree().open_node(thietbi);
                        }
                        $("#kt_tree_4").jstree().close_node(test);
                        $("#kt_tree_4").jstree().open_node(test);
                        
                    })
                    .fail(function (data) {

                        const errorMsg = "Có lỗi xảy ra";
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
                //$("#kt_tree_4").jstree().open_node(data.data.id);
            },
            
        });
    };
    var initValidationsearch = function () {
        debugger
        $(".dmms-treecategoriesadd__form").validate({
            rules: {

                name: {
                    required: true,
                },

            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên thiết bị.'
                },

            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('.dmms-treecategoriesadd__form', -200);
                //$(".submit").attr("disabled", true);
            },
            submitHandler: function (form) {
                debugger
                const data = $(form).serializeJSON() || {};
                var test = $('.kt-body-content').children().last().attr('data-id');
                data.parent_id = $('.kt-body-content').children().last().attr('data-id');
                $.ajax({
                    url: '/TreeCategories/CreateDevice/',
                    method: 'POST',
                    data,
                })
                    .done(function (data) {

                        debugger
                        const successMsg = 'Thêm mới thành công';
                        $.notify(successMsg, { type: 'success' });
                        $('.kt-body-content').append(` <a class="active" data-id="${data.data.id}" >
                                              ${data.data.name}<i class="la la-angle-right"></i>
                                                    </a>`);
                        $('.kt-body-content a').unbind('click');
                        $('.kt-body-content a').bind("click", function () {
                            debugger
                            if ($('.kt-body-content a').length != 1) {
                                $('.kt-body-content').children().last().remove();
                            }
                        })
                        searchtreecus();

                    })
                    .fail(function (data) {

                        const errorMsg = "Có lỗi xảy ra";
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    };
    var initValidationSupplies = function () {
        $(".dmms-treecategoriesaddsupplies__form").validate({
            rules: {

                name: {
                    required: true,
                    existName: true,
                },
               
            },
            'onkeyup': false,
            messages: {
                name: {
                    required: 'Vui lòng nhập tên thiết bị.',
                    existName: 'Tên này đã tồn tại.'
                },
            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('.dmms-treecategoriesaddsupplies__form', -200);
                //$(".submit").attr("disabled", true);
            },
            submitHandler: function (form) {
                debugger
                const data = $(form).serializeJSON() || {};
                data.parent_id = $('.kt-body-content').children().last().attr('data-id');
                var test = $('.kt-body-content').children().last().attr('data-id');
                var delete_child = data.chil;
                const { uuid } = data;
                console.log(data, uuid);
                $.ajax({
                    url: '/TreeCategories/CreateSupplies',
                    method: 'POST',
                    data,
                })
                    .done(function (data) {

                        debugger
                        const successMsg = 'Thêm mới thành công';
                        $.notify(successMsg, { type: 'success' });
                        $('.kt-form-right--body').scrollTop(0);
                        $('.kt-form-left').scrollTop(0);
                        $('.dmms-drawer__content').scrollTop(0);
                        $('#example-form').trigger("reset");
                        $('.kt-body-content').append(` <a class="active" data-id="${data.data.id}" >
                                              ${data.data.name}<i class="la la-angle-right"></i>
                                                    </a>`);
                        if ($('.kt-body-content').children().length == 7) {
                            var html = `<label class="control-label">Mã vật tư</label>
                        <div class="form-group row">
                            <div class="form-group col-6">
                                <div class="kt-input-icon">
                                    <input class="form-control codedevice" placeholder="Nhập tên danh mục vật tư" name="detail[code]" disabled/>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <select class="form-control" id ="checkauto">
                                    <option value="0">Auto</option>
                                    <option value="1">Nhập text</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                        <div class="form-group col-6">
                            <label class="control-label">Hãng vật tư</label>
                            <div class="kt-input-icon">
                                <input class="form-control" placeholder="Nhập hãng vật tư" name="detail[manufacturer]" />
                            </div>
                        </div>
                        <div class="form-group col-6">
                            <label class="control-label">Xuất xứ</label>
                            <div class="kt-input-icon">
                                <input class="form-control" placeholder="Xuất xứ" name="detail[origin]" />
                            </div>
                        </div>
                        </div>
                        <div class="form-group row">
                            <div class="form-group col-6">
                                <label class="control-label">Giá nhập vào</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Giá nhập vào" name="detail[imp_price_ref_string]" />
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <label class="control-label">Giá xuất ra</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Giá xuất ra" name="detail[exp_price_ref_string]" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Đơn vị tính</label>
                            <div class="kt-input-icon">
                                <input class="form-control" placeholder="Đơn vị tính" name="detail[unit]" />
                            </div>
                        </div>`;
                            $('.addfield').append(html);
                            $("input[name*='detail[exp_price_ref_string]']").simpleMoneyFormat();
                            $("input[name*='detail[imp_price_ref_string]']").simpleMoneyFormat();
                            $('.filedlast').prop("checked", true);
                            debugger
                            $('.removefiledlast').prop("disabled", true);
                            $('#checkauto').selectpicker();
                            $('#checkauto')
                                .on('change', function () {
                                    debugger
                                    if ($('#checkauto').val() == 1) {
                                        $('#codedevice').prop("disabled", false)
                                    }
                                    else {
                                        $('#codedevice').prop("disabled", true)
                                    }
                                })
                                .selectpicker();
                            $('.addfield').append(`<input name="parent_id" value="" type="hidden" />`);
                            $('.addfield').append(`<input name="type" value="${data.data.type}" type="hidden" />`);

                        }
                        if (data.data.is_leaf == 1) {
                            $('.filedlast').prop("checked", true);
                            $('.removefiledlast').prop("disabled", true);
                        }
                        $('.kt-body-content a').unbind('click');
                        $('.kt-body-content a').bind("click", function () {
                            debugger
                            if ($('.kt-body-content a').length != 1) {
                                $('.kt-body-content').children().last().remove();
                                $('.addfield').children().remove();
                                $('.removefiledlast').prop("checked", true);
                                $('.removefiledlast').prop("disabled", false);
                                $('.addfield').append(`<input name="parent_id" value="" type="hidden" />`);
                                $('.addfield').append(`<input name="type" value="${data.data.type}" type="hidden" />`);
                            }
                        })


                        if (test === undefined) {
                            $("#kt_tree_4").jstree().close_node(vattu);
                            $("#kt_tree_4").jstree().open_node(vattu);
                        }
                        $("#kt_tree_4").jstree().close_node(test);
                        $("#kt_tree_4").jstree().open_node(test);




                        //window.location = window.location.origin + "/tree";
                    })
                    .fail(function (data) {

                        const errorMsg = "Có lỗi xảy ra";
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    };
    var initValidationTools = function () {
        $(".dmms-treecategoriesaddtools__form").validate({
            rules: {

                name: {
                    required: true,
                    existName: true,
                },
                
            },
            'onkeyup': false,
            messages: {
                name: {
                    required: 'Vui lòng nhập tên thiết bị.',
                    existName: 'Tên này đã tồn tại.'
                },
            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('.dmms-treecategoriesaddtools__form', -200);
                //$(".submit").attr("disabled", true);
            },
            submitHandler: function (form) {
                debugger
                const data = $(form).serializeJSON() || {};
                data.parent_id = $('.kt-body-content').children().last().attr('data-id');
                var test = $('.kt-body-content').children().last().attr('data-id');
                const { uuid } = data;
                console.log(data, uuid);
                $.ajax({
                    url: '/TreeCategories/CreateTools',
                    method: 'POST',
                    data,
                })
                    .done(function (data) {

                        debugger
                        const successMsg = 'Thêm mới thành công';
                        $.notify(successMsg, { type: 'success' });
                        $('.kt-body-content').append(`<a class="active" data-id="${data.data.id}" >
                                              ${data.data.name}<i class="la la-angle-right"></i>
                                                    </a>`);
                        $('.kt-form-right--body').scrollTop(0);
                        $('.kt-form-left').scrollTop(0);
                        $('.dmms-drawer__content').scrollTop(0);
                        $('#example-form').trigger("reset");
                        if ($('.kt-body-content').children().length == 7) {
                            var html = `<label class="control-label">Mã dụng cụ</label>
                        <div class="form-group row">
                            <div class="form-group col-6">
                                <div class="kt-input-icon">
                                    <input class="form-control codedevice" placeholder="Nhập tên danh mục dụng cụ" name="detail[code]" disabled/>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <select class="form-control" id ="checkauto">
                                    <option value="0">Auto</option>
                                    <option value="1">Nhập text</option>
                                </select>
                            </div>
                        </div>
                    <div class="form-group row">
                            <div class="form-group col-6">
                               <label class="control-label">Hãng dụng cụ lao động</label>
                                <div class="kt-input-icon">
                                 <input class="form-control" placeholder="Nhập hãng dụng cụ lao động" name="detail[manufacturer]" />
                                </div>
                            </div>
                            <div class="form-group col-6">
                                 <label class="control-label">Xuất xứ</label>
            <div class="kt-input-icon">
                <input class="form-control" placeholder="Xuất xứ" name="detail[origin]" />
            </div>
                            </div>
                        </div>
 <div class="form-group row">
                            <div class="form-group col-6">
                                <label class="control-label">Giá nhập vào</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Giá nhập vào" name="detail[imp_price_ref_string]" />
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <label class="control-label">Giá xuất ra</label>
                                <div class="kt-input-icon">
                                    <input class="form-control" placeholder="Giá xuất ra" name="detail[exp_price_ref_string]" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Đơn vị tính</label>
                            <div class="kt-input-icon">
                                <input class="form-control" placeholder="Đơn vị tính" name="detail[unit]" />
                            </div>
                        </div>`
                            $('.addfield').append(html);
                            $("input[name*='detail[exp_price_ref_string]']").simpleMoneyFormat();
                            $("input[name*='detail[imp_price_ref_string]']").simpleMoneyFormat();
                            $('#checkauto').selectpicker();
                            $('#checkauto')
                                .on('change', function () {
                                    debugger
                                    if ($('#checkauto').val() == 1) {
                                        $('.codedevice').prop("disabled", false)
                                    }
                                    else {
                                        $('.codedevice').prop("disabled", true)
                                    }
                                })
                                .selectpicker();
                            $('.addfield').append(`<input name="parent_id" value="" type="hidden" />`);
                            $('.addfield').append(`<input name="type" value="${data.data.type}" type="hidden" />`);
                            $('.filedlast').prop("checked", true);
                            debugger
                            $('.removefiledlast').prop("disabled", true);
                        }
                        if (data.data.is_leaf == 1) {
                            $('.filedlast').prop("checked", true);
                            $('.removefiledlast').prop("disabled", true);
                        }
                        $('.kt-body-content a').unbind('click');
                        $('.kt-body-content a').bind("click", function () {
                            debugger
                            if ($('.kt-body-content a').length != 1) {
                                $('.kt-body-content').children().last().remove();
                                $('.addfield').children().remove();
                                $('.removefiledlast').prop("checked", true);
                                $('.removefiledlast').prop("disabled", false);
                                $('.addfield').append(`<input name="parent_id" value="" type="hidden" />`);
                                $('.addfield').append(`<input name="type" value="${data.data.type}" type="hidden" />`);
                            }
                        })

                        if (test === undefined) {
                            $("#kt_tree_4").jstree().close_node(dungcu);
                            $("#kt_tree_4").jstree().open_node(dungcu);
                        }
                        $("#kt_tree_4").jstree().close_node(test);
                        $("#kt_tree_4").jstree().open_node(test);
                    })
                    .fail(function (data) {

                        const errorMsg = "Có lỗi xảy ra";
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    };
    var initValidationEdit = function () {
        $(".dmms-treecategoriesedit__form").validate({
            rules: {

                name: {
                    required: true,
                },

            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên thiết bị.'
                },

            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('.dmms-treecategoriesedit__form', -200);
                //$(".submit").attr("disabled", true);
            },
            submitHandler: function (form) {
                debugger
                var test = $('.kt-body-content').children().last().prev().attr('data-id');
                const data = $(form).serializeJSON() || {};
                $.ajax({
                    url: '/TreeCategories/EditDevice',
                    method: 'PUT',
                    data,
                })
                    .done(function (data) {

                        debugger
                        const successMsg = 'Cập nhật thành công';
                        $.notify(successMsg, { type: 'success' });
                        if (test === undefined) {
                            $("#kt_tree_4").jstree().close_node(thietbi);
                            $("#kt_tree_4").jstree().open_node(thietbi);
                        }
                        $("#kt_tree_4").jstree().close_node(test);
                        $("#kt_tree_4").jstree().open_node(test);
                    })
                    .fail(function (data) {

                        const errorMsg = "Có lỗi xảy ra";
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    };
    var initValidationEditSupplies = function () {
        $(".dmms-treecategorieseditsupplies__form").validate({
            rules: {

                name: {
                    required: true,
                },

            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên thiết bị.'
                },

            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('.dmms-treecategorieseditsupplies__form', -200);
                //$(".submit").attr("disabled", true);
            },
            submitHandler: function (form) {
                debugger
                var test = $('.kt-body-content').children().last().prev().attr('data-id');
                const data = $(form).serializeJSON() || {};
                $.ajax({
                    url: '/TreeCategories/EditSupplies',
                    method: 'PUT',
                    data,
                })
                    .done(function (data) {

                        debugger
                        const successMsg = 'Cập nhật thành công';
                        $.notify(successMsg, { type: 'success' });
                        if (test === undefined) {
                            $("#kt_tree_4").jstree().close_node(vattu);
                            $("#kt_tree_4").jstree().open_node(vattu);
                        }
                        $("#kt_tree_4").jstree().close_node(test);
                        $("#kt_tree_4").jstree().open_node(test);
                    })
                    .fail(function (data) {

                        const errorMsg = "Có lỗi xảy ra";
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    };
    var initValidationEditTools = function () {
        $(".dmms-treecategoriesedittools__form").validate({
            rules: {

                name: {
                    required: true,
                },

            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên thiết bị.'
                },

            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('.dmms-treecategoriesedittools__form', -200);
                //$(".submit").attr("disabled", true);
            },
            submitHandler: function (form) {
                debugger
                var test = $('.kt-body-content').children().last().prev().attr('data-id');
                const data = $(form).serializeJSON() || {};
                $.ajax({
                    url: '/TreeCategories/EditSupplies',
                    method: 'PUT',
                    data,
                })
                    .done(function (data) {

                        debugger
                        const successMsg = 'Cập nhật thành công';
                        $.notify(successMsg, { type: 'success' });
                        if (test === undefined) {
                            $("#kt_tree_4").jstree().close_node(dungcu);
                            $("#kt_tree_4").jstree().open_node(dungcu);
                        }
                        $("#kt_tree_4").jstree().close_node(test);
                        $("#kt_tree_4").jstree().open_node(test);
                    })
                    .fail(function (data) {

                        const errorMsg = "Có lỗi xảy ra";
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    };
    var DeleteNode = function (category_uuid, category_id, parent_id) {

        
        var loading = new KTDialog({
            'type': 'loader',
            'placement': 'top center',
            'message': 'Đang xóa danh mục...'
        });
        KTApp.blockPage();
        $.ajax({
            url: `/TreeCategories/delete/`,
            method: 'DELETE',
            data: {
                category_uuid: category_uuid,
                category_id: category_id
            }
        })
            .done(function (data) {
                debugger
                loading.hide();
                KTApp.unblockPage();
                $.notify('Xóa thành công!', { type: 'success' });
                $("#kt_tree_4").jstree().close_node(parent_id);
                $("#kt_tree_4").jstree().open_node(parent_id);

            })
            .fail(function (data) {
                loading.hide();
                KTApp.unblockPage();
                $.notify('Xóa không thành công!', { type: 'danger' });
            });

    };

    var searchtree = function () {
        debugger
        $('#js-filter-keywordtree').change(function () {
            var searchstring = $(this).val();
            if (searchstring == "") {
                window.location = location.origin + "/treecategories"

            }
            KTApp.blockPage();
            //$('#kt_tree_4').jstree('search', searchstring);
            $.ajax({
                url: `/TreeCategories/searchtree/`,
                method: 'POST',
                data: {
                    keyword: searchstring,
                    page: 1,
                    limit: 5
                }
            })
                .done(function (data) {
                    debugger
                    $.each(data, function (index, item) {
                        if (item.parent == null && item.type == 1) {
                            item.parent = "thietbi";
                        }
                        else if (item.parent == null && item.type == 2) {
                            item.parent = "vattu";
                        }
                        else if (item.parent == null && item.type == 3) {
                            item.parent = "dungcu";
                        }

                        if (item.parent_id == null && item.type == 1) {
                            var node = { "id": "thietbi", "parent": "#", "text": "Thiết bị", "icon": "/statics/images/thietbi.png", "type": 1 }
                            data.push(node);
                        }
                        else if (item.parent_id == null && item.type == 2) {
                            var node1 = { "id": "vattu", "parent": "#", "text": "Vật tư", "icon": item.icon, "type": 2 }
                            data.push(node1);
                        }
                        else if (item.parent_id == null && item.type == 3) {
                            var node2 = { "id": "dungcu", "parent": "#", "text": "Dụng cụ", "icon": item.icon, "type": 3 }
                            data.push(node2);
                        }
                    })
                    KTApp.unblockPage();
                    $('#kt_tree_4').jstree(true).settings.core.data = data;
                    $('#kt_tree_4').jstree(true).refresh();
                    $('#kt_tree_4').jstree('open_all');
                    $('#kt_tree_4').unbind("open_node.jstree");


                })
                .fail(function (data) {
                    KTApp.unblockPage();
                    $.notify(data.msg, { type: 'danger' });
                });
        })
    }
    var searchtreecus = function () {
        debugger

        var searchstring = $('#js-filter-keywordtree').val();
        KTApp.blockPage();
        //$('#kt_tree_4').jstree('search', searchstring);
        $.ajax({
            url: `/TreeCategories/searchtree/`,
            method: 'POST',
            data: {
                keyword: searchstring,
                page: 1,
                limit: 5
            }
        })
            .done(function (data) {
                debugger
                $.each(data, function (index, item) {
                    if (item.parent == null && item.type == 1) {
                        item.parent = "thietbi";
                    }
                    else if (item.parent == null && item.type == 2) {
                        item.parent = "vattu";
                    }
                    else if (item.parent == null && item.type == 3) {
                        item.parent = "dungcu";
                    }

                    if (item.parent_id == null && item.type == 1) {
                        var node = { "id": "thietbi", "parent": "#", "text": "Thiết bị", "icon": "/statics/images/thietbi.png", "type": 1 }
                        data.push(node);
                    }
                    else if (item.parent_id == null && item.type == 2) {
                        var node1 = { "id": "vattu", "parent": "#", "text": "Vật tư", "icon": item.icon, "type": 2 }
                        data.push(node1);
                    }
                    else if (item.parent_id == null && item.type == 3) {
                        var node2 = { "id": "dungcu", "parent": "#", "text": "Dụng cụ", "icon": item.icon, "type": 3 }
                        data.push(node2);
                    }
                })
                KTApp.unblockPage();
                $('#kt_tree_4').jstree(true).settings.core.data = data;
                $('#kt_tree_4').jstree(true).refresh();
                $('#kt_tree_4').jstree('open_all');
                $('#kt_tree_4').unbind("open_node.jstree");


            })
            .fail(function (data) {
                KTApp.unblockPage();
                $.notify(data.msg, { type: 'danger' });
            });

    }
    var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
        'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
        'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
        'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
        'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
        'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
        'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
        'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
        'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
    ];
    var auto = function () {
        $(".autocomplete").autocomplete({
            
            source: JSON.parse(localStorage.getItem("mang"))
        });
       
    }
    

   
    return {
        init: function () {
            getJsonTree();
            initEventListeners();
            initValidation();
            initValidationSupplies();
            initValidationTools();
            initValidationEdit();
            initValidationEditSupplies();
            initValidationEditTools();
            searchtree();
            auto();
            //searchtreecus();
            initValidationsearch();
            $("input[name*='detail[exp_price_ref]']").simpleMoneyFormat();
         
        }
    };
}();

$(document).ready(function () {
    //debugger
    if ($('.dmms-treecategories').length) {
        KTTreeview.init();
    }

});

