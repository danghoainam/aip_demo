﻿"use strict";
// Class definition

var DMMSDeviceHistory = function () {
    // Private functions
    var LIMIT = 5;
    var _this = {};
    var history = {
        $container: $('#device_history_body'),

        load: function (perpage) {
            var uuid = $('#uuid').val();
            KTApp.block(this.$container);
            $.ajax({
                url: `/device/gethistory/${uuid}`,
                method: 'GET',
                data: {
                    limit: LIMIT,
                    page: perpage ? perpage : '',
                }
            }).done((res) => {
                KTApp.unblock(this.$container);
                if (res && res.data) {
                    this.render((res.data || {}), (perpage || 1));
                }
            }).fail(() => {
                KTApp.unblock(this.$container);
                $.notify('Không thể kết nối đến máy chủ', { type: 'danger' });
            });
        },

        render: function (data, perpage) {
            debugger
            var states = {
                1: { 'title': 'Hoạt động', 'class': 'kt-badge--success' },
                2: { 'title': 'Sửa chữa', 'class': ' kt-badge--warning' },
                3: {
                    'title': 'Đến lịch bảo hành', 'class': ' kt-badge--primary', 'style': 'background-color:#074648c4'
                },
                4: { 'title': 'Sửa chữa', 'class': 'kt-badge--warning' },
                5: { 'title': 'Sửa chữa', 'class': 'kt-badge--warning' },
                6: { 'title': 'Sửa chữa', 'class': 'kt-badge--warning' },
                7: { 'title': 'Sửa chữa', 'class': 'kt-badge--warning' },
                8: { 'title': 'Thay thế tạm thời', 'class': 'kt-badge--info' },
                9: { 'title': 'Hỏng', 'class': 'kt-badge--danger' },
                10: { 'title': 'Hoạt động', 'class': 'kt-badge--danger' },
                11: { 'title': 'Hỏng', 'class': 'kt-badge--warning' },
                12: { 'title': 'Hoạt động', 'class': 'kt-badge--success' },
                13: { 'title': 'Đã xuất thanh lý', 'class': 'kt-badge--primary' },
                14: { 'title': 'Đã xuất tiêu hao', 'class': 'kt-badge--primary' },
                15: { 'title': 'Đã hoàn trả nhà cung cấp', 'class': 'kt-badge--primary' },
                16: { 'title': 'Thêm mới/cập nhật thiết bị', 'class': 'kt-badge--info' },
                17: { 'title': 'Hoạt động', 'class': 'kt-badge--success' },
            };
            var html = [];
            $.each(data.items, function (index, item) {
                debugger
                var name = ''
                if (item.staff != null && item.staff.name != null) {
                    name = item.staff.name
                }
                html.push('<div class="kt-device-history">');
                html.push(`
                    <div class="kt-items row">
                        <div class="col-md-12 kt-item">
                            <span class="kt-margin-r-20">Trạng thái thiết bị:</span>
                            <a class="kt-badge ${states[item.state].class} kt-badge--inline kt-badge--pill kt-item__state" style="${states[item.state].style}">${states[item.state].title}</a>
                        </div>
                        <div class="col-md-12 kt-item">
                           <span>Người cập nhật:</span>
                           <a href="/staff/update/${item.staff.uuid}" class="kt-padding-l-20 text-primary">${name}</a>
                           <a class="kt-item__time">${item.time_created_format}</a>
                        </div>

                `);
                if (item.note !== null) {
                    if (item.note.charAt(0) === '{' && item.note.charAt(item.note.length - 1) === "}") {
                        var noteInfo = JSON.parse(item.note);
                        html.push(`
                        <div class="col-md-12 kt-item">
                            <span>Tên thiết bị:&nbsp&nbsp${noteInfo.name}</span><br />
                            <span>Serial:&nbsp&nbsp${noteInfo.serial}</span><br />
                            <span>Imei:&nbsp&nbsp${noteInfo.imei}</span><br />
                            <span>Model:&nbsp&nbsp${noteInfo.model}</span><br />
                        </div>
                    `)
                    }
                    else {
                        html.push(`
                        <div class="col-md-12 kt-item">
                            ${item.note}
                        </div>
                        `)
                    }
                }
                html.push(`<div class="kt-item row">`);
                if (item.images != null) {
                    $.each(item.images, function (index, img) {
                        html.push(`
                        <div class="col-md-2">
                            <img src="${img.path}" title="${img.name}" />
                        </div>
                    `)
                    });
                }
                html.push('</div>');
                html.push('</div>');
                html.push('</div>');
            });

            //pagination
            if (data.pagination != null && data.pagination.pages !== null && data.pagination.pages > 1) {
                debugger
                html.push(`<div class="kt-section">
                                <p class="kt-section__content">
                                    <div class="kt-pagination  kt-pagination--brand">
                                        <ul class="kt-pagination__links kt-pagination-list">`);
                if (perpage <= 1) {
                    html.push(`
                        <li title="Trang đầu" class="kt-pagination__link--first js-load-first disabled">
                            <a><i class="flaticon2-fast-back kt-font-brand"></i></a>
                        </li>
                        <li  title="Trang trước" class="kt-pagination__link--next js-load-next disabled" data-page="${perpage}">
                            <a><i class="flaticon2-back kt-font-brand"></i></a>
                        </li>
                    `)
                }
                else {
                    html.push(`
                        <li "title="Trang đầu" class="kt-pagination__link--first js-load-first">
                            <a><i class="flaticon2-fast-back kt-font-brand"></i></a>
                        </li>
                        <li title="Trang trước" class="kt-pagination__link--next js-load-next" data-page="${perpage}">
                            <a><i class="flaticon2-back kt-font-brand"></i></a>
                        </li>
                    `)
                }
                if (perpage > 10) {
                    for (var j = 10; j > 0; j--) {
                        debugger
                        html.push(`<li title="Trang ${perpage - j}" class="js-load-pagination" data-perpage="${perpage - j}">
                                <a>${perpage - j}</a>
                            </li>`);
                    }
                    html.push(`<li title="Trang ${perpage}" class="js-load-pagination" data-perpage="${perpage}">
                                <a>${perpage}</a>
                            </li>`);
                   
                }
                else {
                for (var i = 1; i <= data.pagination.pages; i++) {
                    debugger
                    
                    
                        if (data.pagination.pages > 10) {
                            if (i == 11) { break; }
                            html.push(`<li title="Trang ${i}" class="js-load-pagination" data-perpage="${i}">
                                <a>${i}</a>
                            </li>`);
                        }
                        else {
                            html.push(`<li title="Trang ${i}" class="js-load-pagination" data-perpage="${i}">
                                <a>${i}</a>
                            </li>`);
                        }
                    }
                   
                   
                }
                if (perpage == data.pagination.pages) {
                    html.push(`<li title="Trang sau" class="kt-pagination__link--prev js-load-prev disabled" data-page="${perpage}">
                                <a><i class="flaticon2-fast-next kt-font-brand"></i></a>
                            </li>
                            <li  title="Trang cuối" class="kt-pagination__link--last js-load-last disabled" data-page="${data.pagination.pages}">
                                <a><i class="flaticon2-next kt-font-brand"></i></a>
                            </li>
                        </ul>
                    </div>
                </p>`)
                }
                else {
                    html.push(`<li title="Trang sau" class="kt-pagination__link--prev js-load-prev" data-page="${perpage}">
                                <a><i class="flaticon2-fast-next kt-font-brand"></i></a>
                            </li>
                            <li  title="Trang cuối" class="kt-pagination__link--last js-load-last" data-page="${data.pagination.pages}">
                                <a><i class="flaticon2-next kt-font-brand"></i></a>
                            </li>
                        </ul>
                    </div>
                </p>`)
                }

                html.push(`</div>`);
            }

            this.$container.html(html.join(''));
            this.select(perpage);
        },

        select: function (perpage) {
            if (perpage == null || perpage == '') {
                perpage = 1;
            }
            const $history = $(`li[data-perpage="${perpage}"]`);

            const $parent = $history.parent('.kt-pagination-list');
            $parent.find('.kt-pagination__link--active').removeClass('kt-pagination__link--active');
            const $active = $parent.siblings('.kt-pagination__link--active');
            $active.removeClass('kt-pagination__link--active').find('.kt-pagination__link--active').removeClass('kt-pagination__link--active');

            $parent.addClass('kt-pagination__link--active');
            $history.addClass('kt-pagination__link--active');
        },
    };

    var initEventListeners = function () {
        $(document)
            .on('click', '.js-load-pagination', function () {
                const $this = $(this);
                const page = $this.attr('data-perpage');

                history.load(page);
            })
            .on('click', '.js-load-next', function () {
                const $this = $(this);
                const page = parseInt($this.attr('data-page')) - 1;

                history.load(page);
            })
            .on('click', '.js-load-prev', function () {
                const $this = $(this);
                const page = parseInt($this.attr('data-page')) + 1;

                history.load(page);
            })
            .on('click', '.js-load-first', function () {
                const page = 1;

                history.load(page);
            })
            .on('click', '.js-load-last', function () {
                const $this = $(this);
                const page = $this.attr('data-page');

                history.load(page);
            });
    };

    return {
        // Public functions
        init: function () {
            $('#device_history').click(function () {
                history.load();
            });
            initEventListeners();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-device').length) {
        DMMSDeviceHistory.init();
    }
});
