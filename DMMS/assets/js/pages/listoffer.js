"use strict";
// Class definition

var DMMSKnowledge = function () {
    // Private functions

    var dataTableInstance = null;
    var drawer = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    var number = "";
    var unit = "";
    var classify = "";
    var selecttext = "";
    var selectval = "";
    // demo initializer
    var initTable = function () {
        var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
        if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
            $.notify(editSucessCache.massage, { type: 'success' });
            localStorage.removeItem('edit_success');
        }
        dataTableInstance = $('.kt-datatable').KTDatatable({

            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/listoffer/gets',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page,
                                //    CURRENT_PAGE = page || 1;
                                //LIMIT = perpage;
                                START_PAGE = 0;
                            //Gán các thuộc tính của images{0} vào đối tượng item
                            //if (raw.items != null) {
                            //    (raw.items).forEach(function (e) {
                            //        e.path = e.image != null ? e.image.path : null;

                            //        return e;
                            //    })
                            //}
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,
            },

            columns: [
                {
                    width: 20,
                    field: 'uuid',
                    title: '#',
                    template: function (data, row) {
                        debugger
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'code',
                    title: 'Mã đề xuất',
                    width: 100,

                },
                {
                    field: 'order_person.name',
                    title: 'Người đề xuất',
                    width: 120,

                },
                {
                    field: 'task.code',
                    title: 'Mã - Công việc',
                    width: 100
                },
                {
                    field: 'from.name',
                    title: 'Kho nguồn',
                    width: 120
                },
                {
                    field: 'to.name',
                    title: 'Kho đích',
                },
                {
                    field: 'time_created_format_nohh',
                    title: 'Thời gian',
                    width: 120
                },
                {
                    field: 'state',
                    title: 'Trạng thái',
                    autoHide: false,
                    width: 150,
                    template: function (data) {

                        var states = {
                            1: { 'title': 'Đã duyệt', 'class': 'kt-badge--success' },
                            2: { 'title': 'Chưa duyệt', 'class': ' kt-badge--warning' },
                            3: { 'title': 'Đã từ chối', 'class': ' kt-badge--danger' },
                            4: { 'title': 'Đã bàn giao TB-VT', 'class': ' kt-badge--success' },
                            5: { 'title': 'Chưa bàn giao đủ TB-VT', 'class': ' kt-badge--brand' },
                        };
                        if (data.status) {
                            return '<span class="kt-badge ' + states[data.status].class + ' kt-badge--inline kt-badge--pill">' + states[data.status].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới</span>';
                        }
                    },
                    width: 150,
                    textAlign: 'center'
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 55,
                    overflow: 'visible',
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        return `
                           <a href="/listoffer/edit/${data.uuid}" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </a>
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };
    var initSearch = function () {
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });

        $('#js-filter-keyword_device')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase().trim(), 'keyword');
                    START_PAGE = 1;
                }
            });
    };

    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var deleteSupplier = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/knowledge/delete/${id}`,
                method: 'DELETE',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa nhà chủ đề thành công!', { type: 'success' });

                    reloadTable();
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa không thành công!', { type: 'danger' });
                });
        }
    };

    var dataCustom = null;
    var initCreateOfferDrawer = function (id, categories) {
        debugger
        const title = 'Phiếu đề xuất thiết bị vật tư từ kho đến kho';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--listoffer' });

        drawer.on('ready', () => {
            $.ajax({
                url: 'listoffer/create',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    dataCustom = response ? categories.data : null;
                    $('.customoffer').click(function (e) {
                        debugger
                        number = $(this).closest("tr").find("#name").val();
                        unit = $(this).closest("tr").find(".td1").text();
                        classify = $(this).closest("tr").find(".td2").text();
                        selecttext = $(this).closest("tr").find("#kt-select-customer1 option:selected").text();
                        selectval = $(this).closest("tr").find("#kt-select-customer1").val();


                        e.preventDefault();
                        initControlSelect(dataCustom)
                        $('.controlSelect1').selectpicker('refresh');
                    });
                    $('.custable').on("click", ".deleteinput", function (e) { //user click on remove text
                        debugger
                        e.preventDefault();
                        $(this).closest('tr').remove();
                        var table = document.getElementById("createtable");
                        var tbodyRowCount = table.tBodies[0].rows.length; // đếm số tr trong body
                        if (tbodyRowCount > 1) {
                            $('#tab1submit').removeAttr('disabled');
                        }
                        else {
                            $('#tab1submit').attr('disabled', 'disabled');
                        }
                        $('.bootstrap-select').selectpicker('refresh');
                    });
                    initContent($content);
                    jQuery.fn.preventDoubleSubmission = function () {
                        $(this).on('submit', function (e) {
                            var $form = $(this);

                            if ($form.data('submitted') === true) {
                                // Previously submitted - don't submit again
                                e.preventDefault();
                            } else {
                                // Mark it so that the next submit can be ignored
                                $form.data('submitted', true);
                            }
                        });

                        // Keep chainability
                        return this;
                    };
                    $('form.dmms-listsupplies__form').preventDoubleSubmission();
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var initControlSelect = function (data) {
        debugger
        //if (data != null && data.length > 0) {
        //Thực hiện lấy danh sách các gói đã select

        var lstItem = $(".controlSelect"),
            arrItem = [],
            //Dánh sách lưu các id được seclect
            lstItemResult = [];//tạo mảng danh sách mới



        for (var i = 0; i < lstItem.length; i++) {
            var valueItem = $(lstItem[i]).val();
            if (valueItem != "") {
                arrItem.push(valueItem);

            }
        }

        //Danh sách mới các item sẽ không có các giá trị selected
        //Thực hiện lọc lại danh sách
        //Sử dụng findler để lọc
        debugger
        data.items.forEach(function (item) {

            var countSelected = arrItem.length;
            if (countSelected > 0) {
                arrItem.forEach(function (i, e) {
                    if (item.id != i) {
                        countSelected--;
                    }
                    if (countSelected == 0) {
                        lstItemResult.push(item);
                    }
                })
            } else {
                lstItemResult = data.items;
                return
            }
        })

        var select = `
            <tr>
                             <td> <input name="totals[][uuid]" hidden value="${selectval}">${selecttext}</td> `;

        var html = `
            <td><input name="totals[][unit]" hidden value="${unit}" />${unit}</td>
            <td><input name="totals[][number]" hidden value="${number}"/>${number}</td>
            <td> <input name="totals[][classify]" hidden value="${classify}" />${classify}</td>
             <td> <i class="btn btn-danger kt-ml-10 la la-trash deleteinput"></i></td>
            </tr>`;
        debugger
        if ($('#kt-select-customer1').val() != "" && $('#name').val() != "") {
            $('.custable').append(select + html);
            $("#name").val('');
            $(".td1").text('');
            $(".td2").text('');
            $("#kt-select-customer1").val('');
            $("#kt-select-customer1").selectpicker('refresh');
        }
        else {
            const successMsg = 'Vui lòng điền đủ thông tin';
            $.notify(successMsg, { type: 'danger' });

        }

        $('.controlSelect').unbind('change');
        $('.controlSelect')
            .bind('change', function () {
                $('.controlSelect1').selectpicker('refresh');

                var lstItem1 = $(".controlSelect"),
                    arrItem1 = [];
                for (var i = 0; i < lstItem1.length; i++) {
                    var valueItem = $(lstItem1[i]).val();
                    if (valueItem != "") {
                        arrItem1.push(valueItem);

                    }
                }
                var abcd = $(this).val()
                var az = arrItem1.filter(item => item == abcd);
                if (az != "" && az.length > 1) {
                    $.notify('Bạn đã chọn thiết bị này rồi. Vui lòng chọn lại!', { type: 'danger' });
                    $(this).val("");

                    $('.controlSelect').selectpicker('refresh');
                }

            });
        //}

    };

    var createOffer = function () {
        const title = 'PHIẾU ĐỀ XUẤT THIẾT BỊ VẬT TƯ TỪ KHO ĐẾN KHO';
        drawer = new KTDrawer({ title, uuid: 'new', className: 'dmms-drawer--addlistoffer' });

        drawer.on('ready', () => {
            $.ajax({
                url: `/listoffer/create`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);

                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    }

    var decideListOffer = function (id, reviewNote, accept) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang thực hiện thao tác...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/listoffer/decide`,
                method: 'POST',
                data: {
                    order_uuid: id,
                    reviewNote: reviewNote,
                    accept: accept
                }
            })
                .done(function () {
                    const msg = accept == "true" ? "Duyệt đề xuất thành công!" : "Từ chối đề xuất thành công!";
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(msg, { type: 'success' });

                    //reload lại trang detail
                    window.location.pathname = `/listoffer/edit/${id}`;
                })
                .fail(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(data.responseJSON.msg, { type: 'danger' });
                });
        }
    };

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-supplier')
            .on('click', '.js-delete-supplier', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                var input = document.createElement("textarea");
                input.className = "note form-control";
                input.cols = "40";
                input.rows = "5";
                input.name = ("note");
                KTApp.swal({
                    text: "Nội dung từ chối đề xuất",
                    content: input,
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteSupplier(id);
                    }
                });
            })
            .off('click', '.js-create-offer')
            .on('click', '.js-create-offer', function () {
                createOffer();
            })
            .off('click', '.js-decide-listoffer')
            .on('click', '.js-decide-listoffer', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                const accept = $this.attr('data-accept');
                var textarea = document.createElement('textarea');
                textarea.placeholder = 'Nhập nội dung từ chối đề xuất...';
                textarea.className = 'form-control';
                textarea.name = 'reviewNote';
                textarea.id = 'reviewNote';

                const accept_title = accept == "true" ? 'DUYỆT ĐỀ XUẤT' : 'TỪ CHỐI ĐỀ XUẤT';
                KTApp.swal({
                    title: accept_title,
                    content: accept == "true" ? '' : textarea,
                    text: accept == "true" ? 'Xác nhận duyệt đề xuất!' : '',
                    dangerMode: true,
                }).then((confirm) => {
                    const reviewNote = $('#reviewNote').val();
                    if (confirm) {
                        decideListOffer(id, reviewNote, accept);
                    }
                });
            });
        //.off('click', '.js-create-offer')
        //.on('click', '.js-create-offer', function () {
        //    debugger
        //    KTApp.blockPage();
        //    const $this = $(this);
        //    const id = $this.attr('data-id');
        //    $.ajax({
        //        url: "/staff/gets",
        //        method: 'GET',
        //        data: { limit: 100 }
        //    })
        //        .done(function (data) {
        //            const categories = data;
        //            KTApp.unblockPage();
        //            initCreateOfferDrawer(id || '', categories || '');
        //        })
        //        .fail(function () {
        //            $.notify('Có lỗi xảy ra !', { type: 'danger' });
        //        });
        //});
    };

    //var initForm = function ($form) {
    //    debugger
    //    var table = document.getElementById("createtable");
    //    var tbodyRowCount = table.tBodies[0].rows.length; // đếm số tr trong body
    //    if (tbodyRowCount > 1) {
    //        alert('xx');
    //        return null;
    //    }
    //    initValidation(true);
    //}

    var initForm = function ($form) {
        $form.validate({
            rules: {

            },
            messages: {

            },

            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
                $('#kt-select-staff').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                $.ajax({
                    url: '/listoffer/create',
                    method: 'POST',
                    data,
                })
                    .done(function () {
                        KTApp.block($form);
                        const successMsg = 'Thêm thông tin thành công!';
                        $.notify(successMsg, { type: 'success' });

                        /**
                         * Close drawer
                         */
                        drawer.close();

                        reloadTable();
                    })
                    .fail(function () {
                        KTApp.unblock($form);
                        const successMsg = 'Thêm thông tin không thành công!';
                        $.notify(successMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    };

    $(".linkofferdevice").on("click", function () {
        const categoryOrderUuid = $('#uuid').val();
        dataTableInstance = $('.kt-datatableofferdevice').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: `/treecategories/getlist/${categoryOrderUuid}`,
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? undefined : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: keyword || $('#js-filter-keyword_device').val(),
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,

            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 30,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'name',
                    title: 'Tên vật tư - thiết bị',
                    template: function (data) {
                        return `<strong class="text-primary">${data.prefixName}</strong><br />
                                <span>${data.name}</span>`
                    }
                },
                {
                    field: 'type',
                    title: 'Phân loại',
                    width: 110,
                    template: function (data) {
                        var types = {
                            1: { 'title': 'Thiết bị' },
                            2: { 'title': 'Vật tư' },
                            3: { 'title': 'Dụng cụ LĐ' },
                            4: { 'title': 'Phần mềm' },
                        };
                        if (data.state) {
                            return '<span>' + types[data.type].title + '</span>';
                        }
                        else {
                            return '<span>' + types[1].title + '</span>';
                        }
                    }
                },
                {
                    field: 'quantity',
                    title: 'Số lượng đề xuất',
                    textAlign: 'center',
                    width: 110,
                },
                {
                    field: 'unit',
                    title: 'Đơn vị tính',
                    textAlign: 'center',
                    width: 110,
                },
            ],
            layout: {
                scroll: !0,
                height: null,
                footer: !1
            },
            sortable: false,
            pagination: true,
            responsive: true,
        });
        $('.filtercustomer').selectpicker();
    });

    var initValidation = function (isForm) {
        $(".dmms-listoffer__form").validate({
            rules: {
                name: {
                    required: true,

                },
            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên câu hỏi.'

                },
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('.dmms-listoffer__form', -200);
            },
            submitHandler: function (form) {
                var table = document.getElementById("createtable");
                var tbodyRowCount = table.tBodies[0].rows.length; // đếm số tr trong body
                if (tbodyRowCount == 1) {
                    KTApp.swal({
                        icon: 'warning',
                        title: 'Không thành công',
                        text: 'Bạn phải thêm Danh sách thiết bị vật tư!',
                        dangerModeCustom: true
                    });
                    return null
                }
                $('.abc').html('changed value');
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                console.log(data, uuid);
                $.ajax({
                    url: 'listoffer/create',
                    method: 'POST',
                    data,
                })
                    .done(function () {
                        $(".submit").attr("disabled", true);
                        KTApp.block('.dmms-listoffer__form');
                        const successMsg = uuid ? 'Sửa thông tin thành công!' : 'Thêm câu hỏi thành công!';
                        $.notify(successMsg, { type: 'success' });
                        if (isForm) {
                            drawer.close();
                            reloadTable();
                        }
                    })
                    .fail(function () {

                        KTApp.unblock('.dmms-listoffer__form');
                        KTApp.swal({
                            icon: 'error',
                            title: 'Không thành công',
                            text: 'Something went wrong!',
                            dangerModeCustom: true
                        });


                    });
                return false;
            },
        });
    }


    //var initContent = function ($content) {
    //    /*
    //        Init form
    //    */
    //    const $form = $content.find('.dmms-listsupplies__form');
    //    initForm($form);
    //    $('.listofferfilter').selectpicker();

    //    /*
    //        init auto-resize
    //    */
    //    autosize($content.find('textarea.auto-resize'));


    //    /**
    //     * Init Dropzone
    //     */
    //    //initDropzone();
    //};

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-form__addoffer');
        initForm($form);

        /*
            Init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

        $('#kt-select-filed').selectpicker();
        $('#kt-select-bunker').selectpicker();
        $('#kt-select-supplies').selectpicker({ showSubtext: true });

        //Change #kt-select-supplies
        $('#kt-select-supplies').on("change", function () {
            const type_categries = { 1: "Thiết bị", 2: "Vật tư", 3: "Dụng cụ LĐ", 4: "Phần mềm" };
            const wrapper = $('.wrapper-fill');
            const category = wrapper.find('#kt-select-supplies');
            const category_id = category.val();

            if ($(this).val() != '') {
                $.ajax({
                    url: `/bunker/DetailSuppliesAjax/${category_id}`,
                    method: 'GET'
                })
                    .done(function (data) {
                        wrapper.find('.kt-select-supplies').attr('name', 'categories[][category_id]');
                        wrapper.find('#offer_quantity').attr('name', 'categories[][offer_quantity]');
                        $('#type_category').html(type_categries[data.data.type]);
                        $('#unit').html(data.data.detail.unit);
                    });
            }
            else {
                wrapper.find('.kt-select-supplies').removeAttr('name');
                wrapper.find('#offer_quantity').removeAttr('name');
            }
        });

        $('#js-add-offersupplies').on('click', function () { //Thêm thiết bị - vật tư
            const wrapper = $('.wrapper-fill');
            const supplies = wrapper.find('#kt-select-supplies');
            const category_id = supplies.val();
            if (category_id != "") {
                addSuppliesToOffer();

                $('#kt-select-supplies').find(`option[value="${category_id}"]`).attr('disabled', true); //Disabled option của chọn vật tư
                $('#kt-select-supplies').selectpicker('refresh');
            }
            else $.notify('Vui lòng chọn vật tư!', { type: 'danger' });
        });

        //close drawer
        $('.kt-close').on('click', function () {
            drawer.close();
        });
    };

    var addSuppliesToOffer = function () {
        const wrapper = $('.wrapper-fill');

        const supplies = wrapper.find('#kt-select-supplies');
        const category_id = supplies.val(); //uuid của category
        const category_prefix = supplies.find('option:selected').text(); //prefix của category
        const category_name = supplies.find('option:selected').attr('data-subtext'); //Tên của category
        const type_category = wrapper.find('#type_category').text(); //Tên của loại (1-vật tư, 2-thiết bị, 3-Dụng cụ LĐ, 4-Phần mềm)
        const offer_quantity = wrapper.find('input[id="offer_quantity"]').val() != '' ? wrapper.find('input[id="offer_quantity"]').val() : 0; //Số lượng thiết bị đề xuất
        const unit = wrapper.find('#unit').text(); //Đơn vị tính

        //Nếu có giá trị của vật tư thiết bị => add thêm raw vật tư thiết bị
        if (category_id != null) {
            wrapper.append(`<tr id="${category_id}">
                        <td class="text-left kt-padding-l-10" style="max-width:340px"><input name="categories[][category_id]" value="${category_id}" hidden />
                            <span class="text-primary">${category_prefix}</span>
                            <br/>${category_name}
                        </td>
                        <td>${type_category}</td>
                        <td><input class="form-control" placeholder="Nhập" name="categories[][offer_quantity]" type="number" min="0" value="${offer_quantity}" /></td>
                        <td>${unit}</td>
                        <td><i class="la la-trash remove_field" style="background-color: #FF5F6D"></i></td>
                    </tr>`);

            //Sau khi add thêm category => reset giá trị của raw nhập liệu
            wrapper.find('#type_category').html('-'); //Phân loại => "-"
            wrapper.find('input[id="offer_quantity"]').val(''); //số lượng theo chứng từ => ''
            wrapper.find('#unit').html('-'); //đơn vị tính => "-"
            supplies.selectpicker('val', ''); //tên thiết bị vật tư => ""
            wrapper.find('.kt-select-supplies').removeAttr('name');
            wrapper.find('#offer_quantity').removeAttr('name');
        }
        else $.notify('Vui lòng chọn vật tư!', { type: 'danger' });

        $(wrapper).find(`tr[id=${category_id}]`).on("click", `.remove_field`, function (e) { //user click on remove text
            e.preventDefault();
            const $this = $(this);

            $this.parent('td').parent('tr').remove();
            $('#kt-select-supplies').find(`option[value="${category_id}"]`).attr('disabled', false); //Disabled option của chọn vật tư
            $('#kt-select-supplies').selectpicker('refresh');
        });
    }

    return {
        // Public functions
        init: function () {
            initTable();
            initSearch();
            initEventListeners();
            initValidation();

        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-listoffer').length) {
        DMMSKnowledge.init();
    }
});
