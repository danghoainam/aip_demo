﻿"use strict";
// Class definition

var DMMSAccount = function () {
    var initValidationUpdateInfo = function () {
        $("#dmms-account-update-info__form").validate({
            rules: {
                "Account.name": {
                    required: true,
                },
                "Account.email": {
                    required: true,
                },
                "Account.phone_number": {
                    phoneVN: true,
                    required: true,
                },
            },
            messages: {
                "Account.name": {
                    required: 'Vui lòng nhập họ tên'
                },
                "Account.email": {
                    required: 'Vui lòng nhập email'
                },
                "Account.phone_number": {
                    required: 'Vui lòng nhập số điện thoại',
                },
            },

            submitHandler: function (form) {
                console.log("info", form, form[0]);
                $.ajax({
                    url: "/account/updateinfo",
                    method: 'PUT',
                    data: $(form).serializeJSON() || {},
                })
                    .done(function (res) {
                        $.notify('Thay đổi thông tin thành công!', { type: 'success' });
                    })
                    .fail(function (res) {
                        $.notify('Thay đổi thông tin không thành công!', { type: 'danger' });
                    });
                return false;
            },
        });

        
    };

    var initValidationChangePass = function () {
        $("#dmms-account-change-pass__form").validate({
            rules: {
                "ChangePassword.old_password": {
                    required: true,
                },
                "ChangePassword.new_password": {
                    required: true,
                },
                "ChangePassword.verify_new_password": {
                    required: true,
                },
            },
            messages: {
                "ChangePassword.old_password": {
                    required: 'Vui lòng mật khẩu cũ'
                },
                "ChangePassword.new_password": {
                    required: 'Vui lòng nhập mật khẩu mới'
                },
                "ChangePassword.verify_new_password": {
                    required: 'Vui lòng nhập lại mật khẩu mới',
                },
            },

            submitHandler: function (form) {
                $.ajax({
                    url: "/account/change-pass",
                    method: 'PUT',
                    data: $(form).serializeJSON() || {},
                })
                .done(function (res) {
                    $.notify('Cập nhật mật khẩu thành công', { type: 'success' });
                })
                    .fail(function (res) {
                        $.notify(res.responseJSON.msg, { type: 'danger' });
                    });

                return false;
            },
        });
    }
    var initEventListeners = function () {
        $(document)
            .off('click', '.js-block-acc')
            .on('click', '.js-block-acc', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Bạn có muốn reset mật khẩu.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        resetPassword(id);
                    }
                });
            })
            
    };
    var resetPassword = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang tải dữ liệu...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/account/resetpassword/${id}`,
                method: 'POST',
            })
                .done(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(`Đặt lại mật khẩu thành công!`, { type: 'success' });
                })
                .fail(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Đặt lại mật khẩu không thành công!', { type: 'danger' });
                });
        }
    };
    return {
        // Public functions
        init: function () {
            initValidationUpdateInfo();
            initValidationChangePass();
            initEventListeners();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-account-personal').length) {
        DMMSAccount.init();
    }
});
