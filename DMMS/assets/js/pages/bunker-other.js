﻿"use strict";
// Class definition

var DMMSBunker = function () {
    // Private functions

    var dataTableInstance = null;
    var dataTableInstanceImDevice = null;
    var dataTableInstanceim = null;
    var dataTableInstanceEx = null;
    var dataTableDetailIm = null;
    //var dataTableInstance = null;
    var drawer = null;
    var subDrawer = null;
    var drawerInner = null;
    var drawerDetail = null;
    var drawerim = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;

    $('.bunker-tab-ticketimport').on('click', function () {
        debugger
        var storageUuid = $('#uuid').val();
        if (dataTableInstance !== null && dataTableInstance !== undefined) { dataTableInstance.destroy(); }
        dataTableInstance = $('.kt-datatable--ticketimport').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: `/bunker/ListImport/${storageUuid}`,
                    },
                    response: {
                        map: function (res) {
                            debugger
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? 0 : pagination.page;
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        debugger
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword, type_of_ballot, type } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            type_of_ballot: type_of_ballot || $("#js-filter-typeiore").val(),
                            type: type || $("#js-filter-typecard").val(),
                            keyword: (keyword || $('#js-filter-keyword--ticketimport').val()).trim(),
                            page: pagination.page,
                        }
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,

            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 30,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'code',
                    title: 'Số chứng từ',
                    width: 90
                },
                {
                    field: 'documentOrigin',
                    title: 'Chứng từ gốc',
                    width: 90
                },
                {
                    field: 'type.type_Name',
                    title: 'Loại phiếu nhập kho',
                    width: 160
                },
                {
                    field: 'namefrom',
                    title: 'Nhập từ-Kho-NVKT-Khác',

                },
                {
                    field: 'toStaff.name',
                    title: 'Nhân viên nhập',
                    width: 100
                },
                {
                    field: 'time_created',
                    title: 'Thời gian',
                    width: 100
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 50,
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        return `<button data-id="${data.uuid}" data-name="${data.namefrom}" type="button" title="Chi tiết" class="btn btn-sm btn-primary btn-icon btn-icon-md js-detail-ticketimport">
                                <i class="la la-info"></i>
                            </a>`;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    });

    $('.bunker-tab-ticketexport').on('click', function () {
        var storageUuid = $('#uuid').val();
        if (dataTableInstance !== null && dataTableInstance !== undefined) { dataTableInstance.destroy(); }
        dataTableInstance = $('.kt-datatable--ticketexport').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: `/bunker/ListExport/${storageUuid}`,
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? 0 : pagination.page;
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword, type_of_ballot, type } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            type_of_ballot: type_of_ballot || $("#js-filter-typeiore_export").val(),
                            type: type || $("#js-filter-typecard_export").val(),
                            keyword: (keyword || $('#js-filter-keyword--ticketexport').val()),
                            page: pagination.page,
                        }
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,

            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 30,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'code',
                    title: 'Số chứng từ',
                    width: 90
                },
                {
                    field: 'documentOrigin',
                    title: 'Chứng từ gốc',
                    width: 90
                },
                {
                    field: 'type.type_Name',
                    title: 'Loại phiếu xuất kho',
                },
                {
                    field: 'nameto',
                    title: 'Kho đích',
                },
                {
                    field: '#',
                    title: 'Trạng thái',
                    width: 150,
                    template: function (data) {
                        var states = {
                            1: { 'title': 'Chưa xác nhận', 'class': 'kt-badge--warning' },
                            2: { 'title': 'Đã xác nhận', 'class': ' kt-badge--success' },
                            3: { 'title': 'Đã từ chối', 'class': ' kt-badge--danger' },
                            4: { 'title': 'Đợi xử lý', 'class': ' kt-badge--info' },
                            5: { 'title': 'Đã thu hồi', 'class': ' kt-badge--success' },
                        };
                        if (data.status) {
                            return '<span class="kt-badge ' + states[data.status].class + ' kt-badge--inline kt-badge--pill">' + states[data.status].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới tạo</span>';
                        }
                    }
                },
                {
                    field: 'fromStaff.name',
                    title: 'Nhân viên xuất kho'
                },
                {
                    field: 'time_created',
                    title: 'Thời gian',
                    width: 100
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 90,
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        var output = `<button data-name="${data.nameto}" data-id="${data.uuid}" type="button" title="Chi tiết" class="btn btn-sm btn-primary btn-icon btn-icon-md js-detail-ticketexport">
                                <i class="la la-info"></i>
                            </button>`;
                        if ($('#state').val() == 1) {
                            if (data.status == 3)
                                output += `<button data-name="${data.nameto}" data-id="${data.uuid}" type="button" title="Thu hồi" class="kt-margin-l-10 btn btn-sm btn-success btn-icon btn-icon-md js-rollback_device">
                                <i class="la la-undo"></i>
                            </button>`;
                            else
                                output += `<button data-name="${data.nameto}" data-id="${data.uuid}" type="button" title="Thu hồi" class="kt-margin-l-10 btn btn-sm btn-success btn-icon btn-icon-md js-rollback_device" disabled>
                                <i class="la la-undo"></i>
                            </button>`;
                        }
                        return output;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    });
    $('.bunker-tab-listexportto').on('click', function () {
        debugger
        var storageUuid = $('#uuid').val();
        if (dataTableInstance !== null && dataTableInstance !== undefined) { dataTableInstance.destroy(); }
        dataTableInstance = $('.kt-datatable--listexportto').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: `/bunker/ListExportTo/${storageUuid}`,
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? 0 : pagination.page;
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword, status } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            //type_of_ballot: type_of_ballot || $("#js-filter-type_storage").val(),
                            status: status || $("#js-filter-state_exportto").val(),
                            keyword: (keyword || $('#js-filter-keyword--messenger').val()),
                            page: pagination.page,
                        }
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,

            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 30,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'code',
                    title: 'Số chứng từ',
                    width: 90
                },
                //{
                //    field: 'documentOrigin',
                //    title: 'Chứng từ gốc',
                //    width: 90
                //},
                //{
                //    field: 'type.type_Name',
                //    title: 'Loại phiếu xuất kho',
                //},
                {
                    field: 'namefrom',
                    title: 'Kho nguồn',
                },
                {
                    field: 'fromStaff.name',
                    title: 'Nhân viên xuất kho'
                },
                {
                    field: '#',
                    title: 'Trạng thái',
                    width: 150,
                    template: function (data) {
                        var states = {
                            1: { 'title': 'Chưa xác nhận', 'class': 'kt-badge--warning' },
                            2: { 'title': 'Đã xác nhận', 'class': ' kt-badge--success' },
                            3: { 'title': 'Đã từ chối', 'class': ' kt-badge--danger' },
                            4: { 'title': 'Đợi xử lý', 'class': ' kt-badge--info' },
                            5: { 'title': 'Đã thu hồi', 'class': ' kt-badge--success' },
                        };
                        if (data.status) {
                            return '<span class="kt-badge ' + states[data.status].class + ' kt-badge--inline kt-badge--pill">' + states[data.status].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới tạo</span>';
                        }
                    }
                },
                {
                    field: 'time_created',
                    title: 'Thời gian',
                    width: 100
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 50,
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        return `<button data-name="${data.type.type_Name}" data-id="${data.uuid}" data-status="${data.status}" type="button" title="Chi tiết" class="btn btn-sm btn-primary btn-icon btn-icon-md js-detail-messenger">
                                    <i class="la la-info"></i>
                            </button>`;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    });

    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var initSearch = function () {
        $('#js-filter-keyword--ticketimport')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });
        $('#js-filter-typecard')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val(), 'type');
                    START_PAGE = 1;
                }
            }).selectpicker();
        $('#js-filter-typeiore')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val(), 'type_of_ballot');
                    START_PAGE = 1;
                }
            }).selectpicker();

        $('#js-filter-keyword--ticketexport')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });

        $('#js-filter-typecard_export')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val(), 'type');
                    START_PAGE = 1;
                }
            }).selectpicker();
        $('#js-filter-typeiore_export')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val(), 'type_of_ballot');
                    START_PAGE = 1;
                }
            }).selectpicker();
        //-----------------------------------------
        $('#js-filter-keyword--messenger')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase().trim(), 'keyword');
                    START_PAGE = 1;
                }
            });

        //$('#js-filter-type_storage')
        //    .on('change', function () {
        //        if (dataTableInstance) {
        //            dataTableInstance.search($(this).val(), 'type');
        //            START_PAGE = 1;
        //        }
        //    }).selectpicker();
        $('#js-filter-state_exportto')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val(), 'status');
                    START_PAGE = 1;
                }
            }).selectpicker();
    };

    var detailImportTicket = function (id, name) {
        if (name != "null") {
            var title = `<span class="text-uppercase">Phiếu nhập kho từ ${name}</span>`;
        }
        else {
            var title = `<span class="text-uppercase">Phiếu nhập kho</span>`;
        }
        drawerim = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--detailimportticket' });

        drawerim.on('ready', () => {
            $.ajax({
                url: `/bunker/detailimportticket`,
                method: 'GET',
                data: { import_uuid: id }
            })
                .done(function (response) {
                    debugger
                    const $content = drawerim.setBody(response);
                    $(".importdetail").on("click", function () {
                        debugger
                        var importUuid = $('.importUuid').val()

                        if (dataTableDetailIm != null) dataTableDetailIm.destroy();
                        
                        if ($('.dmms-form-bunker__importticket').find('.kt-datatableim').length > 0) {
                            dataTableDetailIm = $('.kt-datatableim').KTDatatable({
                                data: {
                                    type: 'remote',
                                    source: {
                                        read: {
                                            url: `/bunker/DetailCategoriesImport/${importUuid}`,
                                        },
                                        response: {
                                            map: function (res) {
                                                const raw = res && res.data || {};
                                                const { pagination } = raw;
                                                const perpage = pagination === undefined ? undefined : pagination.perpage;
                                                const page = pagination === undefined ? undefined : pagination.page;
                                                START_PAGE = 0;
                                                return {
                                                    data: raw.items || [],
                                                    meta: {
                                                        ...pagination || {},
                                                        perpage: perpage || LIMIT,
                                                        page: page || 1,
                                                    },
                                                }
                                            },
                                        },
                                        filter: function (data = {}) {

                                            const query = data.query || {};
                                            const pagination = data.pagination || {};
                                            if (START_PAGE === 1) {
                                                pagination.page = START_PAGE;
                                            }
                                            return {

                                                limit: pagination.perpage || LIMIT,
                                                page: pagination.page
                                            };
                                        }
                                    },
                                    pageSize: LIMIT,
                                    serverPaging: true,
                                    serverFiltering: true,
                                },
                                layout: {
                                    scroll: true
                                },
                                search: {
                                    onEnter: true,
                                },
                                columns: [
                                    {
                                        field: 'uuid',
                                        title: '#',
                                        sortable: false,
                                        textAlign: 'center',
                                        width: 20,
                                        template: function (data, row) {
                                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                                        },
                                    },
                                    {
                                        field: 'name',
                                        title: 'Tên vật tư-thiết bị',
                                        template: function (data) {
                                            var output = '';
                                            if (data.prefixName) {
                                                output += '<strong class="text-primary">' + data.prefixName + '</strong></br>';
                                            }
                                            output += '<span>' + data.name + '</span>';
                                            return output;
                                        },
                                    },
                                    {
                                        field: 'type',
                                        title: 'Phân loại',
                                        width: 50,
                                        autoHide: false,
                                        template: function (data) {
                                            if (data.type == 1) {
                                                return "Thiết bị";
                                            }
                                            else if (data.type == 2) {
                                                return "Vật tư";
                                            }
                                            else if (data.type == 3) {
                                                return "Dụng cụ";
                                            }
                                        },
                                    },

                                    {
                                        field: 'documentQuantity',
                                        title: 'SL theo CT',
                                        autoHide: false,
                                        width: 70
                                    },
                                    {
                                        field: 'realQuantity',
                                        title: 'SL thực',
                                        autoHide: false,
                                        width: 70
                                    },
                                    {
                                        field: 'unitPrice',
                                        title: 'Đơn giá',
                                        autoHide: false,
                                        width: 80,
                                        template: function (data) {
                                            if (data.unitPrice != null) {
                                                var formatter = new Intl.NumberFormat('vi-VN', {
                                                    style: 'currency',
                                                    currency: 'VND',
                                                    minimumFractionDigits: 0
                                                })
                                                return formatter.format(data.unitPrice)
                                            }

                                        }

                                    },
                                    {
                                        field: '#4',
                                        title: 'Thành tiền',
                                        autoHide: false,
                                        width: 110,
                                        autoHide: false,
                                        template: function (data) {
                                            if (data.unitPrice) {
                                                var formatter = new Intl.NumberFormat('vi-VN', {
                                                    style: 'currency',
                                                    currency: 'VND',
                                                    minimumFractionDigits: 0
                                                })
                                                return formatter.format(data.unitPrice * data.documentQuantity)
                                            }
                                        }
                                    },
                                    {
                                        field: 'Tác vụ',
                                        title: 'Tác vụ',
                                        sortable: false,
                                        width: 80,
                                        autoHide: false,
                                        textAlign: 'center',
                                        template: function (data) {
                                            var output = "";
                                            if (data.type != 1) {
                                                output = `<button data-id="${data.id}" data-importUuid =${importUuid} type="button" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md" disabled>
                                                     <i class="la la-edit"></i>
                                                    </button>`;
                                            }
                                            else {
                                                output = `<button data-id="${data.id}" data-importUuid =${importUuid} type="button" title="Chỉnh sửa" class="js-detail-import-device btn btn-sm btn-primary btn-icon btn-icon-md">
                                                     <i class="la la-edit"></i>
                                                    </button>`;
                                            }
                                            return output;
                                        },
                                    }],
                                sortable: false,
                                pagination: true,
                                responsive: true,
                            });
                        }
                        else {
                            dataTableDetailIm = $('.kt-datatableim_nottype2').KTDatatable({
                                data: {
                                    type: 'remote',
                                    source: {
                                        read: {
                                            url: `/bunker/DetailCategoriesImport/${importUuid}`,
                                        },
                                        response: {
                                            map: function (res) {
                                                const raw = res && res.data || {};
                                                const { pagination } = raw;
                                                const perpage = pagination === undefined ? undefined : pagination.perpage;
                                                const page = pagination === undefined ? undefined : pagination.page;
                                                START_PAGE = 0;
                                                return {
                                                    data: raw.items || [],
                                                    meta: {
                                                        ...pagination || {},
                                                        perpage: perpage || LIMIT,
                                                        page: page || 1,
                                                    },
                                                }
                                            },
                                        },
                                        filter: function (data = {}) {

                                            const query = data.query || {};
                                            const pagination = data.pagination || {};
                                            if (START_PAGE === 1) {
                                                pagination.page = START_PAGE;
                                            }
                                            return {

                                                limit: pagination.perpage || LIMIT,
                                                page: pagination.page
                                            };
                                        }
                                    },
                                    pageSize: LIMIT,
                                    serverPaging: true,
                                    serverFiltering: true,
                                },
                                layout: {
                                    scroll: true
                                },
                                search: {
                                    onEnter: true,
                                },
                                columns: [
                                    {
                                        field: 'uuid',
                                        title: '#',
                                        sortable: false,
                                        textAlign: 'center',
                                        width: 20,
                                        template: function (data, row) {
                                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                                        },
                                    },
                                    {
                                        field: 'name',
                                        title: 'Tên vật tư-thiết bị',
                                        template: function (data) {
                                            var output = '';
                                            if (data.prefixName) {
                                                output += '<strong class="text-primary">' + data.prefixName + '</strong></br>';
                                            }
                                            output += '<span>' + data.name + '</span>';
                                            return output;
                                        },
                                    },
                                    {
                                        field: 'type',
                                        title: 'Phân loại',
                                        width: 50,
                                        autoHide: false,
                                        template: function (data) {
                                            if (data.type == 1) {
                                                return "Thiết bị";
                                            }
                                            else if (data.type == 2) {
                                                return "Vật tư";
                                            }
                                            else if (data.type == 3) {
                                                return "Dụng cụ";
                                            }
                                        },
                                    },

                                    {
                                        field: 'documentQuantity',
                                        title: 'SL theo CT',
                                        autoHide: false,
                                        width: 70
                                    },
                                    {
                                        field: 'realQuantity',
                                        title: 'SL thực',
                                        autoHide: false,
                                        width: 70
                                    },
                                    {
                                        field: 'Tác vụ',
                                        title: 'Tác vụ',
                                        sortable: false,
                                        width: 80,
                                        autoHide: false,
                                        textAlign: 'center',
                                        template: function (data) {
                                            var output = "";
                                            if (data.type != 1) {
                                                output = `<button data-id="${data.id}" data-importUuid =${importUuid} type="button" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md" disabled>
                                                     <i class="la la-edit"></i>
                                                    </button>`;
                                            }
                                            else {
                                                output = `<button data-id="${data.id}" data-importUuid =${importUuid} type="button" title="Chỉnh sửa" class="js-detail-import-device btn btn-sm btn-primary btn-icon btn-icon-md">
                                                     <i class="la la-edit"></i>
                                                    </button>`;
                                            }
                                            return output;
                                        },
                                    }],
                                sortable: false,
                                pagination: true,
                                responsive: true,
                            });
                        }
                    });
                    //initContent($content);
                    $('.kt-close').on('click', function () {
                        drawerim.close();
                    });
                })
                .fail(function () {
                    drawer.error();
                });
        });
        drawerim.open();
    };
    var detailExportTicket = function (id, name) {
        if (name != "null") {
            var title = `<span class="text-uppercase">Phiếu xuất kho từ ${name}</span>`;
        }
        else {
            var title = `<span class="text-uppercase">Phiếu xuất kho</span>`;
        }
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--detailexportticket' });

        drawer.on('ready', () => {
            $.ajax({
                url: `/bunker/DetailExportDetail`,
                method: 'GET',
                data: { export_uuid: id }
            })
                .done(function (response) {
                    debugger
                    const $content = drawer.setBody(response);
                    $(".exportdetail").on("click", function () {

                        var exportUuid = $('.exportUuid').val()

                        //if (dataTableInstancepackage != null) dataTableInstancepackage.destroy();
                        if ($('.dmms-form-bunker__exportticket').find('.kt-datatableex').length > 0) {
                            dataTableInstanceEx = $('.kt-datatableex').KTDatatable({
                                data: {
                                    type: 'remote',
                                    source: {
                                        read: {
                                            url: `/bunker/DetailCategoriesExport/${exportUuid}`,
                                        },
                                        response: {
                                            map: function (res) {
                                                const raw = res && res.data || {};
                                                const { pagination } = raw;
                                                const perpage = pagination === undefined ? undefined : pagination.perpage;
                                                const page = pagination === undefined ? undefined : pagination.page;
                                                START_PAGE = 0;
                                                return {
                                                    data: raw.items || [],
                                                    meta: {
                                                        ...pagination || {},
                                                        perpage: perpage || LIMIT,
                                                        page: page || 1,
                                                    },
                                                }
                                            },
                                        },
                                        filter: function (data = {}) {

                                            const query = data.query || {};
                                            const pagination = data.pagination || {};

                                            if (START_PAGE === 1) {
                                                pagination.page = START_PAGE;
                                            }
                                            return {

                                                limit: pagination.perpage || LIMIT,
                                                page: pagination.page
                                            };
                                        }
                                    },
                                    pageSize: LIMIT,
                                    serverPaging: true,
                                    serverFiltering: true,
                                },
                                layout: {
                                    scroll: true
                                },
                                search: {
                                    onEnter: true,
                                },
                                columns: [
                                    {
                                        field: 'uuid',
                                        title: '#',
                                        sortable: false,
                                        textAlign: 'center',
                                        width: 20,
                                        template: function (data, row) {
                                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                                        },
                                    },
                                    {
                                        field: 'name',
                                        title: 'Tên vật tư-thiết bị',
                                        template: function (data) {
                                            var output = '';
                                            if (data.prefixName) {
                                                output += '<strong class="text-primary">' + data.prefixName + '</strong></br>';
                                            }
                                            output += '<span>' + data.name + '</span>';
                                            return output;
                                        },
                                    },
                                    {
                                        field: 'type',
                                        title: 'Phân loại',
                                        width: 70,
                                        autoHide: false,
                                        template: function (data) {
                                            if (data.type == 1) {
                                                return "Thiết bị";
                                            }
                                            else if (data.type == 2) {
                                                return "Vật tư";
                                            }
                                            else if (data.type == 3) {
                                                return "Dụng cụ";
                                            }
                                        },
                                    },
                                    {
                                        field: 'documentQuantity',
                                        title: 'SL theo CT',
                                        width: 50,
                                        autoHide: false,
                                    },
                                    {
                                        field: 'realQuantity',
                                        title: 'SL thực',
                                        width: 50,
                                        autoHide: false,
                                    },
                                    {
                                        field: 'unitPrice',
                                        title: 'Đơn giá',
                                        autoHide: false,
                                        width: 120,
                                        autoHide: false,
                                        template: function (data) {
                                            return new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(data.unitPrice);
                                        }
                                    },
                                    {
                                        field: '#1',
                                        title: 'Thành tiền',
                                        autoHide: false,
                                        width: 120,
                                        autoHide: false,
                                        template: function (data) {
                                            return new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(data.realQuantity * data.unitPrice);
                                        }
                                    },

                                    {
                                        field: 'Tác vụ',
                                        title: 'Tác vụ',
                                        sortable: false,
                                        width: 100,
                                        autoHide: false,
                                        textAlign: 'center',
                                        template: function (data) {
                                            var output = ''
                                            if (data.type == 1) {
                                                output = `<button data-id="${data.id}" data-exportUuid="${exportUuid}" type="button" title="Chỉnh sửa" class="js-detail-export-device btn btn-sm btn-primary btn-icon btn-icon-md">
                                                             <i class="la la-edit" style="background-color: transparent;"></i>
                                                            </button>`
                                            }
                                            else {
                                                output = `<button data-id="${data.id}" data-exportUuid="${exportUuid}" type="button" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md" disabled>
                                                             <i class="la la-edit" style="background-color: transparent;"></i>
                                                            </button>`
                                            }
                                            return output;
                                        }
                                    }],
                                sortable: false,
                                pagination: true,
                                responsive: true,
                            });
                        }
                        else {
                            dataTableInstanceEx = $('.kt-datatableex_nottype4').KTDatatable({
                                data: {
                                    type: 'remote',
                                    source: {
                                        read: {
                                            url: `/bunker/DetailCategoriesExport/${exportUuid}`,
                                        },
                                        response: {
                                            map: function (res) {
                                                const raw = res && res.data || {};
                                                const { pagination } = raw;
                                                const perpage = pagination === undefined ? undefined : pagination.perpage;
                                                const page = pagination === undefined ? undefined : pagination.page;
                                                START_PAGE = 0;
                                                return {
                                                    data: raw.items || [],
                                                    meta: {
                                                        ...pagination || {},
                                                        perpage: perpage || LIMIT,
                                                        page: page || 1,
                                                    },
                                                }
                                            },
                                        },
                                        filter: function (data = {}) {

                                            const query = data.query || {};
                                            const pagination = data.pagination || {};

                                            if (START_PAGE === 1) {
                                                pagination.page = START_PAGE;
                                            }
                                            return {

                                                limit: pagination.perpage || LIMIT,
                                                page: pagination.page
                                            };
                                        }
                                    },
                                    pageSize: LIMIT,
                                    serverPaging: true,
                                    serverFiltering: true,
                                },
                                layout: {
                                    scroll: true
                                },
                                search: {
                                    onEnter: true,
                                },
                                columns: [
                                    {
                                        field: 'uuid',
                                        title: '#',
                                        sortable: false,
                                        textAlign: 'center',
                                        width: 20,
                                        template: function (data, row) {
                                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                                        },
                                    },
                                    {
                                        field: 'name',
                                        title: 'Tên vật tư-thiết bị',
                                        template: function (data) {
                                            var output = '';
                                            if (data.prefixName) {
                                                output += '<strong class="text-primary">' + data.prefixName + '</strong></br>';
                                            }
                                            output += '<span>' + data.name + '</span>';
                                            return output;
                                        },
                                    },
                                    {
                                        field: 'type',
                                        title: 'Phân loại',
                                        width: 70,
                                        autoHide: false,
                                        template: function (data) {
                                            if (data.type == 1) {
                                                return "Thiết bị";
                                            }
                                            else if (data.type == 2) {
                                                return "Vật tư";
                                            }
                                            else if (data.type == 3) {
                                                return "Dụng cụ";
                                            }
                                        },
                                    },
                                    {
                                        field: 'documentQuantity',
                                        title: 'SL theo CT',
                                        width: 60,
                                        autoHide: false,
                                    },
                                    {
                                        field: 'realQuantity',
                                        title: 'SL thực',
                                        width: 60,
                                        autoHide: false,
                                    },
                                    {
                                        field: 'Tác vụ',
                                        title: 'Tác vụ',
                                        sortable: false,
                                        width: 100,
                                        autoHide: false,
                                        textAlign: 'center',
                                        template: function (data) {
                                            
                                            var output = ''
                                            if (data.type == 1) {
                                                output = `<button data-id="${data.id}" data-exportUuid="${exportUuid}" type="button" title="Chỉnh sửa" class="js-detail-export-device btn btn-sm btn-primary btn-icon btn-icon-md">
                                                             <i class="la la-edit" style="background-color: transparent;"></i>
                                                            </button>`
                                            }
                                            else {
                                                output = `<button data-id="${data.id}" data-exportUuid="${exportUuid}" type="button" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md" disabled>
                                                             <i class="la la-edit" style="background-color: transparent;"></i>
                                                            </button>`
                                            }
                                            return output;
                                        },
                                    }],
                                sortable: false,
                                pagination: true,
                                responsive: true,
                            });
                        }
                    });
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });
        drawer.open();
    };

    var aceptImport = function (id, name) {
        if (name != "null") {
            var title = `<span class="text-uppercase">Phiếu nhập kho ${name}</span>`;
        }
        else {
            var title = `<span class="text-uppercase">Phiếu nhập kho</span>`;
        }
        drawerInner = new KTDrawer({ title, id: `acept_${id}` || 'new', className: 'dmms-drawer--aceptimport' });

        drawerInner.on('ready', () => {
            
            $.ajax({
                url: `/bunker/MessengerImport/${id}`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawerInner.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawerInner.error();
                });
        });
        drawerInner.open();
    };

    var detailMessenger = function (id, name, status) {
        if (name != "null") {
            var title = `<span class="text-uppercase">Phiếu xuất kho ${name}</span>`;
        }
        else {
            var title = `<span class="text-uppercase">Phiếu xuất kho</span>`;
        }
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--detailmessenger' });

        drawer.on('ready', () => {
            $.ajax({
                url: `/bunker/DetailMessengerImportDetail`,
                method: 'GET',
                data: { export_uuid: id }
            })
                .done(function (response) {
                    debugger
                    const $content = drawer.setBody(response);
                    $(".exportdetail").on("click", function () {
                        var exportUuid = $('.exportUuid').val()

                        dataTableInstanceEx = $('.kt-datatable--messenger').KTDatatable({
                            data: {
                                type: 'remote',
                                source: {
                                    read: {
                                        url: `/bunker/DetailCategoriesExport/${exportUuid}`,
                                    },
                                    response: {
                                        map: function (res) {
                                            const raw = res && res.data || {};
                                            const { pagination } = raw;
                                            const perpage = pagination === undefined ? undefined : pagination.perpage;
                                            const page = pagination === undefined ? undefined : pagination.page;
                                            START_PAGE = 0;
                                            return {
                                                data: raw.items || [],
                                                meta: {
                                                    ...pagination || {},
                                                    perpage: perpage || LIMIT,
                                                    page: page || 1,
                                                },
                                            }
                                        },
                                    },
                                    filter: function (data = {}) {

                                        const query = data.query || {};
                                        const pagination = data.pagination || {};

                                        if (START_PAGE === 1) {
                                            pagination.page = START_PAGE;
                                        }
                                        return {

                                            limit: pagination.perpage || LIMIT,
                                            page: pagination.page
                                        };
                                    }
                                },
                                pageSize: LIMIT,
                                serverPaging: true,
                                serverFiltering: true,
                            },
                            layout: {
                                scroll: true
                            },
                            search: {
                                onEnter: true,
                            },
                            columns: [
                                {
                                    field: 'uuid',
                                    title: '#',
                                    sortable: false,
                                    textAlign: 'center',
                                    width: 20,
                                    template: function (data, row) {
                                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                                    },
                                },
                                {
                                    field: 'name',
                                    title: 'Tên vật tư-thiết bị',
                                    template: function (data) {
                                        var output = '';
                                        if (data.prefixName) {
                                            output += '<strong class="text-primary">' + data.prefixName + '</strong></br>';
                                        }
                                        output += '<span>' + data.name + '</span>';
                                        return output;
                                    },
                                },
                                {
                                    field: 'type',
                                    title: 'Phân loại',
                                    width: 70,
                                    autoHide: false,
                                    template: function (data) {
                                        if (data.type == 1) {
                                            return "Thiết bị";
                                        }
                                        else if (data.type == 2) {
                                            return "Vật tư";
                                        }
                                        else if (data.type == 3) {
                                            return "Dụng cụ";
                                        }
                                    },
                                },
                                {
                                    field: 'documentQuantity',
                                    title: 'SL theo chứng từ',
                                    width: 60,
                                    autoHide: false,
                                },
                                {
                                    field: 'realQuantity',
                                    title: 'SL thực',
                                    width: 60,
                                    autoHide: false,
                                },
                                {
                                    field: 'Tác vụ',
                                    title: 'Tác vụ',
                                    sortable: false,
                                    width: 100,
                                    autoHide: false,
                                    textAlign: 'center',
                                    template: function (data) {
                                        if (data.type == 1) {
                                            return `
                            <button data-id="${data.id}" data-exportUuid = "${exportUuid}"type="button" title="Chỉnh sửa" class="js-detail-export-device btn btn-sm btn-primary btn-icon btn-icon-md">
                             <i class="la la-edit" style="background-color: transparent;"></i>
                            </button>`;
                                        }
                                        else {
                                            return `
                            <button data-id="${data.id}" data-exportUuid = "${exportUuid}"type="button" title="Chỉnh sửa" class="js-detail-export-device btn btn-sm btn-primary btn-icon btn-icon-md" disabled>
                             <i class="la la-edit" style="background-color: transparent;"></i>
                            </button>`;
                                        }
                                    },
                                }],
                            sortable: false,
                            pagination: true,
                            responsive: true,
                        });

                    });
                    initContent($content);

                    if ($('#state').val() != 1 || (status != 1 && status != 4)) {
                        $('#js-acept_import').attr('disabled', true);
                        $('#js-reject_import').attr('disabled', true);
                    }
                    $('#js-acept_import').off('click').on('click', function () {
                        drawer.close();
                        aceptImport(id, name);
                    });
                    $('#js-reject_import').off('click').on('click', function () {
                        KTApp.swal({
                            title: 'Xác nhận',
                            text: 'Xác nhận từ chối.',
                            icon: 'warning',
                            dangerMode: true,
                        }).then((confirm) => {
                            if (confirm) {
                                rejectExport(id, name);
                            }
                        });
                    });
                })
                .fail(function () {
                    drawer.error();
                });
        });
        drawer.open();
    };

    var rejectExport = function (export_uuid, name) {
        if (export_uuid) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang từ chối...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/bunker/rejectexport/${export_uuid}`,
                method: 'GET',
            })
                .done(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify("Từ chối thành công!", { type: 'success' });

                    drawer.close();
                    reloadTable();
                })
                .fail(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(data.responseJSON.msg, { type: 'danger' });
                });
        }
    }

    var detailImportDevice = function (categoryId, importUuid) {
        var title = `<span class="text-uppercase">Danh sách chi tiết thiết bị</span>`;

        drawerDetail = new KTDrawer({ title, id: null || 'new', className: 'dmms-drawer--detailimportticket' });

        drawerDetail.on('ready', () => {
            $.ajax({
                url: `/bunker/DetailSupplies`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawerDetail.setBody(response);
                    dataTableInstanceImDevice = $('.kt-datatable--detailsuppliesdevice').KTDatatable({
                        data: {
                            type: 'remote',
                            source: {
                                read: {
                                    url: `/bunker/DetailDeviceIm`,
                                },
                                response: {
                                    map: function (res) {
                                        debugger
                                        const raw = res && res.data || {};
                                        const { pagination } = raw;
                                        const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                                        const page = pagination === undefined ? 0 : pagination.page;
                                        CURRENT_PAGE = page || 1;
                                        LIMIT = perpage;
                                        START_PAGE = 0;
                                        return {
                                            data: raw.items || [],
                                            meta: {
                                                ...pagination || {},
                                                perpage: perpage || LIMIT,
                                                page: page || 1,
                                            },
                                        }
                                    },
                                },
                                filter: function (data = {}) {
                                    debugger
                                    const query = data.query || {};
                                    const pagination = data.pagination || {};
                                    if (START_PAGE === 1) {
                                        pagination.page = START_PAGE;
                                    }
                                    return {
                                        limit: pagination.perpage || LIMIT,
                                        page: pagination.page,
                                        categoryId: categoryId,
                                        importUuid: importUuid
                                    }
                                }
                            },
                            pageSize: LIMIT,
                            serverPaging: true,
                            serverFiltering: true,
                        },

                        search: {
                            onEnter: true,

                        },
                        columns: [
                            {
                                field: 'uuid',
                                title: '#',
                                width: 30,
                                textAlign: 'center',
                                template: function (data, row) {
                                    return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                                },
                                responsive: {
                                    visible: 'md',
                                    hidden: 'sm'
                                }
                            },
                            {
                                field: 'name',
                                title: 'Tên thiết bị',
                                template: function (data) {
                                    var output = '';
                                    if (data.prefixName) {
                                        output += '<strong class="text-primary">' + data.prefixName + '</strong></br>';
                                    }
                                    output += '<span>' + data.name + '</span>';
                                    return output;
                                },
                            },
                            {
                                field: 'serial',
                                title: 'Serial',
                                width: 150
                            },
                            //{
                            //    field: 'imei',
                            //    title: 'Imei',
                            //    width: 150
                            //},
                            {
                                field: 'timecreatedformat',
                                title: 'Ngày hết hạn',
                                width: 120
                            }

                        ],
                        sortable: false,
                        pagination: true,
                        responsive: true,
                    });
                    //initContent($content);
                })
                .fail(function () {
                    drawerDetail.error();
                });
        });
        drawerDetail.open();
    };
    var detailExportDevice = function (categoryId, exportUuid) {

        var title = `<span class="text-uppercase">Danh sách chi tiết thiết bị</span>`;



        drawerDetail = new KTDrawer({ title, id: null || 'new', className: 'dmms-drawer--detailimportticket' });

        drawerDetail.on('ready', () => {
            $.ajax({
                url: `/bunker/DetailSupplies`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawerDetail.setBody(response);
                    dataTableInstanceImDevice = $('.kt-datatable--detailsuppliesdevice').KTDatatable({
                        data: {
                            type: 'remote',
                            source: {
                                read: {
                                    url: `/bunker/DetailDeviceEx`,
                                },
                                response: {
                                    map: function (res) {
                                        debugger
                                        const raw = res && res.data || {};
                                        const { pagination } = raw;
                                        const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                                        const page = pagination === undefined ? 0 : pagination.page;
                                        CURRENT_PAGE = page || 1;
                                        LIMIT = perpage;
                                        START_PAGE = 0;
                                        return {
                                            data: raw.items || [],
                                            meta: {
                                                ...pagination || {},
                                                perpage: perpage || LIMIT,
                                                page: page || 1,
                                            },
                                        }
                                    },
                                },
                                filter: function (data = {}) {
                                    debugger
                                    const query = data.query || {};
                                    //const { categoryId, exportUuid } = query
                                    const pagination = data.pagination || {};
                                    if (START_PAGE === 1) {
                                        pagination.page = START_PAGE;
                                    }
                                    return {
                                        limit: pagination.perpage || LIMIT,
                                        page: pagination.page,
                                        categoryId: categoryId,
                                        exportUuid: exportUuid,
                                    }
                                }
                            },
                            pageSize: LIMIT,
                            serverPaging: true,
                            serverFiltering: true,
                        },

                        search: {
                            onEnter: true,

                        },
                        columns: [
                            {
                                field: 'uuid',
                                title: '#',
                                width: 30,
                                textAlign: 'center',
                                template: function (data, row) {
                                    return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                                },
                                responsive: {
                                    visible: 'md',
                                    hidden: 'sm'
                                }
                            },
                            {
                                field: 'name',
                                title: 'Tên thiết bị',
                                template: function (data) {
                                    var output = '';
                                    if (data.prefixName) {
                                        output += '<strong class="text-primary">' + data.prefixName + '</strong></br>';
                                    }
                                    output += '<span>' + data.name + '</span>';
                                    return output;
                                },
                            },
                            {
                                field: 'serial',
                                title: 'Serial',
                                width: 150
                            },
                            //{
                            //    field: 'imei',
                            //    title: 'Imei',
                            //    width: 150
                            //},
                            {
                                field: 'timecreatedformat',
                                title: 'Ngày hết hạn',
                                width: 120
                            }
                        ],
                        sortable: false,
                        pagination: true,
                        responsive: true,
                    });
                    //initContent($content);
                })
                .fail(function () {
                    drawerDetail.error();
                });
        });
        drawerDetail.open();
    };
    var detailSuppliesInImportTicket = function (id) {
        const title = '<span class="text-uppercase">Danh sách chi tiết thiết bị</span>';
        const category_id = id;
        subDrawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--detailimportticket--inner' });

        subDrawer.on('ready', () => {
            $.ajax({
                url: `/bunker/DetailSuppliesAjax/${category_id}`,
                method: 'GET',
            })
                .done(function (response) {
                    var content = `<div class="add-detail-supplies">
                    <table class="table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Tên vật tư-thiết bị</th>
                                <th scope="col">Serial</th>
                                <th scope="col">Ngày hết hạn BH</th>
                            </tr>
                        </thead>
                        <tbody class="wrapper-fill">`;
                    if (response.data != null) {
                        $.each(response.data, function (index, item) {
                            content += `<tr>
                                <td>${index + 1}</td>
                                <td>
                                    <span class="text-primary">${item.prefix_name}</span><br/>
                                    <span>${item.name_display}</span>
                                </td>
                                <td>SERIAL</td>
                                <td>22/04/2020</td>
                            </tr>`
                        });
                    }
                    content += `</tbody>
                                </table>
                                <div class="text-right kt-margin-t-10">
                                    <button type="button" class="btn btn-danger kt-close">
                                        Đóng
                                    </button>
                                </div>
                            </div>`;
                    subDrawer.setBody(content);
                    //initContent($content);
                    $('.kt-close').on('click', function () {
                        subDrawer.close();
                    });
                })
                .fail(function () {
                    subDrawer.error();
                });
        });
        subDrawer.open();
    }

    var rollbackDevice = function (export_uuid, name) {
        if (export_uuid) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang thu hồi thiết bị...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/bunker/rollbackdevice`,
                method: 'GET',
                data: { export_uuid: export_uuid }
            })
                .done(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(`Thu hồi thiết bị từ ${name} thành công!`, { type: 'success' });

                    if (dataTableInstance) {
                        dataTableInstance.reload();
                    }
                })
                .fail(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(data.responseJson.msg, { type: 'danger' });
                });
        }
    }

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-detail-ticketimport')
            .on('click', '.js-detail-ticketimport', function () {
                const id = $(this).attr('data-id');
                const name = $(this).attr('data-name');

                detailImportTicket(id, name);
            })
            .off('click', '.js-detail-importticket_supplies')
            .on('click', '.js-detail-importticket_supplies', function () {
                const id = $(this).attr('data-id');

                detailSuppliesInImportTicket(id);
            })
            .off('click', '.js-detail-ticketexport')
            .on('click', '.js-detail-ticketexport', function () {
                const id = $(this).attr('data-id');
                const name = $(this).attr('data-name');
                detailExportTicket(id, name);
            })
            .off('click', '.js-detail-messenger')
            .on('click', '.js-detail-messenger', function () {
                const id = $(this).attr('data-id');
                const name = $(this).attr('data-name');
                const status = $(this).attr('data-status');

                detailMessenger(id, name, status);
            })
            .off('click', '.js-detail-import-device')
            .on('click', '.js-detail-import-device', function () {
                const categoryId = $(this).attr('data-id');
                const importUuid = $(this).attr('data-importUuid');
                detailImportDevice(categoryId, importUuid);
            })
            .off('click', '.js-detail-export-device')
            .on('click', '.js-detail-export-device', function () {
                const categoryId = $(this).attr('data-id');
                const exportUuid = $(this).attr('data-exportUuid');
                detailExportDevice(categoryId, exportUuid);
            })
            .off('click', '.js-rollback_device')
            .on('click', '.js-rollback_device', function () {
                const $this = $(this);
                const export_uuid = $this.attr('data-id');
                const name = $this.attr('data-name');

                KTApp.swal({
                    title: `Thu hồi thiết bị từ ${name}`,
                    text: `Chọn xác nhận để hoàn thành tác vụ thu hồi thiết bị!`,
                    icon: 'info',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        rollbackDevice(export_uuid, name);
                    }
                });
            });
    };

    var initFormAceptImport = function ($formExport) {   //Save export
        $formExport.validate({
            rules: {
                'detail[][real_quantity]': {
                    required: true
                },
            },
            //messages: {
            //    'detail[][real_quantity]': {
            //        required: "Vui lòng nhập số lượng thực!"
            //    },
            //},
            errorPlacement: function (error, element) {
                if (element.val() === '') {
                    $.notify('Vui lòng nhập số lượng thực!', { type: 'warning' });
                }
            },

            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($formExport, -200);
            },
            submitHandler: function (form) {
                $(form).find('button[type="submit"]').prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading

                const data = $(form).serializeJSON() || {};
                $.ajax({
                    url: `/bunker/importsupplies`,
                    method: 'POST',
                    data,
                })
                    .done(function () {
                        KTApp.block($formExport);
                        $.notify('Thêm thông tin thành công!', { type: 'success' });

                        /**
                         * Close drawer
                         */
                        drawerInner.close();
                        reloadTable();
                    })
                    .fail(function (data) {
                        KTApp.unblock($formExport);
                        $(form).find('button[type="submit"]').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
                        $.notify(data.responseJSON.msg, { type: 'danger' });
                    });
                return false;
            },
        });
    };

    var initFormMess = function ($form, category_id) {
        $form.validate({
            rules: {

            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {
                $form.find('button[type="submit"]').prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading

                const data = $(form).serializeJSON() || {};
                const wrapper = $(`.wrapper-fill tr[id=${category_id}]`);
                //const money_string = wrapper.find('input[name="detail[][unit_price_string]"]').val();
                //const money = parseInt(money_string.replace(/,/g, ''));
                var quantity_real = 0;
                wrapper.find('.list_device').html('');
                if (data != {}) {
                    $.each(data.detail[0].device || [], function (index, item) {
                        if (item.check) {
                            quantity_real++;
                            wrapper.find(`.list_device`).append(`<input name="detail[][device[][uuid]]" value="${item.uuid}" hidden />
                                <input name="detail[][device[][serial]]" value="${item.serial}" hidden />
                                <input name="detail[][device[][expire_device_string]]" value="${item.expire_device_string}" hidden />`);
                        }
                    });
                    wrapper.find('.real_quantity').html('').html(quantity_real);
                    wrapper.find('input[name="detail[][real_quantity]"]').val(quantity_real);
                    //wrapper.find(`.money_string`).html(quantity_real * money);

                    //$('.money_string').simpleMoneyFormat();
                    /**
                    * Close drawer
                    */
                    subDrawer.close();
                }
                $.notify('Đã thêm chi tiết!', { type: 'success' });
                $form.find('button[type="submit"]').prop('disabled', false).addClass('kt-spinner kt-spinner--light'); //loading

                return false;
            },
        });
    }

    var initSaveDetailInMess = function ($content, category_id) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-form-bunker__importinmessenger');
        initFormMess($form, category_id);

        $('.check-all').on('change', function () {
            const isCheckAll = $(this).find('input[type="checkbox"]').prop('checked');
            var checkboxInBody = $(".wrapper-fill").find('input[type="checkbox"]');

            if (isCheckAll === true) {
                checkboxInBody.prop('checked', true);
            }
            else {
                checkboxInBody.prop('checked', false);
            }
        });

        $('.kt-close').on('click', function () {
            subDrawer.close();
        });
    }

    var addDetailImportSupplies = function (exportUuid, categoryId) {
        
        const wrapper = $(`.wrapper-fill tr[id="${categoryId}"]`);
        var devices = wrapper.find('.list_device input').serializeJSON();
        var title = `<span class="text-uppercase">Danh sách chi tiết thiết bị</span>`;

        subDrawer = new KTDrawer({ title, id: exportUuid || 'new', className: 'dmms-drawer--importdetailinmess' });

        subDrawer.on('ready', () => {
            $.ajax({
                async: false,
                url: `/bunker/DetailDeviceEx`,
                method: 'GET',
                data: { exportUuid: exportUuid, categoryId: categoryId, limit: -1 }
            })
                .done(function (data) {
                    var response = `<form class="dmms-form-bunker__importinmessenger" onsubmit="return false">
                    <div class="add-detail-supplies_inner">
                        <table class="table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">                                
                                        <div class="kt-checkbox-inline check-all" data-all="false" style="margin-top:-20px">
                                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--light">
                                                <input type="checkbox">
                                                <span></span>
                                            </label>
                                        </div>
                                    </th>
                                    <th scope="col">Tên thiết bị</th>
                                    <th scope="col">Serial</th>
                                    <th scope="col">Ngày hết hạn BH</th>
                                </tr>
                            </thead>`;
                    $.each(data.data.items, function (index, item) {
                        response += `<tbody class="wrapper-fill">             
                                        <tr id="${item.uuid}">
                                            <td>
                                                <div class="kt-checkbox-inline" style="margin-top:-20px">
                                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">`;

                        if (devices.detail != undefined) {
                            var item_device = devices.detail[0].device;
                            if (item_device.filter(st => st.uuid == item.uuid).length > 0) response += '<input name="detail[][device[][check]]" type="checkbox" checked>';
                            else response += '<input name="detail[][device[][check]]" type="checkbox">';
                        }
                        else response += '<input name="detail[][device[][check]]" type="checkbox">';

                        response += `<span></span>
                                                    </label>
                                                </div>
                                            </td>        
                                            <td class="text-left kt-padding-l-10" style="max-width:230px">
                                                <input name="detail[][device[][uuid]]" value="${item.uuid}" hidden />
                                                <span class="text-primary">${item.prefixName}</span><br/>
                                                <span>${item.name}</span>
                                            </td>
                                            <td>
                                                <input class="form-control" name="detail[][device[][serial]]" placeholder="Nhập" value="${item.serial}" hidden />
                                                <span>${item.serial}</span>
                                            </td>
                                            <td>
                                                <input class="form-control" name="detail[][device[][expire_device_string]]" placeholder="Nhập" value="${item.expireDevice_string}" hidden />
                                                <span>${item.expireDevice_string}</span>
                                            </td>
                                        </tr>
                                    </tbody>`
                    });
                    response += `</table>
                        </div>
                        <div class="text-right kt-margin-t-20">
                            <button type="submit" class="btn btn-info">
                                Áp dụng
                            </button>
                            <button type="button" class="btn btn-danger kt-close">
                                Hủy bỏ
                            </button>
                        </div>
                    </form>`;
                    const $content = subDrawer.setBody(response);
                    initSaveDetailInMess($content, categoryId);
                })
                .fail(function (data) {
                    subDrawer.error();
                });
        });
        subDrawer.open();
    }

    var initContent = function ($content) {
        /*
            Init form
        */
        const $formExport = $content.find('.dmms-form-bunker__aceptimport');
        initFormAceptImport($formExport);

        /*
            Init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

        //$('.unit_price').simpleMoneyFormat();
        ////Khi đơn giá thay đổi => thay đổi tông tiền
        //$('.unit_price').on('change', function () {
        //    const category_id = $(this).attr('data-categoryid');
        //    const wrapper = $(`tr[id="${category_id}"]`);
        //    const real_quantity = wrapper.find('input[name="detail[][real_quantity]"]').val();
        //    const money_string = wrapper.find('input[name="detail[][unit_price_string]"]').val();
        //    const money = parseInt(money_string.replace(/,/g, ''));

        //    if (real_quantity > 0) wrapper.find('.money_string').html('').html(real_quantity * money);
        //    else wrapper.find('.money_string').html('').html('0');
        //    $('.money_string').simpleMoneyFormat();
        //});

        ////Khi vật tư thay đổi số lượng => thay đổi tổng tiền
        //$('input[name="detail[][real_quantity]"]').on('change', function () {
        //    const category_id = $(this).attr('data-categoryid');
        //    const wrapper = $(`tr[id="${category_id}"]`);
        //    const real_quantity = wrapper.find('input[name="detail[][real_quantity]"]').val();
        //    const money_string = wrapper.find('input[name="detail[][unit_price_string]"]').val();
        //    if (money_string != "") {
        //        const money = parseInt(money_string.replace(/,/g, ''));

        //        wrapper.find('.money_string').html('').html(real_quantity * money);
        //        $('.money_string').simpleMoneyFormat();
        //    }
        //    else wrapper.find('.money_string').html('').html('0');
        //});

        //Thêm chi tiết cho category
        $('.js-add-detail-importsupplies').off('click').on('click', function () {
            $formExport.find('.js-add-detail-importsupplies').prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading

            const $this = $(this);
            const categoryId = $this.attr('data-id');
            const exportUuid = $this.attr('data-uuidexport');

            addDetailImportSupplies(exportUuid, categoryId);

            $formExport.find('.js-add-detail-importsupplies').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
        });

        //Đóng drawerInner
        $('.kt-close').on('click', function () {
            drawerInner != null ? drawerInner.close() : drawer.close();
        });
    };

    return {
        // Public functions
        init: function () {
            initSearch();
            initEventListeners();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-bunker').length) {
        DMMSBunker.init();
    }
});