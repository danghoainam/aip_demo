﻿"use strict";
// Class definition

var DMMSProject = function () {
    // Private functions

    var dataTableInstance = null;
    var dataTableInstance2 = null;
    var dataTableInstance3 = null;
    var drawer = null;
    var Packagetable = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    var typeViewByDateTime = 1;
    // demo initializer

    let tFrom = moment().subtract(6, 'days').format('YYYY-MM-DD').toString();
    let tTo = moment().format('YYYY-MM-DD').toString();

    var startDatePicker = function () {
        $('#kt_dashboard_daterangepicker_title').html("");
        $("#kt_dashboard_daterangepicker_date").text(moment().subtract(6, 'days').format('DD-MM-YYYY').toString() + " - " + moment().format('DD-MM-YYYY').toString());
    }

    var daterangepickerInit = function () {
        if ($('#kt_dashboard_daterangepicker').length == 0) {
            return;
        }
        var picker = $('#kt_dashboard_daterangepicker');
        var start = moment().subtract(6, 'days');
        var end = moment();
        function cb(start, end, label) {
            var title = '';
            var range = '';
            if ((end - start) < 100 || label == 'Hôm nay') {
                title = 'Hôm nay: ';
                range = start.format('DD-MM-YYYY');
            } else if (label == 'Hôm qua') {
                title = 'Hôm qua: ';
                range = start.format('DD-MM-YYYY');
            } else {
                range = start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY');
            }
            tFrom = start.format('YYYY-MM-DD');
            tTo = end.format('YYYY-MM-DD');
            $('#charpie1').remove();
            $('.charpie1').append('<div id="charpie1"></div>');
            $('#charpie2').remove();
            $('.charpie2').append('<div id="charpie2"></div>');
            $('#charpie3').remove();
            $('.charpie3').append('<div id="charpie3"></div>');
            $('#chartmix').remove();
            $('.chartmix').append('<div id="chartmix"></div>');
            initTable(tFrom, tTo, typeViewByDateTime);
            $('#kt_dashboard_daterangepicker_date').html(range);
            $('#kt_dashboard_daterangepicker_title').html(title);
            //landd start-----------------------
            $('#chartstackdetail1').remove();
            $('.chartstackdetail1').append('<div id="chartstackdetail1"></div>');
            $('#chartstackdetail2').remove();
            $('.chartstackdetail2').append('<div id="chartstackdetail2"></div>');
            stackChartTaskDetailInit(tFrom, tTo);
            //landd end-----------------------
        }
        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Hôm nay': [moment(), moment()],
                'Hôm qua': [moment().subtract(1, 'days'), moment()],
                '7 ngày qua': [moment().subtract(6, 'days'), moment()],
                '30 ngày qua': [moment().subtract(29, 'days'), moment()],
                'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            locale: {
                applyLabel: 'Áp dụng',
                cancelLabel: 'Hủy bỏ',
                customRangeLabel: 'Tùy chỉnh',
                firstDay: 1
            }
        }, cb);
    }

    var initTable = function (timeFrom, timeTo, typeViewByDateTime) {
        if (typeViewByDateTime == 1) {
            if (dataTableInstance != null && dataTableInstance != undefined) dataTableInstance.destroy();
            dataTableInstance = $('.kt-datatable-detail-task1').KTDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '/ReportOverview/GetReportStatisticDetailTaskTab1',
                        },
                        response: {
                            map: function (res) {
                                const raw = res && res.data || {};
                                const { pagination } = raw;
                                const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                                const page = pagination === undefined ? 0 : pagination.page;
                                CURRENT_PAGE = page || 1;
                                START_PAGE = 0;
                                LIMIT = perpage;
                                return {
                                    data: raw.items || [],
                                    meta: {
                                        ...pagination || {},
                                        perpage: perpage || LIMIT,
                                        page: page || 1,
                                    },
                                }
                            },
                        },
                        filter: function (data = {}) {
                            const query = data.query || {};
                            const pagination = data.pagination || {};
                            const { customerUuid, staff_uuid, teamUuid, stateId, keyword } = query;
                            if (START_PAGE === 1) {
                                pagination.page = START_PAGE;
                            }
                            return {
                                limit: pagination.perpage || LIMIT,
                                customerUuid: customerUuid || $('#js-filter-customer').val(),
                                staff_uuid: staff_uuid || $('#js-filter-staff').val(),
                                teamUuid: teamUuid || $('#js-filter-team').val(),
                                stateId: stateId || $('#js-filter-state').val(),
                                keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                                timeFrom: timeFrom,
                                timeTo: timeTo,
                                page: pagination.page,

                            };
                        }
                    },
                    pageSize: LIMIT,
                    serverPaging: true,
                    serverFiltering: true,
                },

                search: {
                    //input: $('#js-filter-keyword'),
                    onEnter: true,
                },
                columns: [
                    {
                        field: 'id',
                        title: 'STT',
                        width: 30,
                        textAlign: 'center',
                        template: function (data, row) {
                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                        },
                        responsive: {
                            visible: 'md',
                            hidden: 'sm'
                        }
                    },
                    {
                        field: 'code',
                        title: 'Mã công việc',
                        width: 70
                    },
                    {
                        field: 'name',
                        title: 'Nội dung công việc theo sự cố',

                    },
                    {
                        field: 'priority',
                        title: 'Độ ưu tiên',
                        width: 40,
                        textAlign: 'center'
                    },
                    {
                        field: 'customer_name',
                        title: 'Đơn vị triển khai',
                        width: 100
                    },
                    {
                        field: 'staff_name',
                        title: 'Kỹ thuật phụ trách',
                        width: 100
                    },
                    {
                        field: 'team_name',
                        title: 'Team quản lý',
                        width: 100
                    },
                    {
                        field: 'time_created_format_nohh',
                        title: 'Thời gian tạo',
                        width: 80
                    },
                    {
                        field: 'state',
                        title: 'Trạng thái',
                        width: 100,
                        template: function (data) {
                            var states = {
                                5: { 'title': 'Đang xử lý', 'class': 'kt-badge--brand' },
                                7: { 'title': 'Yêu cầu tiếp tục', 'class': ' kt-badge--warning' },
                                4: { 'title': 'Đã hẹn lịch', 'class': ' kt-badge--primary' },
                                8: { 'title': 'Hoàn thành', 'class': 'kt-badge--success' },
                                6: { 'title': 'Đợi duyệt đề xuất xử lý', 'class': 'kt-badge--danger' },
                                3: { 'title': 'Đã tiếp nhận', 'class': 'kt-badge--info' },
                                9: { 'title': 'Cập nhật số lượng thiết bị trong task bảo trì', 'class': 'kt-badge--info' },
                                2: { 'title': 'Yêu cầu hỗ trợ', 'class': 'kt-badge--info' },
                                1: { 'title': 'Tạo mới', 'class': 'kt-badge--info' },
                                10: { 'title': 'Đã hủy', 'class': 'kt-badge--info' },
                            };
                            if (data.state) {
                                return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill">' + states[data.state].title + '</span>';
                            }
                            else {
                                return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới</span>';
                            }
                        },
                        width: 100,
                        textAlign: 'center'
                    }],
                sortable: false,
                pagination: true,
                responsive: true,
            });
        }
        else if (typeViewByDateTime == 2) {
            if (dataTableInstance != null && dataTableInstance != undefined) dataTableInstance.destroy();
            dataTableInstance = $('.kt-datatable-detail-task2').KTDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '/ReportOverview/GetReportStatisticDetailTaskTab2',
                        },
                        response: {
                            map: function (res) {
                                const raw = res && res.data || {};
                                const { pagination } = raw;
                                const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                                const page = pagination === undefined ? 0 : pagination.page;
                                CURRENT_PAGE = page || 1;
                                START_PAGE = 0;
                                LIMIT = perpage;
                                return {
                                    data: raw.items || [],
                                    meta: {
                                        ...pagination || {},
                                        perpage: perpage || LIMIT,
                                        page: page || 1,
                                    },
                                }
                            },
                        },
                        filter: function (data = {}) {
                            const query = data.query || {};
                            const pagination = data.pagination || {};
                            const { customerUuid, staff_uuid, teamUuid, stateId, keyword } = query;
                            if (START_PAGE === 1) {
                                pagination.page = START_PAGE;
                            }
                            return {
                                limit: pagination.perpage || LIMIT,
                                customerUuid: customerUuid || $('#js-filter-customer').val(),
                                staff_uuid: staff_uuid || $('#js-filter-staff').val(),
                                teamUuid: teamUuid || $('#js-filter-team').val(),
                                stateId: stateId || $('#js-filter-state').val(),
                                keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                                timeFrom: timeFrom,
                                timeTo: timeTo,
                                page: pagination.page,

                            };
                        }
                    },
                    pageSize: LIMIT,
                    serverPaging: true,
                    serverFiltering: true,
                },

                search: {
                    //input: $('#js-filter-keyword'),
                    onEnter: true,
                },
                columns: [
                    {
                        field: 'id',
                        title: 'STT',
                        width: 30,
                        textAlign: 'center',
                        template: function (data, row) {
                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                        },
                        responsive: {
                            visible: 'md',
                            hidden: 'sm'
                        }
                    },
                    {
                        field: 'code',
                        title: 'Mã công việc',
                        width: 70
                    },
                    {
                        field: 'project_name',
                        title: 'Nội dung công việc theo sự cố',

                    },
                    {
                        field: 'priority',
                        title: 'Độ ưu tiên',
                        width: 40,
                        textAlign: 'center'
                    },
                    {
                        field: 'project_code',
                        title: 'Mã dự án',
                        width: 70
                    },
                    {
                        field: 'field_name',
                        title: 'Lĩnh vực',
                        width: 100
                    },
                    {
                        field: 'customer_name',
                        title: 'Đơn vị triển khai',
                        width: 120
                    },
                    {
                        field: 'count',
                        title: 'Sản phẩm BHBT',
                        width: 60
                    },
                    {
                        field: 'staff_name',
                        title: 'Kỹ thuật phụ trách',
                        width: 100
                    },
                    {
                        field: 'team_name',
                        title: 'Team quản lý',
                        width: 100
                    },
                    {
                        field: 'time_created_format_nohh',
                        title: 'Thời gian thực hiện',
                        width: 90
                    },
                    {
                        field: 'complete_value',
                        title: 'Tiến độ',
                        width: 130
                    },
                    {
                        field: 'state',
                        title: 'Trạng thái',
                        width: 100,
                        template: function (data) {
                            var states = {
                                5: { 'title': 'Đang xử lý', 'class': 'kt-badge--brand' },
                                7: { 'title': 'Yêu cầu tiếp tục', 'class': ' kt-badge--warning' },
                                4: { 'title': 'Đã hẹn lịch', 'class': ' kt-badge--primary' },
                                8: { 'title': 'Hoàn thành', 'class': 'kt-badge--success' },
                                6: { 'title': 'Đợi duyệt đề xuất xử lý', 'class': 'kt-badge--danger' },
                                3: { 'title': 'Đã tiếp nhận', 'class': 'kt-badge--info' },
                                9: { 'title': 'Cập nhật số lượng thiết bị trong task bảo trì', 'class': 'kt-badge--info' },
                                2: { 'title': 'Yêu cầu hỗ trợ', 'class': 'kt-badge--info' },
                                1: { 'title': 'Tạo mới', 'class': 'kt-badge--info' },
                                10: { 'title': 'Đã hủy', 'class': 'kt-badge--info' },
                            };
                            if (data.state) {
                                return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill">' + states[data.state].title + '</span>';
                            }
                            else {
                                return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới</span>';
                            }
                        },
                        width: 100,
                        textAlign: 'center'
                    }],
                sortable: false,
                pagination: true,
                responsive: true,
            });
        }       
        else {
            if (dataTableInstance != null && dataTableInstance != undefined) dataTableInstance.destroy();
            dataTableInstance = $('.kt-datatable-detail-task3').KTDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '/ReportOverview/GetReportStatisticDetailTaskTab3',
                        },
                        response: {
                            map: function (res) {
                                const raw = res && res.data || {};
                                const { pagination } = raw;
                                const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                                const page = pagination === undefined ? 0 : pagination.page;
                                CURRENT_PAGE = page || 1;
                                START_PAGE = 0;
                                LIMIT = perpage;
                                return {
                                    data: raw.items || [],
                                    meta: {
                                        ...pagination || {},
                                        perpage: perpage || LIMIT,
                                        page: page || 1,
                                    },
                                }
                            },
                        },
                        filter: function (data = {}) {
                            const query = data.query || {};
                            const pagination = data.pagination || {};
                            const { customerUuid, staff_uuid, teamUuid, stateId, keyword } = query;
                            if (START_PAGE === 1) {
                                pagination.page = START_PAGE;
                            }
                            return {
                                limit: pagination.perpage || LIMIT,
                                customerUuid: customerUuid || $('#js-filter-customer').val(),
                                staff_uuid: staff_uuid || $('#js-filter-staff').val(),
                                teamUuid: teamUuid || $('#js-filter-team').val(),
                                stateId: stateId || $('#js-filter-state').val(),
                                keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                                timeFrom: timeFrom,
                                timeTo: timeTo,
                                page: pagination.page,

                            };
                        }
                    },
                    pageSize: LIMIT,
                    serverPaging: true,
                    serverFiltering: true,
                },

                search: {
                    //input: $('#js-filter-keyword'),
                    onEnter: true,
                },
                columns: [
                    {
                        field: 'id',
                        title: 'STT',
                        width: 30,
                        textAlign: 'center',
                        template: function (data, row) {
                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                        },
                        responsive: {
                            visible: 'md',
                            hidden: 'sm'
                        }
                    },
                    {
                        field: 'code',
                        title: 'Tên kế hoạch bảo hành bảo trì',

                    },
                    {
                        field: 'name_project',
                        title: 'Dự án',

                    },
                    {
                        field: 'field',
                        title: 'Sản phẩm',
                        width: 40
                    },
                    {
                        field: 'province',
                        title: 'Đơn vị triển khai',
                        width: 50
                    },
                    {
                        field: 'count_dvtk',
                        title: 'Chi phí dự kiến',
                        width: 130
                    },
                    {
                        field: 'count_product',
                        title: 'Thời gian triển khai',
                        width: 140
                    },
                    {
                        field: 'contract_code',
                        title: 'Người lập',
                        width: 90
                    },

                    {
                        field: 'date_handover',
                        title: 'Người duyệt',
                        width: 90
                    },
                    {
                        field: 'expiry_date',
                        title: 'Thời gian duyệt',
                        width: 80
                    },
                    {
                        field: 'state',
                        title: 'Trạng thái',
                        width: 100,
                        template: function (data) {
                            var states = {
                                5: { 'title': 'Đang xử lý', 'class': 'kt-badge--brand' },
                                7: { 'title': 'Yêu cầu tiếp tục', 'class': ' kt-badge--warning' },
                                4: { 'title': 'Đã hẹn lịch', 'class': ' kt-badge--primary' },
                                8: { 'title': 'Hoàn thành', 'class': 'kt-badge--success' },
                                6: { 'title': 'Đợi duyệt đề xuất xử lý', 'class': 'kt-badge--danger' },
                                3: { 'title': 'Đã tiếp nhận', 'class': 'kt-badge--info' },
                                9: { 'title': 'Cập nhật số lượng thiết bị trong task bảo trì', 'class': 'kt-badge--info' },
                                2: { 'title': 'Yêu cầu hỗ trợ', 'class': 'kt-badge--info' },
                                1: { 'title': 'Tạo mới', 'class': 'kt-badge--info' },
                            };
                            if (data.state) {
                                return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill">' + states[data.state].title + '</span>';
                            }
                            else {
                                return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới</span>';
                            }
                        },
                        width: 100,
                        textAlign: 'center'
                    }],
                sortable: false,
                pagination: true,
                responsive: true,
            });
        }
    };

    //localStorage.setItem("reportStatisticDetailTaskTab", "1");

    //var reportStatisticDetailTaskTab = localStorage.getItem("reportStatisticDetailTaskTab");
    //if (reportStatisticDetailTaskTab == "1") {
    //    var temp = $(".link-report-statistic-detail-task-tab1");
    //    var tab = $(".report-statistic-detail-task-tab1");
    //    temp.addClass("active");
    //    tab.addClass("active");
    //    localStorage.removeItem("reportStatisticDetailTaskTab");

    //    typeViewByDateTime = 1;
    //    initTable(tFrom, tTo, 1);
    //}
    //else if (reportStatisticDetailTaskTab == "2") {
    //    var temp = $(".link-report-statistic-detail-task-tab2");
    //    var tab = $(".report-statistic-detail-task-tab2");
    //    temp.addClass("active");
    //    tab.addClass("active");

    //    typeViewByDateTime = 2;
    //    initTable(tFrom, tTo, 2);
    //}
    //else if (reportStatisticDetailTaskTab == "3") {
    //    var temp = $(".link-report-statistic-detail-task-tab3");
    //    var tab = $(".report-statistic-detail-task-tab3");
    //    temp.addClass("active");
    //    tab.addClass("active");
    //    localStorage.removeItem("reportStatisticDetailTaskTab");

    //    typeViewByDateTime = 3;
    //    initTable(tFrom, tTo, 3);
    //}

    $('.link-report-statistic-detail-task-tab1').on("click", function () {
        typeViewByDateTime = 1;
        initTable(tFrom, tTo, 1);
    });

    $('.link-report-statistic-detail-task-tab2').on("click", function () {
        typeViewByDateTime = 2;
        initTable(tFrom, tTo, 2);
    });

    $('.link-report-statistic-detail-task-tab3').on("click", function () {
        typeViewByDateTime = 3;
        initTable(tFrom, tTo, 3);
    });

    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var initSearch = function () {
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            }).selectpicker();
        $('#js-filter-customer')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'customerUuid');
                    START_PAGE = 1;
                }
            }).selectpicker();
        $('#js-filter-staff')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'staff_uuid');
                    START_PAGE = 1;
                }
            }).selectpicker();
        $('#js-filter-team')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'teamUuid');
                    START_PAGE = 1;
                }
            }).selectpicker();
        $('#js-filter-state')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'stateId');
                    START_PAGE = 1;
                }
            }).selectpicker();
        //if ($("#location_id").val() !== '') {
        //    $('#kt-select-location').selectpicker('val', $("#location_id").val());
        //}
        //else {
        //    $('#kt-select-location').selectpicker();
        //}
    };

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-project__form');
        initForm($form);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

    };

    //landd start------------------------------
    var initBieuDoStackDetail1 = function (categories, data1, data2, data3) {
        // type 1,2 : chưa xử lý
        //      8: Hoàn thành   
        // Còn lại đang xử lý
        var optionsstacked1 = {
            series: [{
                name: 'Chưa tiếp nhận',
                data: data1
            }, {
                name: 'Đang xử lý',
                data: data2
            }, {
                name: 'Đã hoàn thành',
                data: data3
            }],
            chart: {
                type: 'bar',
                height: 450,
                stacked: true,
                toolbar: {
                    show: false
                },
            },
            yaxis: {
                labels: {
                    formatter: function (val) {
                        return val.toFixed(0)
                    }
                }
            },
            colors: ['#36B37E', '#FF931E', '#EB5757'],
            responsive: [{
                breakpoint: 480,
                options: {
                    legend: {
                        position: 'bottom',
                        offsetX: -10,
                        offsetY: 0
                    }
                }
            }],
            plotOptions: {
                bar: {
                    horizontal: false,
                },
            },
            dataLabels: {
                enabled: false
            },
            xaxis: {
                //  type: 'datetime',
                categories: categories,
            }, fill: {
                opacity: 1
            }
        };
        var chartstack1 = new ApexCharts(document.querySelector("#chartstackdetail1"), optionsstacked1);
        chartstack1.render();
    }

    var initBieuDoStackDetail2 = function (categories, data1, data2, data3) {
        var optionsstacked2 = {
            series: [{
                name: 'Chưa xử lý',
                data: data1
            }, {
                name: 'Đang xử lý',
                data: data2
            }, {
                name: 'Đã hoàn thành',
                data: data3
            }],
            chart: {
                type: 'bar',
                height: 450,
                stacked: true,
                toolbar: {
                    show: false
                },
            },
            yaxis: {
                labels: {
                    formatter: function (val) {
                        return val.toFixed(0)
                    }
                }
            },
            colors: ['#36B37E', '#FF931E', '#EB5757'],
            responsive: [{
                breakpoint: 480,
                options: {
                    legend: {
                        position: 'bottom',
                        offsetX: -10,
                        offsetY: 0
                    }
                }
            }],
            plotOptions: {
                bar: {
                    horizontal: false,
                },
            },
            dataLabels: {
                enabled: false
            },
            xaxis: {
                //  type: 'datetime',
                categories: categories,
            }, fill: {
                opacity: 1
            }
        };
        var chartstack2 = new ApexCharts(document.querySelector("#chartstackdetail2"), optionsstacked2);
        chartstack2.render();
    }
    var stackChartTaskDetailInit = function (timeFrom, timeTo) {
        let categoriesTask1 = [];
        let data1Task1 = [];
        let data2Task1 = [];
        let data3Task1 = [];
        var checkShow1 = 0;
        let categoriesTask2 = [];
        let data1Task2 = [];
        let data2Task2 = [];
        let data3Task2 = [];
        var checkShow2 = 0;
        KTApp.block($('.w-100'));
        $.ajax({
            url: `/taskoverview/all`,
            method: 'POST',
            cache: false,
            data: {
                request: {
                    timeFrom: timeFrom, timeTo: timeTo
                }
            }
        })
            .done(function (res) {
                //var data = res.
                $.each(res, function (index, item) {
                    var bookmark1Data1 = 0;
                    var bookmark1Data2 = 0;
                    var bookmark1Data3 = 0;
                    var bookmark2Data1 = 0;
                    var bookmark2Data2 = 0;
                    var bookmark2Data3 = 0;
                    checkShow1 = 0;
                    checkShow2 = 0;                   
                    $.each(item, function (index, item) {
                        //Công việc khắc phục sự cố
                        if (item.type == "1" || item.type == 100) {
                            if (item.state == 100) {
                                bookmark1Data1 += parseInt(item.count);
                                bookmark1Data2 += parseInt(item.count);
                                bookmark1Data3 += parseInt(item.count);
                                checkShow1 = 1;
                            }
                            else
                                if (item.state == "8") {
                                    bookmark1Data3 += parseInt(item.count);
                                    checkShow1 = 1;
                                }
                                else
                                    if (item.state == "1" || item.state == "2") {
                                        bookmark1Data1 += parseInt(item.count);
                                        checkShow1 = 1;
                                    }
                                    else {
                                        bookmark1Data2 += parseInt(item.count);
                                        checkShow1 = 1;
                                    }
                        }
                        //Công việc bảo trì - Bảo dưỡng
                        if (item.type == "3" || item.type == 100) {
                            if (item.state == 100) {
                                bookmark2Data1 += parseInt(item.count);
                                bookmark2Data2 += parseInt(item.count);
                                bookmark2Data3 += parseInt(item.count);
                                checkShow2 = 1;
                            }
                            else
                                if (item.state == "8") {
                                    bookmark2Data3 += parseInt(item.count);
                                    checkShow2 = 1;
                                }
                                else
                                    if (item.state == "1" || item.state == "2") {
                                        bookmark2Data1 += parseInt(item.count);
                                        checkShow2 = 1;
                                    }
                                    else {
                                        bookmark2Data2 += parseInt(item.count);
                                        checkShow2 = 1;
                                    }
                        }

                    });
                    if (checkShow1 == 1) {
                        categoriesTask1.push(index);
                        data1Task1.push(parseInt(bookmark1Data1));
                        data2Task1.push(parseInt(bookmark1Data2));
                        data3Task1.push(parseInt(bookmark1Data3));
                    }
                    if (checkShow2 == 1) {
                        categoriesTask2.push(index);
                        data1Task2.push(parseInt(bookmark2Data1));
                        data2Task2.push(parseInt(bookmark2Data2));
                        data3Task2.push(parseInt(bookmark2Data3));
                    }
                });
                console.log(data1Task1[data1Task1.length - 2]);
                let sumTask1 = data1Task1[data1Task1.length - 2] + data2Task1[data2Task1.length - 2] + data3Task1[data3Task1.length - 2];
                let sumTask2 = data1Task2[data1Task2.length - 2] + data2Task2[data2Task2.length - 2] + data3Task2[data3Task2.length - 2];
                if (data1Task1.length == 1) sumTask1 = 0;
                if (data1Task2.length == 1) sumTask2 = 0;
                $("#tongSoTaskDetail1").text(sumTask1);
                $("#tongSoTaskDetail2").text(sumTask2);
                initBieuDoStackDetail1(categoriesTask1, data1Task1, data2Task1, data3Task1);
                initBieuDoStackDetail2(categoriesTask2, data1Task2, data2Task2, data3Task2);
                KTApp.unblock($('.w-100'));
            })
            .fail(function () {
                $.notify('Có lỗi xảy ra, vui lòng thử lại.', { type: 'danger' });
            });
    }
    //landd end------------------------------

    return {
        // Public functions
        init: function () {
            startDatePicker();
            initTable(tFrom, tTo, 1);
            initSearch();
            daterangepickerInit();
            stackChartTaskDetailInit(tFrom,tTo);
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-reportstatisticdetailtask').length) {
        DMMSProject.init();
    }
});
