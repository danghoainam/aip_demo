﻿"use strict";
// Class definition

var DMMSGroupList = function () {
    // Private functions

    var dataTableInstance = null;

    var LIMIT = 20;

    // demo initializer
    var initTable = function () {
        dataTableInstance = $('.kt-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/group/getList',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: pagination.limit || LIMIT,
                                    page: pagination.page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { location_id, keyword } = query;
                        return {
                            limit: pagination.perpage || LIMIT,
                            location_id: location_id || $('#js-filter-location').val(),
                            keyword: keyword || '',
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                input: $('#js-filter-keyword'),
            },
            columns: [
                {
                    field: 'uuid',
                    title: 'Mã nhóm',
                }, {
                    field: 'name',
                    title: 'Tên nhóm',
                },  {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 110,
                    overflow: 'visible',
                    autoHide: false,
                    template: function (data) {
                        return `
                            <a href="/customer/edit/${data.uuid}" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </a>
                            <a href="/customer/edit/${data.uuid}" title="Xóa" class="btn btn-sm btn-danger btn-icon btn-icon-md ml-2">
                                <i class="la la-trash"></i>
                            </a>
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };



    var initValidation = function () {
        $("#dmms-group__form").validate({
            rules: {

                name: {
                    required: true,
                },
                description: {
                    required: true,
                },
            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên nhóm.'
                },
                description: {
                    required: 'Vui lòng nhập mô tả.'
                },

            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('#dmms-group__form', -200);
            },
            submitHandler: function (form) {
                form[0].submit(); // submit the form
            },
        });
    };

    return {
        // Public functions
        init: function () {
            initTable();
            initSearch();
            initValidation();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-group-getlist').length) {
        DMMSGroupList.init();
    }
});
