"use strict";
// Class definition

var DMMSProject = function () {
    // Private functions
   
    var dataTableInstance = null;
    var drawer = null;

    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    // demo initializer
    var initTable = function () {
        var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
        if (editSucessCache) {
            $.notify(editSucessCache.massage, { type: 'success' });
            localStorage.removeItem('edit_success');
        }
        dataTableInstance = $('.kt-datatable').KTDatatable({                      
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/taskoffer/gets',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            if (raw.items != null) {
                                $.each(raw.items, function (index, item) {
                                    for (var property in item) {
                                        if (item[property] && typeof item[property] === "object") {
                                            $.each(item[property], function (i, v) {
                                                for (var pro in item[property]) {
                                                    if (item[property][pro] && typeof item[property][pro] === "string") {
                                                        item[property][pro] = item[property][pro].replace(/</g, "&lt;").replace(/>/g, "&gt;");
                                                    }
                                                }
                                            })
                                        }
                                        if (item[property] && typeof item[property] === "string") {
                                            item[property] = item[property].replace(/</g, "&lt;").replace(/>/g, "&gt;");
                                        }
                                    }
                                })
                            }
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? 0 : pagination.page;
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage|| LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword, state } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {                           
                            limit: pagination.perpage || LIMIT,
                            keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            state: state || $('.offerstate').val(),
                            page: pagination.page,
                           
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,
            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 20,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'description',
                    title: 'Nội dung',
                    
                }, 
                {
                    field: 'task.name',
                    title: 'Công việc',

                },
                {
                    field: 'employee.name',
                    title: 'Người đề xuất',
                    width: 90,
                    template: function (data) {
                        var output = '';
                        if (data.employee != null && data.employee.name != null) {
                            output += '<a href = "/staff/update/' + data.employee.uuid + '"><span>' + data.employee.name + '</span ></a> <br>';
                        }
                        return output;
                    }
                },
                {
                    field: 'time_created_format_nohh',
                    title: 'Thời gian',
                    width: 70
                },
                {
                    field: 'state',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    width: 90,
                    template: function (data) {
                        var states = {
                            1: { 'title': 'Chưa duyệt', 'class': 'kt-badge--danger' },
                            2: { 'title': 'Đã duyệt', 'class': ' kt-badge--success' },
                            3: { 'title': 'Từ chối', 'class': ' kt-badge--warning' },
                           
                        };
                        if (data.state) {
                            return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill">' + states[data.state].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Chưa rõ</span>';
                        }
                    }
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 40,
                    overflow: 'visible',
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        return `
                             <a href="/taskoffer/update/${data.uuid}" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </a>
                            
                                                     
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };
    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };
    var initSearch = function () {              
        $('.offerstate')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'state');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });

    };
    var DoneOffer = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang duyệt đề xuất'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/taskoffer/Doneoffer/${id}`,
                method: 'GET',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Duyệt thành công!', { type: 'success' });
                    localStorage['edit_success'] = JSON.stringify({ from: (`/taskoffer/Doneoffer/${id}`), massage: 'Duyệt thành công!' });
                    window.location = window.location.origin + "/taskoffer";
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Duyệt không thành công!', { type: 'danger' });
                });
        }
    };

    var CancleOffer = function (id, note) {
        //const data = note;
        var form = $(".note");
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang huỷ đề xuất'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/taskoffer/Cancleoffer/${id}`,
                method: note ? 'POST' : 'GET',
                data: form.serializeJSON(),
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Từ chối thành công!', { type: 'success' });
                    localStorage['edit_success'] = JSON.stringify({ from: (`/taskoffer/Doneoffer/${id}`), massage: 'Từ chối thành công!' });
                    window.location = window.location.origin + "/taskoffer";
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Từ chối không thành công!', { type: 'danger' });
                });
        }
    };
    var initEventListeners = function () {
        $(document)
            .off('click', '.submittaskoffer')
            .on('click', '.submittaskoffer', function () {
                const id = document.getElementById("uuid").value;

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Bạn có muốn duyệt đề xuất.',
                    icon: 'info',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        DoneOffer(id);
                    }
                });
            })
            .off('click', '.cancleoffer')
            .on('click', '.cancleoffer', function () {
                const id = document.getElementById("uuid").value;
                var input = document.createElement("textarea");
                input.className = "note form-control";
                input.cols = "40";
                input.rows = "5";
                input.name = ("note");
                KTApp.swal({
                    text:
                        'Nội dung từ chối',
                    dangerMode: true,
                    content: input,
                }).then((confirm) => {
                    if (confirm) {
                        debugger
                        var note = document.getElementsByClassName("note")[0].value;
                        CancleOffer(id, note);
                       
                    }
                });   
            })
    
      

    };
    var initForm = function ($form) {
        initValidation(true);
    }
    var initValidation = function (isForm) {
        $(".dmms-project__form").validate({
            rules: {
                name: {
                    required: true,
                },
                description: {
                    required: true,
                },

            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên dự án.'
                },

                description: {
                    required: 'Vui lòng nhập mô tả.'
                }
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo('.dmms-project__form', -200);
            },
            //submitHandler: function (form) {
                
            //    const data = $(form).serializeJSON() || {};
            //    const { uuid } = data;
                
            //    $.ajax({
            //        url: uuid ? `/project/edit/` : 'project/create',
            //        method: uuid ? 'PUT' : 'POST',
            //        data,
            //    })
            //        .done(function () {
            //            $(".submit").attr("disabled", true);
            //            KTApp.block('.dmms-project__form');
            //            const successMsg = uuid ? 'Sửa thông tin thành công!' : 'Thêm dự án thành công!';
            //            $.notify(successMsg, { type: 'success' });

            //            if (isForm) {
            //                drawer.close();
            //                reloadTable();
            //            } else {

            //                localStorage['edit_success'] = JSON.stringify({ from: (uuid ? `/supplier/update/` : '/supplier/create'), massage: successMsg });
            //                window.location = window.location.origin + "/project";
            //            }
            //        })
            //        .fail(function () {
            //            KTApp.unblock('.dmms-project__form');
            //            const errorMsg = uuid ? 'Sửa thông tin không thành công!' : 'Thêm dự án không thành công!';
            //            $.notify(errorMsg, { type: 'danger' });
            //        });
                
            //    return false;
            //},
        });
    }
    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-project__form');
        initForm($form);
        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));        
        $('.offerstate').selectpicker();
        $('#js-filter-customer').selectpicker();
    };
    $(".linkpro").on("click", function () {
        var project_uuid = document.getElementById("uuid").value;

        dataTableInstance = $('.kt-datatablepro').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {

                        url: `/device/devicepro/${project_uuid}`,
                    },

                    response: {

                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? undefined : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};


                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,

                            //keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,

            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 30,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                //{
                //    field: 'code',
                //    title: 'Mã công việc',
                //    width: 80
                //},
                {
                    field: 'name',
                    title: 'Thiết bị',

                },
                //{
                //    field: 'ticket.title',
                //    title: 'Tiêu đề yêu cầu',
                //}, 
                {
                    field: 'customer.name',
                    title: 'Đơn vị',

                },


                {
                    field: 'created_at_format',
                    title: 'Ngày tạo',
                    textAlign: 'center',
                    //width : 110,
                },
                {
                    field: 'state',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    width: 140,
                    template: function (data) {
                        var states = {
                            1: { 'title': 'Đang sử dụng', 'class': 'kt-badge--brand' },
                            2: { 'title': 'Đang yêu cầu sửa chữa', 'class': ' kt-badge--warning' },
                            3: { 'title': 'Đến lịch bảo hành', 'class': ' kt-badge--primary' },
                            4: { 'title': 'Đang xử lý theo yêu cầu', 'class': 'kt-badge--success' },
                            5: { 'title': 'Cần thay thế', 'class': 'kt-badge--danger' },
                            6: { 'title': 'Mang đi sửa chữa - Không có thiết bị thay thế', 'class': 'kt-badge--info' },
                            7: { 'title': 'Mang đi sửa chữa - Có thiết bị thay thế', 'class': 'kt-badge--info' },
                            8: { 'title': 'Thay thế', 'class': 'kt-badge--info' },
                            9: { 'title': 'Hỏng', 'class': 'kt-badge--info' },
                        };
                        if (data.state) {
                            return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill">' + states[data.state].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge ' + states[1].class + ' kt-badge--inline kt-badge--pill">' + states[1].title + '</span>';
                        }
                    }
                },

                //{
                //    field: 'created_at',
                //    title: 'Ngày tạo',
                //},
            ],
            layout: {
                scroll: !0,
                height: null,
                footer: !1
            },
            sortable: false,
            pagination: true,
            responsive: true,
        });
    });
  
    return {
        // Public functions
        init: function () {
            initTable();
            initEventListeners();
            initValidation();
            initSearch();
        $('.offerstate').selectpicker();

        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-taskoffer').length) {
        //debugger
        DMMSProject.init();
    }
});
