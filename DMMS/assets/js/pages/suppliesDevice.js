﻿"use strict";

// Class definition

var DMMSSuppliesDevice = function () {
    // Private functions
    var drawer = null;

    var initDeviceDrawer = function () {
        const title = 'THÊM DANH MỤC THIẾT BỊ MỚI';
        drawer = new KTDrawer({ title, id: 'new', className: 'dmms-drawer--device' });

        drawer.on('ready', () => {
            $.ajax({
                url: '/suppliesdevice/createdevice',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    var initSuppliesDrawer = function () {
        const title = 'THÊM DANH MỤC VẬT TƯ MỚI';
        drawer = new KTDrawer({ title, id: 'new', className: 'dmms-drawer--supplies' });

        drawer.on('ready', () => {
            $.ajax({
                url: '/suppliesdevice/createsupplies',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-edit-device')
            .on('click', '.js-edit-device', function () {

                initDeviceDrawer();
            })
            .off('click', '.js-edit-supplies')
            .on('click', '.js-edit-supplies', function () {

                initSuppliesDrawer();
            })
            .on('change', '.check-all', function () {
                const $this = $(this);

                var isCheckAll = $this.find('input[type="checkbox"]').prop('checked');
                var checkboxInBody = $(".kt-form-right--body").find('input[type="checkbox"]');

                if (isCheckAll === true) {
                    checkboxInBody.attr('checked', true);
                }
                else {
                    checkboxInBody.attr('checked', false);
                }

            })
            .on('click', '#kt-close', function () {
                drawer.close()
            });
    };

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-supplies__form');
        const $formDevice = $content.find('.dmms-device__form');
        initForm($form);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

        var form = $("#example-form");

        form.steps({
            headerTag: "h6",
            bodyTag: "section",
            transitionEffect: "fade",
            titleTemplate: '<span class="step">#title# <i class="la la-angle-right"></i></span>',
            showFinishButtonAlways: true,
            labels: {
                cancel: "Hủy",
                current: "current step:",
                pagination: "Pagination",
                finish: "Tạo",
                next: "Thêm danh mục con",
                previous: "Lùi",
                loading: "Đang tải ..."
            },
        });
    };

    var initForm = function ($form) {
        $form.validate({
            rules: {

            },
            messages: {

            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {

                const data = $(form).serializeJSON() || {};
                const { id } = data;
                $.ajax({
                    url: id ? `/suppliesdevice/editdevice/${id}` : 'suppliesdevice/createdevice',
                    method: id ? 'PUT' : 'POST',
                    data,
                })
                    .done(function () {
                        KTApp.block($form);
                        const successMsg = id ? 'Sửa thiết bị vật tư thành công!' : 'Thêm thiết bị vật tư thành công!';
                        $.notify(successMsg, { type: 'success' });

                        /**
                         * Close drawer
                         */
                        drawer.close();

                        reloadTable();
                    })
                    .fail(function () {
                        KTApp.unblock($form);
                        const errorMsg = id ? 'Sửa thiết bị vật tư không thành công!' : 'Thêm thiết bị vật tư không thành công!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }

    return {
        // Public functions
        init: function () {
            initEventListeners();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-supplies').length) {
        DMMSSuppliesDevice.init();
    }
});
