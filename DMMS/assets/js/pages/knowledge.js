"use strict";
// Class definition

var DMMSKnowledge = function () {
    // Private functions

    var dataTableInstance = null;
    var dataTableInstance1 = null;
    var drawer = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    // demo initializer
    var initTable = function () {
        var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
        if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
            $.notify(editSucessCache.massage, { type: 'success' });
            localStorage.removeItem('edit_success');
        }
        dataTableInstance = $('.kt-datatable').KTDatatable({

            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/Knowledge/gets',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page,
                                CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            //Gán các thuộc tính của images{0} vào đối tượng item
                            if (raw.items != null) {
                                (raw.items).forEach(function (e) {

                                    e.path = e.image != null ? e.image.path : null;

                                    return e;


                                })
                            }


                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,
            },

            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 20,
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT /*+` <a data-id="${data.uuid}" class = " la la-comment baseqa" style="font-size: 17px;margin-right: -21%"></a>`*/;
                    },
                    textAlign:'center',
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'name',
                    title: 'Chủ đề câu hỏi',


                },
                {
                    field: 'time_created_format',
                    title: 'Ngày tạo',
                    width: 110,

                },
                {
                    field: 'time_last_update_format',
                    title: 'Ngày cập nhật',
                    width: 110,

                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 110,
                    overflow: 'visible',

                    textAlign: 'center',
                    template: function (data) {
                        return `
                           <a data-id="${data.uuid}" title="Chỉnh sửa" class="js-knowledge btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </a>
                            <button data-id="${data.uuid}" title="Xóa" class="js-delete-supplier btn btn-sm btn-danger btn-icon btn-icon-md ml-2">
                                <i class="la la-trash"></i>
                            </button>
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };
    var initSearch = function () {
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            })


    };
    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var deleteSupplier = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/knowledge/delete/${id}`,
                method: 'DELETE',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa chủ đề thành công!', { type: 'success' });

                    reloadTable();
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa không thành công!', { type: 'danger' });
                });
        }
    };

    var initSupplierDrawer = function (id) {
        const title = id ? 'Chỉnh sửa chủ đề câu hỏi' : 'Thêm Chủ đề câu hỏi';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--knowledge' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/knowledge/edit/${id}` : '/knowledge/create',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);

                    jQuery.fn.preventDoubleSubmission = function () {
                        $(this).on('submit', function (e) {
                            var $form = $(this);

                            if ($form.data('submitted') === true) {
                                // Previously submitted - don't submit again
                                e.preventDefault();
                            } else {
                                // Mark it so that the next submit can be ignored
                                $form.data('submitted', true);
                            }
                        });

                        // Keep chainability
                        return this;
                    };
                    $('form.dmms-knowledge__form').preventDoubleSubmission();
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    //var initSupplierDrawer = function (id) {
    //    const title = 'Danh sách thiết bị chi tiết';
    //    drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--detaildevice' });

    //    drawer.on('ready', () => {
    //        $.ajax({
    //            url: id ? `/knowledge/edit/${id}` : '/knowledge/create',
    //            method: 'GET',
    //        })
    //            .done(function (response) {
    //                debugger
    //                const $content = drawer.setBody(response);
    //                initContent($content);
                   
                   
                    
    //            })
    //            .fail(function () {
    //                drawer.error();
    //            });
    //    });

    //    drawer.open();
    //};
    var initSupplierDrawer1 = function (id) {
        const title = id ? 'Chi tiết câu hỏi câu trả lời' : 'Thêm câu hỏi và trả lời';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--knowledgeqa' });

        drawer.on('ready', () => {
            $.ajax({
                url: `/knowledge/qa/${id}`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    $('.summernote').summernote({
                        dialogsInBody: true,
                        placeholder: '',
                        fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New'],
                        tabsize: 2,
                        height: 200,
                        callbacks: {
                            onImageUpload: function (files, editor, welEditable) {
                                for (var i = 0; i < files.length; i++) {
                                    SendFile(files[i], this);
                                }

                            },
                        },
                    });
                    function SendFile(file, el) {
                        var data = new FormData();
                        data.append("file", file);

                        //CallAjax("/images/upload", data, function (url) {
                        //    debugger

                        //    $(el).summernote('editor.insertImage', url);

                        //}, "post");
                        $.ajax({
                            url: "/images/upload",
                            type: 'POST',
                            data: data,
                            cache: false,
                            dataType: 'json',
                            processData: false, // Don't process the files
                            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                            success: function (data, textStatus, jqXHR) {
                                var url = "";
                                for (var i = 0; i < data.length; i++) {
                                    url = "http://35.240.155.147:1237/" + data[i].path;
                                    $(el).summernote('editor.insertImage', url);
                                }


                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                //Handle errors here


                            }
                        });
                    }
                    initContent($content);

                    jQuery.fn.preventDoubleSubmission = function () {
                        $(this).on('submit', function (e) {
                            var $form = $(this);

                            if ($form.data('submitted') === true) {
                                // Previously submitted - don't submit again
                                e.preventDefault();
                            } else {
                                // Mark it so that the next submit can be ignored
                                $form.data('submitted', true);
                            }
                        });

                        // Keep chainability
                        return this;
                    };
                    $('form.dmms-knowledge__formqa').preventDoubleSubmission();
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-supplier')
            .on('click', '.js-delete-supplier', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Chủ đề sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteSupplier(id);
                    }
                });
            })

            .off('click', '.js-edit-knowledge')
            .on('click', '.js-edit-knowledge', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initSupplierDrawer(id || '');
            })
            .off('click', '.baseqa')
            .on('click', '.baseqa', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initSupplierDrawer1(id || '');
            })
            .off('click', '.js-knowledge')
            .on('click', '.js-knowledge', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initSupplierDrawer(id || '');
            });
    };
    var initForm = function ($form) {
        initValidation(true);
    }
    var initForm1 = function ($form1) {
        initValidation1(true);
    }
    var initValidation = function (isForm) {
        $(".dmms-knowledge__form").validate({
            rules: {
                name: {
                    required: true,
                },


            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên câu hỏi.'
                },


            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('.dmms-knowledge__form', -200);
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                console.log(data, uuid);
                $.ajax({
                    url: uuid ? `/knowledge/edit` : 'knowledge/create',
                    method: uuid ? 'PUT' : 'POST',
                    data,
                })
                    .done(function () {
                        $(".submit").attr("disabled", true);
                        KTApp.block('.dmms-knowledge__form');
                        const successMsg = uuid ? 'Sửa thông tin thành công!' : 'Thêm câu hỏi thành công!';
                        $.notify(successMsg, { type: 'success' });
                        if (isForm) {
                            drawer.close();
                            reloadTable();
                        }
                    })
                    .fail(function () {
                        done = true;
                        KTApp.unblock('.dmms-knowledge__form');
                        const errorMsg = uuid ? 'Sửa thông tin không thành công!' : 'Thêm câu hỏi không thành công!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }
    var initValidation1 = function (isForm) {
        $(".dmms-knowledge__formqa").validate({
            rules: {
                name: {
                    required: true,
                },


            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên câu hỏi.'
                },


            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('.dmms-knowledge__form', -200);
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                console.log(data, uuid);
                $.ajax({
                    url: 'knowledge/qa',
                    method: 'POST',
                    data,
                })
                    .done(function () {
                        $(".submit").attr("disabled", true);
                        KTApp.block('.dmms-knowledge__formqa');
                        const successMsg = uuid ? 'Sửa thông tin thành công!' : 'Thêm câu hỏi thành công!';
                        $.notify(successMsg, { type: 'success' });
                        if (isForm) {
                            drawer.close();
                            reloadTable();
                        }
                    })
                    .fail(function () {
                        done = true;
                        KTApp.unblock('.dmms-knowledge__formqa');
                        const errorMsg = uuid ? 'Sửa thông tin không thành công!' : 'Thêm câu hỏi không thành công!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }

    var initContent = function ($content) {
        /*
            Init form
        */
        dataTableInstance1 = $('.kt-datatable1').KTDatatable({

            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/Knowledge/gets',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page,
                                CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            //Gán các thuộc tính của images{0} vào đối tượng item
                            if (raw.items != null) {
                                (raw.items).forEach(function (e) {

                                    e.path = e.image != null ? e.image.path : null;

                                    return e;


                                })
                            }


                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,
            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 35,
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    },
                    autoHide: false,
                    textAlign: 'center',
                    
                },
                {
                    field: 'name',
                    title: 'Tên thiết bị',
                    autoHide: false,

                },
                {
                    field: 'Serial',
                    title: 'Ngày tạo',
                    width: 110,
                    autoHide: false,
                },
                {
                    field: 'EMEI',
                    title: 'Ngày cập nhật',
                    width: 110,
                    autoHide: false,
                },
                {
                    field: 'EMEI',
                    title: 'Nhà cung cấp',
                    width: 110,
                    autoHide: false,
                },
                {
                    field: 'EMEI',
                    title: 'Ngày hết hạn BH',
                    width: 110,
                    autoHide: false,
                },
               
                {
                    field: 'state',
                    title: 'Trạng thái',
                    width: 150,
                    template: function (data) {
                        var states = {
                            1: { 'title': 'Đang sử dụng', 'class': 'kt-badge--brand' },
                            2: { 'title': 'Đang yêu cầu sửa chữa', 'class': ' kt-badge--warning' },
                            3: { 'title': 'Đến lịch bảo hành', 'class': ' kt-badge--primary' },
                            4: { 'title': 'Đang xử lý theo yêu cầu', 'class': 'kt-badge--success' },
                            5: { 'title': 'Cần thay thế', 'class': 'kt-badge--danger' },
                            6: { 'title': 'Mang đi sửa chữa - Không có thiết bị thay thế', 'class': 'kt-badge--info' },
                            7: { 'title': 'Mang đi sửa chữa - Có thiết bị thay thế', 'class': 'kt-badge--info' },
                            8: { 'title': 'Thay thế', 'class': 'kt-badge--info' },
                            9: { 'title': 'Hỏng', 'class': 'kt-badge--info' },
                        };
                        if (data.state) {
                            return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill">' + states[data.state].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới</span>';
                        }
                    },
                    autoHide: false,
                    width: 150,
                    textAlign: 'center'
                },
               ],
            sortable: false,
            pagination: true,
            responsive: true,
        });
        const $form = $content.find('.dmms-knowledge__form');
        initForm($form);
        const $form1 = $content.find('.dmms-knowledge__formqa');
        initForm1($form1);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));


        /**
         * Init Dropzone
         */
        //initDropzone();
    };



    return {
        // Public functions
        init: function () {
            initTable();
            initSearch();
            initEventListeners();
            initValidation();
            initValidation1();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-knowledge').length) {
        DMMSKnowledge.init();
    }
});
