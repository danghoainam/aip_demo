﻿"use strict";
// Class definition

var DMMSCategory = function () {
    // Private functions

    var dataTableInstance = null;

    var drawer = null;

    function subTableInit(e) {
        console.log(e.data.id);
        $('<div/>').attr('id', 'child_data_ajax_' + e.data.children).appendTo(e.detailCell).KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/category/gets',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            return {
                                data: raw.items || [],

                            }                           
                        },
                    },
                  
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const { keyword } = query;
                        return {
                            keyword: keyword || '',
                        };
                    }
                },
                serverPaging: true,
                serverFiltering: true,
            },

            // layout definition
            layout: {
                scroll: true,
                height: 300,
                footer: false,

                // enable/disable datatable spinner.
                spinner: {
                    type: 1,
                    theme: 'default',
                },
            },

            sortable: true,
            pagination: false,
            // columns definition

            columns: [
                //{
                //    field: '',
                //    title: '#',
                //    width: 20,
                //    textAlign: 'center',
                //    template: function (data, row) {
                //        return (row + 1);
                //    }
                //},
                {
                    field: 'name',
                    title: 'Tên loại thiết bị con',                 
                },
                {
                    field: 'description',
                    title: 'Mô tả',
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 80,
                    overflow: 'visible',
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        return `
                            <button data-id="${data.id}" title="Chỉnh sửa" class="js-edit-category btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </button>
                            <button data-id="${data.id}" title="Xóa" class="js-delete-category btn btn-sm btn-danger btn-icon btn-icon-md">
                                <i class="la la-trash"></i>
                            </button>
                        `;
                    },
                }],
        });
    }

    // demo initializer
    var initTable = function () {
        dataTableInstance = $('.kt-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/category/gets',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            return {
                                data: raw.items || [],
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const { keyword } = query;
                        return {
                            keyword: keyword || '',
                        };
                    }
                },
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                input: $('#js-filter-keyword'),
            },
            detail: {
                title: 'Đang tải dữ liệu ...',
                content: subTableInit,
            },

            columns: [
                {
                    field: 'id',
                    title: '',
                    width: 10,
                    textAlign: 'center',
                },
                {
                    field: '',
                    title: '#',
                    width: 20,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1);
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'name',
                    title: 'Tên loại thiết bị',
                },
                {
                    field: 'description',
                    title: 'Mô tả',
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 70,
                    overflow: 'visible',
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        return `
                            <button data-id="${data.id}" title="Chỉnh sửa" class="js-edit-category btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </button>
                            <button data-id="${data.id}" title="Xóa" class="js-delete-category btn btn-sm btn-danger btn-icon btn-icon-md">
                                <i class="la la-trash"></i>
                            </button>
                        `;
                    },
                }],
            sortable: false,
            pagination: false,
            responsive: true,
        });
    };

    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var deleteCategory = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa loại thiết bị...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/categpory/delete/${id}`,
                method: 'DELETE',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa loại thiết bị thành công!', { type: 'success' });

                    if (dataTableInstance) {
                        dataTableInstance.reload();
                    }
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa loại thiết bị không thành công!', { type: 'danger' });
                });
        }
    };

    var initCategoryDrawer = function (id) {
        const title = id ? 'Sửa thông tin loại thiết bị' : 'Thêm loại thiết bị';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--category' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/category/edit/${id}` : '/category/create',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-category')
            .on('click', '.js-delete-category', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Loại thiết bị sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteCategory(id);
                    }
                });
            })
            .off('click', '.js-edit-category')
            .on('click', '.js-edit-category', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initCategoryDrawer(id || '');
            });

    };

    var initForm = function ($form) {
        $form.validate({
            rules: {
                name: {
                    required: true,
                },
                description: {
                    required: true,
                },
            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên loại thiết bị.'
                },
                description: {
                    required: 'Vui lòng nhập mô tả.'
                }
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {

                const data = $(form).serializeJSON() || {};
                const { id } = data;
                console.log(data, id);
                $.ajax({
                    url: id ? `/category/edit/${id}` : 'category/create',
                    method: id ? 'PUT' : 'POST',
                    data,
                })
                    .done(function () {
                        KTApp.block($form);
                        const successMsg = id ? 'Sửa thông tin thành công!' : 'Thêm loại thiết bị thành công!';
                        $.notify(successMsg, { type: 'success' });

                        /**
                         * Close drawer
                         */
                        drawer.close();

                        reloadTable();
                    })
                    .fail(function () {
                        KTApp.unblock($form);
                        const errorMsg = id ? 'Sửa thông tin không thành công!' : 'Thêm loại thiết bị không thành công!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-category__form');
        initForm($form);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

        //$('#kt-select-devicecategory').selectpicker();
        if ($("#parent_id").val() !== '') {
            $('#kt-select-devicecategory').selectpicker('val', $("#parent_id").val());
        }
        else {
            $('#kt-select-devicecategory').selectpicker();
        }
    };

    return {
        // Public functions
        init: function () {
            initTable();
            initEventListeners();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-category').length) {
        DMMSCategory.init();
    }
});
