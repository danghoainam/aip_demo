"use strict";
// Class definition

var DMMSTaskstate = function () {
    // Private functions

    var dataTableInstance = null;

    var drawer = null;

    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var uuid_images = "";

    // demo initializer
    var initTable = function () {
        dataTableInstance = $('.kt-datatable').KTDatatable({

            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/taskstate/gets',
                    },
                    response: {
                        map: function (res) {

                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page;
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            (raw.items).forEach(function (e) {
                                var pathdata = [];
                                //e.path = e.images.length > 0 ? e.images[0].path : null;
                                if (e.images.length > 0) {
                                    for (var i = 0; i < e.images.length; i++) {
                                        pathdata.push(e.images[i].path || null);

                                        //return e;
                                    }
                                }

                                return e.path = pathdata;



                            })
                            console.log(res, raw, pagination);

                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: pagination.perpage || LIMIT,
                                    page: pagination.page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { task_uuid, state } = query;
                        return {
                            limit: pagination.perpage || LIMIT,
                            page: pagination.page,
                            task_uuid: task_uuid || $('#js-filter-task').val(),
                            state: state || $('#js-filter-states').val(),

                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 50,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    }
                },
                {
                    field: 'date_report_format',
                    title: 'Ngày tạo',
                    textAlign: 'center',
                    width:110,
                }, {
                    field: 'note',
                    title: 'Ghi chú',
                },
                {
                    field: 'path',
                    title: 'anh',
                    template: function (data) {
                        var html = '';

                        if (data.images.length > 0) {
                            debugger
                            for (var i = 0; i < data.images.length; i++) {



                                html = html + ' <img src="' + window.imageDomain + data.images[i].path + '" width="100px">'


                            }

                        }
                        return html;
                    },
                },

                {
                    field: 'state',
                    title: 'Trạng Thái',
                    textAlign: 'center',
                    template: function (data) {
                        var states = {
                            1: { 'title': 'Mới', 'class': 'kt-badge--brand' },
                            2: { 'title': 'Yêu cầu hồ trợ', 'class': ' kt-badge--warning' },
                            3: { 'title': 'Đã tiếp nhận', 'class': ' kt-badge--primary' },
                            4: { 'title': 'Đã hẹn lịch', 'class': 'kt-badge--success' },
                            5: { 'title': 'Đang xử lý', 'class': 'kt - badge--danger' },
                            6: { 'title': 'Đợi duyệt đề xuất xử lý', 'class': 'kt-badge--info' },
                            7: { 'title': 'Yêu cầu tiếp tục', 'class': 'kt-badge--info' },
                            8: { 'title': 'Hoàn thành', 'class': 'kt-badge--info' },

                        };
                        if (data.state) {
                            if (states[data.state] !== undefined) {
                                return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill">' + states[data.state].title + '</span>';
                            }
                            else {
                                return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Chưa rõ</span>';
                            }
                        }
                        else {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Chưa rõ</span>';
                        }
                    }
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    textAlign: 'center',
                    sortable: false,
                    width: 100,
                    overflow: 'visible',
                    autoHide: false,
                    template: function (data) {
                        return `
                            <button data-id="${data.uuid}" title="Chỉnh sửa" class="js-edit-taskstate btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </button>
                            <button data-id="${data.uuid}" title="Xóa" class="js-delete-taskstate btn btn-sm btn-danger btn-icon btn-icon-md ml-2">
                                <i class="la la-trash"></i>
                            </button>
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };
    var initSearch = function () {
        $('#js-filter-task')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'task_uuid');
                }
            })
            .selectpicker();
        $('#js-filter-states')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'task_uuid');
                }
            })
            .selectpicker();

    };
    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var deleteTaskState = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa trạng thái công việc...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/taskstate/delete/${id}`,
                method: 'DELETE',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa trạng thái công việc thành công!', { type: 'success' });

                    reloadTable();
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa trạng thái công việc không thành công!', { type: 'danger' });
                });
        }
    };

    var initTaskStateDrawer = function (id) {
        const title = id ? 'Sửa thông tin trạng thái công việc' : 'Thêm trạng thái công việc';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--taskstate' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/taskstate/edit/${id}` : '/taskstate/create',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-taskstate')
            .on('click', '.js-delete-taskstate', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Trạng thái công việc sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteTaskState(id);
                    }
                });
            })
            .off('click', '.js-edit-taskstate')
            .on('click', '.js-edit-taskstate', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initTaskStateDrawer(id || '');
            });

    };

    var initForm = function ($form) {
        debugger;
        $form.validate({
            rules: {
                task_uuid: {
                    required: true,
                },
                note: {
                    required: true,
                },
                state: {
                    required: true,
                },
            },
            messages: {
                task_uuid: {
                    required: 'Vui chọn công việc.'
                },
                note: {
                    required: 'Vui lòng nhập ghi chú.'
                },
                state: {
                    required: 'Vui lòng chọn trạng thái.'
                }
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {
                debugger;
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                console.log(data, uuid);
                $.ajax({
                    url: uuid ? `/taskstate/edit/${uuid}` : 'taskstate/create',
                    method: uuid ? 'PUT' : 'POST',
                    data,
                })
                    .done(function () {
                        KTApp.block($form);
                        const successMsg = uuid ? 'Sửa thông tin thành công!' : 'Thêm mới thành công!';
                        $.notify(successMsg, { type: 'success' });

                        /**
                         * Close drawer
                         */
                        drawer.close();

                        reloadTable();
                    })
                    .fail(function () {
                        KTApp.unblock($form);
                        const errorMsg = uuid ? 'Sửa thông tin không thành công!' : 'Thêm mới không thành công!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-taskstate__form');
        initForm($form);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));
        $('#js-filter-taskstate').selectpicker();
        $('#kt-multiple-select-taskstate').selectpicker();
        $('#kt-multiple-select-taskstate1').selectpicker();

        /**
         * Init Dropzone
         */
        initDropzone();




    };

    var initDropzone = function () {
        debugger
        new Dropzone('#kt_dropzone_image', {
            url: "/images/upload",
            paramName: "images",
            maxFiles: 4,
            maxFilesize: 1, // MB           
            addRemoveLinks: true,
            removedfile: function (file) {
                var _ref;
                debugger
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            },
            dictDefaultMessage: 'Bấm hoặc kéo thả ảnh vào đây để tải ảnh lên',
            dictRemoveFile: 'Xóa ảnh',
            dictCancelUpload: 'Hủy',
            dictCancelUploadConfirmation: 'Bạn thực sự muốn hủy?',
            dictMaxFilesExceeded: 'Bạn không thể tải thêm ảnh!',
            dictFileTooBig: "Ảnh tải lên dung lượng quá lớn ({{filesize}}MB). Max filesize: {{maxFilesize}}MB.",
            accept: function (file, done) {
                done();
            },

            success: function (file, response) {
                if (response != null && response.length > 0) {
                    var uuid_images = $('#uuid_images').val();
                    for (var i = 0; i < response.length; i++) {
                        uuid_images = uuid_images + "," + response[i].id;
                        $('#uuid_images').val(uuid_images);
                    }

                    console.log(response, uuid_images, $('#uuid_images').val());

                }
                else {
                    $(file.previewElement).addClass("dz-error").find('.dz-error-message').text("Không tải được ảnh lên.");
                }

            },
            error: function (file, response, errorMessage) {
                console.log(file, response);
                if (file.accepted) {
                    $(file.previewElement).addClass("dz-error").find('.dz-error-message').text("Có lỗi xảy ra, không tải được ảnh lên.");
                }
                else {
                    $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(response);
                }
            }
        });
    }

    return {
        // Public functions
        init: function () {
            initTable();
            initEventListeners();
            initSearch();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-taskstate').length) {
        DMMSTaskstate.init();

    }
});
