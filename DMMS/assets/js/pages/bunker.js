﻿"use strict";
// Class definition

var DMMSBunker = function () {
    // Private functions

    var dataTableInstance = null;
    var drawer = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    var MAX_ITEM_INPAGE = 0;

    var initTable = function () {
        dataTableInstance = $('.kt-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: `/bunker/gets`,
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? 0 : pagination.page;
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { state, province_id, town_id, village_id, keyword } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            province_id: province_id || $('#js-filter-provinceID').val(),
                            town_id: town_id || $('#js-filter-townID').val(),
                            village_id: village_id || $('#js-filter-villageID').val(),
                            state: state || $('#js-filter-state').val(),
                            keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,

            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 30,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'code',
                    title: 'Mã kho',
                    width:110
                },
                {
                    field: 'name',
                    title: 'Tên kho vật tư - thiết bị',
                },
                {
                    field: 'province.name',
                    title: 'Tỉnh/thành',
                    width: 110,
                },
                {
                    field: 'town.name',
                    title: 'Quận/huyện',
                    width: 110,
                },
                {
                    field: 'village.name',
                    title: 'Phường/xã',
                    width: 110,
                    autoHiden: false,
                },
                {
                    field: 'address',
                    title: 'Địa chỉ chi tiết',
                },
                {
                    field: 'state',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    width: 100,
                    autoHide: false,
                    template: function (data) {
                        var states = {
                            0: { 'title': 'Tạm ngưng', 'class': 'kt-badge--danger' },
                            1: { 'title': 'Hoạt động', 'class': ' kt-badge--success' },
                            2: { 'title': 'Kiểm kê', 'class': ' kt-badge--primary' },
                        };
                        if (data.state) {
                            return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill">' + states[data.state].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge ' + states[0].class + ' kt-badge--inline kt-badge--pill">' + states[0].title + '</span>';
                        }
                    }
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 90,
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        return `
                            <a href="/bunker/edit/${data.uuid}" title="Chi tiết" class="btn btn-sm btn-success btn-icon btn-icon-md">
                                <i class="la la-info"></i>
                            </a>
                            <buttom data-id="${data.uuid}" title="Xóa" class="btn btn-sm btn-danger btn-icon btn-icon-md js-delete-bunker">
                                <i class="la la-trash"></i>
                            </buttom>
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };

    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var select = function () {
        const is_create = '';

        $('#kt-select-bunker').selectpicker();
        if ($('#state').val() !== '') {
            $('#kt-select-state').selectpicker('val', $('#state').val());
        }
        else $('#kt-select-state').selectpicker();

        if ($('#province_id').val() !== '') {
            $('#kt-select-province').selectpicker('val', $('#province_id').val());
        }
        else $('#kt-select-province').selectpicker();

        if ($('#town_id').val() !== '') {
            $('#kt-select-town').selectpicker('val', $('#town_id').val());
        }
        else $('#kt-select-town').selectpicker();

        if ($('#village_id').val() !== '') {
            $('#kt-select-village').selectpicker('val', $('#village_id').val());
        }
        else $('#kt-select-village').selectpicker();

        $('#kt-select-province').on('change', function () {
            changeLocationProvince($(this).val(), is_create);
        })

        $('#kt-select-town').on('change', function () {

            changeLocationTown($(this).val(), is_create);
        });

        if ($('#unitSignedUuid').val() != '') {
            $('#kt-select-unitsigned').selectpicker('val', $('#unitSignedUuid').val());
        }
        else $('#kt-select-unitsigned').selectpicker();

        //$('#select-choose').on('change', function () {
        //    if ($('#select-choose').prop('checked')) {
        //        $('#input-create').prop('disabled', true);
        //        $('#kt-select-unitsigned').prop('disabled', false).selectpicker('refresh');
        //    }
        //})
        //$('#create-new').on('change', function () {
        //    if ($('#create-new').prop('checked')) {
        //        $('#input-create').prop('disabled', false);
        //        $('#kt-select-unitsigned').prop('disabled', true).selectpicker('refresh');
        //    }
        //})

        $('#kt-select-unitsigned').on('change', function () {
            if ($(this).val() == "") {
                $('.remove_unitsigneds').remove();
                $('.append_unitsigneds').append(`<input name="unitSignedName" class="form-control" placeholder="Đơn vị ký hợp đồng" />`);
                $('.append_unitsigneds').addClass('kt-input-icon col-9');
            }
        })
    }

    var initSearch = function () {
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });

        $('#js-filter-province')
            .on('change', function () {
                changeLocationProvince($(this).val());
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val(), 'province_id');
                    START_PAGE = 1;
                }
            }).selectpicker();

        $('#js-filter-town')
            .on('change', function () {
                changeLocationTown($(this).val());
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val(), 'town_id');
                    START_PAGE = 1;
                }
            }).selectpicker();

        $('#js-filter-village')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val(), 'village_id');
                    START_PAGE = 1;
                }
            }).selectpicker();

        $('#js-filter-state')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val(), 'state');
                    START_PAGE = 1;
                }
            }).selectpicker();
    };

    var changeLocationProvince = function (id, is_create) {
        const selectTown = is_create === undefined ? $('#js-filter-town') : $('#kt-select-town');
        const selectVillages = is_create === undefined ? $('#js-filter-village') : $('#kt-select-village');
        const title = 'Quận/huyện';
        const titleVillages = 'Phường/xã';

        selectTown.html('');
        selectTown.attr('title', title);
        selectTown.append(`<option value="">${title}</option>`);
        selectTown.selectpicker('refresh');

        selectVillages.html('');
        selectVillages.attr('title', titleVillages);
        selectVillages.append(`<option value="">${titleVillages}</option>`);
        selectVillages.selectpicker('refresh');

        if (id !== '') {
            $.ajax({
                url: `/bunker/gettowns/${id}`,
                method: 'GET'
            })
                .done(function (res) {
                    $.each(res, function (index, item) {
                        selectTown.append(`<option value="${item.id}">${item.name}</option>`);
                        selectTown.selectpicker('refresh');
                    });
                });
        }
        else {
            selectTown.html('');
            selectTown.attr('title', title);
            selectTown.append(`<option value="">${title}</option>`);
            selectTown.selectpicker('refresh');

            selectVillages.html('');
            selectVillages.attr('title', titleVillages);
            selectVillages.append(`<option value="">${titleVillages}</option>`);
            selectVillages.selectpicker('refresh');
        }
    }

    var changeLocationTown = function (id, is_create) {
        const selectVillages = is_create === undefined ? $('#js-filter-village') : $('#kt-select-village');
        const title = 'Phường/xã';
        selectVillages.html('');
        selectVillages.append(`<option value="">${title}</option>`);
        selectVillages.selectpicker('refresh');
        if (id !== '') {
            $.ajax({
                url: `/bunker/getvillages/${id}`,
                method: 'GET'
            })
                .done(function (res) {
                    $.each(res, function (index, item) {
                        selectVillages.append(`<option value="${item.id}">${item.name}</option>`);
                        selectVillages.selectpicker('refresh');
                    })
                });
        }
        else {
            selectVillages.html('');
            selectVillages.attr('title', title);
            selectVillages.selectpicker('refresh');
        }
    }

    var deleteBunker = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa kho...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/bunker/delete/${id}`,
                method: 'DELETE',
            })
                .done(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify("Xóa thành công!", { type: 'success' });

                    if (dataTableInstance) {
                        dataTableInstance.reload();
                    }
                })
                .fail(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(data.responseJSON.msg, { type: 'danger' });
                });
        }
    };

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-bunker')
            .on('click', '.js-delete-bunker', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Kho sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteBunker(id);
                    }
                });
            })
            .off('click', '.js-add-bunker')
            .on('click', '.js-add-bunker', function () {

                initEditDrawer();
            });
    };

    var initEditDrawer = function () {
        const title = 'Thêm mới kho';
        drawer = new KTDrawer({ title, uuid: 'new', className: 'dmms-drawer--bunker' });

        drawer.on('ready', () => {
            $.ajax({
                url: '/bunker/create',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);

                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    var initForm = function (is_form) {
        const $form = $('.dmms-bunker__form');
        $form.validate({
            rules: {
                name: {
                    required: true,
                },
                address: {
                    required: true,
                },
                province_id: {
                    required: true,
                },
                town_id: {
                    required: true,
                },
                village_id: {
                    required: true,
                },
                "storage_keeper[]": {
                    required: true,
                },
            },
            messages: {
                name: {
                    required: "Vui lòng nhập tên kho!",
                },
                address: {
                    required: "Vui lòng nhập địa chỉ!",
                },
                province_id: {
                    required: "Vui lòng nhập tỉnh thành!",
                },
                town_id: {
                    required: "Vui lòng nhập quận huyện!",
                },
                village_id: {
                    required: "Vui lòng nhập phường xã!",
                },
                "storage_keeper[]": {
                    required: "Vui lòng nhập nhân viên!",
                },
            },

            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
                $('#kt-select-province').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });
                $('#kt-select-town').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });
                $('#kt-select-village').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });
                $('#kt-select-staffss').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                $.ajax({
                    url: uuid ? `/bunker/edit/${uuid}` : '/bunker/create',
                    method: uuid ? 'PUT' : 'POST',
                    data,
                })
                    .done(function () {
                        $(form).find('button[type="submit"]').prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading
                        KTApp.block($form);

                        const successMsg = uuid ? 'Sửa thông tin thành công!' : 'Thêm thông tin thành công!';
                        $.notify(successMsg, { type: 'success' });
                        if (is_form) {
                            /**
                             * Close drawer
                             */
                            drawer.close();

                            reloadTable();
                        }
                        else window.location = `/bunker/edit/${uuid}`;
                    })
                    .fail(function () {
                        KTApp.unblock($form);
                        $(form).find('button[type="submit"]').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
                        const successMsg = uuid ? 'Sửa thông tin không thành công!' : 'Thêm thông tin không thành công!';
                        $.notify(data.responseJSON.msg, { type: 'danger' });
                    });
                return false;
            },
        });
    };

    var initContent = function ($content) {
        /*
            Init form
        */
        initForm(true);

        /*
            Init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

        $('#kt-select-province').selectpicker();
        $('#kt-select-town').selectpicker();
        $('#kt-select-village').selectpicker();
        $('#kt-select-staffss').selectpicker();
        $('#kt-select-unitsigned').selectpicker();

        var is_create = '';
        $('#kt-select-province').on('change', function () {

            changeLocationProvince($(this).val(), is_create)
        })

        $('#kt-select-town').on('change', function () {

            changeLocationTown($(this).val(), is_create)
        })

        $('#kt-select-unitsigned').on('change', function () {
            const name_temp = $('#kt-select-unitsigned').find(`option[value="${$(this).val()}"]`).text();

            if ($(this).val() != '')
                $('#wrapper-fill_unitsigned').val(name_temp);
        })

        $('#kt-select-staffss').on('change', function () {
            const $this = $(this);
            var isAllSelected = $this.find('option[value=""]').prop('selected');
            var lastAllSelected = $(this).data('all');
            var selectedOptions = ($this.val()) ? $this.val() : [];
            var allOptionsLength = $this.find('option[value!=""]').length;
            var selectedOptionsLength = selectedOptions.length;

            if (isAllSelected == lastAllSelected) {
                if ($.inArray("", selectedOptions) >= 0) {
                    selectedOptionsLength -= 1;
                }

                if (allOptionsLength <= selectedOptionsLength) {

                    $this.find('option[value=""]').prop('selected', true).parent().selectpicker('refresh');
                    isAllSelected = true;
                } else {
                    $this.find('option[value=""]').prop('selected', false).parent().selectpicker('refresh');
                    isAllSelected = false;
                }

            } else {
                $this.find('option').prop('selected', isAllSelected).parent().selectpicker('refresh');
            }

            var text_display = $('.kt-selectbox').find('.filter-option-inner-inner').text();
            $('.kt-selectbox').find('.filter-option-inner-inner').html(text_display.replace('Chọn tất cả, ', ''));

            $(this).data('all', isAllSelected);
        }).trigger('change');
    };

    return {
        // Public functions
        init: function () {
            initTable();
            initSearch();
            initEventListeners();
            if (window.location.pathname !== "/bunker") {
                select();
                initForm();
            }
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-bunker').length) {
        DMMSBunker.init();
    }
});