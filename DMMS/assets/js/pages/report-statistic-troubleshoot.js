﻿"use strict";
// Class definition

var DMMSProject = function () {
    // Private functions

    var dataTableInstance = null;
    var drawer = null;
    var Packagetable = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    // demo initializer

    let tFrom = moment().subtract(6, 'days').format('YYYY-MM-DD').toString();
    let tTo = moment().format('YYYY-MM-DD').toString();

    var startDatePicker = function () {
        $('#kt_dashboard_daterangepicker_title').html("");
        $("#kt_dashboard_daterangepicker_date").text(moment().subtract(6, 'days').format('DD-MM-YYYY').toString() + " - " + moment().format('DD-MM-YYYY').toString());
    }

    var daterangepickerInit = function () {
        if ($('#kt_dashboard_daterangepicker').length == 0) {
            return;
        }
        var picker = $('#kt_dashboard_daterangepicker');
        var start = moment().subtract(6, 'days');
        var end = moment();
        function cb(start, end, label) {
            var title = '';
            var range = '';
            if ((end - start) < 100 || label == 'Hôm nay') {
                title = 'Hôm nay: ';
                range = start.format('DD-MM-YYYY');
            } else if (label == 'Hôm qua') {
                title = 'Hôm qua: ';
                range = start.format('DD-MM-YYYY');
            } else {
                range = start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY');
            }
            tFrom = start.format('YYYY-MM-DD');
            tTo = end.format('YYYY-MM-DD');
            $('#charpie1').remove();
            $('.charpie1').append('<div id="charpie1"></div>');
            $('#charpie2').remove();
            $('.charpie2').append('<div id="charpie2"></div>');
            $('#charpie3').remove();
            $('.charpie3').append('<div id="charpie3"></div>');
            $('#chartmix').remove();
            $('.chartmix').append('<div id="chartmix"></div>');
            initTable(tFrom, tTo);
            $('#kt_dashboard_daterangepicker_date').html(range);
            $('#kt_dashboard_daterangepicker_title').html(title);
            //landd start-----------------------
            $('#charttroules').remove();
            $('.charttroules').append('<div id="charttroules"></div>');
            lineChartProjectInit(tFrom, tTo);
            //landd end-----------------------
        }
        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Hôm nay': [moment(), moment()],
                'Hôm qua': [moment().subtract(1, 'days'), moment()],
                '7 ngày qua': [moment().subtract(6, 'days'), moment()],
                '30 ngày qua': [moment().subtract(29, 'days'), moment()],
                'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            locale: {
                applyLabel: 'Áp dụng',
                cancelLabel: 'Hủy bỏ',
                customRangeLabel: 'Tùy chỉnh',
                firstDay: 1
            }
        }, cb);
    }

    var initTable = function (timeFrom, timeTo) {
        if (dataTableInstance != null && dataTableInstance != undefined) dataTableInstance.destroy();
        dataTableInstance = $('.kt-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/ReportOverview/GetReportStatisticTroubleshoot',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? 0 : pagination.page;
                            CURRENT_PAGE = page || 1;
                            START_PAGE = 0;
                            LIMIT = perpage;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { customerUuid, state, keyword } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            customerUuid: customerUuid || $('#js-filter-customer').val(),
                            state: state || $('#js-filter-state').val(),
                            keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            timeFrom: timeFrom,
                            timeTo: timeTo,
                            page: pagination.page,

                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                //input: $('#js-filter-keyword'),
                onEnter: true,
            },
            columns: [
                {
                    field: 'ticket_uuid',
                    title: 'STT',
                    width: 30,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'ticket_code',
                    title: 'Mã phiếu',
                    width: 70
                },
                {
                    field: 'ticket_title',
                    title: 'Tiêu đề sự cố',

                },
                {
                    field: 'customer_name',
                    title: 'Đơn vị triển khai',
                    width: 100
                },
                {
                    field: 'contact_name',
                    title: 'Người liên hệ',
                    width: 90
                },
                {
                    field: 'priority',
                    title: 'Độ ưu tiên',
                    width: 40,
                    textAlign: 'center'
                },
                {
                    field: 'creator_customer_name',
                    title: 'Người tạo',
                    width: 90
                },
                {
                    field: 'time_created_format_nohh',
                    title: 'Thời gian tạo',
                    width: 80
                },
                {
                    field: '',
                    title: 'Nguồn tạo',
                    width: 100,
                    template: function (data) {
                        //var states = {
                        //    1: { 'title': 'Khách hàng', 'class': 'kt-badge--brand' },
                        //    2: { 'title': 'Nhân viên', 'class': ' kt-badge--warning' },
                        //};
                        if (data.creator_customer_name) {
                            return '<span>Khách hàng: ' + data.creator_customer_name + '</span>';
                        }
                        else if (data.creator_staff_name) {
                            return '<span>Nhân viên: ' + data.creator_staff_name + '</span>';
                        }
                        else {
                            return '<span></span>';
                        }
                    },
                    width: 100,
                },
                {
                    field: 'state',
                    title: 'Trạng thái',
                    width: 100,
                    template: function (data) {
                        var states = {
                            1: { 'title': 'Mới', 'class': 'kt-badge--brand' },
                            3: { 'title': 'Đang xử lý', 'class': ' kt-badge--warning' },
                            4: { 'title': 'Đã xử lý xong', 'class': ' kt-badge--primary' },
                            5: { 'title': 'Hoàn thành', 'class': 'kt-badge--success' },
                            2: { 'title': 'Đã tiếp nhận', 'class': 'kt-badge--danger' },
                            6: { 'title': 'Đã hủy', 'class': 'kt-badge--danger' },
                        };
                        if (data.state) {
                            return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill">' + states[data.state].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới</span>';
                        }
                    },
                    width: 100,
                    textAlign: 'center'
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };

    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var initSearch = function () {
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            }).selectpicker();
        $('#js-filter-customer')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'customerUuid');
                    START_PAGE = 1;
                }
            }).selectpicker();
        $('#js-filter-state')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'state');
                    START_PAGE = 1;
                }
            }).selectpicker();

        //if ($("#location_id").val() !== '') {
        //    $('#kt-select-location').selectpicker('val', $("#location_id").val());
        //}
        //else {
        //    $('#kt-select-location').selectpicker();
        //}
    };

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-project__form');
        initForm($form);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

    };

    //start - landd thêm biểu đồ 
    var initBaoCaoSuCo = function (categories, data1, data2, data3) {
        var optionsmix = {
            series: [{
                name: 'Yêu cầu tạo bởi NVKT',
                type: 'column',
                data: data1
            }, {
                name: 'Yêu cầu tạo từ CSKH',
                type: 'column',
                data: data2
            }, {
                name: 'Tổng',
                type: 'line',
                data: data3
            }],
            chart: {
                height: 500,
                type: 'line',
                // stacked: false,
                toolbar: {
                    show: false
                }
            },
            colors: ['#FF931E', '#36B37E', '#189EFF'],
            dataLabels: {
                enabled: false
            },
            xaxis: {
                categories: categories,
            },
            yaxis: {
                labels: {
                    formatter: function (val) {
                        return val.toFixed(0)
                    }
                }
            },
            stroke: {
                width: [1, 1, 4],

            },
            markers: {
                size: 4,
                colors: ["#FFFFFF"],
                strokeColors: "#189EFF",
                strokeWidth: 2,
                hover: {
                    size: 7,
                }
            },
            tooltip: {
                fixed: {
                    enabled: true,
                    position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
                    offsetY: 30,
                    offsetX: 60
                },
            },
            legend: {
                markers: {
                    radius: 0
                }
              /*  horizontalAlign: 'left',
                offsetX: 40*/
            }
        };
        var chartmix = new ApexCharts(document.querySelector("#charttroules"), optionsmix);
        chartmix.render();
    }

    var lineChartProjectInit = function (tFrom, tTo) {
        let categoriesTicket = [];
        let data1Ticket = [];
        let data2Ticket = [];
        let data3Ticket = [];
        var checkShow = 0;
        KTApp.block($('.w-100'));
        $.ajax({
            url: `/ticketoverview/all`,
            method: 'POST',
            cache: false,
            data: {
                request: {
                    timeFrom: tFrom, timeTo: tTo
                }
            }
        })
            .done(function (res) {
                if (res) {
                    var sumTiket = 0;
                    //var data = res.
                    $.each(res, function (index, item) {
                        var bookmarkData1 = 0;
                        var bookmarkData2 = 0;
                        var bookmarkTotal = 0;
                        checkShow = 0;
                        $.each(item, function (index, item) {
                            if (item.type == 100) {
                                bookmarkData1 += parseInt(item.count);
                                bookmarkData2 += parseInt(item.count);
                                bookmarkTotal += parseInt(item.count);
                                checkShow = 1;
                            }
                            if (item.type == "1") {
                                bookmarkData1 += parseInt(item.count);
                                bookmarkTotal += parseInt(item.count);
                                checkShow = 1;
                            }
                            if (item.type == "2") {
                                bookmarkData2 += parseInt(item.count);
                                bookmarkTotal += parseInt(item.count);
                                checkShow = 1;
                            }
                        });
                        if (checkShow == 1) {
                            categoriesTicket.push(index);
                            data1Ticket.push(parseInt(bookmarkData1));
                            data2Ticket.push(parseInt(bookmarkData2));
                            data3Ticket.push(parseInt(bookmarkTotal));
                            sumTiket += bookmarkTotal;
                        }
                    });
                    $("#tongSoTicketdetail").text(sumTiket);                    
                    initBaoCaoSuCo(categoriesTicket, data1Ticket, data2Ticket, data3Ticket);
                    KTApp.unblock($('.w-100'));
                }
            })
            .fail(function () {
                $.notify('Có lỗi xảy ra, vui lòng thử lại.', { type: 'danger' });
            });
    }
    //end landd----------------------

    return {
        // Public functions
        init: function () {
            startDatePicker();
            initTable(tFrom, tTo);
            initSearch();
            daterangepickerInit();
            lineChartProjectInit(tFrom, tTo)
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-reportstatistictroubleshoot').length) {
        DMMSProject.init();
    }
});
