﻿"use strict";
// Class definition

var DMMSStaffListByTeamLead = function () {
    // Private functions

    var dataTableInstance = null;

    var LIMIT = 20;
    var CURRENT_PAGE = 1;

    // demo initializer
    var initTable = function () {
        dataTableInstance = $('.kt-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/staff/getsListByTeamLead',
                    }, 
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            CURRENT_PAGE = pagination.page || 1;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: pagination.perpage || LIMIT,
                                    page: pagination.page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword, team_uuid } = query;
                        return {
                            limit: pagination.perpage || LIMIT,
                            team_uuid: team_uuid || $('#js-filter-team').val(),
                            page: pagination.page,
                            keyword: keyword || '',
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                input: $('#js-filter-keyword'),

            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 50,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    }
                },{
                    field: 'name',
                    title: 'Tên nhân viên',
                }, {
                    field: 'phone_number',
                    title: 'Số điện thoại',
                }, {
                    field: 'State',
                    title: 'trạng thái',
                    //}, {
                    //    field: 'phone_number',
                    //    title: 'Số điện thoại',
                    //}, {
                    //    field: 'email',
                    //    title: 'Email',
                    //}, {
                    //    field: 'address',
                    //    title: 'Địa chỉ',
                }, {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 110,
                    overflow: 'visible',
                    autoHide: false,
                    template: function (data) {
                        return `
                            <a href="/staff/update/${data.uuid}" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </a>
                            <button data-id="${data.uuid}" type="button" title="Xóa" class="btn btn-sm btn-danger btn-icon btn-icon-md js-delete-staff ml-2">
                                <i class="la la-trash"></i>
                            </button>
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };

    var initSearch = function () {
        $('#js-filter-team')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'team_uuid');
                }
            })
            .selectpicker();
        $('#kt_form_team_add').selectpicker();
        $('#kt_form_team_update').selectpicker();
    };

    var deleteStaff = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa nhân viên...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/staff-list-by-teamlead/delete/${id}`,
                method: 'DELETE',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa nhân viên thành công!', { type: 'success' });

                    if (dataTableInstance) {
                        dataTableInstance.reload();
                    }
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa nhân viên không thành công!', { type: 'danger' });
                });
        }
    };

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-staff-list-by-teamlead')
            .on('click', '.js-delete-staff-list-by-teamlead', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Nhân viên sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteStaff(id);
                    }
                });
            });

    };

    var initValidation = function () {
        $("#dmms-staff-list-by-teamlead__form").validate({
            rules: {

                name: {
                    required: true,
                },


            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên nhân viên.'
                },

            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('#dmms-staff-list-by-teamlead__form', -200);
            },
            submitHandler: function (form) {
                form[0].submit(); // submit the form
            },
        });
    };

    var initDropzone = function () {
        var dropZone = $('#kt_dropzone_image');
        if (dropZone !== undefined && dropZone.length > 0) {
            new Dropzone('#kt_dropzone_image', {
                url: "/images/upload",
                paramName: "images",
                maxFiles: 1,
                maxFilesize: 5, // MB
                addRemoveLinks: true,
                dictDefaultMessage: 'Bấm hoặc kéo thả ảnh vào đây để tải ảnh lên',
                dictRemoveFile: 'Xóa ảnh',
                dictCancelUpload: 'Hủy',
                dictCancelUploadConfirmation: 'Bạn thực sự muốn hủy?',
                dictMaxFilesExceeded: 'Bạn không thể tải thêm ảnh!',
                dictFileTooBig: "Ảnh tải lên dung lượng quá lớn ({{filesize}}MB). Max filesize: {{maxFilesize}}MB.",
                autoProcessQueue: true,
                accept: function (file, done) {
                    done();
                },
                success: function (file, response) {
                    if (response != null && response.length > 0) {
                        $("#image_uuid").val(response[0].id);
                        //$("#imgAvatar").attr('src', "https://api.ngot.club/" + response[0].path);
                    }
                    else {
                        $(file.previewElement).addClass("dz-error").find('.dz-error-message').text("Không tải được ảnh lên.");
                    }
                },
                error: function (file, response, errorMessage) {
                    console.log(file, response);
                    if (file.accepted) {
                        $(file.previewElement).addClass("dz-error").find('.dz-error-message').text("Có lỗi xảy ra, không tải được ảnh lên.");
                    }
                    else {
                        $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(response);
                    }
                }
            });
        }
    }

    return {
        // Public functions
        init: function () {
            initTable();
            initSearch();
            initValidation();
            initEventListeners();
            initDropzone();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-staff-list-by-teamlead').length) {
        DMMSStaffListByTeamLead.init();
    }
});
