﻿"use strict";
// Class definition

var DMMSCustomer = function () {
    // Private functions

    var dataTableInstance = null;
    var dataTableInstancepackage = null;
    var drawer = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    var drawerInstance = null;
    var MAX_ITEM_INPAGE = 0;

    //table contact
    $(".list-contact").on('click', function () {
        const customer_uuid = document.getElementById("uuid").value;
        if (dataTableInstance) {
            dataTableInstance.load()
        }
        else {
            dataTableInstance = $('.kt-datatable--contact').KTDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: `/customer/getcontact/${customer_uuid}`,
                        },
                        response: {
                            map: function (res) {
                                const raw = res && res.data || {};
                                const { pagination } = raw;
                                const perpage = pagination === undefined ? undefined : pagination.perpage;
                                const page = pagination === undefined ? undefined : pagination.page
                                CURRENT_PAGE = page || 1;
                                START_PAGE = 0;
                                return {
                                    data: raw.items || [],
                                    meta: {
                                        ...pagination || {},
                                        perpage: perpage || LIMIT,
                                        page: page || 1,
                                    },
                                }
                            },
                        },
                        filter: function (data = {}) {
                            const query = data.query || {};
                            const pagination = data.pagination || {};
                            const { keyword, state, start_page } = query;
                            MAX_ITEM_INPAGE = pagination.perpage || LIMIT;
                            if (START_PAGE === 1) {
                                pagination.page = START_PAGE
                            }
                            return {
                                limit: MAX_ITEM_INPAGE,
                                keyword: (keyword || $('#js-filter-keyword').val()),
                                state: (state || $('#js-filter-statecontact').val()),
                                page: pagination.page
                            };
                        }
                    },
                    pageSize: LIMIT,
                    serverPaging: true,
                    serverFiltering: true,
                },

                search: {
                    onEnter: true,
                },
                columns: [
                    {
                        field: '',
                        title: '#',
                        sortable: false,
                        width: 20,
                        textAlign: 'center',
                        template: function (data, row) {
                            return (row + 1) + (CURRENT_PAGE - 1) * MAX_ITEM_INPAGE;
                        },
                        responsive: {
                            visible: 'md',
                            hidden: 'sm'
                        }
                    },
                    {
                        field: 'name',
                        title: 'Người liên hệ',

                    },
                    {
                        field: 'duty',
                        title: 'Chức vụ',
                        width: 110
                    },
                    {
                        field: 'phone_number',
                        title: 'Số điện thoại',
                        width: 110
                    },
                    //{
                    //    field: 'state',
                    //    title: 'Trạng thái',
                    //    width: 90,
                    //    textAlign: 'center',
                    //    template: function (data) {
                    //        var states = {
                    //            1: { 'title': 'Khóa vĩnh viễn', 'class': 'kt-badge--danger' },
                    //            2: { 'title': 'Mở khóa', 'class': 'kt-badge--success' }
                    //        };
                    //        if (data.state) {
                    //            return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill">' + states[data.state].title + '</span>'; 
                    //        }
                    //        else {
                    //            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Mở khóa</span>';
                    //        }
                    //    }
                    //},
                    {
                        field: 'Tác vụ',
                        title: 'Tác vụ',
                        sortable: false,
                        textAlign: 'center',
                        overflow: 'visible',
                        width: 70,
                        autoHide: false,
                        template: function (data) {
                            return `
                            <button data-id="${data.uuid}" data-customer_uuid="${data.customer_uuid}" type="button" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md js-edit-contact">
                                <i class="la la-edit"></i>
                            </button>
                            <button data-id="${data.uuid}" type="button" title="Xóa" class="btn btn-sm btn-danger btn-icon btn-icon-md js-delete-contact">
                                <i class="la la-trash"></i>
                            </button>
                        `;
                        },
                    }],
                sortable: false,
                pagination: true,
                responsive: true,
            });
        }
       
    });
    $(".list-package").on('click', function () {
        var customerUuid = $('#uuid').val();
        if (dataTableInstancepackage) {
            dataTableInstancepackage.load()
        }
        else {
            dataTableInstancepackage = $('.kt-datatable--package').KTDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: `/customer/GetProject/${customerUuid}`,
                        },
                        response: {
                            map: function (res) {
                                const raw = res && res.data || {};
                                const { pagination } = raw;
                                const perpage = pagination === undefined ? undefined : pagination.perpage;
                                const page = pagination === undefined ? undefined : pagination.page
                                CURRENT_PAGE = page || 1;
                                START_PAGE = 0;
                                return {
                                    data: raw.items || [],
                                    meta: {
                                        ...pagination || {},
                                        perpage: perpage || LIMIT,
                                        page: page || 1,
                                    },
                                }
                            },
                        },
                        filter: function (data = {}) {
                            const query = data.query || {};
                            const pagination = data.pagination || {};
                            const { keyword, state, start_page } = query;
                            MAX_ITEM_INPAGE = pagination.perpage || LIMIT;
                            if (START_PAGE === 1) {
                                pagination.page = START_PAGE
                            }
                            return {
                                limit: MAX_ITEM_INPAGE,
                                keyword: (keyword || $('#js-filter-keyword').val()),
                                state: (state || $('#js-filter-statecontact').val()),
                                page: pagination.page
                            };
                        }
                    },
                    pageSize: LIMIT,
                    serverPaging: true,
                    serverFiltering: true,
                },

                search: {
                    onEnter: true,
                },
                columns: [
                    {
                        field: '',
                        title: '#',
                        sortable: false,
                        width: 20,
                        textAlign: 'center',
                        template: function (data, row) {
                            return (row + 1) + (CURRENT_PAGE - 1) * MAX_ITEM_INPAGE;
                        },
                        responsive: {
                            visible: 'md',
                            hidden: 'sm'
                        }
                    },
                    {
                        field: 'code',
                        title: 'Mã dự án',
                        width: 100,
                    },
                    {
                        field: 'name',
                        title: 'Tên dự án',
                        template: function (data) {
                            return `<a href="/project/edit/${data.uuid}" title="Thông tin dự án"">
                                    ${data.name}
                                </a>`;
                        },

                    },
                    {
                        field: 'field.fieldName',
                        title: 'Lĩnh vực',
                        width:100

                    },
                    {
                        field: 'packageCount',
                        title: 'Số phòng triển khai',
                        width: 70
                    },
                    {
                        field: 'contract.code',
                        title: 'Hợp đồng',
                        width: 110
                    },
                    {
                        field: 'contract.costEstimate_format',
                        title: 'Giá trị hợp đồng',
                        width: 110
                    },
                    {
                        field: 'state.id',
                        title: 'Trạng thái',
                        width: 120,
                        textAlign: 'center',
                        template: function (data) {
                            debugger
                            var states = {
                                1: { 'title': 'Còn HSD', 'class': 'kt-badge--success' },
                                2: { 'title': 'Qúa HSD', 'class': ' kt-badge--warning' },
                                3: { 'title': 'Đã đóng', 'class': ' kt-badge--danger' },
                                4: { 'title': 'Qúa HSD', 'class': ' kt-badge--warning' },
                                5: { 'title': 'Qúa HSD', 'class': ' kt-badge--warning' },
                            };
                            if (data.contract.state.id) {
                                return '<span class="kt-badge ' + states[data.contract.state.id].class + ' kt-badge--inline kt-badge--pill">' + data.contract.state.name + '</span>';
                            }
                            else {
                                return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Chưa rõ</span>';
                            }
                        }
                    },
                ],
                sortable: false,
                pagination: true,
                responsive: true,
            });
        }
       
    });
    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var initSearch = function () {
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });
        $('#js-filter-statecontact')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'state');
                    START_PAGE = 1;
                }
            }).selectpicker();
    };

    var deleteContact = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa liên hệ...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/customer/deletecontact/${id}`,
                method: 'DELETE',
            })
                .done(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(data.msg, { type: 'success' });

                    if (dataTableInstance) {
                        dataTableInstance.reload();
                    }
                })
                .fail(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(data.responseJSON.msg, { type: 'danger' });
                });
        }
    };

    var initEditContactDrawer = function (id, customer_uuid) {
        console.log(customer_uuid);
        const title = id ? 'Cập nhật thông tin liên hệ' : 'Thêm mới thông tin liên hệ';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--contact' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/customer/updatecontact/${id}` : `/customer/createcontact/${customer_uuid}`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-contact')
            .on('click', '.js-delete-contact', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Liên hệ sẽ bị xóa khỏi danh sách.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteContact(id);
                    }
                });
            })
            .off('click', '.js-edit-contact')
            .on('click', '.js-edit-contact', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                const customer_uuid = $this.attr('data-customer_uuid');

                initEditContactDrawer(id, customer_uuid || '', customer_uuid);
            })
    };

    //edit or crate thông tin liên hệ
    var initForm = function ($form) {
        const number_regex = /^(0|\+)[0-9\s.-]{9,11}$/;
        $form.validate({
            rules: {
                name: {
                    required: true,
                },
                //phone_number: {
                //    phoneVN: true,
                //    required: true,
                //},
            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên.'
                },
                //phone_number: {
                //    required: 'Vui lòng nhập số điện thoại.'
                //},
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {
                $(form).find('button[type="submit"]').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                if (number_regex.test($('#phone_number').val()) == false) {
                    $(form).find('button[type="submit"]').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    $.notify('Số điện thoại sai định dạng!', { type: 'danger' });
                }
                else {
                    $.ajax({
                        url: uuid ? `/customer/updateContact/${uuid}` : '/customer/createcontact',
                        method: uuid ? 'PUT' : 'POST',
                        data,
                    })
                        .done(function (data) {
                            KTApp.block($form);
                            $.notify(data.msg, { type: 'success' });

                            /**
                             * Close drawer
                             */
                            drawer.close();

                            reloadTable();
                        })
                        .fail(function () {
                            $(form).find('button[type="submit"]').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            KTApp.unblock($form);
                            $.notify(data.responseJSON.msg, { type: 'danger' });
                        });
                }
                return false;
            },
        });
    }

    //Thêm thông tin liên hệ
    //var initValidationContact = function () {
    //    $(".dmms-contact__form").validate({
    //        rules: {
    //            name: {
    //                required: true,
    //            },
    //            phone_number: {
    //                phoneVN: true,
    //                required: true,
    //            },
    //        },
    //        messages: {
    //            name: {
    //                required: 'Vui lòng nhập tên.'
    //            },
    //            phone_number: {
    //                required: 'Vui lòng nhập số điện thoại.'
    //            },
    //        },

    //        //display error alert on form submit  
    //        invalidHandler: function (event, validator) {
    //            KTUtil.scrollTo('.dmms-contact__form', -200);
    //        },
    //        submitHandler: function (form) {
    //            const data = $(form).serializeJSON() || {};
    //            const { uuid } = data;
    //            $.ajax({
    //                url: '/customer/createContact',
    //                method: 'POST',
    //                data,
    //            })
    //                .done(function (data) {
    //                    KTApp.block('.dmms-contact__form');
    //                    $.notify(data.msg, { type: 'success' });
    //                    reloadTable();
    //                    //localStorage['edit_success'] = JSON.stringify({ from: (uuid ? `/customer/updateContact` : '/customer/createContact'), message: data.msg });

    //                    //window.location.replace("customer");
    //                    window.location = window.location.origin + `/customer`;

    //                })
    //                .fail(function (data) {
    //                    $.notify(data.responseJSON.msg, { type: 'danger' });
    //                });
    //            return false;
    //        },

    //    });
    //}

    var initContent = function ($content) {
        /*
        Init form
        */
        const $form = $content.find('.dmms-contact__form');

        initForm($form);

        /*
        Init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));
    };

    return {
        // Public functions
        init: function () {
            //initUserForm();
            initSearch();
            //initValidationContact();
            initEventListeners();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-customer').length) {
        DMMSCustomer.init();
    }
});