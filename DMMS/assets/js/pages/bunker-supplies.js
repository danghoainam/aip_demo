﻿"use strict";

// Class definition

var DMMSBunker = function () {
    // Private functions

    var dataTableInstance = null;
    var subDataTableInstance = null;
    var drawer = null;
    var subDrawer = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    var devices = [];
    var selected = [];
    $('.bunker-tab-supplies').on('click', function () {
        if (dataTableInstance !== null) dataTableInstance.destroy();
        const storage_uuid = $('#uuid').val();
        dataTableInstance = $('.kt-datatable--supplies').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: `/bunker/getSuppliesDeviceInBunker/${storage_uuid}`,
                        method: 'GET',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? 0 : pagination.page;
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { category_id, keyword } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            category_id: category_id || $('#js-filter-category').val(),
                            keyword: keyword || $('.js-filter-keyword--supplies').val(),
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,

            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 30,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'name',
                    title: 'Tên vật tư-thiết bị',
                    template: function (data) {
                        var output = '';
                        if (data.prefix_name !== '') {
                            output = `<span class="text-primary">${data.prefix_name}</span> <br />
                                <span>${data.name}</span>`;
                        }
                        else output = `<span>${data.name}</span>`;
                        return output;
                    },
                },
                //{
                //    field: 'code',
                //    title: 'Mã vật tư',
                //},
                {
                    field: 'type_format',
                    title: 'Phân loại',
                },
                {
                    field: 'quantity_exist',
                    title: 'SL tồn kho',
                    width: 70,
                },
                {
                    field: 'unit',
                    title: 'Đơn vị tính',
                    width: 70,
                },
                {
                    field: 'state',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    width: 100,
                    template: function (data) {
                        var states = {
                            0: { 'title': 'Hoạt động', 'class': 'kt-badge--success' },
                            1: { 'title': 'Tạm ngưng', 'class': ' kt-badge--danger' },
                        };
                        if (data.state) {
                            return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill">' + states[data.state].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge ' + states[0].class + ' kt-badge--inline kt-badge--pill">' + states[0].title + '</span>';
                        }
                    }
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 50,
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        var output = '';
                        if (data.type === 1) {
                            output = `<button data-storage="${storage_uuid}" data-category=${data.category_id} type="button" title="Chi tiết"
                                            class="btn btn-sm btn-success btn-icon btn-icon-md js-detail-supplies">
                                <i class="la la-info"></i>
                            </button>`;
                        }
                        else {
                            output = `<button data-storage="${storage_uuid}" type="button" title="Chi tiết" class="btn btn-sm btn-success btn-icon btn-icon-md disabled">
                                <i class="la la-info"></i>
                            </button>`;
                        }
                        return output;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    });

    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var initSearch = function () {
        //Begin: filter by category
        const category = $('.js-filter-category');
        category.selectpicker();
        category.on('change', function () {
            const max_index = 4;
            const id = $(this).val();
            const index = parseInt($(this).attr('data-index'));
            const wrapper = $('.filter-zone');

            if (id != '') {
                $.ajax({
                    url: `/bunker/getcategory/${id}`,
                })
                    .done(function (data) {
                        const $parent = $(`.js-filter-category[data-index="${index + 1}"]`).parent('div').parent('div');
                        if (index < max_index && $parent.hasClass('kt-hidden')) {
                            $parent.removeClass('kt-hidden');
                        }
                        $(`.js-filter-category[data-index="${index + 1}"]`).attr('title', 'Lọc theo danh mục').html('<option value="">Lọc theo danh mục</option>');
                        category.selectpicker('refresh');
                        $.each(data, function (idx, item) {
                            $(`.js-filter-category[data-index="${index + 1}"]`).append(`<option value="${item.id}">${item.name}</option>`);
                            $(`.js-filter-category`).selectpicker('refresh');
                        });
                        for (var i = index; i <= max_index; i++) {
                            $(`.js-filter-category[data-index="${i + 2}"]`).attr('title', 'Lọc theo danh mục').html('<option value="">Lọc theo danh mục</option>');
                            $(`.js-filter-category[data-index="${i + 2}"]`).selectpicker('refresh');
                        }
                    });
            }
            else {
                for (var i = index; i <= max_index; i++) {
                    if (i <= max_index - 2) {
                        $(`.js-filter-category[data-index="${i + 2}"]`).parent('div').parent('div').addClass('kt-hidden');
                        $(`.js-filter-category[data-index="${i + 1}"]`).attr('title', 'Lọc theo danh mục').html('<option value="">Lọc theo danh mục</option>');
                        $(`.js-filter-category[data-index="${i + 1}"]`).selectpicker('refresh');
                    }
                    else {
                        $(`.js-filter-category[data-index="${i + 1}"]`).attr('title', 'Lọc theo danh mục').html('<option value="">Lọc theo danh mục</option>');
                        $(`.js-filter-category[data-index="${i + 1}"]`).selectpicker('refresh');
                    }
                }
            }
            $('.btn-filter_category').on('click', function () {
                if (dataTableInstance) {
                    dataTableInstance.search(id, 'category_id');
                    START_PAGE = 1;
                }
            });
        });
        //End: filter by category

        $('#js-filter-keyword--supplies')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });

    };

    var importSupplies = function (type, storage_uuid, name) {
        devices = [];
        const type_name = { 1: "Nhân viên KT", 2: "Nhà cung cấp", 3: "Kho" };
        const title = `<span class="text-uppercase">PHIẾU NHẬP KHO THIẾT BỊ - VẬT TƯ THEO ${type_name[type]}</span>`;

        drawer = new KTDrawer({ title, uuid: 'new', className: 'dmms-drawer--importsupplier' });

        drawer.on('ready', () => {
            $.ajax({
                url: `/bunker/importsupplies/${type}/${storage_uuid}/${name}`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);

                    initContent($content, type);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    var exportSupplies = function (type, storage_uuid, name) {
        devices = [];
        const type_name = { 1: "Nhân viên KT", 2: "Nhà cung cấp", 3: "Kho", 4: "Thanh lý", 5: "Xuất tiêu hao" };

        const title = `<span class="text-uppercase">PHIẾU XUẤT KHO THIẾT BỊ - VẬT TƯ ĐẾN ${type_name[type]}</span>`;
        drawer = new KTDrawer({ title, uuid: 'new', className: 'dmms-drawer--exportsupplies' });

        drawer.on('ready', () => {
            $.ajax({
                url: `/bunker/exportsupplies/${type}/${storage_uuid}/${name}`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);

                    initContentExport($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    //xuat phieu tu de xuat
    var exportSuppliesoder = function (type, storage_uuid, name, oder_uuid) {

        devices = [];
        var storage_uuid = $('#store_uuid').val();
        const type_name = { 1: "Nhân viên KT", 2: "Nhà cung cấp", 3: "Kho", 4: "Thanh lý", 5: "Xuất tiêu hao" };

        const title = `<span class="text-uppercase">PHIẾU XUẤT KHO THIẾT BỊ - VẬT TƯ ĐẾN ${type_name[type]}</span>`;
        drawer = new KTDrawer({ title, uuid: 'new', className: 'dmms-drawer--exportsupplies' });
        //url: `/bunker/ExportToOrder/${type}/${storage_uuid}/${name}`,
        drawer.on('ready', () => {
            $.ajax({
                url: `/bunker/ExportToOrder/${storage_uuid}/${oder_uuid}`,
                method: 'GET',
                data: { oder_uuid: oder_uuid }
            })
                .done(function (response) {

                    const $content = drawer.setBody(response);
                    //kho dich
                    var name = $('.get_name').val();
                    var uuid = $('.get_uuid').val();
                    $('.to_name').val(name);
                    $('.formto_uuid').val(uuid);
                    //staff_uuid
                    //kho nguon
                    $('.from_uuid').val($('#store_uuid').val())
                    $('.to_staff_uuid').val($('.get_uuid_form').val())
                    //
                    //$('.fromto').val($('#from_name').val())
                    //$('.formto_uuid').val($('.get_uuid_to').val())


                    $('#document_origin').val($('.documentorigin').val())
                    $('.kt-select-supplies').selectpicker('refresh');
                    initContentExport($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var exportSuppliesoderto = function (type, storage_uuid, name, oder_uuid) {

        devices = [];
        var storage_uuid = $('#store_uuid').val();
        const type_name = { 1: "Nhân viên KT", 2: "Nhà cung cấp", 3: "Kho", 4: "Thanh lý", 5: "Xuất tiêu hao" };

        const title = `<span class="text-uppercase">PHIẾU NHẬP KHO THIẾT BỊ - VẬT TƯ ĐẾN ${type_name[type]}</span>`;
        drawer = new KTDrawer({ title, uuid: 'new', className: 'dmms-drawer--importsupplierto' });
        //url: `/bunker/ExportToOrder/${type}/${storage_uuid}/${name}`,
        drawer.on('ready', () => {
            $.ajax({
                url: `/bunker/ExportToOrderTo/${storage_uuid}/${oder_uuid}`,
                method: 'GET',
                data: { oder_uuid: oder_uuid }
            })
                .done(function (response) {

                    const $content = drawer.setBody(response);
                    //kho dich
                    var name = $('.get_name').val();
                    var uuid = $('.get_uuid').val();
                    $('.to_name').val(name);
                    $('.formto_uuid').val(uuid);
                    //staff_uuid
                    //kho nguon
                    $('.from_uuid').val($('#store_uuid').val())
                    $('.to_staff_uuid').val($('.get_uuid_form').val())
                    //
                    //$('.fromto').val($('#from_name').val())
                    //$('.formto_uuid').val($('.get_uuid_to').val())
                    var type = $('#type').val();

                    $('#document_origin').val($('.documentorigin').val())
                    $('.kt-select-supplies').selectpicker('refresh');
                    initContentoderto($content, type);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var initDetailTable = function (storage_uuid, category_id) {

        subDataTableInstance = $('.kt-datatable--detailsuppliesdevice').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: `/bunker/getdetailsuppliesinbunker/${storage_uuid}/${category_id}`,
                        method: 'GET',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? 0 : pagination.page;
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const pagination = data.pagination || {};
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,

            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 30,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'name',
                    title: 'Tên vật tư-thiết bị',
                    template: function (data) {
                        var output = '';
                        if (data.prefix_name !== '') {
                            output = `<span class="text-primary">${data.prefix_name}</span> <br />
                                <span>${data.name}</span>`;
                        }
                        else output = `<span>${data.name}</span>`;
                        return output;
                    },
                },
                //{
                //    field: 'code',
                //    title: 'Mã vật tư',
                //},
                {
                    field: 'serial',
                    title: 'Serial',
                    width: 120,
                },
                //{
                //    field: 'imei',
                //    title: 'Imei',
                //    width: 70,
                //},
                {
                    field: 'expire_device_string',
                    title: 'Ngày hết hạn',
                    width: 120,
                },
                {
                    field: 'state',
                    title: 'Trạng thái',
                    width: 150,
                    template: function (data) {
                        var states = {
                            1: { 'title': 'Đang sử dụng', 'class': 'kt-badge--brand' },
                            2: { 'title': 'Đang yêu cầu sửa chữa', 'class': ' kt-badge--warning' },
                            3: { 'title': 'Đến lịch bảo hành', 'class': ' kt-badge--primary' },
                            4: { 'title': 'Đang xử lý theo yêu cầu', 'class': 'kt-badge--success' },
                            5: { 'title': 'Cần thay thế', 'class': 'kt-badge--danger' },
                            6: { 'title': 'Mang đi sửa chữa - Không có thiết bị thay thế', 'class': 'kt-badge--info' },
                            7: { 'title': 'Mang đi sửa chữa - Có thiết bị thay thế', 'class': 'kt-badge--info' },
                            8: { 'title': 'Thay thế', 'class': 'kt-badge--info' },
                            9: { 'title': 'Hỏng', 'class': 'kt-badge--info' },
                            10: { 'title': 'Thêm mới/Chỉnh sửa', 'class': 'kt-badge--info' },
                        };
                        if (data.state) {
                            return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill">' + states[data.state].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới</span>';
                        }
                    },
                    width: 150,
                    textAlign: 'center',
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    }

    var detailSupplies = function (storage_uuid, category_id) {

        const title = 'DANH SÁCH CHI TIẾT CHỦNG LOẠI THIẾT BỊ';
        drawer = new KTDrawer({ title, category_id: category_id || 'new', className: 'dmms-drawer--detailsupplies1' });

        drawer.on('ready', () => {
            $.ajax({
                url: '/bunker/detailsupplies',
                method: 'GET',
            })
                .done(function (response) {
                    drawer.setBody(response);
                    initDetailTable(storage_uuid, category_id);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    }

    var initEventListeners = function () {
        $(document)
            .off('click', '.import-supplies')
            .on('click', '.import-supplies', function () {
                const type = $(this).attr('data-type');
                const storage_uuid = $('#uuid').val();
                const name = $('#name').val();

                importSupplies(type, storage_uuid, name);
            })
            .off('click', '.export-supplies')
            .on('click', '.export-supplies', function () {
                const type = $(this).attr('data-type');
                const storage_uuid = $('#uuid').val();
                const name = $('#name').val();

                exportSupplies(type, storage_uuid, name);
            })
            .off('click', '.export-supplies-oder')
            .on('click', '.export-supplies-oder', function () {
                const type = 1;
                const storage_uuid = $('#uuid').val();
                const name = $('#name').val();
                const $this = $(this);
                const oder_uuid = $this.attr('data-id');
                exportSuppliesoder(type, storage_uuid, name, oder_uuid);
            })
            //.off('click', '.export-supplies-oder-to')
            //.on('click', '.export-supplies-oder-to', function () {
            //    
            //    var type = $('#type');
            //    const storage_uuid = $('#uuid').val();
            //    const name = $('#name').val();
            //    const $this = $(this);
            //    const oder_uuid = $this.attr('data-id');
            //    exportSuppliesoderto(type, storage_uuid, name, oder_uuid);
            //})
            .off('click', '.js-detail-supplies')
            .on('click', '.js-detail-supplies', function () {
                const $this = $(this);
                const storage_uuid = $this.attr('data-storage');
                const category_id = $this.attr('data-category');

                detailSupplies(storage_uuid, category_id);
            });
    };

    var initForm = function ($form) { //Save import
        $form.validate({
            rules: {
                //document_origin: {
                //    required: true
                //},
                //'detail[][category_id]': {
                //    required: true
                //},
                //'detail[][document_quantity]': {
                //    required: true
                //},
                //'detail[][real_quantity]': {
                //    required: true
                //},
                //'detail[][unit_price_string]': {
                //    required: true
                //}
            },
            messages: {
                //document_origin: {
                //    required: "Vui lòng nhập chứng từ!"
                //},
                //'detail[][category_id]': {
                //    required: "Vui lòng nhập mã thiết bị!"
                //},
                //'detail[][document_quantity]': {
                //    required: "Vui lòng nhập số lượng thiết bị!"
                //},
                //'detail[][real_quantity]': {
                //    required: "Vui lòng nhập số lượng thực!"
                //},
                //'detail[][unit_price_string]': {
                //    required: "Vui lòng nhập đơn giá!"
                //}
            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo($form, -200);
                $('#kt-select-supplies').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });
            },
            submitHandler: function (form) {
                $(form).find('button[type="submit"]').prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading

                const data = $(form).serializeJSON() || {};

                if (($('#type').val() == 2 || $('#type').val() == 3) && $('input[name="document_origin"]').val() == '') {
                    KTApp.unblock($form);
                    $(form).find('button[type="submit"]').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
                    $.notify('Nhập từ kho yêu nhập cầu chứng từ gốc!', { type: 'warning' });
                }
                else {
                    if (data.detail != undefined && $('#kt-select-supplies').val() == "") {
                        $.ajax({
                            url: `/bunker/importsupplies`,
                            method: 'POST',
                            data,
                        })
                            .done(function (data) {
                                KTApp.block($form);
                                $.notify('Thêm thông tin thành công!', { type: 'success' });

                                /**
                                 * Close drawer
                                 */
                                drawer.close();
                                reloadTable();
                            })
                            .fail(function (data) {
                                KTApp.unblock($form);
                                $(form).find('button[type="submit"]').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
                                $.notify(data.responseJSON.msg, { type: 'danger' });
                            });
                    }
                    else {
                        KTApp.unblock($form);
                        $(form).find('button[type="submit"]').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
                        $.notify('Thêm thiết bị vật tư trước khi lưu dữ liệu!', { type: 'warning' });
                    }
                }
                return false;
            },
        });
    };
    $('.detailexportto').on('click', function () {
        alert('xx')
    })
    var initFormExport = function ($formExport) {   //Save export
        $formExport.validate({
            rules: {
                from_staff_uuid: {
                    required: true
                }
            },
            messages: {
                from_staff_uuid: {
                    required: 'Vui lòng nhập nhân viên!'
                }
            },

            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($formExport, -200);
                $('#kt-select-staff').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });

            },
            submitHandler: function (form) {
                $(form).find('button[type="submit"]').prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading

                const data = $(form).serializeJSON() || {};
                if (data.detail != undefined && $('#kt-select-supplies').val() == "") {
                    $.ajax({
                        url: `/bunker/exportsupplies`,
                        method: 'POST',
                        data,
                    })
                        .done(function () {
                            KTApp.block($formExport);
                            $.notify('Thêm thông tin thành công!', { type: 'success' });

                            /**
                             * Close drawer
                             */
                            drawer.close();
                            reloadTable();
                        })
                        .fail(function (data) {
                            KTApp.unblock($formExport);
                            $(form).find('button[type="submit"]').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
                            $.notify(data.responseJSON.msg, { type: 'danger' });
                        });
                }
                else {
                    KTApp.unblock($formExport);
                    $(form).find('button[type="submit"]').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
                    $.notify('Thêm thiết bị vật tư trước khi lưu dữ liệu!', { type: 'warning' });
                }
                return false;
            },
        });
    };
    var initFormExportoder = function ($formExportoder) {   //Save export
        $formExportoder.validate({
            rules: {
                from_staff_uuid: {
                    required: true
                }
            },
            messages: {
                from_staff_uuid: {
                    required: 'Vui lòng nhập nhân viên!'
                }
            },

            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($formExportoder, -200);
                $('#kt-select-staff').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });

            },
            submitHandler: function (form) {
                $(form).find('button[type="submit"]').prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading

                const data = $(form).serializeJSON() || {};
                if ($('#state-of_order').val() == 1 || $('#state-of_order').val() == 5) {
                    $.ajax({
                        url: `/bunker/ExportToOrder`,
                        method: 'POST',
                        data,
                    })
                        .done(function () {
                            KTApp.block($formExportoder);
                            $.notify('Thêm thông tin thành công!', { type: 'success' });

                            /**
                             * Close drawer
                             */
                            drawer.close();
                            reloadTable();
                        })
                        .fail(function (data) {
                            KTApp.unblock($formExportoder);
                            $(form).find('button[type="submit"]').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
                            $.notify(data.responseJSON.msg, { type: 'danger' });
                        });
                }
                else {
                    KTApp.unblock($formExportoder);
                    $(form).find('button[type="submit"]').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
                    $.notify('Vui lòng duyệt đề xuất trước khi tạo phiếu xuất kho!', { type: 'danger' });
                }

                return false;
            },
        });
    };
    var initFormExportoderto = function ($formExportoderto) {   //Save export
        $formExportoderto.validate({
            rules: {
                to_staff_uuid: {
                    required: true
                }
            },
            messages: {
                from_staff_uuid: {
                    required: 'Vui lòng nhập nhân viên!'
                }
            },

            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($formExportoderto, -200);
                $('#kt-select-staff').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });

            },
            submitHandler: function (form) {
                $(form).find('button[type="submit"]').prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading

                const data = $(form).serializeJSON() || {};
                $.ajax({
                    url: `/bunker/ExportToOrderTo`,
                    method: 'POST',
                    data,
                })
                    .done(function () {
                        KTApp.block($formExportoderto);
                        $.notify('Thêm thông tin thành công!', { type: 'success' });

                        /**
                         * Close drawer
                         */
                        drawer.close();
                        reloadTable();
                    })
                    .fail(function (data) {
                        KTApp.unblock($formExportoderto);
                        $(form).find('button[type="submit"]').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
                        $.notify(data.responseJSON.msg, { type: 'danger' });
                    });
                return false;
            },
        });
    };
    var initContent = function ($content, type) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-form-bunker__importsupplies');
        //const $formExport = $content.find('.dmms-form-bunker__exportsupplies');
        initForm($form);
        //initFormExport($formExport);
        const $formExportoderto = $content.find('.dmms-form-bunker__exportsuppliesoderto');
        initFormExportoderto($formExportoderto);
        /*
            Init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

        //const storage_uuid = $('#uuid').val();
        var typecategories = 1;

        $('#unit_price').simpleMoneyFormat();
        $('#kt-select-supplies').selectpicker({ showSubtext: true });
        $('#kt-select-supplies').selectpicker('refresh');
        $('#kt-select-supplier').selectpicker();
        $('#kt-select-document_origin').selectpicker();

        $('#kt-select-document_origin').on('change', function () { //Chọn chứng từ gốc => fill kho nguồn
            const exportUuid = $(this).val();
            if (exportUuid != '') {
                const from_uuid = $('#kt-select-document_origin').find(`option[value="${exportUuid}"]`).attr('data-id');
                const from_name = $('#kt-select-document_origin').find(`option[value="${exportUuid}"]`).attr('data-name');
                const document = $('#kt-select-document_origin').find(`option[value="${exportUuid}"]`).attr('data-document');

                $('#wrapper-from_uuid').val(from_uuid);
                $('#wrapper-from_name').val(from_name);
                $('#wrapper-document_origin').val(document);

                $('#fill-exportUuid').val(exportUuid);

                $.ajax({
                    async: false,
                    url: `/bunker/DetailCategoriesExport/${exportUuid}`,
                    method: 'GET',
                    data: { limit: -1 }
                })
                    .done(function (res) {
                        $.each(res.data.items, function (index, item) {
                            $('#kt-select-supplies').append(`<option value="${item.id}" data-subtext="${item.name_display}">${item.prefixName}</option>`);
                        });
                        $('#kt-select-supplies').selectpicker('refresh');
                    })
                    .fail(function () {
                        $.notify('Lỗi hệ thống vui lòng thử lại!', { type: 'danger' });
                    });
            }
            else {
                $('#wrapper-from_uuid').val("");
                $('#wrapper-from_name').val("");

                $('#fill-exportUuid').val("");

                $('#kt-select-supplies').html(`<option value="">Lựa chọn</option>`);
                $('#kt-select-supplies').selectpicker('refresh');
            }
        });

        const storage_uuid = $('#uuid').val();
        if (type == 3) {
            $(`#kt-select-other option[value="${storage_uuid}"]`).attr('disabled', true);
        }
        $('#kt-select-other').selectpicker();

        $('#kt-select-other').on('change', function () {
            const storage_uuid = $(this).val();
            if (type == 1) $('input[name="from_staff_uuid"]').val(`${storage_uuid}`);
            $('#kt-select-supplies').html('').attr('title', 'Lựa chọn').append('<option value="">Lựa chọn</option>').selectpicker('refresh');
            $.ajax({
                async: false,
                url: `/bunker/getSuppliesDeviceInBunker/${storage_uuid}`,
                method: 'GET',
                data: { limit: -1 }
            })
                .done(function (res) {
                    $.each(res.data.items, function (index, item) {
                        $('#kt-select-supplies').append(`<option value="${item.category_id}" data-subtext="${item.name_display}">${item.prefix_name_format}</option>`);
                    });
                    $('#kt-select-supplies').selectpicker('refresh');
                })
        });

        //Change #kt-select-supplies
        $('#kt-select-supplies').on("change", function () {
            const type_categories = { 1: "Thiết bị", 2: "Vật tư", 3: "Dụng cụ LĐ", 4: "Phần mềm" };
            if (type == 1) var storage_uuid = $('#kt-select-other').val();
            if (type == 3) var storage_uuid = $('#wrapper-from_uuid').val();
            const wrapper = $('.wrapper-fill');
            const category = wrapper.find('#kt-select-supplies');
            const category_id = category.val();
            wrapper.find('#list_device').html('');
            if ($(this).val() != '') {
                $.ajax({
                    url: type == 2 ? `/bunker/DetailSuppliesAjax/${category_id}` : `/bunker/DetailSuppliesAjax/${category_id}/${storage_uuid}`,
                    method: 'GET'
                })
                    .done(function (data) {
                        typecategories = data.data.type;
                        //$('#list_device').html('');
                        $('#type_category').html(type_categories[data.data.type]);
                        $('#unit').html(data.data.detail.unit);
                        if (data.data.type != 1) {
                            $('#js-add-detail-importsupplies').attr('disabled', true);
                            $("#real_quantity").removeClass('kt-hidden').parent('td').find('span').addClass('kt-hidden');
                            if (type != 2) $('#real_quantity').attr('max', parseInt(data.data.quantity)).attr('min', 0);
                        }
                        else {
                            $('#js-add-detail-importsupplies').attr('disabled', false);
                            $("#real_quantity").val('').addClass('kt-hidden').parent('td').find('span').removeClass('kt-hidden');
                            if (type != 2) $('#real_quantity').removeAttr('max', parseInt(data.data.quantity)).removeAttr('min', 0);
                        }
                    });
            }
            else {
                $('#type_category').html("-");
                $('#unit').html("-");
                $('#document_quantity').val('');
                $('#js-add-detail-importsupplies').attr('disabled', false);
                $("#real_quantity").addClass('kt-hidden').parent('td').find('span').html('-').removeClass('kt-hidden');
            }
        });

        $('#js-add-importsupplies').on('click', function () { //Thêm thiết bị - vật tư
            const wrapper = $('.wrapper-fill');
            const suppliesID = wrapper.find('#kt-select-supplies');
            const category_id = suppliesID.val();
            if (category_id != "") {
                if ($('#real_quantity').val() != "") {
                    if (type != 2 && typecategories != 1) {
                        const real = parseInt($('#real_quantity').val());
                        const max = parseInt($('#real_quantity').attr('max'));

                        if (real > max) {
                            $.notify(`Số lượng thực vượt quá số lượng vật tư tồn trong kho ${max}`, { type: 'warning' });
                        }
                        else {
                            addSuppliesToImport(type, typecategories);

                            $('#kt-select-supplies').find(`option[value="${category_id}"]`).attr('disabled', true); //Disabled option của chọn vật tư
                            $('#kt-select-supplies').selectpicker('refresh');
                        }
                    }
                    else {
                        addSuppliesToImport(type, typecategories);

                        $('#kt-select-supplies').find(`option[value="${category_id}"]`).attr('disabled', true); //Disabled option của chọn vật tư
                        $('#kt-select-supplies').selectpicker('refresh');
                    }
                }
                else $.notify('Vui lòng nhập số lượng thực!', { type: 'warning' });
            }
            else $.notify('Vui lòng chọn vật tư!', { type: 'warning' });
        });

        $('#js-add-detail-importsupplies').on('click', function () {
            $form.find('#js-add-detail-importsupplies').prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading
            selected = [];

            //Thêm chi tiết cho thiết bị vật tư
            const wrapper = $('.wrapper-fill');
            const suppliesID = wrapper.find('#kt-select-supplies');
            const categoryId = suppliesID.val();
            const category_prefix = suppliesID.find('option:selected').text(); //prefix của category
            const category_name = suppliesID.find('option:selected').attr('data-subtext'); //Tên của category
            if (categoryId != "") {
                if (type != 2) {      //Chuyển qua export
                    const storage_uuid = $('#kt-select-other').val();
                    const exportUuid = $('#kt-select-document_origin').val();
                    const is_first = true;
                    $.ajax({
                        async: false,
                        url: type == 1 ? `/bunker/getdetailsuppliesinbunker/${storage_uuid}/${categoryId}` : `/bunker/DetailDeviceEx`,
                        method: 'GET',
                        data: type == 1 ? { limit: -1 } : { exportUuid: exportUuid, categoryId: categoryId, limit: -1 },
                    })
                        .done(function (data) {
                            //$('.dmms-bunker').find('.dmms-drawer--detailsupplies').remove();
                            addDetailSuppliesExtra(categoryId, category_prefix, category_name, data.data.items, is_first);
                        })
                        .fail(function (data) {
                            console.log('Lỗi: ' + data != null ? data.responseJSON.msg : 'Lỗi hệ thống!');
                            $.notify('Lỗi hệ thống. Vui lòng thử lại sau!', { type: 'danger' });
                        });
                }
                else {
                    addDetailSupplies(null, category_name, category_prefix);
                }
            }
            else {
                $.notify('Vui lòng chọn vật tư!', { type: 'warning' });
            }
            $form.find('#js-add-detail-importsupplies').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
        });

        if (type == 2) {
            $('#unit_price').on('change', function () { //Thay đổi trạng thái đơn giá => thay đổi tổng tiền
                const unit_price = parseInt($('#unit_price').val().replace(/,/g, ''));
                if ($('#real_quantity').val() != '') {
                    $('#money_string').text($('#real_quantity').val() * unit_price);
                    $('#money_string').simpleMoneyFormat()
                }
                else {
                    $('#money_string').text('0');
                }
            });

            $('#real_quantity').on('change', function () { //Thay đổi số lượng thực => thay đổi giá tiền
                
                if ($('#unit_price').val() != '') {
                    const unit_price = parseInt($('#unit_price').val().replace(/,/g, ''));
                    $('#money_string').text(parseInt($(this).val()) * unit_price);
                    $('#money_string').simpleMoneyFormat()
                }
                else {
                    $('#money_string').text('0');
                }
            })
        }
        else {
            $('#real_quantity').on('change', function () { //Thay đổi số lượng thực
                
                const real = parseInt($('#real_quantity').val());
                const max = parseInt($('#real_quantity').attr('max'));
                if (real > max) {
                    const max = $('#real_quantity').attr('max');
                    $('#real_quantity').attr('title', `Số lượng tồn kho của vật tư là ${max}`)
                }
                else {
                    $('#real_quantity').removeAttr('title');
                }
            })
        }

        //Close drawer
        $('.kt-close').on('click', function () {
            drawer.close();
        });
    };
    var initContentoderto = function ($content, type) {

        /*
            Init form
        */

        //initFormExport($formExport);
        const $formExportoderto = $content.find('.dmms-form-bunker__exportsuppliesoderto');
        initFormExportoderto($formExportoderto);
        /*
            Init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

        //const storage_uuid = $('#uuid').val();
        var typecategories = 1;

        $('#unit_price').simpleMoneyFormat();
        $('#kt-select-supplies').selectpicker({ showSubtext: true });
        $('#kt-select-supplies').selectpicker('refresh');
        $('#kt-select-supplier').selectpicker();

        const storage_uuid = $('#uuid').val();
        if (type == 3) {
            $(`#kt-select-other option[value="${storage_uuid}"]`).attr('disabled', true);
        }
        $('#kt-select-other').selectpicker();

        $('#kt-select-other').on('change', function () {
            const storage_uuid = $(this).val();
            $('#kt-select-supplies').html('').attr('title', 'Lựa chọn').append('<option value="">Lựa chọn</option>').selectpicker('refresh');
            $.ajax({
                async: false,
                url: `/bunker/getSuppliesDeviceInBunker/${storage_uuid}`,
                method: 'GET',
                data: { limit: -1 }
            })
                .done(function (res) {

                    $.each(res.data.items, function (index, item) {
                        $('#kt-select-supplies').append(`<option value="${item.category_id}" data-subtext="${item.name_display}">${item.prefix_name}</option>`);
                    });
                    $('#kt-select-supplies').selectpicker('refresh');
                })
        })

        //$(`#kt-select-bunker option[value="${storage_uuid}"]`).attr('disabled', true);
        //$('#kt-select-bunker').selectpicker();

        //Change #kt-select-supplies
        $('#kt-select-supplies').on("change", function () {
            const type_categories = { 1: "Thiết bị", 2: "Vật tư", 3: "Dụng cụ LĐ", 4: "Phần mềm" };
            const storage_uuid = $('#uuid').val();
            const wrapper = $('.wrapper-fill');
            const category = wrapper.find('#kt-select-supplies');
            const category_id = category.val();
            wrapper.find('#list-device').html('');

            $.ajax({
                url: `/bunker/DetailSuppliesAjax/${category_id}/${storage_uuid}`,
                method: 'GET'
            })
                .done(function (data) {
                    $('#list_device').html('');
                    $('#type_category').html(type_categories[data.data.type]);
                    //$('#document_quantity').val();
                    typecategories = data.data.type;
                    $('#unit').html(data.data.detail.unit);
                    if (data.data.type != 1) {
                        $('#js-add-detail-importsupplies').attr('disabled', true);
                        $("#real_quantity").removeClass('kt-hidden').parent('td').find('span').addClass('kt-hidden');
                    }
                    else {
                        $('#js-add-detail-importsupplies').attr('disabled', false);
                        $("#real_quantity").addClass('kt-hidden').parent('td').find('span').removeClass('kt-hidden');
                    }
                });
        });

        $('#js-add-importsupplies').on('click', function () { //Thêm thiết bị - vật tư
            const wrapper = $('.wrapper-fill');
            const suppliesID = wrapper.find('#kt-select-supplies');
            const category_id = suppliesID.val();
            if (category_id != "") {
                addSuppliesToImport(type, typecategories);

                $('#kt-select-supplies').find(`option[value="${category_id}"]`).attr('disabled', true); //Disabled option của chọn vật tư
                $('#kt-select-supplies').selectpicker('refresh');
            }
            else $.notify('Vui lòng chọn vật tư!', { type: 'warning' });
        });

        $('#js-add-detail-importsupplies').off('click').on('click', function () {
            //$formExportoderto.find('button[type="submit"]').prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading

            selected = [];

            //Thêm chi tiết cho thiết bị vật tư
            const wrapper = $('.wrapper-fill');
            const suppliesID = wrapper.find('#kt-select-supplies');
            const category_id = suppliesID.val();
            const category_prefix = suppliesID.find('option:selected').text(); //prefix của category
            const category_name = suppliesID.find('option:selected').attr('data-subtext'); //Tên của category
            if (category_id != "") {
                if (type == 1 || type == 3) {      //Chuyển qua export
                    const storage_uuid = $('#kt-select-other').val();
                    const is_first = true;
                    $.ajax({
                        async: false,
                        url: `/bunker/getdetailsuppliesinbunker/${storage_uuid}/${category_id}`,
                        method: 'GET',
                        data: { limit: -1 }
                    })
                        .done(function (data) {

                            //$('.dmms-bunker').find('.dmms-drawer--detailsupplies').remove();
                            addDetailSuppliesExtra(category_id, category_prefix, category_name, data.data.items, is_first);
                        })
                        .fail(function (data) {
                            console.log(`Lỗi: ${data.responseJSON.msg}`);
                            $.notify('Lỗi hệ thống. Vui lòng thử lại sau!', { type: 'danger' });
                        });
                }
                else addDetailSupplies(null, category_name, category_prefix);

            }
            else $.notify('Vui lòng chọn vật tư!', { type: 'warning' });
        });

        $('#unit_price').on('change', function () { //Thay đổi trạng thái đơn giá => thay đổi tổng tiền
            const unit_price = parseInt($('#unit_price').val().replace(/,/g, ''));
            if ($('#real_quantity').val() != '') {
                $('#money_string').text($('#real_quantity').val() * unit_price);
                $('#money_string').simpleMoneyFormat()
            }
            else {
                $('#money_string').text('0');
            }
        });

        //Close drawer
        $('.kt-close').on('click', function () {
            drawer.close();
        });
    };
    var addDetailSuppliesExtra = function (category_id, category_prefix, category_name, data, is_first) {
        var list_serial_html = '';
        const title = 'CHI TIẾT THIẾT BỊ';
        subDrawer = new KTDrawer({ title, category_id: category_id || 'new', className: 'dmms-drawer--detailsupplies' });

        if (is_first == undefined) {
            //Lấy tất cả input trong .list_device
            const obj_temp = $('.wrapper-fill').find(`tr[id="${category_id}"] .list_device input`);
            //Gán giá trị object devices
            devices = obj_temp.length > 0 ? obj_temp.serializeJSON().detail[0].device : [];
        }
        else {
            //Lấy tất cả input trong #list_device
            const obj_temp = $('.wrapper-fill').find(`tr[id="first"] #list_device input`);
            //Gán giá trị object devices
            devices = obj_temp.length > 0 ? obj_temp.serializeJSON().detail[0].device : [];
        }
        
        if (data != null) {
            list_serial_html += `<select data-live-search="true" class="form-control" id="kt-select-serial">
                                    <option value="">Lựa chọn</option>`;
            $.each(data, function (index, item) {
                const expire_device_temp = item.expire_device_string != undefined ? item.expire_device_string : item.expireDevice_string;
                if (devices.filter(st => st.uuid == item.uuid) != '') {
                    list_serial_html += `<option value="${item.uuid}" data-expire="${expire_device_temp}" disabled>${item.serial}</a>`;
                }
                else {
                    list_serial_html += `<option value="${item.uuid}" data-expire="${expire_device_temp}">${item.serial}</a>`;
                }
            })
            list_serial_html += `</select>`;
        }
        var response = `<form class="dmms-form-bunker__importdetailsupplies" onsubmit="return false">
                        <div class="add-detail-supplies_inner">
                            <table class="table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">Tên thiết bị</th>
                                        <th scope="col">Serial</th>
                                        <th scope="col">Ngày hết hạn BH</th>
                                        <th scope="col">Tác vụ</th>
                                    </tr>
                                </thead>
                                <tbody class="table-detailsupplies">
                                    <tr>
                                      <td class="text-left kt-padding-l-10"><input id="detail_uuid" hidden />
                                          <span class="text-primary">${category_prefix}</span>
                                          <br/>${category_name}
                                      </td>
                                      <td>${list_serial_html}</td>
                                      <td><input hidden /><span id="detail_expire_device"></span></td>
                                      <td><i class="la la-plus kt-p-0-tablet-and-mobile" id="js-add-detailsupplies_import"></i></td>
                                    </tr>`;
        if (devices.length != 0) {
            $.each(devices, function (index, item) {
                const device_uuid = item.uuid;
                $.ajax({
                    async: false,
                    url: `/device/detailwithparams/${device_uuid}`,
                    method: 'GET',
                })
                    .done(function (res) {
                        response += `<tr>
                                <td class="text-left kt-padding-l-10"><input name="detail[][device[][uuid]]" value="${res.data.uuid}" hidden />
                                    <span class="text-primary">${category_prefix}</span>
                                    <br/>${category_name}
                                </td>      
                                <td><input name="detail[][device[][serial]]" value="${res.data.serial}" hidden /><span>${res.data.serial}</span></td>         
                                <td><input name="detail[][device[][expire_device_string]]" value="${res.data.expire_device_format}" hidden /><span>${res.data.expire_device_format}</span></td>         
                                <td><i class="la la-trash remove_field_detailsupplies" data-serial="${res.data.uuid}" style="background-color: #FF5F6D"></i></td>         
                            </tr>`;
                    })
                    .fail(function (res) {
                        console.log(`Lỗi: ${res.responseJSON.msg}`);
                    });
            });
        }
        response += `</tbody>
                     </table>
                        </div>
                        <div class="text-right kt-margin-t-20">
                            <button type="submit" class="btn btn-info js-add-devices">
                                Áp dụng
                            </button>
                            <button type="button" class="btn btn-danger kt-close">
                                Hủy bỏ
                            </button>
                        </div>
                     </form>`;
        subDrawer.open();
        const $content = subDrawer.setBody(response);
        //selected.forEach(function (item, index) {
        //    $('#kt-select-serial').find(`option[value="${item}"]`).attr('disabled', true);
        //    $('#kt-select-serial').selectpicker('refresh');
        //})
        $('#kt-select-serial').selectpicker();
        $('#kt-select-serial').on('change', function () {
            const device_uuid = $(this).val();
            const expire_device_string = $('#kt-select-serial').find(`option[value="${device_uuid}"]`).attr('data-expire');
            if (device_uuid != '') {
                //$.ajax({
                //    async: false,
                //    url: `/device/detailwithparams/${device_uuid}`
                //})
                //    .done(function (res) {

                //    })
                //fill giá trị
                $('#detail_uuid').val(device_uuid);
                $('#detail_expire_device').parent('td').find('input').val(expire_device_string);
                $('#detail_expire_device').html(expire_device_string);
            }
            else {
                $('#detail_uuid').val("");
                $('#detail_expire_device').parent('td').find('input').val("");
                $('#detail_expire_device').html("");
            }
        });
        initSaveDetailExtra($content, category_id, category_name, category_prefix, is_first);
        devices = [];

        //Close drawer
        $('.kt-close').on('click', function () {

            subDrawer.close();
        });
    }

    //Add chi tiết cho thiết bị
    var initSaveDetailExtra = function ($content, category_id, category_name, category_prefix, is_first) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-form-bunker__importdetailsupplies');
        initFormExtra($form, category_id, is_first);

        const wrapper = $('.table-detailsupplies');

        $('#js-add-detailsupplies_import')
            .off('click')
            .on('click', function () {

                const detail_uuid = $('.add-detail-supplies_inner').find(`input[id="detail_uuid"]`);
                const serial = $('.add-detail-supplies_inner').find(`#kt-select-serial`);
                const serial_text = serial.find('option:selected').text();
                const expire_device_string = $('.add-detail-supplies_inner').find(`#detail_expire_device`);
                if (serial.val() != '') {
                    wrapper.append(`<tr>
                                     <td class="text-left kt-padding-l-10"><input name="detail[][device[][uuid]]" value="${detail_uuid.val()}" hidden />
                                        <span class="text-primary">${category_prefix}</span>
                                        <br/>${category_name}
                                     </td>
                                     <td><input name="detail[][device[][serial]]" value="${serial_text}" hidden /><span>${serial_text}</span></td>
                                     <td><input name="detail[][device[][expire_device_string]]" value="${expire_device_string.text()}" hidden /><span>${expire_device_string.text()}</span></td>         
                                     <td><i class="la la-trash remove_field_detailsupplies" data-serial="${detail_uuid.val()}"style="background-color: #FF5F6D"></i></td>
                                 </tr>`);
                    //reset values
                    $('#kt-select-serial').find(`option[value="${detail_uuid.val()}"]`).attr('disabled', true);
                    $('#kt-select-serial').selectpicker('refresh');
                    detail_uuid.val('');
                    serial.selectpicker('val', '');
                    expire_device_string.parent('td').find('input').val('');
                    expire_device_string.html('');
                }
                else $.notify('Vui lòng chọn serial!', { type: 'warning' });
            });

        $(wrapper).on("click", ".remove_field_detailsupplies", function (e) { //user click on remove text
            e.preventDefault();
            const $this = $(this);
            const serial = $this.attr('data-serial');
            $('#kt-select-serial').find(`option[value="${serial}"]`).attr('disabled', false);
            $('#kt-select-serial').selectpicker('refresh');
            $this.parent('td').parent('tr').remove();
        });
    };

    //Form validate cho add chi tiết thiết bị
    var initFormExtra = function ($form, category_id, is_first) {
        $form.validate({
            rules: {

            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {
                $(form).find('button[type="submit"]').prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading

                devices = [];
                const data = $(form).serializeJSON() || {};
                if (data != {} && data.detail != undefined) {
                    $.each(data.detail[0].device || [], function (index, item) {
                        devices.push(item);
                    });
                }

                if (data.detail != undefined && $('#kt-select-serial').val() == '') {
                    KTApp.block($form);
                    $.notify('Đã thêm chi tiết!', { type: 'success' });

                    if (is_first == undefined) { //Sau khi thêm chi tiết cho thiết bị vật tư (category_id != null)=> lấy ra danh sách thiết bị và thay đổi giá trị SL thực va thành tiền
                        const wrapper = $(`.wrapper-fill tr[id=${category_id}]`);
                        //const money_string = wrapper.find('input[name="detail[][unit_price_string]"]').val();
                        //const money = parseInt(money_string.replace(/,/g, ''));

                        wrapper.find(`.list_device`).html('');
                        $.each(devices, function (index, item) {
                            //<input name="detail[][device[][imei]]" value="${item.imei}" hidden />
                            wrapper.find(`.list_device`).append(`<input name="detail[][device[][uuid]]" value="${item.uuid}" hidden />
                                <input name="detail[][device[][serial]]" value="${item.serial}" hidden />
                                <input name="detail[][device[][expire_device_string]]" value="${item.expire_device_string}" hidden />`);
                        });
                        if ($('#type').val() == 1) { //Nếu import từ NVKT thì Sl thiết bị theo chứng từ chính là số lượng thực tế
                            wrapper.find('input[name="detail[][document_quantity]"]').val(devices.length);
                        }
                        wrapper.find('input[name="detail[][real_quantity]"]').val(`${devices.length}`);
                        wrapper.find('.detail_real_quantity').html(devices.length);
                        //wrapper.find(`#detail_money_string_${category_id}`).html(devices.length * money);

                        //$('.detail_money_string').simpleMoneyFormat();
                        /**
                        * Close drawer
                        */
                        subDrawer.close();
                    }
                    else { //Sau khi thêm chi tiết cho thiết bị vật tư (category_id = null)=> lấy ra danh sách thiết bị và thay đổi giá trị SL thực va thành tiền
                        const wrapper = $(`.wrapper-fill tr[id="first"]`);
                        //const money_string = wrapper.find('input[name="detail[][unit_price_string]"]').val();
                        //const money = parseInt(money_string.replace(/,/g, ''));
                        wrapper.find('#list_device').html('');
                        $.each(devices, function (index, item) {
                            //<input name="detail[][device[][imei]]" value="${item.imei}" hidden />
                            wrapper.find('#list_device').append(`<input name="detail[][device[][uuid]]" value="${item.uuid}" hidden />
                            <input name="detail[][device[][serial]]" value="${item.serial}" hidden />

                            <input name="detail[][device[][expire_device_string]]" value="${item.expire_device_string}" hidden />`);
                        });
                        if ($('#type').val() == 1) { //Nếu import từ NVKT thì Sl thiết bị theo chứng từ chính là số lượng thực tế
                            wrapper.find('input[name="detail[][document_quantity]"]').val(devices.length);
                        }
                        wrapper.find('#real_quantity').val(`${devices.length}`);
                        wrapper.find('#real_quantity').parent('td').find('span').html(devices.length);
                        //wrapper.find('#money_string').html(devices.length * money);

                        //$('#money_string').simpleMoneyFormat();
                        /**
                        * Close drawer
                        */
                        subDrawer.close();
                    }
                }
                else {
                    KTApp.unblock($form);
                    $(form).find('button[type="submit"]').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
                    $.notify('Vui lòng thêm thiết bị trước khi áp dụng!', { type: 'warning' });
                }

                return false;
            },
        });
    }

    //--------------BEGIN: FORM IMPORT----------------------
    //Thêm mới raw trong drawer nhập kho
    var addSuppliesToImport = function (type, typecategories) {
        const type_temp = $('.dmms-form-bunker__importsupplies').find('#type').val(); //type của phiếu nhập
        var body_temp = '';
        var device_string = ''; //Biến chứa html

        const wrapper = $('.wrapper-fill');

        const supplies = wrapper.find('#kt-select-supplies');
        const category_id = supplies.val(); //uuid của category
        const category_prefix = supplies.find('option:selected').text(); //prefix của category
        const category_name = supplies.find('option:selected').attr('data-subtext'); //Tên của category
        const type_category = wrapper.find('#type_category').text(); //Tên của loại (1-vật tư, 2-thiết bị, 3-Dụng cụ LĐ, 4-Phần mềm)
        const document_quantity = wrapper.find('input[id="document_quantity"]').val(); //Số lượng thiết bị theo chứng từ
        const real_quantity = wrapper.find('input[id="real_quantity"]').val(); //Số lượng thực
        const unit = wrapper.find('#unit').text(); //Đơn vị tính
        if (type_temp == 2) {
            var unit_price_str = wrapper.find('input[id="unit_price"]').val(); //Đơn giá kiểu string #,###
            var money_string = wrapper.find('#money_string').text(); //Thành tiền (đơn giá * số lượng thực)
        }

        //Nếu có danh sách devices thì truyền html vào biến devices_string
        if (devices !== []) {
            //<input name="detail[][device[][imei]]" value="${item.imei}" />
            $.each(devices, function (index, item) {
                device_string += `<input name="detail[][device[][uuid]]" value="${item.uuid}" />
                            <input name="detail[][device[][serial]]" value="${item.serial}" />
                            <input name="detail[][device[][expire_device_string]]" value="${item.expire_device_string}" />`;
            });
        }

        //Nếu có giá trị của vật tư thiết bị => add thêm raw vật tư thiết bị
        body_temp += `<tr id="${category_id}">
                        <td class="text-left kt-padding-l-10"><input name="detail[][category_id]" value="${category_id}" hidden />
                            <span class="text-primary">${category_prefix}</span>
                            <br/>${category_name}
                        </td>
                        <td>${type_category}</td>`;
        if (type_temp != 1) {
            body_temp += `<td><input class="form-control" placeholder="Nhập" name="detail[][document_quantity]" value="${document_quantity}" /></td>`;
        }
        else {
            body_temp += `<td><input class="form-control" placeholder="Nhập" name="detail[][document_quantity]" value="${real_quantity}" readonly /></td>`;
        }
        body_temp += `<td><input name="detail[][real_quantity]" value="${real_quantity}" hidden /><span class="detail_real_quantity">${real_quantity}</span></td>
                        <td>${unit}</td>`;
        if (type_temp == 2) {
            body_temp += `<td>
                            <input class="form-control detail_unit_price" name="detail[][unit_price_string]" placeholder="Nhập" value="${unit_price_str}" />
                        </td>
                        <td class="detail_money_string">${money_string}</td>`;
        }
        body_temp += `<td style="display: none" class="list_device">${device_string}</td>
                        <td>
                            <button type="button" class="btn btn-primary js-add-detail-importsupplies" data-id="${category_id}" disabled>
                                <span class="d-none d-sm-inline-block">Xem chi tiết</span>
                            </button>
                        </td>
                        <td><i class="la la-trash remove_field" style="background-color: #FF5F6D"></i></td>
                    </tr>`;
        wrapper.append(body_temp);
        if (typecategories != 1) {
            $(`tr[id="${category_id}"]`).find('.js-add-detail-importsupplies').attr('disabled', true);
        }
        else {
            $(`tr[id="${category_id}"]`).find('.js-add-detail-importsupplies').attr('disabled', false);
        }

        $('.detail_money_string').simpleMoneyFormat();
        $('.detail_unit_price').simpleMoneyFormat();

        //Sau khi add thêm category => reset giá trị của raw nhập liệu
        wrapper.find('#type_category').html('-'); //Phân loại => "-"
        wrapper.find('input[id="document_quantity"]').val(''); //số lượng theo chứng từ => ''
        wrapper.find('input[id="real_quantity"]').val('').addClass('kt-hidden'); //số lượng thực => ''
        wrapper.find('input[id="real_quantity"]').parent('td').find('span').html('-').removeClass('kt-hidden'); //số lượng thực text => "-"
        wrapper.find('#unit').html('-'); //đơn vị tính => "-"
        wrapper.find('#list_device').html(''); //reset list device
        wrapper.find('#js-add-detail-importsupplies').attr('disabled', false);
        if (type_temp == 2) {
            wrapper.find('input[id="unit_price"]').val(''); //đơn giá => ''
            wrapper.find('#money_string').html('0'); //tổng tiền => '0'
        }
        supplies.selectpicker('val', ''); //tên thiết bị vật tư => ""

        devices = [];   //restart arr

        const wrapper_detail = wrapper.find(`tr[id="${category_id}"]`);
        $(wrapper_detail).on('change', `.detail_unit_price`, function (e) { //Thay đổi trường giá
            e.preventDefault();
            const unit_price = parseInt(wrapper_detail.find(`.detail_unit_price`).val().replace(/,/g, ''));
            if (wrapper_detail.find('input[name="detail[][real_quantity]"]').val() != '') {
                wrapper_detail.find(`.detail_money_string`).html(parseInt(wrapper_detail.find('input[name="detail[][real_quantity]"]').val()) * unit_price);
                wrapper_detail.find('.detail_money_string').simpleMoneyFormat()
            }
            else {
                wrapper_detail.find('.detail_money_string').html('0');
            }
        });

        $(wrapper).off('click').on('click', `.js-add-detail-importsupplies`, function () {
            wrapper.find(`tr[id="${categoryId}"] .js-add-detail-importsupplies`).prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading
            const categoryId = $(this).attr('data-id');

            //Thêm chi tiết cho thiết bị vật tư đã được thêm
            //$('.dmms-bunker').find('.dmms-drawer--detailsupplies').remove();
            if (type == 1 || type == 3) { //Chuyển qua export
                const storage_uuid = $('#kt-select-other').val();
                const exportUuid = $('#kt-select-document_origin').val();
                $.ajax({
                    async: false,
                    url: type == 1 ? `/bunker/getdetailsuppliesinbunker/${storage_uuid}/${categoryId}` : `/bunker/DetailDeviceEx`,
                    method: 'GET',
                    data: type == 1 ? { limit: -1 } : { exportUuid: exportUuid, categoryId: categoryId, limit: -1 },
                })
                    .done(function (data) {
                        //$('.dmms-bunker').find('.dmms-drawer--detailsupplies').remove();
                        addDetailSuppliesExtra(categoryId, category_prefix, category_name, data.data.items);
                    })
                    .fail(function (data) {
                        //console.log(`Lỗi: ${data.responseJSON.msg}`);
                        $.notify('Lỗi hệ thống. Vui lòng thử lại sau!', { type: 'danger' });
                    });
            }
            else {
                addDetailSupplies(categoryId, category_name, category_prefix);
            }
            wrapper.find(`tr[id = "${categoryId}"] .js-add-detail-importsupplies`).prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
        });

        $(wrapper).find(`tr[id=${category_id}]`).on("click", `.remove_field`, function (e) { //user click on remove text
            e.preventDefault();
            const $this = $(this);

            $this.parent('td').parent('tr').remove();
            $('#kt-select-supplies').find(`option[value="${category_id}"]`).attr('disabled', false); //Disabled option của chọn vật tư
            $('#kt-select-supplies').selectpicker('refresh');
        });
    }

    //Thêm mới raw trong drawer thêm chi tiết của thiết bị vật tư
    var addDetailSupplies = function (category_id, category_name, category_prefix) {

        const title = 'CHI TIẾT THIẾT BỊ';
        subDrawer = new KTDrawer({ title, category_id: category_id || 'new', className: 'dmms-drawer--detailsupplies' });

        if (category_id != null) {
            //Lấy tất cả input trong #list_device
            const obj_temp = $('.wrapper-fill').find(`tr[id="${category_id}"] .list_device input`);
            //Gán giá trị object devices
            devices = obj_temp.length > 0 ? obj_temp.serializeJSON().detail[0].device : [];
        }
        else {
            //Lấy tất cả input trong #list_device
            const obj_temp = $('.wrapper-fill').find(`tr[id="first"] #list_device input`);
            //Gán giá trị object devices
            devices = obj_temp.length > 0 ? obj_temp.serializeJSON().detail[0].device : [];
        }

        var response = `<form class="dmms-form-bunker__importdetailsupplies" onsubmit="return false">
                        <div class="add-detail-supplies_inner">
                            <table class="table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">Tên thiết bị</th>
                                        <th scope="col">Serial</th>
                                        <th scope="col">Ngày hết hạn BH</th>
                                        <th scope="col">Tác vụ</th>
                                    </tr>
                                </thead>
                                <tbody class="table-detailsupplies">
                                <tr>
                                     <td class="text-left kt-padding-l-10"><input value="" hidden />
                                          <span class="text-primary">${category_prefix}</span>
                                          <br/>${category_name}
                                      </td>
                                     <td><input class="form-control" placeholder="Nhập" id="serial" style="width: 100px" /></td>
                             
                                     <td>
                                        <input class="form-control" readonly placeholder="Chọn ngày" id="kt-datepicker-expdate" style="width: 100px" />     
                                     </td>
                                     <td><i class="la la-plus kt-p-0-tablet-and-mobile" id="js-add-detailsupplies"></i></td>
                                 </tr>`;
        if (devices.length != 0) {
            $.each(devices, function (index, item) {
                response += `<tr>
                                <td class="text-left kt-padding-l-10"><input name="detail[][device[][uuid]]" value="" hidden />
                                    <span class="text-primary">${category_prefix}</span>
                                    <br/>${category_name}
                                </td>      
                                <td><input name="detail[][device[][serial]]" value="${item.serial}" hidden />${item.serial}</td>         
                                 
                                <td>         
                                    <input name="detail[][device[][expire_device_string]]" value="${item.expire_device_string}" hidden />${item.expire_device_string}               
                                </td>         
                                <td><i class="la la-trash remove_field_detailsupplies" style="background-color: #FF5F6D"></i></td>         
                            </tr>`
            })
        }
        response += `</tbody>
                     </table>
                        </div>
                        <div class="text-right kt-margin-t-20">
                            <button type="submit" class="btn btn-info js-add-devices">
                                Áp dụng
                            </button>
                            <button type="button" class="btn btn-danger kt-close">
                                Hủy bỏ
                            </button>
                        </div>
                    </form>`;
        subDrawer.open();
        const $content = subDrawer.setBody(response);
        initSaveDetail($content, category_id, category_name, category_prefix);
        devices = [];
    };

    //Add chi tiết cho thiết bị
    var initSaveDetail = function ($content, category_id, category_name, category_prefix) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-form-bunker__importdetailsupplies');
        initForm1($form, category_id);

        $('#kt-datepicker-expdate').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
        });

        const wrapper = $('.table-detailsupplies');

        $('#js-add-detailsupplies').on('click', function () {
            const serial = $('.add-detail-supplies_inner').find(`input[id="serial"]`);
            const expire_device_string = $('.add-detail-supplies_inner').find(`input[id="kt-datepicker-expdate"]`);

            //if (serial.val() !== '' && imei.val() !== '' && expire_device_string.val() !== '') {
            if (serial.val() !== '' && expire_device_string.val() !== '') {
                wrapper.append(`<tr>
                                     <td class="text-left kt-padding-l-10"><input name="detail[][device[][uuid]]" value="${category_id}" hidden />
                                        <span class="text-primary">${category_prefix}</span>
                                        <br/>${category_name}
                                    </td>
                                     <td><input name="detail[][device[][serial]]" value="${serial.val()}" hidden />${serial.val()}</td>
                                     
                                     <td>
                                        <input name="detail[][device[][expire_device_string]]" value="${expire_device_string.val()}" hidden />${expire_device_string.val()}   
                                     </td>
                                     <td><i class="la la-trash remove_field_detailsupplies" style="background-color: #FF5F6D"></i></td>
                                 </tr>`);
                serial.val('');
                expire_device_string.val('');
            }
            else {
                $.notify('Vui lòng điền đủ các trường trước khi thêm!', { type: 'warning' });
            }
        });

        $(wrapper).on("click", ".remove_field_detailsupplies", function (e) { //user click on remove text
            e.preventDefault();
            const $this = $(this);

            $this.parent('td').parent('tr').remove();
        });

        $('.kt-close').on('click', function () {
            subDrawer.close();
        });
    };

    //Form validate cho add chi tiết thiết bị
    var initForm1 = function ($form, category_id) {
        $form.validate({
            rules: {
                //'detail[][device[][serial]]': {
                //    required: true,
                //},
                //'detail[][device[][expire_device_string]]': {
                //    required: true,
                //}
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {
                $(form).find('button[type="submit"]').prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading

                devices = [];
                const data = $(form).serializeJSON() || {};
                if (data != null && data.detail != undefined) {
                    $.each(data.detail[0].device || [], function (index, item) {
                        devices.push(item);
                    });
                }

                if (data.detail != undefined && $('#serial').val() == "" && $('#kt-datepicker-expdate').val() == "") {
                    KTApp.block($form);
                    $.notify('Đã thêm chi tiết!', { type: 'success' });
                    if (category_id !== null) { //Sau khi thêm chi tiết cho thiết bị vật tư (category_id != null)=> lấy ra danh sách thiết bị và thay đổi giá trị SL thực va thành tiền
                        const wrapper = $(`.wrapper-fill tr[id=${category_id}]`);
                        const money_string = wrapper.find('input[name="detail[][unit_price_string]"]').val();
                        const money = parseInt(money_string.replace(/,/g, ''));
                        wrapper.find(`.list_device`).html('');
                        $.each(devices, function (index, item) {
                            //<input name="detail[][device[][imei]]" value="${item.imei}" hidden />
                            wrapper.find(`.list_device`).append(`<input name="detail[][device[][uuid]]" value="" hidden />
                                <input name="detail[][device[][serial]]" value="${item.serial}" hidden />
                                <input name="detail[][device[][expire_device_string]]" value="${item.expire_device_string}" hidden />`)
                        });
                        wrapper.find('input[name="detail[][real_quantity]"]').val(`${devices.length}`);
                        wrapper.find('.detail_real_quantity').html(devices.length);
                        wrapper.find(`.detail_money_string`).html(devices.length * money);

                        $('.detail_money_string').simpleMoneyFormat();
                        /**
                        * Close drawer
                        */
                        subDrawer.close();
                    }
                    else { //Sau khi thêm chi tiết cho thiết bị vật tư (category_id = null)=> lấy ra danh sách thiết bị và thay đổi giá trị SL thực va thành tiền
                        const wrapper = $(`.wrapper-fill tr[id="first"]`);
                        const money_string = wrapper.find('input[name="detail[][unit_price_string]"]').val();
                        const money = parseInt(money_string.replace(/,/g, ''));
                        wrapper.find('#list_device').html('');
                        $.each(devices, function (index, item) {
                            //<input name="detail[][device[][imei]]" value="${item.imei}" hidden />
                            wrapper.find('#list_device').append(`<input name="detail[][device[][uuid]]" value="" hidden />
                                <input name="detail[][device[][serial]]" value="${item.serial}" hidden />

                                <input name="detail[][device[][expire_device_string]]" value="${item.expire_device_string}" hidden />`)
                        });

                        wrapper.find('input[name="detail[][real_quantity]"]').val(`${devices.length}`);
                        wrapper.find('input[name="detail[][real_quantity]"]').parent('td').find('span').html(devices.length);
                        wrapper.find('#money_string').html(devices.length * money);

                        $('#money_string').simpleMoneyFormat();
                        /**
                        * Close drawer
                        */
                        subDrawer.close();
                    }
                }
                else {
                    KTApp.unblock($form);
                    $(form).find('button[type="submit"]').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
                    $.notify('Vui lòng thêm thiết bị trước khi áp dụng!', { type: 'warning' });
                }

                return false;
            },
        });
    }
    //--------------END: FORM IMPORT----------------------



    //--------------BEGIN: FORM EXPORT----------------------
    var initContentExport = function ($content) {
        /*
            Init form
        */
        const $formExport = $content.find('.dmms-form-bunker__exportsupplies');
        initFormExport($formExport);
        const $formExportoder = $content.find('.dmms-form-bunker__exportsuppliesoder');
        initFormExportoder($formExportoder);
        /*
            Init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

        var storage_uuid = $('#uuid').val();
        const wrapper = $('.wrapper-fill');

        $('#unit_price').simpleMoneyFormat();
        $('#kt-select-supplies').selectpicker({ showSubtext: true });
        $('#kt-select-supplies').selectpicker('refresh');
        $('#kt-select-supplier').selectpicker();
        $('#kt-select-staff').selectpicker();
        $('#kt-select-stafftech').selectpicker();

        $(`#kt-select-bunker option[value="${storage_uuid}"]`).attr('disabled', true);
        $('#kt-select-bunker').selectpicker();

        $('#kt-select-stafftech').on('change', function () {
            $('#to_staff_uuid').val($(this).val());
        });
        var typecategories = 1;
        //Change #kt-select-supplies
        $('#kt-select-supplies').on("change", function () {
            const type_categories = { 1: "Thiết bị", 2: "Vật tư", 3: "Dụng cụ LĐ", 4: "Phần mềm" };
            const storage_uuid = $('#uuid').val();
            const category = wrapper.find('#kt-select-supplies');
            const category_id = category.val();
            $.ajax({
                url: `/bunker/DetailSuppliesAjax/${category_id}/${storage_uuid}`,
                method: 'GET'
            })
                .done(function (data) {
                    $('#type_category').html(type_categories[data.data.type]);
                    //$('#document_quantity').val();
                    $('#unit').html(data.data.detail.unit);
                    $('#list_device').html('');
                    typecategories = data.data.type;
                    if (data.data.type != 1) {
                        $('#js-add-detail-exportsupplies').attr('disabled', true);
                        $("#real_quantity").removeClass('kt-hidden').parent('td').find('span').addClass('kt-hidden');
                        $('#real_quantity').attr('max', parseInt(data.data.quantity)).attr('min', 0);
                    }
                    else {
                        $('#js-add-detail-exportsupplies').attr('disabled', false);
                        $("#real_quantity").val('').addClass('kt-hidden').parent('td').find('span').removeClass('kt-hidden');
                        $('#real_quantity').removeAttr('max', parseInt(data.data.quantity)).removeAttr('min', 0);
                    }
                });
        });

        if ($('#type').val() == 4) {
            $('#unit_price').on('change', function () { //Thay đổi trạng thái đơn giá => thay đổi tổng tiền
                const unit_price = $('#unit_price').val() != '' ? parseInt($('#unit_price').val().replace(/,/g, '')) : 0;
                const real_quantity = $('#real_quantity').val() != '' ? parseInt($('#real_quantity').val()) : 0;
                $('#money_string').text(real_quantity * unit_price);
                $('#money_string').simpleMoneyFormat();
            });

            $('#real_quantity').on('change', function () {
                const real_quantity = $('#real_quantity').val() != '' ? parseInt($('#real_quantity').val()) : 0;
                const unit_price = $('#unit_price').val() != '' ? parseInt($('#unit_price').val().replace(/,/g, '')) : 0;
                $('#money_string').text(real_quantity * unit_price);
                $('#money_string').simpleMoneyFormat();
            });
        }
        else {
            $('#real_quantity').on('change', function () { //Thay đổi số lượng thực
                const real = parseInt($('#real_quantity').val());
                const max = parseInt($('#real_quantity').attr('max'));
                if (real > max) {
                    const max = $('#real_quantity').attr('max');
                    $('#real_quantity').attr('title', `Số lượng tồn kho của vật tư là ${max}`)
                }
                else {
                    $('#real_quantity').removeAttr('title');
                }
            })
        }

        //Export
        $('#js-add-exportsupplies').on('click', function () { //Thêm mới 1 raw trong export device
            const suppliesID = wrapper.find('#kt-select-supplies');
            const category_id = suppliesID.val();

            if (category_id != "") {
                if ($('#real_quantity').val() != '') {
                    if (typecategories != 1) {
                        const real = parseInt($('#real_quantity').val());
                        const max = parseInt($('#real_quantity').attr('max'));
                        if (real > max) {
                            $.notify(`Số lượng thực vượt quá số lượng vật tư tồn trong kho ${max}`, { type: 'warning' });
                        }
                        else {
                            addSuppliesToExport(typecategories);

                            $('#kt-select-supplies').find(`option[value="${category_id}"]`).attr('disabled', true); //Disabled option của chọn vật tư
                            $('#kt-select-supplies').selectpicker('refresh');
                        }
                    }
                    else {
                        addSuppliesToExport(typecategories);

                        $('#kt-select-supplies').find(`option[value="${category_id}"]`).attr('disabled', true); //Disabled option của chọn vật tư
                        $('#kt-select-supplies').selectpicker('refresh');
                    }
                }
                else $.notify('Vui nhập số lượng thực!', { type: 'warning' });
            }
            else $.notify('Vui lòng chọn vật tư!', { type: 'warning' });
        });

        $('#js-add-detail-exportsupplies').on('click', function () { //Thêm mới 1 raw trong detail export device
            $formExport.find('#js-add-detail-exportsupplies').prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading

            selected = [];
            const suppliesID = wrapper.find('#kt-select-supplies');
            const category_id = suppliesID.val();
            const category_prefix = suppliesID.find('option:selected').text(); //prefix của category
            const category_name = suppliesID.find('option:selected').attr('data-subtext'); //Tên của category
            const is_first = true;

            if (category_id != '') {
                $.ajax({
                    url: `/bunker/getdetailsuppliesinbunker/${storage_uuid}/${category_id}`,
                    method: 'GET',
                    data: { limit: -1 }
                })
                    .done(function (data) {
                        addDetailSuppliesExport(category_id, category_prefix, category_name, data.data.items, is_first);

                    })
                    .fail(function (data) {
                        console.log(`Lỗi: ${data.responseJSON.msg}`);
                        $.notify('Lỗi hệ thống. Vui lòng thử lại sau!', { type: 'danger' });
                    });
            }
            else $.notify('Vui lòng chọn vật tư!', { type: 'warning' });

            $formExport.find('#js-add-detail-exportsupplies').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
        });

        $('.js-add-detail-exportsupplies').off('click').on('click', function () { //Mở chi tiết thiết bị
            $formExportoder.find('.js-add-detail-exportsupplies').prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading

            selected = [];
            const $this = $(this)
            const category_id = $this.attr('data-id')
            const category_prefix = $this.attr('data-pre') //prefix của category
            const category_name = $this.attr('data-name') //Tên của category
            const is_first = true;
            var storage_uuid = $('#store_uuid').val();
            if (category_id != '') {
                $.ajax({
                    url: `/bunker/getdetailsuppliesinbunker/${storage_uuid}/${category_id}`,
                    method: 'GET',
                    data: { limit: -1 }
                })
                    .done(function (data) {
                        addDetailSuppliesExportoder(category_id, category_prefix, category_name, data.data.items, is_first);
                    })
                    .fail(function (data) {
                        console.log(`Lỗi: ${data.responseJSON.msg}`);
                        $.notify('Lỗi hệ thống. Vui lòng thử lại sau!', { type: 'danger' });
                    });
            }
            else $.notify('Vui lòng chọn vật tư!', { type: 'warning' });

            $formExportoder.find('.js-add-detail-exportsupplies').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
        });
       
        $('.real_quantity').on('change', function () {   //Thay đổi trường số lượng thực trong phiếu đề xuất
            const category_id = $(this).attr('data-id');
            const wrapper = $(`tr[id="${category_id}"]`);
            const real = wrapper.find('.real_quantity').val();
            const max = wrapper.find('.real_quantity').attr('max');

            if (real > max) {
                wrapper.find('.real_quantity').attr('title', `Số lượng vật tư tồn trong kho là ${max}`);
            }
            else {
                wrapper.find('.real_quantity').removeAttr('title');
            }
        })

        //Close drawer
        $('.kt-close').on('click', function () {
            drawer.close();
        });
    };

    //Thêm mới raw trong drawer xuất kho
    var addSuppliesToExport = function (typecategories) {
        var type_temp = $('.dmms-form-bunker__exportsupplies').find('#type').val(); //type của phiếu xuất
        var device_string = ''; //Biến chứa html

        const wrapper = $('.wrapper-fill');

        const supplies = wrapper.find('#kt-select-supplies');
        const category_id = supplies.val(); //uuid của category
        const category_prefix = supplies.find('option:selected').text(); //prefix của category
        const category_name = supplies.find('option:selected').attr('data-subtext'); //Tên của category
        const type_category = wrapper.find('#type_category').text(); //Tên của loại (1-vật tư, 2-thiết bị, 3-Dụng cụ LĐ, 4-Phần mềm)
        const document_quantity = wrapper.find('input[id="document_quantity"]').val(); //Số lượng thiết bị theo chứng từ
        const real_quantity = wrapper.find('input[id="real_quantity"]').val(); //Số lượng thực
        const unit = wrapper.find('#unit').text(); //Đơn vị tính
        if (type_temp == 4) {
            var unit_price_str = wrapper.find('input[id="unit_price"]').val(); //Đơn giá kiểu string #,###
            var money_string = wrapper.find('#money_string').text(); //Thành tiền (đơn giá * số lượng thực)
        }

        //Nếu có danh sách devices thì truyền html vào biến devices_string
        if (devices !== []) {

            $.each(devices, function (index, item) {
                device_string += `<input name="detail[][device[]]" value="${item}" />`;
            });
        }

        var body_temp = '';
        //Nếu có giá trị của vật tư thiết bị => add thêm raw vật tư thiết bị
        body_temp += `<tr id="${category_id}">
                        <td class="text-left kt-padding-l-10"><input name="detail[][category_id]" value="${category_id}" hidden />
                            <span class="text-primary">${category_prefix}</span>
                            <br/>${category_name}
                        </td>
                        <td>${type_category}</td>
                        <td><input class="form-control" placeholder="Nhập" name="detail[][document_quantity]" value="${document_quantity}" readonly /></td>
                        <td><input name="detail[][real_quantity]" value="${real_quantity}" hidden /><span class="detail_real_quantity">${real_quantity}</span></td>
                        <td>${unit}</td>`;
        if (type_temp == 4) {
            body_temp += `<td>
                            <input class="form-control detail_unit_price" name="detail[][unit_price_string]"
                                placeholder="Nhập" id="detail_unit_price_${category_id}" value="${unit_price_str}" />
                        </td>
                        <td class="detail_money_string detail_money_string" id="detail_money_string_${category_id}">${money_string}</td>`;
        }
        body_temp += `<td style="display: none" class="list_device" id="list_device_${category_id}">${device_string}</td>
                        <td>
                            <button type="button" class="btn btn-primary js-add-detail-exportsupplies" data-id=${category_id} id="js-add-detail-exportsupplies_${category_id}" disabled>
                                <span class="d-none d-sm-inline-block">Xem chi tiết</span>
                            </button>
                        </td>
                        <td><i class="la la-trash remove_field" style="background-color: #FF5F6D"></i></td>
                    </tr>`;
        wrapper.append(body_temp);
        if (typecategories != 1) {
            $(`tr[id="${category_id}"]`).find('.js-add-detail-exportsupplies').attr('disabled', true);
        }
        else {
            $(`tr[id="${category_id}"]`).find('.js-add-detail-exportsupplies').attr('disabled', false);
        }

        $('.detail_money_string').simpleMoneyFormat();
        $('.detail_unit_price').simpleMoneyFormat();

        //Sau khi add thêm category => reset giá trị của raw nhập liệu
        wrapper.find('#type_category').html('-'); //Phân loại => "-"
        wrapper.find('input[id="document_quantity"]').val(''); //số lượng theo chứng từ => ''
        wrapper.find('input[id="real_quantity"]').val('').addClass('kt-hidden'); //số lượng thực => ''
        wrapper.find('input[id="real_quantity"]').parent('td').find('span').html('-').removeClass('kt-hidden'); //số lượng thực text => "-"
        wrapper.find('#js-add-detail-exportsupplies').attr('disabled', false);
        wrapper.find('#unit').html('-'); //đơn vị tính => "-"
        wrapper.find('#list_device').html(''); //reset listdevice
        if (type_temp == 4) {
            wrapper.find('input[id="unit_price"]').val(''); //đơn giá => ''
            wrapper.find('#money_string').html('0'); //tổng tiền => '0'
        }
        supplies.selectpicker('val', ''); //tên thiết bị vật tư => ""

        devices = [];   //restart arr

        $(wrapper).off('click').on('click', '.js-add-detail-exportsupplies', function () { //Thêm chi tiết cho thiết bị vật tư đã được thêm
            const categoryId = $(this).attr('data-id');
            wrapper.find(`tr[id=${categoryId}] .js-add-detail-exportsupplies`).prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading

            const storage_uuid = $('#uuid').val();
            $.ajax({
                async: false,
                url: `/bunker/getdetailsuppliesinbunker/${storage_uuid}/${categoryId}`,
                method: 'GET',
                data: { limit: -1 }
            })
                .done(function (data) {
                    //$('.dmms-bunker').find('.dmms-drawer--detailsupplies').remove();
                    addDetailSuppliesExport(categoryId, category_prefix, category_name, data.data.items);
                })
                .fail(function (data) {
                    console.log(`Lỗi: ${data.responseJSON.msg}`);
                    $.notify('Lỗi hệ thống. Vui lòng thử lại sau!', { type: 'danger' });
                });
            //$('.dmms-bunker').find('.dmms-drawer--detailsupplies').remove();
            //addDetailSuppliesExport(category_id, category_name, category_prefix);
            wrapper.find(`tr[id=${categoryId}] .js-add-detail-exportsupplies`).prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
        });

        const wrapper_detail = wrapper.find(`tr[id="${category_id}"]`);
        $(wrapper_detail).on('change', `.detail_unit_price`, function (e) { //Thay đổi trường giá
            e.preventDefault();
            const unit_price = parseInt(wrapper_detail.find(`.detail_unit_price`).val().replace(/,/g, ''));
            if (wrapper_detail.find(`input[name="detail[][real_quantity]"]`).val() != '') {
                wrapper_detail.find(`.detail_money_string`).html(parseInt(wrapper_detail.find('input[name="detail[][real_quantity]"]').val()) * unit_price);
                wrapper_detail.find('.detail_money_string').simpleMoneyFormat()
            }
            else {
                wrapper_detail.find(`.detail_money_string`).html('0');
            }
        });

        $(wrapper).find(`tr[id=${category_id}]`).on("click", `.remove_field`, function (e) { //user click on remove text
            e.preventDefault();
            const $this = $(this);

            $this.parent('td').parent('tr').remove();
            $('#kt-select-supplies').find(`option[value="${category_id}"]`).attr('disabled', false); //Disabled option của chọn vật tư
            $('#kt-select-supplies').selectpicker('refresh');
        });
    };

    //Thêm mới raw trong drawer thêm chi tiết của thiết bị vật tư
    var addDetailSuppliesExport = function (category_id, category_prefix, category_name, data, is_first) {

        var list_serial_html = '';
        const title = 'CHI TIẾT THIẾT BỊ';
        subDrawer = new KTDrawer({ title, category_id: category_id || 'new', className: 'dmms-drawer--detailsupplies' });

        if (is_first == undefined) {
            //Lấy tất cả input trong #list_device
            const obj_temp = $('.wrapper-fill').find(`tr[id="${category_id}"] .list_device input`);
            //Gán giá trị object devices
            devices = obj_temp.length > 0 ? obj_temp.serializeJSON().detail[0].device : [];
        }
        else {
            //Lấy tất cả input trong #list_device
            const obj_temp = $('.wrapper-fill').find(`tr[id="first"] #list_device input`);
            //Gán giá trị object devices
            devices = obj_temp.length > 0 ? obj_temp.serializeJSON().detail[0].device : [];
        }

        if (data != null) {
            list_serial_html += `<select data-live-search="true" class="form-control" id="kt-select-serial">
                                    <option value="">Lựa chọn</option>`;
            $.each(data, function (index, item) {
                if (devices.filter(st => st == item.uuid) != '') list_serial_html += `<option value="${item.uuid}" data-expire="${item.expire_device_string}" disabled>${item.serial}</a>`;
                else list_serial_html += `<option value="${item.uuid}" data-expire="${item.expire_device_string}">${item.serial}</a>`;
            })
            list_serial_html += `</select>`;
        }
        var response = `<form class="dmms-form-bunker__exportdetailsupplies" onsubmit="return false">
                        <div class="add-detail-supplies_inner">
                            <table class="table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">Tên thiết bị</th>
                                        <th scope="col">Serial</th>
                                        <th scope="col">Ngày hết hạn BH</th>
                                        <th scope="col">Tác vụ</th>
                                    </tr>
                                </thead>
                                <tbody class="table-detailsupplies">
                                <tr>
                                      <td class="text-left kt-padding-l-10"><input id="detail_uuid" hidden />
                                          <span class="text-primary">${category_prefix}</span>
                                          <br/>${category_name}
                                      </td>
                                      <td>${list_serial_html}</td>
                                      <td><span id="detail_expire_device"></span></td>
                                      <td><i class="la la-plus kt-p-0-tablet-and-mobile" id="js-add-detailsupplies_export"></i></td>
                                    </tr>`;
        if (devices.length != 0) {
            $.each(devices, function (index, item) {
                const device_uuid = item;
                $.ajax({
                    async: false,
                    url: `/device/detailwithparams/${device_uuid}`,
                    method: 'GET',
                })
                    .done(function (res) {
                        response += `<tr>
                                <td class="text-left kt-padding-l-10"><input name="detail[][device[]]" value="${item}" hidden />
                                    <span class="text-primary">${category_prefix}</span>
                                    <br/>${category_name}
                                </td>      
                                <td>${res.data.serial}</td>
                                <td>${res.data.expire_device_format}</td>
                                <td><i class="la la-trash remove_field_detailsupplies" data-serial="${res.data.uuid}" style="background-color: #FF5F6D"></i></td>         
                            </tr>`
                    })
                    .fail(function (res) {
                        console.log(`Lỗi: ${res.responseJSON.msg}`);
                    });
            });
        }
        response += `</tbody>
                     </table>
                        </div>
                        <div class="text-right kt-margin-t-20">
                            <button type="submit" class="btn btn-info js-add-devices">
                                Áp dụng
                            </button>
                            <button type="button" class="btn btn-danger kt-close">
                                Hủy bỏ
                            </button>
                        </div>
                    </form>`;
        subDrawer.open();
        const $content = subDrawer.setBody(response);
        //selected.forEach(function (item, index) {
        //    $('#kt-select-serial').find(`option[value="${item}"]`).attr('disabled', true);
        //    $('#kt-select-serial').selectpicker('refresh');
        //})
        $('#kt-select-serial').selectpicker();
        $('#kt-select-serial').on('change', function () {
            const device_uuid = $(this).val();
            const expire_device_string = $('#kt-select-serial').find(`option[value="${device_uuid}"]`).attr('data-expire');

            if (device_uuid != '') {
                //fill giá trị
                $('#detail_uuid').val(device_uuid);
                $('#detail_expire_device').html(expire_device_string);
            }
            else {
                $('#detail_uuid').val("");
                $('#detail_expire_device').html("");
            }
        });

        initSaveDetailExport($content, category_id, category_name, category_prefix, is_first);

        devices = [];

        $('.kt-close').on('click', function () {

            subDrawer.close();
        });
    };
    //xuat phieu tu de xuat-----------------------------------------------------------------------------------
    var addDetailSuppliesExportoder = function (category_id, category_prefix, category_name, data, is_first) {

        var list_serial_html = '';
        const title = 'CHI TIẾT THIẾT BỊ';
        subDrawer = new KTDrawer({ title, category_id: category_id || 'new', className: 'dmms-drawer--detailsupplies' });

        //Lấy tất cả input trong #list_device
        const obj_temp = $('.wrapper-fill').find(`tr[id="${category_id}"] .list_device input`);
        //Gán giá trị object devices
        devices = obj_temp.length > 0 ? obj_temp.serializeJSON().detail[0].device : [];
        if (data != null) {
            list_serial_html += `<select data-live-search="true" class="form-control" id="kt-select-serial">
                                    <option value="">Lựa chọn</option>`;
            $.each(data, function (index, item) {
                if (devices.filter(st => st == item.uuid) != '') list_serial_html += `<option value="${item.uuid}" data-expire="${item.expire_device_string}" disabled>${item.serial}</a>`;
                else list_serial_html += `<option value="${item.uuid}" data-expire="${item.expire_device_string}">${item.serial}</a>`;
            })
            list_serial_html += `</select>`;
        }
        //<th scope="col">IMEI</th>
        var response = `<form class="dmms-form-bunker__exportdetailsupplies" onsubmit="return false">
                        <div class="add-detail-supplies_inner">
                            <table class="table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">Tên thiết bị</th>
                                        <th scope="col">Serial</th>
                                        <th scope="col">Ngày hết hạn BH</th>
                                        <th scope="col">Tác vụ</th>
                                    </tr>
                                </thead>
                                <tbody class="table-detailsupplies">
                                <tr>
                                      <td class="text-left kt-padding-l-10"><input id="detail_uuid" hidden />
                                          <span class="text-primary">${category_prefix}</span>
                                          <br/>${category_name}
                                      </td>
                                      <td>${list_serial_html}</td>
                                      <td><span id="detail_expire_device"></span></td>
                                      <td><i class="la la-plus kt-p-0-tablet-and-mobile" id="js-add-detailsupplies_export"></i></td>
                                    </tr>`;
        if (devices.length != 0) {
            $.each(devices, function (index, item) {
                const device_uuid = item;
                $.ajax({
                    async: false,
                    url: `/device/detailwithparams/${device_uuid}`,
                    method: 'GET',
                })
                    .done(function (res) {
                        response += `<tr>
                                <td class="text-left kt-padding-l-10"><input name="detail[][device[]]" value="${item}" hidden />
                                    <span class="text-primary">${category_prefix}</span>
                                    <br/>${category_name}
                                </td>      
                                <td>${res.data.serial}</td>         
                                <td>${res.data.expire_device_format}</td>         
                                <td><i class="la la-trash remove_field_detailsupplies" data-serial="${res.data.uuid}" style="background-color: #FF5F6D"></i></td>         
                            </tr>`;
                    })
                    .fail(function (res) {
                        console.log(`Lỗi: ${res.responseJSON.msg}`);
                    });
            });
        }
        response += `</tbody>
                     </table>
                        </div>
                        <div class="text-right kt-margin-t-20">
                            <button type="submit" class="btn btn-info js-add-devices">
                                Áp dụng
                            </button>
                            <button type="button" class="btn btn-danger kt-close">
                                Hủy bỏ
                            </button>
                        </div>
                    </form>`;
        subDrawer.open();
        const $content = subDrawer.setBody(response);
        //selected.forEach(function (index, item) {
        //    
        //    $('#kt-select-serial').find(`option[value="${index}"]`).attr('disabled', true);
        //    $('#kt-select-serial').selectpicker('refresh');
        //})
        //if (selected == null) {
        //    $('#kt-select-serial').selectpicker('refresh');
        //}
        $('#kt-select-serial').selectpicker();
        $('#kt-select-serial').on('change', function () {
            const device_uuid = $(this).val();

            const expire_device_string = $('#kt-select-serial').find(`option[value="${device_uuid}"]`).attr('data-expire');

            if (device_uuid != '') {
                //fill giá trị
                $('#detail_uuid').val(device_uuid);
                $('#detail_expire_device').html(expire_device_string);
            }
            else {
                $('#detail_uuid').val("");
                $('#detail_expire_device').html("");
            }
        });
        initSaveDetailExportoder($content, category_id, category_name, category_prefix, is_first);
        devices = [];

        $('.kt-close').on('click', function () {

            subDrawer.close();
        });
    };
    //Add chi tiết cho thiết bị
    var initSaveDetailExport = function ($content, category_id, category_name, category_prefix, is_first) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-form-bunker__exportdetailsupplies');
        initForm2($form, category_id, is_first);

        const wrapper = $('.table-detailsupplies');

        $('#js-add-detailsupplies_export')
            .off('click')
            .on('click', function (e) {
                e.preventDefault();
                const detail_uuid = $('.add-detail-supplies_inner').find(`input[id="detail_uuid"]`);
                const serial = $('.add-detail-supplies_inner').find(`#kt-select-serial`);
                const serial_text = serial.find(`option:selected`).text();
                //const imei = $('.add-detail-supplies_inner').find(`#detail_imei`);
                const expire_device_string = $('.add-detail-supplies_inner').find(`#detail_expire_device`);
                if (serial.val() != '') {
                    //<td>${imei.text()}</td>
                    wrapper.append(`<tr>
                                     <td class="text-left kt-padding-l-10"><input name="detail[][device[]]" value="${detail_uuid.val()}" hidden />
                                        <span class="text-primary">${category_prefix}</span>
                                        <br/>${category_name}
                                     </td>
                                     <td>${serial_text}</td>
                                     <td>${expire_device_string.text()}</td>
                                     <td><i class="la la-trash remove_field_detailsupplies" data-serial=${serial.val()} style="background-color: #FF5F6D"></i></td>
                                 </tr>`);
                    //reset values
                    $('#kt-select-serial').find(`option[value="${detail_uuid.val()}"]`).attr('disabled', true);
                    $('#kt-select-serial').selectpicker('refresh');
                    detail_uuid.val('');
                    serial.selectpicker('val', '');
                    //imei.html('');
                    expire_device_string.html('');
                }
                else $.notify('Vui lòng chọn serial!', { type: 'warning' });
            });

        $(wrapper).on("click", ".remove_field_detailsupplies", function (e) { //user click on remove text
            e.preventDefault();
            const $this = $(this);
            const serial = $this.attr('data-serial');
            $('#kt-select-serial').find(`option[value="${serial}"]`).attr('disabled', false); //Enabled option
            $('#kt-select-serial').selectpicker('refresh');

            $this.parent('td').parent('tr').remove();
        });
    };
    //xuat phieu tu de xuat -------------------------------------------------------------------------------
    var initSaveDetailExportoder = function ($content, category_id, category_name, category_prefix, is_first) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-form-bunker__exportdetailsupplies');
        initForm3($form, category_id, is_first);

        const wrapper = $('.table-detailsupplies');
        const document_quantity = parseInt($(`tr[id="${category_id}"]`).find('.document_quantity').val());
        var temp = 0;

        $('#js-add-detailsupplies_export')
            .off('click').on('click', function (e) {
                e.preventDefault();
                const detail_uuid = $('.add-detail-supplies_inner').find(`input[id="detail_uuid"]`);
                const serial = $('.add-detail-supplies_inner').find(`#kt-select-serial`);
                const serial_text = serial.find('option:selected').text();
                //const imei = $('.add-detail-supplies_inner').find(`#detail_imei`);
                const expire_device_string = $('.add-detail-supplies_inner').find(`#detail_expire_device`);
                if (temp < document_quantity) {
                    if (serial.val() != '') {
                        wrapper.append(`<tr>
                                     <td class="text-left kt-padding-l-10"><input name="detail[][device[]]" value="${detail_uuid.val()}" hidden />
                                        <span class="text-primary">${category_prefix}</span>
                                        <br/>${category_name}
                                     </td>
                                     <td>${serial_text}</td>

                                     <td>${expire_device_string.text()}</td>
                                     <td><i class="la la-trash remove_field_detailsupplies" data-serial="${serial.val()}" style="background-color: #FF5F6D"></i></td>
                                 </tr>`);
                        //reset values

                        $('#kt-select-serial').find(`option[value="${detail_uuid.val()}"]`).attr('disabled', true);
                        $('#kt-select-serial').selectpicker('refresh');
                        detail_uuid.removeAttr('name').val('');
                        serial.selectpicker('val', '');
                        //imei.html('');
                        expire_device_string.html('');
                        temp++;
                    }
                    else $.notify('Vui lòng chọn serial!', { type: 'warning' });
                }
                else $.notify('Số lượng thiết bị không được vượt quá số lượng thiết bị CT!', { type: 'warning' });
            });

        $(wrapper).on("click", ".remove_field_detailsupplies", function (e) { //user click on remove text
            e.preventDefault();
            const $this = $(this);
            const serial = $this.attr('data-serial');

            $('#kt-select-serial').find(`option[value="${serial}"]`).attr('disabled', false); //Enabled option
            $('#kt-select-serial').selectpicker('refresh');

            $this.parent('td').parent('tr').remove();
            temp--;
        });
    };
    //Form validate cho add chi tiết thiết bị
    var initForm2 = function ($form, category_id, is_first) {
        $form.validate({
            rules: {

            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {
                $(form).find('button[type="submit"]').prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading

                devices = [];
                const data = $(form).serializeJSON() || {};
                if (data != {} && data.detail != undefined) {
                    $.each(data.detail[0].device || [], function (index, item) {
                        devices.push(item);
                    });
                }

                if (data.detail != undefined && $('#kt-select-serial').val() == "") {
                    KTApp.block($form);

                    $.notify('Đã thêm chi tiết!', { type: 'success' });
                    if (is_first == undefined) { //Sau khi thêm chi tiết cho thiết bị vật tư (category_id != null)=> lấy ra danh sách thiết bị và thay đổi giá trị SL thực va thành tiền
                        const wrapper = $(`.wrapper-fill tr[id=${category_id}]`);
                        var money_string = '';
                        var money = 0;

                        wrapper.find(`tr[id="${category_id}"] .list_device`).html('');
                        $.each(devices, function (index, item) {
                            wrapper.find(`tr[id="${category_id}"] .list_device`).append(`<input name="detail[][device[]]" value="${item}" hidden />`)
                        });
                        wrapper.find('input[name="detail[][real_quantity]"]').val(`${devices.length}`);
                        wrapper.find('input[name="detail[][real_quantity]"]').parent('td').find('span').html(devices.length);
                        if (wrapper.find('input[name="detail[][unit_price_string]"]').length > 0) {
                            money_string = wrapper.find('input[name="detail[][unit_price_string]"]').val();
                            money = parseInt(money_string.replace(/,/g, ''));
                            wrapper.find(`tr[id="${category_id}"] .detail_money_string`).html(devices.length * money);
                        }

                        $('.detail_money_string').simpleMoneyFormat();
                        /**
                        * Close drawer
                        */
                        subDrawer.close();
                    }
                    else { //Sau khi thêm chi tiết cho thiết bị vật tư (category_id = null)=> lấy ra danh sách thiết bị và thay đổi giá trị SL thực va thành tiền
                        const wrapper = $(`.wrapper-fill tr[id="first"]`);
                        var money_string = '';
                        var money = 0;
                        wrapper.find('#list_device').html('');
                        $.each(devices, function (index, item) {
                            wrapper.find('#list_device').append(`<input name="detail[][device[]]" value="${item}" hidden />`);
                        });

                        wrapper.find('#real_quantity').val(`${devices.length}`);
                        wrapper.find('#real_quantity').parent('td').find('span').html(devices.length);
                        if (wrapper.find('input[name="detail[][unit_price_string]"]').length > 0) {
                            money_string = wrapper.find('input[name="detail[][unit_price_string]"]').val();
                            money = parseInt(money_string.replace(/,/g, ''));
                            wrapper.find(`#money_string`).html(devices.length * money);
                        }

                        $('#money_string').simpleMoneyFormat();
                        /**
                        * Close drawer
                        */
                        subDrawer.close();
                    }
                }
                else {
                    KTApp.unblock($form);
                    $(form).find('button[type="submit"]').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading

                    $.notify('Vui lòng thêm thiết bị trước khi áp dụng!', { type: 'warning' });
                }

                return false;
            },
        });
    }
    //xuat tu phieu de xuat-----------------------------------------------------------------
    var initForm3 = function ($form, category_id, is_first) {

        $form.validate({
            rules: {

            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {
                $(form).find('button[type="submit"]').prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading

                devices = [];
                const data = $(form).serializeJSON() || {};
                if (data != {} && data.detail != undefined) {
                    $.each(data.detail[0].device || [], function (index, item) {
                        devices.push(item);
                    });
                }

                if (data.detail != undefined && $('#kt-select-serial').val() == "") {
                    KTApp.block($form);
                    $.notify('Đã thêm chi tiết!', { type: 'success' });
                    //Sau khi thêm chi tiết cho thiết bị vật tư (category_id != null) => lấy ra danh sách thiết bị và thay đổi giá trị SL thực va thành tiền
                    const wrapper = $(`.wrapper-fill tr[id=${category_id}]`);
                    wrapper.find(`.list_device`).html('');
                    $.each(devices, function (index, item) {
                        wrapper.find(`.list_device`).append(`<input name="detail[][device[]]" value="${item}" hidden />`)
                    });
                    wrapper.find('input[name="detail[][real_quantity]"]').val(`${devices.length}`);
                    wrapper.find('input[name="detail[][real_quantity]"]').parent('td').find('span').html(devices.length);
                    //wrapper.find(`#detail_money_string_${category_id}`).html(devices.length * money);

                    //$('.detail_money_string').simpleMoneyFormat();
                    /**
                    * Close drawer
                    */
                    subDrawer.close();
                }
                else {
                    KTApp.unblock($form);
                    $(form).find('button[type="submit"]').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading

                    $.notify('Vui lòng thêm thiết bị trước khi áp dụng!', { type: 'warning' });
                }

                return false;
            },
        });
    }

    return {
        // Public functions
        init: function () {
            initSearch();
            initEventListeners();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-bunker').length) {
        DMMSBunker.init();
    }
});