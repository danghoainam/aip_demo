"use strict";
// Class definition

var DMMSCustomer = function () {
    // Private functions

    var image = null;
    var dataTableInstance = null;
    var dataTableDevice = null;
    var dataTableInstance1 = null;
    var drawer = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    debugger
    var CURRENT_PAGE_DETAIL = 1;
    var START_PAGE = 0;
    var drawerInstance = null;
    var MAX_ITEM_INPAGE = 0;

    var initTable = function () {
        var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
        if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
            $.notify(editSucessCache.massage, { type: 'success' });
            localStorage.removeItem('edit_success');
        }
        dataTableInstance = $('.kt-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/customer/gets',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            if (raw.items != null) {
                                $.each(raw.items, function (index, item) {
                                    for (var property in item) {
                                        if (item[property] && typeof item[property] === "string") {
                                            item[property] = item[property].replace(/</g, "&lt;").replace(/>/g, "&gt;");
                                        }
                                    }
                                })
                            }
                            const { pagination } = raw;

                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page;
                            CURRENT_PAGE = page || 1;
                            START_PAGE = 0;
                            LIMIT = perpage;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { province_id, group_uuid, keyword, start_page, village_id, town_id, phone_number } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            province_id: $('#js-filter-location').val(),
                            village_id: $('#locationvillage').val(),
                            town_id: $('#locationtown').val(),
                            group_uuid: group_uuid || $('#js-filter-group').val(),
                            keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            phone_number: (phone_number || $('#js-filter-phonenumber').val()),
                            page: pagination.page
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,
                //input: $('#js-filter-keyword'),

            },

            columns: [
                //{
                //    field: 'uuid',
                //    title: '#',
                //    sortable: false,
                //    width: 20,
                //    type: 'string',
                //    selector: { class: 'kt-checkbox--solid' },
                //    textAlign: 'center',
                //},
                {
                    field: '',
                    title: '#',
                    sortable: false,
                    width: 35,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'name',
                    title: 'Đơn vị thụ hưởng',

                },
                //{
                //    field: 'contact_name',
                //    title: 'Người liên hệ',
                //    textAlign: 'center',
                //    width: 130,
                //},
                //{
                //    field: 'contact_phone_number',
                //    title: 'Điện thoại',
                //    textAlign: 'center',
                //    width: 80,
                //},
                {
                    field: 'provinces.name',
                    title: 'Tỉnh/Thành',
                    textAlign: 'center',
                    width: 100,
                }, {
                    field: 'towns.name',
                    title: 'Quận/Huyện/Thị xã',
                    textAlign: 'center',
                    width: 120,
                    template: function (data) {
                        return data.towns.name != null ? data.towns.name : '<span class="text-danger">Chưa xác định</span>';
                    },
                }, {
                    field: 'villages.name',
                    title: 'Phường/Xã/Thị Trấn',
                    textAlign: 'center',
                    width: 130,
                    template: function (data) {
                        return data.villages.name != null ? data.villages.name : '<span class="text-danger">Chưa xác định</span>';
                    },
                },
                {
                    field: 'address',
                    title: 'Địa chỉ',
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    textAlign: 'center',
                    overflow: 'visible',
                    width: 110,
                    autoHide: false,
                    template: function (data) {
                        if (data.account != undefined) {
                            if (data.account.status == 0) {
                                var title = "Mở tài khoản";
                                var class1 = "js-unlock-cus btn btn-sm btn-success btn-icon btn-icon-md";
                                var icon = "la-unlock-alt";

                            }
                            else {
                                var title = "Khóa tài khoản";
                                var class1 = "js-lock-cus btn btn-sm btn-warning btn-icon btn-icon-md";
                                var icon = "la-unlock";
                            }
                        }
                        //<button data-id="${data.account.uuid}" title=${title} class="${class1}">
                        //    <i class="la ${icon}"></i>
                        //</button>
                        return `
                            <a href="/customer/update/${data.uuid}" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </a>
                            <button data-id="${data.uuid}" type="button" title="Xóa" class="btn btn-sm btn-danger btn-icon btn-icon-md js-delete-customer">
                                <i class="la la-trash"></i>
                            </button>
                        `;
                        //<button data-id="${data.uuid}" title="Đặt lại mật khẩu" class="js-reset-account btn btn-sm btn-warning btn-icon btn-icon-md">
                        //    <i class="la la-key"></i>
                        //</button>
                        //<a href="/customer/details/${data.uuid}" title="Chi tiết" class="btn btn-sm btn-success btn-icon btn-icon-md">
                        //    <i class="la la-info"></i>
                        //</a>
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        })
    };

    var initSearch = function () {
        $('#js-filter-location')
            .on('change', function () {
                if ($(this).val() == '') {
                    $('#js-filter-location').val('');
                    $('#js-filter-location').selectpicker('refresh');
                    $('#locationtown').val('');
                    $('#locationtown').attr('disabled', true);
                    $('#locationtown').selectpicker('refresh');
                    $('#locationvillage').val('');
                    $('#locationvillage').attr('disabled', true);
                    $('#locationvillage').selectpicker('refresh')
                    if (dataTableInstance) {
                        dataTableInstance.search($('#js-filter-location').val(), 'provinceId'),
                            START_PAGE = 1
                    };
                }
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'location_id');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-group')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'group_uuid');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#locationtown')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'town_id');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#locationvillage')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'village_id');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });
        $('#js-filter-phonenumber')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase().trim(), 'phone_number');
                    START_PAGE = 1;
                }
            });
        $('#js-filter-keyword--ticket')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });

        $('#js-filter-ticketstate')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'state');
                    START_PAGE = 1;
                }
            })
            .selectpicker();

        if ($("#location_id").val() !== '') {
            $('#kt-select-location').selectpicker('val', $("#location_id").val());
        }
        else {
            $('#kt-select-location').selectpicker();
        }

        $('#js-filter-keyword_device')
            .on('change', function () {
                if (dataTableDevice) {
                    dataTableDevice.search($(this).val().toLowerCase().trim(), 'keyword');
                    START_PAGE = 1;
                }
            });
    };

    var deleteCustomer = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa đơn vị...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/customer/delete/${id}`,
                method: 'DELETE',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa đơn vị thành công!', { type: 'success' });

                    if (dataTableInstance) {
                        dataTableInstance.reload();
                    }
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa đơn vị không thành công!', { type: 'danger' });
                });
        }
    };

    var resetPassword = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang tải dữ liệu...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/account/resetpassword/${id}`,
                method: 'POST',
            })
                .done(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(`Đặt lại mật khẩu thành công!`, { type: 'success' });

                    if (dataTableInstance) {
                        dataTableInstance.reload();
                    }
                })
                .fail(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Đặt lại mật khẩu không thành công!', { type: 'danger' });
                });
        }
    };

    var saveResult = function (id) {
        drawerInstance.block();
        const $drawer = drawerInstance.getElement();
        const selected = $drawer.find('select').val();
        const successMsg = 'Lưu nhóm thành công!';
        const errorMsg = 'Lưu nhóm không thành công!';
        $.ajax({
            // url: `/group/${module}/${id}`,
            url: `/group/upsert/${id}`,
            method: 'POST',
            data: {
                request: {
                    uuid: id,
                    uuids: selected.join(','),
                    name: group_name,
                    description: group_desc,
                    module: module
                }
            }
        }).done((res) => {
            drawerInstance.unblock();
            drawerInstance.close();
            $.notify(successMsg, { type: 'success' });
        }).fail((res) => {
            drawerInstance.unblock();
            $.notify(errorMsg, { type: 'danger' });
        });
    };

    var initDetailDrawer = function (id) {
        const title = id ? 'Chi tiết đơn vị' : '';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--detailcustomer' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/customer/details/${id}` : '',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    const $form = $content.find('.dmms-customer__form');
                    initValidationContact($form);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    //table ticket
    $(".listticket").on('click', function () {
        const customer_uuid = $("#uuid").val();
        if (dataTableInstance) {
            dataTableInstance.load();
        }
        else {
            dataTableInstance = $('.kt-datatable--ticket').KTDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: `/ticket/getsticket/${customer_uuid}`,
                        },
                        response: {
                            map: function (res) {
                                const raw = res && res.data || {};
                                const { pagination } = raw;
                                const perpage = pagination === undefined ? undefined : pagination.perpage;
                                const page = pagination === undefined ? undefined : pagination.page;
                                START_PAGE = 0;
                                return {
                                    data: raw.items || [],
                                    meta: {
                                        ...pagination || {},
                                        perpage: perpage || LIMIT,
                                        page: page || 1,
                                    },
                                }
                            },
                        },
                        filter: function (data = {}) {
                            const query = data.query || {};
                            const pagination = data.pagination || {};
                            const { keyword, state } = query;
                            if (START_PAGE === 1) {
                                pagination.page = START_PAGE;
                            }
                            return {
                                limit: pagination.perpage || LIMIT,
                                keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                                state: state || $('#js-filter-ticketstate').val(),
                                page: pagination.page
                            };
                        }
                    },
                    pageSize: LIMIT,
                    serverPaging: true,
                    serverFiltering: true,
                },
                layout: {
                    scroll: true
                },
                search: {
                    onEnter: true,
                },
                columns: [
                    {
                        field: 'uuid',
                        title: '#',
                        sortable: false,
                        textAlign: 'center',
                        width: 20,
                        template: function (data, row) {
                            return (row + 1) + (CURRENT_PAGE - 1) * MAX_ITEM_INPAGE;
                        },
                    },
                    {
                        field: 'title',
                        title: 'Nội dung ticket',

                        template: function (data) {
                            var subconten = '';
                            var countstring = data.content.split(" ");
                            if (countstring.length > 10) {
                                for (var i = 0; i < countstring.length; i++) {
                                    if (i <= 10) {
                                        subconten = subconten + countstring[i] + " ";
                                    }
                                    else {
                                        subconten = subconten + "..."; break;
                                    }
                                }
                            }
                            else {
                                subconten = data.content
                            }
                            var output = '<div class="kt-user-card__details">\
                                               <span><strong>' + data.title + '</strong></span><br>\
                                               <span>' + subconten + '</span>';
                            output += "</div>";
                            return output;
                        },

                    },
                    {
                        field: '#',
                        title: 'Đơn vị-Người liên hệ',

                        template: function (data) {
                            var output = '';
                            if (data.customer != null) {
                                output += '<div class="kt-user-card__details">\
                                        <a href = "/customer/update/'+ data.customer.uuid + '"><strong>' + data.customer.name + '</strong></a><br>';
                                if (data.contact_name != null) {
                                    output += '<span>' + data.contact_name + '</span>';
                                }
                                else {
                                    output = output;
                                }
                                if (data.contact_phone_number != null) {
                                    output += ' - <span>' + data.contact_phone_number + '</span><br>';
                                }
                                else {
                                    output = output
                                }
                                output += "</div>";
                            }
                            else {
                                output += '<div class="kt-user-card__details">\
                                        <strong> Chưa có đơn vị </strong><br>';
                            }

                            return output;
                        },
                    },
                    {
                        field: 'creator.name',
                        title: 'Thông tin người tạo',
                        width: 150,
                        template: function (data) {
                            var output = '<div class="kt-user-card__details">';
                            if (data.creator !== null && data.creator.name !== '') {
                                output += '<a href = "/staff/update/' + data.creator.uuid + '"><span><strong>' + data.creator.name + '</strong></span ></a> <br>';
                            }
                            output += '<span>' + data.time_created_format + '</span><br>';
                            //if (data.time_last_update_format) {
                            //    output += '<span>Cập nhật cuối: </span><span>' + data.time_last_update_format + '</span><br>';
                            //}
                            output += "</div>";
                            return output;
                        }
                    },
                    {
                        field: 'priority',
                        title: 'Độ ưu tiên',
                        width: 80,
                        textAlign: 'center',
                        template: function (row) {
                            var priorities = {
                                1: { 'title': 'Khẩn cấp', 'class': ' kt-badge--danger' },
                                2: { 'title': 'Ưu tiên', 'class': ' kt-badge--warning' },
                                3: { 'title': 'Bình thường', 'class': 'kt-badge--primary' },
                            };
                            if (row.priority) {
                                return '<p class="kt-badge ' + priorities[row.priority].class + ' kt-badge--inline kt-badge--pill">' + priorities[row.priority].title + '</p>';
                            }
                        }
                    },
                    {
                        field: '#1',
                        title: 'Đánh giá',
                        template: function (data) {
                            var output = '';
                                    if (data.rating != null) {
                                        output += `<div class='rating-stars'>
                                            <ul id='stars'>`;
                                        for (var i = 1; i <= 5; i++) {
                                            if (i <= data.rating) {
                                                output += `<li class='star selected'>
                                                            <i class='la la-star la-fw'></i>
                                                        </li>`;
                                            }
                                            else {
                                                output += `<li class='star'>
                                                            <i class='la la-star la-fw'></i>
                                                        </li>`;
                                            }
                                        }
                                        output += `</ul>
                                        </div>`;
                                    }
                                    else {
                                        output += '<span class="text-danger">Chưa đánh giá</span>';
                                    }
                             
                            return output;
                        }
                    },
                    {
                        field: 'time_created_format',
                        title: 'Thời gian tạo',
                        width: 130,
                    },
                    {
                        field: 'state',
                        title: 'Trạng thái',
                        textAlign: 'center',
                        width: 110,

                        template: function (row) {
                            var states = {
                                1: { 'title': 'Chưa tiếp nhận', 'class': 'kt-badge--danger' },
                                2: { 'title': 'Đã tiếp nhận', 'class': ' kt-badge--warning' },
                                3: { 'title': 'Đang xử lý', 'class': ' kt-badge--primary' },
                                4: { 'title': 'Đã xử lý xong', 'class': ' kt-badge--primary' },
                                5: { 'title': 'Hoàn thành', 'class': ' kt-badge--success' },
                                6: { 'title': 'Đã hủy', 'class': ' kt-badge--danger' },
                            };
                            if (row.state) {
                                return '<p class="kt-badge ' + states[row.state].class + ' kt-badge--inline kt-badge--pill">' + states[row.state].title + '</p>';
                            }
                            else {
                                return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới</span>';
                            }
                        }
                    }],
                sortable: false,
                pagination: true,
                responsive: true,
            });
        }
        
    });

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-customer')
            .on('click', '.js-delete-customer', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Đơn vị sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteCustomer(id);
                    }
                });
            })
            .off('click', '.js-reset-account')
            .on('click', '.js-reset-account', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                $.ajax({
                    url: id ? location.origin + `/customer/detailCustomerAjax/${id}` : '',
                    method: 'GET',
                })
                    .done(function (res) {
                        var account_id = res.data.account.uuid;
                        KTApp.swal({
                            title: 'Bạn có muốn đặt lại mật khẩu?',
                            text: 'Hệ thống sẽ gửi bạn mật khẩu sau ít phút.',
                            icon: 'info',
                            dangerMode: true,
                        }).then((confirm) => {
                            if (confirm) {
                                resetPassword(account_id);
                            }
                        });
                    })
            })
            .off('click', '.js-lock-cus')
            .on('click', '.js-lock-cus', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Bạn muốn khóa tài khoản.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        lockCus(id);
                    }
                });
            })
            .off('click', '.js-unlock-cus')
            .on('click', '.js-unlock-cus', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Bạn muốn mở khóa tài khoản.',
                    icon: 'info',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        unlockCus(id);
                    }
                });
            })
            .off('click', '.js-list-group-customer')
            .on('click', '.js-list-group-customer', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initDrawer({
                    id
                });
            })
            .off('click', '.js-save-group-customer')
            .on('click', '.js-save-group-customer', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                saveResult({
                    id,
                });
            })
            .on('click', '.js-detail-customer', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                initDetailDrawer(id || '');
            })
            .on('click', '.js-deviceincustomer', function () {
                debugger;
                const $this = $(this);
                const customer_uuid = $this.attr('data-customer');
                const categoryId = $this.attr('data-category');
                const package_uuid = $this.attr('data-package');
                initDetail(customer_uuid, categoryId, package_uuid);
            });
    };
    var initDetail = function (customer_uuid, categoryId, package_uuid) {
        debugger
        const title = `DANH SÁCH CHI TIẾT THIẾT BỊ`;
        drawer = new KTDrawer({ title, customer_uuid: customer_uuid || 'new', className: 'dmms-drawer--upload' });

        drawer.on('ready', () => {
            $.ajax({
                url: `/customer/detaildevice`,
                method: 'GET',
                
            })
                .done(function (response) {
                    debugger
                    const $content = drawer.setBody(response);
                    initContentdetailcus($content, customer_uuid, categoryId, package_uuid);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var initContentdetailcus = function ($content, customer_uuid, categoryId, package_uuid) {
        debugger
        var cus = customer_uuid;
        var cate = categoryId;
        var pac = package_uuid;
        dataTableInstance1 = $('.kt-datatable-device-customer').KTDatatable({

            data: {
                type: 'remote',
                source: {
                    read: {
                        url: `/device/gets`,
                    },
                    response: {
                        map: function (res) {
                            debugger
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page;

                            CURRENT_PAGE_DETAIL = page || 1;
                           
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        debugger
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { categoryId, customerUuid, packageUuid } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            customerUuid: cus,
                            categoryId: cate,
                            packageUuid:pac,
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,
            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 35,
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE_DETAIL - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    },
                    autoHide: false,
                    textAlign: 'center',

                },
                {
                    field: 'code',
                    title: 'Code',
                    width: 110,

                },
                {
                    field: 'prefixName',
                    title: 'Tên thiết bị',
                    template: function (data) {
                        return `<strong>${data.prefixName}</strong><br/>${data.name}`
                    }
                },
                {
                    field: 'serial',
                    title: 'Serial',
                },
                
                {
                    field: 'supplierName',
                    title: 'Nhà cung cấp',
                    width: 110,

                },
                {
                    field: 'packageName',
                    title: 'Phòng tiết bị',
                    width: 110,

                },

                {
                    field: 'state',
                    title: 'Trạng thái',
                    width: 150,
                    template: function (data) {
                        var states = {
                            1: { 'title': 'Hoạt động', 'class': 'kt-badge--success' },
                            2: { 'title': 'Sửa chữa', 'class': ' kt-badge--warning' },
                            3: {
                                'title': 'Đến lịch bảo hành', 'class': ' kt-badge--primary', 'style': 'background-color:#074648c4'
                            },
                            4: { 'title': 'Sửa chữa', 'class': 'kt-badge--warning' },
                            5: { 'title': 'Sửa chữa', 'class': 'kt-badge--warning' },
                            6: { 'title': 'Sửa chữa', 'class': 'kt-badge--warning' },
                            7: { 'title': 'Sửa chữa', 'class': 'kt-badge--warning' },
                            8: { 'title': 'Thay thế tạm thời', 'class': 'kt-badge--info' },
                            9: { 'title': 'Hỏng', 'class': 'kt-badge--danger' },
                            10: { 'title': 'Hoạt động', 'class': 'kt-badge--danger' },
                            11: { 'title': 'Hỏng', 'class': 'kt-badge--warning' },
                            12: { 'title': 'Hoạt động', 'class': 'kt-badge--success' },
                            13: { 'title': 'Đã xuất thanh lý', 'class': 'kt-badge--primary' },
                            14: { 'title': 'Đã xuất tiêu hao', 'class': 'kt-badge--primary' },
                            15: { 'title': 'Đã hoàn trả nhà cung cấp', 'class': 'kt-badge--primary' },
                            16: { 'title': 'Thêm mới/cập nhật thiết bị', 'class': 'kt-badge--info' },
                            17: { 'title': 'Hoạt động', 'class': 'kt-badge--success' },

                        };
                        if (data.state) {
                            return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill"' + 'style="' + states[data.state].style + '">' + states[data.state].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới</span>';
                        }
                    },
                    autoHide: false,
                    width: 150,
                    textAlign: 'center'
                },
            ],
            sortable: false,
            pagination: true,
            responsive: true,
        });

        //const $form1 = $content.find('.dmms-knowledge__formqa');
        //initForm1($form1);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));


        /**
         * Init Dropzone
         */
        //initDropzone();
    };
    var initValidation = function () {
        //---add or remove input in draw---
        const number_regex = /^(0|\+)[0-9\s.-]{9,11}$/;
        const wrapper = $(".input_fields_wrap"); //Fields wrapper
        const add_button = $(".add_field_button"); //Add button ID

        const name = $('#name_contact');
        const phone = $('#phone_contact');
        const duty = $('#duty_contact');

        $(add_button).click(function (e) { //on add input button click
            e.preventDefault();
            debugger
            if (phone.val() != "" && number_regex.test(phone.val()) == true) {
                if (phone.val() != "" && duty.val() != "") {
                    $("#phone_contact").css('border', '1px solid #e2e5ec');
                    wrapper.append(`<div class="row kt-margin-t-10" style="padding-left: 10px; padding-right:10px">
                            <div class="kt-input-icon kt-input-icon--left col-4">
                                <input name="contacts[][name]" class="form-control" value="${name.val()}" />
                                <span class="kt-input-icon__icon"><span><i class="la la-user"></i></span></span>
                            </div>
                            <div class="kt-input-icon kt-input-icon--left col-4 kt-padding-l-10">
                                <input name="contacts[][phone_number]" class="form-control" value="${phone.val()}" />
                                <span class="kt-input-icon__icon"><span><i class="la la-phone"></i></span></span>
                            </div>
                            <div class="kt-input-icon kt-input-icon--left col-3 kt-padding-l-10">
                                <input name="contacts[][duty]" class="form-control" value="${duty.val()}" />
                                <span class="kt-input-icon__icon"><span><i class="la la-user"></i></span></span>
                            </div>
                            <i class="btn btn-danger la la-minus remove_field ml-auto"></i>
                        </div>`);
                    name.val('');
                    phone.val('');
                    duty.val('');
                }
                else {
                    $.notify("Vui lòng nhập đủ thông tin người liên hệ!", { type: 'danger' });
                }
            }
            else {
                $.notify("Số điện thoại sai định dạng", { type: 'danger' });
            }

            //$(wrapper).append(ddlStaff); //add input box
        });

        $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
            e.preventDefault();
            $(this).parent('div').remove();
        });

        //---end: add or remove input in draw---

        $("#dmms-customer__form").validate({
            rules: {
                name: {
                    required: true,
                },
                province_id: {
                    required: true,
                }
            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên đơn vị.'
                },
                province_id: {
                    required: 'Vui lòng nhập tỉnh thành.'
                }
            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('#dmms-customer__form', -200);
                $('.kt-select-location').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });
            },
            submitHandler: function (form) {
                debugger
                $(form).find('button[type="submit"]').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                if ($(phone).val() !== '' && $(phone).val() !== undefined && number_regex.test($(phone).val()) == false) {
                    $.notify('Số điện thoại sai định dạng!', { type: 'danger' });
                }
                else {
                    $.ajax({
                        url: uuid ? `/customer/update/${uuid}` : '/customer/create',
                        method: uuid ? 'PUT' : 'POST',
                        data,
                    })
                        .done(function () {
                            KTApp.block('#dmms-customer__form');
                            $(".submit").attr("disabled", true);
                            const successMsg = uuid ? 'Sửa thông tin thành công!' : 'Thêm đơn vị thành công!';
                            $.notify(successMsg, { type: 'success' });
                            /**
                             * Close drawer
                             */
                            reloadTable();
                            localStorage['edit_success'] = JSON.stringify({ from: (uuid ? `/customer/update` : '/customer/create'), massage: successMsg });

                            //window.location.replace("customer");
                            window.location = window.location.origin + `/customer`;
                        })
                        .fail(function (data) {
                            $(form).find('button[type="submit"]').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                            KTApp.unblock('#dmms-customer__form');
                            const errorMsg = uuid ? data.responseJSON.msg + '!' : data.responseJSON.msg + '!';
                            $.notify(errorMsg, { type: 'danger' });
                        });
                }
                return false;
            },

        });

    };

    var lockCus = function (id) {

        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang khóa tài khoản...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/staff/LockAccount/${id}`,
                method: 'GET',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Khóa tài khoản thành công!', { type: 'success' });

                    if (dataTableInstance) {
                        dataTableInstance.reload();
                    }
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Khóa tài khoản không thành công!', { type: 'danger' });
                });
        }
    };
    var unlockCus = function (id) {

        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang mở khóa tài khoản...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/staff/UnlockAccount/${id}`,
                method: 'GET',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Mở khóa tài khoản thành công!', { type: 'success' });

                    if (dataTableInstance) {
                        dataTableInstance.reload();
                    }
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Mở khóa tài khoản không thành công!', { type: 'danger' });
                });
        }
    };

    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };
    var renderDualList = function (left, right) {
        const options = [];
        $.each(left, (index, { uuid, name }) => {
            const text = [name || ''];
            options.push({
                text: text.join(' - ') || '-- No name --',
                value: uuid,
            });
        });

        $.each(right, (index, { uuid, name }) => {
            const text = [name || ''];

            options.push({
                text: text.join(' - ') || '-- No name --',
                value: uuid,
                selected: true,
            });
        });

        const optsHtml = [];
        $.each(options, (index, item) => {
            optsHtml.push(`<option value="${item.value}" ${item.selected ? 'selected' : ''}>${item.text}</option>`);
        });

        const $content = drawerInstance.setBody(`
            <select class="kt-dual-listbox" multiple>${optsHtml.join('')}</select>
        `);

        new KTDualListBox($content.find('.kt-dual-listbox')[0], {
            availableTitle: 'Danh sách nhóm',
            selectedTitle: 'Danh sách nhóm của đơn vị',
            //availableTitle: module === 'staff' ? 'Danh sách nhân viên' : 'Danh sách đơn vị',
            //selectedTitle: module === 'staff' ? 'Nhân viên trong nhóm' : 'Đơn vị trong nhóm',
        });
    };

    var initDrawer = function ({ id }) {
        const title = 'Danh sách nhóm';
        drawerInstance = new KTDrawer({
            title,
            id: `group-all`,
            onRenderFooter: (callback) => {
                callback(`
          <button data-id=${id} type="button" class="btn btn-primary js-save-group-customer">
            <i class="la la-save"></i>
            Lưu
          </button>
        `);
            },
        });

        drawerInstance.on('ready', () => {
            var left = null;
            var right = null;
            $.ajax({
                url: `/customer/get-list-group`,
                method: 'GET',
            })
                .done((res) => {
                    /**
                     * Set data cột trái
                     */
                    if ($.isArray(res)) {
                        left = res;
                    }

                    /**
                     * Load đủ data --> Render List
                     */
                    if (left && right) {
                        renderDualList(left, right);
                    }
                })
                .fail((error) => {
                    drawerInstance.error(error);
                });

            $.ajax({
                url: `/group/customers/${id}`,
                method: 'GET',
            })
                .done((res) => {
                    /**
                     * Set data cột phải
                     */
                    if ($.isArray(res)) {
                        right = res;
                    }

                    /**
                     * Load đủ data --> Render List
                     */
                    if (left && right) {
                        renderDualList(left, right);
                    }
                })
                .fail((error) => {
                    drawerInstance.error(error);
                })
        });

        drawerInstance.open();
    };

    //change avatar
    var avata = function () {
        $('input[type=file]').on('change', function (event) {
            var files = event.target.files;
            event.stopPropagation(); // Stop stuff happening
            event.preventDefault(); // Totally stop stuff happening
            $("#avatar-status").text("Loading new avatar...");
            $("#avatar").css("opacity", "0.4");
            $("#avatar").css("filter", "alpha(opacity=40);");
            //Create a formdata object and add the files
            var data = new FormData();
            $.each(files, function (key, value) {
                data.append(key, value);
            });
            $.ajax({
                url: "/images/UploadAva",
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function (data, textStatus, jqXHR) {
                    if (data != null && data.length > 0) {
                        $("#image").val(data[0].id);
                        //$("#imgAvatar").attr('src', "https://api.ngot.club/" + response[0].path);
                    }
                    else {
                        //Handle errors here
                        alert('ERRORS: ' + textStatus);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //Handle errors here
                    alert('ERRORS: ' + textStatus);

                }
            });
        });
    }

    var avatar = function () {
        image = new KTAvatar('kt_user_avatar');
    }
    var location = function () {
        $('#kt-select-location')

            .on('change', function () {

                $('#locationtown1').val('');
                $('#locationvillage1').val('');
                var provinceid = $(this).val();
                $.ajax({
                    url: `/customer/getlocationtown/${provinceid}`,
                    method: 'GET',
                })
                    .done(function (data) {

                        console.log(data);
                        var town = data.data.items;

                        var $select = $('#locationtown1');

                        $('#locationvillage1').attr('disabled', true);
                        $('#locationvillage1').selectpicker('refresh')
                        $select.find('option').remove();
                        $select.append(`<option value=''>---Quận/Huyện/Thị Xã---</option>)`);
                        $.each(town, function (index, item) {

                            console.log(item);
                            $select.append('<option value=' + item.id + '>' + item.name + '</option>');
                            $select.attr('disabled', false)
                            $select.selectpicker('refresh');
                        })


                    })
                    .fail(function () {

                    });
            });
        $('#locationtown1')
            .on('change', function () {
                $('#locationvillage1').val('');
                var town = $(this).val();
                $.ajax({
                    url: `/customer/getlocationvillages/${town}`,
                    method: 'GET',
                })
                    .done(function (data) {

                        console.log(data);
                        var village = data.data.items;

                        var $select = $('#locationvillage1');
                        $select.find('option').remove();
                        $select.append(`<option value=''>---Phường/Xã/Thị Trấn---</option>)`);
                        $.each(village, function (index, item) {

                            console.log(item);
                            $select.append('<option value=' + item.id + '>' + item.name + '</option>');
                            $select.attr('disabled', false)
                            $select.selectpicker('refresh');
                        })


                    })
                    .fail(function () {

                    });
            });
    }
    //var initDropzone = function () {
    //    new Dropzone('#kt_dropzone_image', {
    //        url: "/images/upload",
    //        paramName: "images",
    //        maxFiles: 1,
    //        maxFilesize: 1, // MB
    //        addRemoveLinks: true,
    //        dictDefaultMessage: 'Bấm hoặc kéo thả ảnh vào đây để tải ảnh lên',
    //        dictRemoveFile: 'Xóa ảnh',
    //        dictCancelUpload: 'Hủy',
    //        dictCancelUploadConfirmation: 'Bạn thực sự muốn hủy?',
    //        dictMaxFilesExceeded: 'Bạn không thể tải thêm ảnh!',
    //        dictFileTooBig: "Ảnh tải lên dung lượng quá lớn ({{filesize}}MB). Max filesize: {{maxFilesize}}MB.",
    //        accept: function (file, done) {
    //            done();
    //        },
    //        success: function (file, response) {
    //            if (response != null && response.length > 0) {
    //                $("#image").val(response[0].id);
    //            }
    //            else {
    //                $(file.previewElement).addClass("dz-error").find('.dz-error-message').text("Không tải được ảnh lên.");
    //            }
    //        },
    //        error: function (file, response, errorMessage) {
    //            console.log(file, response);
    //            if (file.accepted) {
    //                $(file.previewElement).addClass("dz-error").find('.dz-error-message').text("Có lỗi xảy ra, không tải được ảnh lên.");
    //            }
    //            else {
    //                $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(response);
    //            }
    //        }
    //    });
    //}

    //var initUserForm = function () {
    //    avatar = new KTAvatar('kt_user_avatar');
    //}
    var location = function () {
        $('#js-filter-location')
            .on('change', function () {


                if (dataTableInstance) {
                    dataTableInstance.search($(this).val(), 'provinceId');
                    START_PAGE = 1;
                }
                $('#locationtown').val('');
                $('#locationvillage').val('');
                var provinceid = $(this).val();
                $.ajax({
                    url: `/project/getlocationtown/${provinceid}`,
                    method: 'GET',
                })
                    .done(function (data) {

                        console.log(data);
                        var town = data.data.items;

                        var $select = $('#locationtown');

                        $('#locationvillage').attr('disabled', true);
                        $('#locationvillage').selectpicker('refresh')
                        $select.find('option').remove();
                        $select.append(`<option value=''>---Quận/Huyện/Thị Xã---</option>)`);
                        $.each(town, function (index, item) {

                            console.log(item);
                            $select.append('<option value=' + item.id + '>' + item.name + '</option>');
                            $select.attr('disabled', false)
                            $select.selectpicker('refresh');
                        })


                    })
                    .fail(function () {

                    });
            });
        $('#locationtown')
            .on('change', function () {

                if (dataTableInstance) {
                    dataTableInstance.search($(this).val(), 'districtId');
                    START_PAGE = 1;
                }
                $('#locationvillage').val('');
                var town = $(this).val();
                $.ajax({
                    url: `/project/getlocationvillages/${town}`,
                    method: 'GET',
                })
                    .done(function (data) {

                        console.log(data);
                        var village = data.data.items;

                        var $select = $('#locationvillage');

                        //for (var i = 0; i < customer.length; i++) {
                        //    $select.append('<option value=' + customer[i].project_customer_uuid + '>' + customer[i].name + '</option>');
                        //    $select.selectpicker('refresh');
                        //}
                        $select.find('option').remove();
                        $select.append(`<option value=''>---Phường/Xã/Thị Trấn---</option>)`);
                        $.each(village, function (index, item) {

                            console.log(item);
                            $select.append('<option value=' + item.id + '>' + item.name + '</option>');
                            $select.attr('disabled', false)
                            $select.selectpicker('refresh');
                        })


                    })
                    .fail(function () {

                    });
            });
    };

    //Tab device trong chỉnh sửa thông tin đơn vị
    $(".device-in_customer").on('click', function () {
        debugger;

        const customer_uuid = $('#uuid').val();
        if (dataTableDevice) {
            dataTableDevice.load()
        }
        else {
            dataTableDevice = $('.kt-datatable--deviceincustomer').KTDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: `/device/getDeviceInCustomers/${customer_uuid}`,
                            method: 'GET',
                        },
                        response: {
                            map: function (res) {
                                debugger
                                const raw = res && res.data || {};
                                const { pagination } = raw;
                                const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                                const page = pagination === undefined ? undefined : pagination.page;
                                CURRENT_PAGE = page || 1;
                                LIMIT = perpage;
                                START_PAGE = 0;
                                return {
                                    data: raw.items || [],
                                    meta: {
                                        ...pagination || {},
                                        perpage: perpage || LIMIT,
                                        page: page || 1,
                                    },
                                }
                            },
                        },
                        filter: function (data = {}) {
                            const query = data.query || {};
                            const pagination = data.pagination || {};
                            const { keyword } = query;
                            if (START_PAGE === 1) {
                                pagination.page = START_PAGE;
                            }
                            return {
                                limit: pagination.perpage || LIMIT,
                                keyword: (keyword || $('#js-filter-keyword_device').val()),
                                page: pagination.page

                            };
                        }
                    },
                    pageSize: LIMIT,
                    serverPaging: true,
                    serverFiltering: true,
                },

                search: {
                    onEnter: true,
                },
                columns: [
                    {
                        field: 'uuid',
                        title: '#',
                        width: 30,
                        textAlign: 'center',
                        template: function (data, row) {
                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                        },
                        responsive: {
                            visible: 'md',
                            hidden: 'sm'
                        }
                    },
                    {
                        field: 'name_display',
                        title: 'Tên thiết bị sử dụng',
                        template: function (data) {
                            var output = '';
                            if (data.name != null) {
                                output += `<span class="text-primary">${data.name}</span>
                                        <br /><span>${data.prefix_name}</span>`;
                            }
                            return output;
                        }
                    },
                    {
                        field: 'pkg_name',
                        title: 'Gói thiết bị',
                        width: 100,
                    },
                    {
                        field: 'count_defined_devices',
                        title: 'Thiết bị xác định',
                        width: 100,
                        template: function (data) {
                            var output = `<a class="text-primary js-deviceincustomer" data-customer="${customer_uuid}" data-category="${data.category_id}" data-package="${data.package_uuid}" title="Danh sách thiết bị" style="cursor:pointer">
                                        ${data.count_defined_devices} thiết bị
                                    </a>`;
                            return output;
                        }
                    },
                    {
                        field: '#1',
                        title: 'Thiết bị chưa xác định',
                        width: 100,
                        template: function (data) {
                            var output = `<span>${data.total_devices - data.count_defined_devices} thiết bị</span>`;
                            return output;
                        }
                    }],
                sortable: false,
                pagination: true,
                responsive: true,
            });
        }
        
    });

    return {
        // Public functions
        init: function () {
            //initUserForm();
            initTable();
            avata();
            avatar();
            initSearch();
            initValidation();
            initEventListeners();
            location();
            //if (window.location.pathname == '/customer/create') {
            //    initDropzone();
            //}
            $('.locationupdate').selectpicker();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-customer').length) {
        DMMSCustomer.init();
    }
});
