"use strict";
// Class definition

var DMMSSupplier = function () {
    // Private functions

    var dataTableInstance = null;

    var drawer = null;
    var image = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    // demo initializer
    var initTable = function () {
        var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
        if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
            $.notify(editSucessCache.massage, { type: 'success' });
            localStorage.removeItem('edit_success');
        }
        dataTableInstance = $('.kt-datatable').KTDatatable({

            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/supplier/gets',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page,
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            //Gán các thuộc tính của images{0} vào đối tượng item
                            if (raw.items != null) {
                                (raw.items).forEach(function (e) {

                                    e.path = e.image != null ? e.image.path : null;
                                   
                                    return e;


                                })
                            }
                           

                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            page: pagination.page,                          
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter : true,
            },

            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 20,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'name',
                    title: 'Tên nhà cung cấp',
                    autoHide: false,
                    
                }, {
                    field: 'description',
                    title: 'Mô tả',
                    autoHide: false
                },
                {
                    field: 'path',
                    title: 'Ảnh',
                    textAlign: 'center',
                    width: 110,
                    template: function (data) {
                        if (data.path != undefined && data.path !== '') {
                            return `
                            <img src="${window.imageDomain + data.path}" width="100px">
                        `;
                        }
                        return ` <img src="/statics/images/noimage.jpg" width="100px">`;
                    },
                }, {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 110,
                    overflow: 'visible',
                    
                    textAlign: 'center',
                    template: function (data) {
                        return `
                           <a href="/supplier/edit/${data.uuid}" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </a>
                            <button data-id="${data.uuid}" title="Xóa" class="js-delete-supplier btn btn-sm btn-danger btn-icon btn-icon-md ml-2">
                                <i class="la la-trash"></i>
                            </button>
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };
    var initSearch = function () {        
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            })
       

    };
    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var deleteSupplier = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa nhà cung cấp...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/supplier/delete/${id}`,
                method: 'DELETE',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa nhà cung cấp thành công!', { type: 'success' });

                    reloadTable();
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa nhà cung cấp không thành công!', { type: 'danger' });
                });
        }
    };
    $(".linksupp").on("click", function () {
        debugger
        var supplierUuid = document.getElementById("uuid").value;

        dataTableInstance = $('.kt-datatablesupp').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {

                        url: `/supplier/GetDevice/${supplierUuid}`,
                    },

                    response: {

                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? undefined : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};


                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,

                            //keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,

            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 30,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                //{
                //    field: 'code',
                //    title: 'Mã công việc',
                //    width: 80
                //},
                {
                    field: 'code',
                    title: 'Mã thiết bị',
                    width:120
                },
                //{
                //    field: 'ticket.title',
                //    title: 'Tiêu đề yêu cầu',
                //}, 
                {
                    field: 'name',
                    title: 'Tên thiết bị',
                    width: 200,
                    template: function (data) {
                        var output = '<strong>' + data.name + '</strong>';
                        if (data.name) {
                            output += '<br/>' + data.prefixName + '</span>';
                        }
                        return output;
                    }
                },
                {
                    field: 'serial',
                    title: 'Serial',
                    width:110
                },

                {
                    field: 'expiredDevice_nohh',
                    title: 'Thời gian TB',
                    textAlign: 'center',
                    width : 130,
                },
                {
                    field: 'state',
                    title: 'Trạng thái',
                    //width:150,
                    textAlign: 'center',
                    template: function (data) {
                        var states = {
                            1: { 'title': 'Đang sử dụng', 'class': 'kt-badge--brand' },
                            2: { 'title': 'Đang yêu cầu sửa chữa', 'class': ' kt-badge--warning' },
                            3: { 'title': 'Đến lịch bảo hành', 'class': ' kt-badge--primary' },
                            4: { 'title': 'Đang xử lý theo yêu cầu', 'class': 'kt-badge--success' },
                            5: { 'title': 'Cần thay thế', 'class': 'kt-badge--danger' },
                            6: { 'title': 'Mang đi sửa chữa - Không có thiết bị thay thế', 'class': 'kt-badge--info' },
                            7: { 'title': 'Mang đi sửa chữa - Có thiết bị thay thế', 'class': 'kt-badge--info' },
                            8: { 'title': 'Thay thế', 'class': 'kt-badge--info' },
                            9: { 'title': 'Hỏng', 'class': 'kt-badge--info' },
                            10: { 'title': 'Trong kho-Trong kho vật lý,ba lô', 'class': 'kt-badge--info' },
                            11: { 'title': 'Trong kho-Hỏng sau khi kiểm kê', 'class': 'kt-badge--info' },
                            12: { 'title': 'Đang vận chuyển', 'class': 'kt-badge--info' },
                            13: { 'title': 'Đã xuất thanh lý', 'class': 'kt-badge--info' },
                            14: { 'title': 'Đã xuất tiêu hao', 'class': 'kt-badge--info' },
                            15: { 'title': 'Đã hoàn trả nhà cung cấp', 'class': 'kt-badge--info' },
                            16: { 'title': 'Thêm mới/cập nhật thiết bị', 'class': 'kt-badge--info' },
                            17: { 'title': 'Đã sửa xong thiết bị', 'class': 'kt-badge--info' },
                            
                        };
                        if (data.state) {
                            return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill">' + states[data.state].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge ' + states[1].class + ' kt-badge--inline kt-badge--pill">' + states[1].title + '</span>';
                        }
                    }
                },

                //{
                //    field: 'created_at',
                //    title: 'Ngày tạo',
                //},
            ],
            layout: {
                scroll: !0,
                height: null,
                footer: !1
            },
            sortable: false,
            pagination: true,
            responsive: true,
        });
    });
    var initSupplierDrawer = function (id) {
        const title = id ? 'Sửa thông tin nhà cung cấp' : 'Thêm nhà cung cấp';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--supplier' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/supplier/edit/${id}` : '/supplier/create',
                method: 'GET',
            })
            .done(function (response) {
                const $content = drawer.setBody(response);
                initContent($content);

                jQuery.fn.preventDoubleSubmission = function () {
                    $(this).on('submit', function (e) {
                        var $form = $(this);

                        if ($form.data('submitted') === true) {
                            // Previously submitted - don't submit again
                            e.preventDefault();
                        } else {
                            // Mark it so that the next submit can be ignored
                            $form.data('submitted', true);
                        }
                    });
                    
                    // Keep chainability
                    return this;
                };
                $('form.dmms-supplier__form').preventDoubleSubmission();
            })
            .fail(function () {
                drawer.error();
            });
        });

        drawer.open();
    };

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-supplier')
            .on('click', '.js-delete-supplier', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Nhà cung cấp sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteSupplier(id);
                    }
                });
            })
        
            .off('click', '.js-edit-supplier')
            .on('click', '.js-edit-supplier', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initSupplierDrawer(id || '');
            });
    };
    var initForm = function ($form) {
        initValidation(true);
    }
    var initValidation = function (isForm) {
        $(".dmms-supplier__form").validate({
            rules: {
                name: {
                    required: true,
                },
                description: {
                    required: true,
                },

            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên nhà cung cấp.'
                },

                description: {
                    required: 'Vui lòng nhập mô tả.'
                }
            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {               
                KTUtil.scrollTo('.dmms-supplier__form', -200);
            },
            submitHandler: function (form) {
                $(form).find('button[type="submit"]').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

                const data = $(form).serializeJSON() || {};
                const { uuid } = data;               
                console.log(data, uuid);
                $.ajax({
                    url: uuid ? `/supplier/edit` : 'supplier/create',
                    method: uuid ? 'PUT' : 'POST',
                    data,
                })
                    .done(function () {
                    $(".submit").attr("disabled", true);
                    KTApp.block('.dmms-supplier__form');                    
                    const successMsg = uuid ? 'Sửa thông tin thành công!' : 'Thêm nhà cung cấp thành công!';
                    $.notify(successMsg, { type: 'success' });
                    if (isForm) {
                        drawer.close();
                        reloadTable();
                    } else {
                   
                        localStorage['edit_success'] = JSON.stringify({ from: (uuid ? `/supplier/edit/` : '/supplier/create'), massage: successMsg });
                        window.location = window.location.origin + "/supplier";
                    }
                })
                    .fail(function () {
                    $(form).find('button[type="submit"]').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    KTApp.unblock('.dmms-supplier__form');
                    const errorMsg = uuid ? 'Sửa thông tin không thành công!' : 'Thêm nhà cung cấp không thành công!';
                    $.notify(errorMsg, { type: 'danger' });
                });
                return false;
            },
        });
    }

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-supplier__form');
        initForm($form);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));


        /**
         * Init Dropzone
         */
        initDropzone();
    };

    var initDropzone = function () {
        new Dropzone('#kt_dropzone_image', {
            url: "/images/upload",
            paramName: "images",
            maxFiles: 1,
            maxFilesize: 1, // MB
            addRemoveLinks: true,
            dictDefaultMessage: 'Bấm hoặc kéo thả ảnh vào đây để tải ảnh lên',
            dictRemoveFile: 'Xóa ảnh',
            dictCancelUpload: 'Hủy',
            dictCancelUploadConfirmation: 'Bạn thực sự muốn hủy?',
            dictMaxFilesExceeded: 'Bạn không thể tải thêm ảnh!',
            dictFileTooBig: "Ảnh tải lên dung lượng quá lớn ({{filesize}}MB). Max filesize: {{maxFilesize}}MB.",
            accept: function (file, done) {
                done();
            },
            success: function (file, response) {
                if (response != null && response.length > 0) {
                    $("#image").val(response[0].id);
                }
                else {
                    $(file.previewElement).addClass("dz-error").find('.dz-error-message').text("Không tải được ảnh lên.");
                }
            },
            error: function (file, response, errorMessage) {
                console.log(file, response);
                if (file.accepted) {
                    $(file.previewElement).addClass("dz-error").find('.dz-error-message').text("Có lỗi xảy ra, không tải được ảnh lên.");
                }
                else {
                    $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(response);
                }
            }
        });
    }
    var avatar = function () {
        image = new KTAvatar('kt_user_avatar');
    }
    var avata = function () {
        $('input[type=file]').on('change', function (event) {
            var files = event.target.files;
            event.stopPropagation(); // Stop stuff happening
            event.preventDefault(); // Totally stop stuff happening
          
            //Create a formdata object and add the files
            var data = new FormData();
            $.each(files, function (key, value) {
                data.append(key, value);
            });
            $.ajax({
                url: "/images/upload",
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function (data, textStatus, jqXHR) {
                    if (data != null && data.length > 0) {
                        $("#image").val(data[0].id);
                        //$("#imgAvatar").attr('src', "https://api.ngot.club/" + response[0].path);
                    }
                    if (typeof data.error === 'undefined') {
                        //Success so call function to process the form
                        //submitForm(event, data);
                       
                    } else {
                        //Handle errors here
                        alert('ERRORS: ' + textStatus);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //Handle errors here
                    alert('ERRORS: ' + textStatus);

                }
            });
        });
    }

    return {
        // Public functions
        init: function () {
            avatar();
            avata();
            initTable();
            initSearch();
            initEventListeners();
            initValidation();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-supplier').length) {
        DMMSSupplier.init();
    }
});
