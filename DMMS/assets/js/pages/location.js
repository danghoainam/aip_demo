﻿"use strict";
// Class definition

var DMMSLocation = function () {
    // Private functions
    var tableLocation = null;
    var drawer = null;
    var index = 0;

    var columns = [
        {
            "title": "<span class='text-white'>#</span>",
            "target": 0,
            "data": function (item) {
                if (item.parent_id === null || item.parent_id === 0) {
                    return ++index;
                }
                return '';
            },
            "width": "35px"
        },
        {
            "title": "",
            "target": 1,
            "className": 'treegrid-control text-center"',
            "data": function (item) {
                if (item.children && item.children.length > 0) {
                    return `<span><i class="flaticon2-plus"></i></span>`;
                }
                return '';
            },
            "width": "40px"
        },
        {
            "title": "<span class='text-white'>Tên đơn vị hành chính</span>",
            "target": 2,
            "data": function (item) {
                return item.name;
            }
        },
        {
            "title": "<span class='text-white'>Mô tả</span>",
            "target": 3,
            "data": function (item) {
                return item.description;
            }
        },
        {
            title: "<span class='text-white'>Tác vụ</span>",
            target: 4,
            width: "110px",
            data: function (item) {
                return `
                            <button data-id="${item.id}" title="Chỉnh sửa" class="js-edit-location btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </button>
                            <button data-id="${item.id}" title="Xóa" class="js-delete-location btn btn-sm btn-danger btn-icon btn-icon-md">
                                <i class="la la-trash"></i>
                            </button>
                        `;
            }
        },
    ];

    var initLocationDrawer = function (id) {
        const title = id ? 'Sửa thông tin đơn vị hành chính' : 'Thêm đơn vị hành chính';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--location' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/location/edit/${id}` : '/location/create',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    var deleteLocation = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa đơn vị hành chính...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/location/delete/${id}`,
                method: 'DELETE',
            })
                .done(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(data.msg, { type: 'success' });

                    reloadTable();
                })
                .fail(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(data.msg, { type: 'danger' });
                });
        }
    }

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-location')
            .on('click', '.js-delete-location', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Loại thiết bị sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteLocation(id);
                    }
                });
            })
            .off('click', '.js-edit-location')
            .on('click', '.js-edit-location', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initLocationDrawer(id || '');
            });

    };

    // demo initializer
    var initTable = function () {
        tableLocation = $('#location-table').DataTable({
            columns: columns,
            ajax: {
                url: "/location/gets",
                type: "GET",
                datatype: "json",
                error: function (err) {
                    const errorMsg = 'Cố lỗi xảy ra nên không thực hiện được thao tác!'
                    $.notify(errorMsg, { type: 'danger' });
                }
            },

            treeGrid: {
                left: 20,
                expandIcon: '<span><i class="flaticon2-plus"></i></span>',
                collapseIcon: '<span><i class="flaticon2-line"></i></span>'
            },
            paging: false,
            ordering: false,
            info: false,
            searching: true,
            columnDefs: [
                {
                    "targets": 0,
                    "className": "text-center",
                },
                {
                    "targets": 4,
                    "className": "text-center",
                },

            ],
            language: {
                loadingRecords: "Đang tải dữ liệu..."
            }
        });

        $("#js-filter-keyword")
            .on('keyup click', function () {
                tableLocation.search($(this).val()).draw();
            });
    };

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-location__form');
        initForm($form);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

        //$('#kt-select-devicelocation').selectpicker();
        if ($("#parent_id").val() !== '') {
            $('#kt-select-location').selectpicker('val', $("#parent_id").val());
        }
        else {
            $('#kt-select-location').selectpicker();
        }
    };

    var initForm = function ($form) {
        $form.validate({
            rules: {
                name: {
                    required: true,
                },
                description: {
                    required: true,
                },
            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên loại thiết bị.'
                },
                description: {
                    required: 'Vui lòng nhập mô tả.'
                }
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {

                const data = $(form).serializeJSON() || {};
                const { id } = data;
                $.ajax({
                    url: id ? `/location/edit/${id}` : 'location/create',
                    method: id ? 'PUT' : 'POST',
                    data,
                })
                    .done(function () {
                        KTApp.block($form);
                        const successMsg = id ? 'Sửa thông tin thành công!' : 'Thêm loại thiết bị thành công!';
                        $.notify(successMsg, { type: 'success' });

                        /**
                         * Close drawer
                         */
                        drawer.close();

                        reloadTable();
                    })
                    .fail(function () {
                        KTApp.unblock($form);
                        const errorMsg = id ? 'Sửa thông tin không thành công!' : 'Thêm loại thiết bị không thành công!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }

    var reloadTable = function () {
        if (tableLocation) {
            tableLocation.ajax.reload();
            index = 0;
        }
    };

    return {
        // Public functions
        init: function () {
            initTable();
            initEventListeners();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-location').length) {
        DMMSLocation.init();
    }
});
