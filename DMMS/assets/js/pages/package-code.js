﻿"use strict";
// Class definition

var DMMSPackage = function () {
    // Private functions

    var dataTableInstance = null;
    var drawer = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    var MAX_ITEM_INPAGE = 0;

    var initTable = function () {
        dataTableInstance = $('.kt-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: `/package/gets`,
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? undefined : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page
                            CURRENT_PAGE = page || 1;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword } = query;
                        MAX_ITEM_INPAGE = pagination.perpage || LIMIT;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: MAX_ITEM_INPAGE,
                            keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,

            },
            columns: [
                {
                    field: 'id',
                    title: '#',
                    width: 30,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * MAX_ITEM_INPAGE;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'code',
                    title: 'Mã phòng',
                    width: 80,
                },
                {
                    field: 'name',
                    title: 'Tên phòng',
                },
                {
                    field: 'description',
                    title: 'Mô tả',
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 80,
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        return `
                            <a href="/package/edit/${data.id}" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md js-edit">
                                <i class="la la-edit"></i>
                            </a>
                            <buttom data-id="${data.id}" title="Xóa" class="btn btn-sm btn-danger btn-icon btn-icon-md js-package-delete">
                                <i class="la la-trash"></i>
                            </buttom>
                        `;
                    },
                }],
            layout: {
                scroll: !0,
                height: null,
                footer: !1
            },
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };

    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var initSearch = function () {
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });
    };

    var deletePackage = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa phòng...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/package/delete/${id}`,
                method: 'DELETE',
            })
                .done(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa thành công!', { type: 'success' });

                    if (dataTableInstance) {
                        reloadTable();
                    }
                    else {
                        window.location.pathname = '/package';
                    }
                })
                .fail(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa không thành công!', { type: 'danger' });
                });
        }
    };

    var initEditDrawer = function (id) {
        const title = id ? 'Sửa thông tin phòng thiết bị' : 'Thêm mới phòng thiết bị';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--package' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/package/edit/${id}` : 'package/create',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);

                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-package-delete')
            .on('click', '.js-package-delete', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Phòng sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deletePackage(id);
                    }
                });
            })
            .off('click', '.js-edit-package')
            .on('click', '.js-edit-package', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initEditDrawer(id || '');
            });
    };

    var initForm = function ($form) {
        $form.validate({
            rules: {
                name: {
                    required: true,
                },
            },
            messages: {
                name: {
                    required: "Vui lòng nhập tên phòng thiết bị."
                },
            },

            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                const { id } = data;
                $.ajax({
                    url: id ? `/package/edit/${id}` : 'package/create',
                    method: id ? 'PUT' : 'POST',
                    data,
                })
                    .done(function (data) {
                        KTApp.block($form);
                        const successMsg = id ? 'Sửa thông tin thành công!' : 'Thêm thông tin thành công!';
                        $.notify(successMsg, { type: 'success' });

                        /**
                         * Close drawer
                         */
                        drawer.close();

                        reloadTable();
                    })
                    .fail(function (data) {
                        KTApp.unblock($form);
                        const successMsg = id ? 'Sửa thông tin không thành công!' : 'Thêm thông không tin thành công!';
                        $.notify(successMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    };

    var initValidation = function () {
        $(".dmms-package__form").validate({
            rules: {
                name: {
                    required: true,
                },
            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên.'
                },
            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('.dmms-package__form', -200);
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                const { id } = data;
                $.ajax({
                    url: `/package/edit/${id}`,
                    method: 'PUT',
                    data,
                })
                    .done(function (data) {
                        KTApp.block('.dmms-package__form');
                        $.notify('Sửa thông tin thành công!', { type: 'success' });
                        reloadTable();

                        window.location = window.location.origin + `/package`;

                    })
                    .fail(function (data) {
                        $.notify('Sửa thông tin không thành công!', { type: 'danger' });
                    });
                return false;
            },

        });
    }

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-package__form');
        initForm($form);

        /*
            Init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));
    }

    return {
        // Public functions
        init: function () {
            initTable();
            initSearch();
            initEventListeners();
            initValidation();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-package').length) {
        DMMSPackage.init();
    }
});