﻿"use strict";
// Class definition
var datacus = [];
var uuid_team = "";
var DMMSArea = function () {
    var _this = {};
    var team = {
        $container: $('#js-teams'),
        //$title: $('#js-title-2'),

        load: function () {
            KTApp.block(this.$container);
            $.ajax({
                url: '/teams-staff',
                method: 'GET',
            }).done((res) => {
                KTApp.unblock(this.$container);
                if (res && res.data) {
                    this.render(res.data || {});
                }
            }).fail(() => {
                KTApp.unblock(this.$container);
                $.notify('Không kết nối được đến máy chủ', { type: 'danger' });
            });
        },

        render: function (data) {
            var html = [];
            var firstTeamUUID = null;
            $.each(data.items || [], function (index, item) {
                if (index === 0) {
                    firstTeamUUID = item.uuid;
                }
                html.push('<div class="kt-group">');
                html.push(`
                    <div data-name="${item.name}" data-uuid="${item.uuid}" class="kt-group__title d-flex align-items-center js-team">
                        <i class="flaticon2-group mr-2"></i>
                        ${item.name}
                    </div>
                `);
                html.push('<div class="kt-group__list">');
                $.each(item.staff || [], function (index, staff) {
                    html.push(`
                        <div data-name="${staff.name}" data-uuid="${staff.uuid}" class="kt-group__item d-flex align-items-center js-staff">
                            <i class="flaticon-users mr-2"></i>
                            ${staff.name}
                        </div>
                    `);
                });
                html.push('</div>');
                html.push('</div>');
            });

            this.$container.html(html.join(''));

            this.select(firstTeamUUID);
        },

        select: function (uuid) {
            const $team = $(`.js-team[data-uuid="${uuid}"]`);

            const $parent = $team.parent('.kt-group');
            $parent.find('.active').removeClass('active');
            const $active = $parent.siblings('.active');
            $active.removeClass('active').find('.active').removeClass('active');

            const name = $team.attr('data-name');
            //this.$title.html(name);

            $parent.addClass('active');
            $team.addClass('active');

            _this.team_uuid = uuid;
            _this.staff_uuid = null;


            managedCustomer.loadByTeam(uuid);
        },

        selectStaff: function (uuid) {
            const $team = $(`.js-staff[data-uuid="${uuid}"]`);

            const $active = $team.siblings('.active');
            $active.removeClass('active');

            const name = $team.attr('data-name');
            //this.$title.html(name);

            $team.addClass('active');

            _this.team_uuid = null;
            _this.staff_uuid = uuid;


            managedCustomer.loadByStaff(uuid);
        },
    };

    var managedCustomer = {

        $container: $('#js-area-2'),

        reload: function () {
            if (_this.team_uuid) {
                this.loadByTeam(_this.team_uuid);
            }
            if (_this.staff_uuid) {
                this.loadByStaff(_this.staff_uuid);
            }
        },

        loadByTeam: function (uuid) {
            this.$container.attr('data-uuid', uuid);
            KTApp.block(this.$container);
            $.ajax({
                url: "/customers-managed-by-team",
                data: {
                    team_uuid: uuid,
                    keyword: $("#js-filter-team-staff").val()
                },
                method: 'GET',
            }).done((res) => {
                KTApp.unblock(this.$container);
                if (res && res.data) {
                    this.render(res.data || {}, uuid);
                }
            }).fail(() => {
                KTApp.unblock(this.$container);
                $.notify('Không kết nối được đến máy chủ', { type: 'danger' });
            });
        },

        render: function (data, uuid) {
            datacus = data.teams;
            uuid_team = uuid;
            var html = [];
            if (this.$container.attr('data-uuid') !== uuid) {
                return;
            }
            debugger
            if (data.teams.length != 0) {
                html.push('<div class="kt-group kt-group--customer">');
                html.push(`
                    <div class="kt-group__title d-flex align-items-center">
                        ${data.teams[0].team.name}
                    </div>
                `);
            }
         
            $.each(data.teams || [], function (index, item) {
             
                html.push('<div class="kt-group kt-group--customer">');
               
                html.push('<div class="kt-group__list">');
                html.push(`
                    <div data-uuid="${item.uuid}" class="kt-group__item d-flex align-items-center js-managed-customer">
                        ${item.name}
                        <button data-uuid="${item.uuid}" class="kt-group__btn text-danger js-remove-customer ml-auto">
                            <i class="flaticon2-fast-next"></i>
                        </button>
                    </div>
                `);
                html.push('</div>');
                html.push('</div>');
                html.push('</div>');
            });
            $.each(data.staff || [], function (index, staff) {
                html.push('<div class="kt-group kt-group--customer">');
                html.push(`
                    <div class="kt-group__title d-flex align-items-center">
                        ${staff.employee.name}
                    </div>
                `);
                $.each(staff.customers, function (i, v) {
                    html.push('<div class="kt-group kt-group--customer">');
                    html.push('<div class="kt-group__list">');
                    html.push(`
                    <div data-uuid="${v.uuid}" class="kt-group__item d-flex align-items-center js-managed-customer">
                        ${v.name}
                        <button data-uuid="${v.uuid}" class="kt-group__btn text-danger js-remove-customer ml-auto">
                            <i class="flaticon2-fast-next"></i>
                        </button>
                    </div>
                `);
                    html.push('</div>');
                    html.push('</div>');
                })

            });
            $.each(data.items || [], function (index, item) {
                html.push('<div class="kt-group kt-group--customer">');
                html.push('<div class="kt-group__list">');
                html.push(`
                    <div data-uuid="${item.uuid}" class="kt-group__item d-flex align-items-center js-managed-customer">
                        ${item.name}
                        <button data-uuid="${item.uuid}" class="kt-group__btn text-danger js-remove-customer ml-auto">
                            <i class="flaticon2-fast-next"></i>
                        </button>
                    </div>
                `);
                html.push('</div>');
                html.push('</div>');
            });

            this.$container.html(html.join(''));

        },
        render1: function (data, uuid) {
            datacus = data.teams;
            debugger
            var html = [];
            if (this.$container.attr('data-uuid') !== uuid) {
                return;
            }
            //$.each(data.teams || [], function (index, item) {
            //    html.push('<div class="kt-group kt-group--customer">');
            //    html.push('<div class="kt-group__list">');
            //    html.push(`
            //        <div data-uuid="${item.uuid}" class="kt-group__item d-flex align-items-center js-managed-customer">
            //            ${item.name}
            //            <button data-uuid="${item.uuid}" class="kt-group__btn text-danger js-remove-customer ml-auto">
            //                <i class="flaticon2-fast-next"></i>
            //            </button>
            //        </div>
            //    `);
            //    html.push('</div>');
            //    html.push('</div>');
            //});
            $.each(data.staff || [], function (index, staff) {
                html.push('<div class="kt-group kt-group--customer">');
                html.push(`
                    <div class="kt-group__title d-flex align-items-center">
                        ${staff.employee.name}
                    </div>
                `);
                $.each(staff.customers, function (i, v) {
                    html.push('<div class="kt-group kt-group--customer">');
                    html.push('<div class="kt-group__list">');
                    html.push(`
                    <div data-uuid="${v.uuid}" class="kt-group__item d-flex align-items-center js-managed-customer">
                        ${v.name}
                        <button data-uuid="${v.uuid}" class="kt-group__btn text-danger js-remove-customer1 ml-auto">
                            <i class="flaticon2-fast-next"></i>
                        </button>
                    </div>
                `);
                    html.push('</div>');
                    html.push('</div>');
                })

            });
            $.each(data.items || [], function (index, item) {
                html.push('<div class="kt-group kt-group--customer">');
                html.push('<div class="kt-group__list">');
                html.push(`
                    <div data-uuid="${item.uuid}" class="kt-group__item d-flex align-items-center js-managed-customer">
                        ${item.name}
                        <button data-uuid="${item.uuid}" class="kt-group__btn text-danger js-remove-customer1 ml-auto">
                            <i class="flaticon2-fast-next"></i>
                        </button>
                    </div>
                `);
                html.push('</div>');
                html.push('</div>');
            });

            this.$container.html(html.join(''));

        },
        loadByStaff: function (uuid) {
            this.$container.attr('data-uuid', uuid);
            KTApp.block(this.$container);
            $.ajax({
                url: "/customers-managed-by-staff",
                data: {
                    staff_uuid: uuid,
                    keyword: $("#js-filter-team-staff").val() /*|| $("#js-filter-customer").val()*/
                },
                method: 'GET',
            }).done((res) => {
                KTApp.unblock(this.$container);
                if (res && res.data) {
                    this.render1(res.data || {}, uuid);
                    customer.load1(res.data.teams);
                }
            }).fail(() => {
                KTApp.unblock(this.$container);
                $.notify('Không kết nối được đến máy chủ', { type: 'danger' });
            });
        },

        add: function (params) {
            KTApp.block(this.$container);
            KTApp.block(customer.$container);
            $.ajax({
                url: '/customers/add-customer',
                method: 'POST',
                data: params || {},
            }).done((res) => {
                KTApp.unblock(this.$container);
                if (res && res.code === 0) {
                    managedCustomer.reload();
                    customer.load();
                } else {
                    KTApp.unblock(customer.$container);
                    KTApp.unblock(this.$container);
                }
            }).fail(() => {
                KTApp.unblock(this.$container);
                KTApp.unblock(customer.$container);
                $.notify('Thêm khách hàng không thành công!', { type: 'danger' });
            });
        },
        add1: function (params) {
            KTApp.block(this.$container);
            KTApp.block(customer.$container);
            $.ajax({
                url: '/customers/add-customer',
                method: 'POST',
                data: params || {},
            }).done((res) => {
                KTApp.unblock(this.$container);
                if (res && res.code === 0) {
                    managedCustomer.reload();
                    customer.load1();
                } else {
                    KTApp.unblock(customer.$container);
                    KTApp.unblock(this.$container);
                }
            }).fail(() => {
                KTApp.unblock(this.$container);
                KTApp.unblock(customer.$container);
                $.notify('Thêm khách hàng không thành công!', { type: 'danger' });
            });
        },
        remove: function (params) {
            KTApp.block(this.$container);
            KTApp.block(customer.$container);
            $.ajax({
                url: '/customers/remove-customer',
                method: 'POST',
                data: params || {},
            }).done((res) => {
                if (res && res.code === 0) {
                    managedCustomer.reload();
                    customer.load();
                } else {
                    KTApp.unblock(customer.$container);
                    KTApp.unblock(this.$container);
                }
            }).fail(() => {
                KTApp.unblock(this.$container);
                KTApp.unblock(customer.$container);
                $.notify('Xóa khách hàng không thành công!', { type: 'danger' });
            });
        },
        remove1: function (params) {
            KTApp.block(this.$container);
            KTApp.block(customer.$container);
            $.ajax({
                url: '/customers/remove-customer',
                method: 'POST',
                data: params || {},
            }).done((res) => {
                if (res && res.code === 0) {
                    managedCustomer.reload();
                    customer.load1();
                } else {
                    KTApp.unblock(customer.$container);
                    KTApp.unblock(this.$container);
                }
            }).fail(() => {
                KTApp.unblock(this.$container);
                KTApp.unblock(customer.$container);
                $.notify('Xóa khách hàng không thành công!', { type: 'danger' });
            });
        },
    };


    var customer = {

        $container: $('#js-area-3'),

        load: function () {
            KTApp.block(this.$container);
            $.ajax({
                url: "/customers-not-managed",
                method: 'GET',
                data: {
                    keyword: $("#js-filter-customer").val()
                }
            }).done((res) => {
                KTApp.unblock(this.$container);
                if (res && res.data) {
                    this.render(res.data || {});
                }
            }).fail(() => {
                KTApp.unblock(this.$container);
                $.notify('Không kết nối được đến máy chủ', { type: 'danger' });
            });
        },
        load1: function (res) {
            if (res) {
                this.render(res || {});
            }

        },

        render: function (data) {
            var html = [];
            debugger
            $.each(data.items || [], function (index, item) {
                html.push('<div class="kt-group kt-group--customer">');
                html.push(`
                    <div class="kt-group__title d-flex align-items-center">
                        ${item.name}
                    </div>
                `);
                html.push('<div class="kt-group__list">');
                $.each(item.customers || [], function (index, staff) {
                    html.push(`
                        <div data-uuid="${staff.uuid}" class="kt-group__item d-flex align-items-center">
                            ${staff.name}
                            <button data-uuid="${staff.uuid}" class="kt-group__btn text-primary js-add-customer ml-auto">
                                <i class="flaticon2-fast-back"></i>
                            </button>
                        </div>
                    `);
                });
                html.push('</div>');
                html.push('</div>');
            });


            if (data.items == null) {
                debugger
                $.each(data || [], function (index, staff) {

                    html.push(`
                        <div data-uuid="${staff.uuid}" class="kt-group__item d-flex align-items-center">
                            ${staff.name}
                            <button data-uuid="${staff.uuid}" class="kt-group__btn text-primary js-add-customer1 ml-auto">
                                <i class="flaticon2-fast-back"></i>
                            </button>
                        </div>
                    `);
                });
                html.push('</div>');
                html.push('</div>');
            }



            this.$container.html(html.join(''));
        },
        render1: function (data) {
            var html = [];
            $.each(data || [], function (index, item) {
                html.push('<div class="kt-group kt-group--customer">');

                html.push('<div class="kt-group__list">');

                html.push(`
                        <div data-uuid="${item.uuid}" class="kt-group__item d-flex align-items-center">
                            ${item.name}
                            <button data-uuid="${item.uuid}" class="kt-group__btn text-primary js-add-customer ml-auto">
                                <i class="flaticon2-fast-back"></i>
                            </button>
                        </div>
                    `);

                html.push('</div>');
                html.push('</div>');
            });

            this.$container.html(html.join(''));
        },

        select: function () { },
    };

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-team')
            .on('click', '.js-team', function () {
                const uuid = $(this).attr('data-uuid');
                if (uuid) {
                    $("#js-staff-uuid").val('');
                    $("#js-customer-uuid").val('');
                    $("#js-team-uuid").val(uuid);
                    team.select(uuid);
                    customer.load();
                }
            })
            .off('click', '.js-staff')
            .on('click', '.js-staff', function () {
                const uuid = $(this).attr('data-uuid');

                if (uuid) {
                    $("#js-staff-uuid").val(uuid);
                    $("#js-customer-uuid").val(uuid);
                    $("#js-team-uuid").val('');
                    team.selectStaff(uuid);


                }
            })
            .off('click', '.js-add-customer')
            .on('click', '.js-add-customer', function () {
                const uuid = $(this).attr('data-uuid');
                if (uuid) {
                    const params = {
                        customer_uuid: uuid,
                    };

                    if (_this.team_uuid) {
                        params.team_uuid = _this.team_uuid;
                    }

                    if (_this.staff_uuid) {
                        params.staff_uuid = _this.staff_uuid;
                    }

                    managedCustomer.add(params);
                }
            })
            .off('click', '.js-add-customer1')
            .on('click', '.js-add-customer1', function () {
                const uuid = $(this).attr('data-uuid');
                if (uuid) {
                    const params = {
                        customer_uuid: uuid,
                    };

                    if (_this.team_uuid) {
                        params.team_uuid = _this.team_uuid;
                    }

                    if (_this.staff_uuid) {
                        params.staff_uuid = _this.staff_uuid;
                    }

                    managedCustomer.add1(params);
                }
            })
            .off('click', '.js-remove-customer')
            .on('click', '.js-remove-customer', function () {
                const uuid = $(this).attr('data-uuid');
                if (uuid) {
                    const params = {
                        customer_uuid: uuid,
                    };

                    managedCustomer.remove(params);
                }
            })
            .off('click', '.js-remove-customer1')
            .on('click', '.js-remove-customer1', function () {
                const uuid = $(this).attr('data-uuid');
                if (uuid) {
                    const params = {
                        customer_uuid: uuid,
                        target_uuid: uuid_team,
                    };

                    managedCustomer.remove1(params);
                }
            })
            .off('change', '#js-filter-customer')
            .on('change', '#js-filter-customer', function () {

                debugger
                    if ($('#js-customer-uuid').val() !== '') {
                        managedCustomer.loadByStaff($("#js-staff-uuid").val());
                    }
                    else {
                        customer.load();
                    }
                
            })
            .off('change', '#js-filter-team-staff')
            .on('change', '#js-filter-team-staff', function () {
               
                    if ($("#js-staff-uuid").val() !== '') {
                        managedCustomer.loadByStaff($("#js-staff-uuid").val());
                    }

                    if ($("#js-team-uuid").val() !== '') {
                        managedCustomer.loadByTeam($("#js-team-uuid").val());
                    }
                
            })
            ;

    };

    return {
        // Public functions
        init: function () {
            team.load();
            customer.load();
            initEventListeners();
        },
    };
}();
$(document).ready(function () {
    if ($('.dmms-area').length) {
        DMMSArea.init();
    }
});
