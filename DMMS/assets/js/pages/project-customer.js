"use strict";
// Class definition

var DMMSProjectCus = function () {
    // Private functions
    var dataTableInstancecustomer = null;
    var dataTableInstance1 = null;
    var drawer = null;
    var LIMIT = 20;
    var START_PAGE = 0;
    var CURRENT_PAGE = 1;
    //var customercus = null;
    function subTableInit1(e) {

        var project_uuid = document.getElementById("project_uuid").value;
        debugger
        $('<div/>').attr('id', 'child_data_ajax_' + e.data.customid).appendTo(e.detailCell).KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/project/ProjectDetailStatistics',
                    },
                    response: {
                        map: function (res) {
                            debugger
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            if (raw.items.length > 0) {
                                for (var i = 0; i < raw.items.length; i++) {
                                    var a = raw.items[i].packages;
                                }
                            }
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: pagination.limit || LIMIT,
                                    page: pagination.page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword } = query;
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: keyword || '',
                            packageUuid: e.data.uuid,
                            customerUuid: e.data.customerUuid,
                            projectUuid: project_uuid,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            // layout definition
            layout: {
                scroll: false,
                //height: 3000,
                footer: false,
                //customScrollbar:true,
                // enable/disable datatable spinner.
                spinner: {
                    type: 1,
                    theme: 'default',
                },
            },

            sortable: false,
            pagination: false,
            responsive: true,
            // columns definition
            columns: [
                
                {
                    field: 'name',
                    title: 'Tên thiết bị',
                    template: function (data) {
                        return data.prefixName + " " + data.name;
                    }

                },
                {
                    field: 'count_total_device',
                    title: 'Tổng thiết bị',
                    width: 100

                },
                {
                    field: 'count_total_device_identified',
                    title: 'Tổng thiết bị định danh',
                    width: 200
                },
                {
                    field: 'count_total_device_unidentified',
                    title: 'Tổng thiết bị chưa định danh',
                    width: 200
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 50,
                    overflow: 'visible',
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        debugger
                        return `
                            <button data-id="${data.categoryUuid}" data-package ="${e.data.uuid}"data-customer="${e.data.customerUuid}" data-project=${project_uuid} title="Chi tiết" class="js-detail-customer-device btn btn-sm btn-success btn-icon btn-icon-md">
                                <i class="la la-info"></i>
                            </button>
                        `;
                    },
                }
                
                ],
        });
    }
    function subTableInit(e) {

        var project_uuid = document.getElementById("project_uuid").value;
        debugger
        
        $('<div/>').attr('id', 'child_data_ajax_' + e.data.customerUuid).appendTo(e.detailCell).KTDatatable({
            
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/project/ProjectGetCustomerPac',
                    },
                    response: {
                        map: function (res) {

                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            if (raw.items.length > 0) {
                                for (var i = 0; i < raw.items.length; i++) {
                                    var a = raw.items[i].packages;
                                }
                            }
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: pagination.limit || LIMIT,
                                    page: pagination.page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword } = query;
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: keyword || '',
                            contractUuid: e.data.contractUuid,
                            customerUuid: e.data.customerUuid,
                            projectUuid: project_uuid,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            // layout definition
            layout: {
                scroll: false,
                //height: 3000,
                footer: false,

                // enable/disable datatable spinner.
                spinner: {
                    type: 1,
                    theme: 'default',
                },
            },
            detail: {
                title: 'Đang tải dữ liệu ...',
                content: subTableInit1,
            },
            sortable: false,
            pagination: false,
            responsive: true,
            // columns definition
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    sortable: false,
                    textAlign: 'center',
                    width: 20,
                    //template: function (data, type, row) {
                    //    return row.index() + 1;
                    //}
                },
                {
                    field: 'name',
                    title: 'Phòng thiết bị',

                },
                {
                    field: 'count',
                    title: 'Số phòng',
                    width: 100

                },
                {
                    field: 'count_device',
                    title: 'Số lượng thiết bị',
                    width: 100
                },
                ],
        });
    }
    // demo initializer

    //var reloadTable = function () {
    //    if (dataTableInstance) {
    //        dataTableInstance.reload();
    //    }
    //};
    //var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
    //if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
    //    $.notify(editSucessCache.massage, { type: 'success' });
    //    localStorage.removeItem('edit_success');
    //}
    var dataCustomer = null;
    var initProjectDrawer = function (id, package1) {
        debugger
        $('.js-projeccustomer').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
        const title = id ? 'Thêm Đơn vị vào dự án' : '';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--cusproject' });
        var cusid = $('#province_display').val();
        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/project/addcustomer/${id}` : '',
                method: 'GET',
                data: { cusid: cusid },
            })
                .done(function (response) {

                    const $content = drawer.setBody(response);
                    dataCustomer = response ? package1 : null;
                    $(document).ready(function () {
                        $('.addinputp').click(function (e) {
                            debugger
                            var checknoti = $('.getpackage').find('option:selected').val();
                            if (checknoti != '' && $('.getcount').val() != '') {
                                e.preventDefault();
                                initControlSelect(dataCustomer)
                                $('.controlSelect1').selectpicker('refresh');
                            }
                            else {
                                $.notify('Vui lòng điền đủ thông tin!', { type: 'danger' });
                            }

                        });
                    });
                    $('.addpackage').on("click", ".deleteinput", function (e) {
                        debugger//user click on remove text
                       
                        e.preventDefault();
                        $(this).parent('div').parent().remove();
                        var lstItem1 = $(".controlSelectcus"),
                            arrItem1 = [];
                        for (var i = 0; i < lstItem1.length; i++) {
                            var valueItem = $(lstItem1[i]).val();
                            if (valueItem != "") {
                                arrItem1.push(valueItem);

                            }
                        }
                        debugger
                        var valselect = $('.getpackage').find('option:selected').val(); 
                        var arrItem1rls = arrItem1.filter(remove => remove !== valselect);
                        //dataCustomer.forEach(function (item, index) {
                        //    var rst = item
                        //})
                        var rst = dataCustomer.filter(check => !arrItem1rls.includes(check.uuid))
                        var selectcus = `<option value="">Chọn sản phẩm</option>`;
                        rst.forEach(function (item, index) {
                            selectcus += '<option value="' + item.uuid + '">' + item.name + '</option>';
                        })
                        

                        $('.getpackage').find('option').remove();
                        $('.getpackage').find('select').append(selectcus);
                        $('.getpackage').selectpicker('refresh');
                    });
                    initContent($content);


                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var initEditDrawer = function (id, id_cus, contractids, package1, state, date) {
        debugger
        $('.js-edit-customer').attr('disabled', false);
        var test = '';
        package1.forEach(function (i, v) {
            debugger
            test += '<option class = "removepackage'+v+'" value="' + i.uuid + '">' + i.name + '</option>'
        })
        debugger
        const title = 'Chỉnh sửa đơn vị vào dự án';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--cusproject' });
        var cusid = $('#province_display').val();
        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/project/editcustomer/${id}` : '',
                method: 'GET',
                data: { cusid: cusid, id_cus: id_cus, contractids: contractids, state: state, date: date },

            })
                .done(function (response) {
                    $.ajax({
                        url: id ? `/project/GetLstPackage/${id}` : '',
                        method: 'GET',
                        data: { cusid: cusid, id_cus: id_cus, contractids: contractids, state: state, date: date },

                    })
                        .done(function (response) {
                            debugger
                            $(response.data.items).each(function (index, item) {
                                debugger
                                var select = `
                            <div class="kt-input-icon form-group row">
                                <div class="form-group col-6">
                                    <select data-live-search="true" name = "orderPackages[][packageUuid]" class="form-control disabled controlSelectcus controlSelectx${index}">`;
                                if (response.data.items == null && response.data.items.length == 0) {
                                    response.data.items.uuid
                                    select += '<option value=""></option>';
                                }
                                else {
                                    debugger
                                    select += '<option value="' + item.uuid + '">'+item.name+'</option>';



                                }

                                var html = `
            
            </select>
                </div>
         <div class="form-group col-4">
                <input type="number" name="orderPackages[][quantity]" class="form-control" value="${item.count}" placeholder="Số lượng" min="1" />
        </div>
        <div class="kt-ml-10">
                 <i class="btn btn-danger la la-minus deleteinput"></i>       
        </div>
            </div>`;




                                $('.addpackage').append(select + html);
                                //$(`.controlSelect${index}`).val(item.uuid);
                                //$(`.controlSelect${index}`).selectpicker('refresh');
                                //$('.controlSelect1').selectpicker('refresh');
                                var previous;
                                //$('.controlSelectcus').unbind('change');
                                $('.controlSelectx' + index).on('shown.bs.select', function (e) {
                                    previous = $(this).val();

                                })
                                    .bind('change', function () {

                                        debugger
                                        //$('.controlSelect1').selectpicker('refresh');
                                        debugger
                                        var lstItem1 = $(".controlSelectcus"),
                                            arrItem1 = [];
                                        for (var i = 0; i < lstItem1.length; i++) {
                                            var valueItem = $(lstItem1[i]).val();
                                            if (valueItem != "") {
                                                arrItem1.push(valueItem);

                                            }
                                        }
                                        var abcd = $(this).val()
                                        var az = arrItem1.filter(item => item == abcd);
                                        if (az != "" && az.length > 1) {

                                            $.notify('Bạn đã chọn phòng này rồi. Vui lòng chọn lại!', { type: 'danger' });
                                            $(this).val(previous);

                                            $('.controlSelectcus').selectpicker('refresh');
                                        }
                                        $('.controlSelectcus').selectpicker('refresh');
                                        previous = $(this).val();

                                    });
                               
                                $('.controlSelectx' + index).val($(this)[0].uuid);
                                $('.controlSelectx' + index).selectpicker('refresh');
                            });
                           
                            //$('.resetuuid').val('');
                            //$('.resetuuid').selectpicker('refresh');
                            debugger
                            var lstItemcheck = $(".controlSelectcus"),
                                arrItemcheck = [];
                            for (var i = 0; i < lstItemcheck.length; i++) {
                                var valueItemcheck = $(lstItemcheck[i]).val();
                                if (valueItemcheck != "") {
                                    arrItemcheck.push(valueItemcheck);

                                }
                            }
                            var rstcheck = package1.filter(check => !arrItemcheck.includes(check.uuid))
                            var selectcus1 = `<option value="">Chọn sản phẩm</option>`;
                            rstcheck.forEach(function (item, index) {
                                selectcus1 += '<option value="' + item.uuid + '">' + item.name + '</option>';
                            })
                            

                            $('.getpackage').find('option').remove();
                            $('.resetuuid').find('select').append(selectcus1);
                            $('.getpackage').selectpicker('refresh');
                            //$(".controlSelect1 option").val(function (idx, val) {
                            //    debugger
                            //    $(this).siblings('[value="' + val + '"]').remove();
                            //});
                            //var map = {};
                            //$('#client_id option').each(function () {
                            //    if (map[this.value]) {
                            //        $(this).remove();
                            //    }
                            //    map[this.value] = true;
                            //});



                        })

                        .fail(function () {
                            drawer.error();

                        });
                   
                    const $content = drawer.setBody(response);
                    dataCustomer = response ? package1 : null;
                    $(document).ready(function () {
                        $('.addinputp').click(function (e) {
                            debugger
                            var checknoti = $('.getpackage').find('option:selected').val();
                            if (checknoti != '' && $('.getcount').val() != '') {
                                e.preventDefault();
                                initControlSelect(dataCustomer)
                                $('.controlSelect1').selectpicker('refresh');
                            }
                            else {
                                $.notify('Vui lòng điền đủ thông tin!', { type: 'danger' });
                            }
                        });
                    });
                    $('.addpackage').on("click", ".deleteinput", function (e) { //user click on remove text
                        e.preventDefault();
                        $(this).parent('div').parent().remove();
                        var lstItem1 = $(".controlSelectcus"),
                            arrItem1 = [];
                        for (var i = 0; i < lstItem1.length; i++) {
                            var valueItem = $(lstItem1[i]).val();
                            if (valueItem != "") {
                                arrItem1.push(valueItem);

                            }
                        }
                        debugger
                        var valselect = $('.getpackage').find('option:selected').val();
                        var arrItem1rls = arrItem1.filter(remove => remove !== valselect);
                        //dataCustomer.forEach(function (item, index) {
                        //    var rst = item
                        //})
                        var rst = dataCustomer.filter(check => !arrItem1rls.includes(check.uuid))
                        var selectcus = `<option value="">Chọn sản phẩm</option>`;
                        rst.forEach(function (item, index) {
                            selectcus += '<option value="' + item.uuid + '">' + item.name + '</option>';
                        })
                        selectcus += `</select>`

                        $('.getpackage').find('option').remove();
                        $('.getpackage').find('select').append(selectcus);
                        $('.getpackage').selectpicker('refresh');
                    });
                    initContent($content);
                    $('.controlSelectcus').unbind('change');
                    $('.controlSelectcus')
                        .bind('change', function () {
                            debugger
                            //$('.controlSelect1').selectpicker('refresh');
                            debugger
                            var lstItem1 = $(".controlSelectcus"),
                                arrItem1 = [];
                            for (var i = 0; i < lstItem1.length; i++) {
                                var valueItem = $(lstItem1[i]).val();
                                if (valueItem != "") {
                                    arrItem1.push(valueItem);

                                }
                            }
                            var abcd = $(this).val()
                            var az = arrItem1.filter(item => item == abcd);
                            if (az != "" && az.length > 1) {

                                $.notify('Bạn đã chọn phòng này rồi. Vui lòng chọn lại!', { type: 'danger' });
                                $(this).val("");

                                $('.controlSelectcus').selectpicker('refresh');
                            }
                            $('.controlSelectcus').selectpicker('refresh');
                        });
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var initControlSelect = function (data) {
        debugger
        //if (data != null && data.length > 0) {
        //Thực hiện lấy danh sách các gói đã select

        var lstItem = $(".controlSelectcus"),

            arrItem = [], //Dánh sách lưu các id được seclect
            lstItemResult = [];//tạo mảng danh sách mới



        for (var i = 0; i < lstItem.length; i++) {
            var valueItem = $(lstItem[i]).val();
            if (valueItem != "") {
                arrItem.push(valueItem);

            }
        }

        //Danh sách mới các item sẽ không có các giá trị selected
        //Thực hiện lọc lại danh sách
        //Sử dụng findler để lọc

        data.forEach(function (item) {
            var countSelected = arrItem.length;
            arrItem.forEach(function (i, e) {
                if (item.uuid != i) {
                    countSelected--;
                }
                if (countSelected == 0) {
                    lstItemResult.push(item);
                }
            })
            //lstItemResult.push(item);
        })

        var select = `
                            <div class="kt-input-icon form-group row">
                                <div class="form-group col-6">
                                    <select data-live-search="true" name = "orderPackages[][packageUuid]" class="form-control controlSelectcus controlSelect1 disabled">`;
      
        var select1 = `<option value="">Chọn sản phẩm</option>`;
            debugger
            //dataCustom = lstItemResult;
            
                
           
        
        var count1 = $('.getcount').val();
        var package1 = $('.getpackage').find('option:selected').val();
        var namepackage1 = $('.getpackage').find('option:selected').text()
        select += '<option value="' + package1 + '">' + namepackage1 + '</option>';
        if (lstItemResult == null && lstItemResult.length == 0) {

            select += '<option value=""></option>';
        }
        else {
            lstItemResult.forEach(function (item, index) {
                select1 += '<option value="' + item.uuid + '">' + item.name + '</option>';
            })
        }
       
        var html = `
            
            </select>
                </div>
         <div class="form-group col-4">
                <input type="number" name="orderPackages[][quantity]" class="form-control" placeholder="Số lượng" value="${count1}" min="1"/>
        </div>
        <div class="kt-ml-10">
                 <i class="btn btn-danger la la-minus deleteinput"></i>       
        </div>
            </div>`;
        if ($('.getpackage').find('option:selected').val() != null) {
            $('.addpackage').append(select + html);
            $('.getcount').val('');
            $('#getval').val('');

            $('.getpackage').find('option').remove();
            $('.getpackage').find('select').append(select1)
        }
        else {
            $.notify('Vui lòng chọn sản phẩm!', { type: 'danger' });
        }
        
        //}
        $('.controlSelectcus').unbind('change');
        $('.controlSelectcus')
            .bind('change', function () {
                $('.controlSelect1').selectpicker('refresh');
                debugger
                var lstItem1 = $(".controlSelectcus"),
                    arrItem1 = [];
                for (var i = 0; i < lstItem1.length; i++) {
                    var valueItem = $(lstItem1[i]).val();
                    if (valueItem != "") {
                        arrItem1.push(valueItem);

                    }
                }
                var abcd = $(this).find('option:selected').val();
                var az = arrItem1.filter(item => item == abcd);
                if (az != "" && az.length > 1) {

                    $.notify('Bạn đã chọn phòng này rồi. Vui lòng chọn lại!', { type: 'danger' });
                    $(this).val("");

                    $('.controlSelectcus').selectpicker('refresh');
                }

            });

    }
    $('.resetfiler').on('click', function () {

        $('#js-filter-location').val('');
        $('#js-filter-location').selectpicker('refresh');
        $('#locationtown').val('');
        $('#locationtown').attr('disabled', true);
        $('#locationtown').selectpicker('refresh');
        $('#locationvillage').val('');
        $('#locationvillage').attr('disabled', true);
        $('#locationvillage').selectpicker('refresh')
        if (dataTableInstancecustomer) {
            dataTableInstancecustomer.search($('#js-filter-location').val(), 'provinceId'),
                START_PAGE = 1
        };


    })
    var initEventListeners = function () {
        $('body')
            .off('click', '.js-projeccustomer')
            .on('click', '.js-projeccustomer', function (e) {
                debugger
                $(this).addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                e.preventDefault;
                const $this = $(this);
                const id = document.getElementById("project_uuid").value;
                $.ajax({
                    url: `/project/GetlstPac/${id}`,
                    method: 'GET',
                })
                    .done(function (data) {

                        const package1 = data.data.items;
                        
                        initProjectDrawer(id, package1 || '', package1);

                    })
                    .fail(function () {
                        $.notify('Có lỗi xảy ra !', { type: 'danger' });
                    });

            })
            .off('click', '.js-detail-customer-device')
            .on('click', '.js-detail-customer-device', function (e) {
                const $this = $(this);
                const categoryUuid = $this.attr('data-id');
                const project = $this.attr('data-project');
                const customer = $this.attr('data-customer');
                const packagetest = $this.attr('data-package');
                initDetail(categoryUuid, project, customer, packagetest);

            })
            .off('click', '.js-add-contract')
            .on('click', '.js-add-contract', function (e) {
                $(this).attr('disabled',true)
                const $this = $(this);
                const custommerUuid = $this.attr('data-id');
                const project = $this.attr('data-project');
                const customer = $this.attr('data-customer');
                const packagetest = $this.attr('data-package');
                initContract(custommerUuid, project, customer, packagetest);
                $(this).attr('disabled', false)

            })
            .off('click', '.js-delete-customer')
            .on('click', '.js-delete-customer', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                const contractid = $this.attr('data-contract');
                const projectid = $this.attr('data-project');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Xóa đơn vị khỏi dự án.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteCustomer(id, contractid, projectid);
                    }
                });
            })
            .off('click', '.js-edit-customer')
            .on('click', '.js-edit-customer', function () {
                $(this).attr('disabled', true);
                debugger
                const $this = $(this);
                const id_cus = $this.attr('data-id');
                const contractids = $this.attr('data-contract');
                const state = $this.attr('data-state');
                // const projectid = $this.attr('data-project');
                const date = $this.attr('data-date');
                const id = document.getElementById("project_uuid").value;
                $.ajax({
                    url: `/project/GetlstPac/${id}`,
                    method: 'GET',
                })
                    .done(function (data) {

                        const package1 = data.data.items;
                        initEditDrawer(id, id_cus, contractids, package1, state, date || '', '', '', '', '', package1);

                    })
                    .fail(function () {
                        $.notify('Có lỗi xảy ra !', { type: 'danger' });
                    });

            })


    };
    var initDetail = function (categoryUuid, project, customer, packagetest) {
        debugger
        const title = `DANH SÁCH CHI TIẾT THIẾT BỊ`;
        drawer = new KTDrawer({ title, categoryUuid: categoryUuid || 'new', className: 'dmms-drawer--upload' });

        drawer.on('ready', () => {
            $.ajax({
                url: `/project/Detaildevicecustomer`,
                method: 'GET',
            })
                .done(function (response) {
                    debugger
                    const $content = drawer.setBody(response);
                    const $form = $content.find('.dmms-upload__form');

                    initContentdetailcus($content, categoryUuid, project, customer, packagetest);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var initContentdetailcus = function ($content, categoryUuid1, project, customer, packagetest) {
        debugger
        
        dataTableInstance1 = $('.kt-datatable-device-customer').KTDatatable({

            data: {
                type: 'remote',
                source: {
                    read: {
                        url: `/project/ProjectDetailDeviceCustomer`,
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page,
                                CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword, projectUuid, packageUuid, customerUuid, categoryUuid } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            projectUuid: project,
                            packageUuid: packagetest,
                            customerUuid: customer,
                            categoryUuid: categoryUuid1,
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,
            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 35,
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    },
                    autoHide: false,
                    textAlign: 'center',

                },
                {
                    field: 'serial',
                    title: 'Serial',


                },
                {
                    field: 'imei',
                    title: 'Imei',
                    width: 110,

                },
                {
                    field: 'supplier.name',
                    title: 'Nhà cung cấp',
                    width: 110,

                },
                {
                    field: 'time_expireDevice_format',
                    title: 'Ngày hết hạn BH',
                    width: 110,

                },
                {
                    field: 'packageName',
                    title: 'Phòng tiết bị',
                    width: 110,

                },

                {
                    field: 'state',
                    title: 'Trạng thái',
                    width: 150,
                    template: function (data) {
                        debugger
                        var states = {
                            1: { 'title': 'Đang sử dụng', 'class': 'kt-badge--brand' },
                            2: { 'title': 'Đang yêu cầu sửa chữa', 'class': ' kt-badge--warning' },
                            3: { 'title': 'Đến lịch bảo hành', 'class': ' kt-badge--primary' },
                            4: { 'title': 'Đang xử lý theo yêu cầu', 'class': 'kt-badge--success' },
                            5: { 'title': 'Cần thay thế', 'class': 'kt-badge--danger' },
                            6: { 'title': 'Mang đi sửa chữa - Không có thiết bị thay thế', 'class': 'kt-badge--info' },
                            7: { 'title': 'Mang đi sửa chữa - Có thiết bị thay thế', 'class': 'kt-badge--info' },
                            8: { 'title': 'Thay thế', 'class': 'kt-badge--info' },
                            9: { 'title': 'Hỏng', 'class': 'kt-badge--info' },
                            10: { 'title': 'Trong kho - Trong kho vật lý, ba lô nhân viên sau khi import', 'class': 'kt-badge--info' },
                            11: { 'title': 'Trong kho - Hỏng, sau khi kiểm kê', 'class': 'kt-badge--info' },
                            12: { 'title': 'Đang vận chuyển', 'class': 'kt-badge--info' },
                            13: { 'title': 'Đã xuất thanh lý', 'class': 'kt-badge--info' },
                            14: { 'title': 'Đã xuất tiêu hao', 'class': 'kt-badge--info' },
                            15: { 'title': 'Đã hoàn trả nhà cung cấp', 'class': 'kt-badge--info' },
                            16: { 'title': 'Thêm mới/cập nhật thiết bị', 'class': 'kt-badge--info' },
                            17: { 'title': 'Đã sửa xong thiết bị', 'class': 'kt-badge--info' },

                        };
                        if (data.state.id) {
                            return '<span class="kt-badge ' + states[data.state.id].class + ' kt-badge--inline kt-badge--pill">' + states[data.state.id].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Chưa rõ</span>';
                        }
                    },
                    autoHide: false,
                    width: 150,
                    textAlign: 'center'
                },
            ],
            sortable: false,
            pagination: true,
            responsive: true,
        });
        
        //const $form1 = $content.find('.dmms-knowledge__formqa');
        //initForm1($form1);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));


        /**
         * Init Dropzone
         */
        //initDropzone();
    };
    var initContract = function (custommerUuid, project, customer, packagetest) {
        debugger
        const title = `TẠO HỢP ĐỒNG GIA HẠN CHO ĐƠN VỊ TRIỂN KHAI`;
        drawer = new KTDrawer({ title, custommerUuid: custommerUuid || 'new', className: 'dmms-drawer--customercontractproject' });

        drawer.on('ready', () => {
            $.ajax({
                url: `/project/CreateContract/${project}/${custommerUuid}`,
                method: 'GET',
            })
                .done(function (response) {
                    debugger
                    const $content = drawer.setBody(response);
                    $('.kt-select-customer-contract').selectpicker();
                    $('#timeselect').selectpicker();
                    $('.datecomplete').datepicker({ format: 'dd/mm/yyyy' }).datepicker('setDate', new Date());
                    $('.datesigned').datepicker({ format: 'dd/mm/yyyy' }).datepicker('setDate', new Date());
                    $("#timeinput").change(function () {
                        debugger
                        var a = $('#timeinput').val();
                        var b = $('#timeselect').val()
                        var new_date = moment(a, "DD/MM/YYYY").add(b, 'months');
                        var newdate = new_date.format('DD/MM/YYYY');
                        $('.endgame').val(newdate);
                    });
                    $("#timeselect").change(function () {
                        debugger
                        var a = $('#timeinput').val();
                        var b = $('#timeselect').val()
                        var new_date = moment(a, "DD/MM/YYYY").add(b, 'months');
                        var newdate = new_date.format('DD/MM/YYYY');
                        $('.endgame').val(newdate);
                    });
                    const $form = $content.find('.dmms-upload__form');
                    initContentdetail($content, custommerUuid, project, customer, packagetest);

                   
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var initContentdetail = function ($content, custommerUuid, project, customer, packagetest) {
        debugger
        var uuid = document.getElementById("project_uuid").value;
        const $form2 = $content.find('.dmms-project-contract__form');
        initForm2($form2);
        //const $form1 = $content.find('.dmms-knowledge__formqa');
        //initForm1($form1);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));


        /**
         * Init Dropzone
         */
        //initDropzone();
    };
    var deleteCustomer = function (id, contractid, projectid) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa dự án...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/project/DeleteCustomer/`,
                method: 'DELETE',
                data: { contractUuid: contractid, projectUuid: projectid, customerUuid: id }
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa đơn vị thành công!', { type: 'success' });

                    if (dataTableInstancecustomer) {
                        dataTableInstancecustomer.reload();
                    }
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa đơn vị không thành công!', { type: 'danger' });
                });
        }
    };
    var settime = function () {

    }
    var initValidation = function () {
        $('.dmms-project-customer__form').validate({
            rules:
            {
                //'orderPackages[][quantity]': {
                //    required: true,
                //},
                customerUuid: {
                    required: true,
                },
                contractUuid: {
                    required: true,
                },
                //'orderPackages[][packageUuid]': {
                //    required: true,
                //}

            },
            messages: {
                //'orderPackages[][quantity]': {
                //    required: 'Vui lòng nhập số lượng.'
                //},
                customerUuid: {
                    required: 'Vui lòng chọn đơn vị'
                },
                contractUuid: {
                    required: 'Vui lòng chọn hợp đồng'
                },
                //'orderPackages[][packageUuid]': 'Vui lòng chọn phòng triển khai'

            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo('.dmms-project-customer__form', -200);
                $('#kt-select-customer1').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });
                $('.controlSelectcus').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });
            },
            submitHandler: function (form) {
                $(form).find('button[type="submit"]').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                $.ajax({
                    url: `/project/addcustomer/`,
                    method: 'POST',
                    data,
                })
                    .done(function (data) {

                        //$(".submit").attr("disabled", true);
                        //KTApp.block('.dmms-project-customer__form');
                        const successMsg = 'Thêm mới khách hàng vào dự án thành công.';
                        $.notify(successMsg, { type: 'success' });
                        drawer.close();
                        reloadTable();
                        /**
                         * Close drawer
                         */


                        //reloadTable();
                        //localStorage['edit_success'] = JSON.stringify({ from: (`/project/addcustomer/`), massage: successMsg });

                        //window.location.replace("customer");
                        //window.location = window.location.origin + "/project";
                    })
                    .fail(function (data) {
                        debugger
                        $(form).find('button[type="submit"]').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);

                        KTApp.unblock('.dmms-project-customer__form');
                        if (data.responseJSON == 1) {
                            var errorMsg = 'Bạn chưa chọn sản phẩm!'
                        }
                        else {
                            var errorMsg = data.responseJSON.msg + '!';
                        }
                        
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }
    var initValidation1 = function () {
        $('.dmms-project-editcustomer__form').validate({
            rules:
            {
                //'orderPackages[][quantity]': {
                //    required: true,
                //},
                customerUuid: {
                    required: true,
                },
                contractUuid: {
                    required: true,
                },
                //'orderPackages[][packageUuid]': {
                //    required: true,
                //}

            },
            messages: {
                //'orderPackages[][quantity]': {
                //    required: 'Vui lòng nhập số lượng.'
                //},
                customerUuid: {
                    required: 'Vui lòng chọn đơn vị'
                },
                contractUuid: {
                    required: 'Vui lòng chọn hợp đồng'
                },
                //'orderPackages[][packageUuid]': 'Vui lòng chọn phòng triển khai'

            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo('.dmms-project-editcustomer__form', -200);
            },
            submitHandler: function (form) {
                $(form).find('button[type="submit"]').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                $.ajax({
                    url: `/project/editcustomer/`,
                    method: 'POST',
                    data,
                })
                    .done(function (data) {
                        debugger
                        //$(".submit").attr("disabled", true);
                        //KTApp.block('.dmms-project-customer__form');
                        const successMsg = 'Cập nhật khách hàng vào dự án thành công.';
                        $.notify(successMsg, { type: 'success' });
                        drawer.close();
                        reloadTable();
                        /**
                         * Close drawer
                         */


                        //reloadTable();
                        //localStorage['edit_success'] = JSON.stringify({ from: (`/project/addcustomer/`), massage: successMsg });

                        //window.location.replace("customer");
                        //window.location = window.location.origin + "/project";
                    })
                    .fail(function (data) {
                        $(form).find('button[type="submit"]').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);

                        KTApp.unblock('.dmms-project-editcustomer__form');
                        const errorMsg = data.responseJSON.msg + '!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }
    var initValidation2 = function () {
        $('.dmms-project-contract__form').validate({
            rules:
            {
                //'orderPackages[][quantity]': {
                //    required: true,
                //},
                //customerUuid: {
                //    required: true,
                //},
                //contractUuid: {
                //    required: true,
                //},
                //'orderPackages[][packageUuid]': {
                //    required: true,
                //}

            },
            messages: {
                //'orderPackages[][quantity]': {
                //    required: 'Vui lòng nhập số lượng.'
                //},
                //customerUuid: {
                //    required: 'Vui lòng chọn đơn vị'
                //},
                //contractUuid: {
                //    required: 'Vui lòng chọn hợp đồng'
                //},
                //'orderPackages[][packageUuid]': 'Vui lòng chọn phòng triển khai'

            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo('.dmms-project-contract__form', -200);
            },
            submitHandler: function (form) {
                $(form).find('button[type="submit"]').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                $.ajax({
                    url: `/project/CreateContract/`,
                    method: 'POST',
                    data,
                })
                    .done(function (data) {
                        debugger
                        //$(".submit").attr("disabled", true);
                        //KTApp.block('.dmms-project-customer__form');
                        const successMsg = 'Gia hạn hợp đồng thành công.';
                        $.notify(successMsg, { type: 'success' });
                        drawer.close();
                        reloadTable();
                        $('[href="#kt_tabs_1_4"]').trigger('click')
                    })
                    .fail(function () {
                        $(form).find('button[type="submit"]').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);

                        KTApp.unblock('.dmms-project-contract__form');
                        const errorMsg = 'Gia hạn hợp đồng không thành công.';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }
    var initForm = function ($form) {
        initValidation(true);
    }
    var initForm1 = function ($form1) {
        initValidation1(true);
    }
    var initForm2 = function ($form2) {
        initValidation2(true);
    }
    var reloadTable = function () {
        if (dataTableInstancecustomer) {
            dataTableInstancecustomer.reload();
        }
    };
    var initSearch = function () {
        $('#js-filter-packagea')
            .on('change', function () {
                if (dataTableInstancecustomer) {
                    dataTableInstancecustomer.search($(this).val().toLowerCase(), 'packageCodeId');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-supplierID')
            .on('change', function () {
                if (dataTableInstancecustomer) {
                    dataTableInstancecustomer.search($(this).val(), 'contractState');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-keywordp')
            .on('change', function () {
                if (dataTableInstancecustomer) {
                    dataTableInstancecustomer.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            })
        $('#locationvillage')
            .on('change', function () {
                if (dataTableInstancecustomer) {
                    dataTableInstancecustomer.search($(this).val().toLowerCase(), 'wardId');
                    START_PAGE = 1;
                }
            })
        $('#js-filter-location')
            .on('change', function () {
                if ($(this).val() == '') {
                    $('#js-filter-location').val('');
                    $('#js-filter-location').selectpicker('refresh');
                    $('#locationtown').val('');
                    $('#locationtown').attr('disabled', true);
                    $('#locationtown').selectpicker('refresh');
                    $('#locationvillage').val('');
                    $('#locationvillage').attr('disabled', true);
                    $('#locationvillage').selectpicker('refresh')
                    if (dataTableInstancecustomer) {
                        dataTableInstancecustomer.search($('#js-filter-location').val(), 'provinceId'),
                            START_PAGE = 1
                    };
                }
                if (dataTableInstancecustomer) {
                    dataTableInstancecustomer.search($(this).val(), 'provinceId');
                    START_PAGE = 1;
                }
            })
        //$('#js-filter-location')
        //    .on('change', function () {

        //    });


    };
    var location = function () {
        $('#js-filter-location')
            .on('change', function () {


                if (dataTableInstancecustomer) {
                    dataTableInstancecustomer.search($(this).val(), 'provinceId');
                    START_PAGE = 1;
                }
                $('#locationtown').val('');
                $('#locationvillage').val('');
                var provinceid = $(this).val();
                $.ajax({
                    url: `/project/getlocationtown/${provinceid}`,
                    method: 'GET',
                })
                    .done(function (data) {

                        console.log(data);
                        var town = data.data.items;

                        var $select = $('#locationtown');

                        $('#locationvillage').attr('disabled', true);
                        $('#locationvillage').selectpicker('refresh')
                        $select.find('option').remove();
                        $select.append(`<option value=''>---Quận/Huyện/Thị Xã---</option>)`);
                        $.each(town, function (index, item) {

                            console.log(item);
                            $select.append('<option value=' + item.id + '>' + item.name + '</option>');
                            $select.attr('disabled', false)
                            $select.selectpicker('refresh');
                        })


                    })
                    .fail(function () {

                    });
            });
        $('#locationtown')
            .on('change', function () {

                if (dataTableInstancecustomer) {
                    dataTableInstancecustomer.search($(this).val(), 'districtId');
                    START_PAGE = 1;
                }
                $('#locationvillage').val('');
                var town = $(this).val();
                $.ajax({
                    url: `/project/getlocationvillages/${town}`,
                    method: 'GET',
                })
                    .done(function (data) {

                        console.log(data);
                        var village = data.data.items;

                        var $select = $('#locationvillage');

                        //for (var i = 0; i < customer.length; i++) {
                        //    $select.append('<option value=' + customer[i].project_customer_uuid + '>' + customer[i].name + '</option>');
                        //    $select.selectpicker('refresh');
                        //}
                        $select.find('option').remove();
                        $select.append(`<option value=''>---Phường/Xã/Thị Trấn---</option>)`);
                        $.each(village, function (index, item) {

                            console.log(item);
                            $select.append('<option value=' + item.id + '>' + item.name + '</option>');
                            $select.attr('disabled', false)
                            $select.selectpicker('refresh');
                        })


                    })
                    .fail(function () {

                    });
            });
    }
    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-project-customer__form');
        const $form1 = $content.find('.dmms-project-editcustomer__form');

        //$("#js-filter-creator").select2({
        //    placeholder: 'None',
        //    ajax: {
        //        url: function (params, page) {
        //            return "/customer/gets/" + page
        //        },
        //        dataType: 'json',
        //        delay: 250,
        //        data: function (term, page) {
        //            return {
        //                keyword:term || "", // search term
        //                page: page || 1,
        //                limit:10,
        //            };
        //        },
        //        processResults: function (data1, page) {
        //            debugger
        //            var data = data1.data.items;
        //            var more = (page * 10) < data.length;
        //            var udata = [{ 'id': 0, 'text': 'chọn' }];
        //            //for (i in data) {
        //            //    udata.push({ 'uuid': data[i]['uuid'], 'text': data[i]['name'] });
        //            //}
        //            data.forEach(function (index, item) {
        //                udata.push({ 'id': item.uuid, 'text': item.name })
        //            })
        //            console.log(udata);
        //            return {
        //                results: udata,
        //                pagination: {
        //                    // THE `10` SHOULD BE SAME AS `$resultCount FROM PHP, it is the number of records to fetch from table` 
        //                    more: more
        //                }
        //            };
        //        }
        //    },
        //});
        //if ($("#customer_uuid_str").val() !== '') {
        //    $('#kt-select-customer').selectpicker('val', JSON.parse($("#customer_uuid_str").val()));
        //}

        $('#kt-select-customer').selectpicker();
        //$('.search').on('change', function () {
        //    alert('xx')
        //})

        initForm($form);
        initForm1($form1);

        /*
            init auto-resize
        */
        $('#kt-select-customer1').selectpicker({
            noneResultsText: 'Không tìm thấy kết quả'
        }

        );
        $('.kt-select-contract').selectpicker();
        $('.kt-staff-select').selectpicker();
        $('#js-select-statedevice').selectpicker();
        $('#timeHandOver').datepicker({ format: 'dd/mm/yyyy', });

        autosize($content.find('textarea.auto-resize'));

        $('#js-filter-customer').selectpicker();


    };

    $(".linkdetail1").on("click", function () {
        debugger
        if (!$('.nav-item').find('.active').hasClass("linkdetail1")) {
            var projectuuid = document.getElementById("project_uuid").value;
            //if (dataTableInstancecustomer != null) dataTableInstancecustomer.destroy();
            if (dataTableInstancecustomer != null) {
                dataTableInstancecustomer.load();
            }
            else {
                dataTableInstancecustomer = $('#kt-datatableprodetail').KTDatatable({
                    data: {
                        type: 'remote',
                        source: {
                            read: {
                                url: `/project/projectgetcustomer/${projectuuid}`,
                            },
                            response: {
                                map: function (res) {
                                    const raw = res && res.data || {};
                                    const { pagination } = raw;
                                    const perpage = pagination === undefined ? undefined : pagination.perpage;
                                    const page = pagination === undefined ? undefined : pagination.page;
                                    START_PAGE = 0;
                                    return {
                                        data: raw.items || [],
                                        meta: {
                                            ...pagination || {},
                                            perpage: perpage || LIMIT,
                                            page: page || 1,
                                        },
                                    }
                                },
                            },
                            filter: function (data = {}) {

                                const query = data.query || {};
                                const pagination = data.pagination || {};
                                const { project_uuid, keyword, provinceId, districtId, wardId, contractState } = query;
                                if (START_PAGE === 1) {
                                    pagination.page = START_PAGE;
                                }
                                return {
                                    limit: pagination.perpage || LIMIT,
                                    project_uuid: project_uuid || $('#js-filter-customer').val(),
                                    provinceId: provinceId || $('#js-filter-location').val(),
                                    districtId: $('#locationtown').val(),
                                    wardId: $('#locationvillage').val(),
                                    contractState: $('#js-filter-supplierID').val(),
                                    keyword: (keyword || $('#js-filter-keywordp').val()).trim(),
                                    page: pagination.page
                                };
                            }
                        },
                        pageSize: LIMIT,
                        serverPaging: true,
                        serverFiltering: true,
                    },
                    layout: {
                        scroll: true
                    },
                    search: {
                        onEnter: true,
                    },
                    detail: {
                        title: 'Đang tải dữ liệu ...',
                        content: subTableInit,
                    },
                    columns: [
                        {
                            field: 'customid',
                            title: '#',
                            sortable: false,
                            textAlign: 'center',
                            width: 20,
                            //template: function (data, type, row) {
                            //    return row.index() + 1;
                            //}
                        },
                        {
                            field: 'customerName',
                            title: 'Tên Đơn vị',

                        },
                        {
                            field: '',
                            title: 'SL phòng triển khai',
                            width: 60,
                            template: function (row) {
                                debugger
                                var a = 0;
                                $.each(row.packages, function (i, v) {
                                    a += v.countPackageDeploys;
                                })
                                return a;
                            },

                        },
                        {
                            field: 'contractCode',
                            title: 'Số hiệu HĐ',
                            width: 70
                        },
                        {
                            field: 'province.name',
                            title: 'Tỉnh/Thành',
                            width: 75,
                        },
                        {
                            field: 'contract_Timehandover_format',
                            title: 'Ngày bàn giao',
                            width: 75,
                        },
                        {
                            field: 'contract_TimeValidEnd_string',
                            title: 'Thời gian hết BH',
                            width: 75,
                        },
                        {
                            field: 'state',
                            title: 'Trạng thái',

                            textAlign: 'center',
                            width: 110,

                            template: function (row) {
                                var states = {
                                    1: { 'title': 'Đang bảo hành', 'class': 'kt-badge--success' },
                                    2: { 'title': 'Hết bảo hành', 'class': 'kt-badge--warning' },
                                    3: { 'title': 'Đã gửi thông báo', 'class': 'kt-badge--primary' },
                                    4: { 'title': 'Đã ký đóng', 'class': ' kt-badge--primary' },
                                    5: { 'title': 'Đóng bảo hành', 'class': 'kt-badge--info' },
                                    6: { 'title': 'Gia hạn bảo hành', 'class': 'kt-badge--success' },
                                    7: { 'title': 'Chưa nghiệm thu', 'class': 'kt-badge--danger' },
                                };
                                if (row.state) {
                                    return '<p class="kt-badge ' + states[row.state].class + ' kt-badge--inline kt-badge--pill">' + states[row.state].title + '</p>';
                                }
                                else {
                                    return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới</span>';
                                }
                            }
                        },
                        {
                            field: 'Tác vụ',
                            title: 'Tác vụ',
                            sortable: false,
                            width: 110,
                            overflow: 'visible',
                            autoHide: false,
                            textAlign: 'center',
                            template: function (data) {
                                debugger
                                return `
                            <button data-id="${data.customerUuid}" data-contract="${data.contractUuid}" data-project="${projectuuid}" data-state=${data.state} data-date="${data.contract_Timehandover_format}" type="button" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md js-edit-customer">
                                <i class="la la-edit"></i>
                           </button>
                              <button data-id="${data.customerUuid}" data-contract="${data.contractUuid}" data-project="${projectuuid}" type="button" title="Xóa" class="btn btn-sm btn-danger btn-icon btn-icon-md js-delete-customer">
                                <i class="la la-trash"></i>
                           </button>
                            <button data-id="${data.customerUuid}" data-contract="${data.contractUuid}" data-project="${projectuuid}" type="button" title="Gia hạn HĐ" class="btn btn-sm btn-success btn-icon btn-icon-md js-add-contract">
                                <i class="flaticon2-paper"></i>
                           </button>

                                                     
                        `;
                            },
                        }

                    ],
                    sortable: false,
                    pagination: true,
                    responsive: true,
                });
            }
        }

        

    });



    return {
        // Public functions
        init: function () {
            initValidation();
            initValidation1();
            initEventListeners();
            initSearch();
            location();

        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-project').length) {

        DMMSProjectCus.init();
    }
});
