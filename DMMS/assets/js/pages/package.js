"use strict";

// Class definition

var DMMSProjectCus = function () {
    // Private functions
    var dataTableInstancepackage = null;
    var drawer = null;
    var LIMIT = 20;
    var START_PAGE = 0;
    var CURRENT_PAGE = 1;

    //Dánh sách lưu các id được seclect
    // demo initializer

    //var reloadTable = function () {
    //    if (dataTableInstance) {
    //        dataTableInstance.reload();
    //    }
    //};
    //var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
    //if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
    //    $.notify(editSucessCache.massage, { type: 'success' });
    //    localStorage.removeItem('edit_success');
    //}
    function subTableInit(e) {
        $('<div/>').attr('id', 'child_data_ajax_' + e.data.uuid).appendTo(e.detailCell).KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/project/ProjectGetPacSub',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            if (raw.items.length > 0) {
                                (raw.items).forEach(function (e) {




                                })
                            }
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: pagination.limit || LIMIT,
                                    page: pagination.page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword } = query;
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: keyword || '',
                            package_uuid: e.data.uuid
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            // layout definition
            layout: {
                scroll: false,
                //height: 300,
                footer: false,

                // enable/disable datatable spinner.
                spinner: {
                    type: 1,
                    theme: 'default',
                },
            },

            sortable: false,
            pagination: false,
            responsive: true,
            // columns definition
            columns: [
                {
                    field: 'name',
                    title: 'Tên thiết bị',
                    template: function (data) {
                        var prefix = data.prefixName;
                        if (prefix == null) {
                            prefix = "";
                        }
                        if (data.name) {
                            if (data)
                                return `<span>${prefix}  ${data.name}</span>`
                        }
                    }
                },
                {
                    field: 'timeMaintain',
                    title: 'Thời hạn bảo hành',
                    width: 130,
                    template: function (data) {
                        var n = data.timeMaintain
                        if (n) {
                            return `<span>${n}  tháng</span>`;
                        }
                        return `Chưa rõ`;
                    },
                    textAlign: 'center'
                },

                {
                    field: 'count',
                    title: 'Số lượng',
                    width: 70,
                    template: function (data) {
                        return data.count < 10 ? `<span>${data.count} thiết bị</span>` : `<span>${data.count} thiết bị</span>`;
                    },
                },

                //{
                //    field: 'model',
                //    title: 'Model',
                //    width:200

                //},
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 50,
                    overflow: 'visible',
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        return `
                            <button data-id="${data.uuid}" title="Chi tiết" class="js-delete-task btn btn-sm btn-danger btn-icon btn-icon-md">
                                <i class="la la-trash"></i>
                            </button>
                        `;
                    },
                }],
        });
    }
    var initSearch = function () {
        $('#js-filter-packagea')
            .on('change', function () {
                if (dataTableInstancepackage) {
                    dataTableInstancepackage.search($(this).val().toLowerCase(), 'packageCodeId');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-keywordpackage')
            .on('change', function () {
                if (dataTableInstancepackage) {
                    dataTableInstancepackage.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });


    };
    var dataCustom = null;
    var initProjectDrawer = function (id, categories) {
        const title = 'Tạo phòng triển khai mới cho dự án'
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--packageinproject' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/project/addpackage/${id}` : '',
                method: 'GET',
            })
                .done(function (response) {

                    const $content = drawer.setBody(response);
                    $('#test').append(`<input name="projectUuid" value="${id}" hidden />`);

                    dataCustom = response ? categories.data.items : null;
                    $('.addinputpac').click(function (e) {

                        e.preventDefault();

                        initControlSelect(dataCustom)
                        $('.controlSelect1').selectpicker('refresh');
                    });

                    $('#test').on("click", ".deleteinput", function (e) {
                        //user click on remove text
                        e.preventDefault();
                        $(this).parent('div').parent('div').remove();
                        $('.bootstrap-select').selectpicker('refresh');
                    });
                    initContent($content);

                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();

    };
    //var initProjectPackageDrawer = function (id, categories, uuidpackage, codepackage) {
    var initProjectPackageDrawer = function (id, uuidpackage, codepackage) {
        const title = 'Thêm gói thiết bị cho sản phẩm'
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--packagedetailinproject' });
        drawer.on('ready', () => {
            $.ajax({
                url: `/project/PackageDetail`,
                method: 'GET',
                data: {
                    packageCodeId: codepackage,
                    package_uuid: uuidpackage,
                    project_uuid: id
                }
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    $('#test').append(`<input name="projectUuid" value="${id}" hidden />`);

                    //dataCustom = response ? categories.data.items : null;

                    //$('.addinputpac').click(function (e) {

                    //    e.preventDefault();

                    //    initControlSelect(dataCustom)
                    //    $('.controlSelect1').selectpicker('refresh');
                    //});

                    //$('#test').on("click", ".deleteinput", function (e) {
                    //    e.preventDefault();
                    //    $(this).parent('div').parent('div').remove();
                    //    $('.bootstrap-select').selectpicker('refresh');
                    //});
                    initContent($content);

                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();

    };
    var initProjectUpdatePackageDrawer = function (id, uuidpackage, codepackage, packageuuid, name) {
        debugger
        const title = 'Cập nhật gói thiết bị cho sản phẩm'
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--packagedetailinproject' });
        drawer.on('ready', () => {
            $.ajax({
                url: `/project/EditPackage`,
                method: 'GET',
                data: {
                    packageCodeId: codepackage,
                    package_uuid: uuidpackage,
                    project_uuid: id,
                    name: name
                }
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    $('#test').append(`<input name="projectUuid" value="${id}" hidden />`);
                    $('#test').append(`<input class="packageuuid" value="${uuidpackage}" hidden />`);
                    $('.setvalpackage').val(codepackage);
                    $('.setvalpackage').selectpicker('refresh')
                    $.ajax({
                        url: `/project/projectgetpacSub/`,
                        method: 'POST',
                        data: { package_uuid: uuidpackage, limit: -1 }


                    })

                        .done(function (data) {
                            initContent1($content, data, codepackage, packageuuid);
                            var timetest = $('#js-select-date_expire').val();
                            $('.controlSelect').on('change', function () {
                                debugger
                                $('.filldata').val(timetest);
                                $('.filldata').selectpicker('refresh');
                            });
                        })
                        .fail(function () {

                        });
                    //dataCustom = response ? categories.data.items : null;

                    //$('.addinputpac').click(function (e) {

                    //    e.preventDefault();

                    //    initControlSelect(dataCustom)
                    //    $('.controlSelect1').selectpicker('refresh');
                    //});

                    //$('#test').on("click", ".deleteinput", function (e) {
                    //    e.preventDefault();
                    //    $(this).parent('div').parent('div').remove();
                    //    $('.bootstrap-select').selectpicker('refresh');
                    //});


                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();

    };
    var initControlSelect = function (data) {
        //if (data != null && data.length > 0) {
        //Thực hiện lấy danh sách các gói đã select

        var lstItem = $(".controlSelect"),
            arrItem = [],
            //Dánh sách lưu các id được seclect
            lstItemResult = [];//tạo mảng danh sách mới



        for (var i = 0; i < lstItem.length; i++) {
            var valueItem = $(lstItem[i]).val();
            if (valueItem != "") {
                arrItem.push(valueItem);

            }
        }

        //Danh sách mới các item sẽ không có các giá trị selected
        //Thực hiện lọc lại danh sách
        //Sử dụng findler để lọc

        data.forEach(function (item) {

            var countSelected = arrItem.length;
            arrItem.forEach(function (i, e) {
                if (item.uuid != i) {
                    countSelected--;
                }
                if (countSelected == 0) {
                    lstItemResult.push(item);
                }
            })


        })

        var select = `
                    <div class="row">
                         <div class="form-group col-6">
                                    <select data-live-search="true" name = "packageCategories[][categoryId]" class="form-control controlSelect controlSelect1">  <option value="">--- Chọn loại thiết bị ---</option>`;
        if (lstItemResult == null && lstItemResult.length == 0) {

            select += '<option value=""></option>';
        }
        else {
            //dataCustom = lstItemResult;
            $(lstItemResult).each(function (index, item) {
                select += '<option value="' + item.id + '">' + item.name + '</option>';
            });
        }
        var html = `
            
            </select></div>
            <div class="form-group col-4">
                <input type="number" name="packageCategories[][count]" class="form-control" placeholder="Số lượng" />
            </div>
         <div class="form-group col-2">
                <i class="btn btn-danger kt-ml-10 la la-minus deleteinput"></i>
           </div>
            </div>
            </div>`;
        $('#test').prepend(select + html);

        $('.controlSelect').unbind('change');
        $('.controlSelect')
            .bind('change', function () {
                $('.controlSelect1').selectpicker('refresh');

                var lstItem1 = $(".controlSelect"),
                    arrItem1 = [];
                for (var i = 0; i < lstItem1.length; i++) {
                    var valueItem = $(lstItem1[i]).val();
                    if (valueItem != "") {
                        arrItem1.push(valueItem);

                    }
                }
                var abcd = $(this).val()
                var az = arrItem1.filter(item => item == abcd);
                if (az != "" && az.length > 1) {
                    $.notify('Bạn đã chọn thiết bị này rồi. Vui lòng chọn lại!', { type: 'danger' });
                    $(this).val("");

                    $('.controlSelect').selectpicker('refresh');
                }

            });
        //}

    };



    //var initPackageDrawer = function (id) {

    //    const title = 'Cập nhật gói thiết bị cho phòng mới'
    //    drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--cusproject' });

    //    drawer.on('ready', () => {
    //        $.ajax({
    //            url: id ? `/project/EditPackage/${id}` : '',
    //            method: 'GET',

    //        })
    //            .done(function (response) {
    //                const $content = drawer.setBody(response);
    //                var html = `<div class="form-group">

    //        <div class="kt-input-icon form-group row">
    //            <input name="total[][name]" class="form-control col-5" placeholder="Loại thiết bị" />
    //            <input name="total[][model]" class="form-control col-3 kt-ml-10" placeholder="Model" />
    //            <input type="number" name="total[][tota]" class="form-control col-2 kt-ml-10" placeholder="Số lượng" />
    //            <i class="btn btn-danger kt-ml-10 la la-minus deleteinput"></i>        
    //        </div>
    //    </div>`;

    //                    $('.addinput').click(function (e) {

    //                        e.preventDefault();
    //                        $('#test').append(html);
    //                    });

    //                $('.test').on("click", ".deleteinput", function (e) { //user click on remove text
    //                    e.preventDefault();
    //                    $(this).parent('div').remove();
    //                });
    //                initContent($content);

    //            })
    //            .fail(function () {
    //                drawer.error();
    //            });
    //    });

    //    drawer.open();

    //};

    var createPackageInProject = function (id) {
        const title = 'Tạo sản phẩm mới cho dự án'
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--packageinproject' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/project/addpackage/${id}` : '',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);

                    //Lựa chọn sản phẩm có sẵn
                    ChosePackage()


                    //$('#kt-select-customer1').unbind('change');
                    //$('#kt-select-customer1').bind('change', function () {
                    //    var project_uuid = $('#project_uuid').val()

                    //    $.ajax({
                    //        url: `/project/projectgetpac/${project_uuid}`,
                    //        method: 'GET',
                    //    })
                    //        .done(function (response) {
                    //            debugger
                    //        })
                    //        .fail(function (data) {

                    //        });
                    //    const findtext = $('.js-filter-packagelst').find('option:selected').text();
                    //    if ($('.js-filter-packagelst').find('option:selected').val() == '') {
                    //        $('.findname').val('');
                    //    }
                    //    else {
                    //        $('.findname').val(findtext);
                    //    }
                    //});
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    }
    var createPackageImportProject = function (id) {
        const title = 'Tạo sản phẩm mới cho dự án'
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-project-package__form' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/project/ImportPackage/${id}` : '',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    }
    var initEventListeners = function () {
        $('body')
            //.off('click', '.tongquan10')
            //.on('click', '.tongquan10', function () {
            //    const id = document.getElementById("uuid").value;

            //    $.ajax({
            //        url: "/TreeCategories/SearchLeaf",
            //        method: 'GET',
            //        data: { category_type:1,limit:-1}
            //    })
            //        .done(function (data) {
            //            const categories = data;
            //            initProjectDrawer(id, categories || '', categories);

            //        })
            //        .fail(function () {
            //            $.notify('Có lỗi xảy ra !', { type: 'danger' });
            //        });
            //})
            .off('click', '.tongquan10')
            .on('click', '.tongquan10', function () {
                $(this).addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

                const id = $('#project_uuid').val(); //uuid của project

                createPackageInProject(id);
                $(this).removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            })
            .off('click', '#ImportPackage')
            .on('click', '#ImportPackage', function () {
                const id = $('#project_uuid').val(); //uuid của project

                createPackageImportProject(id);
            })
            .off('click', '.js-edit-package')
            .on('click', '.js-edit-package', function () {
                const $this = $(this);
                const uuidpackage = $this.attr('data-id');
                const codepackage = $this.attr('code-id');
                const id = document.getElementById("project_uuid").value;

                initProjectPackageDrawer(id, uuidpackage, codepackage);

                //$.ajax({
                //    url: "/TreeCategories/SearchLeaf",
                //    data: { category_type: 1, limit: -1 }
                //})
                //    .done(function (data) {
                //        const categories = data;
                //        initProjectPackageDrawer(id, categories || '', uuidpackage, codepackage);

                //    })
                //    .fail(function () {
                //        $.notify('Có lỗi xảy ra !', { type: 'danger' });
                //    });
            })
            .off('click', '.js-uppate-package')
            .on('click', '.js-update-package', function () {
                $(this).attr('disabled', true);

                const $this = $(this);
                const uuidpackage = $this.attr('data-id');
                const codepackage = $this.attr('package-id');
                const name = $this.attr('data-name');
                const packageuuid = $this.attr('uuid');
                const id = document.getElementById("project_uuid").value;

                initProjectUpdatePackageDrawer(id, uuidpackage, codepackage, packageuuid, name);
                $(this).attr('disabled', false);

                //$.ajax({
                //    url: "/TreeCategories/SearchLeaf",
                //    data: { category_type: 1, limit: -1 }
                //})
                //    .done(function (data) {
                //        const categories = data;
                //        initProjectPackageDrawer(id, categories || '', uuidpackage, codepackage);

                //    })
                //    .fail(function () {
                //        $.notify('Có lỗi xảy ra !', { type: 'danger' });
                //    });
            })
            .off('click', '.js-delete-package')
            .on('click', '.js-delete-package', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                var project_uuid = $('#project_uuid').val();
                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'sản phẩm sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deletepackage(id, project_uuid);
                    }
                });
            })
            .off('click', '.js-download-package')
            .on('click', '.js-download-package', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                var project_uuid = $('#project_uuid').val().val();
                $.ajax({
                    url: "/project/ExportPackage",
                    method: 'GET',
                    data: { id: id }
                })
                    .done(function (data) {
                        debugger
                        var a = window.origin + data
                        window.location = a;
                    })
                    .fail(function () {
                        $.notify('Có lỗi xảy ra !', { type: 'danger' });
                    });
            })


    };
    var deletepackage = function (id, project_uuid) {

        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa sản phẩm...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/project/DeletePackage/${id}`,
                method: 'DELETE',
                data: { projectUuid: project_uuid, packageUuid: id }
            })
                .done(function (data) {
                    debugger
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify("Xóa thành công!", { type: 'success' });
                    reloadTable();
                })
                .fail(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(data.responseJSON.msg, { type: 'danger' });
                });
        }
    };
    var initValidation2 = function () {
        $('.dmms-project-edit-package__form').validate({
            rules:
            {
                //'packageCategories[][count]': {
                //    required: true,
                //},
                packageCodeID: {
                    required: true,
                },
                name: {
                    required: true,
                },
                //'packageCategories[][categoryId]': {
                //    required: true,
                //}

            },
            messages: {
                //'packageCategories[][count]': {
                //    required: 'Vui lòng nhập số lượng.'
                //},

                packageCodeID: {
                    required: 'Vui lòng chọn mã phòng.'
                },
                name: {
                    required: 'Vui lòng nhập tên phòng'
                },
                //'packageCategories[][categoryId]': {
                //    required: 'Vui lòng chọn loại thiết bị'
                //}
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo('.dmms-project-edit-package__form', -200);
                $('#kt-select-customer1').on('hide.bs.select', function () {
                    debugger
                    $(this).trigger("focusout");
                    $('.findname').trigger("focusout");
                });

            },
            submitHandler: function (form) {
                $(form).find('button[type="submit"]').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
                const data = $(form).serializeJSON() || {};
                //const { uuid } = data;
                $.ajax({
                    url: `/project/EditPackage/`,
                    method: 'POST',
                    data: data,
                })
                    .done(function (data) {

                        //$(".submit").attr("disabled", true);
                        //KTApp.block('.dmms-project-customer__form');
                        const successMsg = 'Cập nhật thành công.';
                        $.notify(successMsg, { type: 'success' });
                        drawer.close();
                        reloadTable();
                        /**
                         * Close drawer
                         */


                        //reloadTable();
                        //localStorage['edit_success'] = JSON.stringify({ from: (`/project/addcustomer/`), massage: successMsg });

                        //window.location.replace("customer");
                        //window.location = window.location.origin + "/project";
                    })
                    .fail(function (data) {
                        $(form).find('button[type="submit"]').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        KTApp.unblock('.dmms-project-edit-package__form');
                        const errorMsg = data.responseJSON.msg + '!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }
    var initValidation = function () {
        $('.dmms-project-package__form').validate({
            rules:
            {
                //'packageCategories[][count]': {
                //    required: true,
                //},
                packageCodeID: {
                    required: true,
                },
                name: {
                    required: true,
                },
                //'packageCategories[][categoryId]': {
                //    required: true,
                //}

            },
            messages: {
                //'packageCategories[][count]': {
                //    required: 'Vui lòng nhập số lượng.'
                //},

                packageCodeID: {
                    required: 'Vui lòng chọn mã phòng.'
                },
                name: {
                    required: 'Vui lòng nhập tên phòng'
                },
                //'packageCategories[][categoryId]': {
                //    required: 'Vui lòng chọn loại thiết bị'
                //}
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo('.dmms-project-package__form', -200);
                $('#kt-select-customer1').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                    $('.findname').trigger("focusout");
                });
            },
            submitHandler: function (form) {
                $(form).find('button[type="submit"]').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

                const data = $(form).serializeJSON() || {};
                //const { uuid } = data;
                $.ajax({
                    url: `/project/addpackage/`,
                    method: 'POST',
                    data: data,
                })
                    .done(function (data) {

                        //$(".submit").attr("disabled", true);
                        //KTApp.block('.dmms-project-customer__form');
                        const successMsg = 'Thêm mới thành công.';
                        $.notify(successMsg, { type: 'success' });
                        drawer.close();
                        reloadTable();
                        /**
                         * Close drawer
                         */


                        //reloadTable();
                        //localStorage['edit_success'] = JSON.stringify({ from: (`/project/addcustomer/`), massage: successMsg });

                        //window.location.replace("customer");
                        //window.location = window.location.origin + "/project";
                    })
                    .fail(function (data) {
                        $(form).find('button[type="submit"]').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        KTApp.unblock('.dmms-project-package__form');
                        const errorMsg = data.responseJSON.msg + '!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }
    var initValidationimport = function () {
        $('.dmms-project-importpackage__form').validate({
            rules:
            {
                //'packageCategories[][count]': {
                //    required: true,
                //},
                packageCodeID: {
                    required: true,
                },
                name: {
                    required: true,
                },
                //'packageCategories[][categoryId]': {
                //    required: true,
                //}

            },
            messages: {
                //'packageCategories[][count]': {
                //    required: 'Vui lòng nhập số lượng.'
                //},

                packageCodeID: {
                    required: 'Vui lòng chọn mã phòng.'
                },
                name: {
                    required: 'Vui lòng nhập tên phòng'
                },
                //'packageCategories[][categoryId]': {
                //    required: 'Vui lòng chọn loại thiết bị'
                //}
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo('.dmms-project-importpackage__form', -200);
                $('#kt-select-customer1').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });
            },
            submitHandler: function (form) {
                debugger
                var object = {};
                var data = new FormData(form);
                var file = event.target.elements[5].files;
                data.append('FileUploadExcel', $('input[type=file]')[0].files[0]);
                data.forEach((value, key) => { object[key] = value });
                var json = JSON.stringify(object);
                console.log(json);
                var fileOther = $('#FileUploadExcel')[0];
                if (fileOther != undefined) {
                    var file = fileOther.files[0];
                    if (file != undefined) {
                        data.append("FileUploadExcel", file);
                    }
                }
                data.append('jsonString', $('.dmms-project-importpackage__form').serializeJSON);
                //const { uuid } = data;
                $.ajax({
                    url: `/project/ImportPackage/`,
                    method: 'POST',
                    data: data,
                    cache: false,
                    dataType: 'json',
                    processData: false, // Don't process the files
                    contentType: false,
                })
                    .done(function (data) {

                        //$(".submit").attr("disabled", true);
                        //KTApp.block('.dmms-project-customer__form');
                        const successMsg = 'Thêm mới thành công.';
                        $.notify(successMsg, { type: 'success' });
                        drawer.close();
                        reloadTable();
                        /**
                         * Close drawer
                         */


                        //reloadTable();
                        //localStorage['edit_success'] = JSON.stringify({ from: (`/project/addcustomer/`), massage: successMsg });

                        //window.location.replace("customer");
                        //window.location = window.location.origin + "/project";
                    })
                    .fail(function () {

                        KTApp.unblock('.dmms-project-importpackage__form');
                        const errorMsg = 'Thêm không thành công.';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }
    var initValidation1 = function ($form1) {
        $('.dmms-project-detail-package__form').validate({
            rules:
            {
                name: {
                    required: true,
                },
                //description: {
                //    required: true,
                //},

            },
            messages: {
                name: {
                    required: 'Vui lòng nhập.'
                },

                //description: {
                //    required: 'Vui lòng nh?p mô t?.'
                //}
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo('.dmms-project-detail-package__form', -200);
            },
            submitHandler: function (form) {
                debugger
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                $.ajax({
                    url: `/project/PackageDetail/`,
                    method: 'POST',
                    data,


                })

                    .done(function (data) {

                        //$(".submit").attr("disabled", true);
                        //KTApp.block('.dmms-project-customer__form');
                        const successMsg = 'Cập nhật thành công.';
                        $.notify(successMsg, { type: 'success' });
                        drawer.close();
                        reloadTable();
                        /**
                         * Close drawer
                         */


                        //reloadTable();
                        //localStorage['edit_success'] = JSON.stringify({ from: (`/project/addcustomer/`), massage: successMsg });

                        //window.location.replace("customer");
                        //window.location = window.location.origin + "/project";
                    })
                    .fail(function () {
                        KTApp.unblock('.dmms-project-detail-package__form');
                        const errorMsg = 'Cập nhật không thành công';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }
    var initForm = function ($form) {
        initValidation(true);
    }
    var initFormimport = function ($formimport) {
        initValidationimport(true);
    }
    var initForm2 = function ($form2) {
        initValidation2(true);
    }

    var initForm1 = function ($form1) {
        initValidation1(true);
    }
    var reloadTable = function () {
        if (dataTableInstancepackage) {
            dataTableInstancepackage.reload();
        }
    };
    var initContent = function ($content) {
        /*
            Init form
        */
        $("#js-select-date_expire1").change(function () {
            debugger
            if ($(this).val() == "") {
                $('.checkpackage').remove();
                $('.appendpackage').append(`<div class="kt-input-icon">
                <input name="packageCategories[][timeMaintain]" class="form-control" id="js-select-date_expire1" placeholder="Thời hạn BH" type="number" />
            </div>`);

            }


        });
        const $form = $content.find('.dmms-project-package__form');
        const $form1 = $content.find('.dmms-project-detail-package__form');
        const $formimport = $content.find('.dmms-project-importpackage__form');
        //const $form1 = $content.find('.dmms-project-edit-package__form');

        initForm($form);
        initForm1($form1);
        initFormimport($formimport);
        /*
            init auto-resize
        */
        $('#js-filter-category').selectpicker();
        $('#kt-select-customer1').selectpicker();

        autosize($content.find('textarea.auto-resize'));
        var timetest = $('#js-select-date_expire').val();
        $('.controlSelectcus').on('change', function () {
            $('.filldata').val(timetest);
            $('.filldata').selectpicker('refresh');
        });
        $('.controlSelect1').selectpicker();
        $('.js-filter-packagelst').selectpicker();
        $('#js-select-date_expire1').selectpicker();
        $('#kt-select-supplies').selectpicker({ showSubtext: true });
        //$('.selectpicker').select2();





        //+ thêm mới vật tư sau khi click
        const wrapper = $('.wrapper-fill');
        //var index = 1;

        $('#js-add-importsupplies').on('click', function () {
            const supplies = $('#kt-select-supplies');
            const count_supplies = $('#count_supplies');
            const date_expire = $('#js-select-date_expire1');
            const category_id = supplies.val();
            const category_name = supplies.find('option:selected').text();

            if (category_id != null && category_id != "") {
                var index = parseInt($(wrapper.find('tr')[wrapper.find('tr').length - 1]).find('.stt').text()) + 1;

                wrapper.append(`<tr id="${category_id}">
                            <td class="stt">${index}</td>
                            <td>
                                <input name="packageCategories[][categoryId]" class="test_${category_id}" value="${category_id}" hidden>${category_name}
                            </td>
                            <td><input class="form-control" type="number" min="1" name="packageCategories[][count]" placeholder="Nhập" value="${count_supplies.val()}" /></td>
                            <td>
                                <input name="packageCategories[][timeMaintain]" class="form-control" value="${date_expire.val()}" placeholder="Thời hạn BH" type="number" />   
                            </td>
                            <td><i class="la la-trash remove_field" data-category="${category_id}" style="background-color: #FF5F6D"></i></td>
                        </tr>`);
                $(`#js-select-date_expire1_${category_id}`).selectpicker('val', date_expire.val()).attr('title', 'Thời gian bảo hành');

                $('#kt-select-supplies').find(`option[value="${category_id}"]`).attr('disabled', true); //Disabled option của chọn vật tư
                $('#kt-select-supplies').selectpicker('refresh');

                supplies.selectpicker('val', '');
                count_supplies.val('');
                date_expire.selectpicker('val', '');
                date_expire.val('');

            }
            else {
                $.notify('Vui lòng chọn thiết bị!', { type: 'danger' });
            }

            //addSuppliesInCreatePackage()
            $(wrapper).off('click').on("click", `.remove_field`, function (e) { //user click on remove text
                e.preventDefault();
                const $this = $(this);
                const category_id = $this.attr('data-category')

                $this.parent('td').parent(`tr[id="${category_id}"]`).remove();
                $('#kt-select-supplies').find(`option[value="${category_id}"]`).attr('disabled', false); //Disabled option của chọn vật tư
                $('#kt-select-supplies').selectpicker('refresh');

                for (var i = 0; i < wrapper.find('tr').length; i++) {
                    $(wrapper.find('tr .stt')[i]).html(i + 1);
                }
            });
        })
        $('.kt-close').on('click', function () {
            drawer.close();
        });

    };
    var initContent1 = function ($content, data, codepackage, packageuuid) {
        $("#js-select-date_expire1").change(function () {
            if ($(this).val() == "") {
                $('.checkpackage').remove();
                $('.appendpackage').append(`<div class="kt-input-icon">
                <input name="packageCategories[][timeMaintain]" class="form-control" id="js-select-date_expire1" placeholder="Thời hạn BH" type="number" />
            </div>`);

            }


        });
        console.log(data.data.items);

        /*
            Init form
        */
        const $form2 = $content.find('.dmms-project-edit-package__form');

        initForm2($form2);
        /*
            init auto-resize
        */
        $('#js-filter-category').selectpicker();
        $('.setvalpackage').selectpicker();
        $('#kt-select-customer1').selectpicker();
        $('.setvalpackage').val(codepackage);
        $('.setvalpackage').selectpicker('refresh');
        autosize($content.find('textarea.auto-resize'));

        $('.controlSelect1').selectpicker();
        $('.js-filter-packagelst').selectpicker();
        $('#js-select-date_expire1').selectpicker();
        $('#kt-select-supplies').selectpicker({ showSubtext: true });
        //$('.selectpicker').select2();

        //$('.setvalpackage').on('change', function (data) {
        //    debugger
        //    const findtext = $('.setvalpackage').find('option:selected').text();
        //    if ($('.setvalpackage').find('option:selected').val() == '') {
        //        $('.findname').val('');
        //        $('#packageCodeID').val('');
        //        $('#packageCodeID').selectpicker('refresh');

        //    }
        //    else {
        //        $('#packageCodeID').val($('.setvalpackage').find('option:selected').val());
        //        $('#packageCodeID').selectpicker('refresh');
        //        $('.findname').val(findtext);
        //    }

        //})


        //Lựa chọn sản phẩm có sẵn
        if (codepackage != '') {
            $.ajax({
                url: `/package/gethint`,
                data: { packageCodeId: codepackage, limit: -1 }
            })
                .done(function (data) {
                    if (data.data.items.packages != null) {
                        $('.findname_autocomplete').append(`<li class="kt-font-bold findname_autocomplete_items" data-uuid="" data-id="">+ Thêm mới</li><br>`);
                        $.each(data.data.items.packages, function (index, item) {
                            $('.findname_autocomplete').append(`<li class="findname_autocomplete_items" data-uuid="${item.uuid}" data-id="${item.id}">${item.name}</li><br>`);
                        });
                    }
                    else {
                        $('.findname_autocomplete').append(`<li class="kt-font-bold findname_autocomplete_items" data-uuid="" data-id="">+ Thêm mới</li><br>`);
                    }

                    $('.findname_autocomplete_items').on('click', function (e) {
                        e.stopPropagation();
                        const findtext = $(this).text();
                        const finduuid = $(this).attr('data-uuid');
                        const findid = $(this).attr('data-id');

                        //Xóa dữ liệu sản phẩm khi thay đổi phòng ban
                        $('#kt-select-supplies').find('option[disabled="disabled"]').removeAttr('disabled');
                        $('#kt-select-supplies').selectpicker('refresh');
                        wrapper.find('tr.display-default').siblings().remove();

                        if (finduuid != '') {
                            $('.findname').prop('readonly', true).val(findtext);
                            //$('.finduuid').val(finduuid);
                            $('.findname_autocomplete').addClass('kt-hidden');
                        }
                        else {
                            $('.findname').val('');
                            //$('.finduuid').val('');
                            $('.findname').prop('readonly', false);
                            $('.findname_autocomplete').addClass('kt-hidden');
                        }
                        $('.findname').trigger("focusout");

                        if (findid != '') {
                            var index = 1;

                            if (data.data.items.categories != null && data.data.items.categories.filter(st => st.id == findid) != null) {
                                $.each(data.data.items.categories.filter(st => st.id == findid), function (ind, item) {
                                    index += 1;
                                    wrapper.append(`<tr id="${item.category_id}">
                                            <td class="stt">${index}</td>
                                            <td>
                                                <input name="packageCategories[][categoryId]" class="test_${item.category_id}" value="${item.category_id}" hidden>${item.category_name}
                                            </td>
                                            <td><input class="form-control" type="number" min="1" name="packageCategories[][count]" placeholder="Nhập" value="${item.count}" /></td>
                                            <td>
                                                <input name="packageCategories[][timeMaintain]" class="form-control" value="${item.time_maintain}" placeholder="Thời hạn BH" type="number" />   
                                            </td>
                                            <td><i class="la la-trash remove_field_items" data-category="${item.category_id}" style="background-color: #FF5F6D"></i></td>
                                        </tr>`);

                                    $('#kt-select-supplies').find(`option[value="${item.category_id}"]`).attr('disabled', true); //Disabled option của chọn vật tư
                                    $('#kt-select-supplies').selectpicker('refresh');
                                });
                            }

                            $(wrapper).on("click", `.remove_field_items`, function (e) { //user click on remove text
                                e.preventDefault();
                                const $this = $(this);
                                const category_id = $this.attr('data-category');
                                //index -= 1;
                                
                                $this.parent('td').parent(`tr[id="${category_id}"]`).remove();
                                $('#kt-select-supplies').find(`option[value="${category_id}"]`).attr('disabled', false); //Enabled option của chọn vật tư
                                $('#kt-select-supplies').selectpicker('refresh');

                                for (var i = 0; i < wrapper.find('tr').length; i++) {
                                    $(wrapper.find('tr .stt')[i]).html(i + 1);
                                }
                            });
                        }
                    });

                    $('.dmms-drawer__content, .dmms-drawer__header').off('click').on('click', function () {
                        $('.findname_autocomplete').addClass('kt-hidden');
                    });
                })
                .fail(function (data) {
                    $.notify('Lỗi hệ thống vui lòng thử lại!', { type: 'danger' });
                });

            $('.findname').off('click').on('click', function (e) {
                e.stopPropagation();
                $('.findname_autocomplete').removeClass('kt-hidden');
            });
        }

        ChosePackage(); //Lựa chọn sản phẩm có sẵn

        //+ thêm mới vật tư sau khi click
        const wrapper = $('.wrapper-fill');
        var index = 1;

        data.data.items.forEach(function (i, v) {
            index += 1;
            var spa = "-"
            wrapper.append(`<tr id="${i.id}">
                            <td class="stt">${index}</td>
                            <td>
                                <input name="packageCategories[][categoryId]" value="${i.id}" hidden>${i.prefixName + spa + i.name}
                            </td>
                            <td><input class="form-control" type="number" min="1" name="packageCategories[][count]" placeholder="Nhập" value="${i.count}" /></td>
                            <td>
                                <input name="packageCategories[][timeMaintain]" class="form-control" value="${i.timeMaintain}" placeholder="Thời hạn BH" type="number" />   
                            </td>
                            <td><i class="la la-trash remove_field" data-category="${i.id}" style="background-color: #FF5F6D"></i></td>
                        </tr>`);
            $(`#js-select-date_expire1_${i.id}`).selectpicker('val', i.timeMaintain).attr('title', 'Thời gian bảo hành');

            $('#kt-select-supplies').find(`option[value="${i.id}"]`).attr('disabled', true); //Disabled option của chọn vật tư
            $('#kt-select-supplies').selectpicker('refresh');

            $(wrapper).off('click').on("click", `.remove_field`, function (e) { //user click on remove text
                debugger;
                e.preventDefault();
                const $this = $(this);
                const id = $this.attr('data-category');
                //index -= 1;
                $this.parent('td').parent(`tr[id="${id}"]`).remove();
                $('#kt-select-supplies').find(`option[value="${id}"]`).attr('disabled', false); //Disabled option của chọn vật tư
                $('#kt-select-supplies').selectpicker('refresh');

                for (var i = 0; i < wrapper.find('tr').length; i++) {
                    $(wrapper.find('tr .stt')[i]).html(i + 1);
                }
            });
        })

        $('#js-add-importsupplies').on('click', function () {
            const supplies = $('#kt-select-supplies');
            const count_supplies = $('#count_supplies');
            const date_expire = $('#js-select-date_expire1');
            const category_id = supplies.val();
            const category_name = supplies.find('option:selected').text();

            if (category_id != null && category_id != "") {
                var index_add = parseInt($(wrapper.find('tr')[wrapper.find('tr').length - 1]).find('.stt').text()) + 1;

                wrapper.append(`<tr id="${category_id}">
                            <td class="stt">${index_add}</td>
                            <td>
                                <input name="packageCategories[][categoryId]" value="${category_id}" hidden>${category_name}
                            </td>
                            <td><input class="form-control" type="number" min="1" name="packageCategories[][count]" placeholder="Nhập" value="${count_supplies.val()}" /></td>
                            <td>
                                <input name="packageCategories[][timeMaintain]" class="form-control" value="${date_expire.val()}" placeholder="Thời hạn BH" type="number" />   
                            </td>
                            <td><i class="la la-trash remove_field" data-category="${category_id}" style="background-color: #FF5F6D"></i></td>
                        </tr>`);

                $(`#js-select-date_expire1_${category_id}`).selectpicker('val', date_expire.val()).attr('title', 'Thời gian bảo hành');

                $('#kt-select-supplies').find(`option[value="${category_id}"]`).attr('disabled', true); //Disabled option của chọn vật tư
                $('#kt-select-supplies').selectpicker('refresh');

                supplies.selectpicker('val', '');
                count_supplies.val('');
                date_expire.selectpicker('val', '');
                date_expire.val('');
            }
            else {
                $.notify('Vui lòng chọn thiết bị!', { type: 'danger' });
            }

            //addSuppliesInCreatePackage()
            $(wrapper).off('click').on("click", `.remove_field`, function (e) { //user click on remove text
                e.preventDefault();
                const $this = $(this);
                const category_id = $this.attr('data-category');
                //index -= 1;
                $this.parent('td').parent(`tr[id=${category_id}]`).remove();
                $('#kt-select-supplies').find(`option[value="${category_id}"]`).attr('disabled', false); //Disabled option của chọn vật tư
                $('#kt-select-supplies').selectpicker('refresh');

                for (var i = 0; i < wrapper.find('tr').length; i++) {
                    $(wrapper.find('tr .stt')[i]).html(i + 1);
                }
            });
        })
        $('.kt-close').on('click', function () {
            drawer.close();
        });
    };
    $(".package").on("click", function () {
        if (!$('.nav-item').find('.active').hasClass("package")) {
            var project_uuid = document.getElementById("project_uuid").value;
            if (dataTableInstancepackage != null) {
                dataTableInstancepackage.load();
            }
            else {

                dataTableInstancepackage = $('.kt-datatablepackage').KTDatatable({
                    data: {
                        type: 'remote',
                        source: {
                            read: {
                                url: `/project/projectgetpac/${project_uuid}`,
                            },
                            response: {
                                map: function (res) {
                                    const raw = res && res.data || {};
                                    const { pagination } = raw;
                                    const perpage = pagination === undefined ? undefined : pagination.perpage;
                                    const page = pagination === undefined ? undefined : pagination.page;
                                    START_PAGE = 0;
                                    return {
                                        data: raw.items || [],
                                        meta: {
                                            ...pagination || {},
                                            perpage: perpage || LIMIT,
                                            page: page || 1,
                                        },
                                    }
                                },

                            },
                            filter: function (data = {}) {

                                const query = data.query || {};
                                const pagination = data.pagination || {};
                                const { packageCodeId, creator_uuid, state, keyword, priority } = query;
                                if (START_PAGE === 1) {
                                    pagination.page = START_PAGE;
                                }
                                return {

                                    limit: pagination.perpage || LIMIT,
                                    packageCodeId: packageCodeId || $('#js-filter-packagea').val(),
                                    creator_uuid: creator_uuid || $('#js-filter-creator').val(),
                                    state: state || $('#js-filter-state').val(),
                                    priority: priority || $('#js-filter-priority').val(),
                                    keyword: (keyword || $('#js-filter-keywordpackage').val()).trim(),
                                    page: pagination.page
                                };
                            }
                        },
                        pageSize: LIMIT,
                        serverPaging: true,
                        serverFiltering: true,
                    },
                    layout: {
                        scroll: true
                    },
                    search: {
                        onEnter: true,
                    },
                    detail: {
                        title: 'Đang tải dữ liệu ...',
                        content: subTableInit,
                    },
                    columns: [
                        {
                            field: 'uuid',
                            title: '#',
                            sortable: false,
                            textAlign: 'center',
                            width: 20,
                            //template: function (data, type, row) {
                            //    return row.index() + 1;
                            //}
                        },
                        {
                            field: 'packageCode',
                            title: 'Mã phòng',
                            width: 110
                        },
                        {
                            field: 'name',
                            title: 'Tên sản phẩm',

                        },
                        {
                            field: 'countDevices',
                            title: 'Tổng số',
                            width: 80,
                            textAlign: 'center',
                            template: function (data) {

                                if (data.countDevices != 0) {
                                    var a = data.countDevices
                                    return `<p>${a} Thiết bị</p>`
                                }
                                return `<p style="color: red;">Chưa có thiết bị</p>`

                            }
                        },
                        {
                            field: 'Tác vụ',
                            title: 'Tác vụ',
                            sortable: false,
                            width: 110,
                            autoHide: false,
                            textAlign: 'center',
                            template: function (data) {
                                return `
                            <button data-id="${data.uuid}" package-id="${data.packageCodeID}" data-name="${data.name}" type="button" title="Chỉnh sửa" class="js-update-package btn btn-sm btn-primary btn-icon btn-icon-md">
                             <i class="la la-edit"></i>
                            </button>
                            <button data-id="${data.uuid}" type="button" title="Xóa" class="js-delete-package btn btn-sm btn-danger btn-icon btn-icon-md">
                                <i class="la la-trash"></i>
                            </button>
                             <button data-id="${data.uuid}" code-id="${data.packageCodeID}" type="button" title="Tải Excel" class="js-download-package btn btn-sm btn-warning btn-icon btn-icon-md">
                                <i class="la la-download"></i>
                            </button>
                        `;
                            },
                        }],
                    sortable: false,
                    pagination: true,
                    responsive: true,

                });
            }

        }
    });

    var ChosePackage = function () {
        $('#kt-select-customer1').on('change', function () {
            const $this = $(this);
            const packageCodeId = $this.val();

            //Reset dữ liệu tên sản phẩm được gợi ý
            $('.findname_autocomplete').addClass('kt-hidden').html('');
            $('.findname').prop('readonly', true).val('');
            $('.finduuid').val('');

            //Xóa dữ liệu sản phẩm khi thay đổi phòng ban
            const wrapper = $('.wrapper-fill');

            $('#kt-select-supplies').find('option[disabled="disabled"]').removeAttr('disabled');
            $('#kt-select-supplies').selectpicker('refresh');
            wrapper.find('tr.display-default').siblings().remove();

            if (packageCodeId != '') {
                //Lấy ra các gợi ý cho sản phẩm
                $.ajax({
                    url: '/package/gethint',
                    data: { packageCodeId: packageCodeId, limit: -1 }
                })
                    .done(function (data) {
                        if (data.data.items.packages != null) {
                            $('.findname_autocomplete').append(`<li class="kt-font-bold findname_autocomplete_items" data-uuid="" data-id="">+ Thêm mới</li><br>`);
                            $.each(data.data.items.packages, function (index, item) {
                                $('.findname_autocomplete').append(`<li class="findname_autocomplete_items" data-uuid="${item.uuid}" data-id="${item.id}">${item.name}</li><br>`);
                            });
                        }
                        else {
                            $('.findname_autocomplete').append(`<li class="kt-font-bold findname_autocomplete_items" data-uuid="" data-id="">+ Thêm mới</li><br>`);
                        }

                        //Sự kiện thay đổi tên sản phẩm
                        $('.findname_autocomplete_items').on('click', function (e) {
                            e.stopPropagation();
                            const findtext = $(this).text();
                            const finduuid = $(this).attr('data-uuid');
                            const findid = $(this).attr('data-id');

                            //Xóa dữ liệu sản phẩm khi thay đổi phòng ban
                            $('#kt-select-supplies').find('option[disabled="disabled"]').removeAttr('disabled');
                            $('#kt-select-supplies').selectpicker('refresh');
                            wrapper.find('tr.display-default').siblings().remove();

                            //Fill dữ liệu
                            if (finduuid != '') {
                                $('.findname').prop('readonly', true).val(findtext);
                                //$('.finduuid').val(finduuid);
                                //$('.findid').val(findid);
                                $('.findname_autocomplete').addClass('kt-hidden');
                            }
                            else {
                                $('.findname').val('');
                                //$('.finduuid').val('');
                                //$('.findid').val('');
                                $('.findname').prop('readonly', false);
                                $('.findname_autocomplete').addClass('kt-hidden');
                            }
                            $('.findname').trigger("focusout");

                            //Lấy ra các thiết bị
                            if (findid != '') {
                                var index = 1;

                                if (data.data.items.categories != null && data.data.items.categories.filter(st => st.id == findid) != null) {
                                    $.each(data.data.items.categories.filter(st => st.id == findid), function (ind, item) {
                                        index += 1;
                                        wrapper.append(`<tr id="${item.category_id}">
                                            <td class="stt">${index}</td>
                                            <td>
                                                <input name="packageCategories[][categoryId]" class="test_${item.category_id}" value="${item.category_id}" hidden>${item.category_name}
                                            </td>
                                            <td><input class="form-control" type="number" min="1" name="packageCategories[][count]" placeholder="Nhập" value="${item.count}" /></td>
                                            <td>
                                                <input name="packageCategories[][timeMaintain]" class="form-control" value="${item.time_maintain}" placeholder="Thời hạn BH" type="number" />   
                                            </td>
                                            <td><i class="la la-trash remove_field" data-category="${item.category_id}" style="background-color: #FF5F6D"></i></td>
                                        </tr>`);

                                        $('#kt-select-supplies').find(`option[value="${item.category_id}"]`).attr('disabled', true); //Disabled option của chọn vật tư
                                        $('#kt-select-supplies').selectpicker('refresh');
                                    });
                                }

                                $(wrapper).off('click').on("click", `.remove_field`, function (e) { //user click on remove text
                                    e.preventDefault();
                                    const $this = $(this);
                                    const category_id = $this.attr('data-category');
                                    //index -= 1;

                                    $this.parent('td').parent(`tr[id="${category_id}"]`).remove();
                                    $('#kt-select-supplies').find(`option[value="${category_id}"]`).attr('disabled', false); //Enabled option của chọn vật tư
                                    $('#kt-select-supplies').selectpicker('refresh');

                                    for (var i = 0; i < wrapper.find('tr').length; i++) {
                                        $(wrapper.find('tr .stt')[i]).html(i + 1);
                                    }
                                });
                            }
                        });

                        $('.dmms-drawer__content, .dmms-drawer__header').off('click').on('click', function () {
                            $('.findname_autocomplete').addClass('kt-hidden');
                        });
                    })
                    .fail(function (data) {
                        $.notify('Lỗi hệ thống vui lòng thử lại!', { type: 'danger' });
                    });
            }

            $('.findname').off('click').on('click', function (e) {
                e.stopPropagation();
                if (packageCodeId != "")
                    $('.findname_autocomplete').removeClass('kt-hidden');
            });
        });
    };

    return {
        // Public functions
        init: function () {
            //initValidation();
            //initTable();
            initSearch();
            initEventListeners();
            $('#js-filter-category').selectpicker();



        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-project').length) {

        DMMSProjectCus.init();
    }
});