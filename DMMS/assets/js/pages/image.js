﻿"use strict";
// Class definition

var DMMSImage = function () {
    // Private functions

    var dataTableInstance = null;

    var drawer = null;

    var LIMIT = 20;

    // demo initializer
    var initTable = function () {
        dataTableInstance = $('.kt-datatable').KTDatatable({

            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/supplier/gets',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            console.log(res, raw, pagination);

                            //Gán các thuộc tính của images{0} vào đối tượng item
                            (raw.items).forEach(function (e) {

                                e.path = e.images.length > 0 ? e.images[0].path : null;
                                e.owner_uuid = e.images.length > 0 ? e.images[0].owner_uuid : null;
                                return e;


                            })

                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: pagination.perpage || LIMIT,
                                    page: pagination.page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword } = query;
                        return {
                            limit: pagination.perpage || LIMIT,
                            page: pagination.page,
                            keyword: keyword || '',

                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                input: $('#js-filter-keyword'),
            },

            columns: [
                {
                    field: 'name',
                    title: 'Tên nhà cung cấp',
                }, {
                    field: 'description',
                    title: 'Mô tả',
                },
                {
                    field: 'path',
                    title: 'Ảnh',
                    template: function (data) {
                        if (data.path != undefined && data.path !== '') {
                            return `
                            <img src="${data.path}" width="100px">
                        `;
                        }
                        return '';
                    },
                }, {
                    field: 'time_created',
                    title: 'Ngày tạo',
                }, {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 110,
                    overflow: 'visible',
                    autoHide: false,
                    template: function (data) {
                        return `
                            <button data-id="${data.uuid}" title="Chỉnh sửa" class="js-edit-supplier btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </button>
                            <button data-id="${data.uuid}" title="Xóa" class="js-delete-supplier btn btn-sm btn-danger btn-icon btn-icon-md ml-2">
                                <i class="la la-trash"></i>
                            </button>
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };

    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var deleteSupplier = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa nhà cung cấp...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/supplier/delete/${id}`,
                method: 'DELETE',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa nhà cung cấp thành công!', { type: 'success' });

                    reloadTable();
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa nhà cung cấp không thành công!', { type: 'danger' });
                });
        }
    };

    var initSupplierDrawer = function (id) {
        const title = id ? 'Sửa thông tin nhà cung cấp' : 'Thêm nhà cung cấp';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--supplier' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/supplier/edit/${id}` : '/supplier/create',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-supplier')
            .on('click', '.js-delete-supplier', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Nhà cung cấp sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteSupplier(id);
                    }
                });
            })
            .off('click', '.js-edit-supplier')
            .on('click', '.js-edit-supplier', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initSupplierDrawer(id || '');
            });

    };

    var initForm = function ($form) {
        $form.validate({
            rules: {
                name: {
                    required: true,
                },
                description: {
                    required: true,
                },

            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên nhà cung cấp.'
                },

                description: {
                    required: 'Vui lòng nhập mô tả.'
                }
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {

                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                console.log(data, uuid);
                $.ajax({
                    url: uuid ? `/supplier/edit/${uuid}` : 'supplier/create',
                    method: uuid ? 'PUT' : 'POST',
                    data,
                })
                    .done(function () {
                        KTApp.block($form);
                        const successMsg = uuid ? 'Sửa thông tin thành công!' : 'Thêm nhà cung cấp thành công!';
                        $.notify(successMsg, { type: 'success' });

                        /**
                         * Close drawer
                         */
                        drawer.close();

                        reloadTable();
                    })
                    .fail(function () {
                        KTApp.unblock($form);
                        const errorMsg = uuid ? 'Sửa thông tin không thành công!' : 'Thêm nhà cung cấp không thành công!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    }

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-supplier__form');
        initForm($form);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));


        /**
         * Init Dropzone
         */
        initDropzone();
    };

    var initDropzone = function () {
        new Dropzone('#kt_dropzone_image', {
            url: "/images/upload",
            paramName: "images",
            maxFiles: 3,
            maxFilesize: 5, // MB
            addRemoveLinks: true,
            dictDefaultMessage: 'Kéo thả ảnh vào đây để tải ảnh', 
            dictRemoveFile: 'Xóa ảnh',
            dictCancelUpload: 'Hủy',
            dictCancelUploadConfirmation: 'Bạn thực sự muốn hủy?',
            dictMaxFilesExceeded: 'Bạn không thể tải thêm ảnh!',
            accept: function (file, done) {
                done();
            },
            success: function (file, response) {
                if (response.error == 0 && response.data.items != null && response.data.items.length > 0) {
                    $("#image").val(response.data.items[0].id);
                }

            }
        });
    }

    return {
        // Public functions
        init: function () {
            //initTable();
            //initEventListeners();
            initDropzone();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-image').length) {
        DMMSImage.init();
    }
});
