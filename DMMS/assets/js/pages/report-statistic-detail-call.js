﻿"use strict";
// Class definition

var DMMSProject = function () {
    // Private functions

    var dataTableInstance = null;
    var dataTableInstance2 = null;
    var drawer = null;
    var Packagetable = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    var typeViewByDateTime = 1;
    // demo initializer

    let tFrom = moment().subtract(6, 'days').format('YYYY-MM-DD').toString();
    let tTo = moment().format('YYYY-MM-DD').toString();

    var startDatePicker = function () {
        $('#kt_dashboard_daterangepicker_title').html("");
        $("#kt_dashboard_daterangepicker_date").text(moment().subtract(6, 'days').format('DD-MM-YYYY').toString() + " - " + moment().format('DD-MM-YYYY').toString());
    }

    var daterangepickerInit = function () {
        if ($('#kt_dashboard_daterangepicker').length == 0) {
            return;
        }
        var picker = $('#kt_dashboard_daterangepicker');
        var start = moment().subtract(6, 'days');
        var end = moment();
        function cb(start, end, label) {
            var title = '';
            var range = '';
            if ((end - start) < 100 || label == 'Hôm nay') {
                title = 'Hôm nay: ';
                range = start.format('DD-MM-YYYY');
            } else if (label == 'Hôm qua') {
                title = 'Hôm qua: ';
                range = start.format('DD-MM-YYYY');
            } else {
                range = start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY');
            }
            tFrom = start.format('YYYY-MM-DD');
            tTo = end.format('YYYY-MM-DD');
            $('#charpie1').remove();
            $('.charpie1').append('<div id="charpie1"></div>');
            $('#charpie2').remove();
            $('.charpie2').append('<div id="charpie2"></div>');
            $('#charpie3').remove();
            $('.charpie3').append('<div id="charpie3"></div>');
            $('#chartmix').remove();
            $('.chartmix').append('<div id="chartmix"></div>');
            initTable(tFrom, tTo, typeViewByDateTime);
            $('#kt_dashboard_daterangepicker_date').html(range);
            $('#kt_dashboard_daterangepicker_title').html(title);
        }
        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Hôm nay': [moment(), moment()],
                'Hôm qua': [moment().subtract(1, 'days'), moment()],
                '7 ngày qua': [moment().subtract(6, 'days'), moment()],
                '30 ngày qua': [moment().subtract(29, 'days'), moment()],
                'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            locale: {
                applyLabel: 'Áp dụng',
                cancelLabel: 'Hủy bỏ',
                customRangeLabel: 'Tùy chỉnh',
                firstDay: 1
            }
        }, cb);
    }

    var initTable = function (timeFrom, timeTo, typeViewByDateTime) {
        if (typeViewByDateTime == 1) {
            if (dataTableInstance != null && dataTableInstance != undefined) dataTableInstance.destroy();
            dataTableInstance = $('.kt-datatable-detail-call1').KTDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '/ReportOverView/GetReportStatisticDetailCall',
                        },
                        response: {
                            map: function (res) {
                                const raw = res && res.data || {};
                                const { pagination } = raw;
                                const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                                const page = pagination === undefined ? 0 : pagination.page;
                                CURRENT_PAGE = page || 1;
                                START_PAGE = 0;
                                LIMIT = perpage;
                                return {
                                    data: raw.items || [],
                                    meta: {
                                        ...pagination || {},
                                        perpage: perpage || LIMIT,
                                        page: page || 1,
                                    },
                                }
                            },
                        },
                        filter: function (data = {}) {
                            const query = data.query || {};
                            const pagination = data.pagination || {};
                            const { customerId, contactId, positionId, stateId, keyword } = query;
                            if (START_PAGE === 1) {
                                pagination.page = START_PAGE;
                            }
                            return {
                                limit: pagination.perpage || LIMIT,
                                customerId: customerId || $('#js-filter-customer-callcoming').val(),
                                contactId: contactId || $('#js-filter-contact-callcoming').val(),
                                positionId: positionId || $('#js-filter-position-callcoming').val(),
                                stateId: stateId || $('#js-filter-state-callcoming').val(),
                                keyword: (keyword || $('#js-filter-keyword-callcoming').val()).trim(),
                                timeFrom: timeFrom,
                                timeTo: timeTo,
                                page: pagination.page,

                            };
                        }
                    },
                    pageSize: LIMIT,
                    serverPaging: true,
                    serverFiltering: true,
                },

                search: {
                    //input: $('#js-filter-keyword'),
                    onEnter: true,
                },
                columns: [
                    {
                        field: 'id',
                        title: 'STT',
                        width: 30,
                        textAlign: 'center',
                        template: function (data, row) {
                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                        },
                        responsive: {
                            visible: 'md',
                            hidden: 'sm'
                        }
                    },
                    {
                        field: 'code',
                        title: 'Lịch sử cuộc gọi',
                        width: 80
                    },
                    {
                        field: 'name_project',
                        title: 'Tiêu đề cuộc gọi đến',

                    },
                    {
                        field: 'field',
                        title: 'Đơn vị liên hệ',
                        width: 80
                    },
                    {
                        field: 'province',
                        title: 'Người liên hệ',
                        width: 70
                    },
                    {
                        field: 'count_dvtk',
                        title: 'Chức vụ',
                        width: 60
                    },
                    {
                        field: 'count_product',
                        title: 'Số điện thoại',
                        width: 60
                    },
                    {
                        field: 'contract_code',
                        title: 'Nhân viên tư vấn',
                        width: 70
                    },
                    {
                        field: 'date_handover',
                        title: 'Trạng thái',
                        width: 70
                    }],
                sortable: false,
                pagination: true,
                responsive: true,
            });
        }
        else {
            if (dataTableInstance != null && dataTableInstance != undefined) dataTableInstance.destroy();
            dataTableInstance = $('.kt-datatable-detail-call2').KTDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '/ReportOverView/GetReportStatisticDetailCallAway',
                        },
                        response: {
                            map: function (res) {
                                const raw = res && res.data || {};
                                const { pagination } = raw;
                                const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                                const page = pagination === undefined ? 0 : pagination.page;
                                CURRENT_PAGE = page || 1;
                                START_PAGE = 0;
                                LIMIT = perpage;
                                return {
                                    data: raw.items || [],
                                    meta: {
                                        ...pagination || {},
                                        perpage: perpage || LIMIT,
                                        page: page || 1,
                                    },
                                }
                            },
                        },
                        filter: function (data = {}) {
                            const query = data.query || {};
                            const pagination = data.pagination || {};
                            const { customerId, contactId, positionId, stateId, keyword } = query;
                            if (START_PAGE === 1) {
                                pagination.page = START_PAGE;
                            }
                            return {
                                limit: pagination.perpage || LIMIT,
                                customerId: customerId || $('#js-filter-customer-callaway').val(),
                                contactId: contactId || $('#js-filter-contact-callaway').val(),
                                positionId: positionId || $('#js-filter-position-callaway').val(),
                                stateId: stateId || $('#js-filter-state-callaway').val(),
                                keyword: (keyword || $('#js-filter-keyword-callaway').val()).trim(),
                                timeFrom: timeFrom,
                                timeTo: timeTo,
                                page: pagination.page,

                            };
                        }
                    },
                    pageSize: LIMIT,
                    serverPaging: true,
                    serverFiltering: true,
                },

                search: {
                    //input: $('#js-filter-keyword'),
                    onEnter: true,
                },
                columns: [
                    {
                        field: 'id',
                        title: 'STT',
                        width: 30,
                        textAlign: 'center',
                        template: function (data, row) {
                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                        },
                        responsive: {
                            visible: 'md',
                            hidden: 'sm'
                        }
                    },
                    {
                        field: 'code',
                        title: 'Lịch sử cuộc gọi',
                        width: 80
                    },
                    {
                        field: 'name_project',
                        title: 'Tiêu đề cuộc gọi đi',

                    },
                    {
                        field: 'field',
                        title: 'Đơn vị liên hệ',
                        width: 80
                    },
                    {
                        field: 'province',
                        title: 'Người liên hệ',
                        width: 70
                    },
                    {
                        field: 'count_dvtk',
                        title: 'Chức vụ',
                        width: 60
                    },
                    {
                        field: 'count_product',
                        title: 'Số điện thoại',
                        width: 60
                    },
                    {
                        field: 'contract_code',
                        title: 'Nhân viên tư vấn',
                        width: 70
                    },
                    {
                        field: 'date_handover',
                        title: 'Trạng thái',
                        width: 70
                    }],
                sortable: false,
                pagination: true,
                responsive: true,
            });
        }
    };   

    $('.link-report-statistic-detail-call-coming').on("click", function () {
        typeViewByDateTime = 1;
        initTable(tFrom, tTo, 1);
    });

    $('.link-report-statistic-detail-call-away').on("click", function () {
        typeViewByDateTime = 2;
        initTable(tFrom, tTo, 2);      
    });

    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var initSearch = function () {
        $('#js-filter-keyword-callcoming')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            }).selectpicker();

        $('#js-filter-keyword-callaway')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            }).selectpicker();

        $('#js-filter-customer-callcoming')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'customerId');
                    START_PAGE = 1;
                }
            }).selectpicker();

        $('#js-filter-customer-callaway')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'customerId');
                    START_PAGE = 1;
                }
            }).selectpicker();

        $('#js-filter-contact-callcoming')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'contactId');
                    START_PAGE = 1;
                }
            }).selectpicker();
        $('#js-filter-contact-callaway')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'contactId');
                    START_PAGE = 1;
                }
            }).selectpicker();
        $('#js-filter-position-callcoming')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'positionId');
                    START_PAGE = 1;
                }
            }).selectpicker();
        $('#js-filter-position-callaway')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'positionId');
                    START_PAGE = 1;
                }
            }).selectpicker();
        $('#js-filter-state-callcoming')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'stateId');
                    START_PAGE = 1;
                }
            }).selectpicker();
        $('#js-filter-state-callaway')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'stateId');
                    START_PAGE = 1;
                }
            }).selectpicker();
        //if ($("#location_id").val() !== '') {
        //    $('#kt-select-location').selectpicker('val', $("#location_id").val());
        //}
        //else {
        //    $('#kt-select-location').selectpicker();
        //}
    };

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-project__form');
        initForm($form);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

    };

    return {
        // Public functions
        init: function () {
            startDatePicker();
            initTable(tFrom, tTo, 1);
            initSearch();
            daterangepickerInit();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-reportstatisticdetailcall').length) {
        DMMSProject.init();
    }
});
