﻿"use strict";
// Class definition

import { Calendar } from '@fullcalendar/core';
import dayGrid from '@fullcalendar/daygrid';
import interaction from '@fullcalendar/interaction';
import timeGrid from '@fullcalendar/timegrid';
import list from '@fullcalendar/list';
import viLocale from '@fullcalendar/core/locales/vi';

var DMMSFullCalendarDemo = function () {
    // Private functions

    var initFullCalendar = function () {
        var className = {
            1: { 'class': '' },
            2: { 'class': 'fc-event-danger fc-event-solid-warning' },
            3: { 'class': 'fc-event-light fc-event-solid-primary' },
            4: { 'class': 'fc-event-solid-danger fc-event-light' },//denied
        };

        var todayDate = moment().startOf('day');
        var YM = todayDate.format('YYYY-MM');
        var YESTERDAY = todayDate.clone().subtract(1, 'day').format('YYYY-MM-DD');
        var TODAY = todayDate.format('YYYY-MM-DD');
        var TOMORROW = todayDate.clone().add(1, 'day').format('YYYY-MM-DD');

        var calendarEl = document.getElementById('dmms-calendar');
        var calendar = new Calendar(calendarEl, {
            plugins: [interaction, dayGrid, timeGrid, list],

            isRTL: false,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay'
            },

            locale: viLocale,

            height: 800,
            contentHeight: 780,
            aspectRatio: 3,  // see: https://fullcalendar.io/docs/aspectRatio

            nowIndicator: true,
            now: TODAY + 'T09:25:00', // just for demo

            // views: {
            //   dayGridMonth: { buttonText: 'Tháng' },
            //   timeGridWeek: { buttonText: 'Tuần' },
            //   timeGridDay: { buttonText: 'Ngày' }
            // },

            defaultView: 'dayGridMonth',
            defaultDate: TODAY,

            editable: true,
            eventLimit: true, // allow "more" link when too many events
            navLinks: true,
            events: function (info, successCallback, failureCallback) {
                $.ajax({
                    url: '/reportdaily/gets',
                    dataType: 'json',
                    data: {
                        limit: 100,
                        date_report: TODAY
                    },
                    success: function (res) {
                        var events = [{
                            id: '123',
                            title: 'Sửa chữa iPhone XS Max',
                            uid: '123',
                            customer: 'customer',
                            task: 'task',
                            start: '2020-01-07',
                            end: '2020-01-09',
                            className: '',
                            description: 'Yêu cầu: <strong>Lỗi màn hình<strong>',

                        }];
                        //if (res.data && res.data.items) {
                        //    for (var i = 0; i < res.data.items.length; i++) {
                        //        events.push({
                        //            id: res.data.items[i].task.id,
                        //            title: res.data.items[i].task.name,
                        //            uuid: res.data.items[i].task.uuid,
                        //            customer: res.data.items[i].customer,
                        //            task: res.data.items[i].task,
                        //            start: moment(res.data.items[i].date_report).format("YYYY-MM-DD"),
                        //            className: className[res.data.items[i].state].class,
                        //            description: '<div>Yêu cầu của khách hàng <strong>' + res.data.items[i].customer.name + '</strong></div>',
                        //        });
                        //    }
                        //}
                        //console.log(events);
                        successCallback(events);
                    }
                    /*
                    url: "/get-calendar",
                    dataType: 'json',
                    data: {
                        limit: 200,
                    },
                    success: function (res) {
                        successCallback(res);
                    }
                    */
                });
            },

            eventRender: function (info) {
                const $el = $(info.el);
                $el.attr({
                    title: info.event.extendedProps.description,
                    'data-placement': 'top',
                });

                KTApp.initTooltip($el);
            },

            eventClick: function (calEvent, jsEvent, view) {
                console.log(calEvent, jsEvent, view);
                alert('Event id: ' + calEvent.event.id);
                alert('View: ' + calEvent.view.name);

                // change the border color just for fun
                $(this).css('border-color', 'red');
            }
        });

        calendar.render();
    };

    return {
        // Public functions
        init: function () {
            initFullCalendar();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-reportdaily-full').length) {
        DMMSFullCalendarDemo.init();
    }
});
