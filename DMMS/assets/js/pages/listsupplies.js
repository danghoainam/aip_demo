"use strict";
// Class definition

var DMMSKnowledge = function () {
    // Private functions

    var dataTableInstance = null;
    var drawer = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    // demo initializer
    var initTable = function () {
        var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
        if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
            $.notify(editSucessCache.massage, { type: 'success' });
            localStorage.removeItem('edit_success');
        }
        dataTableInstance = $('.kt-datatable').KTDatatable({

            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/ListSupplies/gets',
                    },
                    response: {
                        map: function (res) {
                            debugger
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page,
                                CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            //Gán các thuộc tính của images{0} vào đối tượng item
                            if (raw.items != null) {
                                (raw.items).forEach(function (e) {

                                    e.path = e.image != null ? e.image.path : null;

                                    return e;


                                })
                            }


                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword, category_type } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            category_type: (category_type || $('#js-filter-staff').val()),
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,
            },

            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 20,
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'name',
                    title: 'Tên thiết bị-vật tư',
                    template: function (data) {
                        debugger
                        var output = '<strong>' + data.name + '</strong>';
                        if (data.prefix_name) {
                            output += '<br/>' + data.prefix_name + '</span>';
                        }
                        return output;
                    },

                },
                {
                    field: 'type',
                    title: 'Phân loại',
                    width: 110,
                    template: function (data) {
                        var ouput = "";
                        if (data.type == 1) {
                            ouput = "Thiết bị"
                        }
                        else if (data.type == 2) {
                            ouput = "Vật tư"
                        }
                        else if (data.type == 3) {
                            ouput = "Dụng Cụ"
                        }
                        return ouput;
                    }

                },
                {
                    field: 'detail.unit',
                    title: 'Đơn vị tính',
                    width: 90,

                },
                {
                    field: 'imp_price_ref_format',
                    title: 'Giá nhập vào',
                    width: 110,

                },
                {
                    field: 'exp_price_ref_format',
                    title: 'Giá xuất ra',
                    width: 110,

                },
                {
                    field: 'detail.manufacturer',
                    title: 'Hãng thiết bị',
                    width: 110,
                },
                {
                    field: 'detail.origin',
                    title: 'Xuất xứ',
                    width: 110,
                },
                //{
                //    field: 'state',
                //    title: 'Trạng thái',
                //    width: 150,
                //    template: function (data) {
                //        debugger
                //        var states = {
                //            0: { 'title': 'Tạm dừng', 'class': 'kt-badge--warning' },
                //            1: { 'title': 'Hoạt động', 'class': ' kt-badge--brand' },
                            
                //        };
                //        if (data.detail.status != null) {
                //            return '<span class="kt-badge ' + states[data.detail.status].class + ' kt-badge--inline kt-badge--pill">' + states[data.detail.status].title + '</span>';
                //        }
                //        else {
                //            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Chưa rõ</span>';
                //        }
                //    },
                //    width: 150,
                //    textAlign: 'center'
                //},
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 75,
                    overflow: 'visible',

                    textAlign: 'center',
                    template: function (data) {
                        return `
                           <a data-id="${data.id}" title="Chỉnh sửa" class="js-knowledge btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </a>
                            <button data-id="${data.id}" title="Xóa" class="js-delete-supplier btn btn-sm btn-danger btn-icon btn-icon-md ml-2">
                                <i class="la la-trash"></i>
                            </button>
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };
    var initSearch = function () {
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            })
        $('#js-filter-staff')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val(), 'category_type');
                    START_PAGE = 1;
                }
            })


    };
    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var deleteSupplier = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/knowledge/delete/${id}`,
                method: 'DELETE',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa thành công!', { type: 'success' });

                    reloadTable();
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa không thành công!', { type: 'danger' });
                });
        }
    };

    var initSupplierDrawer = function (id) {
        debugger
        const title = 'Cập nhật danh mục thiết bị';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--supplies' });

        drawer.on('ready', () => {
            $.ajax({
                url: `listsupplies/edit/${id}`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);

                    jQuery.fn.preventDoubleSubmission = function () {
                        $(this).on('submit', function (e) {
                            var $form = $(this);

                            if ($form.data('submitted') === true) {
                                // Previously submitted - don't submit again
                                e.preventDefault();
                            } else {
                                // Mark it so that the next submit can be ignored
                                $form.data('submitted', true);
                            }
                        });

                        // Keep chainability
                        return this;
                    };
                    $('form.dmms-listsupplies__form').preventDoubleSubmission();
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
   
    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-supplier')
            .on('click', '.js-delete-supplier', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'TB-VT sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteSupplier(id);
                    }
                });
            })

            
            .off('click', '.js-knowledge')
            .on('click', '.js-knowledge', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                initSupplierDrawer(id || '');
            });
    };
    var initForm = function ($form) {
        debugger
        initValidation(true);
    }
    
    var initValidation = function (isForm) {
        $(".dmms-listsupplies__form").validate({
            rules: {
                //name: {
                //    required: true,
                //},


            },
            messages: {
                //name: {
                //    required: 'Vui lòng nhập tên câu hỏi.'
                //},


            },
            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('.dmms-listsupplies__form', -200);
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;
                console.log(data, uuid);
                $.ajax({
                    url: 'listsupplies/edit',
                    method: 'POST',
                    data,
                })
                    .done(function () {
                        $(".submit").attr("disabled", true);
                        KTApp.block('.dmms-knowledge__form');
                        const successMsg = uuid ? 'Sửa thông tin thành công!' : 'Thêm thành công!';
                        $.notify(successMsg, { type: 'success' });
                        if (isForm) {
                            drawer.close();
                            reloadTable();
                        }
                    })
                    .fail(function () {
                      
                        KTApp.unblock('.dmms-knowledge__form');
                        const errorMsg = uuid ? 'Sửa thông tin không thành công!' : 'Thêm thành không công!';
                        $.notify(errorMsg, { type: 'danger' });
                       
                        
                    });
                return false;
            },
        });
    }
  

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-listsupplies__form');
        initForm($form);
       

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));


        /**
         * Init Dropzone
         */
        //initDropzone();
    };



    return {
        // Public functions
        init: function () {
            initTable();
            initSearch();
            initEventListeners();
            initValidation();
            $('#js-filter-staff').selectpicker();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-listsupplies').length) {
        DMMSKnowledge.init();
    }
});
