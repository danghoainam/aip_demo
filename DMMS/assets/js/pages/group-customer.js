﻿"use strict";
// Class definition

var DMMSGroupCustomer = function () {
    // Private functions

    var dataTableInstance = null;

    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var subTableInit = function (e) {
        $('<div/>').attr('id', 'child_data_local_' + e.data.uuid).appendTo(e.detailCell).KTDatatable({
            data: {
                type: 'local',
                source: e.data.groups,
                pageSize: 10,
            },

            // layout definition
            layout: {
                scroll: true,
                height: 300,
                footer: false,

                // enable/disable datatable spinner.
                spinner: {
                    type: 1,
                    theme: 'default',
                },
            },

            sortable: false,
            pagination: false,
            responsive: true,
            // columns definition
            columns: [
                {
                    field: '',
                    title: '#',
                    sortable: false,
                    width: 20,
                    template: function (data, row) {
                        return row + 1;
                    }
                },
               {
                    field: 'name',
                    title: 'Tên nhóm',
                }, {
                    field: 'description',
                    title: 'Mô tả',
                }
            ],
        });
    };

    // demo initializer
    var initTable = function () {
        dataTableInstance = $('.kt-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/customer/get-list-group',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            CURRENT_PAGE = pagination.page || 1;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: pagination.limit || LIMIT,
                                    page: pagination.page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { location_id, keyword } = query;
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: keyword || '',
                            page: pagination.page,
                            get_all:true
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },
            // layout definition
            layout: {
                scroll: false,
                height: null,
                footer: false,
            },
            detail: {
                title: 'Đang tải danh sách khách hàng...',
                content: subTableInit,
            },
            search: {
                input: $('#js-filter-keyword'),
            },
            columns: [
                
                {
                    field: 'uuid',
                    title: '',
                    sortable: false,
                    width: 10,
                    textAlign: 'center',
                },
                {
                    field: '',
                    title: '#',
                    width: 20,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    }
                },
                {
                    field: 'name',
                    title: 'Tên khách hàng',
                },
                {
                    field: 'contact_phone_number',
                    title: 'Số điện thoại',
                },
                {
                    field: 'email',
                    title: 'Email',
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    textAlign: 'center',
                    overflow: 'visible',
                    autoHide: false,
                    template: function (row) {
                        return `
                            <button type="button" data-group-name="${row.name}" data-group-desc="${row.description}" data-id="${row.uuid}"\
                                title="Thêm vào nhóm" class="js-list-customer btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-plus"></i>
                            </button>
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };



    var initValidation = function () {
        $("#dmms-group__form").validate({
            rules: {

                name: {
                    required: true,
                },
                description: {
                    required: true,
                },
            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên nhóm.'
                },
                description: {
                    required: 'Vui lòng nhập mô tả.'
                },

            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('#dmms-group__form', -200);
            },
            submitHandler: function (form) {
                form[0].submit(); // submit the form
            },
        });
    };

    return {
        // Public functions
        init: function () {
            initTable();
            //initSearch();
            initValidation();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-group-customer').length) {
        DMMSGroupCustomer.init();
    }
});
