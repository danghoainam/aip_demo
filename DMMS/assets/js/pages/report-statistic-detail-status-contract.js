﻿"use strict";
// Class definition

var DMMSProject = function () {
    // Private functions

    var dataTableInstance = null;
    var drawer = null;
    var Packagetable = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    // demo initializer

    let tFrom = moment().subtract(6, 'days').format('YYYY-MM-DD').toString();
    let tTo = moment().format('YYYY-MM-DD').toString();

    var startDatePicker = function () {
        $('#kt_dashboard_daterangepicker_title').html("");
        $("#kt_dashboard_daterangepicker_date").text(moment().subtract(6, 'days').format('DD-MM-YYYY').toString() + " - " + moment().format('DD-MM-YYYY').toString());
    }

    var daterangepickerInit = function () {
        if ($('#kt_dashboard_daterangepicker').length == 0) {
            return;
        }
        var picker = $('#kt_dashboard_daterangepicker');
        var start = moment().subtract(6, 'days');
        var end = moment();
        function cb(start, end, label) {
            var title = '';
            var range = '';
            if ((end - start) < 100 || label == 'Hôm nay') {
                title = 'Hôm nay: ';
                range = start.format('DD-MM-YYYY');
            } else if (label == 'Hôm qua') {
                title = 'Hôm qua: ';
                range = start.format('DD-MM-YYYY');
            } else {
                range = start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY');
            }
            tFrom = start.format('YYYY-MM-DD');
            tTo = end.format('YYYY-MM-DD');
            $('#charpie1').remove();
            $('.charpie1').append('<div id="charpie1"></div>');
            $('#charpie2').remove();
            $('.charpie2').append('<div id="charpie2"></div>');
            $('#charpie3').remove();
            $('.charpie3').append('<div id="charpie3"></div>');
            $('#chartmix').remove();
            $('.chartmix').append('<div id="chartmix"></div>');
            initTable(tFrom, tTo);
            $('#kt_dashboard_daterangepicker_date').html(range);
            $('#kt_dashboard_daterangepicker_title').html(title);
        }
        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Hôm nay': [moment(), moment()],
                'Hôm qua': [moment().subtract(1, 'days'), moment()],
                '7 ngày qua': [moment().subtract(6, 'days'), moment()],
                '30 ngày qua': [moment().subtract(29, 'days'), moment()],
                'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            locale: {
                applyLabel: 'Áp dụng',
                cancelLabel: 'Hủy bỏ',
                customRangeLabel: 'Tùy chỉnh',
                firstDay: 1
            }
        }, cb);
    }

    var initTable = function (timeFrom, timeTo) {
        if (dataTableInstance != null && dataTableInstance != undefined) dataTableInstance.destroy();
        dataTableInstance = $('.kt-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/ReportOverview/GetReportStatisticDetailStatusContract',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? 0 : pagination.page;
                            CURRENT_PAGE = page || 1;
                            START_PAGE = 0;
                            LIMIT = perpage;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { provinceId, fieldId, stateId, keyword } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            provinceId: provinceId || $('#js-filter-location').val(),
                            fieldId: fieldId || $('#js-filter-field').val(),
                            stateId: stateId || $('#js-filter-state').val(),
                            keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            timeFrom: timeFrom,
                            timeTo: timeTo,
                            page: pagination.page,

                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                //input: $('#js-filter-keyword'),
                onEnter: true,
            },
            columns: [
                {
                    field: 'uuid',
                    title: 'STT',
                    width: 30,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'code',
                    title: 'Mã dự án',
                    width: 60
                },
                {
                    field: 'name_project',
                    title: 'Tên dự án',

                },
                {
                    field: 'field',
                    title: 'Lĩnh vực',
                    width: 100
                },
                {
                    field: 'province',
                    title: 'Tỉnh triển khai',
                    width: 90
                },
                {
                    field: 'count_dvtk',
                    title: 'Chủ đầu tư',
                    width: 110
                },
                {
                    field: 'count_product',
                    title: 'Số hiệu HĐ',
                    width: 80
                },
                {
                    field: 'contract_code',
                    title: 'Loại hợp đồng',
                    width: 70
                },

                {
                    field: 'date_handover',
                    title: 'Ngày ký hợp đồng',
                    width: 70
                },
                {
                    field: 'expiry_date',
                    title: 'Đơn vị triển khai ký hợp đồng',
                    width: 110
                },
                {
                    field: 'expiry_date',
                    title: 'Đơn vị ký hợp đồng',
                    width: 90
                },
                {
                    field: 'expiry_date',
                    title: 'Ngày bàn giao',
                    width: 80
                },
                {
                    field: 'expiry_date',
                    title: 'Thời gian hết hạn BH',
                    width: 80
                },
                {
                    field: 'expiry_date',
                    title: 'Trạng thái hợp đồng',
                    width: 110
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };

    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var initSearch = function () {
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            }).selectpicker();
        $('#js-filter-location')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'provinceId');
                    START_PAGE = 1;
                }
            }).selectpicker();
        $('#js-filter-field')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'fieldId');
                    START_PAGE = 1;
                }
            }).selectpicker();
        $('#js-filter-state')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'stateId');
                    START_PAGE = 1;
                }
            }).selectpicker();

        //if ($("#location_id").val() !== '') {
        //    $('#kt-select-location').selectpicker('val', $("#location_id").val());
        //}
        //else {
        //    $('#kt-select-location').selectpicker();
        //}
    };

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-project__form');
        initForm($form);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

    };

    return {
        // Public functions
        init: function () {
            startDatePicker();
            initTable(tFrom, tTo);
            initSearch();
            daterangepickerInit();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-reportstatisticdetailstatuscontract').length) {
        DMMSProject.init();
    }
});
