﻿"use strict";
// Class definition

var DMMSTaskPlan = function () {
    // Private functions

    var dataTableInstance = null;
    var drawer = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    var MAX_ITEM_INPAGE = 0;

    $(".list-staff").on('click', function () {
        var task_uuid = $("#uuid").val();

        dataTableInstance = $('.kt-datatable--staff').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: `/taskplan/getstaff/${task_uuid}`,
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? undefined : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page
                            CURRENT_PAGE = page || 1;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword } = query;
                        MAX_ITEM_INPAGE = pagination.perpage || LIMIT;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: MAX_ITEM_INPAGE,
                            keyword: (keyword || $('#js-filter-keyword').val()),
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,

            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 30,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * MAX_ITEM_INPAGE;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'code',
                    title: 'Mã nhân viên',
                    width: 80,
                },
                {
                    field: 'name',
                    title: 'Tên nhân viên',
                },
                {
                    field: 'phone_number',
                    title: 'Số điện thoại',
                },
                {
                    field: 'adress',
                    title: 'Địa chỉ',
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 50,
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        return `
                            <a href="/taskplan/delete/${data.uuid}" title="Chỉnh sửa" class="btn btn-sm btn-danger btn-icon btn-icon-md">
                                <i class="la la-trash"></i>
                            </a>
                        `;
                    },
                }],
            layout: {
                scroll: !0,
                height: null,
                footer: !1
            },
            sortable: false,
            pagination: true,
            responsive: true,
        });
    });

    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var initSearch = function () {
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });
    };

    return {
        // Public functions
        init: function () {
            initSearch();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-taskPlan').length) {
        DMMSTaskPlan.init();
    }
});