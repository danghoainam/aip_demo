"use strict";
// Class definition

var DMMSDevice = function () {
    // Private functions

    var dataTableInstance = null;
    var drawer = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    var uuid_images = "";
    // demo initializer
    var initTable = function () {
        var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
        if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
            $.notify(editSucessCache.massage, { type: 'success' });
            localStorage.removeItem('edit_success');
        }
        debugger;
        var customer_uuid = localStorage.getItem("customer_uuid");
        var categoryId = localStorage.getItem("categoryId");
        var packageUuid = localStorage.getItem("packageUuid") != 'undefined' ? localStorage.getItem("packageUuid") : null ;
        dataTableInstance = $('.kt-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: categoryId != null ? (packageUuid != null ? `/device/gets/${customer_uuid}/${categoryId}/${packageUuid}` : `/device/gets/${customer_uuid}/${categoryId}`) : '/device/gets',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page;
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        debugger
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { category_id, supplier_uuid, projectUuid, customer_uuid, keyword } = query;
                        var states = [];
                        states = $('#js-filter-states').val().split(',');
                       
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            //category_id: category_id || $('#js-filter-category').val(),
                            //supplier_uuid: supplier_uuid || $('#js-filter-supplier').val(),
                            projectUuid: projectUuid || $('#js-filter-project').val(),
                            //customer_uuid: customer_uuid || $('#js-filter-customer').val(),
                            keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            states: states,
                            page: pagination.page

                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,
            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 30,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                //{
                //    field: 'code',
                //    title: 'Mã',
                //    width: 53,
                //    textAlign:'center'
                //},
                //{
                //    field: 'model',
                //    title: 'Model',
                //    width:110
                //},
                {
                    field: 'name_display',
                    title: 'Tên thiết bị sử dụng',
                    template: function (data) {
                        var output = '';
                        if (data.name != null) {
                            output += `<span class="text-primary">${data.prefixName}</span>
                                        <br /><span>${data.name_display}</span>`;
                        }
                        return output;
                    }
                    //autoHide: false,
                },
                {
                    field: 'serial',
                    title: 'Serial',
                    width: 80
                },
                //{
                //    field: 'supplierName',
                //    title: 'Nhà cung cấp',
                //    width: 120,
                //},
                //{
                //    field: 'packageName',
                //    title: 'Gói thiết bị',
                //    width: 100,
                //},
                {
                    field: 'expiredDevice_nohh',
                    title: 'Ngày hết hạn TB',
                    width: 120,
                    template: function (data) {
                        if (data.expiredDevice_nohh == "01/01/0001") {
                            return 'Chưa rõ'
                        }
                        return data.expiredDevice_nohh
                    }
                },
                {
                    field: 'state',
                    title: 'Trạng thái',
                    width: 150,
                    template: function (data) {
                        var states = {
                            //1: { 'title': 'Đang sử dụng', 'class': 'kt-badge--brand' },
                            //2: { 'title': 'Đang yêu cầu sửa chữa', 'class': ' kt-badge--warning' },
                            //3: { 'title': 'Đến lịch bảo hành', 'class': ' kt-badge--primary' },
                            //4: { 'title': 'Đang xử lý theo yêu cầu', 'class': 'kt-badge--success' },
                            //5: { 'title': 'Cần thay thế', 'class': 'kt-badge--danger' },
                            //6: { 'title': 'Mang đi sửa chữa - Không có thiết bị thay thế', 'class': 'kt-badge--info' },
                            //7: { 'title': 'Mang đi sửa chữa - Có thiết bị thay thế', 'class': 'kt-badge--info' },
                            //8: { 'title': 'Thay thế', 'class': 'kt-badge--info' },
                            //9: { 'title': 'Hỏng', 'class': 'kt-badge--info' },
                            //10: { 'title': 'Trong kho-Trong kho vật lý,ba lô', 'class': 'kt-badge--info' },
                            //11: { 'title': 'Trong kho-Hỏng sau khi kiểm kê', 'class': 'kt-badge--info' },
                            //12: { 'title': 'Đang vận chuyển', 'class': 'kt-badge--info' },
                            //13: { 'title': 'Đã xuất thanh lý', 'class': 'kt-badge--info' },
                            //14: { 'title': 'Đã xuất tiêu hao', 'class': 'kt-badge--info' },
                            //15: { 'title': 'Đã hoàn trả nhà cung cấp', 'class': 'kt-badge--info' },
                            //16: { 'title': 'Thêm mới/cập nhật thiết bị', 'class': 'kt-badge--info' },
                            //17: { 'title': 'Đã sửa xong thiết bị', 'class': 'kt-badge--info' },
                            1: { 'title': 'Hoạt động', 'class': 'kt-badge--success' },
                            2: { 'title': 'Sửa chữa', 'class': ' kt-badge--warning' },
                            3: {
                                'title': 'Đến lịch bảo hành', 'class': ' kt-badge--primary', 'style':'background-color:#074648c4' },
                            4: { 'title': 'Sửa chữa', 'class': 'kt-badge--warning' },
                            5: { 'title': 'Sửa chữa', 'class': 'kt-badge--warning' },
                            6: { 'title': 'Sửa chữa', 'class': 'kt-badge--warning' },
                            7: { 'title': 'Sửa chữa', 'class': 'kt-badge--warning' },
                            8: { 'title': 'Thay thế tạm thời', 'class': 'kt-badge--info' },
                            9: { 'title': 'Hỏng', 'class': 'kt-badge--danger' },
                            10: { 'title': 'Hoạt động', 'class': 'kt-badge--danger' },
                            11: { 'title': 'Hỏng', 'class': 'kt-badge--warning' },
                            12: { 'title': 'Hoạt động', 'class': 'kt-badge--success' },
                            13: { 'title': 'Đã xuất thanh lý', 'class': 'kt-badge--primary' },
                            14: { 'title': 'Đã xuất tiêu hao', 'class': 'kt-badge--primary' },
                            15: { 'title': 'Đã hoàn trả nhà cung cấp', 'class': 'kt-badge--primary' },
                            16: { 'title': 'Thêm mới/cập nhật thiết bị', 'class': 'kt-badge--info' },
                            17: { 'title': 'Hoạt động', 'class': 'kt-badge--success' },
                        };
                        if (data.state) {
                            return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill"' +'style="'+ states[data.state].style + '">' + states[data.state].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới</span>';
                        }
                    },
                    width: 150,
                    textAlign: 'center'
                },

                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 80,
                    overflow: 'visible',
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        return `
                            <a href="/device/edit/${data.uuid}" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-edit"></i>
                            </a>                           
                            <button data-id="${data.uuid}" type="button" title="Xóa" class="btn btn-sm btn-danger btn-icon btn-icon-md js-delete-device">
                                <i class="la la-trash"></i>
                            </button>
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });

        //remove item in localStorage
        localStorage.removeItem("customer_uuid");
        localStorage.removeItem("categoryId");
        localStorage.removeItem("packageUuid");
        debugger
    };
    var initSearch = function () {
        //$('#js-filter-category')
        //    .on('change', function () {
        //        if (dataTableInstance) {
        //            dataTableInstance.search($(this).val().toLowerCase(), 'category_id');
        //            START_PAGE = 1;
        //        }
        //    })
        //    .selectpicker();
        //$('#js-filter-supplier')
        //    .on('change', function () {
        //        if (dataTableInstance) {
        //            dataTableInstance.search($(this).val().toLowerCase(), 'supplier_uuid');
        //            START_PAGE = 1;
        //        }
        //    })
        //    .selectpicker();
        $('#js-filter-project')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'projectUuid');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        //$('#js-filter-customer')
        //    .on('change', function () {
        //        if (dataTableInstance) {
        //            dataTableInstance.search($(this).val().toLowerCase(), 'customer_uuid');
        //            START_PAGE = 1;
        //        }
        //    })
        //    .selectpicker();
        $('#js-filter-states')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'states');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });

    };

    var deleteDevice = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa thiết bị...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/device/delete/${id}`,
                method: 'DELETE',
            })
                .done(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa thiết bị thành công!', { type: 'success' });

                    if (dataTableInstance) {
                        dataTableInstance.reload();
                    }
                })
                .fail(function () {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify('Xóa thiết bị không thành công!', { type: 'danger' });
                });
        }
    };

    $(document).on('change', '#js-filter-project', function () {
        //$('#variant').find('.tesst').remove();
        $('#variant').find('.test').remove();
        var project_uuid = $(this).val();
        if (project_uuid != '') {
            $.ajax({
                url: `/project/projectgetcustomer/${project_uuid}`,
                method: 'POST',
                dataType: "json",
                data: {
                    limit: 1000,
                },
                success: function (data) {

                    console.log(data);
                    var customer = data.data.items;
                    var json = JSON.stringify(customer);
                    var $select = $('#variant');

                    //for (var i = 0; i < customer.length; i++) {
                    //    $select.append('<option value=' + customer[i].project_customer_uuid + '>' + customer[i].name + '</option>');
                    //    $select.selectpicker('refresh');
                    //}

                    $.each(customer, function (index, item) {
                        console.log(item);
                        $select.append('<option value=' + item.customerUuid + '>' + item.customerName + '</option>');
                        $select.selectpicker('refresh');
                    })
                }
            })
        } else {
            $('#model').html('<option value="">Select Model</option>');
        }

        $('#variant').html('<option value="">---Chọn đơn vị---</option>');
    });

    $(document).on('change', '#js-filter-projectedit', function () {
        $('#variant1').find('.test').remove();
        var project_uuid = $(this).val();
        if (project_uuid != '') {
            $.ajax({
                url: `/project/getCustomer/${project_uuid}`,
                method: 'POST',
                dataType: "json",
                success: function (data) {

                    console.log(data);
                    var customer = data.data.customers;
                    var json = JSON.stringify(customer);
                    console.log(json);
                    var $select = $('#variant1');

                    for (var i = 0; i < customer.length; i++) {
                        $select.append('<option value=' + customer[i].project_customer_uuid + '>' + customer[i].name + '</option>');
                        $select.selectpicker('refresh');
                    }


                }
            })
        } else {
            $('#model').html('<option value="">Select Model</option>');
        }

        $('#variant1').html('<option value="">---Chọn đơn vị---</option>');
    });



    if (document.getElementById("project_uuid")) {



        const id = document.getElementById("project_uuid").value;
        if (id != '') {


            $.ajax({
                url: `/project/Detail/${id}`,
                method: 'POST',
                dataType: "json",
                success: function (data) {
                    //$('.getcuspro').find('.remove1').remove();

                    var firtid = $('.remove1').val();
                    console.log(data);
                    var customer = data.customers;
                    var json = JSON.stringify(customer);
                    console.log(json);
                    var $select = $('.getcuspro');
                    for (var i = 0; i < customer.length; i++) {
                        //debugger
                        if (customer[i].project_customer_uuid !== firtid) {
                            $select.append('<option value=' + customer[i].project_customer_uuid + ' class ="test">' + customer[i].name + '</option>');
                            $select.selectpicker('refresh');
                            //$('.getcuspro').selectpicker();

                        }

                    }

                    //$('#variant').selectpicker('val', json.val());

                }
            })
        } else {
            $('#model').html('<option value="">Select Model</option>');
        }
    }

    //var demo8 = function () {
    //    var map = new GMaps({
    //        div: '#kt_gmap_8',
    //        lat: 21.027763,
    //        lng: 105.834160
    //    });

    //    var handleAction = function () {
    //        var text = $.trim($('#kt_gmap_8_address').val());
    //        GMaps.geocode({
    //            address: text,
    //            callback: function (results, status) {
    //                if (status == 'OK') {
    //                    var latlng = results[0].geometry.location;
    //                    map.setCenter(latlng.lat(), latlng.lng());
    //                    map.addMarker({
    //                        lat: latlng.lat(),
    //                        lng: latlng.lng()
    //                    });
    //                    KTUtil.scrollTo('kt_gmap_8');
    //                }
    //            }
    //        });
    //    }

    //    $('#kt_gmap_8_btn').click(function (e) {
    //        e.preventDefault();
    //        handleAction();
    //    });

    //    $("#kt_gmap_8_address").keypress(function (e) {
    //        var keycode = (e.keyCode ? e.keyCode : e.which);
    //        if (keycode == '13') {
    //            e.preventDefault();
    //            handleAction();
    //        }
    //    });
    //}

    var initDetailDrawer = function (id) {
        const title = id ? 'Chi tiết đơn vị' : '';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--detaildevice' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/device/detail/${id}` : '',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    const $form = $content.find('.dmms-device__form');
                    initValidation($form);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-device')
            .on('click', '.js-delete-device', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Thiết bị sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteDevice(id);
                    }
                });

            })
            .on('click', '.js-detail-device', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                initDetailDrawer(id || '');
            });


    };

    var initValidation = function () {
        if ($("#expire_device_str").val() !== '') {
            $('.kt_datepicker_expire_device').datepicker({ format: 'dd/mm/yyyy' }).datepicker('setDate', new Date($("#expire_device_str").val()));
        }
        else {
            $('.kt_datepicker_expire_device').datepicker({ format: 'dd/mm/yyyy' });
        }

        if ($("#expire_contract_str").val() !== '') {
            $('.kt_datepicker_expire_contract').datepicker({ format: 'dd/mm/yyyy' }).datepicker('setDate', new Date($("#expire_contract_str").val()));
        }
        else {

            $('.kt_datepicker_expire_contract').datepicker({ format: 'dd/mm/yyyy' });
        }

        //initDropzone();

        $("#dmms-device__form").validate({
            rules: {

                name: {
                    required: true,
                },
                state: {
                    required: true,
                },
                serial: {
                    required: true,
                },
                imei: {
                    required: true,
                },
                expire_device: {
                    required: true,
                },
                expire_contract: {
                    required: true,
                }
            },
            messages: {
                name: {
                    required: 'Vui lòng nhập tên thiết bị.'
                },
                serial: {
                    required: 'Vui lòng nhập serial'
                },
                imei: {
                    required: 'Vui lòng nhập imei'
                },
                expire_device: {
                    required: 'Vui lòng chọn ngày'
                },
                expire_contract: {
                    required: 'Vui lòng chọn ngày'
                },

                state: {
                    required: 'Vui lòng trạng thái.'
                }
            },

            //display error alert on form submit  
            invalidHandler: function (event, validator) {
                KTUtil.scrollTo('#dmms-device__form', -200);
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                const { uuid } = data;

                console.log(data, uuid);
                $.ajax({
                    url: uuid ? `/device/edit` : '/device/create',
                    method: uuid ? 'PUT' : 'POST',
                    data,
                })
                    .done(function () {
                        $(".submit").attr("disabled", true);
                        const successMsg = uuid ? 'Sửa thông tin thành công!' : 'Thêm thiết bị thành công!';


                        /**
                         * Close drawer
                         */

                        localStorage['edit_success'] = JSON.stringify({ from: (uuid ? `/device/edit` : '/device/create'), massage: successMsg });


                        window.location = window.location.origin + "/device";
                    })
                    .fail(function (data) {
                        const errorMsg = uuid ? data.responseJSON.msg + '!' : data.responseJSON.msg + '!';
                        $.notify(errorMsg, { type: 'danger' });
                    });
                return false;
            },
        });
    };
    var initDropzone = function () {

        new Dropzone('#kt_dropzone_image', {
            url: "/images/upload",
            paramName: "images",
            maxFiles: 4,
            maxFilesize: 1, // MB
            addRemoveLinks: true,
            dictDefaultMessage: 'Bấm hoặc kéo thả ảnh vào đây để tải ảnh lên',
            dictRemoveFile: 'Xóa ảnh',
            dictCancelUpload: 'Hủy',
            dictCancelUploadConfirmation: 'Bạn thực sự muốn hủy?',
            dictMaxFilesExceeded: 'Bạn không thể tải thêm ảnh!',
            dictFileTooBig: "Ảnh tải lên dung lượng quá lớn ({{filesize}}MB). Max filesize: {{maxFilesize}}MB.",
            accept: function (file, done) {
                done();
            },
            //removedfile: function (file, response) {
            //    //debugger
            //    var uuid_images = $('#uuid_images').val();
            //    file.previewElement.remove();
            //},
            success: function (file, response) {
                if (response != null && response.length > 0) {
                    var uuid_images = $('#uuid_images').val();
                    for (var i = 0; i < response.length; i++) {
                        uuid_images = uuid_images + "," + response[i].id;
                        $('#uuid_images').val(uuid_images);
                    }

                    console.log(response, uuid_images, $('#uuid_images').val());

                }
                else {
                    $(file.previewElement).addClass("dz-error").find('.dz-error-message').text("Không tải được ảnh lên.");
                }

            },
            error: function (file, response, errorMessage) {
                console.log(file, response);
                if (file.accepted) {
                    $(file.previewElement).addClass("dz-error").find('.dz-error-message').text("Có lỗi xảy ra, không tải được ảnh lên.");
                }
                else {
                    $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(response);
                }
            }
        });
    }

    return {
        // Public functions
        init: function () {
            initTable();
            initSearch();
            initValidation();
            initEventListeners();
            //demo8();
            //if ($("#expire_device_str").val() !== '') {
            //    $('.kt_datepicker_expire_device').datepicker().datepicker('setDate', new Date($("#expire_device_str").val()));
            //}
            //else {
            //    $('.kt_datepicker_expire_device').datepicker({format:'dd/mm/yyyy'});
            //}

            //if ($("#expire_contract_str").val() !== '') {
            //    $('.kt_datepicker_expire_contract').datepicker().datepicker('setDate', new Date($("#expire_contract_str").val()));
            //}
            //else {

            //    $('.kt_datepicker_expire_contract').datepicker();
            //}
            //if (window.location.pathname != '/device') {
            //    initDropzone();
            //}
            $('#variant').selectpicker();
            $('#js-filter-projectedit').selectpicker();
            $('#js-filter-state').selectpicker();
            var text = $("#js-filter-state option:selected").text();
            $('.fillstate').val(text);

        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-device').length) {
        DMMSDevice.init();


    }
});
