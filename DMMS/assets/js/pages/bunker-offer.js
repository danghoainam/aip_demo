﻿"use strict";
// Class definition

var DMMSBunker = function () {
    // Private functions

    var dataTableInstance = null;
    var drawer = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;

    $('.bunker-tab-offer').on('click', function () {
        const storageUuid = $('#uuid').val();
        if (dataTableInstance !== null) dataTableInstance.destroy();
        dataTableInstance = $('.kt-datatable--offer').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: `/bunker/getofferinbunker/${storageUuid}`,
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? 0 : pagination.page;
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword, status, storage_uuid } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            status: status || $("#js-filter-state").val(),
                            storage_uuid: storage_uuid || $("#js-filter-typeoffer").val(),
                            keyword: (keyword || $('#js-filter-keyword').val()) !== undefined ? (keyword || $('#js-filter-keyword').val()).trim() : (keyword || $('#js-filter-keyword').val()),
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,

            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 30,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'code',
                    title: 'Mã đề xuất',
                    width: 90
                },
                {
                    field: 'order_person.name',
                    title: 'Người đề xuất',
                    width: 140,
                    template: function (data) {
                        return `<a href="/staff/update/${data.order_person.uuid}" title="Chi tiết nhân viên ${data.order_person.name}">${data.order_person.name}</a>`
                    }
                },
                {
                    field: '#',
                    title: 'Mã-Công việc',
                    template: function (data) {
                        return `<a href="/task/update/${data.task.uuid}" title="Chi tiết công việc ${data.task.name}">
                                    <strong>${data.task.code}</strong><br />
                                    <span style="color:#595d6e">${data.task.name}</span>
                                </a>`;
                    },
                },
                {
                    field: 'from.name',
                    title: 'Kho nguồn',
                    width: 150
                },
                {
                    field: 'state',
                    title: 'Trạng Thái',
                    textAlign: 'center',
                    width: 110,
                    template: function (data) {
                        var states = {
                            2: { 'title': 'Chưa duyệt', 'class': 'kt-badge--warning' },
                            1: { 'title': 'Đã duyệt', 'class': ' kt-badge--primary' },
                            4: { 'title': 'Đã tiếp nhận', 'class': ' kt-badge--primary' },
                            3: { 'title': 'Đã từ chối', 'class': ' kt-badge--danger' },
                            5: { 'title': 'Đã hoàn thành', 'class': 'kt-badge--success' },
                        };
                        if (data.status) {
                            if (states[data.status] !== undefined) {
                                return '<span class="kt-badge ' + states[data.status].class + ' kt-badge--inline kt-badge--pill">' + states[data.status].title + '</span>';
                            }
                            else {
                                return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Chưa rõ</span>';
                            }
                        }
                        else {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Chưa rõ</span>';
                        }
                    }
                },
                {
                    field: 'to.name',
                    title: 'Kho đích',
                    width: 150
                },
                {
                    field: 'time_created_format',
                    title: 'Thời gian',
                    width: 100
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 50,
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        var storge_uuid = $('#uuid').val();
                        var uuid = data.uuid;
                        const state_bunker = $('#state').val();
                        return `
                            <a href="/bunker/detailoffer/${uuid}/${storge_uuid}/${state_bunker}" title="Chỉnh sửa" class="btn btn-sm btn-primary btn-icon btn-icon-md">
                                <i class="la la-info"></i>
                            </a>
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    });

    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var initSearch = function () {
        $('#js-filter-keyword--offer')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });
        $('#js-filter-state')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val(), 'state');
                    START_PAGE = 1;
                }
            }).selectpicker();
        $('#js-filter-typeoffer')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val(), 'storage_uuid');
                    START_PAGE = 1;
                }
            }).selectpicker();
    };

    var addOfferInBunker = function () {
        const title = 'PHIẾU ĐỀ XUẤT THIẾT BỊ VẬT TƯ TỪ KHO ĐẾN KHO';
        drawer = new KTDrawer({ title, uuid: 'new', className: 'dmms-drawer--addoffer' });

        const storage_uuid = $('#uuid').val();
        const storage_name = $('#name').val();

        drawer.on('ready', () => {
            $.ajax({
                url: `/bunker/addoffer/${storage_uuid}/${storage_name}`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);

                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    }

    var initEventListeners = function () {
        $(document)
            .off('click', '.js-add-offer')
            .on('click', '.js-add-offer', function () {

                addOfferInBunker();
            })
    };

    var initForm = function ($form) {
        $form.validate({
            rules: {

            },
            messages: {

            },

            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
                $('#kt-select-staff').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });
            },
            submitHandler: function (form) {
                $(form).find('button[type="submit"]').prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading

                const data = $(form).serializeJSON() || {};
                $.ajax({
                    url: '/bunker/addoffer',
                    method: 'POST',
                    data,
                })
                    .done(function () {
                        KTApp.block($form);
                        const successMsg = 'Thêm thông tin thành công!';
                        $.notify(successMsg, { type: 'success' });

                        /**
                         * Close drawer
                         */
                        drawer.close();

                        reloadTable();
                    })
                    .fail(function (data) {
                        KTApp.unblock($form);
                        $(form).find('button[type="submit"]').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
                        const successMsg = 'Thêm thông tin không thành công!';
                        $.notify(data.responseJSON.msg, { type: 'danger' });
                    });
                return false;
            },
        });
    };

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-form-bunker__addoffer');
        initForm($form);

        /*
            Init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

        $('#kt-select-filed').selectpicker();
        $('#kt-select-supplies').selectpicker({ showSubtext: true });

        //Change #kt-select-supplies
        $('#kt-select-supplies').on("change", function () {
            const type_categries = { 1: "Thiết bị", 2: "Vật tư", 3: "Dụng cụ LĐ", 4: "Phần mềm" };
            const wrapper = $('.wrapper-fill');
            const category = wrapper.find('#kt-select-supplies');
            const category_id = category.val();

            $.ajax({
                url: `/bunker/DetailSuppliesAjax/${category_id}`,
                method: 'GET'
            })
                .done(function (data) {
                    $('#type_category').html(type_categries[data.data.type]);
                    $('#unit').html(data.data.detail.unit);
                });
        });

        $('#js-add-offersupplies').on('click', function () { //Thêm thiết bị - vật tư
            const wrapper = $('.wrapper-fill');
            const supplies = wrapper.find('#kt-select-supplies');
            const category_id = supplies.val();
            if (category_id != "") {
                addSuppliesToOffer();

                $('#kt-select-supplies').find(`option[value="${category_id}"]`).attr('disabled', true); //Disabled option của chọn vật tư
                $('#kt-select-supplies').selectpicker('refresh');
            }
            else $.notify('Vui lòng chọn vật tư!', { type: 'danger' });
        });

        //close drawer
        $('.kt-close').on('click', function () {
            drawer.close();
        });
    };

    //Thêm mới raw trong drawer nhập kho
    var addSuppliesToOffer = function () {
        const wrapper = $('.wrapper-fill');

        const supplies = wrapper.find('#kt-select-supplies');
        const category_id = supplies.val(); //uuid của category
        const category_prefix = supplies.find('option:selected').text(); //prefix của category
        const category_name = supplies.find('option:selected').attr('data-subtext'); //Tên của category
        const type_category = wrapper.find('#type_category').text(); //Tên của loại (1-vật tư, 2-thiết bị, 3-Dụng cụ LĐ, 4-Phần mềm)
        const offer_quantity = wrapper.find('input[id="offer_quantity"]').val() != '' ? wrapper.find('input[id="offer_quantity"]').val() : 0; //Số lượng thiết bị đề xuất
        const unit = wrapper.find('#unit').text(); //Đơn vị tính

        //Nếu có giá trị của vật tư thiết bị => add thêm raw vật tư thiết bị
        if (category_id != null) {
            wrapper.append(`<tr id="${category_id}">
                        <td class="text-left kt-padding-l-10"><input name="categories[][category_id]" value="${category_id}" hidden />
                            <span class="text-primary">${category_prefix}</span>
                            <br/>${category_name}
                        </td>
                        <td>${type_category}</td>
                        <td><input class="form-control" placeholder="Nhập" name="categories[][offer_quantity]" type="number" min="0" value="${offer_quantity}" /></td>
                        <td>${unit}</td>
                        <td><i class="la la-trash remove_field" style="background-color: #FF5F6D"></i></td>
                    </tr>`);

            //Sau khi add thêm category => reset giá trị của raw nhập liệu
            wrapper.find('#type_category').html('-'); //Phân loại => "-"
            wrapper.find('input[id="offer_quantity"]').val(''); //số lượng theo chứng từ => ''
            wrapper.find('#unit').html('-'); //đơn vị tính => "-"
            supplies.selectpicker('val', ''); //tên thiết bị vật tư => ""
        }
        else $.notify('Vui lòng chọn vật tư!', { type: 'danger' });

        $(wrapper).find(`tr[id=${category_id}]`).on("click", `.remove_field`, function (e) { //user click on remove text
            e.preventDefault();
            const $this = $(this);

            $this.parent('td').parent('tr').remove();
            $('#kt-select-supplies').find(`option[value="${category_id}"]`).attr('disabled', false); //Disabled option của chọn vật tư
            $('#kt-select-supplies').selectpicker('refresh');
        });
    }


    return {
        // Public functions
        init: function () {
            initSearch();
            initEventListeners();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-bunker').length) {
        DMMSBunker.init();
    }
});