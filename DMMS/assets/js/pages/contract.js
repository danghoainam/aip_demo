"use strict";
// Class definition

var DMMSContract = function () {
    var dataTableInstance = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    var MAX_ITEM_INPAGE = 0;

    var initTable = function () {
        //var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
        //if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
        //    $.notify(editSucessCache.massage, { type: 'success' });
        //    localStorage.removeItem('edit_success');
        //}
        dataTableInstance = $('.kt-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/contract/gets',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            debugger
                            if (raw.items != null) {
                                $.each(raw.items, function (index, item) {
                                    if (item.projectName != null) {
                                        item.projectName = item.projectName.replace(/</g, "&lt;").replace(/>/g, "&gt;");
                                    }
                                  
                                })
                            }
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page;
                            CURRENT_PAGE = page || 1;
                            START_PAGE = 0;
                            LIMIT = perpage;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword, contractStatus, customer_uuid, investor } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: (keyword || $('#js-filter-keyword').val()).trim(),                            
                            contractStatus: (contractStatus || $('#js-filter-state').val()),                            
                            customer_uuid: (customer_uuid || $('#js-filter-customer').val()),                            
                            investor: (investor || $('#js-filter-investor').val()),                            
                            page: pagination.page
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,
            },

            columns: [
                {
                    field: '',
                    title: '#',
                    sortable: false,
                    width: 35,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'contractCode',
                    title: 'Số hiệu HĐ',
                    width: 80,
                },{
                    field: 'projectCode',
                    title: 'Mã DA',
                    width: 80,
                },
                {
                    field: 'projectName',
                    title: 'Dự án',
                    template: function (data) {
                        return `<a href="project/edit/${data.projectUuid}" title="Thông tin dự án"">
                                    ${data.projectName}
                                </a>`;
                    },
                },
                {
                    field: 'consumerCount',
                    title: 'Đơn vị thụ hưởng',
                    width: 100,
                },
                {
                    field: 'investorName',
                    title: 'Chủ đầu tư',
                    width: 120,
                },
                {
                    field: 'timeValidValue',
                    title: 'Thời hạn HĐ',
                    width: 100,
                    template: function (data) {
                        if (data.timeValidValue != null) {
                            return data.timeValidValue + ' tháng';
                        }
                        else {
                            return  '0 tháng';
                        }
                    },
                },
                {
                    field: 'timeValidEnd_format',
                    title: 'Thời gian kết thúc HĐ',
                    width: 110,
                },
                {
                    field: 'state',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    width: 110,
                    template: function (data) {
                        debugger
                        var states = {
                            1: { 'title': 'Đang bảo hành', 'class': 'kt-badge--success' },
                            2: { 'title': 'Hết bảo hành', 'class': 'kt-badge--warning' },
                            3: { 'title': 'Đã gửi thông báo', 'class': 'kt-badge--primary' },
                            4: { 'title': 'Đã ký đóng', 'class': ' kt-badge--primary' },
                            5: { 'title': 'Đóng bảo hành', 'class': 'kt-badge--info' },
                            6: { 'title': 'Gia hạn bảo hành', 'class': 'kt-badge--success' },
                            7: { 'title': 'Chưa nghiệm thu', 'class': 'kt-badge--danger' },
                        };
                        if (data.stateId) {
                            return '<span class="kt-badge ' + states[data.stateId].class + ' kt-badge--inline kt-badge--pill">' + states[data.stateId].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới tạo</span>';
                        }
                    }
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 50,
                    overflow: 'visible',
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        return `
                            <a href="contract/details/${data.uuid}" title="Chi tiết" class="btn btn-sm btn-success btn-icon btn-icon-md">
                                <i class="la la-info"></i>
                            </a>
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        })
    };


    //table contact
    $(".js-list-customer").on('click', function () {
        const contract_uuid = $('#uuid').val();
        dataTableInstance = $('.kt-datatable--customer').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: `/contract/getcontractcus/${contract_uuid}`,
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? undefined : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page
                            CURRENT_PAGE = page || 1;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword } = query;
                        MAX_ITEM_INPAGE = pagination.perpage || LIMIT;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE
                        }
                        return {
                            limit: MAX_ITEM_INPAGE,
                            keyword: (keyword || $('#js-filter-cus-keyword').val()),
                            page: pagination.page,                            
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,
            },
            columns: [
                {
                    field: '',
                    title: '#',
                    sortable: false,
                    width: 20,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * MAX_ITEM_INPAGE;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                //{
                //    field: 'code',
                //    title: 'Mã đơn vị',
                //    width: 60,
                //},
                {
                    field: 'customerName',
                    title: 'Tên đơn vị triển khai',
                },
                //{
                //    field: 'address',
                //    title: 'Địa chỉ chi tiết',
                //},
                {
                    field: 'village.name',
                    title: 'Phường/Xã',
                    width: 120
                },
                {
                    field: 'town.name',
                    title: 'Quận/Huyện',
                    width: 120
                },
                {
                    field: 'province.name',
                    title: 'Tỉnh/Thành',
                    width: 120
                },
                {
                    field: 'packageDeployCount',
                    title: 'Số lượng phòng triển khai',
                    textAlign: 'center',
                    width: 110
                },
                {
                    field: 'countDevice',
                    title: 'Số lượng thiết bị',
                    textAlign: 'center',
                    width: 100
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    });
    
    var initSearch = function () {
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });
        $('#js-filter-cus-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });
        $('#js-filter-state')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'contractStatus');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-customer')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'customer_uuid');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-investor')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'investor');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
    };

    return {
        // Public functions
        init: function () {
            //initUserForm();
            initTable();          
            initSearch();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-contract').length) {
        DMMSContract.init();
    }
});
