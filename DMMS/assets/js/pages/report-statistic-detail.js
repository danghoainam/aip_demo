﻿"use strict";
// Class definition

var DMMSProject = function () {
    // Private functions
    let teamUuid = null;
    let provinceId = null;
    let fieldId = null;
    let unitSignedUuIId = null;

    var dataTableInstance = null;
    var dataTableInstance2 = null;
    var drawer = null;
    var Packagetable = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    var typeViewByDateTime = 1;
    // demo initializer

    let tFrom = moment().subtract(6, 'days').format('YYYY-MM-DD').toString();
    let tTo = moment().format('YYYY-MM-DD').toString();

    var startDatePicker = function () {
        $('#kt_dashboard_daterangepicker_title').html("");
        $("#kt_dashboard_daterangepicker_date").text(moment().subtract(6, 'days').format('DD-MM-YYYY').toString() + " - " + moment().format('DD-MM-YYYY').toString());
    }

    var daterangepickerInit = function () {
        if ($('#kt_dashboard_daterangepicker').length == 0) {
            return;
        }
        var picker = $('#kt_dashboard_daterangepicker');
        var start = moment().subtract(6, 'days');
        var end = moment();
        function cb(start, end, label) {
            var title = '';
            var range = '';
            if ((end - start) < 100 || label == 'Hôm nay') {
                title = 'Hôm nay: ';
                range = start.format('DD-MM-YYYY');
            } else if (label == 'Hôm qua') {
                title = 'Hôm qua: ';
                range = start.format('DD-MM-YYYY');
            } else {
                range = start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY');
            }
            tFrom = start.format('YYYY-MM-DD');
            tTo = end.format('YYYY-MM-DD');           
            initTable(tFrom, tTo, typeViewByDateTime);
            $('#kt_dashboard_daterangepicker_date').html(range);
            $('#kt_dashboard_daterangepicker_title').html(title);         
            clickEventChangeChart(tFrom, tTo, teamUuid, provinceId, fieldId, unitSignedUuIId);
        }
        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Hôm nay': [moment(), moment()],
                'Hôm qua': [moment().subtract(1, 'days'), moment()],
                '7 ngày qua': [moment().subtract(6, 'days'), moment()],
                '30 ngày qua': [moment().subtract(29, 'days'), moment()],
                'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            locale: {
                applyLabel: 'Áp dụng',
                cancelLabel: 'Hủy bỏ',
                customRangeLabel: 'Tùy chỉnh',
                firstDay: 1
            }
        }, cb);
    }
    
    var initTable = function (timeFrom, timeTo, typeViewByDateTime) {
        if (typeViewByDateTime == 1) {
            if (dataTableInstance != null && dataTableInstance != undefined) dataTableInstance.destroy();
            dataTableInstance = $('.kt-datatable').KTDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '/ReportOverview/GetReportStatisticDetailProject',
                        },
                        response: {
                            map: function (res) {
                                const raw = res && res.data || {};
                                const { pagination } = raw;
                                const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                                const page = pagination === undefined ? 0 : pagination.page;
                                CURRENT_PAGE = page || 1;
                                START_PAGE = 0;
                                LIMIT = perpage;
                                return {
                                    data: raw.items || [],
                                    meta: {
                                        ...pagination || {},
                                        perpage: perpage || LIMIT,
                                        page: page || 1,
                                    },
                                }
                            },
                        },
                        filter: function (data = {}) {
                            const query = data.query || {};
                            const pagination = data.pagination || {};
                            const { unitSignedUuid, fieldId, provinceId, teamUuid, keyword } = query;
                            if (START_PAGE === 1) {
                                pagination.page = START_PAGE;
                            }
                            return {
                                limit: pagination.perpage || LIMIT,
                                unitSignedUuid: unitSignedUuid || $('#js-filter-unitSignedUuid').val(),
                                fieldId: fieldId || $('#js-filter-field').val(),
                                provinceId: provinceId || $('#js-filter-location').val(),
                                teamUuid: teamUuid || $('#js-filter-team').val(),
                                keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                                timeFrom: timeFrom,
                                timeTo: timeTo,
                                page: pagination.page,

                            };
                        }
                    },
                    pageSize: LIMIT,
                    serverPaging: true,
                    serverFiltering: true,
                },

                search: {
                    //input: $('#js-filter-keyword'),
                    onEnter: true,
                },
                columns: [
                    {
                        field: 'uuid',
                        title: 'STT',
                        width: 30,
                        textAlign: 'center',
                        template: function (data, row) {
                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                        },
                        responsive: {
                            visible: 'md',
                            hidden: 'sm'
                        }
                    },
                    {
                        field: 'code',
                        title: 'Mã dự án',
                        textAlign: 'center',
                        width: 70
                    },
                    {
                        field: 'project_name',
                        title: 'Tên dự án đang triển khai',

                    },
                    {
                        field: 'field_name',
                        title: 'Lĩnh vực',
                        width: 90
                    },
                    {
                        field: 'province_name',
                        title: 'Tỉnh triển khai',
                        width: 100
                    },
                    {
                        field: 'customer_count',
                        title: 'Số ĐVTK',
                        textAlign: 'center',
                        width: 50
                    },
                    {
                        field: 'package_deloy_count',
                        title: 'Sản phẩm',
                        textAlign: 'center',
                        width: 50
                    },
                    {
                        field: 'code_contract',
                        title: 'Số hiệu hợp đồng',
                        width: 150
                    },

                    {
                        field: 'time_start_format',
                        title: 'Ngày bàn giao',
                        width: 90
                    },
                    {
                        field: 'time_valid',
                        title: 'Thời hạn BH',
                        textAlign: 'center',
                        width: 90
                    }],
                sortable: false,
                pagination: true,
                responsive: true,
            });
        }
        else if (typeViewByDateTime == 2)
        {
            if (dataTableInstance != null && dataTableInstance != undefined) dataTableInstance.destroy();
            dataTableInstance = $('.kt-datatable-project-detail').KTDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '/ReportOverview/GetReportStatisticDetailProjectTab2',
                        },
                        response: {
                            map: function (res) {
                                const raw = res && res.data || {};
                                const { pagination } = raw;
                                const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                                const page = pagination === undefined ? 0 : pagination.page;
                                CURRENT_PAGE = page || 1;
                                START_PAGE = 0;
                                LIMIT = perpage;
                                return {
                                    data: raw.items || [],
                                    meta: {
                                        ...pagination || {},
                                        perpage: perpage || LIMIT,
                                        page: page || 1,
                                    },
                                }
                            },
                        },
                        filter: function (data = {}) {
                            const query = data.query || {};
                            const pagination = data.pagination || {};
                            const { unitSignedUuid, fieldId, provinceId, teamUuid, keyword } = query;
                            if (START_PAGE === 1) {
                                pagination.page = START_PAGE;
                            }
                            return {
                                limit: pagination.perpage || LIMIT,
                                unitSignedUuid: unitSignedUuid || $('#js-filter-unitSignedUuid').val(),
                                fieldId: fieldId || $('#js-filter-field').val(),
                                provinceId: provinceId || $('#js-filter-location').val(),
                                teamUuid: teamUuid || $('#js-filter-team').val(),
                                keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                                timeFrom: timeFrom,
                                timeTo: timeTo,
                                page: pagination.page,

                            };
                        }
                    },
                    pageSize: LIMIT,
                    serverPaging: true,
                    serverFiltering: true,
                },

                search: {
                    //input: $('#js-filter-keyword'),
                    onEnter: true,
                },
                columns: [
                    {
                        field: 'uuid',
                        title: 'STT',
                        width: 30,
                        textAlign: 'center',
                        template: function (data, row) {
                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                        },
                        responsive: {
                            visible: 'md',
                            hidden: 'sm'
                        }
                    },
                    {
                        field: 'code',
                        title: 'Mã dự án',
                        textAlign: 'center',
                        width: 70
                    },
                    {
                        field: 'project_name',
                        title: 'Tên dự án đang triển khai',

                    },
                    {
                        field: 'field_name',
                        title: 'Lĩnh vực',
                        width: 90
                    },
                    {
                        field: 'province_name',
                        title: 'Tỉnh triển khai',
                        width: 90
                    },
                    {
                        field: 'package_deloy_count',
                        title: 'Sản phẩm',
                        textAlign: 'center',
                        width: 50
                    },
                    {
                        field: 'code_contract',
                        title: 'Số hiệu hợp đồng',
                        width: 150
                    },
                    {
                        field: 'unitsigned_name',
                        title: 'Đơn vị ký hợp đồng',
                        width: 90
                    },
                    {
                        field: 'customer_name',
                        title: 'Đơn vị triển khai',
                        width: 90
                    },
                    {
                        field: 'package_code_name',
                        title: 'Tên sản phẩm',
                        width: 100
                    },
                    {
                        field: 'time_end_format',
                        title: 'Ngày bàn giao',
                        width: 80
                    },
                    {
                        field: 'time_valid',
                        title: 'Thời hạn BH',
                        textAlign: 'center',
                        width: 80
                    }],
                sortable: false,
                pagination: true,
                responsive: true,
            });
        }
        else {
            if (dataTableInstance != null && dataTableInstance != undefined) dataTableInstance.destroy();
            dataTableInstance = $('.kt-datatable-detail2').KTDatatable({
                data: {
                    type: 'remote',
                    source: {
                        read: {
                            url: '/ReportOverview/GetReportStatisticDetailArea',
                        },
                        response: {
                            map: function (res) {
                                const raw = res && res.data || {};
                                const { pagination } = raw;
                                const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                                const page = pagination === undefined ? 0 : pagination.page;
                                CURRENT_PAGE = page || 1;
                                START_PAGE = 0;
                                LIMIT = perpage;
                                return {
                                    data: raw.items || [],
                                    meta: {
                                        ...pagination || {},
                                        perpage: perpage || LIMIT,
                                        page: page || 1,
                                    },
                                }
                            },
                        },
                        filter: function (data = {}) {
                            const query = data.query || {};
                            const pagination = data.pagination || {};
                            const { unitSignedUuid, fieldId, provinceId, teamUuid, keyword } = query;
                            if (START_PAGE === 1) {
                                pagination.page = START_PAGE;
                            }
                            return {
                                limit: pagination.perpage || LIMIT,
                                unitSignedUuid: unitSignedUuid || $('#js-filter-unitSignedUuid').val(),
                                fieldId: fieldId || $('#js-filter-field').val(),
                                provinceId: provinceId || $('#js-filter-location').val(),
                                teamUuid: teamUuid || $('#js-filter-team').val(),
                                keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                                timeFrom: timeFrom,
                                timeTo: timeTo,
                                page: pagination.page,

                            };
                        }
                    },
                    pageSize: LIMIT,
                    serverPaging: true,
                    serverFiltering: true,
                },

                search: {
                    //input: $('#js-filter-keyword'),
                    onEnter: true,
                },
                columns: [
                    {
                        field: 'province_id',
                        title: 'STT',
                        width: 30,
                        textAlign: 'center',
                        template: function (data, row) {
                            return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                        },
                        responsive: {
                            visible: 'md',
                            hidden: 'sm'
                        }
                    },
                    {
                        field: 'name',
                        title: 'Khu vực triển khai dự án',

                    },
                    {
                        field: 'project_deploy',
                        title: 'Số dự án triển khai',
                        width: 60
                    },
                    {
                        field: 'customer_deploy',
                        title: 'Số ĐVTK',
                        width: 50
                    },
                    {
                        field: 'package_deploy',
                        title: 'Sản phẩm',
                        width: 60
                    },
                    {
                        field: 'device_deploy',
                        title: 'Số lượng thiết bị',
                        width: 70
                    },
                    {
                        field: 'cost_format',
                        title: 'Tổng giá trị dự án',
                        width: 150
                    }],
                sortable: false,
                pagination: true,
                responsive: true,
            });
        }
    };

    $('.link-report-statistic-detail-project').on("click", function () {
        typeViewByDateTime = 1;
        initTable(tFrom, tTo, 1);     
    });

    $('.link-report-statistic-detail-project-2').on("click", function () {
        typeViewByDateTime = 2;
        initTable(tFrom, tTo, 2);
    });

    $('.link-report-statistic-detail-area').on("click", function () {
        typeViewByDateTime = 3;
        initTable(tFrom, tTo, 3);     
    });

    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var initSearch = function () {
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            }).selectpicker();

        $('#js-filter-unitSignedUuid')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'unitSignedUuid');
                    START_PAGE = 1;
                }
                teamUuid = $(this).val();
                clickEventChangeChart(tFrom, tTo, teamUuid, provinceId, fieldId, unitSignedUuIId);
            }).selectpicker();

        $('#js-filter-field')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'fieldId');
                    START_PAGE = 1;
                }
                fieldId = $(this).val();
                clickEventChangeChart(tFrom, tTo, teamUuid, provinceId, fieldId, unitSignedUuIId);
            }).selectpicker();

        $('#js-filter-location')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'provinceId');
                    START_PAGE = 1;
                }
                provinceId = $(this).val();
                clickEventChangeChart(tFrom, tTo, teamUuid, provinceId, fieldId, unitSignedUuIId);
            }).selectpicker();

        $('#js-filter-team')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'teamUuid');
                    START_PAGE = 1;
                }
                unitSignedUuIId = $(this).val();
                clickEventChangeChart(tFrom, tTo, teamUuid, provinceId, fieldId, unitSignedUuIId);
            }).selectpicker();
        //if ($("#location_id").val() !== '') {
        //    $('#kt-select-location').selectpicker('val', $("#location_id").val());
        //}
        //else {
        //    $('#kt-select-location').selectpicker();
        //}
    };

    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-project__form');
        initForm($form);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

    };

    //start landd---------------------------------------
    var innitBieuDoTron1 = function (dataSeries, total) {
        let optionsDuAn = {
            series: dataSeries,
            chart: {
                width: 280,
                type: 'pie',
            },
            colors: ['#F64E60', '#8950FC', '#FFA800', '#36B37E'],
            legend: {
                display: true,
                show: false,
            },
            labels: ['Giáo dục & đào tạo', 'Y tế & sức khỏe', 'Xử lý môi trường', 'Quan trắc môi trường'],
            dataLabels: {
                enabled: true,
                style: {
                    colors: ['#fff'],
                    fontSize: '14px',
                },
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        minAngleToShowLabel: 10,
                        offset: -5,
                    },
                }
            },
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                }
            }],
            noData: {
                text: "Không có dữ liệu hiển thị báo cáo",
                align: 'center',
                verticalAlign: 'middle',
                offsetX: 0,
                offsetY: 0,
                style: {
                    fontSize: '14px',
                }
            },
            tooltip: {
                enabled: true,
                y: {
                    formatter: function (value) {
                        return (value / total * 100).toFixed(1) + "% (" + value + ")";
                    }
                }
            },

        };
        var charpie1 = new ApexCharts(document.querySelector("#charpie1"), optionsDuAn);
        charpie1.render();
    }

    var innitBieuDoTron2 = function (dataSeries, total) {
        let optionsDonVi = {
            series: dataSeries,
            chart: {
                width: 280,
                type: 'pie',
            },
            colors: ['#F64E60', '#8950FC', '#FFA800', '#36B37E'],
            legend: {
                display: true,
                show: false,
            },
            labels: ['Giáo dục & đào tạo', 'Y tế & sức khỏe', 'Xử lý môi trường', 'Quan trắc môi trường'],
            dataLabels: {
                style: {
                    colors: ['#fff'],
                    fontSize: '14px',
                },
                // offsetX: 50,
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        minAngleToShowLabel: 10,
                        offset: -5,
                    },
                }
            },
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                }
            }],
            noData: {
                text: "Không có dữ liệu hiển thị báo cáo",
                align: 'center',
                verticalAlign: 'middle',
                offsetX: 0,
                offsetY: 0,
                style: {
                    fontSize: '14px',
                }
            },
            tooltip: {
                enabled: true,
                y: {
                    formatter: function (value) {
                        return (value / total * 100).toFixed(1) + "% (" + value + ")";
                    }
                }
            },
        };
        var charpie2 = new ApexCharts(document.querySelector("#charpie2"), optionsDonVi);
        charpie2.render();
    }

    var innitBieuDoTron3 = function (dataSeries, total) {
        let optionsThietBi = {
            series: dataSeries,
            chart: {
                width: 280,
                type: 'pie',
            },
            colors: ['#F64E60', '#8950FC', '#FFA800', '#36B37E'],
            legend: {
                display: true,
                show: false,
            },
            labels: ['Giáo dục & đào tạo', 'Y tế & sức khỏe', 'Xử lý môi trường', 'Quan trắc môi trường'],
            dataLabels: {
                style: {
                    colors: ['#fff'],
                    fontSize: '14px',

                },
                // offsetX: 50,
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        minAngleToShowLabel: 10,
                        offset: -5,
                    },
                }
            },
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                }
            }],
            noData: {
                text: "Không có dữ liệu hiển thị báo cáo",
                align: 'center',
                verticalAlign: 'middle',
                offsetX: 0,
                offsetY: 0,
                style: {
                    fontSize: '14px',
                }
            },
            tooltip: {
                enabled: true,
                y: {
                    formatter: function (value) {
                        return (value / total * 100).toFixed(1) + "% (" + value + ")";
                    }
                }
            },
        };
        var charpie3 = new ApexCharts(document.querySelector("#charpie3"), optionsThietBi);
        charpie3.render();
    }

    var pieChartProjectInit = function (timeFrom, timeTo, teamId, provinceId, fieldId, unitSignedUuIId) {
        let seriesThongKeDuAn = [];
        let seriesDonVi = [];
        let seriesThietBi = [];
        let totalDuAn = 0;
        let totalDonVi = 0;
        let totalThietBi = 0;
        KTApp.block($('.w-100'));
        $.ajax({
            url: `/projectoverview/all`,
            method: 'POST',
            cache: false,
            data: {
                request: {
                    timeFrom: timeFrom, timeTo: timeTo, teamId: teamId, provinceId: provinceId, fieldId: fieldId, unitSignedUuid: unitSignedUuIId
                }
            }
        })
            .done(function (res) {
                if (res) {
                    if (res.data.items != null) {
                        $.each(res.data.items, function (index, item) {
                            if (index == 0) {
                                $("#duantrienkhai").text(item.data[0].count);
                            };
                            if (index == 1) {
                                item.data.forEach(function (item) {
                                    seriesThongKeDuAn.push(item.count);
                                    totalDuAn += parseInt(item.count);
                                });
                            };
                            if (index == 2) {
                                $("#donvitrienkhai").text(item.data[0].count);
                            };
                            if (index == 3) {
                                item.data.forEach(function (item) {
                                    seriesDonVi.push(item.count);
                                    totalDonVi += parseInt(item.count);
                                });
                            };
                            if (index == 4) {
                                $("#sanphamtrienkhai").text(item.data[0].count);
                            };
                            if (index == 6) {
                                $("#thietbitrienkhai").text(item.data[0].count);
                            };
                            if (index == 7) {
                                item.data.forEach(function (item) {
                                    seriesThietBi.push(item.count);
                                    totalThietBi += parseInt(item.count);
                                });
                            };
                        });
                    };
                    innitBieuDoTron1(seriesThongKeDuAn, totalDuAn);
                    innitBieuDoTron2(seriesDonVi, totalDonVi);
                    innitBieuDoTron3(seriesThietBi, totalDonVi);
                    //$("#duantrienkhai").text(totalDuAn);
                    //$("#donvitrienkhai").text(totalDonVi);
                    KTApp.unblock($('.w-100'));
                }
            })
            .fail(function () {
                $.notify('Có lỗi xảy ra, vui lòng thử lại.', { type: 'danger' });
            });
    }

    var clickEventChangeChart = function (tFrom, tTo, teamId, provinceId, fieldId, unitSignedUuIId) {
        $('#charpie1').remove();
        $('.charpie1').append('<div id="charpie1"></div>');
        $('#charpie2').remove();
        $('.charpie2').append('<div id="charpie2"></div>');
        $('#charpie3').remove();
        $('.charpie3').append('<div id="charpie3"></div>');
        $('#chartmix').remove();
        $('.chartmix').append('<div id="chartmix"></div>');
        pieChartProjectInit(tFrom, tTo, teamId, provinceId, fieldId, unitSignedUuIId);
    }
    //landd end------------------------------------------------------------------------


    return {
        // Public functions
        init: function () {   
            startDatePicker();
            initTable(tFrom, tTo, 1);
            initSearch();
            daterangepickerInit();
            pieChartProjectInit(tFrom, tTo, teamUuid, provinceId, fieldId, unitSignedUuIId);
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-reportstatisticdetail').length) {
        DMMSProject.init();
    }
});
