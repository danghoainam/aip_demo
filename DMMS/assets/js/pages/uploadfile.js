﻿"use strict"

// Class definition

var DMMSProjectCus = function () {
    // Private functions
    //
    var dataTableInstancedevice = null;
    var dataTableInstance1 = null;
    var drawer = null;
    var subdrawer = null;
    var uuid_images = "";
    var LIMIT = 20;
    var START_PAGE = 0;
    var CURRENT_PAGE = 1;
    // demo initializer

    //var reloadTable = function () {
    //    if (dataTableInstance) {
    //        dataTableInstance.reload();
    //    }
    //};
    var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
    if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
        $.notify(editSucessCache.massage, { type: 'success' });
        localStorage.removeItem('edit_success');
    }


    var initEventListeners = function () {
        $('body')
            .off('click', '#ImportDeivce')
            .on('click', '#ImportDeivce', function () {
                
                const $this = $(this);
                const id = $this.attr('data-id');
                initUpload(id || '');
            })
            .off('click', '.js-detail-prodevice')
            .on('click', '.js-detail-prodevice', function () {
                
                const $this = $(this);
                const id = $this.attr('data-id');
                initDetail(id || '');
            });

    };
    var initDetail = function (id) {
        debugger
        const title = `DANH SÁCH CHI TIẾT THIẾT BỊ`;
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--upload' });

        drawer.on('ready', () => {
            $.ajax({
                url: `/project/Detaildevice`,
                method: 'GET',
            })
                .done(function (response) {
                    debugger
                    const $content = drawer.setBody(response);
                    const $form = $content.find('.dmms-upload__form');

                    initContentdetail($content,id);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var initContentdetail = function ($content,id) {
        debugger
        var uuid = document.getElementById("project_uuid").value;
        dataTableInstance1 = $('.kt-datatable1').KTDatatable({

            data: {
                type: 'remote',
                source: {
                    read: {
                        url: `/project/ProjecDetailDevice/${id}`,
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page,
                                CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            //Gán các thuộc tính của images{0} vào đối tượng item
                            if (raw.items != null) {
                                (raw.items).forEach(function (e) {

                                    e.path = e.image != null ? e.image.path : null;

                                    return e;


                                })
                            }


                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword, project_uuid } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            project_uuid:uuid,
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,
            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 35,
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    },
                    autoHide: false,
                    textAlign: 'center',

                },
                {
                    field: 'serial',
                    title: 'Serial',
             

                },
                {
                    field: 'imei',
                    title: 'Imei',
                    width: 110,
                
                },
                {
                    field: 'supplier.name',
                    title: 'Nhà cung cấp',
                    width: 110,
                  
                },
                {
                    field: 'time_expireDevice_format',
                    title: 'Ngày hết hạn BH',
                    width: 110,
                
                },
                {
                    field: 'EMEI',
                    title: 'Phòng tiết bị',
                    width: 110,
                 
                },

                {
                    field: 'state',
                    title: 'Trạng thái',
                    width: 150,
                    template: function (data) {
                        debugger
                        var states = {
                            1: { 'title': 'Đang sử dụng', 'class': 'kt-badge--brand' },
                            2: { 'title': 'Không sử dụng', 'class': ' kt-badge--warning' },
                           
                        };
                        if (data.state.id) {
                            return '<span class="kt-badge ' + states[data.state.id].class + ' kt-badge--inline kt-badge--pill">' + states[data.state.id].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Chưa rõ</span>';
                        }
                    },
                    autoHide: false,
                    width: 150,
                    textAlign: 'center'
                },
            ],
            sortable: false,
            pagination: true,
            responsive: true,
        });
        const $form = $content.find('.dmms-upload__form');
        initForm($form);
        //const $form1 = $content.find('.dmms-knowledge__formqa');
        //initForm1($form1);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));


        /**
         * Init Dropzone
         */
        //initDropzone();
    };
    var initUpload = function (id) {
        var id = document.getElementById("project_uuid").value;
        const title = `THÊM THIẾT BỊ TỪ FILE EXCEL`;
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--upload' });

        drawer.on('ready', () => {
            $.ajax({
                url: `/project/UploadFileExcel/${id}`,
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    const $form = $content.find('.dmms-upload__form');
                    $('#kt-select-customer').selectpicker();
                    $('#kt-select-contract').selectpicker();
                    initContent($content);
                    $('#kt-select-customer').on('change', function () {
                        debugger
                        var contract_uuid = '';
                        if ($('#kt-select-contract').val() == '') {
                            contract_uuid = $('#kt-select-contract option:not(:selected)').val();
                        }
                        else {
                            contract_uuid = $('#kt-select-contract').val();
                        }
                                $('#kt-select-contract').val(contract_uuid);
                                $('#kt-select-contract').selectpicker('refresh');
                    })
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-upload__form');
        const $form1 = $content.find('.dmms-savedevice__form');




        initForm($form);
        initForm1($form1);

        /*
            init auto-resize
        */
        $('#kt-select-customer1').selectpicker();
        $('.kt-select-contract').selectpicker();

        autosize($content.find('textarea.auto-resize'));
        autoupload();



    };
    var initContentSave = function ($content1) {
        /*
            Init form
        */
        debugger
        const $form1 = $content1.find('.dmms-savedevice__form');
        initForm1($form1);
        /*
            init auto-resize
        */
        $('#kt-select-customer1').selectpicker();
        $('.kt-select-contract').selectpicker();

        autosize($content1.find('textarea.auto-resize'));
       



    };
    var initForm = function ($form) {
        
        //$('.submit').on('click', function () {
        //    $('[name^=listDevices]').each(function () {
        //        $(this).rules("add", {
        //            required: true,
        //            messages: {
        //                required: "Specify a valid email"
        //            }
        //        });
        //    });
        //})
        initValidation(true);
        
    }
    var initForm1 = function ($form1) {
        initValidation1(true);
    }
    var autoupload = function () {
        $('input[type=file]').on('change', function (event) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'center',
                'message': 'Vui lòng chờ...'
            });
            loading.show();
            KTApp.blockPage();
            var object = {};
            var files = event.target.files;
            event.stopPropagation(); // Stop stuff happening
            event.preventDefault(); // Totally stop stuff happening

            //Create a formdata object and add the files
            var data = new FormData();
            var file = event.target.files;
            data.append('FileUploadExcel', $('input[type=file]')[0].files[0]);
            data.forEach((value, key) => { object[key] = value });
            var json = JSON.stringify(object);
            console.log(json);
            var fileOther = $('#FileUploadExcel')[0];
            if (fileOther != undefined) {
                var file = fileOther.files[0];
                if (file != undefined) {
                    data.append("FileUploadExcel", file);
                }
            }
            data.append('jsonString', $('.dmms-upload__form').serializeJSON);
            $.ajax({
                url: `/project/AutoUploadFileExcel`,
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false,
            })
                .done(function (data) {
                    debugger
                    var project_uuid = $('#project_uuid').val();
                    var customer_uuid = $('#kt-select-customer').val();
                    if (customer_uuid == '') {
                        var cusselect = ``;
                    }
                    else {
                        var cusselect = `<option value="">Chọn sản phẩm</option>`;
                    }
                    
                    $.ajax({

                        url: `/project/ProjectGetCustomerPac`,
                        data: { customerUuid: customer_uuid, projectUuid: project_uuid}
                    })
                        .done(function (res) {
                            debugger
                            if (customer_uuid == '') {

                            }
                            else {
                                var select = res.data.items;
                                select.forEach(function (item, index) {
                                    cusselect += `<option value="${item.uuid}">${item.name}</option>`;

                                })
                                
                            }
                            initSaveDevice(data, cusselect, loading)
                            $('#kt-select-customer').on('change', function () {
                                //var contract_uuid = $('#kt-select-contract option:not(:selected)').val();
                                //$('#kt-select-contract').val(contract_uuid);
                                //$('#kt-select-contract').selectpicker('refresh');
                                debugger
                               
                                var project_uuid = $('#project_uuid').val();
                                var customer_uuid = $('#kt-select-customer').val();
                                if (customer_uuid == '') 
                                    {
                                    $('.table-bordered').find("tr").each(function () {
                                            $(this).children("td:eq(4)").remove();
                                            $(this).children("th:eq(4)").remove();
                                    });
                                    $('#kt-select-contract').val("");
                                    $('#kt-select-contract').selectpicker('refresh');
                                    return;
                                    }
                                    
                                var cusselect = `<option value="">Chọn sản phẩm</option>`;
                                $('.select-package').find('option').remove()
                                $.ajax({

                                    url: `/project/ProjectGetCustomerPac`,
                                    data: { customerUuid: customer_uuid, projectUuid: project_uuid }
                                })
                                    .done(function (res) {
                                        //var select = res.data.items;
                                        //select.forEach(function (item, index) {
                                        //    cusselect += `<option value="${item.uuid}">${item.name}</option>`;
                                            
                                        //})
                                        //$('.select-package').find('select').append(`
                                        //${cusselect}`)
                                        //$('.select-package').selectpicker('refresh');
                                        if (customer_uuid == '') {

                                        }
                                        else {
                                            var select = res.data.items;
                                            select.forEach(function (item, index) {
                                                cusselect += `<option value="${item.uuid}">${item.name}</option>`;

                                            })

                                        }
                                        initSaveDevice(data, cusselect, loading)
                                    })
                                    .fail(function () {

                                    })
                                
                            })
                        })
                        .fail(function () {

                        });
                    
                })
                .fail(function (data) {
                    debugger
                    loading.hide();
                    KTApp.unblockPage();
                    $('#devicetable').find('.table-bordered').remove();
                    $.notify('Có lỗi xảy ra !', { type: 'danger' });

                });
        }
        )
    };
    var initValidation = function () {
     
        $('.dmms-upload__form').validate({
            rules:
            {
                'listDevices[][packageUuid]': {
                    mytst: true
                }
                   
                //FileUploadExcel: {
                //    required: true,
                //},

            },
            messages: {
                'listDevices[][packageUuid]': {
                    mytst: 'Vui lòng chọn sản phẩm.'
                },

                //FileUploadExcel: {
                //    required: 'Vui lòng chọn file.'
                //}
            },

            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo('.dmms-upload__form', -200);
                $('.select-package').trigger("focusout");
                $("[name*= packageUuid]").each(function (i, j) {
                    debugger
                    if ($.trim($(this).val()) != '') {
                        $(this).parent().find('button').css('border-color', '#e2e5ec')
                        $(this).parent().find('.error').attr('hidden', true);
                    }
                });
                $("[name*= packageUuid]").unbind('hide.bs.select');
                $("[name*= packageUuid]").bind('hide.bs.select', function () {
                    debugger
                    if ($(this).val() != '') {
                        $(this).parent().find('button').css('border-color', '#e2e5ec')
                        $(this).parent().find('.error').attr('hidden', true);
                    }
                    else {
                        $(this).parent().find('button').css('border-color', 'red')
                        $(this).parent().find('.error').attr('hidden', false);
                    }
                    $('.select-package').trigger("focusout");
                    
                });
                $('#FileUploadExcel').on('hide.bs.select', function () {
                    $(this).trigger("focusout");
                });
            },
            submitHandler: function (form) {
               
                $(form).find('button[type="submit"]').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

                debugger
                const data = $(form).serializeJSON() || {};
                $.ajax({
                    url: `/project/UploadFileExcel`,
                    type: 'POST',
                    data: data,
                  
                })
                    .done(function (data) {
                        
                        KTApp.block('.dmms-upload__form');
                        $.notify('Thêm thành công!', { type: 'success' });
                        drawer.close();
                        reloadTable();
                    })
                    .fail(function (data) {
                        $(form).find('button[type="submit"]').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                        if (data.responseJSON.error == 5) {
                            $.notify(data.responseJSON.msg, { type: 'danger' });
                        }
                        else {
                            $.notify('Có lỗi xảy ra !', { type: 'danger' });
                        }
                        

                    });
                return false;
            },
        });
    }
    var initValidation1 = function () {

        $('.dmms-savedevice__form').validate({
            rules:
            {
               

            },
            messages: {
                
            },
            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo('.dmms-savedevice__form', -200);
            },
            submitHandler: function (form) {
                const data = $(form).serializeJSON() || {};
                $.ajax({
                    url: `/project/SaveDevice`,
                    type: 'POST',
                    data: data,
                })
                    .done(function (data) {
                        KTApp.block('.dmms-savedevice__form');
                        $.notify('Thêm thành công!', { type: 'success' });
                        subdrawer.close();
                        reloadTable();
                    })
                    .fail(function (data) {
                        $.notify('Có lỗi xảy ra !', { type: 'danger' });

                    });
                return false;
            },
        });
    }
    var initSaveDevice = function (data, cusselect,loading) {
        debugger
        
        var select = null;
        
        
        var datadevice = data;
                    debugger
                    var getuuidpro = $('#project_uuid').val();
                    //const $content1 = subdrawer.setBody(response);
                    
        $('.addproject_uuid').val(getuuidpro);
        if (cusselect == "") {
            var html = `<table class="table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">Loại thiết bị</th>
                                        <th scope="col">Nhà cung cấp</th>
                                        <th scope="col">Serial</th>
                                        <th scope="col">Ngày hết hạn BH</th>
                                        <th scope="col">Tác vụ</th>
                                    </tr>
                                </thead>
                                <tbody class="table-detailsupplies">
                                    
                                    `;
        }
        else {
            var html = `<table class="table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">Loại thiết bị</th>
                                        <th scope="col">Nhà cung cấp</th>
                                        <th scope="col">Serial</th>
                                        <th scope="col">Ngày hết hạn BH</th>
                                        <th scope="col">Sản phẩm</th>
                                        <th scope="col">Tác vụ</th>
                                    </tr>
                                </thead>
                                <tbody class="table-detailsupplies">
                                    
                                    `;
        }
        
                    
                    datadevice.forEach(function (item, index) {
                        debugger
                        var stringexdevice = item.expireDevice;
                        var momentObj = moment(stringexdevice);
                        var momentString = momentObj.format('DD-MM-YYYY');
                        var stringexcontract = item.expiredContract;
                        var momentObj1 = moment(stringexcontract);
                        var momentString1 = momentObj1.format('DD-MM-YYYY');
                        var textcolor = '';
                        var icon = '';
                        if (item.code == 1) {
                            textcolor = "text-danger";
                            icon = `class ="fa flaticon-warning"`;
                        }
                        else{
                            textcolor = "text-primary"
                            
                        }
                        item.category_name == null ? "Không rõ" : item.category_name;
                        //if (item.supplier_name == null) {
                        //    item.supplier_name = "Không rõ"
                        //}
                        item.supplier_name = item.supplier_name == null ? "Không rõ" : item.supplier_name;
                        if (cusselect == "") {
                            html += `
                                       <tr>
                                      <td><input name="listDevices[][categoryId]" value="${item.categoryId}" hidden/><span class="${textcolor}">${item.category_name}</span><span ${icon} style="color: #f39605;margin-left: 3px;"></span></td>
                                      <td><input name="listDevices[][supplierId]" value="${item.supplierId}" hidden/><span class="${textcolor}">${item.supplier_name}</span><span ${icon} style="color: #f39605;margin-left: 3px;"></span></td>
                                      <td><input name="listDevices[][serial]" value="${item.serial}" hidden/>${item.serial}</td>
                                      <td><input name="listDevices[][expireDevice]" value="${item.expireDevice}" hidden>${momentString}</span></td>
                                      </td>
                                      <td><i class="la la-trash kt-p-0-tablet-and-mobile js-delete-device" style="background-color: #f45c5c;"></i></td> 
                                      </tr>
                                     `       
                        }
                        else {
                            html += `
                                       <tr>
                                      <td><input name="listDevices[][categoryId]" value="${item.categoryId}" hidden/><span class="${textcolor}">${item.category_name}</span><span ${icon} style="color: #f39605;margin-left: 3px;"></span></td>
                                      <td><input name="listDevices[][supplierId]" value="${item.supplierId}" hidden/><span class="${textcolor}">${item.supplier_name}</span><span ${icon} style="color: #f39605;margin-left: 3px;"></span></td>
                                      <td><input name="listDevices[][serial]" value="${item.serial}" hidden/>${item.serial}</td>
                                      <td><input name="listDevices[][expireDevice]" value="${item.expireDevice}" hidden>${momentString}</span></td>
                                      <td><select class="form-control select-package" name="listDevices[][packageUuid]">
                                        ${cusselect}
                                      </select>
                                      </td>
                                      <td><i class="la la-trash kt-p-0-tablet-and-mobile js-delete-device" style="background-color: #f45c5c;"></i></td> 
                                      </tr>
                                     `       
                        }
                                        
                    });
                     html += `</tbody></table>`;
                    $('#devicetable').find('.table-bordered').remove();
                    $('#devicetable').append(html);
                    $('.select-package').selectpicker();
                    loading.hide();
                    KTApp.unblockPage();
                    $('.js-delete-device').on('click', function () {
                        debugger
                        const $this = $(this);
                        $this.parent('td').parent('tr').remove();
                    })
                    //initContentSave($content1);
                    
                    
        
    };
    $(".linkprodevice").on("click", function () {
        if (!$('.nav-item').find('.active').hasClass("linkprodevice")) {
            var projectUuid = document.getElementById("project_uuid").value;
            //if (dataTableInstancedevice != null) dataTableInstancedevice.destroy();
            if (dataTableInstancedevice != null) {
                dataTableInstancedevice.load();
            }
            else{
                dataTableInstancedevice = $('.kt-datatablepro').KTDatatable({
                    data: {
                        type: 'remote',
                        source: {
                            read: {

                                url: `/project/projectgetdevice/${projectUuid}`,
                            },

                            response: {

                                map: function (res) {
                                    debugger
                                    const raw = res && res.data || {};
                                    const { pagination } = raw;
                                    const perpage = pagination === undefined ? undefined : pagination.perpage;
                                    const page = pagination === undefined ? undefined : pagination.page
                                    CURRENT_PAGE = page || 1;
                                    LIMIT = perpage;
                                    START_PAGE = 0;
                                    return {
                                        data: raw.items || [],
                                        meta: {
                                            ...pagination || {},
                                            perpage: perpage || LIMIT,
                                            page: page || 1,
                                        },
                                    }
                                },
                            },
                            filter: function (data = {}) {
                                const query = data.query || {};
                                const pagination = data.pagination || {};
                                const { categoryId, customerUuid, supplierID, keyword } = query;

                                if (START_PAGE === 1) {
                                    pagination.page = START_PAGE;
                                }
                                return {
                                    limit: pagination.perpage || LIMIT,
                                    categoryId: categoryId || $('#js-filter-categoryIddevice').val(),
                                    supplierID: supplierID || $('#js-filter-supplierID').val(),
                                    customerUuid: customerUuid || $('#js-filter-customerUuid').val(),
                                    keyword: (keyword || $('#js-filter-keyworddevice').val()).trim(),
                                    page: pagination.page,
                                };
                            }
                        },
                        pageSize: LIMIT,
                        serverPaging: true,
                        serverFiltering: true,
                    },

                    search: {
                        onEnter: true,

                    },
                    columns: [
                        {
                            field: 'uuid',
                            title: '#',
                            width: 20,
                            textAlign: 'center',
                            template: function (data, row) {
                                return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                            },
                            responsive: {
                                visible: 'md',
                                hidden: 'sm'
                            }
                        },
                        {
                            field: 'code',
                            title: 'Mã thiết bị',
                            width: 100
                        },
                        {
                            field: 'name',
                            title: 'Tên thiết bị - Vật tư',
                            template: function (data) {
                                var output = '<strong>' + data.prefixName + '</strong>';
                                if (data.name) {
                                    output += '<br/>' + data.name + '</span>';
                                }
                                return output;
                            },
                        },
                        {
                            field: 'serial',
                            title: 'Serial',
                            width: 120,
                        },
                        {
                            field: 'supplierName',
                            title: 'Nhà cung cấp',
                            width: 120,
                        },
                        {
                            field: 'packageName',
                            title: 'Phòng thiết bị',
                            width: 120,
                        },
                        {
                            field: 'expiredDevice_nohh',
                            title: 'Thời gian TB',
                            width: 120,
                        },
                        {
                            field: 'state',
                            title: 'Trạng thái',
                            width: 150,
                            template: function (data) {
                                var states = {
                                    1: { 'title': 'Hoạt động', 'class': 'kt-badge--success' },
                                    2: { 'title': 'Sửa chữa', 'class': ' kt-badge--warning' },
                                    3: {
                                        'title': 'Đến lịch bảo hành', 'class': ' kt-badge--primary', 'style': 'background-color:#074648c4'
                                    },
                                    4: { 'title': 'Sửa chữa', 'class': 'kt-badge--warning' },
                                    5: { 'title': 'Sửa chữa', 'class': 'kt-badge--warning' },
                                    6: { 'title': 'Sửa chữa', 'class': 'kt-badge--warning' },
                                    7: { 'title': 'Sửa chữa', 'class': 'kt-badge--warning' },
                                    8: { 'title': 'Thay thế tạm thời', 'class': 'kt-badge--info' },
                                    9: { 'title': 'Hỏng', 'class': 'kt-badge--danger' },
                                    10: { 'title': 'Hoạt động', 'class': 'kt-badge--danger' },
                                    11: { 'title': 'Hỏng', 'class': 'kt-badge--warning' },
                                    12: { 'title': 'Hoạt động', 'class': 'kt-badge--success' },
                                    13: { 'title': 'Đã xuất thanh lý', 'class': 'kt-badge--primary' },
                                    14: { 'title': 'Đã xuất tiêu hao', 'class': 'kt-badge--primary' },
                                    15: { 'title': 'Đã hoàn trả nhà cung cấp', 'class': 'kt-badge--primary' },
                                    16: { 'title': 'Thêm mới/cập nhật thiết bị', 'class': 'kt-badge--info' },
                                    17: { 'title': 'Hoạt động', 'class': 'kt-badge--success' },
                                };
                                if (data.state) {
                                    return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill"' + 'style="' + states[data.state].style + '">' + states[data.state].title + '</span>';
                                }
                                else {
                                    return '<span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Mới</span>';
                                }
                            },
                            width: 150,
                            textAlign: 'center'
                        },
                        //{
                        //    field: 'Tác vụ',
                        //    title: 'Tác vụ',
                        //    sortable: false,
                        //    width: 50,
                        //    overflow: 'visible',
                        //    autoHide: false,
                        //    textAlign: 'center',
                        //    template: function (data) {
                        //        return `
                        //            <button data-id="${data.id}" title="Chi tiết" class="js-detail-prodevice btn btn-sm btn-success btn-icon btn-icon-md">
                        //                <i class="la la-info"></i>
                        //            </button>
                        //        `;
                        //    },





                        //    //{
                        //    //    field: 'created_at',
                        //    //    title: 'Ngày tạo',
                        //    //},
                        //}],
                    ],
                    layout: {
                        scroll: !0,
                        height: null,
                        footer: !1
                    },
                    sortable: false,
                    pagination: true,
                    responsive: true,
                });
            }
            
        }
    });
    var reloadTable = function () {
        if (dataTableInstancedevice) {
            
            dataTableInstancedevice.reload();
        }
    };
    var initSearch = function () {
        $('#js-filter-category')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'category_id');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-supplier')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'supplier_uuid');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-project')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'project_uuid');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-customer')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'customer_uuid');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-categoryIddevice')
            .on('change', function () {
                if (dataTableInstancedevice) {
                    dataTableInstancedevice.search($(this).val().toLowerCase(), 'state');
                    START_PAGE = 1;
                }
            })
            .selectpicker();
        $('#js-filter-keyworddevice')
            .on('change', function () {
                if (dataTableInstancedevice) {
                    dataTableInstancedevice.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });

    };
    return {
        // Public functions
        init: function () {
            initValidation();
            initValidation1();
            initEventListeners();
            initSearch();
            autoupload();
            
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-project').length) {
        
        DMMSProjectCus.init();
    }
});
