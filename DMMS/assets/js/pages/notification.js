"use strict";
// Class definition

var DMMSNotification = function () {
    // Private functions

    var dataTableInstance = null;
    var drawer = null;
    var dataGlobal = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var total = 0;
    // demo initializer
    $('.readall').append(`<a class="readallnotification" href="#" role="button" id="u_0_j" style="
    font-size: 1rem;  tabindex="0">Đánh dấu tất cả là đã đọc</a>`)

    $(".kt-header__topbar-wrapper").on("click", function () {
        //var loading = new KTDialog({
        //    'type': 'loader',
        //    'placement': 'top center',
        //    'message': 'Đang khóa tài khoản...'
        //});
        //KTApp.blockPage();
        var loading = `<div class="kt-spinner loading kt-spinner--brand" style="position: absolute;margin-top: 13rem;right: 16rem;"></div>`;
        $('#noti').append(loading);
        total = 0;
        page = 2;

        $.ajax({
            url: window.location.origin + `/Notification/index`,
            method: 'GET',
        })
            .done(function (response) {
                if (response.length > 0) {
                    $('#noti').find(".loading").remove();
                    $('#noti').find(".notificationCustom").remove();

                    for (var i = 0; i < response.length; i++) {
                        var notifice = JSON.parse(response[i].content)
                        var datanoti = JSON.parse(response[i].data)
                        var time = new Date() - new Date(response[i].time_created);

                        var strConv = "";
                        var success = true;
                        var converted = parseInt(time / 1000);
                        if (success && converted > 0) {
                            strConv = converted + " giây";
                        } else success = false;
                        converted = parseInt(converted / 60);
                        if (success && converted > 0) {
                            strConv = converted + " phút";
                        } else success = false;
                        converted = parseInt(converted / 60);
                        if (success && converted > 0) {
                            strConv = converted + " giờ";
                        } else success = false;
                        converted = parseInt(converted / 24);
                        if (success && converted > 0) {
                            strConv = converted + " ngày";
                        } else success = false;
                        var uuidnoti = response[i].uuid;
                        var $add = $('#noti');
                        if (datanoti.action_id == 2) {
                            var icon = "<i class='flaticon-list-1 kt-font-brand'></i>";
                        }
                        else if (datanoti.action_id == 3) {
                            var icon = "<i class='flaticon-settings kt-font-brand'></i>";
                        }
                        else if (datanoti.action_id == 0) {
                            var icon = "<i class='flaticon-list kt-font-brand'></i>";
                        }
                        var style1 = response[i].status == 0 ? "style = 'background-color: #edf0f4;'" : null;

                        var html = "<div data-id='" + datanoti.uuid + "' data-no= '" + uuidnoti + "'action='" + datanoti.action_id + "' " + style1 + " class='kt-notification__item notificationCustom'>" + "<div class ='kt-notification__item-icon'>" + icon + "</div>" + "<div class='kt-notification__item-details'><div class='kt-notification__item-title'>" + notifice.body + "<div class='kt-notification__item-time'>" + strConv + "</div></div></div></div>";
                        $add.append(html);

                    }
                    $('#noti').scrollTop(0);
                }
                $('.kt-notification__item').click(function () {
                    const $this = $(this);
                    const id = $this.attr('data-no');
                    const uuid = $this.attr('data-id');
                    const action = $this.attr('action');
                    $.ajax({
                        url: window.location.origin + `/notification/upsert/${id}`,
                        method: 'GET',
                    })
                        .done(function (response) {
                            var id = uuid
                            if (action == 2) {
                                window.location = window.location.origin + `/task/update/${id}`;
                                //$('#noti').append(`<a href="/device/edit/${id}"></a>`);                               
                            }
                            else if (action == 1) {

                                window.location = window.location.origin + `/device/edit/${id}`;
                            }
                            else if (action == 3) {
                                initDetailDrawer3(id || '');
                            }
                            else if (action == 0) {
                                window.location = window.location.origin + `/ticket/edit/${id}`;
                            }
                            XXX();
                        })
                        .fail(function () {

                        });



                });
            })
            .fail(function () {

            });
    })

    //Ham chaylan dau


    $.ajax({
        url: window.location.origin + `/notification/count`,
        method: 'GET',
    })
        .done(function (response) {
            if (response.data.notification_count != 0) {
                $('#notificetion').show();
                if (response.data.notification_count > 99) {
                    $('#notificetion').text('99+');
                }
                else {
                    $('#notificetion').text(response.data.notification_count);
                }
                $('#totalnoti').text(response.data.notification_count);
                $('.btn btn-label-primary btn-sm btn-bold btn-font-md').text(response.data.notification_count);
            }
            else {
                $('#notificetion').hide();
                $('#totalnoti').text(0);
            }
        })
        .fail(function () {

        });

    var XXX = function () {
        $.ajax({
            url: window.location.origin + `/notification/count`,
            method: 'GET',
        })
            .done(function (response) {
                if (response.data.notification_count != 0) {
                    $('#notificetion').show();
                    $('#notificetion').text(response.data.notification_count);
                    $('#totalnoti').text(response.data.notification_count);
                    $('.btn btn-label-primary btn-sm btn-bold btn-font-md').text(response.data.notification_count);
                }
                else {
                    $('#notificetion').hide();
                    $('#totalnoti').text(0);
                }
            })
            .fail(function () {

            });
    }
    var initDetailDrawer = function (id) {
        const title = id ? 'Chi tiết công việc' : '';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--detailtask' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/task/detail/${id}` : '',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var initDetailDrawer4 = function (id) {
        const title = id ? 'Chi tiết ticket' : '';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--detailtask' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/ticket/details/${id}` : '',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var initDetailDrawer3 = function (id) {
        const title = id ? 'Chi tiết báo cáo' : '';
        drawer = new KTDrawer({ title, id: id || 'new', className: 'dmms-drawer--detailreport' });

        drawer.on('ready', () => {
            $.ajax({
                url: id ? `/reportdaily/details/${id}` : '',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);
                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };
    var page = 2

    $('.kt-notification').scroll(function (event) {
        //debugger
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {

            //alert('end reached');
            if (page == 2 || page <= total) {
                $.ajax({
                    url: window.location.origin + `/Notification/index/${page}`,
                    method: 'GET',
                })
                    .done(function (response) {

                        total = response.pagination.pages
                        page++;
                        console.log(page);
                        if (response.items.length > 0) {
                            for (var i = 0; i < response.items.length; i++) {
                                var notifice = JSON.parse(response.items[i].content)
                                var datanoti = JSON.parse(response.items[i].data)
                                var time = new Date() - new Date(response.items[i].time_created);

                                var strConv = "";
                                var success = true;
                                var converted = parseInt(time / 1000);
                                if (success && converted > 0) {
                                    strConv = converted + " giây";
                                } else success = false;
                                converted = parseInt(converted / 60);
                                if (success && converted > 0) {
                                    strConv = converted + " phút";
                                } else success = false;
                                converted = parseInt(converted / 60);
                                if (success && converted > 0) {
                                    strConv = converted + " giờ";
                                } else success = false;
                                converted = parseInt(converted / 24);
                                if (success && converted > 0) {
                                    strConv = converted + " ngày";
                                } else success = false;
                                var uuidnoti = response.items[i].uuid;
                                var $add = $('#noti');
                                if (datanoti.action_id == 2) {
                                    var icon = "<i class='flaticon-list-1 kt-font-brand'></i>";
                                }
                                else if (datanoti.action_id == 3) {
                                    var icon = "<i class='flaticon-settings kt-font-brand'></i>";
                                }
                                else if (datanoti.action_id == 0) {
                                    var icon = "<i class='flaticon-list kt-font-brand'></i>";
                                }
                                var style1 = response.items[i].status == 0 ? "style = 'background-color: #edf0f4;'" : null;

                                var html = "<div data-id='" + datanoti.uuid + "' data-no= '" + uuidnoti + "'action='" + datanoti.action_id + "' " + style1 + " class='kt-notification__item notificationCustom'>" + "<div class ='kt-notification__item-icon'>" + icon + "</div>" + "<div class='kt-notification__item-details'><div class='kt-notification__item-title'>" + notifice.body + "<div class='kt-notification__item-time'>" + strConv + "</div></div></div></div>";
                                $add.append(html);


                            }


                        }
                        $('.kt-notification__item').click(function () {
                            const $this = $(this);
                            const id = $this.attr('data-no');
                            const uuid = $this.attr('data-id');
                            const action = $this.attr('action');
                            $.ajax({
                                url: window.location.origin + `/notification/upsert/${id}`,
                                method: 'GET',
                            })
                                .done(function (response) {
                                    var id = uuid
                                    if (action == 2) {
                                        window.location = window.location.origin + `/task/update/${id}`;
                                        //$('#noti').append(`<a href="/device/edit/${id}"></a>`);                               
                                    }
                                    else if (action == 1) {

                                        window.location = window.location.origin + `/device/edit/${id}`;
                                    }
                                    else if (action == 3) {
                                        initDetailDrawer3(id || '');
                                    }
                                    else {
                                        window.location = window.location.origin + `/ticket/edit/${id}`;
                                    }
                                    XXX();
                                })



                        });

                    })
                    .fail(function () {

                    });
            }

        }
    });


    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-task__form');

    }

    //setInterval(function () {
    //    $.ajax({
    //        url: window.location.origin +`/notification/count`,
    //        method: 'GET',

    //    })

    //         .done(function (response) {
    //            if (response.data.notification_count != 0) {
    //                $('#notificetion').show();
    //                $('#notificetion').text(response.data.notification_count);
    //                $('#totalnoti').text(response.data.notification_count);
    //                $('.btn btn-label-primary btn-sm btn-bold btn-font-md').text(response.data.notification_count);
    //            }
    //            else {
    //                $('#notificetion').hide();
    //            }
    //        })f
    //        .fail(function () {

    //        });

    //}, 10000);
    $(document)
        .on('click', '.readallnotification', function () {
            const $this = $(this);
            const id = $this.attr('data-id');
            initReadDrawer(id || '');
        });
    var initReadDrawer = function (id) {

        $.ajax({
            url: '/Notification/readall',
            method: 'GET',
        })
            .done(function (response) {
                XXX();
            })
            .fail(function () {

            });


       
    };
    var initEventListeners = function () {


    };



    return {
        // Public functions
        init: function () {
            initEventListeners

        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-default').length) {
        DMMSNotification.init();
    }
});
