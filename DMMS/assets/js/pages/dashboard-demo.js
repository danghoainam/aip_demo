"use strict";
// Class definition

var DMMSDashboard = function () {
    debugger
    var chart;
    var commonOptions = {
        credits: {
            enabled: false,
        },

        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
        },

        title: {
            text: '',
            style: {
                display: 'none'
            }
        },

        subtitle: {
            text: '',
            style: {
                display: 'none'
            }
        },

        yAxis: {
            title: {
                enabled: true,
            },
            //gridLineColor: '#004A9B',
            labels: {
                style: {
                    //color: '#82ACD2',
                },
            },
        },

        xAxis: {
            labels: {
                style: {
                    //color: '#82ACD2',
                },
            },
        },

        plotOptions: {
            column: {
                borderWidth: 0
            },
            series: {
                allowPointSelect: true
            },
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    style: {
                        //color: '#82ACD2',
                        textOutline: '0px',
                    },
                },
                showInLegend: true,
            }
        },

        legend: {
            padding: 0,
            itemMarginTop: 5,
            itemMarginBottom: 5,
            itemStyle: {
                lineHeight: '20px',
                fontSize: '12px',
                fontWeight: 500,
            },
            itemStyle: {
                //color: '#F1F3F4',
            },
            itemHoverStyle: {
                //color: '#F1F3F4',
            },
            itemHiddenStyle: {
                //color: '#ccc',
            },
        },
    };
    //Added: TienPT
    // Khởi tạo ngày tháng cho DateRangePicker
    var daterangepickerInit = function () {
        debugger
        if ($('#kt_dashboard_daterangepicker').length == 0) {
            return;
        }

        var picker = $('#kt_dashboard_daterangepicker');
        var start = moment();
        var end = moment();
        var a = moment();
        var b = moment();
        function cb(start, end, label) {
            var title = '';
            var range = '';

            if ((end - start) < 100 || label == 'Hôm nay') {
                title = 'Hôm nay: ';
                range = start.format('D MMMM');
            } else if (label == 'Hôm qua') {
                title = 'Hôm qua: ';
                range = start.format('D MMMM');
            } else {
                range = start.format('D MMMM') + ' - ' + end.format('D MMMM');
            }
            a = Math.floor(end._d.getTime() / 1000);
            b = Math.floor(start._d.getTime() / 1000);
            $.ajax({
                url: `/chart/all`,
                method: 'GET',
                data: { from: b, to:a}
            })
                .done(function (res) {
                    debugger
                    if (res) {
                        const { tasks, devices, offers, contracts, totalTasks, totalDevices, totalOffers, totalContracts, tickets, dailyreport, datatask3, datatask2, datatask1 } = res;
                        initTaskData(tasks, totalTasks);
                        initCallWarning(tasks);
                        initTicketWarning(tickets);
                        //initTask();
                        initTime(tasks, datatask1, datatask2, datatask3);
                        initContract(contracts);
                        intitDailyReport(dailyreport);
                        //initStaff();
                        initDevice(devices);
                        //initOffers(offers);
                       
                    }
                   
                })
                .fail(function () {
                    $.notify('Có lỗi xảy ra, vui lòng thử lại.', { type: 'danger' });
                   
                });
            $('#kt_dashboard_daterangepicker_date').html(range);
            $('#kt_dashboard_daterangepicker_title').html(title);
        }

        picker.daterangepicker({
            direction: KTUtil.isRTL(),
            startDate: start,
            endDate: end,
            opens: 'left',
            ranges: {
                'Hôm nay': [moment(), moment()],
                'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 ngày trước': [moment().subtract(6, 'days'), moment()],
                '30 ngày trước': [moment().subtract(29, 'days'), moment()],
                'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            locale: {
                applyLabel: 'Áp dụng',
                cancelLabel: 'Hủy bỏ',
                customRangeLabel: 'Tùy chỉnh',
                //daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                //monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        }, cb);
        debugger
       
        //cb(start, end, '');
        
    }
    //Sử dụng cho cảnh báo ticket chưa xử lý
    var initTicketWarning = function (tickets, dataticket) {
        debugger
        //var data = [];
        //var a = 0, b = 0, c = 0;
        //if (tickets) {
        //    for (var k = 0; k < tickets.length; k++) {
        //        if (tickets[k].level == 1) {
        //            for (var i = 0; i < tickets[k].info.length; i++) {
        //                if (tickets[k].info[i].state == 1) {
        //                    a += tickets[k].info[i].count;
        //                }

        //            }
        //        }
        //    }
        //    for (var k = 0; k < tickets.length; k++) {
        //        if (tickets[k].level == 2) {
        //            for (var i = 0; i < tickets[k].info.length; i++) {
        //                if (tickets[k].info[i].state == 1) {
        //                    b += tickets[k].info[i].count;
        //                }
        //            }
        //        }
        //    }
        //    for (var k = 0; k < tickets.length; k++) {
        //        if (tickets[k].level == 3) {
        //            for (var i = 0; i < tickets[k].info.length; i++) {
        //                if (tickets[k].info[i].state == 1) {
        //                    c += tickets[k].info[i].count;
        //                }
        //            }
        //        }
        //    }
        //}
       
        
        var data = dataticket;
        if (data == null) {
            data=[0,0,0]
        } 
        const chartOptions = {
            colors: ['#27AE60', '#F2994A', '#E85656'],
            chart: {
                type: 'column'
            },

            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: {
                //type: 'category',
                categories: ['Mức độ 1', 'Mức độ 2', 'Mức độ 3'],
            },
            yAxis: {
                title: {
                    text: ''
                },

            },
   

            plotOptions: {
                series: {
                    cursor: 'pointer',
                    legendType: 'point',
                    point: {
                        events: {
                            click: function (event) {
                                console.log(event.point, this);
                                //location.href = 'https://en.wikipedia.org/wiki/' +
                                //    this.options.key;
                            }
                        }
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> ticket.<br/>'
            },
            series: [
                {
                    name: "Ticket chưa xử lý",
                    colorByPoint: true,
                    data:data.map(function (value, i) {
                        return ['Mức độ ' + (i+1), value];
                    })
                }
            ],
        };

        Highcharts.chart('dmms-chart-ticket', $.extend(true, {}, commonOptions, chartOptions));
    };
   
    //Sử dụng cho cảnh báo call chưa xử lý
    var initCallWarning = function (data) {
        debugger;
        const chartOptions = {
            colors: ['#27AE60', '#F2994A', '#E85656'],
            chart: {
                type: 'column'
            },

            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: {
                type: 'category',
            },
            yAxis: {
                title: {
                    text: ''
                },

            },
            legend: {
                enabled: true
            },

            plotOptions: {
                series: {
                    cursor: 'pointer',
                    legendType: 'point',
                    point: {
                        events: {
                            click: function (event) {
                                console.log(event.point, this);
                                //location.href = 'https://en.wikipedia.org/wiki/' +
                                //    this.options.key;
                            }
                        }
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> cuộc gọi.<br/>'
            },
            series: [
                {
                    name: "Cuộc gọi chưa xử lý",
                    colorByPoint: true,
                    data: [
                        {
                            name: "Mức độ 1",
                            y: 178
                        },
                        {
                            name: "Mức độ 2",
                            y: 100
                        },
                        {
                            name: "Mức độ 3",
                            y: 50
                        }
                    ]
                }
            ],
        };

        Highcharts.chart('dmms-chart-call', $.extend(true, {}, commonOptions, chartOptions));
    };
    
    // Private functions
    var initTask = function () {
        const chartOptions = {
            colors: ['#7AC943', '#3FA9F5', '#FF931E', '#9B51E0'],

            chart: {
                type: 'pie',
            },

            title: {
                text: 270,
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                style: {
                    fontFamily: 'inherit',
                    fontSize: 24,
                    fontWeight: 'bold',
                    display: 'block',
                },
            },

            tooltip: {
                pointFormat: '{point.y}'
            },

            plotOptions: {
                pie: {
                    dataLabels: {
                        format: '{point.y}'
                    },
                }
            },

            series: [{
                innerSize: '40%',
                colorByPoint: true,
                data: [{
                    name: 'Công việc mới',
                    y: 70
                }, {
                    name: 'Đã tiếp nhận',
                    y: 50
                }, {
                    name: 'Đang xử lý',
                    y: 100
                }, {
                    name: 'Yêu cầu thay thế',
                    y: 50
                }]
            }],

            legend: {
                labelFormatter: function labelFormatter() {
                    return `<b>${this.name}</b>`;
                },
            },
        };

        Highcharts.chart('dmms-chart-task', $.extend(true, {}, commonOptions, chartOptions));
    };
    //Sử dụng cho Dashboard trạng thái công việc
    var initTaskData = function (data, total) {
        debugger
        const chartOptions = {
            //colors: ['#7AC943', '#3FA9F5', '#FF931E', '#9B51E0'],

            chart: {
                type: 'pie'
            },
            
            title: {
                text: total,
                align: 'center',
                verticalAlign: 'middle',
                y: 0,
                style: {
                    fontFamily: 'inherit',
                    fontSize: 24,
                    fontWeight: 'bold',
                    display: 'block',
                    //color: "#FFF",
                    //fill: "#FFF"
                },
            },

            tooltip: {
                pointFormat: '{point.y}'
            },

            plotOptions: {
                pie: {
                    dataLabels: {
                        format: '{point.y}'
                    },
                    point: {
                        events: {
                            legendItemClick: function () {
                                //Tích mất đi biểu đồ
                                //this.total = this.visible ? this.total - this.y : this.total + this.y;
                                //if (this.total == 0)
                                //    this.total = "Không có công việc nào!"
                                //chart.setTitle({ text: this.total });
                                return false;
                                //Nếu muốn kích mở link task thì bỏ comment dòng dưới
                                //location.href = `/task?state=${this.x}`;
                            }
                        }
                    },
                },
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (event) {
                                location.href = `/task?state=${event.point.key}`;
                            }
                            
                        }
                    },
                }
            },

            series: [{
                innerSize: '40%',
                colorByPoint: true,
                data: data
            }],

            legend: {
                labelFormatter: function labelFormatter() {
                    return `<b>${this.name}</b>`;
                },
            },
        };

        chart = Highcharts.chart('dmms-chart-task', $.extend(true, {}, commonOptions, chartOptions));
    };

    var initTime = function (tasks, datatask1, datatask2, datatask3) {
        const chartOptions = {
            colors: ['#27AE60', '#F1994A', '#E85656'],

            chart: {
                type: 'column'
            },

            xAxis: {
                categories: [
                    'Công việc mới',
                    'Đã tiếp nhận',
                    'Đang xử lý',
                    'Yêu cầu thay thế',
                ],
                crosshair: true
            },

            yAxis: {
                min: 0,
                title: {
                    text: ' '
                }
            },

            tooltip: {
                headerFormat: '<span>{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },

            series: [{
                name: 'Mức độ 1',
                data: datatask1

            }, {
                name: 'Mức độ 2',
                    data: datatask2

            }, {
                    name: 'Mức độ 3',
                    data: datatask3

            }]
           
        };

        Highcharts.chart('dmms-chart-time', $.extend(true, {}, commonOptions, chartOptions));
    };
    var intitDailyReport = function (dailyreport) {
        debugger
        var data1 = [], data2 = [], data3 = [];
        var a = 0, b = 0, c = 0, d = 0, e = 0, f = 0, g = 0, h = 0, i1 = 0, j = 0, k1 = 0, l = 0;
        if (dailyreport!= null) {
            for (var k = 0; k < dailyreport.length; k++) {
                if (dailyreport[k].level == 1) {
                    for (var i = 0; i < dailyreport[k].info.length; i++) {
                        if (dailyreport[k].info[i].state == 1) {
                            a = dailyreport[k].info[i].count
                        }
                        if (dailyreport[k].info[i].state == 2) {
                            b = dailyreport[k].info[i].count
                        }
                        if (dailyreport[k].info[i].state == 3) {
                            c = dailyreport[k].info[i].count
                        }
                        if (dailyreport[k].info[i].state == 4) {
                            d = dailyreport[k].info[i].count
                        }
                    }
                }
                if (dailyreport[k].level == 2) {
                    for (var i = 0; i < dailyreport[k].info.length; i++) {
                        if (dailyreport[k].info[i].state == 1) {
                            e = dailyreport[k].info[i].count
                        }
                        if (dailyreport[k].info[i].state == 2) {
                            f = dailyreport[k].info[i].count
                        }
                        if (dailyreport[k].info[i].state == 3) {
                            g = dailyreport[k].info[i].count
                        }
                        if (dailyreport[k].info[i].state == 4) {
                            h = dailyreport[k].info[i].count
                        }
                    }
                }
                if (dailyreport[k].level == 3) {
                    for (var i = 0; i < dailyreport[k].info.length; i++) {
                        if (dailyreport[k].info[i].state == 1) {
                            i1 = dailyreport[k].info[i].count
                        }
                        if (dailyreport[k].info[i].state == 2) {
                            j = dailyreport[k].info[i].count
                        }
                        if (dailyreport[k].info[i].state == 3) {
                            k1 = dailyreport[k].info[i].count
                        }
                        if (dailyreport[k].info[i].state == 4) {
                            l = dailyreport[k].info[i].count
                        }
                    }
                }
            }
        }
      
        data1 = [a, b, c, d];
        data2 = [e, f, g, h];
        data3 = [i1, j, k1, l];
        const chartOptions = {
            colors: ['#27AE60', '#F1994A', '#E85656'],

            chart: {
                type: 'column'
            },

            xAxis: {
                categories: [
                    'Chưa Tạo',
                    'Chưa duyệt',
                    'Đã duyệt',
                    'Từ Chối',
                   
                ],
                crosshair: true
            },

            yAxis: {
                min: 0,
                title: {
                    text: ' '
                }
            },

            tooltip: {
                headerFormat: '<span>{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },

            series: [{
                name: 'Mức độ 1',
                data: data1

            }, {
                name: 'Mức độ 2',
                data: data2

            }, {
                    name: 'Mức độ 3',
                    data: data3

            }]
        };

        Highcharts.chart('dmms-chart-dailyreport', $.extend(true, {}, commonOptions, chartOptions));
    };

    var initContract = function (data) {
        const chartOptions = {
            colors: ['#27AE60', '#F2994A', '#9B51E0', '#EB5757'],

            chart: {
                type: 'pie',
            },

            tooltip: {
                pointFormat: '<b>{point.y} HĐ</b>'
            },

            plotOptions: {
                pie: {
                    dataLabels: {
                        format: '{point.y} HĐ'
                    },
                },
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (event) {
                                //location.href = `/task?state=${event.point.key}`;
                            }
                        }
                    }
                }
            },

            series: [{
                colorByPoint: true,
                data: data
            }],

            legend: {
                labelFormatter: function labelFormatter() {
                    return `<b>${this.name}</b>`;
                },
            },

        };

        Highcharts.chart('dmms-chart-contract', $.extend(true, {}, commonOptions, chartOptions));
    };
    /*
    var initStaff = function () {
        const chartOptions = {
            colors: ['#27AE60', '#F2994A', '#9B51E0', '#EB5757'],

            chart: {
                type: 'pie',
            },

            tooltip: {
                pointFormat: '<b>{point.y} NV</b>'
            },

            plotOptions: {
                pie: {
                    dataLabels: {
                        format: '{point.y} NV'
                    },
                }
            },

            series: [{
                colorByPoint: true,
                data: [{
                    name: 'NV Rảnh',
                    y: 70
                }, {
                    name: 'NV Bận',
                    y: 50
                }, {
                    name: 'NV Đi công tác',
                    y: 100
                }, {
                    name: 'NV Nghỉ phép',
                    y: 50
                }]
            }],

            legend: {
                labelFormatter: function labelFormatter() {
                    return `<b>${this.name}</b>`;
                },
            },

        };

        Highcharts.chart('dmms-chart-staff', $.extend(true, {}, commonOptions, chartOptions));
    };
    */
    var initDevice = function (data) {
        const chartOptions = {
            //colors: ['#27AE60', '#F2994A', '#E85656'],
            chart: {
                type: 'column'
            },

            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: {
                type: 'category',
            },
            yAxis: {
                title: {
                    text: ''
                },

            },
            legend: {
                enabled: false
            },

            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (event) {
                                console.log(event.point, this);
                                //location.href = 'https://en.wikipedia.org/wiki/' +
                                //    this.options.key;
                            }
                        }
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> thiết bị.<br/>'
            },
            series: [
                {
                    name: "Thiết bị đang sửa",
                    colorByPoint: true,
                    data: data
                }
            ],
        };

        Highcharts.chart('dmms-chart-device', $.extend(true, {}, commonOptions, chartOptions));
    };
    /*
    var initOffers = function (data) {
        const chartOptions = {
            colors: ['#27AE60', '#3FA9F5', '#EB5757', '#9B51E0'],

            chart: {
                type: 'pie',
            },

            tooltip: {
                pointFormat: '<b>{point.y} Đề xuất</b>'
            },

            plotOptions: {
                pie: {
                    dataLabels: {
                        format: '{point.y} ĐX'
                    },
                },

            },

            series: [{
                colorByPoint: true,
                data: data
            }],

            legend: {
                labelFormatter: function labelFormatter() {
                    return `<b>${this.name}</b>`;
                },
            },

        };

        Highcharts.chart('dmms-chart-pr', $.extend(true, {}, commonOptions, chartOptions));
    };
    */
    return {
        // Public functions
        init: function () {
            KTApp.block($('.dmms-home-chart'));
            $.ajax({
                url: `/chart/all`,
                method: 'GET',
            })
                .done(function (res) {
                    debugger
                    if (res) {
                        const { tasks, devices, offers, contracts, totalTasks, totalDevices, totalOffers, totalContracts, tickets, dailyreport, datatask3, datatask2, datatask1, dataticket } = res;
                        initTaskData(tasks, totalTasks);
                        initCallWarning(tasks);
                        initTicketWarning(tickets, dataticket);
                        //initTask();
                        initTime(tasks, datatask1, datatask2, datatask3);
                        initContract(contracts);
                        intitDailyReport(dailyreport);
                        //initStaff();
                        initDevice(devices);
                        //initOffers(offers);
                        // init daterangepicker
                        daterangepickerInit();
                    }
                    KTApp.unblock($('.dmms-home-chart'));
                })
                .fail(function () {
                    $.notify('Có lỗi xảy ra, vui lòng thử lại.', { type: 'danger' });
                    KTApp.unblock($('.dmms-home-chart'));
                });
            daterangepickerInit();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-dashboard-demo').length) {
        debugger
        DMMSDashboard.init();
    }
});
