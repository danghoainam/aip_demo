"use strict";
// Class definition

var DMMSCallLog = function () {
    // Private functions

    var dataTableInstance = null;
    var drawer = null;
    var dataGlobal = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    // demo initializer
    var initTable = function () {
        const { base64encode, base64decode } = require('nodejs-base64');

        dataTableInstance = $('.kt-datatable').KTDatatable({


            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/calllog/gets',
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? 0 : pagination.page;
                            CURRENT_PAGE = page || 1;
                            START_PAGE = 0;
                            LIMIT = perpage;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword, caller } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }  
                        return {
                            limit: pagination.perpage || LIMIT,
                            caller: (caller || $('#js-filter-keyword').val()).trim(),
                            page: pagination.page,

                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                //input: $('#js-filter-keyword'),
                onEnter: true,
            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 35,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                   
                },
                {
                    field: 'call_log.dateTime',
                    title: 'Lịch sử',
                    width:115,

                }, {
                    field: 'customer.name',
                    title: 'Đơn vị liên hệ',
                    template: function (data) {
                        var output = '';
                        if (data.customer != null && data.customer.name) {
                            output += '<a href = "/customer/update/' + data.customer.uuid + '"><span>' + data.customer.name + '</span ></a>';
                        }
                        return output;
                    }

                },
                {
                    field: 'call_log.caller',
                    title: 'Số điện thoại',
                    width: 80,
                    textAlign:'center'
                },
                {
                    field: 'customer.address',
                    title: 'Địa chỉ',
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 90,
                    overflow: 'visible',
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        dataGlobal = data;
                        var uri = new Object();
                        uri.authorization = data.authorization;
                        uri.phone = data.call_log.caller;                        
                        var strCallWebToUri = "agent-call:" + base64encode(JSON.stringify(uri));
                        return `
                            <script>
                            var calling = function (strCallWebToUri) {
                                window.onbeforeunload = null;
                                window.open(strCallWebToUri);
                            };  
                            </script>
                             <button title="Gọi" class="js-edit-project btn btn-sm btn-primary btn-icon btn-icon-md" onclick="calling('`+ strCallWebToUri+`')">
                                <i class="la la-phone-square"></i>
                            </button>                            
                            <button data-id="${data.call_log.audio}" data-name="${data.customer?data.customer.name:''}" type="button" title="Nghe file ghi âm" class="js-edit-calllog btn btn-sm btn-success btn-icon btn-icon-md js-call">
                                <i class="la la-play-circle"></i>
                            </button> 
                       


                                                     
                        `;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };
    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var initSearch = function () {
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'caller');
                    START_PAGE = 1;
                }
            });
    };

    var initEventListeners = function () {
        $(document)          
            .off('click', '.js-edit-calllog')
            .on('click', '.js-edit-calllog', function () {
                const $this = $(this);
                const uuid = $this.attr('data-id');
                const nameCustom = $this.attr('data-name');
                var span = document.createElement("span");
                var token = "?token=" + dataGlobal.authorization.toString() + "' > </audio > ";
                span.innerHTML = "<audio controls> <source src='http://35.240.155.147:1237/api/call-logs/play-audio/"+ uuid + token;
                KTApp.swal({
                    title: 'File ghi âm ' + nameCustom,
                    //html: "<audio controls style='width: 112px;'> <source src='https://zingmp3.vn/nghe-si/Minh-Vuong-M4U'> </audio>",
                   
                   
                    dangerModeCustom:true,
                    content: span,
                })
                });
           

    };
    
    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-project__form');
        initForm($form);

        /*
            init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

        $('#js-filter-customer').selectpicker();
    };

    return {
        // Public functions
        init: function () {
            initTable();
            initSearch();
            initEventListeners();
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-calllog').length) {
        DMMSCallLog.init();
    }
});
