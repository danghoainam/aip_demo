﻿"use strict";
// Class definition

var DMMSTaskPlan = function () {
    // Private functions

    var dataTableInstance = null;
    var drawer = null;
    var LIMIT = 20;
    var CURRENT_PAGE = 1;
    var START_PAGE = 0;
    var MAX_ITEM_INPAGE = 0;

    var initTable = function () {
        var editSucessCache = localStorage['edit_success'] != null ? JSON.parse(localStorage['edit_success']) : null;
        if (editSucessCache && document.referrer.includes(editSucessCache.from)) {
            $.notify(editSucessCache.massage, { type: 'success' });
            localStorage.removeItem('edit_success');
        }
        dataTableInstance = $('.kt-datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: `/taskplan/gets`,
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? 0 : pagination.page;
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { customerUuid, filterStatus, keyword } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            customerUuid: customerUuid || $('#js-filter-customer').val(),
                            filterStatus: filterStatus || $('#js-filter-state').val(),
                            keyword: (keyword || $('#js-filter-keyword').val()).trim(),
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,

            },
            columns: [
                {
                    field: 'uuid',
                    title: '#',
                    width: 30,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'name',
                    title: 'Tên kế hoạch - Mô tả công việc',
                    template: function (data) {
                        if (data.name == null) {
                            data.name = 'Chưa rõ'
                        }
                        var output = `<div class="kt-user-card__details">
                                          <strong>${data.name}</strong><br>`;
                        if (data.description != null) {
                            let maxstr = 25;
                            if (data.description.length > maxstr) {
                                let substr = data.description.substring(0, maxstr) + '...';
                                output += `<span title="${data.description}">${substr}</span></div>`
                            }
                            else output += `<span>${data.description}</span></div>`;
                        }

                        return output;
                    },
                },
                {
                    field: 'projectName',
                    title: 'Dự án thuộc kế hoạch',
                    textAlign: 'center',
                    template: function (data) {
                        var output = '';
                        if (data.projectName !== null) {
                            let maxstr = 50;
                            if (data.projectName.length > maxstr) {
                                let substring = data.projectName.substring(0, maxstr) + '...';
                                output += `<a href="/project/edit/${data.projectUuid}" title="${data.projectName}">
                                                ${substring}
                                            </a>`;
                            }
                            else {
                                output += `<a href="/project/edit/${data.projectUuid}" title="${data.projectName}">
                                                ${data.projectName}
                                            </a>`;
                            }
                        }
                        return output;
                    },
                },
                {
                    field: 'costEstimate_format',
                    title: 'Chi phí dự kiến',
                    width: 110,
                },
                {
                    field: 'timecreatedformat',
                    title: 'Ngày tạo kế hoạch',
                    width: 70,
                },
                {
                    field: '#',
                    title: 'Ngày triển khai-Ngày kết thúc',
                    width: 150,
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        var output = `<div class="kt-user-card__details">
                                          <span>${data.startTime_format}</span>`;
                        if (data.description != null) {
                            output += `<span> - ${data.finishTime_format}</span></div>`
                        }

                        return output;
                    },
                },
                {
                    field: 'creatorName',
                    title: 'Người đề xuất',
                    width: 90,
                    textAlign: 'center',
                    template: function (data) {
                        if (data.creatorName) {
                            return `<a href="#" title="Thông tin người đề xuất" class="js-detail-creator" data-id="${data.creatorID}">
                                    ${data.creatorName}
                                </a>`;
                        }
                        return `Chưa rõ`;
                    },
                },
                {
                    field: 'state',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    width: 110,
                    autoHide: false,
                    template: function (data) {
                        var states = {
                            0: { 'title': 'Chưa duyệt', 'class': 'kt-badge--primary' },
                            1: { 'title': 'Đã duyệt', 'class': ' kt-badge--success' },
                            2: { 'title': 'Đã từ chối', 'class': ' kt-badge--danger' },
                            3: { 'title': 'Đã hoàn thành', 'class': ' kt-badge--success' },
                            4: { 'title': 'Đã hủy', 'class': ' kt-badge--danger' },
                            5: { 'title': 'Chưa lập kế hoạch', 'class': 'kt-badge--warning' },
                            6: { 'title': 'Đang triển khai', 'class': 'kt-badge--success' },
                            7: { 'title': 'Đã báo cáo', 'class': 'kt-badge--success' },
                        };
                        if (data.state) {
                            return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill">' + states[data.state].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge ' + states[0].class + ' kt-badge--inline kt-badge--pill">' + states[0].title + '</span>';
                        }
                    }
                },
                {
                    field: 'Tác vụ',
                    title: 'Tác vụ',
                    sortable: false,
                    width: 90,
                    autoHide: false,
                    textAlign: 'center',
                    template: function (data) {
                        return `<a href="/taskplan/detail/${data.uuid}" title="Chi tiết" class="btn btn-sm btn-success btn-icon btn-icon-md">
                                <i class="la la-info"></i>
                            </a>
                            <buttom data-id=${data.uuid} title="Xóa" class="btn btn-sm btn-danger btn-icon btn-icon-md js-delete-taskplan">
                                <i class="la la-trash"></i>
                            </buttom>`;
                    },
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    };

    var reloadTable = function () {
        if (dataTableInstance) {
            dataTableInstance.reload();
        }
    };

    var initSearch = function () {
        $('#js-filter-customer')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'customerUuid');
                    START_PAGE = 1;
                }
            }).selectpicker();
        $('#js-filter-keyword')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase(), 'keyword');
                    START_PAGE = 1;
                }
            });

        var stateIds = [""];
        if ($("#stateIds").val() !== '' && $("#stateIds").val() !== undefined) {
            stateIds = $("#stateIds").val().split(",");
        }
        $('#js-filter-state')
            .on('change', function () {
                $('#ckbTest:checkbox:checked').length > 0 ? $('#ckbTest').val(1) : $('#ckbTest').val() == null;
                $('#ckbTest2:checkbox:checked').length > 0 ? $('#ckbTest2').val(2) : $('#ckbTest2').val(null);
                if (dataTableInstance) {
                    var stateArr = $(this).val();
                    dataTableInstance.search(stateArr.toString(), 'filterStatus');
                    START_PAGE = 1;
                }
            })
            .selectpicker('val', stateIds);

        //Customer in plan
        $('#js-filter-keyword--customer')
            .on('change', function () {
                if (dataTableInstance) {
                    dataTableInstance.search($(this).val().toLowerCase().trim(), 'keyword');
                    START_PAGE = 1;
                }
            });

        //Task in plan
        var taskStateIds = [""];
        if ($("#stateIds").val() !== '' && $("#stateIds").val() !== undefined) {
            taskStateIds = $("#stateIds").val().split(",");
        }
        $('#js-filter-taskState')
            .on('change', function () {
                $('#ckbTest:checkbox:checked').length > 0 ? $('#ckbTest').val(1) : $('#ckbTest').val() == null;
                $('#ckbTest2:checkbox:checked').length > 0 ? $('#ckbTest2').val(2) : $('#ckbTest2').val(null);
                if (dataTableInstance) {
                    var stateArr = $(this).val();
                    dataTableInstance.search(stateArr.toString(), 'filterStatusTask');
                    START_PAGE = 1;
                }
            })
            .selectpicker('val', taskStateIds);
    };

    //if ($('#customer_uuid').val() !== '') {
    //    $('#kt-select-customer').selectpicker('val', $('#customer_uuid').val());
    //}
    //else {
    //    $('#kt-select-customer').selectpicker();
    //}

    //Chọn ngày
    var initdaterangepicker = function () {
        if ($("#startdate").val() !== '' && $("#enddate").val() !== '') {
            $('#kt_daterangepicker').daterangepicker({
                timePicker: true,
                endDate: $("#enddate").val(),
                startDate: $("#startdate").val(),

                locale: {
                    cancelLabel: "Hủy",
                    applyLabel: "Xác nhận",
                    format: 'DD/MM/YYYY'
                }
            }, function (start, end, label) {
                //$('#kt_daterangepicker .form-control').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
                $('#startdate').val(start.format('DD/MM/YYYY'));
                $('#enddate').val(end.format('DD/MM/YYYY'));
            });
        }
        else {
            $('#kt_daterangepicker').daterangepicker({
                timePicker: true,

                locale: {
                    cancelLabel: "Hủy",
                    applyLabel: "Xác nhận",
                    format: 'DD/MM/YYYY'
                }
            }, function (start, end, label) {
                //$('#kt_daterangepicker .form-control').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
                $('#startdate').val(start.format('DD/MM/YYYY'));
                $('#enddate').val(end.format('DD/MM/YYYY'));
            });
        }
    }

    var deleteTaskPlan = function (id) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang xóa kế hoạch công việc...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/taskplan/delete/${id}`,
                method: 'DELETE',
            })
                .done(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify("Xóa thành công!", { type: 'success' });

                    if (dataTableInstance) {
                        dataTableInstance.reload();
                    }
                })
                .fail(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(data.responseJSON.msg, { type: 'danger' });
                });
        }
    };

    var decideTaskPlan = function (id, reviewNote, accept) {
        if (id) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang thực hiện thao tác...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/taskplan/decide`,
                method: 'POST',
                data: {
                    id: id,
                    reviewNote: reviewNote,
                    accept: accept
                }
            })
                .done(function () {
                    const msg = accept == "true" ? "Duyệt thành công!" : "Từ chối yêu cầu thành công!";
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(msg, { type: 'success' });

                    //reload lại trang detail
                    window.location.pathname = `/taskplan/detail/${id}`;
                })
                .fail(function (data) {
                    if (data.responseJSON.error == -3) {
                        var msg = accept == "true" ? "Kế hoạch có đơn vị chưa có nhân viên chịu trách nhiệm. Không thể duyệt!" : "Kế hoạch có đơn vị chưa có nhân viên chịu trách nhiệm. Không thể từ chối yêu cầu!";
                    }
                    else {
                        var msg = data.responseJSON.msg;
                    }
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(msg, { type: 'danger' });
                });
        }
    };
    var CancelPlan = function (uuid, reviewNote) {
        debugger
        var note = $('.reviewNote')
        var datanote = note.serializeJSON();
        var form = $('.dmms-taskplan__form');
        var data = form.serializeJSON();
        delete data.reviewNote;
        data.state = 4;
        data.reviewNote = datanote.reviewNote;
        if (uuid) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang thực hiện thao tác...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/taskplan/detail/${uuid}`,
                method: 'PUT',
                data: data,
            })
                .done(function (data) {
                    debugger
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify("Hủy kế hoạch thành công!", { type: 'success' });

                    //reload lại trang detail
                    window.location.pathname = `/taskplan/detail/${uuid}`;
                })
                .fail(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(data.responseJSON.msg, { type: 'danger' });
                });
        }
    };
    var DeployPlan = function (uuid, reviewNote) {
        debugger
        var note = $('.reviewNote')
        var datanote = note.serializeJSON();
        var form = $('.dmms-taskplan__form');
        var data = form.serializeJSON();
        delete data.reviewNote;
        data.state = 4;
        data.reviewNote = datanote.reviewNote;
        if (uuid) {
            var loading = new KTDialog({
                'type': 'loader',
                'placement': 'top center',
                'message': 'Đang thực hiện thao tác...'
            });
            KTApp.blockPage();
            $.ajax({
                url: `/taskplan/Deploy/${uuid}`,
                method: 'POST',
                data: data,
            })
                .done(function (data) {
                    debugger
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify("Hủy kế hoạch thành công!", { type: 'success' });

                    //reload lại trang detail
                    window.location.pathname = `/taskplan/detail/${uuid}`;
                })
                .fail(function (data) {
                    loading.hide();
                    KTApp.unblockPage();
                    $.notify(data.responseJSON.msg, { type: 'danger' });
                });
        }
    };
    var initEventListeners = function () {
        $(document)
            .off('click', '.js-delete-taskplan')
            .on('click', '.js-delete-taskplan', function () {
                const $this = $(this);
                const id = $this.attr('data-id');

                KTApp.swal({
                    title: 'Xác nhận',
                    text: 'Kế hoạch sẽ bị xóa khỏi hệ thống.',
                    icon: 'warning',
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        deleteTaskPlan(id);
                    }
                });
            })
            .off('click', '.js-decide-plan')
            .on('click', '.js-decide-plan', function () {
                const $this = $(this);
                const id = $this.attr('data-id');
                const accept = $this.attr('data-accept');

                var textarea = document.createElement('textarea');
                textarea.placeholder = 'Nhập nội dung từ chối kế hoạch...';
                textarea.className = 'form-control';
                textarea.name = 'reviewNote';
                textarea.id = 'reviewNote1';

                const accept_title = accept == "true" ? 'DUYỆT KẾ HOẠCH' : 'TỪ CHỐI YÊU CẦU';
                KTApp.swal({
                    title: accept_title,
                    content: accept == "true" ? '' : textarea,
                    text: accept == "true" ? 'Xác nhận duyệt kế hoạch!' : '',
                    dangerMode: true,
                }).then((confirm) => {
                    const reviewNote = $('#reviewNote1').val();
                    debugger;
                    if (confirm) {
                        decideTaskPlan(id, reviewNote, accept);
                    }
                });
            })
            .off('click', '.js-cancel-plan')
            .on('click', '.js-cancel-plan', function () {
                const $this = $(this);
                const uuid = $this.attr('data-id');
                var input = document.createElement("textarea");
                input.className = "reviewNote form-control";
                input.cols = "40";
                input.rows = "5";
                input.name = 'reviewNote';
                input.id = 'reviewNote';
                KTApp.swal({

                    text: 'Lý do hủy kế hoạch khỏi hệ thống.',
                    content: input,
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        const reviewNote = $('#reviewNote').val();
                        CancelPlan(uuid, reviewNote);
                    }
                });
            })
            .off('click', '.js-deploy-plan')
            .on('click', '.js-deploy-plan', function () {
                const $this = $(this);
                const uuid = $this.attr('data-id');
                var input = document.createElement("textarea");
                input.className = "reviewNote form-control";
                input.cols = "40";
                input.rows = "5";
                input.name = 'reviewNote';
                input.id = 'reviewNote';
                KTApp.swal({

                    text: 'Ghi chú triển khai kế hoạch.',
                    content: input,
                    dangerMode: true,
                }).then((confirm) => {
                    if (confirm) {
                        const reviewNote = $('#reviewNote').val();
                        DeployPlan(uuid, reviewNote);
                    }
                });
            })
            .off('click', '.js-edit-taskplan')
            .on('click', '.js-edit-taskplan', function () {
                const $this = $(this);
                const uuid = $this.attr('data-id');

                initEditDrawer(uuid || '');
            });
    };

    var initEditDrawer = function (uuid, staffs) {
        const title = uuid ? 'Sửa thông tin kế hoạch công việc' : 'Thêm mới kế hoạch công việc';
        drawer = new KTDrawer({ title, uuid: uuid || 'new', className: 'dmms-drawer--taskplan' });

        drawer.on('ready', () => {
            $.ajax({
                url: uuid ? `/taskplan/edit/${uuid}` : 'taskplan/create',
                method: 'GET',
            })
                .done(function (response) {
                    const $content = drawer.setBody(response);

                    //---add or remove input in draw---
                    const types = {
                        1: 'Chịu trách nhiệm',
                        2: 'Hỗ trợ',
                    };

                    var wrapper = $(".input_fields_wrap"); //Fields wrapper
                    var add_button = ".add_field_button"; //Add button ID
                    var staffInput = "#kt-input-staff"; //input staff
                    var typeInput = "#kt-input-type"; //input type
                    var staffUuid = ''; //uuid staff value
                    var typeId = 0; //type value

                    $(document)
                        .on('change', staffInput, function () { //change input staff
                            staffUuid = '';
                            const $this = $(this);
                            staffUuid = $this.val();
                        })
                        .on('change', typeInput, function () { //change input type
                            const $this = $(this);
                            typeId = $this.val();
                        })
                        .on('click', add_button, function () { //event click Add button ID
                            var html = '';
                            $.ajax({
                                url: `/staff/getdetail/${staffUuid}`,
                                method: 'GET',
                            })
                                .done(function (data) {
                                    if (typeId !== 0) {
                                        html += `<div class="container row kt-margin-b-10" style="width: auto">`;
                                        html += `<div class="kt-input-icon col-11 row">
                                                    <div class="col-8 kt-padding-l-10">
                                                        <input name="staffUuids[][uuid]" value="${data.data.uuid}" type="hidden" />
                                                        <div class="kt-input-icon">
                                                            <input class="form-control" value="${data.data.name}" readonly />
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <input name="staffUuids[][type]" value="${typeId}" type="hidden" />
                                                        <div class="kt-input-icon">
                                                            <input class="form-control" value="${types[typeId]}" readonly />
                                                        </div>
                                                    </div>
                                                </div>
                                                <i class="btn btn-danger la la-minus remove_field ml-auto" data-uuid="${data.data.uuid}" data-name="${data.data.name}"></i>
                                                </div>`;

                                        $(wrapper).prepend(html); //add input 

                                        $(".kt-staff-select option[value='" + data.data.uuid + "']").remove();

                                        //Refresh selectpicker
                                        $(staffInput).val('').selectpicker("refresh");
                                        $(typeInput).val('').selectpicker("refresh");

                                        $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
                                            e.preventDefault();
                                            const $this = $(this);
                                            const uuid = $this.attr('data-uuid');
                                            const name = $this.attr('data-name');

                                            $this.parent('div').remove();

                                            $(staffInput).append('<option value="' + uuid + '">' + name + '</option>');
                                            $(staffInput).selectpicker("refresh");
                                        });
                                    }
                                    else {
                                        $.notify('Vui lòng chọn vai trò!', { type: 'danger' });
                                    }
                                })
                                .fail(function () {
                                    $.notify('Vui lòng chọn nhân viên kỹ thuật!', { type: 'danger' });
                                });

                        });
                    //---end: add or remove input in draw---

                    initContent($content);
                })
                .fail(function () {
                    drawer.error();
                });
        });

        drawer.open();
    };

    var initForm = function ($form) {
        $form.validate({
            rules: {
                name: {
                    required: true
                },
                description: {
                    required: true
                },
                costEstimate_string: {
                    required: true
                },
                startTime: {
                    required: true
                },
                finishTime: {
                    required: true
                },
                projectUuid: {
                    required: true
                },
                "customerUuids[]": {
                    required: true
                },
            },
            messages: {
                name: {
                    required: "Vui lòng nhập tên kế hoạch"
                },
                description: {
                    required: "Vui lòng nhập mô tả"
                },
                costEstimate_string: {
                    required: "Vui lòng nhập chi phí dự kiến"
                },
                startTime: {
                    required: "Vui lòng nhập ngày bắt đầu"
                },
                finishTime: {
                    required: "Vui lòng nhập ngày kết thúc"
                },
                projectUuid: {
                    required: "Vui lòng nhập dự án"
                },
                customerUuids: {
                    required: "Vui lòng nhập đơn vị"
                },
            },

            //display error alert on form submit  
            invalidHandler: function () {
                KTUtil.scrollTo($form, -200);
            },
            submitHandler: function (form) {
                debugger
                $(form).find('button[type="submit"]').prop('disabled', true).addClass('kt-spinner kt-spinner--light'); //loading

                const data = $(form).serializeJSON() || {};
                const { uuid } = data;

                $(this).prop("disabled", true);
                $.ajax({
                    url: uuid ? `/taskplan/detail/${uuid}` : 'taskplan/create',
                    method: uuid ? 'PUT' : 'POST',
                    data,
                })
                    .done(function (data) {
                        debugger
                        KTApp.block($form);
                        const successMsg = uuid ? 'Sửa thông tin thành công!' : 'Thêm thông tin thành công!';
                        $.notify(successMsg, { type: 'success' });

                        /**
                         * Close drawer
                         */
                        if (uuid) {
                            localStorage['edit_success'] = JSON.stringify({ from: (uuid ? `/taskplan/detail` : 'taskplan/create'), massage: successMsg });
                            window.location = window.location.origin + `/taskplan`;
                        }
                        else {
                            drawer.close();

                            reloadTable();
                        }

                    })
                    .fail(function (data) {
                        KTApp.unblock($form);
                        $(form).find('button[type="submit"]').prop('disabled', false).removeClass('kt-spinner kt-spinner--light'); //remove loading
                        const successMsg = uuid ? 'Sửa thông tin không thành công!' : 'Thêm thông tin không thành công!';
                        $.notify(data.responseJSON.msg, { type: 'danger' });
                    });
                return false;
            },
        });
    };
    var $form = $('.dmms-taskplan__form');
    initForm($form);
    var initContent = function ($content) {
        /*
            Init form
        */
        const $form = $content.find('.dmms-taskplan__form');
        initForm($form);

        /*
            Init auto-resize
        */
        autosize($content.find('textarea.auto-resize'));

        initdaterangepicker();
        $('#costEstimate_string').simpleMoneyFormat();
        $('.kt-staff-select').selectpicker();
        $('.kt-staffrole-select').selectpicker();
        $('#kt-select-project').selectpicker();
        $('#kt-select-customer').selectpicker({
            liveSearch: true,
            liveSearchPlaceholder: "Tìm kiếm theo tên đơn vị",
            showTick: false,
        });

        $('#kt-select-customer').on('change', function () {
            const $this = $(this);
            var isAllSelected = $this.find('option[value=""]').prop('selected');
            var lastAllSelected = $(this).data('all');
            var selectedOptions = ($this.val()) ? $this.val() : [];
            var allOptionsLength = $this.find('option[value!=""]').length;
            var selectedOptionsLength = selectedOptions.length;

            if (isAllSelected == lastAllSelected) {

                if ($.inArray("", selectedOptions) >= 0) {
                    selectedOptionsLength -= 1;
                }

                if (allOptionsLength <= selectedOptionsLength) {

                    $this.find('option[value=""]').prop('selected', true).parent().selectpicker('refresh');
                    isAllSelected = true;
                } else {
                    $this.find('option[value=""]').prop('selected', false).parent().selectpicker('refresh');
                    isAllSelected = false;
                }

            } else {
                $this.find('option').prop('selected', isAllSelected).parent().selectpicker('refresh');
            }

            var text_display = $('.kt-customer-selectbox').find('.filter-option-inner-inner').text();
            $('.kt-customer-selectbox').find('.filter-option-inner-inner').html(text_display.replace('Chọn tất cả, ', ''));
            debugger;
            $(this).data('all', isAllSelected);
        }).trigger('change');
    };

    //auto fill
    $(document).on('change', '#kt-select-project', function () {
        const projectuuid = $(this).val();
        const wrapper = $("#kt-select-customer"); //Fields wrapper

        //Refresh select box #kt-select-customers
        $(wrapper).html('');

        //fill customers
        if (projectuuid != '') {
            $.ajax({
                url: `/project/projectgetcustomer/${projectuuid}`,
                method: 'GET',
            })
                .done(function (data) {
                    $(wrapper).append('<option value="">Chọn tất cả</option>');
                    if (data.data.items.length !== 0) {
                        $(data.data.items).each(function (index, item) {
                            $(wrapper).append('<option value="' + item.customerUuid + '">' + item.customerName + '</option>');
                        });
                    }
                    //Refresh selectpicker
                    $(wrapper).selectpicker('refresh');
                })
        }
        else {
            //Refresh selectpicker
            $(wrapper).selectpicker('refresh');
        }
    });

    //table customer in plan
    $(".customerInPlan").on('click', function () {
        if (dataTableInstance !== null && dataTableInstance !== undefined) dataTableInstance.destroy();
        const plan_uuid = $('#uuid').val();
        dataTableInstance = $('.kt-datatable--customer').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: `/taskplan/getCustomers/${plan_uuid}`,
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page;
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword, filterStatus } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: (keyword || $('#js-filter-keyword--customer').val()),
                            filterStatus: filterStatus || $('#js-filter-state').val(),
                            page: pagination.page,
                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,
            },
            columns: [
                {
                    field: '',
                    title: '#',
                    sortable: false,
                    width: 20,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * LIMIT;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                //{
                //    field: 'code',
                //    title: 'Mã đơn vị',
                //    width: 60,
                //},
                {
                    field: 'name',
                    title: 'Đơn vị thụ hưởng',
                },
                {
                    field: 'address',
                    title: 'Địa chỉ chi tiết',
                },
                {
                    field: 'villageName',
                    title: 'Phường/Xã',
                    width: 120
                },
                {
                    field: 'townName',
                    title: 'Quận/Huyện',
                    width: 120
                },
                {
                    field: 'provinceName',
                    title: 'Tỉnh/Thành',
                    width: 120
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    });

    //Table task in plan
    $(".taskInPlan").on('click', function () {
        if (dataTableInstance !== null && dataTableInstance !== undefined) dataTableInstance.destroy();
        const plan_uuid = $('#uuid').val();
        dataTableInstance = $('.kt-datatable--task').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: `/taskplan/getTasks/${plan_uuid}`,
                    },
                    response: {
                        map: function (res) {
                            const raw = res && res.data || {};
                            const { pagination } = raw;
                            const perpage = pagination === undefined ? LIMIT : pagination.perpage;
                            const page = pagination === undefined ? undefined : pagination.page;
                            CURRENT_PAGE = page || 1;
                            LIMIT = perpage;
                            START_PAGE = 0;
                            return {
                                data: raw.items || [],
                                meta: {
                                    ...pagination || {},
                                    perpage: perpage || LIMIT,
                                    page: page || 1,
                                },
                            }
                        },
                    },
                    filter: function (data = {}) {
                        const query = data.query || {};
                        const pagination = data.pagination || {};
                        const { keyword, filterStatus } = query;
                        if (START_PAGE === 1) {
                            pagination.page = START_PAGE;
                        }
                        return {
                            limit: pagination.perpage || LIMIT,
                            keyword: (keyword || $('#js-filter-keyword--task').val()),
                            filterStatus: filterStatus || $('#js-filter-state').val(),
                            page: pagination.page,

                        };
                    }
                },
                pageSize: LIMIT,
                serverPaging: true,
                serverFiltering: true,
            },

            search: {
                onEnter: true,
            },
            columns: [
                {
                    field: '',
                    title: '#',
                    sortable: false,
                    width: 20,
                    textAlign: 'center',
                    template: function (data, row) {
                        return (row + 1) + (CURRENT_PAGE - 1) * MAX_ITEM_INPAGE;
                    },
                    responsive: {
                        visible: 'md',
                        hidden: 'sm'
                    }
                },
                {
                    field: 'code',
                    title: 'Mã công việc',
                    width: 80,
                },
                {
                    field: 'name',
                    title: 'Công việc',
                },
                {
                    field: 'customerName',
                    title: 'Đơn vị thụ hưởng',
                },
                {
                    field: '#',
                    title: 'Kỹ thuật phụ trách',
                    width: 120,
                    template: function (data) {
                        console.log(data);
                        return `<span>${data.technicianName} - ${data.technicianPhoneNumber}</span>`;
                    }
                },
                {
                    field: 'percent',
                    title: 'Tiến độ công việc',
                    width: 100,
                    textAlign: 'center',
                    template: function (data) {
                        debugger
                        var floor = Math.floor(data.percent);
                        var color = ``;
                        if (floor == 0) {
                            color = "progress-bar-striped bg-danger";
                        }
                        else if (floor < 50) {
                            color = "progress-bar-striped bg-warning";
                        }
                        else if (floor == 100) {
                            color = "progress-bar-striped bg-success";
                        }
                        else {
                            color = "progress-bar-striped bg-primary";
                        };
                        var html = `<div class="progress">
                                        <div class="progress-bar ${color}" role="progressbar" style="width: ${floor}%;" aria-valuenow="${floor}" aria-valuemin="0" aria-valuemax="100">${floor}%</div>
                                    </div>`
                        return html;
                    }
                },
                {
                    field: 'state',
                    title: 'Trạng thái',
                    textAlign: 'center',
                    width: 110,
                    template: function (data) {
                        var states = {
                            1: { 'title': 'Chưa tiếp nhận', 'class': 'kt-badge--danger' },
                            2: { 'title': 'Yêu cầu hỗ trợ', 'class': ' kt-badge--danger' },
                            3: { 'title': 'Đã tiếp nhận', 'class': ' kt-badge--primary' },
                            4: { 'title': 'Đã hẹn lịch', 'class': ' kt-badge--info' },
                            5: { 'title': 'Đang xử lý', 'class': ' kt-badge--warning' },
                            6: { 'title': 'Đợi đề xuất', 'class': ' kt-badge--warning' },
                            7: { 'title': 'Yêu cầu tiếp tục', 'class': ' kt-badge--success' },
                            8: { 'title': 'Hoàn thành', 'class': ' kt-badge--success' },
                        };
                        if (data.state) {
                            return '<span class="kt-badge ' + states[data.state].class + ' kt-badge--inline kt-badge--pill">' + states[data.state].title + '</span>';
                        }
                        else {
                            return '<span class="kt-badge ' + states[1].class + ' kt-badge--inline kt-badge--pill">' + states[1].title + '</span>';
                        }
                    }
                }],
            sortable: false,
            pagination: true,
            responsive: true,
        });
    });
    $('.changestate').selectpicker();
    $('#kt-select-customer').selectpicker({
        liveSearch: true,
        liveSearchPlaceholder: "Tìm kiếm theo tên đơn vị",
        showTick: false,
    });

    $('#kt-select-customer').on('change', function () {
        const $this = $(this);
        var isAllSelected = $this.find('option[value=""]').prop('selected');
        var lastAllSelected = $(this).data('all');
        var selectedOptions = ($this.val()) ? $this.val() : [];
        var allOptionsLength = $this.find('option[value!=""]').length;
        var selectedOptionsLength = selectedOptions.length;

        if (isAllSelected == lastAllSelected) {

            if ($.inArray("", selectedOptions) >= 0) {
                selectedOptionsLength -= 1;
            }

            if (allOptionsLength <= selectedOptionsLength) {

                $this.find('option[value=""]').prop('selected', true).parent().selectpicker('refresh');
                isAllSelected = true;
            } else {
                $this.find('option[value=""]').prop('selected', false).parent().selectpicker('refresh');
                isAllSelected = false;
            }

        } else {
            $this.find('option').prop('selected', isAllSelected).parent().selectpicker('refresh');
        }

        var text_display = $('.kt-customer-selectbox').find('.filter-option-inner-inner').text();
        $('.kt-customer-selectbox').find('.filter-option-inner-inner').html(text_display.replace('Chọn tất cả, ', ''));
        debugger;
        $(this).data('all', isAllSelected);
    }).trigger('change');

    return {
        // Public functions
        init: function () {
            initTable();
            initSearch();
            initEventListeners();

            if (window.location.pathname != "/taskplan") {
                initdaterangepicker();
                if ($('#responserUuid_value').val() != '') {
                    $('#kt-select-teamleader').selectpicker('val', $('#responserUuid_value').val());
                }
                else {
                    $('#kt-select-teamleader').selectpicker();
                }
            }
        },
    };
}();

$(document).ready(function () {
    if ($('.dmms-taskPlan').length) {
        DMMSTaskPlan.init();
    }
});