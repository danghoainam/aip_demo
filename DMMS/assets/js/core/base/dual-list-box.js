"use strict";

// plugin setup
var KTDualListBox = function (element, options) {
  // Main object
  var the = this;

  var instance;

  // Default options
  var defaultOptions = {};

  ////////////////////////////
  // ** Private Methods  ** //
  ////////////////////////////

  var Plugin = {
    /**
     * Construct
     */

    construct: function (element, options) {
      Plugin.init(element, options);

      return the;
    },

    /**
     * Handles subtoggle click toggle
     */
    init: function (element, options) {
      the.events = [];

      instance = new DualListbox(element, {
        addButtonText: `<i class='flaticon2-next'></i>`,
        removeButtonText: `<i class='flaticon2-back'></i>`,
        addAllButtonText: `<i class='flaticon2-fast-next'></i>`,
        removeAllButtonText: `<i class='flaticon2-fast-back'></i>`,
        ...options,
      });

      $(instance.search).attr('placeholder', 'Tìm kiếm');

      // merge default and user defined options
      the.options = KTUtil.deepExtend({}, defaultOptions, options);
    },

    /**
         * Trigger events
         */
    eventTrigger: function (name) {
      //KTUtil.triggerCustomEvent(name);
      for (var i = 0; i < the.events.length; i++) {
        var event = the.events[i];
        if (event.name == name) {
          if (event.one == true) {
            if (event.fired == false) {
              the.events[i].fired = true;
              return event.handler.call(this, the);
            }
          } else {
            return event.handler.call(this, the);
          }
        }
      }
    },

    addEvent: function (name, handler, one) {
      the.events.push({
        name: name,
        handler: handler,
        one: one,
        fired: false
      });

      return the;
    }
  };

  //////////////////////////
  // ** Public Methods ** //
  //////////////////////////

  /**
   * Set default options 
   */

  the.setDefaults = function (options) {
    defaultOptions = options;
  };

  /**
     * Attach event
     */
  the.on = function (name, handler) {
    return Plugin.addEvent(name, handler);
  };

  /**
   * Attach event that will be fired once
   */
  the.one = function (name, handler) {
    return Plugin.addEvent(name, handler, true);
  };

  // Construct plugin
  Plugin.construct.apply(the, [element, options]);

  return the;
};

// webpack support
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
  module.exports = KTDualListBox;
}