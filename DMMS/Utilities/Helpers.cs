﻿using DMMS.Objects;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DMMS.Utilities
{
    public class Helpers
    {
        public static string ConvertDateTimeToString(long time)
        {
            if(time > 0)
            {
                var date = new DateTime(time);
                return date.ToString("dd/MM/yyyy HH:mm");
            }
            return string.Empty;

        }

        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        public static bool IsTokenExpiration(string token)
        {            
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(token);
            var tokenS = handler.ReadToken(token) as JwtSecurityToken;
            var exp = tokenS.Claims.First(claim => claim.Type == "exp").Value;
            var currentTime = DateTime.Now;
            var expTime = new DateTime(1970,1,1).AddMilliseconds(Convert.ToDouble(exp));
            if(currentTime > expTime)
            {
                return false;
            }
            return true;
            //eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlhdCI6MTU3NjUxNzE3NSwiZXhwIjoxNTc2NTI3MjU1fQ.y81WfG4SczoJUhl9RbHwoeC9W10nQDCZWwkq_gAiRv4
        }

        /// <summary>
        /// Check token expiration: True is timeout
        /// </summary>
        /// <param name="claimsIndentity"></param>
        /// <returns></returns>
        public static bool IsTokenExpiration(ClaimsIdentity claimsIndentity)
        {
            var ttl = (claimsIndentity != null && claimsIndentity.Claims != null && claimsIndentity.FindFirst(Constants.TTL) != null) ? claimsIndentity.FindFirst(Constants.TTL).Value : string.Empty;
            if (string.IsNullOrEmpty(ttl))
            {
                return true;
            }
            var currentTime = DateTime.Now.AddMinutes(Constants.TTL_DELTA);
            var timeOut = Convert.ToDateTime(ttl);
            if (currentTime >= timeOut)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Map them field cho pagination
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="limit"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public static PaginationResponse MergePagination(PaginationResponse pagination, int limit, int page)
        {
            if (pagination == null)
            {
                return new PaginationResponse();
            }
            pagination.perpage = limit;
            pagination.page = page;
            return pagination;
        }

        public static byte[] UploadFiles(string address, byte[] fileBinary, string contentType, string fileName)
        {
            var request = WebRequest.Create(address);
            request.Method = "POST";
            var boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x", NumberFormatInfo.InvariantInfo);
            request.ContentType = "multipart/form-data; boundary=" + boundary;
            boundary = "--" + boundary;

            using (var requestStream = request.GetRequestStream())
            {
                Stream stream = new MemoryStream(fileBinary);
                var buffer = Encoding.ASCII.GetBytes(boundary + Environment.NewLine);
                requestStream.Write(buffer, 0, buffer.Length);
                buffer = Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"upload\"; filename=\"{1}\"{2}", fileName, fileName, Environment.NewLine));
                requestStream.Write(buffer, 0, buffer.Length);
                buffer = Encoding.ASCII.GetBytes(string.Format("Content-Type: {0}{1}{1}", contentType, Environment.NewLine));
                requestStream.Write(buffer, 0, buffer.Length);
                stream.CopyTo(requestStream);
                buffer = Encoding.ASCII.GetBytes(Environment.NewLine);
                requestStream.Write(buffer, 0, buffer.Length);

                var boundaryBuffer = Encoding.ASCII.GetBytes(boundary + "--");
                requestStream.Write(boundaryBuffer, 0, boundaryBuffer.Length);
            }

            using (var response = request.GetResponse())
            using (var responseStream = response.GetResponseStream())
            using (var stream = new MemoryStream())
            {
                if (responseStream != null) responseStream.CopyTo(stream);
                return stream.ToArray();
            }
        }

        

    }

    public static class ExtensionMethods
    {
        public static SelectList ToSelectList<TEnum>()
            where TEnum : struct, IComparable, IFormattable, IConvertible // correct one
        {

            return new SelectList(Enum.GetValues(typeof(TEnum)).OfType<Enum>()
                .Select(x =>
                    new SelectListItem
                    {
                        Text = GetEnumDescription(x),
                        Value = (Convert.ToInt32(x)).ToString()
                    }), "Value", "Text");
        }

        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = fi.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];

            if (attributes != null && attributes.Any())
            {
                return attributes.First().Description;
            }

            return value.ToString();
        }

        public static string DisplayName(this Enum value)
        {
            FieldInfo field = value.GetType().GetField(value.ToString());

            EnumDisplayNameAttribute attribute
                    = Attribute.GetCustomAttribute(field, typeof(EnumDisplayNameAttribute))
                        as EnumDisplayNameAttribute;

            return attribute == null ? value.ToString() : attribute.DisplayName;
        }

        public class EnumDisplayNameAttribute : Attribute
        {
            private string _displayName;
            public string DisplayName
            {
                get { return _displayName; }
                set { _displayName = value; }
            }
        }
    }
}
