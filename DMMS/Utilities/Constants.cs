﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DMMS.Utilities
{
    public class Constants
    {
        public const string SubHeaderData = "SubHeader";
        /// <summary>
        /// Minute after timeout
        /// </summary>
        public const int TTL_DELTA = 1;

        public const string TOKEN = "Token";
        public const string REFRESH_TOKEN = "Refresh_Token";
        public const string TTL = "TTL";
        public const string IMAGE_UPLOAD_FORMAT = "------{0}\r\nContent-Disposition: form-data; name=\"images[{1}]\"; filename=\"{2}\"\r\nContent-Type: {3}\r\n\r\n\r\n";
        public const string USER_AVATAR = "User_Avatar";
        public const string USER_INFO = "User_Info";

        #region Message
        public const string MESSAGE_ERROR = "Có lỗi xảy ra, vui lòng kiểm tra lại thông tin!";
        #endregion
    }

    public static class Icon
    {
        public const string Dashboard = "flaticon2-architecture-and-city";
        public const string Account = "flaticon2-user-1";
        public const string Ticket = "flaticon-responsive";
        public const string Task = "la la-tasks";
        public const string Project = "flaticon-notes";
        public const string Customer = "flaticon-user";
        public const string Staff = "flaticon-users";
        public const string Device = " flaticon-responsive";
        public const string Team = "flaticon2-group";
        public const string Group = "flaticon-users";
        public const string ReportDaily = "flaticon-responsive";
        public const string Category = "kt-menu__link-bullet--dot";
        public const string Supplier = "kt-menu__link-bullet--dot";
        public const string Location = "la la-location-arrow";
        public const string TimeWarning = "la la-warning";
        public const string Contract = "kt-menu__link-bullet--dot";
        public const string Sipaccount = "flaticon-computer";
        public const string Calllog = "flaticon-computer";
        public const string Tree = "flaticon-map";
        public const string Package = "flaticon2-delivery-package";
        public const string TaskPlan = "flaticon2-list-2";
        public const string Bunker = "flaticon2-list-2";
    }

    public static class MenuTitle
    {
        public const string Dashboard = "Dashboard";
        public const string Account = "Tài khoản";
        public const string Ticket = "Yêu cầu";
        public const string Task = "Công việc";
        public const string Project = "Dự án";
        public const string Customer = "Đơn vị";
        public const string Staff = "Nhân viên";
        public const string Device = "Thiết bị";
        public const string Team = "Đội - nhóm";
        public const string Group = "Khu vực quản lý";
        public const string ReportDaily = "Báo cáo";
        public const string Category = "Loại thiết bị";
        public const string Supplier = "Nhà cung cấp";
        public const string TaskOffer = "Danh sách đề xuất";
        public const string TimeWarning = "Thời gian cảnh báo";
        public const string Location = "Đơn vị hành chính";
        public const string Contract = "Hợp đồng";
        public const string Report = "Báo cáo";
        public const string SipAccount = "SipAccount";
        public const string Calllog = "Quản lý cuộc gọi";
        public const string Tree = "Mô hình phân quyền bộ máy tổ chức AIC Group JSC";
        public const string Package = "Quản lý phòng thiết bị";
        public const string TaskPlan = "Kế hoạch công việc";
        public const string Bunker = "Kho thiết bị vật tư";
        public const string Field = "Quản lý lĩnh vực";
        public const string listsupplies = "Quản lý danh mục Thiết bị - Vật tư";
        public const string Roles = "Quản lý danh chức năng";
    }

    public static class SubMenuTitle
    {
        public const string AccountInfo = "Thông tin tài khoản";
        public const string DeviceList = "Danh sách thiết bị";
        public const string Category = "Loại thiết bị";
        public const string Supplier = "Nhà cung cấp";
        public const string DailyReport = "Báo cáo công việc";
        public const string Bunker = "Danh sách đề xuất kho Vật tư - Thiết bị";
        public const string ReportOverview = "Báo cáo thống kê tổng quan";
        public const string ReportStatisticDetail = "Báo cáo thống kê chi tiết dự án";
        public const string ReportStatisticTroubleshoot = "Báo cáo thống kê theo khắc phục sự cố";
        public const string ReportStatisticOverviewTask = "Báo cáo thống kê tổng quan theo công việc";
        public const string ReportStatisticDetailTask = "Báo cáo thống kê chi tiết theo công việc";
        public const string ReportStatisticOverviewCall = "Báo cáo thống kê tổng quan cuộc gọi";
        public const string ReportStatisticDetailCall = "Báo cáo thống kê chi tiết cuộc gọi";
        public const string ReportStatisticOverviewDeviceAndSupplies = "Thống kê thiết bị sửa chữa và vật tư tiêu hao";
        public const string ReportStatisticDetailStatusDevice = "Báo cáo thống kê chi tiết trạng thái thiết bị sửa chữa";
        public const string ReportStatisticDetailSuppliesTask = "Báo cáo thống kê chi tiết vật tư tiêu hao trong công việc";
        public const string ReportStatisticDetailStatusContract = "Báo cáo thống kê chi tiết hợp đồng";
        public const string Roles = "Danh sách chức năng";
    }

    public enum EnumError
    {
        Error = 1,
        Success = 0
    }

    public enum EnumTicketState
    {
        [Description("Chưa tiếp nhận")]
        New = 1,
        [Description("Đã tiếp nhận")]
        Received = 2,
        [Description("Đang xử lý")]
        Processing = 3,
        [Description("Đã xử lý xong")]
        Processinged = 4,
        [Description("Hoàn thành")]
        Finish = 5,
       [Description("Hủy")]
        Cancel = 6
    }

    public enum EnumStaffRole
    {
        [Description("Chịu trách nhiệm")]
        Responsible = 1,
        [Description("Hỗ trợ")]
        Support = 2,
    }

    public enum EnumTypeStaff
    {
        [Description("Kỹ thuật")]
        Tech = 1,
        [Description("CSKH")]
        OP = 2,
        [Description("Quản lý")]
        GM = 3,
        [Description("Kho")]
        Stoge = 4,
    }

    public enum EnumTicketPriority
    {
        [Description("Khẩn cấp")]
        Emergency = 1,
        [Description("Ưu tiên")]
        Priority = 2,
        [Description("Bình thường")]
        Normal = 3,
    }

    public enum EnumTypeWarning
    {
        [Description("Yêu cầu đề xuất")]
        Ticket = 0,
        [Description("Thiết bị")]
        Device = 1,
        [Description("Công việc")]
        Task = 2,
        [Description("Báo cáo hằng ngày")]
        DailyReport = 3,
        [Description("HSD hợp đồng")]
        ContractExpired = 4,
    }

    public enum EnumLockOrUnlock
    {
        [Description("Khóa vĩnh viễn")]
        Lock = 1,
        [Description("Mở khóa")]
        Unlock = 2
    }

    public enum EnumTaskType
    {
        [Description("Phát sinh từ Ticket")]
        TicketIncurred = 1,
        [Description("Team Leader phân công")]
        LeaderAssigned = 2,
        [Description("Phát sinh từ Bảo hành bảo trì")]
        WarrantyIncurred = 3
    }

    public enum EnumContractState
    {
        [Description("Còn HSD")]
        Remaining = 1,
        [Description("Quá HSD")]
        OutDate = 2,
        [Description("Đã Đóng")]
        Closed = 3
    }

    public enum EnumTaskPlanState
    {
        [Description("Chưa duyệt")]
        NotApproved = 0,
        [Description("Đã duyệt")]
        Approved = 1,
        [Description("Đã từ chối")]
        Refuse = 2,
        [Description("Đã hoàn thành")]
        Refuse3 = 3,
        [Description("Đã hủy")]
        Refuse4 = 4,
        [Description("Chưa lập kế hoạch")]
        Refuse5 = 5,
        [Description("Đang triển khai")]
        Refuse6 = 6,
        [Description("Đã báo cáo kết quả")]
        Refuse7 = 7,
    }

    public enum EnumTaskState
    {
        [Description("Chưa tiếp nhận")]
        New = 1,
        [Description("Yêu cầu hỗ trợ")]
        RequireSupported = 2,
        [Description("Đã tiếp nhận")]
        Received = 3,
        [Description("Đã lên kế hoạch")]
        ScheduleAppointment = 4,
        [Description("Đang xử lý")]
        Processing = 5,
        [Description("Đợi duyệt đề xuất xử lý")]
        WaitApproved = 6,
        [Description("Yêu cầu tiếp tục")]
        RequestContinue = 7,
        [Description("Hoàn thành")]
        Finish = 8,
        [Description("Cập nhật số lượng TB")]
        Finish1 = 9,
        [Description("Hủy")]
        Cancel = 10
    }

    public enum EnumReportTaskState
    {
        [Description("Tạo mới")]
        New = 1,
        [Description("Yêu cầu hỗ trợ")]
        RequireSupported = 2,
        [Description("Đã tiếp nhận")]
        Received = 3,
        [Description("Đã hẹn lịch")]
        ScheduleAppointment = 4,
        [Description("Đang xử lý")]
        Processing = 5,
        [Description("Đợi duyệt đề xuất xử lý")]
        WaitApproved = 6,
        [Description("Yêu cầu tiếp tục")]
        RequestContinue = 7,
        [Description("Hoàn thành")]
        Finish = 8,
        [Description("Cập nhật số lượng thiết bị trong task bảo trì")]
        UpdateCountDevice = 9,
        [Description("Đã hủy")]
        Cancelled = 10
    }

    public enum EnumReportTicketState
    {
        [Description("Mới")]
        New = 1,
        [Description("Đã tiếp nhận")]
        Received = 2,
        [Description("Đang xử lý")]
        Processing = 3,
        [Description("Đã xử lý xong")]
        FinishProcessing = 4,
        [Description("Hoàn thành")]
        Finish = 5,
        [Description("Đã hủy")]
        Cancelled = 6
    }

    public enum EnumDeviceState
    {
        [Description("Đang sử dụng")]
        Using = 1,
        [Description("Đang yêu cầu sửa chữa")]
        RequestingRepairs = 2,
        [Description("Đến lịch bảo hành")]
        WarrantySchedule = 3,
        [Description("Đang xử lý theo yêu cầu")]
        ProcessingRequest = 4,
        [Description("Cần thay thế")]
        NeedReplace = 5,
        [Description("Mang đi sửa chữa-Không có thiết bị thay thế")]
        NoReplacementEquipment = 6,
        [Description("Mang đi sửa chữa-Có thiết bị thay thế")]
        HaveReplacementEquipment = 7,
        [Description("Thay thế")]
        Replace = 8,
        [Description("Hỏng")]
        Damaged = 9,
        [Description("Trong kho - Trong kho vật lý, ba lô nhân viên sau khi import")]
        Damaged1 = 10,
        [Description("Trong kho - Hỏng, sau khi kiểm kê")]
        Damaged2 = 11,
        [Description("Đang vận chuyển")]
        Damaged3 = 12,
        [Description("Đã xuất thanh lý")]
        Damaged4 = 13,
        [Description("Đã xuất tiêu hao")]
        Damaged5 = 14,
        [Description("Đã hoàn trả nhà cung cấp")]
        Damaged6 = 15,


    }
    public enum EnumStateprocus
    {
        [Description("Đang bảo hành")]
        BH = 1,
        [Description("Hết bảo hành")]
        BH1 = 2,
        [Description("Đã gửi thông báo")]
        BH2 = 3,
        [Description("Đã ký đóng")]
        BH3 = 4,
        [Description("Đóng bảo hành")]
        BH4 = 5,
        [Description("Gia hạn bảo hành")]
        BH5 = 6,
        [Description("Chưa nghiệm thu")]
        BH6 = 7,

    }
    public enum EnumReportState
    {
        [Description("Chưa tạo")]
        NotCreated = 1,
        [Description("Chưa duyệt")]
        NotApproved = 2,
        [Description("Đã duyệt")]
        Approved = 3,
        [Description("Từ chối")]
        Denined = 4,
    }
    public enum EnumOffferState
    {
        [Description("Chưa duyệt")]
        NotCreated = 1,
        [Description("Đã duyệt")]
        NotApproved = 2,
        [Description("Từ chối")]
        Approved = 3,

    }

    public enum EnumBunkerState //Trạng thái của kho
    {
        [Description("Tạm ngưng")]
        Pause = 0,
        [Description("Hoạt động")]
        Active = 1,
        [Description("Kiểm kê")]
        Inventory = 2,
    }

    public enum EnumOfferInBunker //Trạng thái của đề xuất trong kho
    {
        [Description("Đã duyệt")]
        Approved = 1,
        [Description("Chưa duyệt")]
        NotApproved = 2,
        [Description("Đã từ chối")]
        Declined = 3,
        [Description("Đã tiếp nhận")]
        Received = 4,
        [Description("Đã Hoàn thành")]
        Finished = 5,
    }

    public enum EnumTypeCard //Loại phiếu
    {
        [Description("Nhập kho")]
        Import = 1,
        [Description("Xuất kho")]
        Export = 2,
    }

    public enum EnumTypeImportExport //Loại phiếu
    {
        [Description("Nhập từ NCC")]
        ImportFromSupplier = 1,
        [Description("Nhập từ NVKT")]
        ImportFromTech = 2,
        [Description("Nhập từ kho")]
        ImportFromBunker = 3,
        [Description("Xuất cho NVKT")]
        ExportForTech = 4,
        [Description("Xuất cho kho")]
        ExportForBunker = 5,
        [Description("Xuất cho công việc")]
        ExportForTask = 6,
        [Description("Xuất hoàn trả NCC")]
        ExportForSupplier = 7,
        [Description("Xuất tiêu hao")]
        ExportConsumable = 8,
        [Description("Xuất thanh lý")]
        ExportLiquidation = 9,
    }

    public enum EnumStateExportTo
    {
        [Description("Chưa xác nhận")]
        Unconfimred = 1,
        [Description("Đã xác nhận")]
        Confimred = 2,
        [Description("Đã từ chối")]
        Declined = 3,
        [Description("Đợi xử lý")]
        Processing = 4,
        [Description("Đã thu hồi")]
        Withdrawn = 5,
    }

    public enum EnumGuarantee
    {
        [Description("1 Tháng")]
        One = 1,
        [Description("2 Tháng")]
        Two = 2,
        [Description("3 Tháng")]
        Three = 3,
        [Description("4 Tháng")]
        Four = 4,
        [Description("5 Tháng")]
        Five = 5,
        [Description("6 Tháng")]
        Six = 6,
        [Description("7 Tháng")]
        Seven = 7,
        [Description("8 Tháng")]
        Èight = 8,
        [Description("9 Tháng")]
        Nine = 9,
        [Description("10 Tháng")]
        Ten = 10,
        [Description("11 Tháng")]
        Eleven = 11,
        [Description("12 Tháng")]
        Twelve = 12,
    }
    public enum ListSupplies
    {
        [Description("Thiết bị")]
        Device = 1,
        [Description("Vật tư")]
        Supplies = 2,
        [Description("Dụng cụ lao động")]
        Labor = 3,

    }
    public enum EnumVote
    {
        [Description("Nhập kho")]
        Warehousing = 1,
        [Description("Xuất kho")]
        Outstock = 2,
    }
    public enum EnumAction
    {
        View,
        Add,
        Edit,
        Delete,
        Import,
        Export
    }

}
